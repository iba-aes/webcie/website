#!/usr/bin/php
<?php
$input = file_get_contents("php://stdin");
if ($input === "") {
	echo getenv("MSGFILTER_MSGID");
} else {
	echo $input;
}
