<?php
// geheime dingen

class secret
{
	static function dbpasswords()
	{
		return [
			'gast'		=> getenv('DB_PASS_GAST'),
			'ingelogd'	=> getenv('DB_PASS_INGELOGD'),
			'dibstab'	=> getenv('DB_PASS_DIBSTABLET'),
			'mollie'	=> getenv('DB_PASS_MOLLIE'),
			'god'		=> getenv('DB_PASS_GOD')
		];
	}
}
define("IDEAL_AES_CERT_PASSWD", getenv("IDEAL_AES_CERT_PASSWD"));
define("SITE_WIDE_SALT", getenv("SITE_WIDE_SALT"));
define("IDEAL_AES_KEY", getenv("IDEAL_AES_KEY"));
define("KERB_AES_KEY", getenv("KERB_AES_KEY"));
define("KERB_SALT", getenv("KERB_SALT"));
define("QRCODE_SALT", getenv("QRCODE_SALT"));
define("SENTRY_URL", getenv("SENTRY_URL"));

define("MOLLIE_SALT_KEY", getenv("MOLLIE_SALT_KEY"));

define("MOLLIE_LIVE", getenv("MOLLIE_LIVE"));
define("MOLLIE_TEST", getenv("MOLLIE_TEST"));

define("DATABASE_HOST", getenv("DATABASE_HOST"));
