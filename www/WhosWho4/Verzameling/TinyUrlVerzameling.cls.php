<?

/**
 * $Id$
 */
class TinyUrlVerzameling
	extends TinyUrlVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TinyUrlVerzameling_Generated
	}

	//Geef het totaal aantal TinyUrls, handig voor overzichten.
	static public function totaalAantal()
	{
		global $WSW4DB;
		return $WSW4DB->q("COLUMN SELECT COUNT(`ID`) FROM `TinyUrl`");
	}
	
	/**
	 * Geef het totaal aantal TinyUrls die voldoen aan de zoekopdracht.
	 * @param string|null zoektekst De tekst om op te zoeken. Indien leeg wordt niet niet gefilterd.
	 * @return int
	 */
	public static function totaalZoekAantal($zoektekst = null)
	{
		global $WSW4DB;
		
		$zoekSQL = "";
		if ($zoektekst)
		{
			$zoekSQL = " WHERE `tinyUrl` LIKE %s OR `outgoingUrl` LIKE %s";
		}
		else
		{
			$zoekSQL = " ";
		}		
		return $WSW4DB->q("COLUMN SELECT COUNT(`ID`) FROM `TinyUrl`". $zoekSQL, "%$zoektekst%", "%$zoektekst%");
	}

	static public function geefAlle($range = null, $zoektekst = null)
	{
		global $WSW4DB;

		//Check of er een limit nodig is (format: plek_aantal)
		$rangeSQL = "";
		if($range){
			$range = explode("_",$range)[0];
			$rangeSQL = " LIMIT %i, 50";
		} else {
			$rangeSQL = " LIMIT 50";
		}
				
		$zoekSQL = " WHERE `tinyUrl` LIKE %s OR `outgoingUrl` LIKE %s";	

		$ids = $WSW4DB->q("COLUMN SELECT `ID` FROM `TinyUrl`" . $zoekSQL . $rangeSQL, "%$zoektekst%", "%$zoektekst%", $range);

		return TinyUrlVerzameling::verzamel($ids);

	}

	static public function vanMailing(Mailing $mailing)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `tinyUrl_id` FROM `TinyMailingUrl` 
							WHERE `mailing_mailingID` = %i", $mailing->getMailingID());

		return TinyUrlVerzameling::verzamel($ids);
	}

	public function totaalHits()
	{
		$totaal = 0;

		foreach($this as $tinyUrl){
			$totaal = $totaal + $tinyUrl->amountOfHits();
		}

		return $totaal;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
