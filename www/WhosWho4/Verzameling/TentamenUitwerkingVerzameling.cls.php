<?
class TentamenUitwerkingVerzameling
	extends TentamenUitwerkingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TentamenUitwerkingVerzameling_Generated
	}

	public function filterZichtbaar()
	{
		return self::filter('isZichtbaar' , true);
	}

	public function geefAantalZichtbaar()
	{
		return self::filter('isZichtbaar' , true)->aantal();
	}

	static public function geefVanTentamen(Tentamen $tentamen)
	{
		return TentamenUitwerkingQuery::table()
			->whereProp('Tentamen', $tentamen)
			->verzamel();
	}

	// Geef alle niet nagekeken uitwerkingen.
	static public function getAlleNietGecontroleerd()
	{
		return TentamenUitwerkingQuery::table()
			->whereProp('gecontroleerd', false)
			->verzamel();
	}

	static public function getAantalNietGecontroleerd()
	{
		return TentamenUitwerkingQuery::table()
			->whereProp('gecontroleerd', false)
			->count();
	}
}
