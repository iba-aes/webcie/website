<?
class ExtraPosterVerzameling
	extends ExtraPosterVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ExtraPosterVerzameling_Generated
	}

	public static function posters($alleenZichtbaar = FALSE)
	{
		$query = ExtraPosterQuery::table()->orderByDesc('extraPosterID');
		if ($alleenZichtbaar) {
			$query->whereProp('zichtbaar', true);
		}
		return $query->verzamel();
	}
}
