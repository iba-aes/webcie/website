<?
class PersoonBadgeVerzameling
	extends PersoonBadgeVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PersoonBadgeVerzameling_Generated
	}

	static public function geefAllePersonenMetBadge($naam = null)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `persoon_contactID` FROM `PersoonBadge` "
				. (!is_null($naam) ? "WHERE `badges` LIKE %s" : "%_"), "%" . $naam . "%");

		return PersoonVerzameling::verzamel($ids);
	}

	static public function geefAlleAantallenVanBadges()
	{
		global $BADGES, $WSW4DB, $cache;

		$last = $WSW4DB->q("MAYBEVALUE SELECT MAX(`gewijzigdWanneer`) "
						. "FROM `PersoonBadge`"
					);

		$to_cache = false;
		$cached_last_item = $cache->getItem('achievements_last_modified');

		if(is_null($last))
		{
			$to_cache = true;
		}
		else
		{
			$last = new DateTimeLocale($last);

			if(!$cached_last_item->isHit() || new DateTimeLocale($cached_last_item->get()) < $last)
			{
				$to_cache = true;
			}
			else
			{
				$to_cache = false;
			}
		}

		if($to_cache)
		{
			$cached_last_item->set($last->__toString());
			$cache->save($cached_last_item);
			$personen = PersoonBadgeVerzameling::geefAllePersonenMetBadge();
		}

		$aantallen = array();

		foreach($BADGES['onetime'] as $key => $badge)
		{
			$item = $cache->getItem("achievements_$key");
			if($to_cache)
			{
				$aantal = 0;
				foreach($personen as $pers) {
					if($pers->hasBadge($key))
						$aantal++;
				}

				$item->set($aantal);
				$cache->save($item);
			}
			else
			{
				$aantal = $item->get();
			}

			$aantallen[$key] = $aantal;
		}

		foreach($BADGES['bsgp'] as $key => $badge)
		{
			if($badge['multi'] == 'short') {
				$multi = array(1, 2, 3, 4);
			} else {
				$multi = array(1, 2, 5, 10);
			}

			foreach($multi as $k => $m)
			{
				$m = $badge['value'] * $m;

				$item = $cache->getItem("achievements_{$key}_{$m}");

				if($to_cache)
				{
					$aantal = 0;
					foreach($personen as $pers) {
						if($pers->hasBadge($key.'_'.$m))
							$aantal++;
					}

					$item->set($aantal);
					$cache->save($item);
				}
				else
				{
					$aantal = $item->get();
				}

				$aantallen[$key.'_'.$m] = $aantal;
			}
		}

		return $aantallen;
	}
}
