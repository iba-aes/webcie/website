<?
/**
 * $Id$
 */
class TinyUrlHitVerzameling
	extends TinyUrlHitVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TinyUrlHitVerzameling_Generated
	}

	public static function vanTinyUrl(TinyUrl $turl)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `id` FROM `TinyUrlHit`
						WHERE `tinyUrl_id` = %i", $turl->geefID());

		return TinyUrlHitVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
