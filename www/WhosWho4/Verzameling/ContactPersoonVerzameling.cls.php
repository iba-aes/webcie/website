<?

/**
 * $Id$
 */
class ContactPersoonVerzameling
	extends ContactPersoonVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContactPersoonVerzameling_Generated
	}

	public static function metConstraints(Organisatie $org = null, $functie = null, $status = 'huidig', $persoonid = null, $type = "")
	{
		global $WSW4DB;

		switch(strtolower($status)){
			case 'alle':
				$statusSQL = '';
				break;
			case 'oud':
				$statusSQL = ' AND `einddatum` < NOW()';
				break;
			case 'huidig':
			default:
				$statusSQL = ' AND ( `einddatum` IS NULL OR `einddatum` > NOW())
							 AND ( `begindatum` IS NULL OR `begindatum` < NOW())';
				break;
		}

		$orgid = null;
		if($org)
			$orgid = $org->geefID();

		$rows = $WSW4DB->q('TABLE SELECT `persoon_contactID` as persid, `organisatie_contactID` as orgid, `type` as type'
						. ' FROM `ContactPersoon`'
						. ' WHERE 1'
						. ($functie?' AND `Functie` = %s':'%_')
						. $statusSQL
						. ($org?' AND `organisatie_contactID` = %i':'%_')
						. ($persoonid?' AND `persoon_contactID` = %i':'%_')
						. ($type?' AND `type` = %s':'%_')
						. ' ORDER BY `type`',
						$functie, $orgid, $persoonid, $type);

		//Door type op te vragen uit de db, ook al is het meegegeven in de aanroep,
		// kunnen we altijd een juiste set id's maken.
		$ids = array();
		foreach($rows as $row) {
			$ids[] = array($row['persid'], $row['orgid'], $row['type']);
		}

		return self::verzamel($ids);
	}

	/**
	 *  Zoek alle huidige bedrijfscontactcontactpersonen op 
	 **/
	static public function vakidioten()
	{
		global $WSW4DB;
		$rows = $WSW4DB->q('TABLE SELECT `persoon_contactID` as persid, `organisatie_contactID` as orgid'
						. ' FROM `ContactPersoon`'
						. ' WHERE 1'
						. ' AND ( `einddatum` IS NULL OR `einddatum` > NOW())'
						. ' AND ( `begindatum` IS NULL OR `begindatum` < NOW())'
						. ' AND `type` = "BEDRIJFSCONTACT"'
						. ' AND `persoon_contactID` IN ('
							.' SELECT `contactID`'
							.' FROM `Contact`'
							.' WHERE `vakidOpsturen` = 1'
							.')'
						);

		$ids = array();
		foreach($rows as $row) {
			$ids[] = array($row['persid'], $row['orgid'], "BEDRIJFSCONTACT");
		}

		return self::verzamel($ids);
	}

	public function personen()
	{
		return PersoonVerzameling::verzamel(explode(',',$this->implode('getPersoonContactID', ',')));
	}

	public function organisaties()
	{
		return OrganisatieVerzameling::verzamel(explode(',',$this->implode('getOrganisatieContactID', ',')));
	}

	/**
	 *  /Brief Geeft de gezamelijk url van een generiek spook of bedrijfscontact.
	 *
	 *	  Retourneert de url van een verzameling contactpersonen die eigenlijk maar een
	 *	  persoon voorstellen. Als er meerdere personen in de verzameling zitten geef
	 *	  dan niets terug.
	 **/
	public function singleSpookWebUrl()
	{
		if($this->aantal() == 0)
			return null;

		$persoonid = $this->first()->getPersoonContactID();
		foreach($this as $cp){
			if($cp->getPersoonContactID() != $persoonid)
				return null;
		}
		
		return SPOOKBASE . ucfirst(strtolower($this->first()->getType())) ."/". $persoonid;
	}

	/**
	 *  Sorteringen
	 **/

	static public function sorteerOpMijEerst($aID, $bID)
	{
		//Het kan dat het object nog niet opgeslagen is, dus sloop het contactid
		// uit het cpid ipv geef te gebruiken.
		$aID = explode("_",$aID);
		$bID = explode("_",$bID);
		return PersoonVerzameling::sorteerOpMijEerst($aID[0], $bID[0]);
	}

	static public function sorteerOpEindDatum($aID, $bID)
	{
		$a = ContactPersoon::geef($aID)->getEindDatum();
		$b = ContactPersoon::geef($bID)->getEindDatum();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}

	static public function sorteerOpBeginDatum($aID, $bID)
	{
		$a = ContactPersoon::geef($aID)->getBeginDatum();
		$b = ContactPersoon::geef($bID)->getBeginDatum();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
