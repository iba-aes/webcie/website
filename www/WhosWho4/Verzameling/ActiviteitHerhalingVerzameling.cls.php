<?
class ActiviteitHerhalingVerzameling
	extends ActiviteitHerhalingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ActiviteitHerhalingVerzameling_Generated
	}

	/**
	 *  Geeft alle herhalingschildrren van een activiteit terug
	 *
	 * @param act De Activiteit waarvan je de herhalingen wilt
	 *
	 * @return ActiviteitHerhalingVerzameling De ActiviteitHerhalingVerzameling
	 */
	static public function geefHerhalingsChildren(Activiteit $act)
	{
		return ActiviteitHerhalingQuery::table()
			->whereProp('parent', $act)
			->verzamel();
	}
}
