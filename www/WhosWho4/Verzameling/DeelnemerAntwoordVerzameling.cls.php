<?

/**
 * $Id$
 */
class DeelnemerAntwoordVerzameling
	extends DeelnemerAntwoordVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DeelnemerAntwoordVerzameling_Generated
	}

	/**
	 * @brief Geeft alle antwoorden van een deelnemer
	 * @param Deelnemer $dlnr
	 * @return DeelnemerVerzameling
	 */
	public static function vanDeelnemer(Deelnemer $dlnr)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `vraag_vraagID` FROM `DeelnemerAntwoord`
							WHERE `deelnemer_persoon_contactID` = %i AND
								`deelnemer_activiteit_activiteitID` = %i AND
								`vraag_activiteit_activiteitID` = %i',
								$dlnr->getPersoonContactID(),
								$dlnr->getActiviteitActiviteitID(),
								$dlnr->getActiviteitActiviteitID()); 


		$dlnrAntwoordIDs = array();
		foreach($ids as $id)
		{
			$dlnrAntwoordIDs[] = array($dlnr->getPersoonContactID(), $dlnr->getActiviteitActiviteitID(),
										$dlnr->getActiviteitActiviteitID(), $id);
		}

		return self::verzamel($dlnrAntwoordIDs);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
