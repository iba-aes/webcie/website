<?

/***
 * $Id$
 */
class MededelingVerzameling
	extends MededelingVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/**
		 Retourneert /alle/ mededelingen in de database
	**/
	static public function allemaal()
	{
		$query = MededelingQuery::table()
			->whereProp('doelgroep', 'ALLEN')
			->orderByDesc('datumBegin');

		$list = array("lid" => "LEDEN", "actief" => "ACTIEF");
		foreach($list as $level => $aud)
			if(hasAuth($level))
				$query->orWhereProp('doelgroep', $aud);

		return $query->verzamel();
	}

	/**
		 Retourneert een MededelingVerzameling van Mededeling objecten
		die nu relevant zijn voor de huidige gebruiker

		Het resultaat is dus afhankelijk van het 'level' van de gebruiker en de
		datum van vandaag!
	**/
	static public function huidige()
	{
		global $WSW4DB;

		// ENUM => level
		$list = array("lid" => "LEDEN", "actief" => "ACTIEF");

		$ret = "(`doelgroep` = 'ALLEN'";
		foreach($list as $level => $aud)
		{
			if(hasAuth($level))
				$ret .= " OR `doelgroep` = '$aud'";
		}
		$ret .= " ) AND"
				." ( `datumBegin` IS NULL OR `datumBegin` <= CURDATE()"
				." ) AND"
				." ( `datumEind` IS NULL OR `datumEind` >= CURDATE())";

		$ids = $WSW4DB->q('COLUMN SELECT `mededelingID`'
						. ' FROM `Mededeling`'
						. ' WHERE '.$ret
						. ' ORDER BY `prioriteit` DESC'
								.', `datumBegin` DESC'
						);

		return self::verzamel($ids);
	}

	static public function zoek ($arg, $limiet = 10)
	{
		global $WSW4DB;

		if (hasAuth('actief'))
		{
			$authwhere = '(1)';
		}
		else
		{
			if (hasAuth('ingelogd'))
			{
				$authwhere = '(`Mededeling`.`doelgroep` = \'ALLEN\'
					OR `Mededeling`.`doelgroep` = \'LEDEN\') ';
			}
			else
			{
				$authwhere = ' (`Mededeling`.`doelgroep` = \'ALLEN\') ';
			}
		}

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Mededeling`.`mededelingID`
			FROM `Mededeling`
			JOIN `Commissie` ON `Mededeling`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE ' . $authwhere . '
			AND (`Mededeling`.`omschrijving_NL` LIKE %c
			  OR `Mededeling`.`omschrijving_EN` LIKE %c
			  OR `Mededeling`.`mededeling_NL` LIKE %c
			  OR `Mededeling`.`mededeling_EN` LIKE %c
			  OR `Commissie`.`naam` LIKE %c)
			ORDER BY `Mededeling`.`datumBegin` DESC
			LIMIT %i',
			$arg, $arg, $arg, $arg, $arg, $limiet);

		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
