<?

/**
 * $Id$
 */
class ActiviteitVraagVerzameling
	extends ActiviteitVraagVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ActiviteitVraagVerzameling_Generated
	}

	public static function vanActiviteit($act)
	{
		return ActiviteitVraagQuery::table()
			->whereProp('Activiteit', $act)
			->verzamel();
	}

	public function geefVraagIDs()
	{
		$ids = array();

		foreach($this as $vraag){
			$ids[] = $vraag->getVraagID();
		}

		return $ids;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
