<?

/**
 * $Id$
 */
class DonateurVerzameling
	extends DonateurVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	public function toPersoonVerzameling()
	{
		$verz = parent::toPersoonVerzameling();
		$verz->union(parent::toAnderPersoonVerzameling());
		return $verz;
	}

	public static function allemaal()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID`'
						.' FROM `Donateur`'
						);
		return self::verzamel($ids);
	}
	public static function huidige()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID`'
						.' FROM `Donateur`'
						.' WHERE `jaarEind` IS NULL OR `jaarEind` > %i'
						, colJaar()
						);
		return self::verzamel($ids);
	}

	/**
	 * @brief Geef een lijst met alle huidige donateurs die iets moeten ontvangen.
	 *
	 * Deze lijst is uitgesplitst op soort (almanak, avnotulenemail, ...)
	 * en bevat verder een korte naam, menselijk leesbare naam en DonateurVerzameling.
	 *
	 * @return Een array `[$key => ['kort' => ..., 'naam' => ..., 'verzameling' => ...]]`.
	 */
	public static function ontvangerSoorten()
	{
		return
			[ 'almanak' =>
				[ 'kort'  => 'alm'
				, 'naam' => _('Almanak')
				, 'verzameling' => static::ontvangersAlmanak()
				]
			, 'avnotulenemail' =>
				[ 'kort'  => 'avE'
				, 'naam' => _('AV-notulen (per email)')
				, 'verzameling' => static::ontvangersAVNotulenEmail()
				]
			, 'avnotulenpost' =>
				[ 'kort'  => 'avP'
				, 'naam' => _('AV-notulen (per post)')
				, 'verzameling' => static::ontvangersAVNotulenPost()
				]
			, 'jaarverslag' =>
				[ 'kort'  => 'jaa'
				, 'naam' => _('Jaarverslag')
				, 'verzameling' => static::ontvangersJaarverslag()
				]
			, 'studiereis' =>
				[ 'kort'  => 'stu'
				, 'naam' => _('Studiereis')
				, 'verzameling' => static::ontvangersStudiereis()
				]
			, 'symposium' =>
				[ 'kort'  => 'sym'
				, 'naam' => _('Symposium')
				, 'verzameling' => static::ontvangersSymposium()
				]
			, 'vakidioot' =>
				[ 'kort'  => 'vak'
				, 'naam' => _('Vakidioot')
				, 'verzameling' => static::ontvangersVakidioot()
				]
			];
	}

	/**
	 * @brief Geef alle huidige donateurs die de almanak moeten ontvangen.
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersAlmanak()
	{
		return DonateurQuery::table()
			->whereProp('almanak', true)
			->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die de AV-notulen per e-mail moeten ontvangen.
	 *
	 * (Deze kunnen misschien ook AV-notulen per post willen hebben.)
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersAVNotulenEmail()
	{
		return DonateurQuery::table()
			->where(function($q) {
				$q->whereProp('avNotulen', 'EMAIL')
				->orWhereProp('avNotulen', 'POST_EMAIL');
			})->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die de AV-notulen per post moeten ontvangen.
	 *
	 * (Deze kunnen misschien ook AV-notulen per e-mail willen hebben.)
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersAVNotulenPost()
	{
		return DonateurQuery::table()
			->where(function($q) {
				$q->whereProp('avNotulen', 'POST')
				->orWhereProp('avNotulen', 'POST_EMAIL');
			})->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die jaarverslagen moeten ontvangen.
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersJaarverslag()
	{
		return DonateurQuery::table()
			->whereProp('jaarverslag', true)
			->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die studiereisverslagen moeten ontvangen.
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersStudiereis()
	{
		return DonateurQuery::table()
			->whereProp('studiereis', true)
			->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die symposiaverslagen(?) moeten ontvangen.
	 *
	 * TODO: vraag een oude lul wat dit veld precies betekent.
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersSymposium()
	{
		return DonateurQuery::table()
			->whereProp('symposium', true)
			->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/**
	 * @brief Geef alle huidige donateurs die Vakidioten moeten ontvangen.
	 *
	 * @return Een DonateurVerzameling.
	 */
	public static function ontvangersVakidioot()
	{
		return DonateurQuery::table()
			->whereProp('vakid', true)
			->where(DonateurQuery::whereHuidig())
			->verzamel();
	}

	/*** Sorteren ***/
	static public function sorteerOpNaam($aID, $bID)
	{
		return PersoonVerzameling::sorteerOpNaam($aID, $bID);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
