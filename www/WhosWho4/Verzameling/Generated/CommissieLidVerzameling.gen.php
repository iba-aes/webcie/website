<?
abstract class CommissieLidVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CommissieLidVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CommissieLidVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze CommissieLidVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze CommissieLidVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze CommissieLidVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CommissieLidVerzameling van Persoon.
	 *
	 * @return CommissieLidVerzameling
	 * Een CommissieLidVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new CommissieLidVerzameling();

		return CommissieLidQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een CommissieLidVerzameling van Commissie.
	 *
	 * @return CommissieLidVerzameling
	 * Een CommissieLidVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new CommissieLidVerzameling();

		return CommissieLidQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
