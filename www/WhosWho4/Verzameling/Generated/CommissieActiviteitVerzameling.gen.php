<?
abstract class CommissieActiviteitVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CommissieActiviteitVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CommissieActiviteitVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze CommissieActiviteitVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze CommissieActiviteitVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze CommissieActiviteitVerzameling.
	 */
	public function toActiviteitVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getActiviteitActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CommissieActiviteitVerzameling van Commissie.
	 *
	 * @return CommissieActiviteitVerzameling
	 * Een CommissieActiviteitVerzameling die elementen bevat die bij de Commissie
	 * hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new CommissieActiviteitVerzameling();

		return CommissieActiviteitQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
	/**
	 * @brief Maak een CommissieActiviteitVerzameling van Activiteit.
	 *
	 * @return CommissieActiviteitVerzameling
	 * Een CommissieActiviteitVerzameling die elementen bevat die bij de Activiteit
	 * hoort.
	 */
	static public function fromActiviteit($activiteit)
	{
		if(!isset($activiteit))
			return new CommissieActiviteitVerzameling();

		return CommissieActiviteitQuery::table()
			->whereProp('Activiteit', $activiteit)
			->verzamel();
	}
}
