<?
abstract class PersoonVoorkeurVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PersoonVoorkeurVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PersoonVoorkeurVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PersoonVoorkeurVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze PersoonVoorkeurVerzameling een BugCategorieVerzameling.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze PersoonVoorkeurVerzameling.
	 */
	public function toFavoBugCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new BugCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getFavoBugCategorieBugCategorieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getFavoBugCategorieBugCategorieID()
			                      );
		}
		$this->positie = $origPositie;
		return BugCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PersoonVoorkeurVerzameling van Persoon.
	 *
	 * @return PersoonVoorkeurVerzameling
	 * Een PersoonVoorkeurVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new PersoonVoorkeurVerzameling();

		return PersoonVoorkeurQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een PersoonVoorkeurVerzameling van FavoBugCategorie.
	 *
	 * @return PersoonVoorkeurVerzameling
	 * Een PersoonVoorkeurVerzameling die elementen bevat die bij de FavoBugCategorie
	 * hoort.
	 */
	static public function fromFavoBugCategorie($favoBugCategorie)
	{
		if(!isset($favoBugCategorie))
			return new PersoonVoorkeurVerzameling();

		return PersoonVoorkeurQuery::table()
			->whereProp('FavoBugCategorie', $favoBugCategorie)
			->verzamel();
	}
}
