<?
abstract class PersoonRatingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PersoonRatingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PersoonRatingVerzameling een RatingObjectVerzameling.
	 *
	 * @return RatingObjectVerzameling
	 * Een RatingObjectVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze PersoonRatingVerzameling.
	 */
	public function toRatingObjectVerzameling()
	{
		if($this->aantal() == 0)
			return new RatingObjectVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getRatingObjectRatingObjectID()
			                      );
		}
		$this->positie = $origPositie;
		return RatingObjectVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze PersoonRatingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PersoonRatingVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PersoonRatingVerzameling van RatingObject.
	 *
	 * @return PersoonRatingVerzameling
	 * Een PersoonRatingVerzameling die elementen bevat die bij de RatingObject hoort.
	 */
	static public function fromRatingObject($ratingObject)
	{
		if(!isset($ratingObject))
			return new PersoonRatingVerzameling();

		return PersoonRatingQuery::table()
			->whereProp('RatingObject', $ratingObject)
			->verzamel();
	}
	/**
	 * @brief Maak een PersoonRatingVerzameling van Persoon.
	 *
	 * @return PersoonRatingVerzameling
	 * Een PersoonRatingVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new PersoonRatingVerzameling();

		return PersoonRatingQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
