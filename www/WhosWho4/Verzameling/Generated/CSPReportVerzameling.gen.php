<?
abstract class CSPReportVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CSPReportVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CSPReportVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze CSPReportVerzameling.
	 */
	public function toIngelogdLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getIngelogdLidContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getIngelogdLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CSPReportVerzameling van IngelogdLid.
	 *
	 * @return CSPReportVerzameling
	 * Een CSPReportVerzameling die elementen bevat die bij de IngelogdLid hoort.
	 */
	static public function fromIngelogdLid($ingelogdLid)
	{
		if(!isset($ingelogdLid))
			return new CSPReportVerzameling();

		return CSPReportQuery::table()
			->whereProp('IngelogdLid', $ingelogdLid)
			->verzamel();
	}
}
