<?
abstract class TinyUrlHitVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TinyUrlHitVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TinyUrlHitVerzameling een TinyUrlVerzameling.
	 *
	 * @return TinyUrlVerzameling
	 * Een TinyUrlVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TinyUrlHitVerzameling.
	 */
	public function toTinyUrlVerzameling()
	{
		if($this->aantal() == 0)
			return new TinyUrlVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTinyUrlId()
			                      );
		}
		$this->positie = $origPositie;
		return TinyUrlVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TinyUrlHitVerzameling van TinyUrl.
	 *
	 * @return TinyUrlHitVerzameling
	 * Een TinyUrlHitVerzameling die elementen bevat die bij de TinyUrl hoort.
	 */
	static public function fromTinyUrl($tinyUrl)
	{
		if(!isset($tinyUrl))
			return new TinyUrlHitVerzameling();

		return TinyUrlHitQuery::table()
			->whereProp('TinyUrl', $tinyUrl)
			->verzamel();
	}
}
