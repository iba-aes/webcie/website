<?
abstract class LidStudieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de LidStudieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze LidStudieVerzameling een StudieVerzameling.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze LidStudieVerzameling.
	 */
	public function toStudieVerzameling()
	{
		if($this->aantal() == 0)
			return new StudieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getStudieStudieID()
			                      );
		}
		$this->positie = $origPositie;
		return StudieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze LidStudieVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze LidStudieVerzameling.
	 */
	public function toLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze LidStudieVerzameling een IntroGroepVerzameling.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze LidStudieVerzameling.
	 */
	public function toGroepVerzameling()
	{
		if($this->aantal() == 0)
			return new IntroGroepVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getGroepGroepID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getGroepGroepID()
			                      );
		}
		$this->positie = $origPositie;
		return IntroGroepVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een LidStudieVerzameling van Studie.
	 *
	 * @return LidStudieVerzameling
	 * Een LidStudieVerzameling die elementen bevat die bij de Studie hoort.
	 */
	static public function fromStudie($studie)
	{
		if(!isset($studie))
			return new LidStudieVerzameling();

		return LidStudieQuery::table()
			->whereProp('Studie', $studie)
			->verzamel();
	}
	/**
	 * @brief Maak een LidStudieVerzameling van Lid.
	 *
	 * @return LidStudieVerzameling
	 * Een LidStudieVerzameling die elementen bevat die bij de Lid hoort.
	 */
	static public function fromLid($lid)
	{
		if(!isset($lid))
			return new LidStudieVerzameling();

		return LidStudieQuery::table()
			->whereProp('Lid', $lid)
			->verzamel();
	}
	/**
	 * @brief Maak een LidStudieVerzameling van Groep.
	 *
	 * @return LidStudieVerzameling
	 * Een LidStudieVerzameling die elementen bevat die bij de Groep hoort.
	 */
	static public function fromGroep($groep)
	{
		if(!isset($groep))
			return new LidStudieVerzameling();

		return LidStudieQuery::table()
			->whereProp('Groep', $groep)
			->verzamel();
	}
}
