<?
abstract class TentamenUitwerkingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TentamenUitwerkingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TentamenUitwerkingVerzameling een TentamenVerzameling.
	 *
	 * @return TentamenVerzameling
	 * Een TentamenVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TentamenUitwerkingVerzameling.
	 */
	public function toTentamenVerzameling()
	{
		if($this->aantal() == 0)
			return new TentamenVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTentamenId()
			                      );
		}
		$this->positie = $origPositie;
		return TentamenVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TentamenUitwerkingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TentamenUitwerkingVerzameling.
	 */
	public function toUploaderVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getUploaderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TentamenUitwerkingVerzameling van Tentamen.
	 *
	 * @return TentamenUitwerkingVerzameling
	 * Een TentamenUitwerkingVerzameling die elementen bevat die bij de Tentamen hoort.
	 */
	static public function fromTentamen($tentamen)
	{
		if(!isset($tentamen))
			return new TentamenUitwerkingVerzameling();

		return TentamenUitwerkingQuery::table()
			->whereProp('Tentamen', $tentamen)
			->verzamel();
	}
	/**
	 * @brief Maak een TentamenUitwerkingVerzameling van Uploader.
	 *
	 * @return TentamenUitwerkingVerzameling
	 * Een TentamenUitwerkingVerzameling die elementen bevat die bij de Uploader hoort.
	 */
	static public function fromUploader($uploader)
	{
		if(!isset($uploader))
			return new TentamenUitwerkingVerzameling();

		return TentamenUitwerkingQuery::table()
			->whereProp('Uploader', $uploader)
			->verzamel();
	}
}
