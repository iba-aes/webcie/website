<?
abstract class TentamenVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TentamenVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TentamenVerzameling een VakVerzameling.
	 *
	 * @return VakVerzameling
	 * Een VakVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze TentamenVerzameling.
	 */
	public function toVakVerzameling()
	{
		if($this->aantal() == 0)
			return new VakVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVakVakID()
			                      );
		}
		$this->positie = $origPositie;
		return VakVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TentamenVerzameling van Vak.
	 *
	 * @return TentamenVerzameling
	 * Een TentamenVerzameling die elementen bevat die bij de Vak hoort.
	 */
	static public function fromVak($vak)
	{
		if(!isset($vak))
			return new TentamenVerzameling();

		return TentamenQuery::table()
			->whereProp('Vak', $vak)
			->verzamel();
	}
}
