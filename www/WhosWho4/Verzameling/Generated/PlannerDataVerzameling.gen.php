<?
abstract class PlannerDataVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PlannerDataVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PlannerDataVerzameling een PlannerVerzameling.
	 *
	 * @return PlannerVerzameling
	 * Een PlannerVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PlannerDataVerzameling.
	 */
	public function toPlannerVerzameling()
	{
		if($this->aantal() == 0)
			return new PlannerVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPlannerPlannerID()
			                      );
		}
		$this->positie = $origPositie;
		return PlannerVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PlannerDataVerzameling van Planner.
	 *
	 * @return PlannerDataVerzameling
	 * Een PlannerDataVerzameling die elementen bevat die bij de Planner hoort.
	 */
	static public function fromPlanner($planner)
	{
		if(!isset($planner))
			return new PlannerDataVerzameling();

		return PlannerDataQuery::table()
			->whereProp('Planner', $planner)
			->verzamel();
	}
}
