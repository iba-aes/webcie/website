<?
abstract class DocuBestandVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de DocuBestandVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze DocuBestandVerzameling een DocuCategorieVerzameling.
	 *
	 * @return DocuCategorieVerzameling
	 * Een DocuCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze DocuBestandVerzameling.
	 */
	public function toCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new DocuCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCategorieId()
			                      );
		}
		$this->positie = $origPositie;
		return DocuCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DocuBestandVerzameling van Categorie.
	 *
	 * @return DocuBestandVerzameling
	 * Een DocuBestandVerzameling die elementen bevat die bij de Categorie hoort.
	 */
	static public function fromCategorie($categorie)
	{
		if(!isset($categorie))
			return new DocuBestandVerzameling();

		return DocuBestandQuery::table()
			->whereProp('Categorie', $categorie)
			->verzamel();
	}
}
