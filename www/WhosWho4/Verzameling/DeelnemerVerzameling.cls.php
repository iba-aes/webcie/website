<?

/***
 * $Id$
 */
class DeelnemerVerzameling
	extends DeelnemerVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Schrijf een hoop deelnemers uit
	 */
	public function uitschrijven ()
	{
		user_error("Maak gebruik van de generated classes met een simpele foreach!", E_USER_DEPRECATED);

		global $WSW4DB;

		// prepareer de ids
		// todo generaliseer dit
		$ids = array();
		foreach ($this as $deelnemer)
			$ids[] = array($deelnemer->getPersoonContactID(), $deelnemer->getActiviteitActiviteitID());

		// todo is verwijderen uit de database wel gewenst?
		$WSW4DB->q('DELETE FROM `Deelnemer`'
				 . 'WHERE (`persoon_contactID`, `activiteit_activiteitID`)'
				 . '	IN (%A{ii})',
				 $ids);
	}

	/** METHODEN **/

	/**
	 * Maak een hoop deelnemers aan
	 */
	static public function aanmaken (PersoonVerzameling $leden, Activiteit $act)
	{
		$ids = array();
		foreach ($leden as $lid)
			$ids[] = array($lid->geefID(), $act->geefID());
		return self::verzamel($ids);
	}

	/**
	 *	 alle deelnemers van een activiteit
	 */
	static public function vanActiviteit(Activiteit $act)
	{
		global $WSW4DB;

		$actid = $act->getActiviteitID();

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID`'
						.' FROM `Deelnemer`'
						.' WHERE `activiteit_activiteitID` = %i'
						.' ORDER BY `momentInschrijven`'
						, $actid);

		foreach ($ids as &$id)
			$id = array($id, $actid);

		return self::verzamel($ids);
	}
	/**
	 *	 alle activiteiten waar persoon aan heeft/gaat deelnemen
	 */
	static public function vanPersoon(Persoon $persoon)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `activiteit_activiteitID`'
						.' FROM `Deelnemer`'
						.' WHERE `persoon_contactID` = %i'
						.' ORDER BY `momentInschrijven` DESC'
						, $persoon->geefID()
						);

		foreach($ids as &$id) {
			$id = array($persoon->geefID(), $id);
		}

		return self::verzamel($ids);
	}

	/*** Sorteren ***/
	/**
	 *	 Maak het mogelijk om een DeelnemerVerzameling te sorteren op de
	 *	datum van Activiteit.  Er wordt gesorteerd op eerst
	 *	Activiteit::getMomentBegin en als die gelijk zijn
	 *	Activiteit::getMomentEind
	 *
	 *	\note Vergeet niet eerst DeelnemerVerzameling::toActiviteitVerzameling
	 *	aan te roepen zodat alle Activiteit objecten alvast in de cache zitten.
	 *
	 *	@see Verzameling::sorteer()
	 */
	static public function sorteerOpActiviteitDatum($aID, $bID)
	{
		$a = Deelnemer::geef($aID)->getActiviteit()->getMomentBegin();
		$b = Deelnemer::geef($bID)->getActiviteit()->getMomentBegin();

		if($a == $b)
		{
			$a = Deelnemer::geef($aID)->getActiviteit()->getMomentEind();
			$b = Deelnemer::geef($bID)->getActiviteit()->getMomentEind();

			if($a == $b)
				return 0;
		}
		if($a < $b)
			return -1;

		return 1;
	}

	static public function sorteerOpNaam($aID, $bID)
	{
		$a = Deelnemer::geef($aID)->getPersoonContactID();
		$b = Deelnemer::geef($bID)->getPersoonContactID();
		return PersoonVerzameling::sorteerOpNaam($a, $b);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
