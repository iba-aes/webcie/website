<?
/**
 * $Id$
 */
class ContractonderdeelVerzameling
	extends ContractonderdeelVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContractonderdeelVerzameling_Generated
	}

	static public function metConstraints($contractid = null, $soort = null, $beginuitvoerdatum = null, $einduitvoerdatum = null, $verantwoordelijke = null )
	{
		if($soort == 'alle')
			$soort = null;

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `id`'
						. ' FROM `Contractonderdeel`'
						. ' WHERE 1'
						. ($contractid?' AND `contract_contractID` = %i':'%_')
						. ($soort?' AND `soort` = %s':'%_')
						. ($beginuitvoerdatum?' AND `uitvoerDatum` > %s':'%_')
						. ($einduitvoerdatum?' AND `uitvoerDatum` < %s':'%_')
						. ($verantwoordelijke?' AND `verantwoordelijke_contactID` = %s':'%_')
						, $contractid, $soort, $beginuitvoerdatum, $einduitvoerdatum, $verantwoordelijke);

		return self::verzamel($ids);
	}

/**
 *  Verstuur een mail naar de spocie met alle deadline informatie. Wordt gebruikt in CRON.
 **/
	public static function deadlineMail($dagen = 45)
	{
		//Zoek deadlines op
		$now = new DateTimeLocale();
		$end = new DateTimeLocale();

		//Er is geen constructor voor negatieve tijden :/
		// Maar doe -1 dag zodat ook alles van deze dag nog wel wordt meegenomen.
		$nowinterval = new DateInterval("P1D");
		$nowinterval->invert = 1;

		$now->add($nowinterval);
		$end->add(new DateInterval("P".$dagen."D"));

		$deadlines = ContractonderdeelVerzameling::metConstraints(null,null, $now->format("Y-m-d"), $end->format("Y-m-d"));
		$deadlines->sorteer("Datum");

		//Ga de text maken
		if($deadlines->aantal() == 0) {
			$inleiding = "Er zijn voor de komende $dagen dagen geen deadlines of uitvoerdata gevonden in Spookweb.
						\r\n";
		} else { 
			$inleiding = "De volgende deadlines of uitvoerdata zijn voor de komende $dagen dagen gevonden in Spookweb:\r\n";
		}

		$data = "";

		if($deadlines->aantal() != 0) {
			$data = "Datum\t\tWat\t\t\tWie\t\t\tUitgevoerd\tBedrijf\r\n\n";
			foreach($deadlines as $deadline)
			{
				$data .= ContractonderdeelView::waardeUitvoerdatum($deadline). "\t" .
					addTabs(ContractonderdeelView::waardeSoort($deadline), 24).
					addTabs(PersoonView::naam($deadline->getVerantwoordelijke()), 24).
					ContractonderdeelView::waardeUitgevoerd($deadline, false). "\t\t" .
					BedrijfView::waardeNaam($deadline->getContract()->getBedrijf()). "\n";
			}
			$data .= "\r\n";
		} 
		$text = 
<<< HEREDOC
Beste SpoCie,\r\n

$inleiding
$data
Groetjes,

Spookweb
(Dit is een automatische gegenereerd mailtje)
HEREDOC;

		//En stuur mailtje
		sendmail(WWWEMAIL, "SpoCie@A-Eskwadraat.nl, Alexander@A-Eskwadraat.nl", "Deadlinesoverzicht", $text);
	}


	/**
	 *  Sorteringen
	 **/

	function sorteerOpDatum($aID, $bID)
	{
		$a = Contractonderdeel::geef($aID)->getUitvoerDatum();
		$b = Contractonderdeel::geef($bID)->getUitvoerDatum();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
