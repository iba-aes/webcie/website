<?
abstract class BedrijfStudieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BedrijfStudieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BedrijfStudieVerzameling een StudieVerzameling.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BedrijfStudieVerzameling.
	 */
	public function toStudieVerzameling()
	{
		if($this->aantal() == 0)
			return new StudieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getStudieStudieID()
			                      );
		}
		$this->positie = $origPositie;
		return StudieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BedrijfStudieVerzameling een BedrijfVerzameling.
	 *
	 * @return BedrijfVerzameling
	 * Een BedrijfVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BedrijfStudieVerzameling.
	 */
	public function toBedrijfVerzameling()
	{
		if($this->aantal() == 0)
			return new BedrijfVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBedrijfContactID()
			                      );
		}
		$this->positie = $origPositie;
		return BedrijfVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BedrijfStudieVerzameling van Studie.
	 *
	 * @return BedrijfStudieVerzameling
	 * Een BedrijfStudieVerzameling die elementen bevat die bij de Studie hoort.
	 */
	static public function fromStudie($Studie)
	{
		if(!isset($Studie))
			return new BedrijfStudieVerzameling();

		return BedrijfStudieQuery::table()
			->whereProp('Studie', $Studie)
			->verzamel();
	}
	/**
	 * @brief Maak een BedrijfStudieVerzameling van Bedrijf.
	 *
	 * @return BedrijfStudieVerzameling
	 * Een BedrijfStudieVerzameling die elementen bevat die bij de Bedrijf hoort.
	 */
	static public function fromBedrijf($Bedrijf)
	{
		if(!isset($Bedrijf))
			return new BedrijfStudieVerzameling();

		return BedrijfStudieQuery::table()
			->whereProp('Bedrijf', $Bedrijf)
			->verzamel();
	}
}
