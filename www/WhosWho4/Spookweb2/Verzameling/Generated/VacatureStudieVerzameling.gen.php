<?
abstract class VacatureStudieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VacatureStudieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VacatureStudieVerzameling een StudieVerzameling.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VacatureStudieVerzameling.
	 */
	public function toStudieVerzameling()
	{
		if($this->aantal() == 0)
			return new StudieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getStudieStudieID()
			                      );
		}
		$this->positie = $origPositie;
		return StudieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze VacatureStudieVerzameling een VacatureVerzameling.
	 *
	 * @return VacatureVerzameling
	 * Een VacatureVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VacatureStudieVerzameling.
	 */
	public function toVacatureVerzameling()
	{
		if($this->aantal() == 0)
			return new VacatureVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVacatureId()
			                      );
		}
		$this->positie = $origPositie;
		return VacatureVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VacatureStudieVerzameling van Studie.
	 *
	 * @return VacatureStudieVerzameling
	 * Een VacatureStudieVerzameling die elementen bevat die bij de Studie hoort.
	 */
	static public function fromStudie($Studie)
	{
		if(!isset($Studie))
			return new VacatureStudieVerzameling();

		return VacatureStudieQuery::table()
			->whereProp('Studie', $Studie)
			->verzamel();
	}
	/**
	 * @brief Maak een VacatureStudieVerzameling van Vacature.
	 *
	 * @return VacatureStudieVerzameling
	 * Een VacatureStudieVerzameling die elementen bevat die bij de Vacature hoort.
	 */
	static public function fromVacature($Vacature)
	{
		if(!isset($Vacature))
			return new VacatureStudieVerzameling();

		return VacatureStudieQuery::table()
			->whereProp('Vacature', $Vacature)
			->verzamel();
	}
}
