<?
abstract class ContractonderdeelVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ContractonderdeelVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze ContractonderdeelVerzameling een ContractVerzameling.
	 *
	 * @return ContractVerzameling
	 * Een ContractVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContractonderdeelVerzameling.
	 */
	public function toContractVerzameling()
	{
		if($this->aantal() == 0)
			return new ContractVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContractContractID()
			                      );
		}
		$this->positie = $origPositie;
		return ContractVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze ContractonderdeelVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContractonderdeelVerzameling.
	 */
	public function toVerantwoordelijkeVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getVerantwoordelijkeContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getVerantwoordelijkeContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze ContractonderdeelVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContractonderdeelVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCommissieCommissieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ContractonderdeelVerzameling van Contract.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling die elementen bevat die bij de Contract hoort.
	 */
	static public function fromContract($contract)
	{
		if(!isset($contract))
			return new ContractonderdeelVerzameling();

		return ContractonderdeelQuery::table()
			->whereProp('Contract', $contract)
			->verzamel();
	}
	/**
	 * @brief Maak een ContractonderdeelVerzameling van Verantwoordelijke.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling die elementen bevat die bij de
	 * Verantwoordelijke hoort.
	 */
	static public function fromVerantwoordelijke($verantwoordelijke)
	{
		if(!isset($verantwoordelijke))
			return new ContractonderdeelVerzameling();

		return ContractonderdeelQuery::table()
			->whereProp('Verantwoordelijke', $verantwoordelijke)
			->verzamel();
	}
	/**
	 * @brief Maak een ContractonderdeelVerzameling van Commissie.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new ContractonderdeelVerzameling();

		return ContractonderdeelQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
