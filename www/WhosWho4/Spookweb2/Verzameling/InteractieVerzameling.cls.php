<?
/**
 * $Id$
 */
class InteractieVerzameling
	extends InteractieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // InteractieVerzameling_Generated
	}

	/**
	 *  Functie die met constraints op de db kan zoeken.
	 *  TODO: $vanaf afhandeling opschonen, huidige manier "werkt" maar is verre van netjes. Strings != "alle" laten
	 *   de boel bv onderuit gaan.
	 **/
	static public function metConstraints($vanaf = null, $medium = null, $spookid = null,
		 $bedrijfscontactid = null, $bedrijfsid = null, $commissieid = null, $ToDo = null)
	{
		//Voor 1990 zijn er geen interacties geweest.
		if(strtolower($vanaf) == "alle")
			$vanaf = new DateTimeLocale("1990-01-01");

		//Default is alles van de afgelopen 3 maanden
		if(!$vanaf){
			$vanaf = new DateTimeLocale();
			$vanaf->modify('-3 month');
		}

		//Is het een nette datetimelocale, maak het dan SQLbaar.
		//Is het een aantal, zorg dan dat we $vanaf gebruiken als limiet.
		if($vanaf instanceof DateTimeLocale || $vanaf instanceof DateTime)
		{
			$vanafstring = ' AND `datum` >= ' . $vanaf->format('Y-m-d');
		} elseif(is_int($vanaf))
		{
			$vanafstring = null;
		}else { 
			$vanafstring = ' AND `datum` >= ' .$vanaf;
		}

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `id`'
						. ' FROM `Interactie`'
						. ' WHERE 1'
						. ($vanafstring?' AND `datum` >= %s':' %_')
						. ($medium?' AND `medium` = %s':'%_')
						. ($spookid?' AND `spook_persoon_contactID` = %i':'%_')
						. ($bedrijfscontactid?' AND `bedrijfPersoon_persoon_contactID` = %i':'%_')
						. ($bedrijfsid?' AND `bedrijf_contactID` = %i':'%_')
						. ($commissieid?' AND `commissie_commissieID` = %i':'%_')
						. ($ToDo?' AND `todo` = 1':'')
						. ' ORDER BY `datum` DESC'
						. ($vanafstring?' %_':' LIMIT %i ')
						,$vanafstring, $medium, $spookid, $bedrijfscontactid, $bedrijfsid, $commissieid, $vanaf);

		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
