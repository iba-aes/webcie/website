<?
/**
 * $Id$
 */
class Bedrijf
	extends Bedrijf_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Bedrijf_Generated

		$this->contactID = NULL;
		$this->soort = 'BEDRIJF';
	}

	public static function vanProfiel(Bedrijfsprofiel $profiel)
	{
		return $profiel->getContractonderdeel()->getContract()->getBedrijf();
	}

	public static function vanVacature(Vacature $vacature)
	{
		return $vacature->getContractonderdeel()->getContract()->getBedrijf();
	}

	public function magVerwijderen ()
	{
		return hasAuth("bestuur");
	}

	/**
		 Retourneert de URL waarop dit bedrijf te vinden is

		@returns Een URL (string) waarop informatie over dit bedrijf te	vinden is
	**/
	public function url()
	{
		return SPOOKBASE . 'Bedrijf/' . $this->geefID();
	}

	/**
		 Retourneert de URL's van de vacaturen behorende bij dit bedrijf, wanneer deze bestaan.

		@returns Een (of meerdere) URL's (array of strings) naar de vacature(n) van dit bedrijf.
	**/
	public function VacaturenUrl()
	{
		$vacatures = VacatureVerzameling::metConstraints('huidig');
		$links = array(NULL);	

		foreach($vacatures as $vacature)
		{
			if($this == Bedrijf::vanVacature($vacature))
			{
				$links[]= new HtmlAnchor($vacature->url(), $vacature->getTitel());
				$links[]='<br>';
			}
		}
		
		return $links;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
