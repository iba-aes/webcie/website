<?
/**
 * $Id$
 */
abstract class VacatureVerzamelingView
	extends VacatureVerzamelingView_Generated
{

	public static function vacatureSelectieForm()
	{
		$types = array();
		//Maak de namen mooi
		foreach(Vacature::enumsType() as $type)
			$types[$type] = VacatureView::labelenumType($type);

		$types = array_merge(array('' => 'Alle'), $types);

		$form = new HtmlForm('get');
		$form->addClass('form-inline');
		$div = new HtmlDiv($formTable = new HtmlTable(), "vacatureselectform");
		$formTable->add(new HtmlTableRow(array(
				new HtmlTableDataCell(_('Vacaturetype:')),
				new HtmlTableDataCell( HtmlSelectbox::fromArray('type', $types, trypar('type'), 1))
			)))
			->add(new HtmlTableRow(array(
				new HtmlTableDataCell(_('Studies:')),
				new HtmlTableDataCell( HtmlInput::makeCheckbox("ica", trypar("ica")) . _('Informatica') . new HtmlBreak()
				. HtmlInput::makeCheckbox("iku", trypar("iku")) . _('Informatiekunde') . new HtmlBreak() 
				. HtmlInput::makeCheckbox("na", trypar("na")) . _('Natuurkunde') . new HtmlBreak()
				. HtmlInput::makeCheckbox("wis", trypar("wis")) . _('Wiskunde'))
			)));
		if(hasAuth('spocie'))
			$formTable->add(new HtmlTableRow(array(
				new HtmlTableDataCell(_('Spocie:')),
				new HtmlTableDataCell( HtmlInput::makeCheckbox("oud", trypar("oud")) . _("Laat ook oude vacatures zien"))
				)));

		$div->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_('Zoeken'))));

		$form->add($div);
		return $form;
	}

	public static function vacaturebanklijst(VacatureVerzameling $vacatures)
	{
		$div = new HtmlDiv();

		$table = new HtmlTable();
		$table->setAttribute('id','vacaturebank');

		$light = false;
		foreach($vacatures as $vacature)
		{
			if($light){
				$cssclass = 'light';
			} else {
				$cssclass = null;
			}
			$light = !$light;

			//We delegeren ditmaal niet naar de view van een los object omdat ze onderling 
			// verband houden en de code redelijk simpel is.
			$bedrijf = Bedrijf::vanVacature($vacature);
			if(hasAuth('spocie')){
				$verloopdatum = $vacature->getDatumVerloop()->format('Y-m-d');
				$views = VacatureView::waardeViews($vacature);
				$duur = VacatureView::waardePeriode($vacature);
				$table->add(new HtmlTableRow(array(
						$td = new HtmlTableDataCell("[".new HtmlAnchor($vacature->wijzigURL(),_("Wijzigen"))."]"
												. new HtmlBreak()
												. _("Deze vacature zal verlopen op $verloopdatum, heeft een totale duur van $duur en is $views keer bekeken.")
												)),$cssclass));
				$td->setAttribute('colspan',2);
			}

			//Note: voor het bekijken van een vacaturen linken we "lokaal": alleen met het ID.
			$namelink = new HtmlAnchor();
			$namelink->setAttribute('name', $bedrijf->getNaam());
			$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell(array(
						$namelink
						. new HtmlHeader(2, new HtmlAnchor($vacature->geefID(), $vacature->getTitel()))
						. new HtmlParagraph($vacature->getInleiding())
						. new HtmlAnchor($vacature->geefID(), _("Meer informatie")),
					new HtmlTableDataCell(array(
							new HtmlAnchor($vacature->url(),$img = new HtmlImage($bedrijf->getLogo(),$bedrijf->getNaam(),null,'bedrijfslogo')),
							new HtmlParagraph(VacatureView::waardeType($vacature) ." "._("voor")." ". VacatureView::waardeStudies($vacature),"vacaturestudies")  ),'bedrijfslogo')
						))),$cssclass));
			$img->setLazyLoad();
		}

		$div->add($table);

		return $div;
	}

	public static function administratielijst(VacatureVerzameling $vacatures)
	{
		if($vacatures->aantal() == 0)
			return new HtmlParagraph(_("Geen vacatures gevonden of weer te geven."));

		$vacatureDiv = new HtmlDiv();
		$vacatureTable = new HtmlTable();
		
		$vacatureTable->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("Verloopdatum")), new HtmlTableHeaderCell(_("Bedrijf")),
						new HtmlTableHeaderCell(_("Titel")), new HtmlTableHeaderCell(_("Views")),
						new HtmlTableHeaderCell(_("Plaatsingsdatum")), new HtmlTableHeaderCell(_("Duur")),
						new HtmlTableHeaderCell(_("Type")),
						new HtmlTableHeaderCell(_("Ica")), new HtmlTableHeaderCell(_("Iku")),
						new HtmlTableHeaderCell(_("Na")), new HtmlTableHeaderCell(_("Wis"))
						))));

		foreach($vacatures as $vacature){
			$bedrijf = Bedrijf::vanVacature($vacature);
			$row = new HtmlTableRow(array(
						new HtmlTableDataCell(VacatureView::waardeDatumVerloop($vacature)),
						new HtmlTableDataCell(new HtmlAnchor($bedrijf->url(),$bedrijf->getNaam())),
						new HtmlTableDataCell(VacatureView::waardeTitel($vacature)),
						new HtmlTableDataCell(VacatureView::waardeViews($vacature)),
						new HtmlTableDataCell(VacatureView::waardeDatumPlaatsing($vacature)),
						new HtmlTableDataCell(VacatureView::waardePeriode($vacature)),
						new HtmlTableDataCell(VacatureView::waardeType($vacature)),
						new HtmlTableDataCell(VacatureView::waardeIca($vacature)),
						new HtmlTableDataCell(VacatureView::waardeIku($vacature)),
						new HtmlTableDataCell(VacatureView::waardeNa($vacature)),
						new HtmlTableDataCell(VacatureView::waardeWis($vacature)),
						new HtmlTableDataCell(new HtmlAnchor($vacature->wijzigURL(),
							new HtmlSpan(_('Wijzigen'), 'pencil'))),
						));

			$vacatureTable->add($row);
		}

		return $vacatureDiv->add($vacatureTable);
		
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
