<?
/**
 * $Id$
 */
abstract class BedrijfStudieVerzamelingView
	extends BedrijfStudieVerzamelingView_Generated
{

	public static function htmllijst($bedrijfstudies)
	{
		if($bedrijfstudies->aantal() == 0)
			return new HtmlParagraph(_("Geen berijfstudies gevonden of weer te geven."));

		$page = new HtmlDiv();

		$table = StudieVerzamelingView::tabel($bedrijfstudies->toStudieVerzameling());

		$page->add($table);

		return $page;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
