<?
abstract class ContractonderdeelView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContractonderdeelView.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContractonderdeel(Contractonderdeel $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Contractonderdeel $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Contractonderdeel $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contract.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contract labelt.
	 */
	public static function labelContract(Contractonderdeel $obj)
	{
		return 'Contract';
	}
	/**
	 * @brief Geef de waarde van het veld contract.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contract van het object obj
	 * representeert.
	 */
	public static function waardeContract(Contractonderdeel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContract())
			return NULL;
		return ContractView::defaultWaardeContract($obj->getContract());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contract.
	 *
	 * @see genericFormcontract
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contract staat en kan
	 * worden bewerkt. Indien contract read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContract(Contractonderdeel $obj, $include_id = false)
	{
		return ContractView::defaultForm($obj->getContract());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contract. In
	 * tegenstelling tot formcontract moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcontract
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contract staat en kan
	 * worden bewerkt. Indien contract read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContract($name, $waarde=NULL)
	{
		return ContractView::genericDefaultForm('Contract');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contract bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contract representeert.
	 */
	public static function opmerkingContract()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Contractonderdeel $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(Contractonderdeel::enumsSoort() as $id)
			$soorten[$id] = ContractonderdeelView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Contractonderdeel $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', Contractonderdeel::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrag.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrag labelt.
	 */
	public static function labelBedrag(Contractonderdeel $obj)
	{
		return 'Bedrag';
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrag van het object obj
	 * representeert.
	 */
	public static function waardeBedrag(Contractonderdeel $obj)
	{
		return static::defaultWaardeInt($obj, 'Bedrag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrag.
	 *
	 * @see genericFormbedrag
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is betreft het een statisch html-element.
	 */
	public static function formBedrag(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Bedrag', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrag. In
	 * tegenstelling tot formbedrag moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbedrag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBedrag($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Bedrag');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bedrag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrag representeert.
	 */
	public static function opmerkingBedrag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uitvoerDatum.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitvoerDatum labelt.
	 */
	public static function labelUitvoerDatum(Contractonderdeel $obj)
	{
		return 'UitvoerDatum';
	}
	/**
	 * @brief Geef de waarde van het veld uitvoerDatum.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitvoerDatum van het object
	 * obj representeert.
	 */
	public static function waardeUitvoerDatum(Contractonderdeel $obj)
	{
		return static::defaultWaardeDate($obj, 'UitvoerDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uitvoerDatum.
	 *
	 * @see genericFormuitvoerDatum
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitvoerDatum staat en kan
	 * worden bewerkt. Indien uitvoerDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUitvoerDatum(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'UitvoerDatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uitvoerDatum. In
	 * tegenstelling tot formuitvoerDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuitvoerDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitvoerDatum staat en kan
	 * worden bewerkt. Indien uitvoerDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUitvoerDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'UitvoerDatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uitvoerDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitvoerDatum representeert.
	 */
	public static function opmerkingUitvoerDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Contractonderdeel $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Contractonderdeel $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uitgevoerd.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitgevoerd labelt.
	 */
	public static function labelUitgevoerd(Contractonderdeel $obj)
	{
		return 'Uitgevoerd';
	}
	/**
	 * @brief Geef de waarde van het veld uitgevoerd.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitgevoerd van het object obj
	 * representeert.
	 */
	public static function waardeUitgevoerd(Contractonderdeel $obj)
	{
		return static::defaultWaardeString($obj, 'Uitgevoerd');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uitgevoerd.
	 *
	 * @see genericFormuitgevoerd
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgevoerd staat en kan
	 * worden bewerkt. Indien uitgevoerd read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUitgevoerd(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Uitgevoerd', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uitgevoerd. In
	 * tegenstelling tot formuitgevoerd moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuitgevoerd
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgevoerd staat en kan
	 * worden bewerkt. Indien uitgevoerd read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUitgevoerd($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Uitgevoerd', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uitgevoerd bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitgevoerd representeert.
	 */
	public static function opmerkingUitgevoerd()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld verantwoordelijke.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld verantwoordelijke labelt.
	 */
	public static function labelVerantwoordelijke(Contractonderdeel $obj)
	{
		return 'Verantwoordelijke';
	}
	/**
	 * @brief Geef de waarde van het veld verantwoordelijke.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verantwoordelijke van het
	 * object obj representeert.
	 */
	public static function waardeVerantwoordelijke(Contractonderdeel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVerantwoordelijke())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getVerantwoordelijke());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld verantwoordelijke.
	 *
	 * @see genericFormverantwoordelijke
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verantwoordelijke staat en
	 * kan worden bewerkt. Indien verantwoordelijke read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formVerantwoordelijke(Contractonderdeel $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getVerantwoordelijke());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld verantwoordelijke. In
	 * tegenstelling tot formverantwoordelijke moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formverantwoordelijke
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verantwoordelijke staat en
	 * kan worden bewerkt. Indien verantwoordelijke read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormVerantwoordelijke($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Verantwoordelijke');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verantwoordelijke bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verantwoordelijke representeert.
	 */
	public static function opmerkingVerantwoordelijke()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld gefactureerd.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld gefactureerd labelt.
	 */
	public static function labelGefactureerd(Contractonderdeel $obj)
	{
		return 'Gefactureerd';
	}
	/**
	 * @brief Geef de waarde van het veld gefactureerd.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld gefactureerd van het object
	 * obj representeert.
	 */
	public static function waardeGefactureerd(Contractonderdeel $obj)
	{
		return static::defaultWaardeBool($obj, 'Gefactureerd');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld gefactureerd.
	 *
	 * @see genericFormgefactureerd
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gefactureerd staat en kan
	 * worden bewerkt. Indien gefactureerd read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGefactureerd(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Gefactureerd', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld gefactureerd. In
	 * tegenstelling tot formgefactureerd moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgefactureerd
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gefactureerd staat en kan
	 * worden bewerkt. Indien gefactureerd read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGefactureerd($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Gefactureerd');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * gefactureerd bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld gefactureerd representeert.
	 */
	public static function opmerkingGefactureerd()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(Contractonderdeel $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(Contractonderdeel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(Contractonderdeel $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld latexString.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld latexString labelt.
	 */
	public static function labelLatexString(Contractonderdeel $obj)
	{
		return 'LatexString';
	}
	/**
	 * @brief Geef de waarde van het veld latexString.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld latexString van het object
	 * obj representeert.
	 */
	public static function waardeLatexString(Contractonderdeel $obj)
	{
		return static::defaultWaardeText($obj, 'LatexString');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld latexString.
	 *
	 * @see genericFormlatexString
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld latexString staat en kan
	 * worden bewerkt. Indien latexString read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLatexString(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'LatexString', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld latexString. In
	 * tegenstelling tot formlatexString moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formlatexString
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld latexString staat en kan
	 * worden bewerkt. Indien latexString read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLatexString($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'LatexString', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * latexString bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld latexString representeert.
	 */
	public static function opmerkingLatexString()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld annulering.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld annulering labelt.
	 */
	public static function labelAnnulering(Contractonderdeel $obj)
	{
		return 'Annulering';
	}
	/**
	 * @brief Geef de waarde van het veld annulering.
	 *
	 * @param Contractonderdeel $obj Het Contractonderdeel-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld annulering van het object obj
	 * representeert.
	 */
	public static function waardeAnnulering(Contractonderdeel $obj)
	{
		return static::defaultWaardeBool($obj, 'Annulering');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld annulering.
	 *
	 * @see genericFormannulering
	 *
	 * @param Contractonderdeel $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld annulering staat en kan
	 * worden bewerkt. Indien annulering read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAnnulering(Contractonderdeel $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Annulering', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld annulering. In
	 * tegenstelling tot formannulering moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formannulering
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld annulering staat en kan
	 * worden bewerkt. Indien annulering read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAnnulering($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Annulering');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * annulering bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld annulering representeert.
	 */
	public static function opmerkingAnnulering()
	{
		return NULL;
	}
}
