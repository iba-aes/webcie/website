<?
abstract class BedrijfVerzamelingView_Generated
	extends OrganisatieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfVerzamelingView.
	 *
	 * @param BedrijfVerzameling $obj Het BedrijfVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijfVerzameling(BedrijfVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
