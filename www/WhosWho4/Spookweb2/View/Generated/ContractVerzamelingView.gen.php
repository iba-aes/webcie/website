<?
abstract class ContractVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContractVerzamelingView.
	 *
	 * @param ContractVerzameling $obj Het ContractVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContractVerzameling(ContractVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
