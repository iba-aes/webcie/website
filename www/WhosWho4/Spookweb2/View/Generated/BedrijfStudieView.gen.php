<?
abstract class BedrijfStudieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfStudieView.
	 *
	 * @param BedrijfStudie $obj Het BedrijfStudie-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijfStudie(BedrijfStudie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld Studie.
	 *
	 * @param BedrijfStudie $obj Het BedrijfStudie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld Studie labelt.
	 */
	public static function labelStudie(BedrijfStudie $obj)
	{
		return 'Studie';
	}
	/**
	 * @brief Geef de waarde van het veld Studie.
	 *
	 * @param BedrijfStudie $obj Het BedrijfStudie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Studie van het object obj
	 * representeert.
	 */
	public static function waardeStudie(BedrijfStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getStudie())
			return NULL;
		return StudieView::defaultWaardeStudie($obj->getStudie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld Studie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Studie representeert.
	 */
	public static function opmerkingStudie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld Bedrijf.
	 *
	 * @param BedrijfStudie $obj Het BedrijfStudie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld Bedrijf labelt.
	 */
	public static function labelBedrijf(BedrijfStudie $obj)
	{
		return 'Bedrijf';
	}
	/**
	 * @brief Geef de waarde van het veld Bedrijf.
	 *
	 * @param BedrijfStudie $obj Het BedrijfStudie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Bedrijf van het object obj
	 * representeert.
	 */
	public static function waardeBedrijf(BedrijfStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBedrijf())
			return NULL;
		return BedrijfView::defaultWaardeBedrijf($obj->getBedrijf());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld Bedrijf
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Bedrijf representeert.
	 */
	public static function opmerkingBedrijf()
	{
		return NULL;
	}
}
