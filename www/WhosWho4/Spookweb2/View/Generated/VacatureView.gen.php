<?
abstract class VacatureView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VacatureView.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVacature(Vacature $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Vacature $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Vacature $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contractonderdeel.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contractonderdeel labelt.
	 */
	public static function labelContractonderdeel(Vacature $obj)
	{
		return 'Contractonderdeel';
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contractonderdeel van het
	 * object obj representeert.
	 */
	public static function waardeContractonderdeel(Vacature $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContractonderdeel())
			return NULL;
		return ContractonderdeelView::defaultWaardeContractonderdeel($obj->getContractonderdeel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contractonderdeel.
	 *
	 * @see genericFormcontractonderdeel
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contractonderdeel staat en
	 * kan worden bewerkt. Indien contractonderdeel read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formContractonderdeel(Vacature $obj, $include_id = false)
	{
		return ContractonderdeelView::defaultForm($obj->getContractonderdeel());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contractonderdeel. In
	 * tegenstelling tot formcontractonderdeel moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formcontractonderdeel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contractonderdeel staat en
	 * kan worden bewerkt. Indien contractonderdeel read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormContractonderdeel($name, $waarde=NULL)
	{
		return ContractonderdeelView::genericDefaultForm('Contractonderdeel');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contractonderdeel bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contractonderdeel representeert.
	 */
	public static function opmerkingContractonderdeel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumVerloop.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumVerloop labelt.
	 */
	public static function labelDatumVerloop(Vacature $obj)
	{
		return 'DatumVerloop';
	}
	/**
	 * @brief Geef de waarde van het veld datumVerloop.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumVerloop van het object
	 * obj representeert.
	 */
	public static function waardeDatumVerloop(Vacature $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumVerloop');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumVerloop.
	 *
	 * @see genericFormdatumVerloop
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumVerloop staat en kan
	 * worden bewerkt. Indien datumVerloop read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumVerloop(Vacature $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumVerloop', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumVerloop. In
	 * tegenstelling tot formdatumVerloop moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumVerloop
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumVerloop staat en kan
	 * worden bewerkt. Indien datumVerloop read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumVerloop($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumVerloop');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumVerloop bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumVerloop representeert.
	 */
	public static function opmerkingDatumVerloop()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumPlaatsing.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumPlaatsing labelt.
	 */
	public static function labelDatumPlaatsing(Vacature $obj)
	{
		return 'DatumPlaatsing';
	}
	/**
	 * @brief Geef de waarde van het veld datumPlaatsing.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumPlaatsing van het object
	 * obj representeert.
	 */
	public static function waardeDatumPlaatsing(Vacature $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumPlaatsing');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumPlaatsing.
	 *
	 * @see genericFormdatumPlaatsing
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumPlaatsing staat en
	 * kan worden bewerkt. Indien datumPlaatsing read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumPlaatsing(Vacature $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumPlaatsing', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumPlaatsing. In
	 * tegenstelling tot formdatumPlaatsing moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumPlaatsing
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumPlaatsing staat en
	 * kan worden bewerkt. Indien datumPlaatsing read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumPlaatsing($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumPlaatsing');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumPlaatsing bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumPlaatsing representeert.
	 */
	public static function opmerkingDatumPlaatsing()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(Vacature $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld type.
	 *
	 * @param string $value Een enum-waarde van het veld type.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumType($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld type horen.
	 *
	 * @see labelenumType
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld type
	 * representeren.
	 */
	public static function labelenumTypeArray()
	{
		$soorten = array();
		foreach(Vacature::enumsType() as $id)
			$soorten[$id] = VacatureView::labelenumType($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(Vacature $obj)
	{
		return static::defaultWaardeEnum($obj, 'Type');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld type.
	 *
	 * @see genericFormtype
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is betreft het een statisch html-element.
	 */
	public static function formType(Vacature $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Type', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld type. In tegenstelling
	 * tot formtype moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtype
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormType($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Type', Vacature::enumstype());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ica.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ica labelt.
	 */
	public static function labelIca(Vacature $obj)
	{
		return 'Ica';
	}
	/**
	 * @brief Geef de waarde van het veld ica.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ica van het object obj
	 * representeert.
	 */
	public static function waardeIca(Vacature $obj)
	{
		return static::defaultWaardeBool($obj, 'Ica');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ica.
	 *
	 * @see genericFormica
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ica staat en kan worden
	 * bewerkt. Indien ica read-only is betreft het een statisch html-element.
	 */
	public static function formIca(Vacature $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Ica', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ica. In tegenstelling
	 * tot formica moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formica
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ica staat en kan worden
	 * bewerkt. Indien ica read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormIca($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Ica');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ica
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ica representeert.
	 */
	public static function opmerkingIca()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld iku.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld iku labelt.
	 */
	public static function labelIku(Vacature $obj)
	{
		return 'Iku';
	}
	/**
	 * @brief Geef de waarde van het veld iku.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld iku van het object obj
	 * representeert.
	 */
	public static function waardeIku(Vacature $obj)
	{
		return static::defaultWaardeBool($obj, 'Iku');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld iku.
	 *
	 * @see genericFormiku
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld iku staat en kan worden
	 * bewerkt. Indien iku read-only is betreft het een statisch html-element.
	 */
	public static function formIku(Vacature $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Iku', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld iku. In tegenstelling
	 * tot formiku moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formiku
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld iku staat en kan worden
	 * bewerkt. Indien iku read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormIku($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Iku');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld iku
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld iku representeert.
	 */
	public static function opmerkingIku()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld na.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld na labelt.
	 */
	public static function labelNa(Vacature $obj)
	{
		return 'Na';
	}
	/**
	 * @brief Geef de waarde van het veld na.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld na van het object obj
	 * representeert.
	 */
	public static function waardeNa(Vacature $obj)
	{
		return static::defaultWaardeBool($obj, 'Na');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld na.
	 *
	 * @see genericFormna
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld na staat en kan worden
	 * bewerkt. Indien na read-only is betreft het een statisch html-element.
	 */
	public static function formNa(Vacature $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Na', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld na. In tegenstelling
	 * tot formna moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formna
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld na staat en kan worden
	 * bewerkt. Indien na read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNa($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Na');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld na
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld na representeert.
	 */
	public static function opmerkingNa()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wis.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld wis labelt.
	 */
	public static function labelWis(Vacature $obj)
	{
		return 'Wis';
	}
	/**
	 * @brief Geef de waarde van het veld wis.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wis van het object obj
	 * representeert.
	 */
	public static function waardeWis(Vacature $obj)
	{
		return static::defaultWaardeBool($obj, 'Wis');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wis.
	 *
	 * @see genericFormwis
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wis staat en kan worden
	 * bewerkt. Indien wis read-only is betreft het een statisch html-element.
	 */
	public static function formWis(Vacature $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Wis', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wis. In tegenstelling
	 * tot formwis moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formwis
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wis staat en kan worden
	 * bewerkt. Indien wis read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormWis($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Wis');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wis
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wis representeert.
	 */
	public static function opmerkingWis()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(Vacature $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(Vacature $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(Vacature $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Titel', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Titel', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Titel', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Titel', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inleiding.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inleiding labelt.
	 */
	public static function labelInleiding(Vacature $obj)
	{
		return 'Inleiding';
	}
	/**
	 * @brief Geef de waarde van het veld inleiding.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inleiding van het object obj
	 * representeert.
	 */
	public static function waardeInleiding(Vacature $obj)
	{
		return static::defaultWaardeHtml($obj, 'Inleiding');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inleiding.
	 *
	 * @see genericForminleiding
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inleiding staat en kan
	 * worden bewerkt. Indien inleiding read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formInleiding(Vacature $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormHtml($obj, 'Inleiding', 'nl', $include_id),
			'en' => static::defaultFormHtml($obj, 'Inleiding', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inleiding. In
	 * tegenstelling tot forminleiding moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see forminleiding
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inleiding staat en kan
	 * worden bewerkt. Indien inleiding read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormInleiding($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormHtml($name, $waarde, 'Inleiding', 'nl'),
			'en' => static::genericDefaultFormHtml($name, $waarde, 'Inleiding', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * inleiding bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inleiding representeert.
	 */
	public static function opmerkingInleiding()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inhoud.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inhoud labelt.
	 */
	public static function labelInhoud(Vacature $obj)
	{
		return 'Inhoud';
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inhoud van het object obj
	 * representeert.
	 */
	public static function waardeInhoud(Vacature $obj)
	{
		return static::defaultWaardeHtml($obj, 'Inhoud');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inhoud.
	 *
	 * @see genericForminhoud
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is betreft het een statisch html-element.
	 */
	public static function formInhoud(Vacature $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormHtml($obj, 'Inhoud', 'nl', $include_id),
			'en' => static::defaultFormHtml($obj, 'Inhoud', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inhoud. In
	 * tegenstelling tot forminhoud moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see forminhoud
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormInhoud($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormHtml($name, $waarde, 'Inhoud', 'nl'),
			'en' => static::genericDefaultFormHtml($name, $waarde, 'Inhoud', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld inhoud
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inhoud representeert.
	 */
	public static function opmerkingInhoud()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld views.
	 *
	 * @param Vacature $obj Het Vacature-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld views labelt.
	 */
	public static function labelViews(Vacature $obj)
	{
		return 'Views';
	}
	/**
	 * @brief Geef de waarde van het veld views.
	 *
	 * @param Vacature $obj Het Vacature-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld views van het object obj
	 * representeert.
	 */
	public static function waardeViews(Vacature $obj)
	{
		return static::defaultWaardeInt($obj, 'Views');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld views.
	 *
	 * @see genericFormviews
	 *
	 * @param Vacature $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld views staat en kan worden
	 * bewerkt. Indien views read-only is betreft het een statisch html-element.
	 */
	public static function formViews(Vacature $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Views', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld views. In
	 * tegenstelling tot formviews moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formviews
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld views staat en kan worden
	 * bewerkt. Indien views read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormViews($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Views');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld views
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld views representeert.
	 */
	public static function opmerkingViews()
	{
		return NULL;
	}
}
