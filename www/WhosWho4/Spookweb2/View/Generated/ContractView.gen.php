<?
abstract class ContractView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContractView.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContract(Contract $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld contractID.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contractID labelt.
	 */
	public static function labelContractID(Contract $obj)
	{
		return 'ContractID';
	}
	/**
	 * @brief Geef de waarde van het veld contractID.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contractID van het object obj
	 * representeert.
	 */
	public static function waardeContractID(Contract $obj)
	{
		return static::defaultWaardeInt($obj, 'ContractID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contractID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contractID representeert.
	 */
	public static function opmerkingContractID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrijf.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrijf labelt.
	 */
	public static function labelBedrijf(Contract $obj)
	{
		return 'Bedrijf';
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrijf van het object obj
	 * representeert.
	 */
	public static function waardeBedrijf(Contract $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBedrijf())
			return NULL;
		return BedrijfView::defaultWaardeBedrijf($obj->getBedrijf());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrijf.
	 *
	 * @see genericFormbedrijf
	 *
	 * @param Contract $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijf staat en kan
	 * worden bewerkt. Indien bedrijf read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBedrijf(Contract $obj, $include_id = false)
	{
		return BedrijfView::defaultForm($obj->getBedrijf());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrijf. In
	 * tegenstelling tot formbedrijf moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbedrijf
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijf staat en kan
	 * worden bewerkt. Indien bedrijf read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBedrijf($name, $waarde=NULL)
	{
		return BedrijfView::genericDefaultForm('Bedrijf');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bedrijf
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrijf representeert.
	 */
	public static function opmerkingBedrijf()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld korting.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld korting labelt.
	 */
	public static function labelKorting(Contract $obj)
	{
		return 'Korting';
	}
	/**
	 * @brief Geef de waarde van het veld korting.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld korting van het object obj
	 * representeert.
	 */
	public static function waardeKorting(Contract $obj)
	{
		return static::defaultWaardeString($obj, 'Korting');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld korting.
	 *
	 * @see genericFormkorting
	 *
	 * @param Contract $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld korting staat en kan
	 * worden bewerkt. Indien korting read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKorting(Contract $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Korting', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld korting. In
	 * tegenstelling tot formkorting moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formkorting
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld korting staat en kan
	 * worden bewerkt. Indien korting read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKorting($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Korting', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld korting
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld korting representeert.
	 */
	public static function opmerkingKorting()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(Contract $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(Contract::enumsStatus() as $id)
			$soorten[$id] = ContractView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(Contract $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param Contract $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(Contract $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', Contract::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ingangsdatum.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ingangsdatum labelt.
	 */
	public static function labelIngangsdatum(Contract $obj)
	{
		return 'Ingangsdatum';
	}
	/**
	 * @brief Geef de waarde van het veld ingangsdatum.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ingangsdatum van het object
	 * obj representeert.
	 */
	public static function waardeIngangsdatum(Contract $obj)
	{
		return static::defaultWaardeDate($obj, 'Ingangsdatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ingangsdatum.
	 *
	 * @see genericFormingangsdatum
	 *
	 * @param Contract $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ingangsdatum staat en kan
	 * worden bewerkt. Indien ingangsdatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formIngangsdatum(Contract $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'Ingangsdatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ingangsdatum. In
	 * tegenstelling tot formingangsdatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formingangsdatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ingangsdatum staat en kan
	 * worden bewerkt. Indien ingangsdatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormIngangsdatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'Ingangsdatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * ingangsdatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ingangsdatum representeert.
	 */
	public static function opmerkingIngangsdatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param Contract $obj Het Contract-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(Contract $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param Contract $obj Het Contract-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(Contract $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param Contract $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(Contract $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
}
