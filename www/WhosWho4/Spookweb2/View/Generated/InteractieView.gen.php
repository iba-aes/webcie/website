<?
abstract class InteractieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in InteractieView.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeInteractie(Interactie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Interactie $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Interactie $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld spook.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld spook labelt.
	 */
	public static function labelSpook(Interactie $obj)
	{
		return 'Spook';
	}
	/**
	 * @brief Geef de waarde van het veld spook.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld spook van het object obj
	 * representeert.
	 */
	public static function waardeSpook(Interactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getSpook())
			return NULL;
		return ContactPersoonView::defaultWaardeContactPersoon($obj->getSpook());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld spook.
	 *
	 * @see genericFormspook
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spook staat en kan worden
	 * bewerkt. Indien spook read-only is betreft het een statisch html-element.
	 */
	public static function formSpook(Interactie $obj, $include_id = false)
	{
		return ContactPersoonView::defaultForm($obj->getSpook());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld spook. In
	 * tegenstelling tot formspook moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formspook
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spook staat en kan worden
	 * bewerkt. Indien spook read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSpook($name, $waarde=NULL)
	{
		return ContactPersoonView::genericDefaultForm('Spook');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld spook
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld spook representeert.
	 */
	public static function opmerkingSpook()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrijf.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrijf labelt.
	 */
	public static function labelBedrijf(Interactie $obj)
	{
		return 'Bedrijf';
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrijf van het object obj
	 * representeert.
	 */
	public static function waardeBedrijf(Interactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBedrijf())
			return NULL;
		return BedrijfView::defaultWaardeBedrijf($obj->getBedrijf());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrijf.
	 *
	 * @see genericFormbedrijf
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijf staat en kan
	 * worden bewerkt. Indien bedrijf read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBedrijf(Interactie $obj, $include_id = false)
	{
		return BedrijfView::defaultForm($obj->getBedrijf());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrijf. In
	 * tegenstelling tot formbedrijf moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbedrijf
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijf staat en kan
	 * worden bewerkt. Indien bedrijf read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBedrijf($name, $waarde=NULL)
	{
		return BedrijfView::genericDefaultForm('Bedrijf');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bedrijf
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrijf representeert.
	 */
	public static function opmerkingBedrijf()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrijfPersoon.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrijfPersoon labelt.
	 */
	public static function labelBedrijfPersoon(Interactie $obj)
	{
		return 'BedrijfPersoon';
	}
	/**
	 * @brief Geef de waarde van het veld bedrijfPersoon.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrijfPersoon van het object
	 * obj representeert.
	 */
	public static function waardeBedrijfPersoon(Interactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBedrijfPersoon())
			return NULL;
		return ContactPersoonView::defaultWaardeContactPersoon($obj->getBedrijfPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrijfPersoon.
	 *
	 * @see genericFormbedrijfPersoon
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijfPersoon staat en
	 * kan worden bewerkt. Indien bedrijfPersoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBedrijfPersoon(Interactie $obj, $include_id = false)
	{
		return ContactPersoonView::defaultForm($obj->getBedrijfPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrijfPersoon. In
	 * tegenstelling tot formbedrijfPersoon moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbedrijfPersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrijfPersoon staat en
	 * kan worden bewerkt. Indien bedrijfPersoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBedrijfPersoon($name, $waarde=NULL)
	{
		return ContactPersoonView::genericDefaultForm('BedrijfPersoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bedrijfPersoon bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrijfPersoon representeert.
	 */
	public static function opmerkingBedrijfPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(Interactie $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(Interactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(Interactie $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datum.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datum labelt.
	 */
	public static function labelDatum(Interactie $obj)
	{
		return 'Datum';
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datum van het object obj
	 * representeert.
	 */
	public static function waardeDatum(Interactie $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Datum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datum.
	 *
	 * @see genericFormdatum
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is betreft het een statisch html-element.
	 */
	public static function formDatum(Interactie $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Datum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datum. In
	 * tegenstelling tot formdatum moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formdatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Datum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld datum
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datum representeert.
	 */
	public static function opmerkingDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inhoud.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inhoud labelt.
	 */
	public static function labelInhoud(Interactie $obj)
	{
		return 'Inhoud';
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inhoud van het object obj
	 * representeert.
	 */
	public static function waardeInhoud(Interactie $obj)
	{
		return static::defaultWaardeHtml($obj, 'Inhoud');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inhoud.
	 *
	 * @see genericForminhoud
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is betreft het een statisch html-element.
	 */
	public static function formInhoud(Interactie $obj, $include_id = false)
	{
		return static::defaultFormHtml($obj, 'Inhoud', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inhoud. In
	 * tegenstelling tot forminhoud moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see forminhoud
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormInhoud($name, $waarde=NULL)
	{
		return static::genericDefaultFormHtml($name, $waarde, 'Inhoud', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld inhoud
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inhoud representeert.
	 */
	public static function opmerkingInhoud()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld medium.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld medium labelt.
	 */
	public static function labelMedium(Interactie $obj)
	{
		return 'Medium';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld medium.
	 *
	 * @param string $value Een enum-waarde van het veld medium.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumMedium($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld medium horen.
	 *
	 * @see labelenumMedium
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld medium
	 * representeren.
	 */
	public static function labelenumMediumArray()
	{
		$soorten = array();
		foreach(Interactie::enumsMedium() as $id)
			$soorten[$id] = InteractieView::labelenumMedium($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld medium.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld medium van het object obj
	 * representeert.
	 */
	public static function waardeMedium(Interactie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Medium');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld medium.
	 *
	 * @see genericFormmedium
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld medium staat en kan worden
	 * bewerkt. Indien medium read-only is betreft het een statisch html-element.
	 */
	public static function formMedium(Interactie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Medium', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld medium. In
	 * tegenstelling tot formmedium moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmedium
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld medium staat en kan worden
	 * bewerkt. Indien medium read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMedium($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Medium', Interactie::enumsmedium());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld medium
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld medium representeert.
	 */
	public static function opmerkingMedium()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld todo.
	 *
	 * @param Interactie $obj Het Interactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld todo labelt.
	 */
	public static function labelTodo(Interactie $obj)
	{
		return 'Todo';
	}
	/**
	 * @brief Geef de waarde van het veld todo.
	 *
	 * @param Interactie $obj Het Interactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld todo van het object obj
	 * representeert.
	 */
	public static function waardeTodo(Interactie $obj)
	{
		return static::defaultWaardeBool($obj, 'Todo');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld todo.
	 *
	 * @see genericFormtodo
	 *
	 * @param Interactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld todo staat en kan worden
	 * bewerkt. Indien todo read-only is betreft het een statisch html-element.
	 */
	public static function formTodo(Interactie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Todo', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld todo. In tegenstelling
	 * tot formtodo moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtodo
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld todo staat en kan worden
	 * bewerkt. Indien todo read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTodo($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Todo');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld todo
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld todo representeert.
	 */
	public static function opmerkingTodo()
	{
		return NULL;
	}
}
