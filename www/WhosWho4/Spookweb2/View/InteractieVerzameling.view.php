<?
/**
 * $Id$
 */
abstract class InteractieVerzamelingView
	extends InteractieVerzamelingView_Generated
{

	public static function htmlList(InteractieVerzameling $interacties, $bedrijfinfo = false)
	{
		if($interacties->aantal() == 0)
			return new HtmlParagraph(_("Geen interacties gevonden of weer te geven."));

		$page = new HtmlDiv();

		$table = new HtmlTable(null,null,'alternatingtable');
		$i = 1;
		foreach($interacties as $interactie)
		{
			$class = null;
			if($i%2)
				$class = 'light';
			$table->add(InteractieView::lijstEntry($interactie, $bedrijfinfo, $class));
			$i++;
		}
		$page->add($table);

		return $page;
	}

	public static function zoekForm()
	{
		//Haal de mooie array met cies voor optgroups op.
		$cies = spookwebSelectboxCiesArray();

		$mediumarray = array_merge(array( '' => 'Alle'), array_combine(Interactie::enumsMedium(), Interactie::enumsMedium()));
		foreach(Interactie::enumsMedium() as $value)
		{
			$mediumarray[$value] = ucfirst(InteractieView::labelenumMedium($value));
		}

		$zoekform = new HtmlForm('get');
		$zoektable = new HtmlTable();
		$zoektable->add(new HtmlTableRow(array(
					new HtmlTableDataCell("Via medium:"),
					new HtmlTableDataCell($mediumSelectbox = HtmlSelectbox::fromArray("Medium", $mediumarray, trypar('Medium'), 1)))))
			->add(new HtmlTableRow(array(
					new HtmlTableDataCell(_("Voor commissie:")),
					new HtmlTableDataCell($cieSelectbox = HtmlSelectbox::fromArray("Commissie", $cies, trypar('Commissie'), 1)))));
		$zoekform->add($zoektable)
			->add(HtmlInput::makeSubmitButton("Zoeken","interactiezoek"));

		$mediumSelectbox->addClass("zoekSelectboxSmall");
		$cieSelectbox->addClass("zoekSelectboxSmall");

		return $zoekform;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
