<?
/**
 * $Id$
 */
abstract class ContractonderdeelVerzamelingView
	extends ContractonderdeelVerzamelingView_Generated
{

	public static function htmlLijst(ContractonderdeelVerzameling $onderdelen)
	{
		$page = new HtmlDiv();

		if($onderdelen->aantal() > 0)
		{
			$wijzigen = $onderdelen->current()->magWijzigen();

			$table = new HtmlTable(null, 'sortable');
			$table->add(new HtmlTableHead(new HtmlTableRow(array(
							new HtmlTableHeaderCell(_("Soort")), new HtmlTableHeaderCell(_("Bedrag")),
							new HtmlTableHeaderCell(_("Deadline of <br> uitvoerdatum"), "lefttext"), new HtmlTableHeaderCell(_("Uitgevoerd")),
							new HtmlTableHeaderCell(_("Gefactureerd")),
							new HtmlTableHeaderCell(_("Verantwoordelijke")), new HtmlTableHeaderCell(_("Omschrijving")),
							))));

			foreach($onderdelen as $onderdeel){
				$row = new HtmlTableRow(array(
						new HtmlTableDataCell(ContractonderdeelView::waardeSoort($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeBedrag($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeUitvoerdatum($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeUitgevoerd($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeGefactureerd($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeVerantwoordelijke($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeOmschrijving($onderdeel)),
						($wijzigen ? new HtmlTableDataCell(new HtmlAnchor($onderdeel->wijzigURL(),
							HtmlSpan::fa('pencil', _('Wijzigen')))) : null)
						));
				$table->add($row);
			}

			$page->add($table);
		} else {
			$page->add(new HtmlParagraph(_('Er zijn nog geen contractonderdelen aan dit contract gekoppeld!')));
		}

		return $page;
	}

	public static function deadlines(ContractonderdeelVerzameling $onderdelen, $persoonlijk = false)
	{
		$page = new HtmlDiv();

		$table = new HtmlTable(null, 'sortable');
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("Datum")), new HtmlTableHeaderCell(_("Soort")),
						new HtmlTableHeaderCell(_("Uitgevoerd")), ($persoonlijk ?null:new HtmlTableHeaderCell(_("Verantwoordelijke"))),
						))));

		foreach($onderdelen as $onderdeel){
			$wijzigen = $onderdelen->current()->magWijzigen();

			$row = new HtmlTableRow(array(
					new HtmlTableDataCell(ContractonderdeelView::waardeUitvoerdatum($onderdeel)),
					new HtmlTableDataCell(ContractonderdeelView::waardeSoort($onderdeel)),
					new HtmlTableDataCell(ContractonderdeelView::waardeUitgevoerd($onderdeel)),
					($persoonlijk ?null:new HtmlTableDataCell(ContractonderdeelView::waardeVerantwoordelijke($onderdeel))),
					($wijzigen ? new HtmlTableDataCell(new HtmlAnchor($onderdeel->wijzigURL(),
						HtmlSpan::fa('pencil', _('Wijzigen')))) : null)

			));
			$table->add($row);
		}

		$page->add($table);
		return $page;
	}

	public static function deadlinePeriodeForm()
	{
		$table = new HtmlTable();

		$periodeArray = array( "30d" => _("Komende 30 dagen"),
						"3m" => _("Komende 3 maanden"),
						"1j" => _("Komend jaar")
					);

		$form = new HtmlForm('get');
		$form->add($periode = HtmlSelectbox::fromArray("Periode", $periodeArray, trypar('Periode'), 1) );
		$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell("Selectie: "), 
					new HtmlTableDataCell($form)
				)));

		$periode->setAutoSubmit(true);
		$periode->addClass("zoekSelectboxSmall");

		return $table;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
