<?
class BedrijfQuery
	extends BedrijfQuery_Generated
{
	public static function metConstraints($naam = null, $type = null, $status = null)
	{
		$q = BedrijfQuery::table();
		if ($naam)
			$q->whereLikeProp('naam', '%' . str_replace('%', '[%]', $naam) . '%');
		if ($status && in_array($status, Bedrijf::enumsStatus()))
			$q->whereProp('status', $status);
		if ($type && in_array($type, Bedrijf::enumsType()))
			$q->whereProp('type', $type);
		return $q;
	}

}
