<?
abstract class RegisterVerzamelingView
	extends RegisterVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in RegisterVerzamelingView.
	 *
	 * @param obj Het RegisterVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeRegisterVerzameling(RegisterVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
