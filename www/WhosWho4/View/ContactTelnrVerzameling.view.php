<?

/**
 * $Id$
 */
abstract class ContactTelnrVerzamelingView
	extends ContactTelnrVerzamelingView_Generated
{
	/**
	 * @brief geeft een HtmlList met daarin alle telefoonnummers van de verzameling
	 * @param telnrs verzameling met telefoonnummers (en hun soort)
	 *
	 * Een element van de lijst wordt getoond als "SOORT: NUMMER"
	 */
	public static function lijst (ContactTelnrVerzameling $telnrs)
	{
		$ul = new HtmlList();
		foreach ($telnrs as $telnr)
			$ul->add(new HtmlListItem(array(new HtmlSpan(ContactTelnrView::waardeSoort($telnr) . ': ', 'strongtext'),
						ContactTelnrView::waardeTelefoonnummer($telnr))));
		return $ul;
	}

	/**
	 * @brief geeft een witregel-gescheiden lijst met telefoonnumers
	 * @param telnrs verzameling met telefoonnummers
	 * @see ContactTelnrView::regel
	 */
	public static function regels(ContactTelnrVerzameling $telnrs)
	{
		$regels = '';
		foreach($telnrs as $telnr)
		{
			$regels .= ContactTelnrView::regel($telnr) . "<br>";
		}
		return $regels;
	}

	/**
	 * @brief Geeft de div die een wijzig-form is voor telefoonnummers van een contact
	 * @param obj een contact/lid/persoon
	 * Dit formulier kan verwerkt worden door processWijzigTelnr
	 */
	public static function wijzigTelnrsDiv($obj, $show_error, $default = "")
	{
		/** Pas ook telefoonnummers aan **/
		$div = new HtmlDiv();

		$div->add(new HtmlHeader(4, _("Telefoonnummers")));
		if ($show_error && !$obj->getTelefoonNummers()->allValid())
			$div->add(new HtmlParagraph(_("Er waren fouten in de ingevoerde telefoonnummers."), 'text-danger strongtext'));

		$jsonArray = array();

		$voorkeur = NULL;
		foreach($obj->getTelefoonNummers() as $telnr) {
			if ($telnr->getVoorkeur())
				$voorkeur = $telnr;
			$jsonArray[] = array(
				'id' => $telnr->getTelnrID(),
				'telnr' => $telnr->getTelefoonnummer(),
				'soort' => $telnr->getSoort()
			);
		}

		$JS = "var telnrsJSON = " . json_encode($jsonArray) . ";\n";
		$JS .= "var telnrsSoortenJSON = " .  json_encode(ContactTelnrView::labelenumSoortArray()) . ";\n";
		$JS .= "var telnrSoortDefaultJSON = " . json_encode($default) . ";\n";
		if (!is_null($voorkeur)) {
			$JS .= "var telnrVoorkeurJSON = " . json_encode($voorkeur->getTelnrID()) . ";\n";
		}
		$div->add($script = HtmlScript::makeJavascript($JS));
		$div->add($table = new HtmlTable());

		// Maak de header:
		$thead = $table->addHead();
		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(
			_("Voorkeur"), _("Categorie"), _("Nummer"), _("Verwijderen")
		));

		$type = get_class($obj) . "[Telefoonnr]";

		// Maak de body:
		$tbody = $table->addBody();
		$tbody->setAttribute('data-bind', 'foreach: telnrs');

		$row = $tbody->addRow();
		$row->addData($voorkeur = HtmlInput::makeRadio($type . "[pref]"));
		$voorkeur->setAttribute('data-bind', 'checked: $root.voorkeur, checkedValue: id, value: id');

		$row->addData($selectbox = new HtmlSelectbox(''));
		$selectbox->setAttribute('data-bind',
				'attr: { name: "' . $type . '"+"["+id+"][Soort]" }, options: $root.telnrsSoorten, value: soort, optionsValue: \'waarde\', optionsText: \'label\', selectPicker: {}');
		$selectbox->setNoDefaultClasses();
		$selectbox->addClass('form-control');

		$row->addData($telnr = new HtmlInput(''));
		// Dit zorgt ervoor dat cijfers getoond worden bij een mobiel:
		$telnr->setType('tel');
		$telnr->setAttribute('data-bind',
				'attr: { name: "' . $type . '"+"["+id+"][Telefoonnummer]" }, value: telnr');

		$row->addData($verwijder = new HtmlButton('button', HtmlSpan::fa('trash', 'Weg ermee')));
		$verwijder->setAttribute('data-bind', 'click: $root.verwijder');

		$div->add($nieuw = new HtmlButton('button', 'Nieuw telefoonnummer!'));
		$nieuw->setAttribute('data-bind', 'click: $root.nieuw');
		return $div;
	}

	/**
	 * @brief Verwerkt de data van een contact-achtig object met zijn lijst van telefoonnummers
	 * @param obj het object waarop de telefoonnummers worden gewijzigd
	 * @param data de opgestuurde data van het form wat opgevangen is
	 */
	public static function processWijzigTelnrs($obj, $data)
	{
		// Huidige telefoonnummers:
		$nrs = $obj->getTelefoonNummers();
		// Telefoonnummers ingevuld in het formulier:
		if (array_key_exists('Telefoonnr', $data)) {
			$telnrs = $data['Telefoonnr'];
		}
		else {
			$telnrs=array();
		}

		$voorkeur = NULL;
		if (array_key_exists('pref', $telnrs)) {
			$voorkeur = $telnrs['pref'];
		}

		// De bestaande nummers veranderen
		// Hou bij welke nummers je gaat verwijderen. Verwijderen binnen foreach loop geeft rare bugs.
		$verwijdernummers = array();
		foreach ($nrs as $nummer) {
			if (!array_key_exists($nummer->geefID(), $telnrs)) {
				// Het nummer wordt uit de databaas gegooid
				$verwijdernummers[] = $nummer;
				var_dump("Verwijder", $nummer->geefID());

				$melding = 'Verwijderd telnr ' . ContactTelnrView::waardeSoort($nummer) . '  #' . $nummer->geefID();
				$obj->wijzigingen[$melding] = array('nummer:' => array($nummer->getTelefoonnummer(), ''));
			} else {
				$telnr = $telnrs[$nummer->geefID()];
				if ($nummer != $telnr['Telefoonnummer']) {
					// Geef een wijziging van het nummer door
					$melding = 'Telnr ' . ContactTelnrView::waardeSoort($nummer) . '  #' . $nummer->geefID();
					$obj->wijzigingen[$melding] = array('nummer:' => array($nummer->getTelefoonnummer(), $telnr['Telefoonnummer']));
				}
				$nummer->setTelefoonnummer($telnr['Telefoonnummer'])
					   ->setSoort($telnr['Soort'])
					   ->setVoorkeur(($voorkeur == $nummer->geefID()) ? true : false);
			}
		}

		// Bepaal wat de bestaande nummers zijn voordat we dingen gaan
		// verwijderen want we kijken of een ID niet in de lijst voorkomt of
		// het nieuw is
		$bestaandeNrs = array();
		foreach ($nrs as $nummer) $bestaandeNrs[$nummer->geefID()] = true;

		// De nummers die verwijderd hadden moeten worden, verwijderen.
		foreach ($verwijdernummers as $verwijder) {
			$nrs->verwijder($verwijder);
		}

		foreach ($telnrs as $naam => $telnr) {
			if (!is_array($telnr) || array_key_exists($naam, $bestaandeNrs)) continue;
			// Dit nummer is nieuw

			$nieuwTelnr = new ContactTelnr($obj);
			$nieuwTelnr->setTelefoonnummer($telnr['Telefoonnummer'])
				->setSoort($telnr['Soort'])
				->setVoorkeur($voorkeur == $naam);

			// Voeg toe aan de verzameling
			$nrs->voegtoe($nieuwTelnr);
			$melding = 'Nieuw telnr ' . ContactTelnrView::waardeSoort($nieuwTelnr);
			$obj->wijzigingen[$melding] = array('nummer:' => array('', $nieuwTelnr->getTelefoonnummer()));
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
