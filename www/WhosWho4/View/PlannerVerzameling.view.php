<?

/**
 * $Id$
 */
abstract class PlannerVerzamelingView
	extends PlannerVerzamelingView_Generated
{
	/**
	 * Toon een uitebgreid overzicht van planners
	 */
	static public function toonOverzicht (PlannerVerzameling $planners)
	{
		$output = new HtmlDiv();

		if ($planners->aantal() == 0)
			return $output->add(new HtmlSpan(_("Er zijn geen open planners gevonden.")));

		$output->add($table = new HtmlTable(null, 'sortable'));
		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());
		$thead->add($row = new HtmlTableRow());
		$row->makeHeaderFromArray(array(_("Titel"), _("Auteur"), _("Ingevuld"), _("Eerste datum"), _("Laatste datum")));

		foreach ($planners as $planner)
		{
			$tbody->add(PlannerView::overzichtTableRow($planner));
		}

		return $output;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
