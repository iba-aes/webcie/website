<?

/**
 * $Id$
 */
abstract class LidStudieVerzamelingView
	extends LidStudieVerzamelingView_Generated
{
	static public function simpel (LidStudieVerzameling $verzameling)
	{
		return StudieVerzamelingView::toString($verzameling->toStudieVerzameling());
	}

	static public function kort (LidStudieVerzameling $verzameling)
	{
		if (!$verzameling->aantal())
			return new HtmlEmphasis(_('Deze persoon heeft geen studies.'));
		$studies = new HtmlList();
		foreach ($verzameling as $studie)
			$studies->add(new HtmlListItem(new HtmlSpan(LidStudieView::kort($studie))));
		return $studies;
	}

	static public function wijzigStudies (LidStudieVerzameling $studies, $show_error) {
		$persoon = $studies->last()->getLid();

		if($persoon instanceof Persoon) {
			$naam = PersoonView::naam($persoon);
		} else {
			$naam = sprintf(" contact %s", $persoon->geefID());
		}

		$page = Page::getInstance()->start(sprintf("Studies wijzigen van %s", $naam));

		$page->add(LidStudieVerzamelingView::wijzigStudieForm('Wijzig', $studies, $show_error));

		$page->end();
	}

	/**
	 * Geef een formulier voor het wijzigen van studies terug
	 */
	static public function wijzigStudieForm ($name = 'Wijzig', LidStudieVerzameling $studies, $show_error = true)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$form->add(self::wijzigStudiesDiv($studies, $show_error));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));

		return $form;
	}

	/*
	 * Geeft een div met alle velden voor het wijzigen van studies
	 */
	public static function wijzigStudiesDiv(LidStudieVerzameling $studies, $show_error)
	{
		$div = new HtmlDiv();

		$div->add($idi = new HtmlDiv());

		$first = true;
		foreach ($studies as $studie)
		{
			$idi->add($iidiv = new HtmlDiv());

			if($studie == $studies->last())
				$iidiv->add(self::makeFormBoolRow('NieuwStudie', tryPar('NieuwStudie')
					, _('Nieuwe studie toevoegen')));

			$iidiv->add(LidStudieView::wijzigTR($studie, 'Studie', $show_error, true))
				  ->add(LidStudieView::wijzigTR($studie, 'DatumBegin', $show_error, true))
				  ->add(LidStudieView::wijzigTR($studie, 'DatumEind', $show_error, true))
				  ->add(LidStudieView::wijzigTR($studie, 'Status', $show_error, true));

			if($studie != $studies->last())
			{
				$iidiv->add(self::makeFormBoolRow('LidStudie['.$studie->geefID().'][verwijder]'
					, tryPar('LidStudie['.$studie->geefID().'][verwijder]')
					, _('Verwijder deze studie')));

				$iidiv->add(new HtmlHR());
			}
			$first = false;
		}

		return $div;

	}

	public static function processWijzigStudie(LidStudieVerzameling $studies)
	{
		foreach($studies as $studie) {
			LidStudieView::processForm($studie, 'viewWijzig', null, true);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
