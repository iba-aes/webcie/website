<?

/**
 * $Id$
 */
abstract class CommissieLidView
	extends CommissieLidView_Generated
{
	static public function infoPagina($cielid)
	{
		$page = static::pageLinks($cielid);
		$page->start();

		$cieliddiv = new HtmlDiv();

		$cieliddiv->add(self::viewInfo($cielid, true));

		$cieliddiv->add(HtmlAnchor::button($cielid->getCommissie()->systeemUrl() . "/Info", _("Ga terug naar de commissie")));

		$page->add($cieliddiv);
		return $page;
	}

	static public function persoonNaam($cielid){
		return PersoonView::naam($cielid->getPersoon());
	}

	static public function htmlLijstRegel($cielid)
	{
		return self::persoonNaam($cielid);
	}

	static public function htmlLijstRegelKlikbaar($cielid)
	{
		return new HtmlAnchor( "/leden/".$cielid->getPersoonContactID(), self::persoonNaam($cielid));
	}

	static public function htmlLijstRegelKlikbaarBonus($cielid)
	{
		return new HtmlAnchor( "/leden/".$cielid->getPersoonContactID() , self::persoonNaam($cielid))
				. " ("
				. new HtmlAnchor("Leden/".$cielid->getPersoonContactID(), _("info"))
				. ($cielid->magWijzigen()?"," . new HtmlAnchor("Leden/".$cielid->getPersoonContactID()."/Wijzig",_("wijzig")):"")
				. (hasAuth('god')?"," . new HtmlAnchor("Leden/".$cielid->getPersoonContactID()."/Verwijder",_("verwijder")):"")
				. ")";
	}

	static public function wijzigForm(CommissieLid $cielid, $isNew = false, $show_error = false)
	{
		$page = static::pageLinks($cielid);

		if(!$isNew)
			$page->start(_('Gegevens van ' . PersoonView::naam(Persoon::geef($cielid->getPersoonContactID())) 
				. ' voor ' . $cielid->getCommissie()->getNaam() . ' aanpassen'));
		else
			$page->start(_('Nieuw commissielid'));

		$page->add($div = new HtmlDiv(null, 'btn-group'));
		$div->add(HtmlAnchor::button(
			$cielid->getCommissie()->systeemUrl() . '/info'
			, _('Terug naar de commissie')));

		if(!$isNew)
		{
			$div->add(HtmlAnchor::button(
				$cielid->getCommissie()->systeemUrl() . '/Leden/' . $cielid->getPersoonContactID()
				, _('Terug naar het commissielid')));
		}

		$page->add(new HtmlHR());

		if($isNew) {
			$form = HtmlForm::named('CieLidNieuw');
			$form->add(self::createForm($cielid, $show_error, 'nieuwLid')
				 , HtmlInput::makeFormSubmitButton(_('Aanmaken')));
		} else {
			$form = HtmlForm::named('CieLidWijzig');
			$form->add(self::createForm($cielid, $show_error, 'viewWijzig')
				 , HtmlInput::makeFormSubmitButton(_('Wijzig')));
		}

		$page->add($form);

		$page->end();
	}

	static public function systeemFix(CommissieLid $cielid, $uid, $inCie, $emailInCie)
	{
		$page = static::pageLinks($cielid);
		$page->start(_('SysteemFix'));
		$page->add($ul = new HtmlList());
		$ul->addChild(sprintf('%s: %s', _('Commissie'), CommissieView::makeLink($cielid->getCommissie())));;
		$ul->addChild(sprintf('%s: %s', _('Lid'), PersoonView::makeLink($cielid->getPersoon())));

		if(!$inCie && $uid && !$emailInCie)
		{
			$page->add(new HtmlParagraph(_("Het systeemaccount van het cielid is "
				. "niet gekoppeld op het systeem!")));
			$page->add($form = HtmlForm::named("VoegSysteemAccountToe"));
			$form->add(HtmlInput::makeSubmitButton(_("Fix het!")));
		}
		else if(!$inCie && $uid && $emailInCie)
		{
			$page->add(new HtmlParagraph(_("Het systeemaccount van het cielid is "
				. "niet gekoppeld op het systeem, maar het emailadres van het lid "
				. "staat wel in de forward op het systeem.")));
			$page->add($form = HtmlForm::named("VoegSysteemAccountToeHaalEmailWeg"));
			$form->add(HtmlInput::makeSubmitButton(_("Fix het!")));
		}
		else if(!$uid && !$emailInCie)
		{
			$page->add(new HtmlParagraph(_("Het commissielid heeft geen systeemaccount "
				. "en het emailadres staat ook niet in de forward!")));
			$page->add($form = HtmlForm::named("VoegEmailToe"));
			$form->add(HtmlInput::makeSubmitButton(_("Doe het, voeg het emailadres toe!")));
			$page->add(new HtmlBreak());
			$page->add(HtmlAnchor::button($cielid->getPersoon()->url() . '/GeefSysteemAccount', _("Geef een systeemaccount")));
		}
		else if(!$uid && $emailInCie)
		{
			$page->add(new HtmlParagraph(_("Het commissielid heeft een email in "
				. "de forward op het systeem, maar het lid heeft nog geen "
				. "systeemaccount.")));
			$page->add(HtmlAnchor::button($cielid->getPersoon()->url() . '/GeefSysteemAccount', _("Geef een systeemaccount")));
		}
		else
		{
			$page->add(new HtmlParagraph(_("Er zijn geen problemen met het systeem van dit commissielid")));
		}

		$page->end();
	}

	static public function opmerkingCommissie()
	{
		return _("Kies er een");
	}

	static public function formCommissie(CommissieLid $obj)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}

	//Ledenkart
	static public function labelPersoon(CommissieLid $obj)
	{
		return "Leden";
	}
	static public function labelDatumBegin(CommissieLid $obj)
	{
		return _("Begindatum");
	}
	static public function labelDatumEind(CommissieLid $obj)
	{
		return _("Einddatum");
	}
	static public function labelSpreuk(CommissieLid $obj)
	{
		return _("Spreuk/motto");
	}
	static public function labelSpreukZichtbaar(CommissieLid $obj)
	{
		return _("Spreuk zichtbaar");
	}
	static public function labelFunctie(CommissieLid $obj)
	{
		return _("Functie");
	}
	static public function labelFunctieNaam(CommissieLid $obj)
	{
		return _("Custom functienaam");
	}
	/**
	 *  Geef een tekstuele weergave van het veld 'functie' of 'functieNaam'.
	 * Als deze een custom naam heeft, wordt de officiële omschrijving in haakjes erbij gezet.
	 * @returns Een string klaar om in HTML gezet te worden.
	 */
	static public function waardeFunctie(CommissieLid $obj)
	{
		$functie = commissieFuncties();
		$echteNaam = $functie[$obj->getFunctie()];
		$customNaam = $obj->getFunctieNaam();
		if ($customNaam) {
			return sprintf("%s (%s)", $customNaam, $echteNaam);
		} else {
			return $echteNaam;
		}
	}

	public static function labelenumFunctie($value)
	{
		switch($value)
		{
			case 'VOORZ':	return _('[VOC:commissiefunctie]Voorzitter');
			case 'SECR':	return _('[VOC:commissiefunctie]Secretaris');
			case 'PENNY':	return _('[VOC:commissiefunctie]Penningmeester');
			case 'PR':		return _('[VOC:commissiefunctie]Promocommissaris');
			case 'SPONS':	return _('[VOC:commissiefunctie]Sponsorcommissaris');
			case 'BESTUUR':	return _('[VOC:commissiefunctie]Bestuurslid');
			case 'SFEER':
				return _('[VOC:commissiefunctie]Sfeercommissaris');
			default:
			case 'OVERIG':	return _('[VOC:commissiefunctie]Overig');
		}
	}

	static public function opmerkingPersoon()
	{
		return "";
	}

	static public function formPersoon() {
		return self::defaultFormPersoonVerzameling(null, "CieLid");
	}

    /**
     * @brief Geef de waarde van het veld datumBegin.
     *
     * @param obj Het CommissieLid-object waarvan de waarde wordt verkregen.
     * @return Een html-veilige string die de waarde van het veld datumBegin
     * van het object obj representeert.
     */
    public static function waardeDatumBeginISO(CommissieLid $obj)
    {
		$datum = self::callobj($obj, "get", "datumBegin");
		return static::outputDateTime($datum, "%Y-%m-%d");
	}

	public static function waardeDatumEindISO(CommissieLid $obj)
	{
		$datum = self::callobj($obj, "get", "datumEind");
		return static::outputDateTime($datum, "%Y-%m-%d");
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
