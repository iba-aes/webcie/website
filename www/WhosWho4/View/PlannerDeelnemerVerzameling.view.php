<?

/**
 * $Id$
 */
abstract class PlannerDeelnemerVerzamelingView
	extends PlannerDeelnemerVerzamelingView_Generated
{
	/**
	 * Formulier om een planner in te vullen
	 */
	static public function invullenForm ($name, PlannerDeelnemerVerzameling $verzameling)
	{
		$form = HtmlForm::named($name);
		$form->addClass('form-inline');
		$form->add(new HtmlSpan(_("Sleep over de antwoorden die je in wilt vullen")));
		$form->add($table = new HtmlTable(null, 'noSelect'))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Invullen!"))));

		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->makeHeaderFromArray(array(_("Datum"), _("Ja"), _("Half"), _("Nee"), _("Onbekend"), _("Activiteiten")));

		foreach ($verzameling as $deelnemer)
		{
			$datum = $deelnemer->getPlannerData();
			$tbody->add($row = new HtmlTableRow());
			$row->addData(PlannerDataView::waardeMomentBegin($datum), 'min10em');
			$row->addData(HtmlInput::makeRadio('PlannerDeelnemer[' . $datum->geefID() . ']', 'JA', $deelnemer->getAntwoord() == 'JA'));
			$row->addData(HtmlInput::makeRadio('PlannerDeelnemer[' . $datum->geefID() . ']', 'LIEVER_NIET', $deelnemer->getAntwoord() == 'LIEVER_NIET'));
			$row->addData(HtmlInput::makeRadio('PlannerDeelnemer[' . $datum->geefID() . ']', 'NEE', $deelnemer->getAntwoord() == 'NEE'));
			$row->addData(HtmlInput::makeRadio('PlannerDeelnemer[' . $datum->geefID() . ']', 'ONBEKEND', $deelnemer->getAntwoord() == 'ONBEKEND'));
			$alleActiviteiten = ActiviteitVerzameling::getActiviteitenMetBegindatum($datum->getMomentBegin(), 0, 'ALLE');
			$row->addData(ActiviteitVerzamelingView::klikbaarKommaLijst($alleActiviteiten));
		}

		return $form;
	}

	static public function processInvullenForm (PlannerDeelnemerVerzameling $verzameling)
	{
		/** Haal alle data op **/
		if (!$data = tryPar('PlannerDeelnemer', null))
			return false;

		foreach ($verzameling as $deelnemer)
		{
			$dataID = $deelnemer->getPlannerData()->geefID();
			if (isset($data[$dataID]))
				$deelnemer->setAntwoord($data[$dataID]);
		}
	
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
