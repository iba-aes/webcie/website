<?
abstract class DocuBestandVerzamelingView
	extends DocuBestandVerzamelingView_Generated
{
	/*
	 *  Geeft het overzicht van alle docubestanden
	 *
	 * @param categorie De categorie waarvan bestanden worden laten zien
	 * @param cats Alle categorieën om uit te kiezen
	 * @param docs De docubestanden om weer te geven
	 */
	static public function overzicht($categorie, $cats, $docs, $offset)
	{
		global $filesystem;

		$table = new HtmlTable(null, null, 'docuTable');
		$table->setCssStyle('display: none;');

		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_('Upload-datum'));
		$row->addHeader(_('Bestand'));
		$row->addHeader(_('Categorie'));
		$row->addHeader(_('Grootte'));
		if(hasAuth('bestuur'))
			$row->addHeader(_('Wijzigen/Verwijderen'));

		$i = 0;
		$warnings = 0;

		foreach($docs as $doc) {

			$bestand = $doc->getBestand();

			$rood = false;
			$size = true;
			// Als het niet gevonden is en het is een bestuurslid die het
			// bekijkt
			// laat dan het bestand rood zie en maak het niet een anchor
			if(!$filesystem->has($bestand))
			{
				if(!hasAuth('bestuur'))
					continue;

				Page::addMelding(sprintf('Bestand %s[VOC: bestandsnaam] niet gevonden!', $bestand), 'fout');

				$rood = true;
				$size = false;
			}

			$row = $tbody->addRow();
			$cell = $row->addData(DocuBestandView::waardeDatum($doc));
			$cell->setAttribute('data-order', $doc->getDatum()->getTimestamp());

			if($rood)
			{
				$row->addData(DocuBestandView::waardeTitel($doc));
				$row->addClass('text-danger strongtext');
			}
			else
			{
				$row->addData(new HtmlAnchor(DOCUBASE.$doc->getId(),
					find_icon($bestand) . ' ' . DocuBestandView::waardeTitel($doc)));
			}

			$row->addData(DocuBestandView::waardeCategorie($doc));

			if($size)
			{
				$grootte = $filesystem->getSize($bestand);
				$cell = $row->addData(show_readable_size($grootte));
				$cell->setAttribute('data-order', $grootte);
			}
			else
			{
				$cell = $row->addData('??? k');
				$cell->setAttribute('data-order', 0);
			}

			if(hasAuth('bestuur')) {
				$row->addData(array(
					HtmlAnchor::button(DOCUBASE.$doc->getId().'/Wijzig', 'Edit', null, null, null, 'default'),
					HtmlAnchor::button(DOCUBASE.$doc->getId().'/Verwijder', 'Del', null, null, null, 'danger')));
			}
		}

		$page = Page::getInstance()
			->addFooterJS(Page::minifiedFile('datatables.js', 'js'))
			->addHeadCss(Page::minifiedFile('datatables.css', 'css'))
			->addFooter(HtmlScript::makeJavascript('$("#docuTable").show().DataTable({aaSorting: [[0, "desc"]]});'))
			->start(_('Documentatiecentrum'));

		if(hasAuth('bestuur'))
		{
			$page->add(HtmlAnchor::button(DOCUBASE.'Toevoegen', 'Toevoegen'));
			$page->add(HtmlAnchor::button(DOCUBASE.'Categorie', 'Category management'));
			$page->add(new HtmlBreak());
			$page->add(new HtmlBreak());
		}

		$page->add($table)
			->end();
	}
}
