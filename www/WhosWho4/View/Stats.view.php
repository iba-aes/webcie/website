<?php

abstract class StatsView
	extends View
{
	/**
	 * @brief Maak een tabel van een array van VFS entries en hun statistieken.
	 *
	 * @param entries Een array van associative arrays van de vorm:
	 *     `array('entry' => vfsEntry, 'views' => int)`
	 *
	 * @returns Een HTML-object met de gegenereerde tabel.
	 */
	public static function viewCountTabel($entries)
	{
		$table = new HtmlTable();

		$table->add($head = new HtmlTableHead());
		$head->addRow()->makeHeaderFromArray(array(_("ID"), _("URL"), _("Views")));

		foreach ($entries as $pair) {
			$entry = $pair['entry'];
			$views = $pair['views'];

			$table->add($row = new HtmlTableRow());

			$entryURL = $entry->url();
			$benamiteURL = $entry->benamiteUrl();

			$row->addData(new HtmlAnchor($benamiteURL, $entry->getId()));
			$row->addData(
				// Variabele entries hebben *-s enzo in de URL dus dat zou niet
				// werken als link.
				$entry->hasConcreteUrl()
					? new HtmlAnchor($entryURL, $entryURL)
					: $entryURL
			);
			$row->addData($views);
		}

		$table->addClass('sortable');

		return $table;
	}
}
