<?
abstract class ActiviteitCategorieVerzamelingView
	extends ActiviteitCategorieVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde($obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Toon alle (gegeven) categorieen in een mooi tabelletje.
	 * @param cats Indien NULL, laad ze allemaal uit de databaas.
	 * @returns Een HtmlTable
	 */
	public static function categorieOverzicht(ActiviteitCategorieVerzameling $cats = null) {
		if (is_null($cats)) {
			$cats = ActiviteitCategorieQuery::table()->verzamel();
		}
		$tabel = new HtmlTable();
		$tabel->add(new HtmlTableHead($row = new HtmlTableRow()));
		$row->add(new HtmlTableHeaderCell(_("[VOC:activiteit-]Categorie")));
		$row->add(new HtmlTableHeaderCell()); // wijzig
		$row->add(new HtmlTableHeaderCell()); // verwijder

		foreach ($cats as $cat) {
			$catID = $cat->geefID();
			$row = new HtmlTableRow();
			$row->add(new HtmlTableDataCell(ActiviteitCategorieView::waardeTitel($cat)));
			$row->add(new HtmlTableDataCell(HtmlAnchor::button("/Activiteiten/Categorie/".$catID, _("Info"))));
			$row->add(new HtmlTableDataCell(HtmlAnchor::button("/Activiteiten/Categorie/".$catID."/Wijzig", _("Wijzig"))));
			$row->add(new HtmlTableDataCell(HtmlAnchor::button("/Activiteiten/Categorie/".$catID."/Verwijder", _("Verwijder"))));

			$tabel->addImmutable($row);
		}

		return $tabel;
	}
}
