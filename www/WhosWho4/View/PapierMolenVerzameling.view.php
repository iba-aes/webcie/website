<?
abstract class PapierMolenVerzamelingView
	extends PapierMolenVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function overzicht(PapierMolenVerzameling $pms)
	{
		global $STUDIENAMEN;

		$page = Page::getInstance()->start(_("Papiermolen"));

		$page->add($form = new HtmlForm('GET', null, true));

		$studie_options = array_merge(array('' => 'Alles'), $STUDIENAMEN);
		$studie = tryPar('studie', '');
		$form->add($box = HtmlSelectbox::fromArray('studie', $studie_options, array($studie), 1));
		$box->setAttribute('onchange', 'this.form.submit()');

		$page->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("ISBN"), _("Auteur"), _("Titel"), _("Prijs"), ""));

		foreach($pms as $pm)
		{
			$row = $tbody->addRow();
			$row->addData(PapierMolenView::waardeEan($pm));
			$row->addData(PapierMolenView::waardeAuteur($pm));
			$row->addData(PapierMolenView::waardeTitel($pm));
			$row->addData(PapierMolenView::waardePrijs($pm));
			$row->addData(new HtmlAnchor($pm->url(), _("Meer info")));
		}

		$page->end();
	}
}
