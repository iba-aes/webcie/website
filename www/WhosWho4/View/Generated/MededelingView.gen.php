<?
abstract class MededelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MededelingView.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMededeling(Mededeling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mededelingID.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mededelingID labelt.
	 */
	public static function labelMededelingID(Mededeling $obj)
	{
		return 'MededelingID';
	}
	/**
	 * @brief Geef de waarde van het veld mededelingID.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mededelingID van het object
	 * obj representeert.
	 */
	public static function waardeMededelingID(Mededeling $obj)
	{
		return static::defaultWaardeInt($obj, 'MededelingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mededelingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mededelingID representeert.
	 */
	public static function opmerkingMededelingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(Mededeling $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(Mededeling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(Mededeling $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld doelgroep.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld doelgroep labelt.
	 */
	public static function labelDoelgroep(Mededeling $obj)
	{
		return 'Doelgroep';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld doelgroep.
	 *
	 * @param string $value Een enum-waarde van het veld doelgroep.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumDoelgroep($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld doelgroep horen.
	 *
	 * @see labelenumDoelgroep
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld doelgroep
	 * representeren.
	 */
	public static function labelenumDoelgroepArray()
	{
		$soorten = array();
		foreach(Mededeling::enumsDoelgroep() as $id)
			$soorten[$id] = MededelingView::labelenumDoelgroep($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld doelgroep.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld doelgroep van het object obj
	 * representeert.
	 */
	public static function waardeDoelgroep(Mededeling $obj)
	{
		return static::defaultWaardeEnum($obj, 'Doelgroep');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld doelgroep.
	 *
	 * @see genericFormdoelgroep
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld doelgroep staat en kan
	 * worden bewerkt. Indien doelgroep read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDoelgroep(Mededeling $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Doelgroep', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld doelgroep. In
	 * tegenstelling tot formdoelgroep moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdoelgroep
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld doelgroep staat en kan
	 * worden bewerkt. Indien doelgroep read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDoelgroep($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Doelgroep', Mededeling::enumsdoelgroep());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * doelgroep bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld doelgroep representeert.
	 */
	public static function opmerkingDoelgroep()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prioriteit.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld prioriteit labelt.
	 */
	public static function labelPrioriteit(Mededeling $obj)
	{
		return 'Prioriteit';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld prioriteit.
	 *
	 * @param string $value Een enum-waarde van het veld prioriteit.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumPrioriteit($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld prioriteit horen.
	 *
	 * @see labelenumPrioriteit
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld prioriteit
	 * representeren.
	 */
	public static function labelenumPrioriteitArray()
	{
		$soorten = array();
		foreach(Mededeling::enumsPrioriteit() as $id)
			$soorten[$id] = MededelingView::labelenumPrioriteit($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prioriteit van het object obj
	 * representeert.
	 */
	public static function waardePrioriteit(Mededeling $obj)
	{
		return static::defaultWaardeEnum($obj, 'Prioriteit');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prioriteit.
	 *
	 * @see genericFormprioriteit
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPrioriteit(Mededeling $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Prioriteit', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prioriteit. In
	 * tegenstelling tot formprioriteit moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formprioriteit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPrioriteit($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Prioriteit', Mededeling::enumsprioriteit());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * prioriteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prioriteit representeert.
	 */
	public static function opmerkingPrioriteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(Mededeling $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(Mededeling $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(Mededeling $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumEind.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumEind labelt.
	 */
	public static function labelDatumEind(Mededeling $obj)
	{
		return 'DatumEind';
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumEind van het object obj
	 * representeert.
	 */
	public static function waardeDatumEind(Mededeling $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumEind.
	 *
	 * @see genericFormdatumEind
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumEind(Mededeling $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumEind', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumEind. In
	 * tegenstelling tot formdatumEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumEind representeert.
	 */
	public static function opmerkingDatumEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld url.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld url labelt.
	 */
	public static function labelUrl(Mededeling $obj)
	{
		return 'Url';
	}
	/**
	 * @brief Geef de waarde van het veld url.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld url van het object obj
	 * representeert.
	 */
	public static function waardeUrl(Mededeling $obj)
	{
		return static::defaultWaardeUrl($obj, 'Url');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld url.
	 *
	 * @see genericFormurl
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld url staat en kan worden
	 * bewerkt. Indien url read-only is betreft het een statisch html-element.
	 */
	public static function formUrl(Mededeling $obj, $include_id = false)
	{
		return static::defaultFormUrl($obj, 'Url', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld url. In tegenstelling
	 * tot formurl moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formurl
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld url staat en kan worden
	 * bewerkt. Indien url read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormUrl($name, $waarde=NULL)
	{
		return static::genericDefaultFormUrl($name, $waarde, 'Url');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld url
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld url representeert.
	 */
	public static function opmerkingUrl()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Mededeling $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Mededeling $obj)
	{
		return static::defaultWaardeString($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Mededeling $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Omschrijving', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Omschrijving', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Omschrijving', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Omschrijving', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mededeling.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mededeling labelt.
	 */
	public static function labelMededeling(Mededeling $obj)
	{
		return 'Mededeling';
	}
	/**
	 * @brief Geef de waarde van het veld mededeling.
	 *
	 * @param Mededeling $obj Het Mededeling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mededeling van het object obj
	 * representeert.
	 */
	public static function waardeMededeling(Mededeling $obj)
	{
		return static::defaultWaardeHtml($obj, 'Mededeling');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mededeling.
	 *
	 * @see genericFormmededeling
	 *
	 * @param Mededeling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mededeling staat en kan
	 * worden bewerkt. Indien mededeling read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMededeling(Mededeling $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormHtml($obj, 'Mededeling', 'nl', $include_id),
			'en' => static::defaultFormHtml($obj, 'Mededeling', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mededeling. In
	 * tegenstelling tot formmededeling moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmededeling
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mededeling staat en kan
	 * worden bewerkt. Indien mededeling read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMededeling($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormHtml($name, $waarde, 'Mededeling', 'nl'),
			'en' => static::genericDefaultFormHtml($name, $waarde, 'Mededeling', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mededeling bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mededeling representeert.
	 */
	public static function opmerkingMededeling()
	{
		return NULL;
	}
}
