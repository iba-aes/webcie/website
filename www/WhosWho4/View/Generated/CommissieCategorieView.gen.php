<?
abstract class CommissieCategorieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CommissieCategorieView.
	 *
	 * @param CommissieCategorie $obj Het CommissieCategorie-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieCategorie(CommissieCategorie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld categorieID.
	 *
	 * @param CommissieCategorie $obj Het CommissieCategorie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorieID labelt.
	 */
	public static function labelCategorieID(CommissieCategorie $obj)
	{
		return 'CategorieID';
	}
	/**
	 * @brief Geef de waarde van het veld categorieID.
	 *
	 * @param CommissieCategorie $obj Het CommissieCategorie-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorieID van het object
	 * obj representeert.
	 */
	public static function waardeCategorieID(CommissieCategorie $obj)
	{
		return static::defaultWaardeInt($obj, 'CategorieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorieID representeert.
	 */
	public static function opmerkingCategorieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param CommissieCategorie $obj Het CommissieCategorie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(CommissieCategorie $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param CommissieCategorie $obj Het CommissieCategorie-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(CommissieCategorie $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param CommissieCategorie $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(CommissieCategorie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
}
