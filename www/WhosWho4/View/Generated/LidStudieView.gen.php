<?
abstract class LidStudieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LidStudieView.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidStudie(LidStudie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lidStudieID.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidStudieID labelt.
	 */
	public static function labelLidStudieID(LidStudie $obj)
	{
		return 'LidStudieID';
	}
	/**
	 * @brief Geef de waarde van het veld lidStudieID.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidStudieID van het object
	 * obj representeert.
	 */
	public static function waardeLidStudieID(LidStudie $obj)
	{
		return static::defaultWaardeInt($obj, 'LidStudieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * lidStudieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidStudieID representeert.
	 */
	public static function opmerkingLidStudieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld studie.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studie labelt.
	 */
	public static function labelStudie(LidStudie $obj)
	{
		return 'Studie';
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studie van het object obj
	 * representeert.
	 */
	public static function waardeStudie(LidStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getStudie())
			return NULL;
		return StudieView::defaultWaardeStudie($obj->getStudie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld studie.
	 *
	 * @see genericFormstudie
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studie staat en kan worden
	 * bewerkt. Indien studie read-only is betreft het een statisch html-element.
	 */
	public static function formStudie(LidStudie $obj, $include_id = false)
	{
		return StudieView::defaultForm($obj->getStudie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld studie. In
	 * tegenstelling tot formstudie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstudie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studie staat en kan worden
	 * bewerkt. Indien studie read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStudie($name, $waarde=NULL)
	{
		return StudieView::genericDefaultForm('Studie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld studie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studie representeert.
	 */
	public static function opmerkingStudie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(LidStudie $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(LidStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lid.
	 *
	 * @see genericFormlid
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lid staat en kan worden
	 * bewerkt. Indien lid read-only is betreft het een statisch html-element.
	 */
	public static function formLid(LidStudie $obj, $include_id = false)
	{
		return static::waardeLid($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lid. In tegenstelling
	 * tot formlid moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formlid
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lid staat en kan worden
	 * bewerkt. Indien lid read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLid($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(LidStudie $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(LidStudie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(LidStudie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumEind.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumEind labelt.
	 */
	public static function labelDatumEind(LidStudie $obj)
	{
		return 'DatumEind';
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumEind van het object obj
	 * representeert.
	 */
	public static function waardeDatumEind(LidStudie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumEind.
	 *
	 * @see genericFormdatumEind
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumEind(LidStudie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumEind', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumEind. In
	 * tegenstelling tot formdatumEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumEind representeert.
	 */
	public static function opmerkingDatumEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(LidStudie $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(LidStudie::enumsStatus() as $id)
			$soorten[$id] = LidStudieView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(LidStudie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(LidStudie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', LidStudie::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld groep.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld groep labelt.
	 */
	public static function labelGroep(LidStudie $obj)
	{
		return 'Groep';
	}
	/**
	 * @brief Geef de waarde van het veld groep.
	 *
	 * @param LidStudie $obj Het LidStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld groep van het object obj
	 * representeert.
	 */
	public static function waardeGroep(LidStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getGroep())
			return NULL;
		return IntroGroepView::defaultWaardeIntroGroep($obj->getGroep());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld groep.
	 *
	 * @see genericFormgroep
	 *
	 * @param LidStudie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld groep staat en kan worden
	 * bewerkt. Indien groep read-only is betreft het een statisch html-element.
	 */
	public static function formGroep(LidStudie $obj, $include_id = false)
	{
		return IntroGroepView::defaultForm($obj->getGroep());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld groep. In
	 * tegenstelling tot formgroep moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formgroep
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld groep staat en kan worden
	 * bewerkt. Indien groep read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormGroep($name, $waarde=NULL)
	{
		return IntroGroepView::genericDefaultForm('Groep');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld groep
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld groep representeert.
	 */
	public static function opmerkingGroep()
	{
		return NULL;
	}
}
