<?
abstract class TinyUrlView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TinyUrlView.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTinyUrl(TinyUrl $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(TinyUrl $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(TinyUrl $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tinyUrl.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tinyUrl labelt.
	 */
	public static function labelTinyUrl(TinyUrl $obj)
	{
		return 'TinyUrl';
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tinyUrl van het object obj
	 * representeert.
	 */
	public static function waardeTinyUrl(TinyUrl $obj)
	{
		return static::defaultWaardeString($obj, 'TinyUrl');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tinyUrl.
	 *
	 * @see genericFormtinyUrl
	 *
	 * @param TinyUrl $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tinyUrl staat en kan
	 * worden bewerkt. Indien tinyUrl read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTinyUrl(TinyUrl $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'TinyUrl', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tinyUrl. In
	 * tegenstelling tot formtinyUrl moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtinyUrl
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tinyUrl staat en kan
	 * worden bewerkt. Indien tinyUrl read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTinyUrl($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'TinyUrl', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tinyUrl
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tinyUrl representeert.
	 */
	public static function opmerkingTinyUrl()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld outgoingUrl.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld outgoingUrl labelt.
	 */
	public static function labelOutgoingUrl(TinyUrl $obj)
	{
		return 'OutgoingUrl';
	}
	/**
	 * @brief Geef de waarde van het veld outgoingUrl.
	 *
	 * @param TinyUrl $obj Het TinyUrl-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld outgoingUrl van het object
	 * obj representeert.
	 */
	public static function waardeOutgoingUrl(TinyUrl $obj)
	{
		return static::defaultWaardeUrl($obj, 'OutgoingUrl');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld outgoingUrl.
	 *
	 * @see genericFormoutgoingUrl
	 *
	 * @param TinyUrl $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld outgoingUrl staat en kan
	 * worden bewerkt. Indien outgoingUrl read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOutgoingUrl(TinyUrl $obj, $include_id = false)
	{
		return static::defaultFormUrl($obj, 'OutgoingUrl', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld outgoingUrl. In
	 * tegenstelling tot formoutgoingUrl moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formoutgoingUrl
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld outgoingUrl staat en kan
	 * worden bewerkt. Indien outgoingUrl read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOutgoingUrl($name, $waarde=NULL)
	{
		return static::genericDefaultFormUrl($name, $waarde, 'OutgoingUrl');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * outgoingUrl bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld outgoingUrl representeert.
	 */
	public static function opmerkingOutgoingUrl()
	{
		return NULL;
	}
}
