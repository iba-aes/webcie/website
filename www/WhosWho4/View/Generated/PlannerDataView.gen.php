<?
abstract class PlannerDataView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerDataView.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlannerData(PlannerData $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld plannerDataID.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld plannerDataID labelt.
	 */
	public static function labelPlannerDataID(PlannerData $obj)
	{
		return 'PlannerDataID';
	}
	/**
	 * @brief Geef de waarde van het veld plannerDataID.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld plannerDataID van het object
	 * obj representeert.
	 */
	public static function waardePlannerDataID(PlannerData $obj)
	{
		return static::defaultWaardeInt($obj, 'PlannerDataID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * plannerDataID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld plannerDataID representeert.
	 */
	public static function opmerkingPlannerDataID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld planner.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld planner labelt.
	 */
	public static function labelPlanner(PlannerData $obj)
	{
		return 'Planner';
	}
	/**
	 * @brief Geef de waarde van het veld planner.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld planner van het object obj
	 * representeert.
	 */
	public static function waardePlanner(PlannerData $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPlanner())
			return NULL;
		return PlannerView::defaultWaardePlanner($obj->getPlanner());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld planner.
	 *
	 * @see genericFormplanner
	 *
	 * @param PlannerData $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld planner staat en kan
	 * worden bewerkt. Indien planner read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPlanner(PlannerData $obj, $include_id = false)
	{
		return PlannerView::defaultForm($obj->getPlanner());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld planner. In
	 * tegenstelling tot formplanner moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formplanner
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld planner staat en kan
	 * worden bewerkt. Indien planner read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPlanner($name, $waarde=NULL)
	{
		return PlannerView::genericDefaultForm('Planner');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld planner
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld planner representeert.
	 */
	public static function opmerkingPlanner()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentBegin.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentBegin labelt.
	 */
	public static function labelMomentBegin(PlannerData $obj)
	{
		return 'MomentBegin';
	}
	/**
	 * @brief Geef de waarde van het veld momentBegin.
	 *
	 * @param PlannerData $obj Het PlannerData-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentBegin van het object
	 * obj representeert.
	 */
	public static function waardeMomentBegin(PlannerData $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentBegin.
	 *
	 * @see genericFormmomentBegin
	 *
	 * @param PlannerData $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentBegin staat en kan
	 * worden bewerkt. Indien momentBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMomentBegin(PlannerData $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentBegin. In
	 * tegenstelling tot formmomentBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmomentBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentBegin staat en kan
	 * worden bewerkt. Indien momentBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMomentBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentBegin representeert.
	 */
	public static function opmerkingMomentBegin()
	{
		return NULL;
	}
}
