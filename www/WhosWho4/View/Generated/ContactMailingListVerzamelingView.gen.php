<?
abstract class ContactMailingListVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactMailingListVerzamelingView.
	 *
	 * @param ContactMailingListVerzameling $obj Het
	 * ContactMailingListVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactMailingListVerzameling(ContactMailingListVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
