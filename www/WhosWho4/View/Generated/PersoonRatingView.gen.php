<?
abstract class PersoonRatingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonRatingView.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonRating(PersoonRating $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld ratingObject.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld ratingObject labelt.
	 */
	public static function labelRatingObject(PersoonRating $obj)
	{
		return 'RatingObject';
	}
	/**
	 * @brief Geef de waarde van het veld ratingObject.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ratingObject van het object
	 * obj representeert.
	 */
	public static function waardeRatingObject(PersoonRating $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getRatingObject())
			return NULL;
		return RatingObjectView::defaultWaardeRatingObject($obj->getRatingObject());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * ratingObject bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ratingObject representeert.
	 */
	public static function opmerkingRatingObject()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(PersoonRating $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(PersoonRating $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rating.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld rating labelt.
	 */
	public static function labelRating(PersoonRating $obj)
	{
		return 'Rating';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld rating.
	 *
	 * @param string $value Een enum-waarde van het veld rating.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumRating($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld rating horen.
	 *
	 * @see labelenumRating
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld rating
	 * representeren.
	 */
	public static function labelenumRatingArray()
	{
		$soorten = array();
		foreach(PersoonRating::enumsRating() as $id)
			$soorten[$id] = PersoonRatingView::labelenumRating($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @param PersoonRating $obj Het PersoonRating-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rating van het object obj
	 * representeert.
	 */
	public static function waardeRating(PersoonRating $obj)
	{
		return static::defaultWaardeEnum($obj, 'Rating');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rating.
	 *
	 * @see genericFormrating
	 *
	 * @param PersoonRating $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is betreft het een statisch html-element.
	 */
	public static function formRating(PersoonRating $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Rating', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rating. In
	 * tegenstelling tot formrating moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrating
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormRating($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Rating', PersoonRating::enumsrating());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld rating
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rating representeert.
	 */
	public static function opmerkingRating()
	{
		return NULL;
	}
}
