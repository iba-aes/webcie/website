<?
abstract class CommissieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CommissieView.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissie(Commissie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld commissieID.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissieID labelt.
	 */
	public static function labelCommissieID(Commissie $obj)
	{
		return 'CommissieID';
	}
	/**
	 * @brief Geef de waarde van het veld commissieID.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissieID van het object
	 * obj representeert.
	 */
	public static function waardeCommissieID(Commissie $obj)
	{
		return static::defaultWaardeInt($obj, 'CommissieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissieID representeert.
	 */
	public static function opmerkingCommissieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Commissie $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Commissie $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Commissie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Commissie $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(Commissie::enumsSoort() as $id)
			$soorten[$id] = CommissieView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Commissie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Commissie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', Commissie::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld categorie.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorie labelt.
	 */
	public static function labelCategorie(Commissie $obj)
	{
		return 'Categorie';
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorie van het object obj
	 * representeert.
	 */
	public static function waardeCategorie(Commissie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCategorie())
			return NULL;
		return CommissieCategorieView::defaultWaardeCommissieCategorie($obj->getCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld categorie.
	 *
	 * @see genericFormcategorie
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCategorie(Commissie $obj, $include_id = false)
	{
		return CommissieCategorieView::defaultForm($obj->getCategorie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld categorie. In
	 * tegenstelling tot formcategorie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCategorie($name, $waarde=NULL)
	{
		return CommissieCategorieView::genericDefaultForm('Categorie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorie representeert.
	 */
	public static function opmerkingCategorie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld thema.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld thema labelt.
	 */
	public static function labelThema(Commissie $obj)
	{
		return 'Thema';
	}
	/**
	 * @brief Geef de waarde van het veld thema.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld thema van het object obj
	 * representeert.
	 */
	public static function waardeThema(Commissie $obj)
	{
		return static::defaultWaardeString($obj, 'Thema');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld thema.
	 *
	 * @see genericFormthema
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld thema staat en kan worden
	 * bewerkt. Indien thema read-only is betreft het een statisch html-element.
	 */
	public static function formThema(Commissie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Thema', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld thema. In
	 * tegenstelling tot formthema moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formthema
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld thema staat en kan worden
	 * bewerkt. Indien thema read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormThema($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Thema', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld thema
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld thema representeert.
	 */
	public static function opmerkingThema()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Commissie $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Commissie $obj)
	{
		return static::defaultWaardeString($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Commissie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(Commissie $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(Commissie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(Commissie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumEind.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumEind labelt.
	 */
	public static function labelDatumEind(Commissie $obj)
	{
		return 'DatumEind';
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumEind van het object obj
	 * representeert.
	 */
	public static function waardeDatumEind(Commissie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumEind.
	 *
	 * @see genericFormdatumEind
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumEind(Commissie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumEind', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumEind. In
	 * tegenstelling tot formdatumEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumEind representeert.
	 */
	public static function opmerkingDatumEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld login.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld login labelt.
	 */
	public static function labelLogin(Commissie $obj)
	{
		return 'Login';
	}
	/**
	 * @brief Geef de waarde van het veld login.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld login van het object obj
	 * representeert.
	 */
	public static function waardeLogin(Commissie $obj)
	{
		return static::defaultWaardeString($obj, 'Login');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld login.
	 *
	 * @see genericFormlogin
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld login staat en kan worden
	 * bewerkt. Indien login read-only is betreft het een statisch html-element.
	 */
	public static function formLogin(Commissie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Login', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld login. In
	 * tegenstelling tot formlogin moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlogin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld login staat en kan worden
	 * bewerkt. Indien login read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLogin($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Login', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld login
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld login representeert.
	 */
	public static function opmerkingLogin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld email.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld email labelt.
	 */
	public static function labelEmail(Commissie $obj)
	{
		return 'Email';
	}
	/**
	 * @brief Geef de waarde van het veld email.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld email van het object obj
	 * representeert.
	 */
	public static function waardeEmail(Commissie $obj)
	{
		return static::defaultWaardeString($obj, 'Email');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld email.
	 *
	 * @see genericFormemail
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld email staat en kan worden
	 * bewerkt. Indien email read-only is betreft het een statisch html-element.
	 */
	public static function formEmail(Commissie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Email', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld email. In
	 * tegenstelling tot formemail moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formemail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld email staat en kan worden
	 * bewerkt. Indien email read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormEmail($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Email', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld email
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld email representeert.
	 */
	public static function opmerkingEmail()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld homepage.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld homepage labelt.
	 */
	public static function labelHomepage(Commissie $obj)
	{
		return 'Homepage';
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld homepage van het object obj
	 * representeert.
	 */
	public static function waardeHomepage(Commissie $obj)
	{
		return static::defaultWaardeUrl($obj, 'Homepage');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld homepage.
	 *
	 * @see genericFormhomepage
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHomepage(Commissie $obj, $include_id = false)
	{
		return static::defaultFormUrl($obj, 'Homepage', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld homepage. In
	 * tegenstelling tot formhomepage moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formhomepage
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHomepage($name, $waarde=NULL)
	{
		return static::genericDefaultFormUrl($name, $waarde, 'Homepage');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * homepage bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld homepage representeert.
	 */
	public static function opmerkingHomepage()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld emailZichtbaar.
	 *
	 * @param Commissie $obj Het Commissie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld emailZichtbaar labelt.
	 */
	public static function labelEmailZichtbaar(Commissie $obj)
	{
		return 'EmailZichtbaar';
	}
	/**
	 * @brief Geef de waarde van het veld emailZichtbaar.
	 *
	 * @param Commissie $obj Het Commissie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld emailZichtbaar van het object
	 * obj representeert.
	 */
	public static function waardeEmailZichtbaar(Commissie $obj)
	{
		return static::defaultWaardeBool($obj, 'EmailZichtbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld emailZichtbaar.
	 *
	 * @see genericFormemailZichtbaar
	 *
	 * @param Commissie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld emailZichtbaar staat en
	 * kan worden bewerkt. Indien emailZichtbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEmailZichtbaar(Commissie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'EmailZichtbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld emailZichtbaar. In
	 * tegenstelling tot formemailZichtbaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formemailZichtbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld emailZichtbaar staat en
	 * kan worden bewerkt. Indien emailZichtbaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEmailZichtbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'EmailZichtbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * emailZichtbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld emailZichtbaar representeert.
	 */
	public static function opmerkingEmailZichtbaar()
	{
		return NULL;
	}
}
