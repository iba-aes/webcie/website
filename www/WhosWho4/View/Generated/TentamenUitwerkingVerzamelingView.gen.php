<?
abstract class TentamenUitwerkingVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TentamenUitwerkingVerzamelingView.
	 *
	 * @param TentamenUitwerkingVerzameling $obj Het
	 * TentamenUitwerkingVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTentamenUitwerkingVerzameling(TentamenUitwerkingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
