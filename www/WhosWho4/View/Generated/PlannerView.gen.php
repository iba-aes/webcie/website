<?
abstract class PlannerView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerView.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlanner(Planner $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld plannerID.
	 *
	 * @param Planner $obj Het Planner-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld plannerID labelt.
	 */
	public static function labelPlannerID(Planner $obj)
	{
		return 'PlannerID';
	}
	/**
	 * @brief Geef de waarde van het veld plannerID.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld plannerID van het object obj
	 * representeert.
	 */
	public static function waardePlannerID(Planner $obj)
	{
		return static::defaultWaardeInt($obj, 'PlannerID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * plannerID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld plannerID representeert.
	 */
	public static function opmerkingPlannerID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Planner $obj Het Planner-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Planner $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Planner $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param Planner $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(Planner $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Persoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param Planner $obj Het Planner-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(Planner $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(Planner $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param Planner $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(Planner $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Planner $obj Het Planner-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Planner $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Planner $obj)
	{
		return static::defaultWaardeHtml($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Planner $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Planner $obj, $include_id = false)
	{
		return static::defaultFormHtml($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormHtml($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mail.
	 *
	 * @param Planner $obj Het Planner-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mail labelt.
	 */
	public static function labelMail(Planner $obj)
	{
		return 'Mail';
	}
	/**
	 * @brief Geef de waarde van het veld mail.
	 *
	 * @param Planner $obj Het Planner-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mail van het object obj
	 * representeert.
	 */
	public static function waardeMail(Planner $obj)
	{
		return static::defaultWaardeBool($obj, 'Mail');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mail.
	 *
	 * @see genericFormmail
	 *
	 * @param Planner $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mail staat en kan worden
	 * bewerkt. Indien mail read-only is betreft het een statisch html-element.
	 */
	public static function formMail(Planner $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Mail', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mail. In tegenstelling
	 * tot formmail moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formmail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mail staat en kan worden
	 * bewerkt. Indien mail read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMail($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Mail');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld mail
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mail representeert.
	 */
	public static function opmerkingMail()
	{
		return NULL;
	}
}
