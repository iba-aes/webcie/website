<?
abstract class ActiviteitView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitView.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteit(Activiteit $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld activiteitID.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld activiteitID labelt.
	 */
	public static function labelActiviteitID(Activiteit $obj)
	{
		return 'ActiviteitID';
	}
	/**
	 * @brief Geef de waarde van het veld activiteitID.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteitID van het object
	 * obj representeert.
	 */
	public static function waardeActiviteitID(Activiteit $obj)
	{
		return static::defaultWaardeInt($obj, 'ActiviteitID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteitID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteitID representeert.
	 */
	public static function opmerkingActiviteitID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentBegin.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentBegin labelt.
	 */
	public static function labelMomentBegin(Activiteit $obj)
	{
		return 'MomentBegin';
	}
	/**
	 * @brief Geef de waarde van het veld momentBegin.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentBegin van het object
	 * obj representeert.
	 */
	public static function waardeMomentBegin(Activiteit $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentBegin.
	 *
	 * @see genericFormmomentBegin
	 *
	 * @param Activiteit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentBegin staat en kan
	 * worden bewerkt. Indien momentBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMomentBegin(Activiteit $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentBegin. In
	 * tegenstelling tot formmomentBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmomentBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentBegin staat en kan
	 * worden bewerkt. Indien momentBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMomentBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentBegin representeert.
	 */
	public static function opmerkingMomentBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentEind.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentEind labelt.
	 */
	public static function labelMomentEind(Activiteit $obj)
	{
		return 'MomentEind';
	}
	/**
	 * @brief Geef de waarde van het veld momentEind.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentEind van het object obj
	 * representeert.
	 */
	public static function waardeMomentEind(Activiteit $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentEind.
	 *
	 * @see genericFormmomentEind
	 *
	 * @param Activiteit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentEind staat en kan
	 * worden bewerkt. Indien momentEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMomentEind(Activiteit $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentEind', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentEind. In
	 * tegenstelling tot formmomentEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmomentEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentEind staat en kan
	 * worden bewerkt. Indien momentEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMomentEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentEind representeert.
	 */
	public static function opmerkingMomentEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld toegang.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld toegang labelt.
	 */
	public static function labelToegang(Activiteit $obj)
	{
		return 'Toegang';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld toegang.
	 *
	 * @param string $value Een enum-waarde van het veld toegang.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumToegang($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld toegang horen.
	 *
	 * @see labelenumToegang
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld toegang
	 * representeren.
	 */
	public static function labelenumToegangArray()
	{
		$soorten = array();
		foreach(Activiteit::enumsToegang() as $id)
			$soorten[$id] = ActiviteitView::labelenumToegang($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld toegang.
	 *
	 * @param Activiteit $obj Het Activiteit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld toegang van het object obj
	 * representeert.
	 */
	public static function waardeToegang(Activiteit $obj)
	{
		return static::defaultWaardeEnum($obj, 'Toegang');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld toegang.
	 *
	 * @see genericFormtoegang
	 *
	 * @param Activiteit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld toegang staat en kan
	 * worden bewerkt. Indien toegang read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formToegang(Activiteit $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Toegang', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld toegang. In
	 * tegenstelling tot formtoegang moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtoegang
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld toegang staat en kan
	 * worden bewerkt. Indien toegang read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormToegang($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Toegang', Activiteit::enumstoegang());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld toegang
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld toegang representeert.
	 */
	public static function opmerkingToegang()
	{
		return NULL;
	}
}
