<?
abstract class CommissieActiviteitView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CommissieActiviteitView.
	 *
	 * @param CommissieActiviteit $obj Het CommissieActiviteit-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieActiviteit(CommissieActiviteit $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param CommissieActiviteit $obj Het CommissieActiviteit-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(CommissieActiviteit $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param CommissieActiviteit $obj Het CommissieActiviteit-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(CommissieActiviteit $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld activiteit.
	 *
	 * @param CommissieActiviteit $obj Het CommissieActiviteit-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld activiteit labelt.
	 */
	public static function labelActiviteit(CommissieActiviteit $obj)
	{
		return 'Activiteit';
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @param CommissieActiviteit $obj Het CommissieActiviteit-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteit van het object obj
	 * representeert.
	 */
	public static function waardeActiviteit(CommissieActiviteit $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getActiviteit())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getActiviteit());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteit representeert.
	 */
	public static function opmerkingActiviteit()
	{
		return NULL;
	}
}
