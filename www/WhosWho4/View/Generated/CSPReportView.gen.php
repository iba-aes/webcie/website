<?
abstract class CSPReportView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CSPReportView.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCSPReport(CSPReport $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(CSPReport $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(CSPReport $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ingelogdLid.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ingelogdLid labelt.
	 */
	public static function labelIngelogdLid(CSPReport $obj)
	{
		return 'IngelogdLid';
	}
	/**
	 * @brief Geef de waarde van het veld ingelogdLid.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ingelogdLid van het object
	 * obj representeert.
	 */
	public static function waardeIngelogdLid(CSPReport $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getIngelogdLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getIngelogdLid());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ingelogdLid.
	 *
	 * @see genericFormingelogdLid
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ingelogdLid staat en kan
	 * worden bewerkt. Indien ingelogdLid read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formIngelogdLid(CSPReport $obj, $include_id = false)
	{
		return LidView::defaultForm($obj->getIngelogdLid());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ingelogdLid. In
	 * tegenstelling tot formingelogdLid moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formingelogdLid
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ingelogdLid staat en kan
	 * worden bewerkt. Indien ingelogdLid read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormIngelogdLid($name, $waarde=NULL)
	{
		return LidView::genericDefaultForm('IngelogdLid');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * ingelogdLid bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ingelogdLid representeert.
	 */
	public static function opmerkingIngelogdLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ip.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ip labelt.
	 */
	public static function labelIp(CSPReport $obj)
	{
		return 'Ip';
	}
	/**
	 * @brief Geef de waarde van het veld ip.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ip van het object obj
	 * representeert.
	 */
	public static function waardeIp(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'Ip');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ip.
	 *
	 * @see genericFormip
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ip staat en kan worden
	 * bewerkt. Indien ip read-only is betreft het een statisch html-element.
	 */
	public static function formIp(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Ip', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ip. In tegenstelling
	 * tot formip moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formip
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ip staat en kan worden
	 * bewerkt. Indien ip read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormIp($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Ip', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ip
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ip representeert.
	 */
	public static function opmerkingIp()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld documentUri.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld documentUri labelt.
	 */
	public static function labelDocumentUri(CSPReport $obj)
	{
		return 'DocumentUri';
	}
	/**
	 * @brief Geef de waarde van het veld documentUri.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld documentUri van het object
	 * obj representeert.
	 */
	public static function waardeDocumentUri(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'DocumentUri');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld documentUri.
	 *
	 * @see genericFormdocumentUri
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld documentUri staat en kan
	 * worden bewerkt. Indien documentUri read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDocumentUri(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'DocumentUri', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld documentUri. In
	 * tegenstelling tot formdocumentUri moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdocumentUri
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld documentUri staat en kan
	 * worden bewerkt. Indien documentUri read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDocumentUri($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'DocumentUri', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * documentUri bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld documentUri representeert.
	 */
	public static function opmerkingDocumentUri()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld referrer.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld referrer labelt.
	 */
	public static function labelReferrer(CSPReport $obj)
	{
		return 'Referrer';
	}
	/**
	 * @brief Geef de waarde van het veld referrer.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld referrer van het object obj
	 * representeert.
	 */
	public static function waardeReferrer(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'Referrer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld referrer.
	 *
	 * @see genericFormreferrer
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld referrer staat en kan
	 * worden bewerkt. Indien referrer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formReferrer(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Referrer', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld referrer. In
	 * tegenstelling tot formreferrer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formreferrer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld referrer staat en kan
	 * worden bewerkt. Indien referrer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormReferrer($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Referrer', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * referrer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld referrer representeert.
	 */
	public static function opmerkingReferrer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld blockedUri.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld blockedUri labelt.
	 */
	public static function labelBlockedUri(CSPReport $obj)
	{
		return 'BlockedUri';
	}
	/**
	 * @brief Geef de waarde van het veld blockedUri.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld blockedUri van het object obj
	 * representeert.
	 */
	public static function waardeBlockedUri(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'BlockedUri');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld blockedUri.
	 *
	 * @see genericFormblockedUri
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld blockedUri staat en kan
	 * worden bewerkt. Indien blockedUri read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBlockedUri(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'BlockedUri', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld blockedUri. In
	 * tegenstelling tot formblockedUri moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formblockedUri
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld blockedUri staat en kan
	 * worden bewerkt. Indien blockedUri read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBlockedUri($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'BlockedUri', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * blockedUri bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld blockedUri representeert.
	 */
	public static function opmerkingBlockedUri()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld violatedDirective.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld violatedDirective labelt.
	 */
	public static function labelViolatedDirective(CSPReport $obj)
	{
		return 'ViolatedDirective';
	}
	/**
	 * @brief Geef de waarde van het veld violatedDirective.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld violatedDirective van het
	 * object obj representeert.
	 */
	public static function waardeViolatedDirective(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'ViolatedDirective');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld violatedDirective.
	 *
	 * @see genericFormviolatedDirective
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld violatedDirective staat en
	 * kan worden bewerkt. Indien violatedDirective read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formViolatedDirective(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'ViolatedDirective', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld violatedDirective. In
	 * tegenstelling tot formviolatedDirective moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formviolatedDirective
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld violatedDirective staat en
	 * kan worden bewerkt. Indien violatedDirective read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormViolatedDirective($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'ViolatedDirective', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * violatedDirective bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld violatedDirective representeert.
	 */
	public static function opmerkingViolatedDirective()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld originalPolicy.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld originalPolicy labelt.
	 */
	public static function labelOriginalPolicy(CSPReport $obj)
	{
		return 'OriginalPolicy';
	}
	/**
	 * @brief Geef de waarde van het veld originalPolicy.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld originalPolicy van het object
	 * obj representeert.
	 */
	public static function waardeOriginalPolicy(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'OriginalPolicy');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld originalPolicy.
	 *
	 * @see genericFormoriginalPolicy
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld originalPolicy staat en
	 * kan worden bewerkt. Indien originalPolicy read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOriginalPolicy(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'OriginalPolicy', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld originalPolicy. In
	 * tegenstelling tot formoriginalPolicy moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formoriginalPolicy
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld originalPolicy staat en
	 * kan worden bewerkt. Indien originalPolicy read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOriginalPolicy($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'OriginalPolicy', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * originalPolicy bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld originalPolicy representeert.
	 */
	public static function opmerkingOriginalPolicy()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld userAgent.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld userAgent labelt.
	 */
	public static function labelUserAgent(CSPReport $obj)
	{
		return 'UserAgent';
	}
	/**
	 * @brief Geef de waarde van het veld userAgent.
	 *
	 * @param CSPReport $obj Het CSPReport-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld userAgent van het object obj
	 * representeert.
	 */
	public static function waardeUserAgent(CSPReport $obj)
	{
		return static::defaultWaardeString($obj, 'UserAgent');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld userAgent.
	 *
	 * @see genericFormuserAgent
	 *
	 * @param CSPReport $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld userAgent staat en kan
	 * worden bewerkt. Indien userAgent read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUserAgent(CSPReport $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'UserAgent', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld userAgent. In
	 * tegenstelling tot formuserAgent moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuserAgent
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld userAgent staat en kan
	 * worden bewerkt. Indien userAgent read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUserAgent($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'UserAgent', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * userAgent bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld userAgent representeert.
	 */
	public static function opmerkingUserAgent()
	{
		return NULL;
	}
}
