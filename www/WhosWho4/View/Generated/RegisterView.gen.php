<?
abstract class RegisterView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in RegisterView.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeRegister(Register $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Register $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Register $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld label.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld label labelt.
	 */
	public static function labelLabel(Register $obj)
	{
		return 'Label';
	}
	/**
	 * @brief Geef de waarde van het veld label.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld label van het object obj
	 * representeert.
	 */
	public static function waardeLabel(Register $obj)
	{
		return static::defaultWaardeString($obj, 'Label');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld label.
	 *
	 * @see genericFormlabel
	 *
	 * @param Register $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld label staat en kan worden
	 * bewerkt. Indien label read-only is betreft het een statisch html-element.
	 */
	public static function formLabel(Register $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Label', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld label. In
	 * tegenstelling tot formlabel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlabel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld label staat en kan worden
	 * bewerkt. Indien label read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLabel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Label', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld label
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld label representeert.
	 */
	public static function opmerkingLabel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beschrijving.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beschrijving labelt.
	 */
	public static function labelBeschrijving(Register $obj)
	{
		return 'Beschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld beschrijving.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beschrijving van het object
	 * obj representeert.
	 */
	public static function waardeBeschrijving(Register $obj)
	{
		return static::defaultWaardeString($obj, 'Beschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beschrijving.
	 *
	 * @see genericFormbeschrijving
	 *
	 * @param Register $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeschrijving(Register $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Beschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beschrijving. In
	 * tegenstelling tot formbeschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Beschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beschrijving representeert.
	 */
	public static function opmerkingBeschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld waarde.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld waarde labelt.
	 */
	public static function labelWaarde(Register $obj)
	{
		return 'Waarde';
	}
	/**
	 * @brief Geef de waarde van het veld waarde.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld waarde van het object obj
	 * representeert.
	 */
	public static function waardeWaarde(Register $obj)
	{
		return static::defaultWaardeString($obj, 'Waarde');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld waarde.
	 *
	 * @see genericFormwaarde
	 *
	 * @param Register $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waarde staat en kan worden
	 * bewerkt. Indien waarde read-only is betreft het een statisch html-element.
	 */
	public static function formWaarde(Register $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Waarde', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld waarde. In
	 * tegenstelling tot formwaarde moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwaarde
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waarde staat en kan worden
	 * bewerkt. Indien waarde read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormWaarde($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Waarde', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld waarde
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld waarde representeert.
	 */
	public static function opmerkingWaarde()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(Register $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld type.
	 *
	 * @param string $value Een enum-waarde van het veld type.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumType($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld type horen.
	 *
	 * @see labelenumType
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld type
	 * representeren.
	 */
	public static function labelenumTypeArray()
	{
		$soorten = array();
		foreach(Register::enumsType() as $id)
			$soorten[$id] = RegisterView::labelenumType($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(Register $obj)
	{
		return static::defaultWaardeEnum($obj, 'Type');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld type.
	 *
	 * @see genericFormtype
	 *
	 * @param Register $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is betreft het een statisch html-element.
	 */
	public static function formType(Register $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Type', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld type. In tegenstelling
	 * tot formtype moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtype
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormType($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Type', Register::enumstype());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld auth.
	 *
	 * @param Register $obj Het Register-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld auth labelt.
	 */
	public static function labelAuth(Register $obj)
	{
		return 'Auth';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld auth.
	 *
	 * @param string $value Een enum-waarde van het veld auth.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumAuth($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld auth horen.
	 *
	 * @see labelenumAuth
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld auth
	 * representeren.
	 */
	public static function labelenumAuthArray()
	{
		$soorten = array();
		foreach(Register::enumsAuth() as $id)
			$soorten[$id] = RegisterView::labelenumAuth($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld auth.
	 *
	 * @param Register $obj Het Register-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld auth van het object obj
	 * representeert.
	 */
	public static function waardeAuth(Register $obj)
	{
		return static::defaultWaardeEnum($obj, 'Auth');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld auth.
	 *
	 * @see genericFormauth
	 *
	 * @param Register $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auth staat en kan worden
	 * bewerkt. Indien auth read-only is betreft het een statisch html-element.
	 */
	public static function formAuth(Register $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Auth', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld auth. In tegenstelling
	 * tot formauth moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formauth
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auth staat en kan worden
	 * bewerkt. Indien auth read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAuth($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Auth', Register::enumsauth());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld auth
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld auth representeert.
	 */
	public static function opmerkingAuth()
	{
		return NULL;
	}
}
