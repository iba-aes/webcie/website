<?
abstract class DeelnemerVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DeelnemerVerzamelingView.
	 *
	 * @param DeelnemerVerzameling $obj Het DeelnemerVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDeelnemerVerzameling(DeelnemerVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
