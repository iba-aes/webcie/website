<?
abstract class ActiviteitHerhalingView_Generated
	extends ActiviteitView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitHerhalingView.
	 *
	 * @param ActiviteitHerhaling $obj Het ActiviteitHerhaling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitHerhaling(ActiviteitHerhaling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld parent.
	 *
	 * @param ActiviteitHerhaling $obj Het ActiviteitHerhaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld parent labelt.
	 */
	public static function labelParent(ActiviteitHerhaling $obj)
	{
		return 'Parent';
	}
	/**
	 * @brief Geef de waarde van het veld parent.
	 *
	 * @param ActiviteitHerhaling $obj Het ActiviteitHerhaling-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld parent van het object obj
	 * representeert.
	 */
	public static function waardeParent(ActiviteitHerhaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getParent())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getParent());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld parent.
	 *
	 * @see genericFormparent
	 *
	 * @param ActiviteitHerhaling $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld parent staat en kan worden
	 * bewerkt. Indien parent read-only is betreft het een statisch html-element.
	 */
	public static function formParent(ActiviteitHerhaling $obj, $include_id = false)
	{
		return static::waardeParent($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld parent. In
	 * tegenstelling tot formparent moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formparent
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld parent staat en kan worden
	 * bewerkt. Indien parent read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormParent($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld parent
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld parent representeert.
	 */
	public static function opmerkingParent()
	{
		return NULL;
	}
}
