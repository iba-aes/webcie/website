<?
abstract class ContactPersoonView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactPersoonView.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactPersoon(ContactPersoon $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(ContactPersoon $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(ContactPersoon $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld organisatie.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld organisatie labelt.
	 */
	public static function labelOrganisatie(ContactPersoon $obj)
	{
		return 'Organisatie';
	}
	/**
	 * @brief Geef de waarde van het veld organisatie.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld organisatie van het object
	 * obj representeert.
	 */
	public static function waardeOrganisatie(ContactPersoon $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getOrganisatie())
			return NULL;
		return OrganisatieView::defaultWaardeOrganisatie($obj->getOrganisatie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * organisatie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld organisatie representeert.
	 */
	public static function opmerkingOrganisatie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(ContactPersoon $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld type.
	 *
	 * @param string $value Een enum-waarde van het veld type.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumType($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld type horen.
	 *
	 * @see labelenumType
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld type
	 * representeren.
	 */
	public static function labelenumTypeArray()
	{
		$soorten = array();
		foreach(ContactPersoon::enumsType() as $id)
			$soorten[$id] = ContactPersoonView::labelenumType($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(ContactPersoon $obj)
	{
		return static::defaultWaardeEnum($obj, 'Type');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld Functie.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld Functie labelt.
	 */
	public static function labelFunctie(ContactPersoon $obj)
	{
		return 'Functie';
	}
	/**
	 * @brief Geef de waarde van het veld Functie.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Functie van het object obj
	 * representeert.
	 */
	public static function waardeFunctie(ContactPersoon $obj)
	{
		return static::defaultWaardeString($obj, 'Functie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld Functie.
	 *
	 * @see genericFormFunctie
	 *
	 * @param ContactPersoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Functie staat en kan
	 * worden bewerkt. Indien Functie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formFunctie(ContactPersoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Functie', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld Functie. In
	 * tegenstelling tot formFunctie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formFunctie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Functie staat en kan
	 * worden bewerkt. Indien Functie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormFunctie($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Functie', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld Functie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Functie representeert.
	 */
	public static function opmerkingFunctie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beginDatum.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld beginDatum labelt.
	 */
	public static function labelBeginDatum(ContactPersoon $obj)
	{
		return 'BeginDatum';
	}
	/**
	 * @brief Geef de waarde van het veld beginDatum.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beginDatum van het object obj
	 * representeert.
	 */
	public static function waardeBeginDatum(ContactPersoon $obj)
	{
		return static::defaultWaardeDate($obj, 'BeginDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beginDatum.
	 *
	 * @see genericFormbeginDatum
	 *
	 * @param ContactPersoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginDatum staat en kan
	 * worden bewerkt. Indien beginDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeginDatum(ContactPersoon $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'BeginDatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beginDatum. In
	 * tegenstelling tot formbeginDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeginDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginDatum staat en kan
	 * worden bewerkt. Indien beginDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeginDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'BeginDatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beginDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beginDatum representeert.
	 */
	public static function opmerkingBeginDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld eindDatum.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld eindDatum labelt.
	 */
	public static function labelEindDatum(ContactPersoon $obj)
	{
		return 'EindDatum';
	}
	/**
	 * @brief Geef de waarde van het veld eindDatum.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld eindDatum van het object obj
	 * representeert.
	 */
	public static function waardeEindDatum(ContactPersoon $obj)
	{
		return static::defaultWaardeDate($obj, 'EindDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld eindDatum.
	 *
	 * @see genericFormeindDatum
	 *
	 * @param ContactPersoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindDatum staat en kan
	 * worden bewerkt. Indien eindDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEindDatum(ContactPersoon $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'EindDatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld eindDatum. In
	 * tegenstelling tot formeindDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formeindDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindDatum staat en kan
	 * worden bewerkt. Indien eindDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEindDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'EindDatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * eindDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld eindDatum representeert.
	 */
	public static function opmerkingEindDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(ContactPersoon $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param ContactPersoon $obj Het ContactPersoon-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(ContactPersoon $obj)
	{
		return static::defaultWaardeString($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param ContactPersoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(ContactPersoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
}
