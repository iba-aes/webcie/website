<?
abstract class ActiviteitCategorieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitCategorieVerzamelingView.
	 *
	 * @param ActiviteitCategorieVerzameling $obj Het
	 * ActiviteitCategorieVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitCategorieVerzameling(ActiviteitCategorieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
