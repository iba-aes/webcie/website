<?
abstract class ContactAdresVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactAdresVerzamelingView.
	 *
	 * @param ContactAdresVerzameling $obj Het ContactAdresVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactAdresVerzameling(ContactAdresVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
