<?
abstract class CommissieLidVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CommissieLidVerzamelingView.
	 *
	 * @param CommissieLidVerzameling $obj Het CommissieLidVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieLidVerzameling(CommissieLidVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
