<?
abstract class PlannerVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerVerzamelingView.
	 *
	 * @param PlannerVerzameling $obj Het PlannerVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlannerVerzameling(PlannerVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
