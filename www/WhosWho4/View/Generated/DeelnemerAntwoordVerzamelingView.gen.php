<?
abstract class DeelnemerAntwoordVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DeelnemerAntwoordVerzamelingView.
	 *
	 * @param DeelnemerAntwoordVerzameling $obj Het DeelnemerAntwoordVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDeelnemerAntwoordVerzameling(DeelnemerAntwoordVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
