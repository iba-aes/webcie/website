<?
abstract class StudieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in StudieView.
	 *
	 * @param Studie $obj Het Studie-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeStudie(Studie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld studieID.
	 *
	 * @param Studie $obj Het Studie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studieID labelt.
	 */
	public static function labelStudieID(Studie $obj)
	{
		return 'StudieID';
	}
	/**
	 * @brief Geef de waarde van het veld studieID.
	 *
	 * @param Studie $obj Het Studie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studieID van het object obj
	 * representeert.
	 */
	public static function waardeStudieID(Studie $obj)
	{
		return static::defaultWaardeInt($obj, 'StudieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * studieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studieID representeert.
	 */
	public static function opmerkingStudieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Studie $obj Het Studie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Studie $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Studie $obj Het Studie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Studie $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Studie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Studie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Naam', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Naam', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Naam', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Naam', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Studie $obj Het Studie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Studie $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(Studie::enumsSoort() as $id)
			$soorten[$id] = StudieView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Studie $obj Het Studie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Studie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Studie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Studie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', Studie::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld fase.
	 *
	 * @param Studie $obj Het Studie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld fase labelt.
	 */
	public static function labelFase(Studie $obj)
	{
		return 'Fase';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld fase.
	 *
	 * @param string $value Een enum-waarde van het veld fase.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumFase($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld fase horen.
	 *
	 * @see labelenumFase
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld fase
	 * representeren.
	 */
	public static function labelenumFaseArray()
	{
		$soorten = array();
		foreach(Studie::enumsFase() as $id)
			$soorten[$id] = StudieView::labelenumFase($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld fase.
	 *
	 * @param Studie $obj Het Studie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld fase van het object obj
	 * representeert.
	 */
	public static function waardeFase(Studie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Fase');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld fase.
	 *
	 * @see genericFormfase
	 *
	 * @param Studie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld fase staat en kan worden
	 * bewerkt. Indien fase read-only is betreft het een statisch html-element.
	 */
	public static function formFase(Studie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Fase', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld fase. In tegenstelling
	 * tot formfase moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formfase
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld fase staat en kan worden
	 * bewerkt. Indien fase read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormFase($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Fase', Studie::enumsfase());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld fase
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld fase representeert.
	 */
	public static function opmerkingFase()
	{
		return NULL;
	}
}
