<?
abstract class ContactView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactView.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContact(Contact $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld contactID.
	 *
	 * @param Contact $obj Het Contact-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contactID labelt.
	 */
	public static function labelContactID(Contact $obj)
	{
		return 'ContactID';
	}
	/**
	 * @brief Geef de waarde van het veld contactID.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contactID van het object obj
	 * representeert.
	 */
	public static function waardeContactID(Contact $obj)
	{
		return static::defaultWaardeInt($obj, 'ContactID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contactID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contactID representeert.
	 */
	public static function opmerkingContactID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld email.
	 *
	 * @param Contact $obj Het Contact-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld email labelt.
	 */
	public static function labelEmail(Contact $obj)
	{
		return 'Email';
	}
	/**
	 * @brief Geef de waarde van het veld email.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld email van het object obj
	 * representeert.
	 */
	public static function waardeEmail(Contact $obj)
	{
		return static::defaultWaardeString($obj, 'Email');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld email.
	 *
	 * @see genericFormemail
	 *
	 * @param Contact $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld email staat en kan worden
	 * bewerkt. Indien email read-only is betreft het een statisch html-element.
	 */
	public static function formEmail(Contact $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Email', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld email. In
	 * tegenstelling tot formemail moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formemail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld email staat en kan worden
	 * bewerkt. Indien email read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormEmail($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Email', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld email
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld email representeert.
	 */
	public static function opmerkingEmail()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld homepage.
	 *
	 * @param Contact $obj Het Contact-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld homepage labelt.
	 */
	public static function labelHomepage(Contact $obj)
	{
		return 'Homepage';
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld homepage van het object obj
	 * representeert.
	 */
	public static function waardeHomepage(Contact $obj)
	{
		return static::defaultWaardeUrl($obj, 'Homepage');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld homepage.
	 *
	 * @see genericFormhomepage
	 *
	 * @param Contact $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHomepage(Contact $obj, $include_id = false)
	{
		return static::defaultFormUrl($obj, 'Homepage', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld homepage. In
	 * tegenstelling tot formhomepage moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formhomepage
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHomepage($name, $waarde=NULL)
	{
		return static::genericDefaultFormUrl($name, $waarde, 'Homepage');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * homepage bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld homepage representeert.
	 */
	public static function opmerkingHomepage()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vakidOpsturen.
	 *
	 * @param Contact $obj Het Contact-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vakidOpsturen labelt.
	 */
	public static function labelVakidOpsturen(Contact $obj)
	{
		return 'VakidOpsturen';
	}
	/**
	 * @brief Geef de waarde van het veld vakidOpsturen.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vakidOpsturen van het object
	 * obj representeert.
	 */
	public static function waardeVakidOpsturen(Contact $obj)
	{
		return static::defaultWaardeBool($obj, 'VakidOpsturen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vakidOpsturen.
	 *
	 * @see genericFormvakidOpsturen
	 *
	 * @param Contact $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vakidOpsturen staat en kan
	 * worden bewerkt. Indien vakidOpsturen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVakidOpsturen(Contact $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'VakidOpsturen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vakidOpsturen. In
	 * tegenstelling tot formvakidOpsturen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvakidOpsturen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vakidOpsturen staat en kan
	 * worden bewerkt. Indien vakidOpsturen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVakidOpsturen($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'VakidOpsturen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * vakidOpsturen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vakidOpsturen representeert.
	 */
	public static function opmerkingVakidOpsturen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aes2rootsOpsturen.
	 *
	 * @param Contact $obj Het Contact-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aes2rootsOpsturen labelt.
	 */
	public static function labelAes2rootsOpsturen(Contact $obj)
	{
		return 'Aes2rootsOpsturen';
	}
	/**
	 * @brief Geef de waarde van het veld aes2rootsOpsturen.
	 *
	 * @param Contact $obj Het Contact-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aes2rootsOpsturen van het
	 * object obj representeert.
	 */
	public static function waardeAes2rootsOpsturen(Contact $obj)
	{
		return static::defaultWaardeBool($obj, 'Aes2rootsOpsturen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aes2rootsOpsturen.
	 *
	 * @see genericFormaes2rootsOpsturen
	 *
	 * @param Contact $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aes2rootsOpsturen staat en
	 * kan worden bewerkt. Indien aes2rootsOpsturen read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formAes2rootsOpsturen(Contact $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Aes2rootsOpsturen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aes2rootsOpsturen. In
	 * tegenstelling tot formaes2rootsOpsturen moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formaes2rootsOpsturen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aes2rootsOpsturen staat en
	 * kan worden bewerkt. Indien aes2rootsOpsturen read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormAes2rootsOpsturen($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Aes2rootsOpsturen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * aes2rootsOpsturen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aes2rootsOpsturen representeert.
	 */
	public static function opmerkingAes2rootsOpsturen()
	{
		return NULL;
	}
}
