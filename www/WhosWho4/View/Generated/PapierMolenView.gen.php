<?
abstract class PapierMolenView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PapierMolenView.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePapierMolen(PapierMolen $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld papierMolenID.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld papierMolenID labelt.
	 */
	public static function labelPapierMolenID(PapierMolen $obj)
	{
		return 'PapierMolenID';
	}
	/**
	 * @brief Geef de waarde van het veld papierMolenID.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld papierMolenID van het object
	 * obj representeert.
	 */
	public static function waardePapierMolenID(PapierMolen $obj)
	{
		return static::defaultWaardeInt($obj, 'PapierMolenID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * papierMolenID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld papierMolenID representeert.
	 */
	public static function opmerkingPapierMolenID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld geplaatst.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld geplaatst labelt.
	 */
	public static function labelGeplaatst(PapierMolen $obj)
	{
		return 'Geplaatst';
	}
	/**
	 * @brief Geef de waarde van het veld geplaatst.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld geplaatst van het object obj
	 * representeert.
	 */
	public static function waardeGeplaatst(PapierMolen $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Geplaatst');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld geplaatst.
	 *
	 * @see genericFormgeplaatst
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geplaatst staat en kan
	 * worden bewerkt. Indien geplaatst read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGeplaatst(PapierMolen $obj, $include_id = false)
	{
		return static::waardeGeplaatst($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld geplaatst. In
	 * tegenstelling tot formgeplaatst moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgeplaatst
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geplaatst staat en kan
	 * worden bewerkt. Indien geplaatst read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGeplaatst($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * geplaatst bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld geplaatst representeert.
	 */
	public static function opmerkingGeplaatst()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(PapierMolen $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(PapierMolen $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lid.
	 *
	 * @see genericFormlid
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lid staat en kan worden
	 * bewerkt. Indien lid read-only is betreft het een statisch html-element.
	 */
	public static function formLid(PapierMolen $obj, $include_id = false)
	{
		return LidView::defaultForm($obj->getLid());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lid. In tegenstelling
	 * tot formlid moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formlid
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lid staat en kan worden
	 * bewerkt. Indien lid read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLid($name, $waarde=NULL)
	{
		return LidView::genericDefaultForm('Lid');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ean.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ean labelt.
	 */
	public static function labelEan(PapierMolen $obj)
	{
		return 'Ean';
	}
	/**
	 * @brief Geef de waarde van het veld ean.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ean van het object obj
	 * representeert.
	 */
	public static function waardeEan(PapierMolen $obj)
	{
		return static::defaultWaardeInt($obj, 'Ean');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ean.
	 *
	 * @see genericFormean
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ean staat en kan worden
	 * bewerkt. Indien ean read-only is betreft het een statisch html-element.
	 */
	public static function formEan(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Ean', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ean. In tegenstelling
	 * tot formean moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formean
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ean staat en kan worden
	 * bewerkt. Indien ean read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormEan($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Ean');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ean
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ean representeert.
	 */
	public static function opmerkingEan()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld auteur.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld auteur labelt.
	 */
	public static function labelAuteur(PapierMolen $obj)
	{
		return 'Auteur';
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld auteur van het object obj
	 * representeert.
	 */
	public static function waardeAuteur(PapierMolen $obj)
	{
		return static::defaultWaardeString($obj, 'Auteur');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld auteur.
	 *
	 * @see genericFormauteur
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is betreft het een statisch html-element.
	 */
	public static function formAuteur(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Auteur', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld auteur. In
	 * tegenstelling tot formauteur moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formauteur
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAuteur($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Auteur', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld auteur
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld auteur representeert.
	 */
	public static function opmerkingAuteur()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(PapierMolen $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(PapierMolen $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld druk.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld druk labelt.
	 */
	public static function labelDruk(PapierMolen $obj)
	{
		return 'Druk';
	}
	/**
	 * @brief Geef de waarde van het veld druk.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld druk van het object obj
	 * representeert.
	 */
	public static function waardeDruk(PapierMolen $obj)
	{
		return static::defaultWaardeString($obj, 'Druk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld druk.
	 *
	 * @see genericFormdruk
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld druk staat en kan worden
	 * bewerkt. Indien druk read-only is betreft het een statisch html-element.
	 */
	public static function formDruk(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Druk', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld druk. In tegenstelling
	 * tot formdruk moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formdruk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld druk staat en kan worden
	 * bewerkt. Indien druk read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormDruk($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Druk', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld druk
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld druk representeert.
	 */
	public static function opmerkingDruk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prijs.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld prijs labelt.
	 */
	public static function labelPrijs(PapierMolen $obj)
	{
		return 'Prijs';
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prijs van het object obj
	 * representeert.
	 */
	public static function waardePrijs(PapierMolen $obj)
	{
		return static::defaultWaardeMoney($obj, 'Prijs');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prijs.
	 *
	 * @see genericFormprijs
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is betreft het een statisch html-element.
	 */
	public static function formPrijs(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Prijs', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prijs. In
	 * tegenstelling tot formprijs moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formprijs
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPrijs($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Prijs');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld prijs
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prijs representeert.
	 */
	public static function opmerkingPrijs()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(PapierMolen $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(PapierMolen $obj)
	{
		return static::defaultWaardeString($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld studie.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studie labelt.
	 */
	public static function labelStudie(PapierMolen $obj)
	{
		return 'Studie';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld studie.
	 *
	 * @param string $value Een enum-waarde van het veld studie.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStudie($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld studie horen.
	 *
	 * @see labelenumStudie
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld studie
	 * representeren.
	 */
	public static function labelenumStudieArray()
	{
		$soorten = array();
		foreach(PapierMolen::enumsStudie() as $id)
			$soorten[$id] = PapierMolenView::labelenumStudie($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studie van het object obj
	 * representeert.
	 */
	public static function waardeStudie(PapierMolen $obj)
	{
		return static::defaultWaardeEnum($obj, 'Studie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld studie.
	 *
	 * @see genericFormstudie
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studie staat en kan worden
	 * bewerkt. Indien studie read-only is betreft het een statisch html-element.
	 */
	public static function formStudie(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Studie', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld studie. In
	 * tegenstelling tot formstudie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstudie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studie staat en kan worden
	 * bewerkt. Indien studie read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStudie($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Studie', PapierMolen::enumsstudie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld studie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studie representeert.
	 */
	public static function opmerkingStudie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld verloopdatum.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld verloopdatum labelt.
	 */
	public static function labelVerloopdatum(PapierMolen $obj)
	{
		return 'Verloopdatum';
	}
	/**
	 * @brief Geef de waarde van het veld verloopdatum.
	 *
	 * @param PapierMolen $obj Het PapierMolen-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verloopdatum van het object
	 * obj representeert.
	 */
	public static function waardeVerloopdatum(PapierMolen $obj)
	{
		return static::defaultWaardeDate($obj, 'Verloopdatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld verloopdatum.
	 *
	 * @see genericFormverloopdatum
	 *
	 * @param PapierMolen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verloopdatum staat en kan
	 * worden bewerkt. Indien verloopdatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVerloopdatum(PapierMolen $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'Verloopdatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld verloopdatum. In
	 * tegenstelling tot formverloopdatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formverloopdatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verloopdatum staat en kan
	 * worden bewerkt. Indien verloopdatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVerloopdatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'Verloopdatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verloopdatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verloopdatum representeert.
	 */
	public static function opmerkingVerloopdatum()
	{
		return NULL;
	}
}
