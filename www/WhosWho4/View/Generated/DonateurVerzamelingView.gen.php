<?
abstract class DonateurVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DonateurVerzamelingView.
	 *
	 * @param DonateurVerzameling $obj Het DonateurVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDonateurVerzameling(DonateurVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
