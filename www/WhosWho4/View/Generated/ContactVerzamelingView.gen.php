<?
abstract class ContactVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactVerzamelingView.
	 *
	 * @param ContactVerzameling $obj Het ContactVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactVerzameling(ContactVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
