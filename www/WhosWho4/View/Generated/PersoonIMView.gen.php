<?
abstract class PersoonIMView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonIMView.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonIM(PersoonIM $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(PersoonIM $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(PersoonIM $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(PersoonIM $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(PersoonIM::enumsSoort() as $id)
			$soorten[$id] = PersoonIMView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(PersoonIM $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld accountName.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld accountName labelt.
	 */
	public static function labelAccountName(PersoonIM $obj)
	{
		return 'AccountName';
	}
	/**
	 * @brief Geef de waarde van het veld accountName.
	 *
	 * @param PersoonIM $obj Het PersoonIM-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld accountName van het object
	 * obj representeert.
	 */
	public static function waardeAccountName(PersoonIM $obj)
	{
		return static::defaultWaardeString($obj, 'AccountName');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld accountName.
	 *
	 * @see genericFormaccountName
	 *
	 * @param PersoonIM $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld accountName staat en kan
	 * worden bewerkt. Indien accountName read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAccountName(PersoonIM $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'AccountName', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld accountName. In
	 * tegenstelling tot formaccountName moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formaccountName
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld accountName staat en kan
	 * worden bewerkt. Indien accountName read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAccountName($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'AccountName', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * accountName bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld accountName representeert.
	 */
	public static function opmerkingAccountName()
	{
		return NULL;
	}
}
