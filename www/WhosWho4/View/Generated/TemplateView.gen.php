<?
abstract class TemplateView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TemplateView.
	 *
	 * @param Template $obj Het Template-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTemplate(Template $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Template $obj Het Template-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Template $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Template $obj Het Template-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Template $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld label.
	 *
	 * @param Template $obj Het Template-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld label labelt.
	 */
	public static function labelLabel(Template $obj)
	{
		return 'Label';
	}
	/**
	 * @brief Geef de waarde van het veld label.
	 *
	 * @param Template $obj Het Template-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld label van het object obj
	 * representeert.
	 */
	public static function waardeLabel(Template $obj)
	{
		return static::defaultWaardeString($obj, 'Label');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld label.
	 *
	 * @see genericFormlabel
	 *
	 * @param Template $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld label staat en kan worden
	 * bewerkt. Indien label read-only is betreft het een statisch html-element.
	 */
	public static function formLabel(Template $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Label', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld label. In
	 * tegenstelling tot formlabel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlabel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld label staat en kan worden
	 * bewerkt. Indien label read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLabel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Label', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld label
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld label representeert.
	 */
	public static function opmerkingLabel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beschrijving.
	 *
	 * @param Template $obj Het Template-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beschrijving labelt.
	 */
	public static function labelBeschrijving(Template $obj)
	{
		return 'Beschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld beschrijving.
	 *
	 * @param Template $obj Het Template-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beschrijving van het object
	 * obj representeert.
	 */
	public static function waardeBeschrijving(Template $obj)
	{
		return static::defaultWaardeString($obj, 'Beschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beschrijving.
	 *
	 * @see genericFormbeschrijving
	 *
	 * @param Template $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeschrijving(Template $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Beschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beschrijving. In
	 * tegenstelling tot formbeschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Beschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beschrijving representeert.
	 */
	public static function opmerkingBeschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld waarde.
	 *
	 * @param Template $obj Het Template-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld waarde labelt.
	 */
	public static function labelWaarde(Template $obj)
	{
		return 'Waarde';
	}
	/**
	 * @brief Geef de waarde van het veld waarde.
	 *
	 * @param Template $obj Het Template-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld waarde van het object obj
	 * representeert.
	 */
	public static function waardeWaarde(Template $obj)
	{
		return static::defaultWaardeText($obj, 'Waarde');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld waarde.
	 *
	 * @see genericFormwaarde
	 *
	 * @param Template $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waarde staat en kan worden
	 * bewerkt. Indien waarde read-only is betreft het een statisch html-element.
	 */
	public static function formWaarde(Template $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Waarde', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld waarde. In
	 * tegenstelling tot formwaarde moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwaarde
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waarde staat en kan worden
	 * bewerkt. Indien waarde read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormWaarde($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Waarde', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld waarde
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld waarde representeert.
	 */
	public static function opmerkingWaarde()
	{
		return NULL;
	}
}
