<?
abstract class ContactTelnrVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactTelnrVerzamelingView.
	 *
	 * @param ContactTelnrVerzameling $obj Het ContactTelnrVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactTelnrVerzameling(ContactTelnrVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
