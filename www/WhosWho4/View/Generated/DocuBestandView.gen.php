<?
abstract class DocuBestandView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DocuBestandView.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDocuBestand(DocuBestand $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(DocuBestand $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(DocuBestand $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(DocuBestand $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(DocuBestand $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param DocuBestand $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(DocuBestand $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld extensie.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld extensie labelt.
	 */
	public static function labelExtensie(DocuBestand $obj)
	{
		return 'Extensie';
	}
	/**
	 * @brief Geef de waarde van het veld extensie.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld extensie van het object obj
	 * representeert.
	 */
	public static function waardeExtensie(DocuBestand $obj)
	{
		return static::defaultWaardeString($obj, 'Extensie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld extensie.
	 *
	 * @see genericFormextensie
	 *
	 * @param DocuBestand $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld extensie staat en kan
	 * worden bewerkt. Indien extensie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formExtensie(DocuBestand $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Extensie', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld extensie. In
	 * tegenstelling tot formextensie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formextensie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld extensie staat en kan
	 * worden bewerkt. Indien extensie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormExtensie($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Extensie', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * extensie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld extensie representeert.
	 */
	public static function opmerkingExtensie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld categorie.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorie labelt.
	 */
	public static function labelCategorie(DocuBestand $obj)
	{
		return 'Categorie';
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorie van het object obj
	 * representeert.
	 */
	public static function waardeCategorie(DocuBestand $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCategorie())
			return NULL;
		return DocuCategorieView::defaultWaardeDocuCategorie($obj->getCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld categorie.
	 *
	 * @see genericFormcategorie
	 *
	 * @param DocuBestand $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCategorie(DocuBestand $obj, $include_id = false)
	{
		return DocuCategorieView::defaultForm($obj->getCategorie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld categorie. In
	 * tegenstelling tot formcategorie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCategorie($name, $waarde=NULL)
	{
		return DocuCategorieView::genericDefaultForm('Categorie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorie representeert.
	 */
	public static function opmerkingCategorie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datum.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datum labelt.
	 */
	public static function labelDatum(DocuBestand $obj)
	{
		return 'Datum';
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @param DocuBestand $obj Het DocuBestand-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datum van het object obj
	 * representeert.
	 */
	public static function waardeDatum(DocuBestand $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Datum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datum.
	 *
	 * @see genericFormdatum
	 *
	 * @param DocuBestand $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is betreft het een statisch html-element.
	 */
	public static function formDatum(DocuBestand $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Datum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datum. In
	 * tegenstelling tot formdatum moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formdatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Datum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld datum
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datum representeert.
	 */
	public static function opmerkingDatum()
	{
		return NULL;
	}
}
