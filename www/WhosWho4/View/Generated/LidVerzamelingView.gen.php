<?
abstract class LidVerzamelingView_Generated
	extends PersoonVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LidVerzamelingView.
	 *
	 * @param LidVerzameling $obj Het LidVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidVerzameling(LidVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
