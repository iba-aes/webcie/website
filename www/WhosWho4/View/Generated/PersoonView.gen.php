<?
abstract class PersoonView_Generated
	extends ContactView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonView.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoon(Persoon $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld voornaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voornaam labelt.
	 */
	public static function labelVoornaam(Persoon $obj)
	{
		return 'Voornaam';
	}
	/**
	 * @brief Geef de waarde van het veld voornaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voornaam van het object obj
	 * representeert.
	 */
	public static function waardeVoornaam(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Voornaam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voornaam.
	 *
	 * @see genericFormvoornaam
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaam staat en kan
	 * worden bewerkt. Indien voornaam read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoornaam(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Voornaam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voornaam. In
	 * tegenstelling tot formvoornaam moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoornaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaam staat en kan
	 * worden bewerkt. Indien voornaam read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoornaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Voornaam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voornaam bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voornaam representeert.
	 */
	public static function opmerkingVoornaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bijnaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bijnaam labelt.
	 */
	public static function labelBijnaam(Persoon $obj)
	{
		return 'Bijnaam';
	}
	/**
	 * @brief Geef de waarde van het veld bijnaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bijnaam van het object obj
	 * representeert.
	 */
	public static function waardeBijnaam(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Bijnaam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bijnaam.
	 *
	 * @see genericFormbijnaam
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bijnaam staat en kan
	 * worden bewerkt. Indien bijnaam read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBijnaam(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Bijnaam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bijnaam. In
	 * tegenstelling tot formbijnaam moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbijnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bijnaam staat en kan
	 * worden bewerkt. Indien bijnaam read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBijnaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Bijnaam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bijnaam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bijnaam representeert.
	 */
	public static function opmerkingBijnaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorletters.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voorletters labelt.
	 */
	public static function labelVoorletters(Persoon $obj)
	{
		return 'Voorletters';
	}
	/**
	 * @brief Geef de waarde van het veld voorletters.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorletters van het object
	 * obj representeert.
	 */
	public static function waardeVoorletters(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Voorletters');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorletters.
	 *
	 * @see genericFormvoorletters
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorletters staat en kan
	 * worden bewerkt. Indien voorletters read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorletters(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Voorletters', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorletters. In
	 * tegenstelling tot formvoorletters moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorletters
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorletters staat en kan
	 * worden bewerkt. Indien voorletters read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorletters($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Voorletters', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorletters bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorletters representeert.
	 */
	public static function opmerkingVoorletters()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld geboortenamen.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld geboortenamen labelt.
	 */
	public static function labelGeboortenamen(Persoon $obj)
	{
		return 'Geboortenamen';
	}
	/**
	 * @brief Geef de waarde van het veld geboortenamen.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld geboortenamen van het object
	 * obj representeert.
	 */
	public static function waardeGeboortenamen(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Geboortenamen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld geboortenamen.
	 *
	 * @see genericFormgeboortenamen
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geboortenamen staat en kan
	 * worden bewerkt. Indien geboortenamen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGeboortenamen(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Geboortenamen', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld geboortenamen. In
	 * tegenstelling tot formgeboortenamen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgeboortenamen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geboortenamen staat en kan
	 * worden bewerkt. Indien geboortenamen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGeboortenamen($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Geboortenamen', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * geboortenamen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld geboortenamen representeert.
	 */
	public static function opmerkingGeboortenamen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tussenvoegsels.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tussenvoegsels labelt.
	 */
	public static function labelTussenvoegsels(Persoon $obj)
	{
		return 'Tussenvoegsels';
	}
	/**
	 * @brief Geef de waarde van het veld tussenvoegsels.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tussenvoegsels van het object
	 * obj representeert.
	 */
	public static function waardeTussenvoegsels(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Tussenvoegsels');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tussenvoegsels.
	 *
	 * @see genericFormtussenvoegsels
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tussenvoegsels staat en
	 * kan worden bewerkt. Indien tussenvoegsels read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTussenvoegsels(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Tussenvoegsels', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tussenvoegsels. In
	 * tegenstelling tot formtussenvoegsels moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtussenvoegsels
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tussenvoegsels staat en
	 * kan worden bewerkt. Indien tussenvoegsels read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTussenvoegsels($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Tussenvoegsels', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tussenvoegsels bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tussenvoegsels representeert.
	 */
	public static function opmerkingTussenvoegsels()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld achternaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld achternaam labelt.
	 */
	public static function labelAchternaam(Persoon $obj)
	{
		return 'Achternaam';
	}
	/**
	 * @brief Geef de waarde van het veld achternaam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld achternaam van het object obj
	 * representeert.
	 */
	public static function waardeAchternaam(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Achternaam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld achternaam.
	 *
	 * @see genericFormachternaam
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld achternaam staat en kan
	 * worden bewerkt. Indien achternaam read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAchternaam(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Achternaam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld achternaam. In
	 * tegenstelling tot formachternaam moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formachternaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld achternaam staat en kan
	 * worden bewerkt. Indien achternaam read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAchternaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Achternaam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * achternaam bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld achternaam representeert.
	 */
	public static function opmerkingAchternaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titelsPrefix.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titelsPrefix labelt.
	 */
	public static function labelTitelsPrefix(Persoon $obj)
	{
		return 'TitelsPrefix';
	}
	/**
	 * @brief Geef de waarde van het veld titelsPrefix.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titelsPrefix van het object
	 * obj representeert.
	 */
	public static function waardeTitelsPrefix(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'TitelsPrefix');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titelsPrefix.
	 *
	 * @see genericFormtitelsPrefix
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titelsPrefix staat en kan
	 * worden bewerkt. Indien titelsPrefix read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTitelsPrefix(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'TitelsPrefix', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titelsPrefix. In
	 * tegenstelling tot formtitelsPrefix moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtitelsPrefix
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titelsPrefix staat en kan
	 * worden bewerkt. Indien titelsPrefix read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTitelsPrefix($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'TitelsPrefix', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * titelsPrefix bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titelsPrefix representeert.
	 */
	public static function opmerkingTitelsPrefix()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titelsPostfix.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titelsPostfix labelt.
	 */
	public static function labelTitelsPostfix(Persoon $obj)
	{
		return 'TitelsPostfix';
	}
	/**
	 * @brief Geef de waarde van het veld titelsPostfix.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titelsPostfix van het object
	 * obj representeert.
	 */
	public static function waardeTitelsPostfix(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'TitelsPostfix');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titelsPostfix.
	 *
	 * @see genericFormtitelsPostfix
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titelsPostfix staat en kan
	 * worden bewerkt. Indien titelsPostfix read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTitelsPostfix(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'TitelsPostfix', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titelsPostfix. In
	 * tegenstelling tot formtitelsPostfix moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtitelsPostfix
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titelsPostfix staat en kan
	 * worden bewerkt. Indien titelsPostfix read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTitelsPostfix($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'TitelsPostfix', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * titelsPostfix bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titelsPostfix representeert.
	 */
	public static function opmerkingTitelsPostfix()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld geslacht.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld geslacht labelt.
	 */
	public static function labelGeslacht(Persoon $obj)
	{
		return 'Geslacht';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld geslacht.
	 *
	 * @param string $value Een enum-waarde van het veld geslacht.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumGeslacht($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld geslacht horen.
	 *
	 * @see labelenumGeslacht
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld geslacht
	 * representeren.
	 */
	public static function labelenumGeslachtArray()
	{
		$soorten = array();
		foreach(Persoon::enumsGeslacht() as $id)
			$soorten[$id] = PersoonView::labelenumGeslacht($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld geslacht.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld geslacht van het object obj
	 * representeert.
	 */
	public static function waardeGeslacht(Persoon $obj)
	{
		return static::defaultWaardeEnum($obj, 'Geslacht');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld geslacht.
	 *
	 * @see genericFormgeslacht
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geslacht staat en kan
	 * worden bewerkt. Indien geslacht read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGeslacht(Persoon $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Geslacht', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld geslacht. In
	 * tegenstelling tot formgeslacht moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgeslacht
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geslacht staat en kan
	 * worden bewerkt. Indien geslacht read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGeslacht($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Geslacht', Persoon::enumsgeslacht());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * geslacht bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld geslacht representeert.
	 */
	public static function opmerkingGeslacht()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumGeboorte.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumGeboorte labelt.
	 */
	public static function labelDatumGeboorte(Persoon $obj)
	{
		return 'DatumGeboorte';
	}
	/**
	 * @brief Geef de waarde van het veld datumGeboorte.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumGeboorte van het object
	 * obj representeert.
	 */
	public static function waardeDatumGeboorte(Persoon $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumGeboorte');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumGeboorte.
	 *
	 * @see genericFormdatumGeboorte
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeboorte staat en kan
	 * worden bewerkt. Indien datumGeboorte read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumGeboorte(Persoon $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumGeboorte', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumGeboorte. In
	 * tegenstelling tot formdatumGeboorte moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumGeboorte
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeboorte staat en kan
	 * worden bewerkt. Indien datumGeboorte read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumGeboorte($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumGeboorte');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumGeboorte bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumGeboorte representeert.
	 */
	public static function opmerkingDatumGeboorte()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld overleden.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld overleden labelt.
	 */
	public static function labelOverleden(Persoon $obj)
	{
		return 'Overleden';
	}
	/**
	 * @brief Geef de waarde van het veld overleden.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld overleden van het object obj
	 * representeert.
	 */
	public static function waardeOverleden(Persoon $obj)
	{
		return static::defaultWaardeBool($obj, 'Overleden');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld overleden.
	 *
	 * @see genericFormoverleden
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld overleden staat en kan
	 * worden bewerkt. Indien overleden read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOverleden(Persoon $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Overleden', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld overleden. In
	 * tegenstelling tot formoverleden moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formoverleden
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld overleden staat en kan
	 * worden bewerkt. Indien overleden read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOverleden($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Overleden');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * overleden bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld overleden representeert.
	 */
	public static function opmerkingOverleden()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld GPGkey.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld GPGkey labelt.
	 */
	public static function labelGPGkey(Persoon $obj)
	{
		return 'GPGkey';
	}
	/**
	 * @brief Geef de waarde van het veld GPGkey.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld GPGkey van het object obj
	 * representeert.
	 */
	public static function waardeGPGkey(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'GPGkey');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld GPGkey.
	 *
	 * @see genericFormGPGkey
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld GPGkey staat en kan worden
	 * bewerkt. Indien GPGkey read-only is betreft het een statisch html-element.
	 */
	public static function formGPGkey(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'GPGkey', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld GPGkey. In
	 * tegenstelling tot formGPGkey moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formGPGkey
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld GPGkey staat en kan worden
	 * bewerkt. Indien GPGkey read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormGPGkey($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'GPGkey', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld GPGkey
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld GPGkey representeert.
	 */
	public static function opmerkingGPGkey()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld Rekeningnummer.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld Rekeningnummer labelt.
	 */
	public static function labelRekeningnummer(Persoon $obj)
	{
		return 'Rekeningnummer';
	}
	/**
	 * @brief Geef de waarde van het veld Rekeningnummer.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Rekeningnummer van het object
	 * obj representeert.
	 */
	public static function waardeRekeningnummer(Persoon $obj)
	{
		return static::defaultWaardeString($obj, 'Rekeningnummer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld Rekeningnummer.
	 *
	 * @see genericFormRekeningnummer
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Rekeningnummer staat en
	 * kan worden bewerkt. Indien Rekeningnummer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRekeningnummer(Persoon $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Rekeningnummer', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld Rekeningnummer. In
	 * tegenstelling tot formRekeningnummer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formRekeningnummer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Rekeningnummer staat en
	 * kan worden bewerkt. Indien Rekeningnummer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRekeningnummer($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Rekeningnummer', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * Rekeningnummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Rekeningnummer representeert.
	 */
	public static function opmerkingRekeningnummer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerkingen.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerkingen labelt.
	 */
	public static function labelOpmerkingen(Persoon $obj)
	{
		return 'Opmerkingen';
	}
	/**
	 * @brief Geef de waarde van het veld opmerkingen.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerkingen van het object
	 * obj representeert.
	 */
	public static function waardeOpmerkingen(Persoon $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerkingen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerkingen.
	 *
	 * @see genericFormopmerkingen
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerkingen staat en kan
	 * worden bewerkt. Indien opmerkingen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerkingen(Persoon $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerkingen', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerkingen. In
	 * tegenstelling tot formopmerkingen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerkingen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerkingen staat en kan
	 * worden bewerkt. Indien opmerkingen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerkingen($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerkingen', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerkingen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerkingen representeert.
	 */
	public static function opmerkingOpmerkingen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Persoon $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Persoon $obj)
	{
		return static::defaultWaardeText($obj, 'Naam');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voornaamwoord.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voornaamwoord labelt.
	 */
	public static function labelVoornaamwoord(Persoon $obj)
	{
		return 'Voornaamwoord';
	}
	/**
	 * @brief Geef de waarde van het veld voornaamwoord.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voornaamwoord van het object
	 * obj representeert.
	 */
	public static function waardeVoornaamwoord(Persoon $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoornaamwoord())
			return NULL;
		return VoornaamwoordView::defaultWaardeVoornaamwoord($obj->getVoornaamwoord());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voornaamwoord.
	 *
	 * @see genericFormvoornaamwoord
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaamwoord staat en kan
	 * worden bewerkt. Indien voornaamwoord read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoornaamwoord(Persoon $obj, $include_id = false)
	{
		return VoornaamwoordView::defaultForm($obj->getVoornaamwoord());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voornaamwoord. In
	 * tegenstelling tot formvoornaamwoord moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoornaamwoord
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaamwoord staat en kan
	 * worden bewerkt. Indien voornaamwoord read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoornaamwoord($name, $waarde=NULL)
	{
		return VoornaamwoordView::genericDefaultForm('Voornaamwoord');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voornaamwoord bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voornaamwoord representeert.
	 */
	public static function opmerkingVoornaamwoord()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voornaamwoordZichtbaar.
	 *
	 * @param Persoon $obj Het Persoon-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voornaamwoordZichtbaar labelt.
	 */
	public static function labelVoornaamwoordZichtbaar(Persoon $obj)
	{
		return 'VoornaamwoordZichtbaar';
	}
	/**
	 * @brief Geef de waarde van het veld voornaamwoordZichtbaar.
	 *
	 * @param Persoon $obj Het Persoon-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voornaamwoordZichtbaar van
	 * het object obj representeert.
	 */
	public static function waardeVoornaamwoordZichtbaar(Persoon $obj)
	{
		return static::defaultWaardeBool($obj, 'VoornaamwoordZichtbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld
	 * voornaamwoordZichtbaar.
	 *
	 * @see genericFormvoornaamwoordZichtbaar
	 *
	 * @param Persoon $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaamwoordZichtbaar
	 * staat en kan worden bewerkt. Indien voornaamwoordZichtbaar read-only is betreft
	 * het een statisch html-element.
	 */
	public static function formVoornaamwoordZichtbaar(Persoon $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'VoornaamwoordZichtbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld
	 * voornaamwoordZichtbaar. In tegenstelling tot formvoornaamwoordZichtbaar moeten
	 * naam en waarde meegegeven worden, en worden niet uit het object geladen.
	 *
	 * @see formvoornaamwoordZichtbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voornaamwoordZichtbaar
	 * staat en kan worden bewerkt. Indien voornaamwoordZichtbaar read-only is, betreft
	 * het een statisch html-element.
	 */
	public static function genericFormVoornaamwoordZichtbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'VoornaamwoordZichtbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voornaamwoordZichtbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voornaamwoordZichtbaar representeert.
	 */
	public static function opmerkingVoornaamwoordZichtbaar()
	{
		return NULL;
	}
}
