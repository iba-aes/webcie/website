<?
abstract class TemplateVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TemplateVerzamelingView.
	 *
	 * @param TemplateVerzameling $obj Het TemplateVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTemplateVerzameling(TemplateVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
