<?
abstract class StudieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in StudieVerzamelingView.
	 *
	 * @param StudieVerzameling $obj Het StudieVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeStudieVerzameling(StudieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
