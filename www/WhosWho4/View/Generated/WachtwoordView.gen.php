<?
abstract class WachtwoordView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in WachtwoordView.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeWachtwoord(Wachtwoord $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Wachtwoord $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Wachtwoord $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld login.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld login labelt.
	 */
	public static function labelLogin(Wachtwoord $obj)
	{
		return 'Login';
	}
	/**
	 * @brief Geef de waarde van het veld login.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld login van het object obj
	 * representeert.
	 */
	public static function waardeLogin(Wachtwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Login');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld login.
	 *
	 * @see genericFormlogin
	 *
	 * @param Wachtwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld login staat en kan worden
	 * bewerkt. Indien login read-only is betreft het een statisch html-element.
	 */
	public static function formLogin(Wachtwoord $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Login', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld login. In
	 * tegenstelling tot formlogin moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlogin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld login staat en kan worden
	 * bewerkt. Indien login read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLogin($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Login', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld login
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld login representeert.
	 */
	public static function opmerkingLogin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld laatsteLogin.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld laatsteLogin labelt.
	 */
	public static function labelLaatsteLogin(Wachtwoord $obj)
	{
		return 'LaatsteLogin';
	}
	/**
	 * @brief Geef de waarde van het veld laatsteLogin.
	 *
	 * @param Wachtwoord $obj Het Wachtwoord-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld laatsteLogin van het object
	 * obj representeert.
	 */
	public static function waardeLaatsteLogin(Wachtwoord $obj)
	{
		return static::defaultWaardeDate($obj, 'LaatsteLogin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld laatsteLogin.
	 *
	 * @see genericFormlaatsteLogin
	 *
	 * @param Wachtwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld laatsteLogin staat en kan
	 * worden bewerkt. Indien laatsteLogin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLaatsteLogin(Wachtwoord $obj, $include_id = false)
	{
		return static::waardeLaatsteLogin($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld laatsteLogin. In
	 * tegenstelling tot formlaatsteLogin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formlaatsteLogin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld laatsteLogin staat en kan
	 * worden bewerkt. Indien laatsteLogin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLaatsteLogin($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * laatsteLogin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld laatsteLogin representeert.
	 */
	public static function opmerkingLaatsteLogin()
	{
		return NULL;
	}
}
