<?
abstract class TentamenUitwerkingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TentamenUitwerkingView.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTentamenUitwerking(TentamenUitwerking $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld uitwerkingID.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitwerkingID labelt.
	 */
	public static function labelUitwerkingID(TentamenUitwerking $obj)
	{
		return 'UitwerkingID';
	}
	/**
	 * @brief Geef de waarde van het veld uitwerkingID.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitwerkingID van het object
	 * obj representeert.
	 */
	public static function waardeUitwerkingID(TentamenUitwerking $obj)
	{
		return static::defaultWaardeInt($obj, 'UitwerkingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uitwerkingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitwerkingID representeert.
	 */
	public static function opmerkingUitwerkingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tentamen.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tentamen labelt.
	 */
	public static function labelTentamen(TentamenUitwerking $obj)
	{
		return 'Tentamen';
	}
	/**
	 * @brief Geef de waarde van het veld tentamen.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tentamen van het object obj
	 * representeert.
	 */
	public static function waardeTentamen(TentamenUitwerking $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTentamen())
			return NULL;
		return TentamenView::defaultWaardeTentamen($obj->getTentamen());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tentamen.
	 *
	 * @see genericFormtentamen
	 *
	 * @param TentamenUitwerking $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamen staat en kan
	 * worden bewerkt. Indien tentamen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTentamen(TentamenUitwerking $obj, $include_id = false)
	{
		return static::waardeTentamen($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tentamen. In
	 * tegenstelling tot formtentamen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtentamen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamen staat en kan
	 * worden bewerkt. Indien tentamen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTentamen($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tentamen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tentamen representeert.
	 */
	public static function opmerkingTentamen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uploader.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uploader labelt.
	 */
	public static function labelUploader(TentamenUitwerking $obj)
	{
		return 'Uploader';
	}
	/**
	 * @brief Geef de waarde van het veld uploader.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uploader van het object obj
	 * representeert.
	 */
	public static function waardeUploader(TentamenUitwerking $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getUploader())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getUploader());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uploader.
	 *
	 * @see genericFormuploader
	 *
	 * @param TentamenUitwerking $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uploader staat en kan
	 * worden bewerkt. Indien uploader read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUploader(TentamenUitwerking $obj, $include_id = false)
	{
		return static::waardeUploader($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uploader. In
	 * tegenstelling tot formuploader moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuploader
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uploader staat en kan
	 * worden bewerkt. Indien uploader read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUploader($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uploader bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uploader representeert.
	 */
	public static function opmerkingUploader()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(TentamenUitwerking $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(TentamenUitwerking $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wanneer.
	 *
	 * @see genericFormwanneer
	 *
	 * @param TentamenUitwerking $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWanneer(TentamenUitwerking $obj, $include_id = false)
	{
		return static::waardeWanneer($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wanneer. In
	 * tegenstelling tot formwanneer moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwanneer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWanneer($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(TentamenUitwerking $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(TentamenUitwerking $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param TentamenUitwerking $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(TentamenUitwerking $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld gecontroleerd.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld gecontroleerd labelt.
	 */
	public static function labelGecontroleerd(TentamenUitwerking $obj)
	{
		return 'Gecontroleerd';
	}
	/**
	 * @brief Geef de waarde van het veld gecontroleerd.
	 *
	 * @param TentamenUitwerking $obj Het TentamenUitwerking-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld gecontroleerd van het object
	 * obj representeert.
	 */
	public static function waardeGecontroleerd(TentamenUitwerking $obj)
	{
		return static::defaultWaardeBool($obj, 'Gecontroleerd');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld gecontroleerd.
	 *
	 * @see genericFormgecontroleerd
	 *
	 * @param TentamenUitwerking $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gecontroleerd staat en kan
	 * worden bewerkt. Indien gecontroleerd read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGecontroleerd(TentamenUitwerking $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Gecontroleerd', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld gecontroleerd. In
	 * tegenstelling tot formgecontroleerd moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgecontroleerd
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gecontroleerd staat en kan
	 * worden bewerkt. Indien gecontroleerd read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGecontroleerd($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Gecontroleerd');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * gecontroleerd bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld gecontroleerd representeert.
	 */
	public static function opmerkingGecontroleerd()
	{
		return NULL;
	}
}
