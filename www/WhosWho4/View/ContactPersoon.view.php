<?php
/**
 * $Id$
 */
abstract class ContactPersoonView
	extends ContactPersoonView_Generated
{
	static public function detailsPagina(ContactPersoon $cpers)
	{
		$pers = $cpers->getPersoon();

		$page = static::pageLinks($cpers);

		$page->start(PersoonView::naam($pers) . "; " . $cpers->getFunctie()); 

		/** Ga naar de view **/
		$page->add(PersoonView::viewInfo($pers, true));
		$page->add(ContactPersoonView::details($cpers));

		return $page;
	}

	static public function detailsDiv (ContactPersoon $persoon)
	{
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(3, self::waardeFunctie($persoon)))
			->add(ContactPersoonView::makeLink($persoon));

		return $div;
	}

	static public function details(ContactPersoon $persoon)
	{
		$div = new HtmlDiv();

		if(!isSpook($persoon) && !isBedrijfscontact($persoon) && !isOverigSpookweb($persoon) && !isAlumnus($persoon))
		{
			$div->add(ContactPersoonView::viewInfo($persoon, true))
				->add($btn_group = new HtmlDiv(null, 'btn-group'));
			$btn_group->add(HtmlAnchor::button($persoon->getOrganisatie()->url(), _('Naar organisatie')))
					  ->add(HtmlAnchor::button($persoon->getPersoon()->url(), _('Naar persoon')));
		}
		else
		{
			$type = isBedrijfscontact($persoon) ? _("Bedrijfscontact") : _("Spook");
			$table = new HtmlDiv(null, 'info-table');

			$table->add(self::infoTR($persoon, 'Organisatie'))
				->add(self::infoTR($persoon, 'Persoon'))
				->add(self::infoTR($persoon, 'Begindatum'))
				->add(self::infoTR($persoon, 'Einddatum'))
				->add(self::infoTR($persoon, 'Type'))
				->add(self::infoTR($persoon, 'Opmerking'));

			if(isBedrijfscontact($persoon) || isAlumnus($persoon))
			{
				$mobiel = new ContactTelnr($persoon->getPersoon());
				$mobielverz = ContactTelnrVerzameling::vanContact($persoon->getPersoon(), 'MOBIEL');
				if ($mobielverz->aantal() > 0)
					$mobiel = $mobielverz->first();

				$werk = new ContactTelnr($persoon->getPersoon());
				$werkverz = ContactTelnrVerzameling::vanContact($persoon->getPersoon(), 'WERK');
				if ($werkverz->aantal() > 0)
					$werk = $werkverz->first();

				$table->add(self::infoTRData(_('Mobiel nummer'), ContactTelnrView::waardeTelefoonnummer($mobiel)))
					  ->add(self::infoTRData(_('Werk nummer'), ContactTelnrView::waardeTelefoonnummer($werk)))
					  ->add(self::infoTRData(_('Email'), ContactView::waardeEmail($persoon->getPersoon())))
					  ->add(self::infoTRData(_('Vakidioot'), ContactView::waardeVakidOpsturen($persoon->getPersoon())));
			}

			$div->add(new HtmlDiv(new HtmlDiv($table, 'panel-body'), 'panel panel-default'))
				->add($btn_group = new HtmlDiv(null, 'btn-group'));
			$btn_group->add(HtmlAnchor::button($persoon->getOrganisatie()->url(), _('Naar bedrijf')))
				->add(HtmlAnchor::button($persoon->getPersoon()->url(), _('Naar persoon')));

			if(isBedrijfscontact($persoon))
				$interactie = InteractieVerzameling::metConstraints(null, null, null, $persoon->getPersoonContactID(), $persoon->getOrganisatieContactID());
			if(isSpook($persoon) || isOverigSpookweb($persoon) || isAlumnus($persoon))
				$interactie = InteractieVerzameling::metConstraints(null, null, $persoon->getPersoonContactID(), null, $persoon->getOrganisatieContactID());

			$div->add(new HtmlHeader(3, _("Alle interacties van dit $type")))
				->add(new HtmlHR())
				->add(InteractieVerzamelingView::htmlList($interactie));
		}
		return $div;
	}

	/**
	 * Toont het formulier voor het wijzigen van een contactpersoon.
	 */
	static public function wijzig ($cp, $show_error)
	{
		if($cp->getIndb()) {
			$wat = ContactPersoonView::waardeFunctie($cp);
		} else {
			$wat = _("Nieuw") . " " . ContactPersoonView::waardeType($cp);
		}

		$page = static::pageLinks($cp);
		$page->start("$wat " . PersoonView::naam($cp->getPersoon())
			. " van " . OrganisatieView::waardeNaam($cp->getOrganisatie()));

		$page->addFooterJS(Page::minifiedFile('knockout.js'))
			 ->addFooterJS(Page::minifiedFile('TelefoonNummers.js'));

		$page->add(self::wijzigForm('ContactPersoonWijzig', $cp, $show_error));
		$page->end();

	}

	/**
	 * @brief Geef een HtmlAnchor die je naar de pagina voor deze ContactPersoon stuurt.
	 *
	 * Als de ContactPersoon niet bekeken mag worden door de gebruiker,
	 * wordt het gewoon niet-klikbare platte tekst.
	 *
	 * @param cpers De ContactPersoon om de link van te maken.
	 * @param caption De tekst waar je op kan klikken. Default: de naam van de ContactPersoon.
	 * @param link De URL waar je heengaat bij klikken. Default: de infopagina van de ContactPersoon.
	 *
	 * @return Een HtmlElement of een html-veilige string.
	 */
	public static function makeLink (ContactPersoon $cpers, $caption = null, $link = null)
	{
		$pers = $cpers->getPersoon();

		if (is_null($link))
		{
			$link = "Contactpersonen/" . $cpers->getPersoonContactID();
		}

		return PersoonView::makeLink($pers, $caption, $link);
	}

	public static function wijzigForm($name = 'WijzigForm', $cp, $show_error = true)
	{
		$persoon = $cp->getPersoon();
		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv());

		if(!isSpook($cp) && !isOverigSpookweb($cp) && !isAlumnus($cp))
		{
			$div->add(PersoonView::wijzigTR($persoon, 'Voornaam', $show_error));
			$div->add(PersoonView::wijzigTR($persoon, 'Tussenvoegsels', $show_error));
			$div->add(PersoonView::wijzigTR($persoon, 'Achternaam', $show_error));
			$div->add(PersoonView::wijzigTR($persoon, 'Email', $show_error));
		}
		else if(!$cp->getIndb())
		{
			$div->add(self::wijzigTR($cp, 'Persoon', $show_error));
			$form->add(HtmlInput::makeHidden("ContactPersoon[Type]", $cp->getType()));
		}

		if(!isSpook($cp) && !isBedrijfscontact($cp) && !isOverigSpookweb($cp) && !isAlumnus($cp))
			$div->add(self::wijzigTR($cp, 'Type', $show_error));

		$div->add(self::wijzigTR($cp, 'BeginDatum', $show_error));
		$div->add(self::wijzigTR($cp, 'EindDatum', $show_error));

		if(isBedrijfscontact($cp) || isAlumnus($cp))
			$div->add(self::wijzigTR($cp, 'Functie', $show_error));

		$div->add(self::wijzigTR($cp, 'Opmerking', $show_error));

		if(isBedrijfscontact($cp))
			$div->add(ContactView::wijzigTR($persoon, 'vakidOpsturen', $show_error));

		if(!isSpook($cp) && !isOverigSpookweb($cp) && !isAlumnus($cp))
			$form->add(ContactTelnrVerzamelingView::wijzigTelnrsDiv($persoon, $show_error, "WERK"));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Doen!")));

		return $form;
	}

	public static function processWijzigForm(ContactPersoon $cp)
	{
		//Als het een spook is gaat het om iemand die je niet via hier aan mag passen.
		if(!isSpook($cp) && !isOverigSpookweb($cp) && !isAlumnus($cp))
		{
			$pers = $cp->getPersoon();

			/** Haal alle data op **/
			if (!$data = tryPar(PersoonView::formobj($pers), null))
				return false;

			PersoonView::processForm($pers);

			/** Verwerk telefoonnummer(s) **/
			ContactTelnrVerzamelingView::processWijzigTelnrs($pers, $data);
		}

		ContactPersoonView::processForm($cp);
	}

	/**
	 *  Werk om de boel wat mooier te maken dan de generated waardes.
	 **/

	public static function labelPersoon(ContactPersoon $obj)
	{
		if(isSpook($obj))
			return _('Spook');
		return _('Persoon');
	}
	public static function labelOrganisatie(ContactPersoon $obj)
	{
		if(isSpook($obj) || isOverigSpookweb($obj))
			return _('Bedrijf');
		return _('Organisatie');
	}
	public static function labelFunctie(ContactPersoon $obj)
	{
		return _('Functie');
	}
	public static function labelBeginDatum(ContactPersoon $obj)
	{
		return _('Begindatum');
	}
	public static function labelEindDatum(ContactPersoon $obj)
	{
		return _('Einddatum');
	}
	public static function labelOpmerking(ContactPersoon $obj)
	{
		return _('Opmerking');
	}
	public static function labelType(ContactPersoon $obj)
	{
		return _('Type');
	}

	public static function formPersoon(ContactPersoon $obj, $include_id = false)
	{
		if($obj->geefID() && isAlumnus($obj))
		{
			$opties = PersoonVerzameling::verzamel(array($obj->geefID()));
			return static::defaultFormPersoonVerzameling($opties, 'Persoon', true);
		}

		$spoken = PersoonVerzameling::vanCommissie(SPOCIE);
		$spoken->voegtoe(Persoon::getIngelogd());
		$spoken->sorteer("mijEerst");
		return static::defaultFormPersoonVerzameling($spoken, 'Persoon', true);
	}

	public static function formType(ContactPersoon $obj, $include_id = false)
	{
		return ContactPersoonView::waardeType($obj);
	}
	
	public static function opmerkingPersoon()
	{
		return sprintf(_("Probeer ook eens de %s voor meer efficientie!"), new HtmlAnchor("/Service/Kart", "Kart"));
	}

	public static function waardePersoon(ContactPersoon $obj)
	{
		return PersoonView::naam($obj->getPersoon());
	}
	public static function waardeOrganisatie(ContactPersoon $obj)
	{
		return BedrijfView::waardeNaam($obj->getOrganisatie());
	}
	public static function waardeFunctie(ContactPersoon $obj)
	{
		return ucfirst(parent::waardeFunctie($obj));
	}

	public static function labelenumType($value)
	{
		switch ($value)
		{
			case 'SPOOK': return _('Spook');
			case 'BEDRIJFSCONTACT': return _('Bedrijfscontact');
			case 'OVERIGSPOOKWEB': return _('Sponsorcommissaris of overig');
			case 'ALUMNUS': return _('Alumnus/a');
			default: return parent::labelenumType($value);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
