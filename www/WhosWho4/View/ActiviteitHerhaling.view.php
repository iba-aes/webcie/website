<?
abstract class ActiviteitHerhalingView
	extends ActiviteitHerhalingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Geeft een listitem van het object terug zodat het in een ul gestopt
	 * kan worden
	 *
	 * @param herAct De herhalende activiteit
	 *
	 * @return Het listitem
	 */
	static public function lijstRegel($herAct)
	{
		$li = new HtmlListItem($span = new HtmlSpan(array(
			new HtmlSpan("[" . self::waardeMomentBegin($herAct, 'lijst') . "]", 'smalltext'),
			ActiviteitView::titel($herAct)
		)));

		return $li;
	}

	/**
	 *  Maakt de maindiv met informatie van de herhalende activiteit aan.
	 *
	 * @param act De herhalende activiteit
	 *
	 * @return De maindiv
	 */
	static public function maakMainDiv($act)
	{
		$div = new HtmlDiv();

		$div->add($callout = new HtmlDiv(null, 'bs-callout bs-callout-info'));
		$callout->add(sprintf('%s %s %s %s', _('Dit is een herhalende activiteit.')
			, sprintf(_('Bekijk de originele activiteit %s[VOC:hier].'),
				new HtmlAnchor($act->getInformatie()->url(), _('hier')))
			, sprintf(_('Bekijk ook de rest van de herhalingen %s[VOC:hier].'),
				new HtmlAnchor($act->getInformatie()->url() . '/Herhalingen', _('hier')))
			, sprintf(_('Of promoveer deze herhaling %s[VOC:hier]'),
				new HtmlAnchor($act->url() . '/Wijzig/Promoveer', _('hier')))));

		/** Voeg de informatie van de parent toe **/
		$div->add(ActiviteitView::maakInfoDiv($act));

		return $div;
	}

	/**
	 *  Maakt een pagina voor het wijzigen van een herhalende activiteit
	 *
	 * @param Activiteit $act De herhalende activiteiti
	 * @param bool $show_error Of errors moeten worden laten zien
	 * @return Page
	 */
	static public function wijzigen(Activiteit $act, $show_error = false)
	{
		$page = (new HTMLPage());
		$page->start(sprintf(_('%s wijzigen'), ActiviteitView::titel($act)));

		$form = self::wijzigForm($act, $show_error);

		$page->add($form);
		return $page;
	}

	/**
	 *  Maakt een form voor het wijzigen van een herhalende activiteit
	 *
	 * @param obj De herhalende activiteit
	 * @param show_error Of errors moeten worden laten zien
	 *
	 * @return Het formulier voor het wijzigen van het object
	 */
	static public function wijzigForm ($obj, $show_error = false)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named('ActWijzig');

		/**
		 * We gaan 2 divs maken:
		 * - statische informatie
		 * - datum
		 */

		/** Eerst de div voor statische informatie in te vullen **/
		$form->add($div = new HtmlDiv());

		$div->add($callout = new HtmlDiv(null, 'bs-callout bs-callout-warning'));
		$callout->add(_('Deze activiteit is een herhaling. Je past nu dus '
			. 'alleen de herhaling aan.'));

		$div->add(new HtmlHeader(3, _("Informatie")));

		$div->add(self::wijzigTR($obj, 'Toegang', $show_error));

		/** Dan de div voor data in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Data")));

		$div->add(self::wijzigTR($obj, 'MomentBegin', $show_error))
			->add(self::wijzigTR($obj, 'MomentEind', $show_error));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzig de activiteit!")));

		return $form;
	}

	/**
	 *  Verwerk het wijzig formulier van een herhalende activiteit
	 *
	 * @param act De herhalende activiteit
	 *
	 * @return False, als er geen data is om te verwerken
	 */
	static public function processWijzigForm (ActiviteitHerhaling $act)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($act), NULL))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($act);
	}

	/**
	 *  Promoveert een herhalende activiteit; hoort bij Activiteit_Controller::promoveer()
	 *
	 * @param Activiteit $act De herhalende activiteit om te promoveren
	 * @return HTMLPage|Page
	 */
	static public function promoveer (Activiteit $act)
	{
		$page = (new HTMLPage())->start(sprintf(_('Promoveren %s'), ActiviteitView::titel($act)));

		$div = new HtmlDiv();
		$div->add(new HtmlParagraph(
			 _("Het promoveren van een activiteit die onderdeel is van een overkoepelende herhalende activiteit, "
			 . "houdt in dat alle bindingen met de overkoepende activiteit worden geschorst. "
			 . "In plaats daarvan wordt het een losstaande activiteit met eigen informatie.")));
		$page->add($div);

		$form = new HtmlForm('post');
		$form->add(new HtmlParagraph(HtmlInput::makeCheckbox('actid')
					->setCaption(_("Ik weet het heel zeker!"))))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Volgende"), 'check')));
		$page->add($form);
		return $page;
	}
}
