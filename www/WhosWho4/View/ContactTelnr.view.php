<?

/**
 * $Id$
 */
abstract class ContactTelnrView
	extends ContactTelnrView_Generated
{   
	static public function labelTelefoonnummer(ContactTelnr $obj = NULL)
	{
		$soort = "";
		if($obj)
			$soort = self::labelenumSoort($obj->getSoort());

		return _('Telefoonnummer') . " " . $soort;
	}
	
	static public function labelSoort(ContactTelnr $obj)
	{
		return _('Soort');
	}
	
	public static function labelenumSoort($value)
	{
		switch ($value)
		{
			case 'THUIS': return _('Thuisnummer');
			case 'MOBIEL': return _('Mobiel');
			case 'OUDERS': return _('Ouder');
			case 'WERK': return _('Werk');
			case 'RECEPTIE': return _('Receptie');
		}
		return parent::labelenumSoort($value);
	}

	static public function waardeTelefoonnummer(ContactTelnr $obj = null)
	{
		if ($obj instanceof ContactTelnr)
			return parent::waardeTelefoonnummer($obj);
		return '-';
	}

	/**
	 * @brief Geeft een regel tekst terug met tekst "$soort: $telnr"
	 */
	static public function regel($telnr)
	{
		$soort = ContactTelnrView::waardeSoort($telnr);
		return $soort . ": " . ContactTelnrView::waardeTelefoonnummer($telnr);
	}

	static public function detailsDiv($telnr)
	{
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(4, self::waardeSoort($telnr) . ":"))
			->add(self::waardeTelefoonnummer($telnr));

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
