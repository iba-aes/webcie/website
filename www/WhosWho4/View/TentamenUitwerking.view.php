<?
abstract class TentamenUitwerkingView
	extends TentamenUitwerkingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}


	/**
	 *  Maak een form voor het wijzigen van een tentamen uitwerking
	 * 
	 */
	public static function wijzigForm(TentamenUitwerking $uitwerking, $new = false, $show_error = false)
	{
		$tentamen = $uitwerking->getTentamen();
		$vak = $tentamen->getVak();

		$page = Page::getInstance();
		$page->start();

		//Set enctype attribute voor het versturen van file data

		// initialize form
		if($new)
		{
			$form = HtmlForm::named('nieuweUitwerking');
		}
		else
		{
			$form = HtmlForm::named('wijzigUitwerking');
		}
		$form->setAttribute('enctype', "multipart/form-data");

		// Geef de naam/datum van het tentamen en de naam van het vak.
		$vakNaam = $vak->getNaam();
		$form->add(new HtmlHeader(4, $vakNaam));

		if($tentamen->getNaam() != NULL)
			$form->add(new HtmlHeader(4, $tentamen->getNaam()));
		else
			$form->add(new HtmlHeader(4, 'Toets van '.$tentamen->getDatum()->format('d-m-Y')));


		$form->add($div = new HtmlDiv());

		//Voeg opmerking veld toe
		$div->add(self::wijzigTR($uitwerking, 'Opmerking', $show_error));

		// Hierzo stoppen we het file-upload-veld in het form
		$div->add(self::wijzigTR($uitwerking, 'Bestand', $show_error));

		// Controleren, kan alleen door tbc's
		if(hasAuth('tbc'))
			$div->add(self::wijzigTR($uitwerking, 'Gecontroleerd', $show_error));

		if($new)
			$form->add(HtmlInput::makeFormSubmitButton(_('Voeg toe')));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_('Wijzig')));


		$page->add($form);


		$page->end();
	}

	public static function opmerkingOpmerking()
	{
		return 'Voeg een opmerking toe die andere mensen kunnen zien!';
	}

	public static function opmerkingGecontroleerd()
	{
		return '<i>Dit veld is alleen zichtbaar voor leden van de TBC.<br>
			De tentamens zijn pas voor iedereen zichtbaar als ze gecontroleerd zijn.</i>';
	}

	static public function labelBestand()
	{
		return _('Bestand');
	}

	static public function opmerkingBestand()
	{
		return '';
	}

	static public function formBestand()
	{
		return HtmlInput::makeFile('file', 1);
	}
}
