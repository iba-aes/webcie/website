<?
abstract class ExtraPosterView
	extends ExtraPosterView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	private static function addRow($container, $label, $for, $input)
	{
		$container->add(new HtmlTableRow(array(
			$label = new HtmlTableDataCell(new HtmlLabel($for, array($label))),
			new HtmlTableDataCell($input)
		)));
		$label->setAttributes(array('valign' => 'middle'));
	}

	/**
	 * Returnt een HtmlForm wat een formulier bevat om een extra poster toe te kunnen voegen of te wijzigen.
	 */
	public static function formWijzigPoster($poster)
	{
		$hasPoster = $poster !== NULL;

		$body = new HtmlDiv();
		
		$body->add($btnBar = new HtmlDiv(null, 'btn-group'));
		$btnBar->add(HtmlAnchor::button(($hasPoster ? "../" : "") ."index.html", _("Terug naar het overzicht")));
		if ($hasPoster) {
			$btnBar->add(HtmlAnchor::button("/Service/ExtraPoster/" . $poster->getExtraPosterID(), _("Zie poster")));
			$btnBar->add(HtmlAnchor::button("Verwijder", _("Verwijder poster")));
		}

		$body->add($form = new HtmlForm());
		$form->setAttribute("enctype", "multipart/form-data");
		$form->add($table = new HtmlTable());
		$table->add($tbody = new HtmlTableBody());

		self::addRow($tbody, _("Naam"), "naam", HtmlInput::makeText("naam", $hasPoster ? $poster->getNaam() : NULL));
		self::addRow($tbody, _("Beschrijving"), "Beschrijving", HtmlInput::makeText("beschrijving", $hasPoster ? $poster->getBeschrijving() : NULL));
		self::addRow($tbody, _("Zichtbaar"), "zichtbaar", HtmlInput::makeCheckbox("zichtbaar", $hasPoster ? $poster->getZichtbaar() : TRUE));
		self::addRow($tbody, _("Poster"), "poster", $fileInput = HtmlInput::makeFile("poster", ACTPOSTERS_MAX_FILE_SIZE, false, "center-block"));

		$fileInput->setCssStyle("display: block;");
		$form->add(HtmlInput::makeHidden("pasPosterAan","aye"));
		$form->add($submit = HtmlInput::makeSubmitButton(_($hasPoster ? "Update gegevens!" : "Voeg poster toe!"), "submit", "center-block"));
		$submit->setCssStyle("margin-top:.5em;");

		if ($hasPoster) {
			$body->add(new HtmlDiv(array(
				new HtmlParagraph(_("Huidige poster:")),
				$img = new HtmlImage($poster->getURL(), "huidige poster")
			), "poster-preview"));
			$img->setLazyLoad();
		}

		$title = $hasPoster ? "Wijzig extra poster" : "Voeg extra poster toe";
		Page::getInstance()->start($title)->add($body)->end();
	}
}

