<?
abstract class ActiviteitHerhalingVerzamelingView
	extends ActiviteitHerhalingVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function htmlTable(ActiviteitHerhalingVerzameling $verz)
	{
		if(!$verz || $verz->aantal() == 0)
			return new HtmlEmphasis(_('Geen resultaten gevonden'));

		$res = self::makeTable($verz,array('Herhaling', 'momentBegin', 'momentEind'));

		return $res;
	}
}
