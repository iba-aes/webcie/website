<?
abstract class DocuCategorieVerzamelingView
	extends DocuCategorieVerzamelingView_Generated
{
	/*
	 *  Geeft het overzicht voor de docucategorieën
	 *
	 * @param cats De verzameling van DocuCategorieën
	 * @param msg Een eventuele foutmeldingen
	 */
	static public function overzicht(DocuCategorieVerzameling $cats, $msg)
	{
		$page = Page::getInstance()->start(_("Categoriemanagement"));

		// als er fouten zijn, laat die dan zien
		if($msg)
			$page->add(new HtmlDiv($msg, 'alert alert-danger'));

		$page->add(HtmlAnchor::button(DOCUBASE, _("Terug")));

		$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
		$div->add($ul = new HtmlList());

		foreach($cats as $cat) {
			$ul->add(new 
				HtmlListItem(DocuCategorieView::waardeOmschrijving($cat)));
		}

		// form om een nieuwe categorie toe te voegen
		$page->add($form = HtmlForm::named("nieuw"));
		$form->addClass('form-inline');
		$form->add(_("Voeg nieuwe categorie toe: "));
		$form->add(HtmlInput::makeText("omschrijving"));
		$form->add(HtmlInput::makeSubmitButton(_("Voeg toe")));

		$page->end();
	}
}
