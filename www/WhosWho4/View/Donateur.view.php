<?

/**
 * $Id$
 */
abstract class DonateurView
	extends DonateurView_Generated
{
	/** @name View: label/waarde/form/opmerking @{ **/
	public static function labelPersoon(Donateur $obj)
	{
		return _('Naam');
	}
	public static function labelAnderPersoon(Donateur $obj)
	{
		return _('Tweede persoon');
	}
	public static function labelAanhef(Donateur $obj)
	{
		return _('Aanhef');
	}
	public static function labelJaarBegin(Donateur $obj)
	{
		return _('Beginjaar');
	}
	public static function labelJaarEind(Donateur $obj)
	{
		return _('Eindjaar');
	}
	public static function labelDonatie(Donateur $obj)
	{
		return _('Donatie');
	}
	public static function labelMachtiging(Donateur $obj)
	{
		return _('Machtiging');
	}
	public static function labelMachtigingOud(Donateur $obj)
	{
		return _('Oude machtiging');
	}
	public static function labelAlmanak(Donateur $obj)
	{
		return _('Almanak');
	}
	public static function labelAvNotulen(Donateur $obj)
	{
		return _('AV-notulen');
	}
	public static function labelenumAvNotulen($value)
	{
		switch($value) {
			case 'N':
				return _('nee');
			case 'EMAIL':
				return _('ja, per email');
			case 'POST':
				return _('ja, per post');
			case 'POST_EMAIL':
				return _('ja, per email en post');
			default:
				user_error('onbekende waarde van AV-notulenenum '.$value);
		}
	}
	public static function labelJaarverslag(Donateur $obj)
	{
		return _('Jaarverslag');
	}
	public static function labelStudiereis(Donateur $obj)
	{
		return _('Studiereis');
	}
	public static function labelSymposium(Donateur $obj)
	{
		return _('Symposium');
	}
	public static function labelVakid(Donateur $obj)
	{
		return _('Vakidioot');
	}
	public static function waardePersoon(Donateur $dona)
	{
		return new HtmlAnchor('/Leden/'.$dona->getPersoonContactID()
							, PersoonView::naam($dona->getPersoon()));
	}
	public static function waardeAnderPersoon(Donateur $dona)
	{
		$p2 = $dona->getAnderPersoon();
		if(!$p2) return NULL;
		return new HtmlAnchor('/Leden/'.$p2->getContactID(), PersoonView::naam($p2));
	}
	public static function waardeJaarBegin(Donateur $dona)
	{
		return self::defaultWaardeColjaar($dona, 'JaarBegin');
	}
	public static function waardeJaarEind(Donateur $dona)
	{
		if(!$dona->getJaarEind())
			return NULL;

		return self::defaultWaardeColjaar($dona, 'JaarEind');
	}
	public static function waardeDonatie(Donateur $dona)
	{
		$waarde = parent::waardeDonatie($dona);

		if($dona->getMachtiging())
			return $waarde.' ('.self::labelMachtiging($dona).')';
		if($dona->getMachtigingOud())
			return $waarde.' ('.self::labelMachtigingOud($dona).')';

		return $waarde;
	}
	public static function formAnderPersoon(Donateur $dona, $include_id = false)
	{
		return self::defaultFormPersoonVerzameling(PersoonVerzameling::verzamel(array($dona->getAnderPersoonContactID())), 'Donateur[AnderPersoon]', true);
	}
	public static function opmerkingJaarBegin()
	{
		return _('2003 betekent "collegejaar 2003-2004"');
	}
	//@}

		/**
Overwrite the viewInfo om ruimte te maken voor het Totaal Bedrag
	 */
	public static function viewInfo($obj, $header = false)
	{
		$panel = parent::viewInfo($obj, $header);

		$tbl = $panel->getChild(1)->getChild(0);

		$tbl->add(self::infoTRData(_('Totaalbedrag'), self::wiewatTDTotaal($obj)));

		return $panel;
	}

	/** @name View: info TR @{ **/
	protected static function infoTRAnderPersoon(Donateur $dona)
	{
		$p2 = $dona->getAnderPersoon();
		if(!$p2) return NULL;

		$row = new HtmlTableRow();
		$row->addEmptyCell();
		$row->addData(self::waardeAnderPersoon($dona));
		return $row;
	}
	protected static function infoTRMachtiging()
	{
		return NULL; /* geen eigen TR, zit al in bij de waardeDonatie */
	}
	protected static function infoTRMachtigingOud()
	{
		return NULL; /* geen eigen TR, zit al in bij de waardeDonatie */
	}
	//@}

	/** @name View: wiewatTR @{ **/
	public static function wiewatTR($dona, $velden)
	{
		$row = new HtmlTableRow();

		if(is_null($dona)) {
			$row->addHeader(_('Wie'));
			$row->addHeader(_('Wanneer'));
			$row->addHeader(_('Wat'))->colspan(6);
			$row->addHeader(_('Euro'))->colspan(2);
			$row->addHeader(_('Totaalbedrag'));
			return $row;
		}
		
		foreach($velden as $veld)
		{
			$data = self::call('wiewatTD', $veld, $dona);

			if($data)
				$row->addData($data);
			else
				$row->addEmptyCell();
		}
		return $row;
	}

	protected static function wiewatTD(Donateur $dona, $veld)
	{
		return self::call('waarde', $veld, $dona);
	}
	protected static function wiewatTDPersoon(Donateur $dona)
	{
		
		$href = new HtmlAnchor('/Leden/Donateurs/'.$dona->getPersoonContactID());
		$href->add(PersoonView::naam($dona->getPersoon()));
		$p2 = $dona->getAnderPersoon();
		if($p2) {
			$href->add(new HtmlBreak());
			$href->add(PersoonView::naam($p2));
		}
		return $href;
	}
	protected static function wiewatTDJaarBegin(Donateur $dona)
	{
		$jaar = $dona->getJaarEind();
		return $dona->getJaarBegin().'-'.($jaar ? $jaar : 'nu');
	}
	protected static function wiewatTDDonatie(Donateur $dona)
	{
		return parent::waardeDonatie($dona);
	}
	protected static function wiewatTDMachtiging(Donateur $dona)
	{
		if($dona->getMachtiging())
			return self::labelMachtiging($dona);
		if($dona->getMachtigingOud())
			return self::labelMachtigingOud($dona);

		return '';
	}
	protected static function wiewatTDAlmanak(Donateur $dona)
	{
		return $dona->getAlmanak() ? new HtmlAbbreviation('alm', _('Almanak')) : '';
	}

	/**
	 * @brief Geef de inhoud van de wiewat-cel voor de AV-notulen weer.
	 *
	 * @param dona De donateur waarvan we de AV-notulenwaarde willen zien.
	 *
	 * @return Een HTMLElement om in je TD te stoppen.
	 */
	protected static function wiewatTDAvNotulen(Donateur $dona)
	{
		switch ($dona->getAvNotulen())
		{
			case 'N':
				return '';
			case 'EMAIL':
				return new HtmlAbbreviation('avE', _('AV-notulen per e-mail'));
			case 'POST':
				return new HtmlAbbreviation('avP', _('AV-notulen per post'));
			case 'POST_EMAIL':
				return new HtmlAbbreviation('avEP', _('AV-notulen per e-mail en post'));
			default:
				user_error('onbekende waarde van AV-notulenenum ' . $dona->getAVNotulen());
		}
	}
	protected static function wiewatTDJaarverslag(Donateur $dona)
	{
		return $dona->getJaarverslag() ? new HtmlAbbreviation('jaa', _('Jaarverslag')) : '';
	}
	protected static function wiewatTDStudiereis(Donateur $dona)
	{
		return $dona->getStudiereis() ? new HtmlAbbreviation('stu', _('Studiereis')) : '';
	}
	protected static function wiewatTDSymposium(Donateur $dona)
	{
		return $dona->getSymposium() ? new HtmlAbbreviation('sym', _('Symposium')) : '';
	}
	protected static function wiewatTDVakid(Donateur $dona)
	{
		return $dona->getVakid() ? new HtmlAbbreviation('vak', _('VakIdioot')) : '';
	}
	protected static function wiewatTDTotaal(Donateur $dona)
	{
		$totaal = $dona->totaalbedrag();
		return "&#8364 ".$totaal;
	}
	//@}

	/** @name View: latexCommand @{ **/
	public static function latexCommand($dona)
	{
		$velden = array('Aanhef', 'Almanak', 'AvNotulen', 'Jaarverslag', 'Studiereis', 'Symposium', 'Vakid', 'Machtiging');

		if($dona == NULL) {
			return "%\donateur{naam\\\\straat~huisnr\\postcode~~plaats}{"
				. implode('}{', $velden) . "}\n";
		}

		$persoon = $dona->getPersoon();
		$adres = ContactAdres::eersteBijContactAdresGesorteerd($persoon);
		if($adres instanceof ContactAdres)
		{
			$regel = '\donateur{' . PersoonView::naam($persoon) . '\\\\' .
				ContactAdresView::latexAdres($adres) . '}';
		}
		else
		{
			return NULL;
		}

		foreach ($velden as $veld)
		{
			$regel .= self::call('latexParameter', $veld, $dona); 
		}
		return $regel."\n";
	}
	protected static function latexParameter(Donateur $dona, $veld)
	{
		$waarde = self::call('waarde', $veld, $dona);
		switch($waarde)
		{
			case 'ja':  return "{true}";
			case 'nee': return "{false}";
			default:    return '{'.latexescape($waarde).'}';
		}
	}
	protected static function latexParameterAvNotulen(Donateur $dona)
	{
		switch ($dona->getAvNotulen())
		{
			case 'EMAIL': return "{email}";
			case 'POST':  return "{post}";
			default:	  return "{false}";
		}
	}
	//@}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
