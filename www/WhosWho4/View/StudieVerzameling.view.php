<?
/**
 * $Id$
 */
abstract class StudieVerzamelingView
	extends StudieVerzamelingView_Generated
{

	/**
	 *  /brief Geef een form om een verzameling aan te passen.
	 *
	 *  /param studies de studieverzameling.
	 **/
	public static function wijzigForm(StudieVerzameling $studies)
	{
		$div = new HtmlDiv();
		$div->add($form = HtmlForm::named("studieverzameling"));
		$selectbox = self::alleStudiesSelect('studies')
			->setMultiple(true)
			->select($studies->keys())
			->setSize(10);

		$form->add($selectbox)
			->add(HtmlInput::makeSubmitButton(_("Wijzig")));

		return $div;
	}

	static public function toString (StudieVerzameling $studies)
	{
		switch ($studies->aantal())
		{
		case 0:
			return NULL;

		case 1:
			return StudieView::waardeNaam($studies->first());

		default:
			global $studienamen;
			$namen = array();
			foreach ($studies as $studie)
			{
				$namen[] = $studie->getNaam('nl');
			}
			sort($namen);
			$namenstring = implode(',', $namen);
			foreach ($studienamen as $ids => $studienaam)
			{
				if ($namenstring == $ids)
					return $studienaam;
			}

			$ret = array();
			foreach ($studies as $studie)
			{
				$ret[] = StudieView::waardeNaam($studie);
			}
			return  implode(', ', $ret);
		}
	}

	static public function tabel (StudieVerzameling $studies)
	{
		$table = new HtmlTable();
		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());
		$thead->add($row = new HtmlTableRow());
		$row->makeHeaderFromArray(array(
			StudieView::labelNaam(),
			StudieView::labelSoort(),
			StudieView::labelFase(),
			StudieView::labelAantalStudenten()));

		foreach ($studies as $studie)
			$tbody->add(StudieView::toTableRow($studie));

		return $table;
	}

	/**
	 * \param selected kan zowel array van id's zijn, of een Studie object
	 */
	static public function alleStudiesSelect ($name, $selected = NULL)
	{
		$ba = StudieVerzameling::alleStudies('BA');
		$ma = StudieVerzameling::alleStudies('MA');
		$arr = array();
		if (!$selected || (is_array($selected) && empty($selected)))
			$arr[''] = '';

		if ($selected && !is_array($selected)) $selected = array($selected->geefID());

		foreach ($ba as $studie)
			$arr[_('Bachelorstudies')][$studie->geefID()] = StudieView::waardeNaam($studie);
		foreach ($ma as $studie)
			$arr[_('Masterstudies')][$studie->geefID()] = StudieView::waardeNaam($studie);

		return HtmlSelectbox::fromArray($name, $arr, $selected);
	}

	static public function masterStudiesSelect ($name, Studie $selected = NULL)
	{
		$ma = StudieVerzameling::alleStudies('MA');
		$arr = array();
		if (!$selected)
			$arr[''] = '';

		foreach ($ma as $studie)
			$arr[_('Masterstudies')][$studie->geefID()] = StudieView::waardeNaam($studie);

		$box = HtmlSelectBox::fromArray($name, $arr,
			$selected ? $selected->geefID() : array());
		return $box;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
