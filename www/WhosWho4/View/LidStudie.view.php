<?

/**
 * $Id$
 */
abstract class LidStudieView
	extends LidStudieView_Generated
{
	
	static public function kort (LidStudie $lidstudie)
	{
		$studie = $lidstudie->getStudie();
		$tekst = new HtmlSpan(
			new HtmlStrong(StudieView::waardeNaamLang($studie))
			. new HtmlBreak()
			. self::waardeDatumBegin($lidstudie) . ' &ndash; '
			. self::waardeDatumEind($lidstudie)
			. (($lidstudie->getStatus() != 'STUDEREND') ? ', ' . self::waardeStatus($lidstudie) : '')
		);
		$groep = $lidstudie->getGroep();
		if($groep)
		{
			$tekst->add(new HtmlBreak())
				  ->add(_('Mentorgroep: ') . IntroGroepView::maakLink($groep));
		}
		return $tekst;
	}

	static public function toString (LidStudie $lidstudie)
	{
		$studie = $lidstudie->getStudie();
		return new HtmlStrong(StudieView::waardeNaamLang($studie))
			. ' (' . self::waardeDatumBegin($lidstudie, true) . ' - '
			. (($lidstudie->getDatumEind()->hasTime()) ? self::waardeDatumEind($lidstudie, true) :
				(($lidstudie->getStatus() == 'STUDEREND') ? _('heden') : _('onbekend')))
			. ')'
			. (($lidstudie->getStatus() != 'STUDEREND') ? ' ' . self::waardeStatus($lidstudie) : '');
	}

	static public function wijzigStudieTR (LidStudie $obj = NULL, $show_error = false)
	{
		$div = new HtmlDiv(null, 'form-group');
		if($show_error)
			$div->addClass('has-success has-feedback');

		$div->add(new HtmlLabel(static::labelStudie($obj), static::labelStudie($obj), 'col-sm-2 control-label'));
		$div->add(new HtmlDiv($box = StudieVerzamelingView::alleStudiesSelect('LidStudie' . 
			($obj ? sprintf('[%d]', $obj->geefID()) : '') . '[Studie]',
			$obj && $obj->getStudie() ? $obj->getStudie() : NULL), 'col-sm-10'));
		return $div;
	}

	static public function wijzigStudieMasterTR (LidStudie $obj = NULL, $show_error = false)
	{
		if($obj)
			$obj = new LidStudie();
		$row = new HtmlTableRow();
		$row->addData(_("Studie<font color=red>*</font>:"));
		$row->addData($box = StudieVerzamelingView::masterStudiesSelect('LidStudie' .
			($obj ? sprintf('[%d]', $obj->geefID()) : '') . '[Studie]',
				$obj && $obj->getStudie() ? $obj->getStudie() : NULL));
		$row->addEmptyCell();
		if ($show_error && $obj->getStudieStudieID() == 0)
			$row->addData('dit is een verplicht veld');
		return $row;
	}

	static public function labelStudie(LidStudie $obj = NULL)
	{
		return _('Studie');
	}
	static public function labelStatus (LidStudie $obj)
	{
		return _('Status');
	}
	static public function labelDatumBegin (LidStudie $obj)
	{
		return _('Begindatum');
	}
	static public function labelDatumEind (LidStudie $obj)
	{
		return _('Einddatum');
	}

	static public function waardeStatus (LidStudie $studie)
	{
		switch ($studie->getStatus())
		{
			case 'STUDEREND' : return _('studerend');
			case 'ALUMNUS' : return _('alumnus');
			case 'GESTOPT' : return _('gestopt');
		}
	}
	static public function waardeDatumBegin (LidStudie $studie, $uitgebreid = false)
	{
		if ($uitgebreid)
			return parent::waardeDatumBegin($studie);
		return "'" . $studie->getDatumBegin()->strftime('%y');
	}
	static public function waardeDatumEind (LidStudie $obj, $uitgebreid = false)
	{
		if ($obj->getDatumEind()->hasTime())
		{
			if($obj->getStatus() == 'STUDEREND' && $obj->getDatumEind() > new DateTimeLocale())
				return _('heden');
			if ($uitgebreid)
				return parent::waardeDatumEind($obj);
			return "'" . $obj->getDatumEind()->strftime('%y');
		}
		else if($obj->getStatus() == 'STUDEREND')
			return _('heden');
		return _('onbekend');
	}

	public static function formStudie(LidStudie $obj, $include_id = false)
	{
		$box = StudieVerzamelingView::alleStudiesSelect('LidStudie' . 
			($obj ? sprintf('[%d]', $obj->geefID()) : '') . '[Studie]',
			$obj && $obj->getStudie() ? $obj->getStudie() : NULL);
		return $box;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
