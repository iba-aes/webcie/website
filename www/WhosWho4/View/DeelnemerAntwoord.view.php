<?

/**
 * $Id$
 */
abstract class DeelnemerAntwoordView
	extends DeelnemerAntwoordView_Generated
{
	public static function labelDeelnemer(DeelnemerAntwoord $obj)
	{
		return _('Deelnemer');
	}
	public static function labelVraag(DeelnemerAntwoord $obj)
	{
		return _('Vraag');
	}
	public static function labelAntwoord(DeelnemerAntwoord $obj)
	{
		$label = _("Antwoord");
		if ($obj->getVraag()->getVerplicht())
		{
			$label .= new HTMLSpan("*", "waarschuwing");
		}
		return $label;
	}

	public static function formAntwoord(DeelnemerAntwoord $obj, $include_id = false)
	{
		$name = 'DeelnemerAntwoord['.$obj->geefID().'][Antwoord]';
		if ($obj->getVraag()->getType() == 'ENUM')
		{
			$opties = $obj->getVraag()->getOpties();
			$opties = array(null => _('-- Selecteer een antwoord --')) + $opties;
			return HtmlSelectbox::fromArray($name, $opties, $obj->getAntwoord() ? array_search($obj->getAntwoord(), $opties) : array());
		}
		else
		{
			$elem = new HtmlTextarea($name);
			$elem->addChild(htmlspecialchars(tryPar($name, '')));
			return $elem;
		}
	}

	public static function processForm($obj, $velden = 'viewWijzig', $data = null, $include_id = false)
	{
		parent::processForm($obj, $velden, $data, $include_id);

		if ($obj->getVraag()->getType() == 'ENUM')
		{
			$opties = $obj->getVraag()->getOpties();

			if (isset($data['Antwoord']) && isset($opties[$data['Antwoord']]))
			{
				$obj->setAntwoord($opties[$data['Antwoord']]);
			}
			else
			{
				$obj->setAntwoord('');
			}
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
