<?

/**
 * $Id$
 */
abstract class ActiviteitVraagView
	extends ActiviteitVraagView_Generated
{

	public static function labelVraag(ActiviteitVraag $actvraag)
	{
		if($actvraag->getVraagID()){
			$nr = $actvraag->getVraagID();
			return _("Vraag $nr aan deelnemer");
		} else if($actvraag->getFakeID()) {
			$nr = $actvraag->getFakeID();
			return _("Vraag $nr aan deelnemer");
		} else {
			return _("Nieuwe vraag");
		}
	}

	public static function labelenumType($value)
	{
		if($value == 'STRING')
			return _('Open vraag');
		else if($value == 'ENUM')
			return _('Meerkeuzevraag');
	}

	public static function processForm($vraag, $velden = 'viewWijzig', $data = null, $include_id = false)
	{
		parent::processForm($vraag, $velden, $data, $include_id);

		if($vraag->getType() == 'ENUM' && isset($data['Opties']))
			$vraag->setOpties($data['Opties']);
		else if($vraag->getType() == 'ENUM')
			$vraag->setOpties(array());

		return $vraag->valid();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1

