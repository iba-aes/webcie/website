<?
abstract class VoornaamwoordVerzamelingView
	extends VoornaamwoordVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in VoornaamwoordVerzamelingView.
	 *
	 * @param obj Het VoornaamwoordVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoornaamwoordVerzameling(VoornaamwoordVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef een tabel weer van deze (of alle) voornaamwoorden.
	 *
	 * @param verz De voornaamwoorden om te tonen. Default: allemaal.
	 * @returns een HTMLobject dat je in je pagina kan stoppen.
	 */
	public static function tabel(VoornaamwoordVerzameling $verz = NULL) {
		if (is_null($verz)) {
			$verz = VoornaamwoordQuery::table()->verzamel();
		}

		if ($verz->aantal() > 0) {
			$tabel = new HtmlTable();

			$tabel->add(new HtmlTableHead($headRow = new HtmlTableRow()));
			if (Persoon::getIngelogd()) {
				$headRow->addHeader(""); // Kies
			}
			$headRow->addHeader(_("Soort"));
			$headRow->addHeader(_("Nederlands[VOC: -talig]"));
			$headRow->addHeader(_("Engels[VOC: -talig]"));
			if (hasAuth('promocie')) {
				$headRow->addHeader(""); // Wijzig
				$headRow->addHeader(""); // Verwijder
			}

			foreach ($verz as $obj) {
				$id = $obj->geefID();
				$row = new HtmlTableRow();
				if (Persoon::getIngelogd()) {
					$row->addData(HtmlAnchor::button("/Service/Voornaamwoord/".$id."/Kies", _("Kies[VOC: dit woord als voornaamwoord]")));
				}
				$row->addData(VoornaamwoordView::waardeSoort($obj));
				$row->addData(VoornaamwoordView::vervoegingen($obj, 'nl'));
				$row->addData(VoornaamwoordView::vervoegingen($obj, 'en'));
				if (hasAuth('promocie')) {
					$row->addData(HtmlAnchor::button("/Service/Voornaamwoord/".$id."/Wijzig", _("Wijzig")));
					$row->addData(HtmlAnchor::button("/Service/Voornaamwoord/".$id."/Verwijder", _("Verwijder")));
				}
				$tabel->addImmutable($row);
			}

			return $tabel;
		} else {
			return new HtmlParagraph(_("Er zijn geen voornaamwoorden gevonden."));
		}
	}
}
