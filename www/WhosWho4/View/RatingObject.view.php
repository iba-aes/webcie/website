<?
abstract class RatingObjectView
	extends RatingObjectView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}
	
	
	/**
	 *  Geeft die leuke vijf sterretjes terug voor de rating
	 *
	 * @param ratingObject Het rating object waar we de sterretjes van willen hebben
	 */
	static public function maakRating($ratingObject) {
		// Checkt of er op een sterretje van dit rating object is gedrukt.
		$ratingObject->checkForRating();
		
		$rating = $ratingObject->getRating();
		$rating = round($rating/10,1);
		$votes = $ratingObject->getRatingCount();

		$wholestar = '/Layout/Images/Icons/star_whole.gif';
		$halfstar = '/Layout/Images/Icons/star_half.gif';
		$emptystar = '/Layout/Images/Icons/star_empty.gif';

		$span = new HtmlSpan();
		$span->setCssStyle('display: inline-block; white-space: nowrap');
		
		$counter = 1;
		if(Persoon::getIngelogd())
		{
			$span->add($ratingForm = new HtmlForm());
			while($counter <= 5)
			{
				if($rating >= 2 * $counter)
					$ratingForm->add(self::getEnabledStar($ratingObject, $counter, $wholestar));
				elseif($rating >= (2 * $counter) -1)
					$ratingForm->add(self::getEnabledStar($ratingObject, $counter, $halfstar));
				else
					$ratingForm->add(self::getEnabledStar($ratingObject, $counter, $emptystar));
				$counter++;
			}
		}
		else
		{
			while($counter <= 5)
			{
				if($rating >= 2 * $counter)
					$span->add(self::getDisabledStar($wholestar));
				elseif($rating >= (2 * $counter) -1)
					$span->add(self::getDisabledStar($halfstar));
				else
					$span->add(self::getDisabledStar($emptystar));
				$counter++;
			}
		}
		return $span;
	}
	
	//Een sterretje waar je op kan drukken
	private static function getEnabledStar($ratingObject, $number, $imageSrc)
	{
		$star = new HtmlInput('rating'.$ratingObject->geefID().'_'.$number);
		$star->setAttribute('type', 'image');
		$star->setAttribute('src', $imageSrc);
		$star->setAttribute('alt', 'Rate ' . $number . ' ster');
		$star->setCssStyle('border: 0 none; background-color:rgba(0,0,0,0); display: table-col; width: 21px; height: 21px; padding : 3px 3px; background-image: none;' );
		
		return $star;
	}
	//Een sterretje waar je NIET op kan drukken
	private static function getDisabledStar($imageSrc)
	{
		$star = new HtmlImage($imageSrc, _('Log in om te kunnen raten!'));
		return $star;
	}

}
