<?

/**
 * $Id$
 */
abstract class OrganisatieView
	extends OrganisatieView_Generated
{

	static public function csvRegel($org)
	{
		$cp = $org->getContactPersoon()->first(); //We gaan er van uit dat er maximaal eentje is.
		$tel = $org->getPrefTelnr();
		$adres = $org->getAdressen();
		$post = $adres->filter('getSoort','POST')->first();
		$bezoek = $adres->filter('getSoort','BEZOEK')->first(); //Bezoekadres waar nodig

		$result = $org->getNaam();
		$result .= ";" . $org->getSoort();
		if($cp){
			$result .= ";" . PersoonView::naam($cp->getPersoon());
			$result .= ";" . $cp->getFunctie();
		} else {
			$result .= ";;";
		}
		$result .= ";" . $tel;
		$result .= ";" . $org->getEmail();
		$result .= ";" . $org->getHomepage();
		if ($post) {
			$result .= ";" . $post->getStraat1();
			$result .= ";" . $post->getHuisNummer();
			$result .= ";" . $post->getPostcode();
			$result .= ";" . $post->getWoonplaats();
		} else {
			$result .= ";;;;";
		}
		if($bezoek){
			$result .= ";" . $bezoek->getStraat1();
			$result .= ";" . $bezoek->getHuisNummer();
			$result .= ";" . $bezoek->getPostcode();
			$result .= ";" . $bezoek->getWoonplaats();
		} else {
			$result .= ";;;;";
		}

		$result .= ";" . $org->getOmschrijving();

		return $result;

	}

	public static function latexRegel($org)
	{
		$cp = $org->getContactPersoon()->first(); //We gaan er van uit dat er maximaal eentje is.
		$adres = $org->getAdressen();
		$post = $adres->filter('getSoort','POST')->first();
		//fallback op thuis als dat nodig is
		if(!$post) $post = $adres->filter('getSoort','THUIS')->first();

		$result = array();
		$result[] = $org->getNaam();
		$result[] = $cp ? PersoonView::naam($cp->getPersoon()) : '';
		$result[] = $post ? ( $post->getStraat1().' '.$post->getHuisNummer()
			.'\\'.$post->getPostcode().'\\'.$post->getWoonplaats() ) : '';
		$result[] = $org->getPrefTelnr();
		$result[] = $post ? $post->kixcode() : '';

		return '\person{'.implode('}{', array_map('latexescape',$result))."}\n";
	}

	static public function details(Contact $Organisatie, $noPage = false)
	{
		$pageDiv = new HtmlDiv();

		$pageDiv->add(OrganisatieView::viewInfo($Organisatie, true))
				->add(HtmlAnchor::button('/Leden/Organisatie', _('Terug naar overzicht')))
				->add(new HtmlHeader(3, sprintf(_('Contactgegevens %s'), new HtmlSmall(new HtmlAnchor('WijzigAdres', _("Wijzig adres"))))));

		if($Organisatie->getContactPersoon()->aantal() == 0)
			$pageDiv->add(HtmlAnchor::button('Contactpersonen/Nieuw', _('Nieuwe contactpersoon')));

		foreach($Organisatie->getContactPersoon() as $contactpersoon){
			$pageDiv->add(ContactPersoonView::detailsDiv($contactpersoon));
		}

		foreach($Organisatie->getAdressen() as $adres){
			$pageDiv->add(ContactAdresView::detailsDiv($adres));
		}

		foreach($Organisatie->getTelefoonNummers() as $telnr){
			$pageDiv->add(ContactTelnrView::detailsDiv($telnr));
		}

		$pageDiv->add(new HtmlSpan(new HtmlBreak() .
			_('LaTeX-adresregel: ') .
			new HtmlBreak() .
			new HtmlSpan(OrganisatieView::latexRegel($Organisatie), 'monospace')));

		$page = static::pageLinks($Organisatie);
		return $page->start(_('Organisatie') . ': ' . $Organisatie->getnaam())
			->add($pageDiv);
	}

	/**
	 * Toont het formulier voor het wijzigen van een organisatie
	 */
	static public function wijzig ($org, $form, $msg)
	{
		$page = static::pageLinks($org);
		$page->start(null, 'leden');

		$page->addFooterJS(Page::minifiedFile('knockout.js'))
			 ->addFooterJS(Page::minifiedFile('TelefoonNummers.js'));

		$page->add(new HtmlHeader(2, new HtmlAnchor($org->url(), $org->getNaam())));
		if ($msg !== null)
			$page->add($msg);
		if ($form)
			if ($msg === null)
				$page->add(OrganisatieView::wijzigForm('OrganisatieWijzig', $org, false));
			else
				$page->add(OrganisatieView::wijzigForm('OrganisatieWijzig', $org, true));
		$page->end();
	}

	/**
	 * Geef een formulier voor het wijzigen van een organisatie terug
	 */
	static public function wijzigForm ($name = 'OrganisatieWijzig', $obj = null, $show_error = true)
	{
		/**
		 * We gaan 2 divs maken:
		 * - Organisatieinformatie
		 * - Contactsinformatie + telefoonnummers + adressen
		 */

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		/** Eerst de div voor persoonsinformatie in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Organisatieinformatie")));
		$div->add(self::wijzigTR($obj, 'Naam', $show_error))
			  ->add(self::wijzigTR($obj, 'Soort', $show_error))
			  ->add(self::wijzigTR($obj, 'Omschrijving', $show_error))
			  ->add(self::wijzigTR($obj, 'Aes2rootsOpsturen', $show_error))
			  ->add(self::wijzigTR($obj, 'VakidOpsturen', $show_error))
			  ->add(self::wijzigTR($obj, 'Homepage', $show_error))
			  ->add(self::wijzigTR($obj, 'Email', $show_error));

		/** Pas ook telefoonnummers aan **/
		$form->add(ContactTelnrVerzamelingView::wijzigTelnrsDiv($obj, $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB()) { 
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		} else {
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));
		}
		return $form;
	}

	/**
	 * Verwerk het wijzig formulier
	 */
	static public function processWijzigForm($obj)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($obj);

		/** Verwerk telefoonnummer(s) **/
		ContactTelnrVerzamelingView::processWijzigTelnrs($obj, $data);

		/** Verwerk adressen **/
		self::processWijzigAdres($obj);

	}

	/**
	 * Verwijder een organisatie;
	 */
	static public function verwijder ($org, $msg)
	{
		$page = static::pageLinks($org);
		$page
			 ->start(null, 'leden')
			 ->add(new HtmlHeader(2, _("Verwijderen") . ' ' . new HtmlAnchor($org->url(), self::waardeNaam($org))));

		if ($msg === null)
		{
			$form = HtmlForm::named('OrganisatieVerwijder');
			$form->add(new HtmlParagraph(_("Weet je het zeker? Dit betekent dat ook de contactpersonen worden verwijderd.")))
				 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Jazeker!"))));
			$page->add($form);
		}
		else
			$page->add($msg);
		$div = new HtmlDiv();
		$page->add($div)
			 ->end();
	}

	public static function waardeNaamFilename(Organisatie $obj)
	{
		list($spaceless) = explode(' ',self::waardeNaam($obj));
		list($pointless) = explode('.', $spaceless);
		return $pointless;
	}

	public static function waardeNaamUrl (Organisatie $obj)
	{
		$naam = static::waardeNaam($obj);
		if ($obj->magBekijken())
			return new HtmlAnchor($obj->url(), $naam);
		return $naam;
	}

	/** @name View: label/waarde/form/opmerking @{ **/
	public static function labelNaam(Organisatie $obj)
	{
		return _('Naam');
	}

	public static function labelSoort(Organisatie $obj)
	{
		return _('Soort');
	}

	public static function labelLogo(Organisatie $obj){
		return _('Logo');
	}

	public static function labelOmschrijving(Organisatie $obj)
	{
		return _('Omschrijving');
	}

	public static function labelenumSoort($value)
	{
		switch($value) {
		case 'ZUS_UU':		return _('Zusje uit Utrecht');
		case 'ZUS_NL':		return _('Landelijk zusje');
		case 'OG_MED':		return _('Overleggroep of medezeggenschap');
		case 'UU':			return _('Universiteit Utrecht');
		case 'VAKGROEP':	return _('Vakgroep');
		case 'BEDRIJF':		return _('Bedrijf');
		case 'OVERIG':		return _('Overig');
		default:			return user_error(_('missende enum case:').$value);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
