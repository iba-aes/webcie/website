<?
abstract class VoornaamwoordView
	extends VoornaamwoordView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in VoornaamwoordView.
	 *
	 * @param obj Het Voornaamwoord-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoornaamwoord(Voornaamwoord $obj)
	{
		return sprintf("%s (%s; %s)",
			$obj->getSoort(),
			static::samenvatting($obj, 'nl'),
			static::samenvatting($obj, 'en')
		);
	}

	/**
	 * @brief Geef alle vervoegingen van het voornaamwoord.
	 *
	 * Deze worden gescheiden door een '/'.
	 *
	 * @param obj Het voornaamwoord om te vervoegen.
	 * @param lang De taal waarin te vervoegen. Default: getLang().
	 *
	 * @return Een string met de vervoegingen.
	 */
	public static function vervoegingen(Voornaamwoord $obj, $lang = NULL) {
		return sprintf("%s/%s/%s/%s",
			$obj->vervoeg("jij", $lang),
			$obj->vervoeg("jou", $lang),
			$obj->vervoeg("jouw", $lang),
			$obj->vervoeg("jezelf", $lang)
		);
	}

	/**
	 * @brief Geef alle inhoud van het voornaamwoord.
	 *
	 * Dat zijn dus vervoegingen en familierelaties.
	 *
	 * @param obj Het voornaamwoord om te samenvatten.
	 * @param lang De taal waarin te vervoegen. Default: getLang().
	 *
	 * @return Een string met de samenvatting.
	 */
	public static function samenvatting(Voornaamwoord $obj, $lang = NULL) {
		return sprintf("%s; %s; %s; %s; %s; %s; %s",
			static::vervoegingen($obj, $lang),
			$obj->getOuder($lang),
			$obj->getKind($lang),
			$obj->getBrusje($lang),
			$obj->getKozijn($lang),
			$obj->getKindGrootouder($lang),
			$obj->getKleinkindOuder($lang)
		);
	}

	/**
	 * @brief Geef een formulier om een nieuw object te maken.
	 *
	 * Indien het object NULL is, wordt die automagisch aangemaakt.
	 * @param obj Het object waar de inhoud gezet moet worden.
	 * @returns Een HTML-formulier om in je pagina te zetten.
	 */
	public static function nieuwForm(Voornaamwoord $obj = null, $showErrors = false) {
		if (is_null($obj)) {
			$obj = new Voornaamwoord();
		}

		$form = HtmlForm::named("nieuwVoornaamwoord", "/Service/Voornaamwoord/Nieuw");
		$form->add(self::createForm($obj, $showErrors));
		$form->add(HtmlInput::makeSubmitButton(_("Nieuw voornaamwoord")));
		return $form;
	}

	/**
	 * @brief Geef een formulier om een object te wijzigen.
	 *
	 * @param obj Het object waar de inhoud gezet moet worden.
	 * @returns Een HTML-formulier om in je pagina te zetten.
	 */
	public static function wijzigForm(Voornaamwoord $obj = null, $showErrors = false) {
		$form = HtmlForm::named("wijzigVoornaamwoord", "/Service/Voornaamwoord/" . $obj->geefID() . "/Wijzig");
		$form->add(self::createForm($obj, $showErrors));
		$form->add(HtmlInput::makeSubmitButton(_("Wijzig voornaamwoord")));
		return $form;
	}

	public static function opmerkingSoort() {
		return _("Op welk gender past dit voornaamwoord?");
	}
	public static function opmerkingOnderwerp() {
		return _("De onderwerpsvorm, zoals 'jij'. Bijvoorbeeld: '() is vandaag jarig.'");
	}
	public static function opmerkingVoorwerp() {
		return _("De voorwerpsvorm, zoals 'jou'. Bijvoorbeeld: 'Vergeet niet () te feliciteren.'");
	}
	public static function opmerkingBezittelijk() {
		return _("De bezittelijke vorm, zoals 'jouw'. Bijvoorbeeld: 'Gefeliciteerd met () verjaardag.'");
	}
	public static function opmerkingWederkerend() {
		return _("De wederkerende vorm, zoals 'jezelf'. Bijvoorbeeld: 'De Webcie'er maakt () herkenbaar door een feesthoedje op te doen.'");
	}
	public static function opmerkingOuder() {
		return _("Hoe je heet als ouder van iemand. Bijvoorbeeld 'Nee, Luuk, ik ben je ().'");
	}
	public static function opmerkingKind() {
		return _("Hoe je heet als kind van iemand. Bijvoorbeeld 'Met mijn mentorpartner heb ik 15 gezonde mentor()'en gekregen.'");
	}
	public static function opmerkingBrusje() {
		return _("Hoe je heet als kind van iemands ouders. Bijvoorbeeld 'Het motto van IBA is vrijheid, ()schap en bugs oplossen.'.");
	}
	public static function opmerkingKozijn() {
		return _("Hoe je heet als kleinkind van iemands grootouders. Bijvoorbeeld 'Albert Einstein en zijn vrouw waren ()'en'.");
	}
	public static function opmerkingKindGrootouder() {
		return _("Hoe je heet als kind van iemands grootouders. Bijvoorbeeld 'Ik heb een () uit Marokko en die komt, hiep hoi.'");
	}
	public static function opmerkingKleinkindOuder() {
		return _("Hoe je heet als kleinkind van iemands ouders. Bijvoorbeeld 'Je lijkt wel Donald Duck met al die () in je huis wonen.' Hier wordt '%s' vervangen met de naam van dit originele kind van je grooutouders, zodat '%szegger' bijvoorbeeld 'oomzegger' wordt.");
	}

	/**
	 * @brief Geef een formulieronderdeel om voornaamwoorden te selecteren.
	 *
	 * @param obj Het huidig geselecteerde Voornaamwoord.
	 * @return Een HTML-object dat je in een form kan stoppen.
	 * @see genericDefaultForm
	 */
	public static function defaultForm($obj) {
		return static::genericDefaultForm("Voornaamwoord", $obj->geefID());
	}

	/**
	 * @brief Geef een formulieronderdeel om voornaamwoorden te selecteren.
	 *
	 * @param name De naam van het formulieronderdeel.
	 * @param waarde De waarde die het formulieronderdeel mee begint, of NULL voor default.
	 * @return Een HTML-object dat je in een form kan stoppen.
	 * @see defaultForm
	 */
	public static function genericDefaultForm($naam, $waarde=NULL) {
		$opties = VoornaamwoordQuery::table()->verzamel();
		$optieArray = array();

		foreach ($opties as $optie) {
			$optieArray[$optie->geefID()] = static::defaultWaardeVoornaamwoord($optie);
		}
		return HtmlSelectbox::fromArray($naam, $optieArray, $waarde);
	}
}
