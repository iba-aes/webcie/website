<?

/**
 * $Id$
 */
abstract class TinyUrlVerzamelingView
	extends TinyUrlVerzamelingView_Generated
{

	static public function lijst(TinyUrlVerzameling $verz)
	{
		$div = new HtmlDiv();
		
		$zoektekst = tryPar('zoektekst');
		
		$div->add(new HtmlDiv($form = new HtmlForm("GET", null, true)));
		$form->setAttribute('name', 'tinyUrlselect');

		$form->add(new HtmlDiv(array(
			new HtmlLabel('zoektekst', _("Zoek op tinyUrl"), "col-sm-2 control-label"),
			new HtmlDiv(HtmlInput::makeText("zoektekst", $zoektekst), "col-sm-10")
		), "form-group"));

		$form->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeSubmitButton("Zoek!"), "col-sm-offset-2 col-sm-10"), "form-group"));
		
		$div->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("ID"), _("TinyUrl"), _("OutgoingUrl"), _("Hits"),
			_("# koppelingen")));

		foreach($verz as $tinyUrl)
		{
			$row = $tbody->addRow();
			$row->addData(TinyUrlView::makeLink($tinyUrl));
			$row->addData(TinyUrlView::waardeTinyUrl($tinyUrl));
			$row->addData(TinyUrlview::waardeOutgoingUrl($tinyUrl));
			$row->addData(TinyUrlview::waardeHits($tinyUrl));
			$row->addData(TinyMailingUrlVerzameling::vanUrl($tinyUrl)->aantal());

			if(hasAuth('bestuur')) {
				$row->addData(new HtmlAnchor($tinyUrl->verwijderURL()
					, HtmlSpan::fa('times', _('Verwijderen'), 'text-danger')));
			}
		}

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
