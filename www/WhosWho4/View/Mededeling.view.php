<?

/**
 * $Id$
 */
abstract class MededelingView
	extends MededelingView_Generated
{
	static public function makeLink(Mededeling $mdd, $caption = null, $action = null)
	{
		if (!$action)
			$action = "";
		else
			$action = "/$action";

		if(!$caption)
			$caption = MededelingView::waardeOmschrijving($mdd);

		return new HtmlAnchor("/Vereniging/Nieuws/" . $mdd->getMededelingID() . $action, $caption);
	}

	static public function details (Mededeling $med)
	{
		self::pageLinks($med)
			->start(htmlspecialchars(self::waardeOmschrijving($med)), 'vereniging')
			->add(MededelingView::maakMededelingDiv($med, true, false))
			->end();
	}

	/**
	 * Geeft een RSS-item terug
	 */
	static public function rssItem (Mededeling $m)
	{
		$item = new Rss_Item();
		$item->setTitle(self::waardeOmschrijving($m))
			->setDescription(self::waardeMededeling($m))
			->setPubdate($m->getDatumBegin()->format(DateTime::RSS))
			->setLink(HTTPS_ROOT . $m->url());
		return $item;
	}

	static function maakMededelingTR (Mededeling $mededeling)
	{
		$row = new HtmlTableRow();
		$omschrijving = static::waardeOmschrijving($mededeling);
		if ($mededeling->magWijzigen())
			$omschrijving = static::makeLink($mededeling, $omschrijving);
		$titel = $row->addData($omschrijving);
		if (hasAuth('lid'))
			$row->addData(static::waardeDoelgroep($mededeling));
		$row->addData(static::waardePrioriteit($mededeling));
		$row->addData(static::waardeDatumBegin($mededeling));
		$row->addData(static::waardeDatumEind($mededeling));
		return $row;
	}

	static function maakMededelingDiv(Mededeling $mededeling, $toonDetails = false, $toonHeader = true)
	{
		$div = new HtmlDiv();

		$div->add($mdiv = new HtmlDiv(null, 'panel panel-default'));
		$omschrijving = static::waardeOmschrijving($mededeling);
		$hdr = new HtmlHeader(4, $omschrijving);

		if($toonHeader) {
			if($mededeling->magWijzigen())
				$mdiv->add(new HtmlDiv(MededelingView::makeLink($mededeling, $hdr), 'panel-heading'));
			else
				$mdiv->add(new HtmlDiv($hdr, 'panel-heading'));
		}

		$tekst = static::waardeMededeling($mededeling);
		$mdiv->add($body = new HtmlDiv($tekst, 'panel-body'));
		$linknaam = $mededeling->getUrl();

		if ($linknaam != null)
		{
			$body->add($div_url = new HtmlDiv(self::waardeURL($mededeling)));
		}

		if ($toonDetails)
		{
			$div->add(new HtmlBreak());
			$div->add($details = new HtmlDiv(null, 'details'));
			$details->add(static::viewInfo($mededeling));
		}

		return $div;
	}


	static public function maakFormPagina ($act, $nieuw = true, $show_error)
	{
		$page = Page::getInstance();
		if ($nieuw)
			$page->start(_("Mededeling toevoegen"));
		else
			$page->start(_("Mededeling wijzigen"));
		$page->add(MededelingView::nieuwForm('MedNieuw', $act, $show_error));
		$page->end();
	}

	static public function nieuwForm ($name = 'MedNieuw', $obj = null, $show_error = true, $nieuw = true)
	{
		/**
		 * We gaan 2 divs maken:
		 * - verantwoordelijke commissies
		 * - statische informatie
		 */

		/** Als er nog geen object is meegestuurd, dan maken we zelf wel iets **/
		if (is_null($obj))
			$obj = new Mededeling();

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);
		$form->add(self::createForm($obj, $show_error, $nieuw ? 'nieuw' : 'viewWijzig'));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton( $obj->getInDB()
			?  _("Wijzig de mededeling") : _("Maak de mededeling aan!")));

		return $form;
	}
	
	static public function processNieuwForm (Mededeling $med)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($med), NULL))
			return false;

		/** Verwerk de geselecteerde commissies **/
		if (isset($data['commissie']))
		{
			$commissies = Commissie::geef($data['commissie']);
			$med->setCommissie($commissies);
		}
		
		/** Vul alle statische informatie in **/
		self::processForm($med);
	}

	public static function labelCommissie (Mededeling $obj = NULL)
	{
		return _('Commissie');
	}
	public static function labelDatumBegin (Mededeling $obj = NULL)
	{
		return _('Begindatum');
	}
	public static function labelDatumEind (Mededeling $obj = NULL)
	{
		return _('Einddatum');
	}

	public static function labelUrl (Mededeling $obj = NULL)
	{
		return _("Bijbehorende link");
	}

	public static function opmerkingUrl()
	{
		return _("Mag een relatieve url zijn (bv /Activiteiten/Fotos)");
	}

	public static function labelDoelgroep (Mededeling $obj = NULL)
	{
		return _('Doelgroep');
	}	

	public static function labelenumDoelgroep($value)
	{
		switch ($value)
		{
			case 'ALLEN': return _('Iedereen');
			case 'LEDEN': return _('Leden');
			case 'ACTIEF': return _('Actieve leden');
		}
		return parent::labelenumSoort($value);
	}

	public static function labelMededeling (Mededeling $obj = NULL)
	{
		return _('Mededeling');
	}

	public static function labelOmschrijving (Mededeling $obj = NULL)
	{
		return _('Omschrijving');
	}

	public static function labelPrioriteit (Mededeling $obj = NULL)
	{
		return _('Prioriteit');
	}

	public static function labelenumPrioriteit($value)
	{
		switch ($value)
		{
			case 'LAAG': return _('Laag');
			case 'NORMAAL': return _('Normaal');
			case 'HOOG': return _('Hoog');
		}
		return parent::labelenumSoort($value);
	}

	public static function waardeCommissie (Mededeling $obj)
	{
		if ($cie = $obj->getCommissie())
			return CommissieView::makeLink($cie);
		return NULL;
	}

	public static function waardeOmschrijving (Mededeling $obj)
	{
		$omschrijving = parent::waardeOmschrijving($obj);
		if(empty($omschrijving) && getLang() == 'en')
			$omschrijving = htmlspecialchars($obj->getOmschrijving('nl'));
		return $omschrijving;
	}

	public static function waardeMededeling (Mededeling $obj)
	{
		$tekst = parent::waardeMededeling($obj);
		if(empty($tekst) && getLang() == 'en')
			$tekst = new HtmlEmphasis('[No translation available]&nbsp;') .
			new HtmlSpan(htmlspecialchars($obj->getMededeling('nl')));
		return $tekst;
	}

	public static function waardeOmschrijvingLink(Mededeling $obj)
	{
		return self::makeLink($obj, self::waardeOmschrijving($obj));
	}

	static public function formCommissie(Mededeling $obj, $include_id = false)
	{
		return self::defaultFormCommissieVerzameling($obj, 'commissie', $obj->getCommissieCommissieID(), false, true);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
