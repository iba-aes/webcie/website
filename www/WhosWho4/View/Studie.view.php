<?
/**
 * $Id$
 */
abstract class StudieView
	extends StudieView_Generated
{
	static public function defaultWaardeStudie(Studie $studie)
	{
		return self::waardeNaam($studie);
	}

	static public function toTableRow (Studie $studie)
	{
		$row = new HtmlTableRow();
		$row->addData(static::maakLink($studie));
		$row->addData(static::waardeSoort($studie));
		$row->addData(static::waardeFase($studie));
		$row->addData(new HtmlAnchor(
			$studie->url() . '/Studenten',
			static::waardeAantalStudenten($studie)));
		return $row;
	}

	static public function weergaveDiv (Studie $studie)
	{
		$div = new HtmlDiv();
		$div->add(static::viewInfo($studie, true));
		return $div;
	}

	static public function maakLink (Studie $studie)
	{
		return new HtmlAnchor(
			'/Leden/Studie/' . $studie->geefID(),
			static::waardeNaam($studie));
	}

	static public function wijzigStudie(Studie $studie, $show_error = false)
	{
		$page = Page::getInstance();
		$page->start();
		$page->add(HtmlAnchor::button($studie->url(), _('Terug naar de studie')));

		$page->add($form = HtmlForm::named('wijzigStudie'));

		$form->add(self::wijzigTR($studie, 'naam', $show_error));

		$form->add(HtmlInput::makeFormSubmitButton('Voer door'));
		$page->end();
	}

	static public function labelNaam (Studie $studie = NULL)
	{
		return _('Naam');
	}
	static public function labelSoort (Studie $studie = NULL)
	{
		return _('Soort');
	}
	static public function labelFase (Studie $studie = NULL)
	{
		return _('Fase');
	}
	static public function labelAantalStudenten (Studie $studie = NULL)
	{
		return _('Aantal studenten');
	}

	static public function waardeNaamLang (Studie $studie)
	{
		return static::waardeFase($studie) . ' ' . static::waardeNaam($studie) .
			($studie->getFase() == 'MA' ? ' [' . static::waardeSoort($studie) . ']' : '');
	}

	static public function waardeNaamKort (Studie $studie)
	{
		return static::waardeFase($studie) . ' ' .
			static::waardeNaam($studie);
	}

	static public function waardeSoort (Studie $studie)
	{
		return self::labelenumSoort($studie->getSoort());
	}

	public static function labelenumSoort($value)
	{
		switch ($value)
		{
		case 'GT':  return _('Gametechnologie');
		case 'IC':  return _('Informatica');
		case 'IK':  return _('Informatiekunde');
		case 'NA':  return _('Natuurkunde');
		case 'WI':  return _('Wiskunde');
		case 'WIT': return _('Wiskunde en toepassingen');
		case 'OVERIG': return _('Overig');
		}
	}

	static public function waardeFase (Studie $studie)
	{
		switch ($studie->getFase())
		{
		case 'BA': return _('Bachelor');
		case 'MA': return _('Master');
		}
	}

	static public function waardeAantalStudenten (Studie $studie, $status = NULL)
	{
		return $studie->getAantalStudenten($status);
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
