<?

/**
 * $Id$
 */
abstract class ContactPersoonVerzamelingView
	extends ContactPersoonVerzamelingView_Generated
{

	/**
	 *  Geef een lijst van bedrijfscontacten weer.
	 **/
	public static function htmlListBedrijfContact($contactpersonen, $tot = true)
	{
		if($contactpersonen->aantal() == 0)
			return new HtmlParagraph(_("Er is niemand om weer te geven."));

		$page = new HtmlDiv();

		$vansinds = _('Van');
		if(!$tot)
			$vansinds = _('Sinds');

		$table = new HtmlTable(null, 'sortable');
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("Naam"), ($tot?_("Tot"):null), _("Telefoon"),
			_("Mobiel"), _("Email"), _("Opmerking")));

		foreach($contactpersonen as $contactpersoon){
			$werknummer = ContactTelnrVerzameling::vanContact($contactpersoon->getPersoon(), 'WERK');
			$mobiel = ContactTelnrVerzameling::vanContact($contactpersoon->getPersoon(), 'MOBIEL');

			$row = $tbody->addRow();
			$row->addData(new HtmlAnchor($contactpersoon->url(),
							PersoonView::naam($contactpersoon->getPersoon())));
			$row->addData(ContactPersoonView::waardeBeginDatum($contactpersoon) );
			$row->addData($tot?ContactPersoonView::waardeEindDatum($contactpersoon):null);
			$row->addData($werknummer->aantal() > 0?ContactTelnrView::waardeTelefoonnummer($werknummer->current()):'');
			$row->addData($mobiel->aantal() > 0?ContactTelnrView::waardeTelefoonnummer($mobiel->current()):'');
			$row->addData($contactpersoon->getPersoon()->getEmail());
			$row->addData(ContactPersoonView::waardeOpmerking($contactpersoon));

		}

		$page->add($table);

		return $page;
	}

	public static function htmlListSpookweb($contactpersonen)
	{
		if($contactpersonen->aantal() == 0)
			return new HtmlParagraph(_("Er zijn geen spoken om weer te geven."));

		$page = new HtmlDiv();

		$table = new HtmlTable(null, 'sortable');
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("Naam"), _("Bedrijf"), _("Telefoon"),
			_("Mobiel"), _("Email"), _("Opmerking")));

		foreach($contactpersonen as $contactpersoon){
			if(isSpook($contactpersoon) || isOverigSpookweb($contactpersoon)){
				$eerstenummer = 'THUIS';
			} else {
				$eerstenummer = 'WERK';
			}
			$werknummer = ContactTelnrVerzameling::vanContact($contactpersoon->getPersoon(), $eerstenummer);
			$mobiel = ContactTelnrVerzameling::vanContact($contactpersoon->getPersoon(), 'MOBIEL');
			$bedrijf = $contactpersoon->getOrganisatie();

			$row = $tbody->addRow();
			$row->addData(new HtmlAnchor($contactpersoon->url(),
							PersoonView::naam($contactpersoon->getPersoon())));
			$row->addData($bedrijf?new HtmlAnchor($bedrijf->url(),$bedrijf->getNaam()):'');
			$row->addData($werknummer->aantal() > 0?ContactTelnrView::waardeTelefoonnummer($werknummer->current()):'');
			$row->addData($mobiel->aantal() > 0?ContactTelnrView::waardeTelefoonnummer($mobiel->current()):'');
			$row->addData($contactpersoon->getPersoon()->getEmail());
			$row->addData(ContactPersoonView::waardeOpmerking($contactpersoon));
		}

		$page->add($table);

		return $page;
	}

	/**
	 *  Functies die een spook of bedrijfscontactpersoon weergeven, ze kunnen
	 *  namelijk bestaan uit verzamelingen die een enkel entiteit representeren.
	 *  TODO: zorgen dat het ook echt met verzamelingen gaat werken.
	 **/
	public static function spookDetails(ContactPersoonVerzameling $spook)
	{
		$interactie = InteractieVerzameling::metConstraints(null, null, $spook->first()->getPersoonContactID());
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(3, sprintf(_("Alle interacties van %s"), PersoonView::naam($spook->first()->getPersoon()))))
			->add(InteractieVerzamelingView::htmlList($interactie));
		return $div;
	}

	public static function bedrijfscontactDetails(ContactPersoonVerzameling $bedrijfscontact)
	{
		$interactie = InteractieVerzameling::metConstraints(null, null, null, $bedrijfscontact->first()->getPersoonContactID());
		$div = new HtmlDiv();
		$div->add(ContactPersoonView::viewInfo($bedrijfscontact->first()))
			->add(InteractieVerzamelingView::htmlList($interactie));
		return $div;
	}

	// Geeft een (klikbare) string terug met alle namen van de contactpersonenen
	// in de verzameling plus een link om hun gegevens te wijzigen.
	// VB: Tom Janmaat (wijzig), 
	// Tom Janmaat linkt naar /Spook/6766/9196/
	// wijzig linkt naar /Spook/6766/9196/Wijzig
	public static function stringLijst(ContactPersoonVerzameling $contactpersonen)
	{
		if($contactpersonen->aantal() == 0)
			return _("geen");

		$naamArray = array();
		foreach ($contactpersonen as $contactpersoon)
		{
			if($contactpersoon->magBekijken())
			{
				$naamArray[] = new HtmlAnchor($contactpersoon->url(),PersoonView::naam($contactpersoon->getPersoon()));
				$naamArray[] = ' &nbsp (';
				$naamArray[] = new HtmlAnchor($contactpersoon->url()."/".'Wijzig','wijzig');
				$naamArray[] = ') </br>';
			} else {
				$naamArray[] = PersoonView::naam($contactpersoon->getPersoon());
			}
		}

		return implode($naamArray);
	}

	public static function htmlSelectBox($obj, ContactPersoonVerzameling $contactpersonen, $veld, $selected = array(), $multiple = false)
	{
		if($selected instanceof ContactPersoonVerzameling)
			$selected = $selected->keys();

		$options = array();

		if($obj instanceof Interactie && !$obj->getBedrijfPersoon() && $obj->getInDB() )
			$options[0] = _("Onbekend");

		foreach($contactpersonen as $contactpersoon)
		{
			$options[$contactpersoon->geefID()] = PersoonView::naam($contactpersoon->getPersoon());
		}

		$name = self::formveld($obj, $veld);
		$sel = HtmlSelectbox::fromArray($name, $options, $selected);
		if($multiple) {
			$sel->setMultiple();
		} else {
			$sel->setSize(1);
		}
		return $sel;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
