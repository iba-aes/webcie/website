<?
/**
 * Let op: geen standaard WSW4-object in de zin van geen koppeling met db
 */
abstract class KartView
	extends View
{
	// In een afzenderlijst zit deze prefix voor de naam van deze optie om aan
	// te geven dat het een functie van het bestuur betreft
	const BESTUURFUNCTIE_PREFIX = "BESTUURFUNCTIE_";

	public static function overzicht ($form = null, $prefix = '')
	{
		$kart = Kart::geef();
		$aantalPersonen = $kart->getAantalPersonen();
		$cies = $kart->getCies();
		$aantalCies = $cies->aantal();

		if (is_null($form))
		{
			$div = new HtmlDiv();

			$div->add(PersoonKartView::kartOverzicht());
		}
		else
		{
			Page::getInstance()->addFooterJS(Page::minifiedFile('KartOverzicht.js'));
			$div = HtmlForm::named($form)
				->setAttribute('name', 'KartLijst')
				->add(HtmlInput::makeHidden('Kart[Actie]', ''));

			$div->add($btn_group = new HtmlDiv(null, 'btn-group'));
			$btn_group->add(HtmlAnchor::button('Toevoegen',
				($aantalPersonen !== 0 || $aantalCies !== 0) ? _('Voeg meer personen/cies toe') : _('Voeg personen/cies toe')));

			if($aantalPersonen !== 0 || $aantalCies !== 0)
				$btn_group->add(HtmlAnchor::button('KartOpslaan', _('Huidige kart opslaan')));

			$div->add(PersoonKartView::kartOverzicht());
		}


		if ($aantalPersonen > 0)
		{
			$div->add(new HtmlHeader(3, sprintf(_('Personen in je kart: %d'), $aantalPersonen)));

			$div->add(new HtmlDiv(PersoonVerzamelingView::lijst($kart->getPersonen(), $form, $kart->getPersonen()->keys(), $prefix), NULL, 'kart_personenlijst'));

			$menu = array();
			$menu[] = array(_('Toon koppenblad'), new HtmlAnchor('Koppenblad', HtmlSpan::fa('smile-o', _('Koppenblad'))));
			$menu[] = array(_('Toon printbaar koppenblad'), new HtmlAnchor('Koppenblad?printbaar=true', HtmlSpan::fa('smile-o', _('Koppenblad'))));
			$menu[] = array(_('Toon printbaar koppenblad zonder naam'), new HtmlAnchor('Koppenblad?printbaar=true&zondernamen=true', HtmlSpan::fa('smile-o', _('Koppenblad'))));

			if (hasAuth('bestuur'))
			{
				$menu[] = array(_('Exporteer LaTeX'), new HtmlAnchor('Latex', HtmlSpan::fa('file-excel-o', _('LaTeX'))));
				$menu[] = array(_('Exporteer CSV'), new HtmlAnchor('LedenCSV', HtmlSpan::fa('file-archive-o', _('CSV'))));
			}

			if (hasAuth('actief'))
			{
				$menu[] = array(_('Mail deze personen'), new HtmlAnchor('#', HtmlSpan::fa('send', _('Mailen')), 'Kart_Overzicht_Personen_Mailen()'));
			}
			$menu[] = array(_('Haal deze personen uit je kart'),  new HtmlAnchor('#', HtmlSpan::fa('times-circle', _('Haal deze personen uit je kart'), 'text-danger'), 'Kart_Overzicht_Personen_Uitschrijven()'));

			$actieTabel = new HtmlTable();
			$tbody = $actieTabel->addBody();

			foreach ($menu as $menuItem)
			{
				$tbody->add($row = new HtmlTableRow());
				$row->addData($menuItem[0]);
				$row->addData($menuItem[1]);
			}

			$div->add($actieTabel);
		}

		if ($aantalPersonen > 0 && $aantalCies > 0)
		{
			$div->add(new HtmlHR());
		}

		if ($aantalCies > 0)
		{
			$div->add(new HtmlHeader(3, sprintf(_('Commissies in je kart: %d'), $cies->aantal())));

			$div->add(new HtmlDiv(CommissieVerzamelingView::kartLijst($cies, $prefix), NULL, 'kart_cieslijst'));

			$menu = array();

			if (hasAuth('bestuur'))
			{
				$menu[] = array(_('Exporteer LaTeX'), new HtmlAnchor('CiesLatex', HtmlSpan::fa('file-excel-o', _('LaTeX'))));
				$menu[] = array(_('Exporteer CSV'), new HtmlAnchor('CiesCSV', HtmlSpan::fa('file-archive-o', _('CSV'))));
			}

			if (hasAuth('actief'))
			{
				$menu[] = array(_('Mail deze commissies'), new HtmlAnchor('#', HtmlSpan::fa('send', _('Mailen')), 'Kart_Overzicht_Cies_Mailen()'));
			}
			$menu[] = array(_('Haal deze commissies uit je kart'),  new HtmlAnchor('#', HtmlSpan::fa('times-circle', _('Haal deze commissies uit je kart'), 'text-danger'), 'Kart_Overzicht_Cies_Uitschrijven()'));

			$actieTabel = new HtmlTable();
			$tbody = $actieTabel->add(new HtmlTableBody());
			foreach ($menu as $menuItem)
			{
				$row = $tbody->addRow();
				$row->addData($menuItem[0]);
				$row->addData($menuItem[1]);
			}

			$div->add($actieTabel);
		}

		if ($aantalPersonen == 0 && $aantalCies == 0)
		{
			$div->add(new HtmlSpan(new HtmlEmphasis(_('Je kart is leeg.'))));
		}

		return $div;
	}

	/**
	 *  Maak een form-divje voor een lijst lidnummers.
	 * Bevat ook wat opmerkingen om het doel duidelijk te maken.
	 */
	public static function formDivLidNummers() {
		$divje = new HtmlDiv();
		$opmerking = new HtmlParagraph(_("Voeg toe op basis van lidnummers: (gescheiden door spaties, komma's en/of regels)"));
		$input = new HtmlTextarea('lidnrs');
		$divje
			->add($opmerking)
			->add($input);

		return $divje;
	}

	public static function nieuwForm ($name)
	{
		$form = HtmlForm::named($name);
		$form->addClass('form-inline');
		$form->add(new HtmlHeader(4, _("Selecteer welke personen je aan je kart wilt toevoegen.")))
			->add(self::defaultFormPersoonVerzameling(NULL, "LidVerzameling"))
			->add(self::formDivLidNummers())
			->add(new HtmlHeader(4, _("Selecteer welke commissies je aan je kart wilt toevoegen.")))
			->add(self::genericDefaultFormCommissieVerzameling('CommissieVerzameling', null, tryPar('CommissieVerzameling', array()), true))
			->add(new HtmlBreak())->add(new HtmlBreak())
			->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Voeg toe!"))));
		return $form;
	}

	/**
	 * Stop mensen via formulier in de kart.
	 * @return Of er daadwerkelijk mensen ingestopt zijn
	 */
	public static function processNieuwForm ()
	{
		/** Haal alle data op **/
		$data = tryPar('LidVerzameling', NULL);
		$ciedata = tryPar('CommissieVerzameling', NULL);
		if (!$data && !$ciedata)
			return false;

		/** Verwerk ids **/
		$ids = array();
		if($data) {
			foreach ($data as $id => $derp)
				if ($derp)
					$ids[] = $id;
		}
		$cies = array();
		if($ciedata) {
			$cies = $ciedata;
		}

		// ga ook het veld met vrijere invoer van lidnummers na
		$lidnrs = tryPar('lidnrs', NULL);
		if ($lidnrs) {
			// split op whitespace of komma's
			// door stom toeval letterlijk wat in de php docs staat
			$lidnrsGesplit = preg_split('/[\s,]+/', $lidnrs);
			foreach ($lidnrsGesplit as $lidnr) {
				// het moet wel op een int lijken
				$id = (int)$lidnr;
				if ($id) {
					$ids[] = $id;
				}
			}
		}

		/** Stop alles in de kart **/
		Kart::geef()->voegPersonenToe($ids);
		Kart::geef()->voegCiesToe($cies);

		return count($ids) > 0 || count($cies) > 0;
	}

	public static function mailForm (Verzameling $geadresseerden, Email $mail, $form, $msg, $type)
	{
		global $BESTUUR;

		if ($type == 'Persoon')
		{
			$actie = 'personen_mailen';
			$geselecteerdeInput = 'Geselecteerde';
		}
		elseif ($type == 'Commissie')
		{
			$actie = 'cies_mailen';
			$geselecteerdeInput = 'GeselecteerdeCie';
		}
		else
		{
			user_error(sprintf('Ongeldig type %s!'), $type);
		}

		$page = Page::getInstance()
			->start()
			->add(new HtmlParagraph(HtmlAnchor::button('/Service/Kart', _('Terug naar de kart'))));
		if ($msg !== null)
			$page->add($msg);
		if ($form)
		{
			if ($geadresseerden->aantal() > 0)
			{
				$page->add(new HtmlHeader(3, _('Geadresseerden:')))
					->add($ul = new HtmlList());
				foreach ($geadresseerden as $geadresseerde)
					$ul->add(geefNaam($geadresseerde));
			}

			$page->add(new HtmlHeader(3, _('Headers')));
			$frm = HtmlForm::named('Kart');

			//Geef de gebruiker lekker lang (1h) te tijd om een mailtje te typen:
			$frm->getToken()->setTTL(3600);

			$frm->add(HtmlInput::makeHidden('Kart[Actie]', $actie));
			foreach ($geadresseerden as $geadresseerde)
				$frm->add(HtmlInput::makeHidden(
					sprintf('Kart[%s][%s]', $geselecteerdeInput, $geadresseerde->geefID())
					, 'true'));

			$frm->add($table = new HtmlTable());
			$table->add($thead = new HtmlTableHead())
				->add($tbody = new HtmlTableBody());

			$thead->add($tr = new HtmlTableRow());
			$tr->addHeader(_('Wat'));
			$tr->addHeader(_('Gegevens'));

			$tbody->add($tr = new HtmlTableRow());
			$tr->addData(_('Vanaf:'));

			$ingelogd = Persoon::getIngelogd();
			$ingelogdMail = ContactView::waardeEmail($ingelogd);

			// Construeer de selectbox opties
			$mailLijst = array(
				$ingelogdMail => (PersoonView::naam($ingelogd) .' &lt;'.  $ingelogdMail .'&gt;')
			);

			if(hasAuth('bestuur')) {
				$bestuurFuncties = array();
				foreach ($BESTUUR as $functie => $lid) {
					$naam = self::BESTUURFUNCTIE_PREFIX . $lid->getFunctie();
					$bestuurFuncties[$naam] = htmlspecialchars($lid->getEmail());
				}
				$mailLijst[_("Bestuursfunctie")] = $bestuurFuncties;

				$mijnCies = CommissieVerzameling::huidige('ALLE');
			} else {
				$mijnCies = CommissieVerzameling::vanPersoon($ingelogd);
			}

			$mailCieLijst = array();
			foreach($mijnCies as $cie) {
				$mailCieLijst[$cie->getLogin()] = CommissieView::waardeNaam($cie) .' &lt;'. $cie->getEmail() . '&gt;';
			}
			$mailLijst[_("Commissie")] = $mailCieLijst;

			$tr->addData(HtmlSelectbox::fromArray('from', $mailLijst, array(), 1));

			$tbody->add($tr = new HtmlTableRow());
			$tr->addData(_('Reply-to:'));
			$tr->addData(HtmlInput::makeText('Kart[replyto]', $mail->getReplyTo()));

			$tbody->add($tr = new HtmlTableRow());
			$tr->addData(_('Onderwerp:'));
			$tr->addData(HtmlInput::makeText('Kart[subject]', $mail->getSubject()));

			$tbody->add($tr = new HtmlTableRow());
			$tr->addData(_('Bijlagen:'));
			$tr->addData(HtmlInput::makeFile('Kart[attachments][]', null, true));

			$frm->add(new HtmlHeader(3, _('Inhoud')))
				->add($body = new HtmlTextarea('Kart[body]', null, 'Kart[body]'));
			$body->add($mail->getBody())
				->setAttribute('data-ckeditor', 'true');

			$p = new HtmlParagraph(_('De volgende substituties zijn mogelijk:'));
			$p->add($ul = new HtmlList());
			foreach(Email::substituties($geadresseerden->first()) as $key=>$value)
				$ul->add($key . ' &rarr; ' . $value);

			$frm->add(HtmlInput::makeSubmitButton(_('Verstuur mail!'), 'KartMail'));
			$page->add($frm)
				->add($p);
		}
		$page->end();
	}

	public static function ciecsv() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetCiesHebben($kart);

		header("Content-Disposition: attachment; filename=kart_cies.csv");
		header("Content-type: text/csv; encoding=utf-8");
		CommissieVerzamelingView::csv($kart->getCies());
		exit();
	}

	public static function cielatex() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetCiesHebben($kart);

		header("Content-Disposition: attachment; filename=kart_cies.tex");
		header("Content-type: text/tex; encoding=utf-8");
		CommissieVerzamelingView::latex($kart->getCies());
		exit();
	}

	public static function koppen() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetPersonenHebben($kart);

		Page::getInstance(tryPar('printbaar', false) ? 'cleanhtml' : null)
			->start(_("Koppenblad Kart"))
			->add(new HtmlParagraph(HtmlAnchor::button('/Service/Kart/', _('Terug naar kart'))))
			->add(PersoonVerzamelingView::koppen($kart->getPersonen(), !tryPar('zondernamen', false), tryPar('printbaar', false)))
			->end();
	}

	public static function ledenlatex() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetPersonenHebben($kart);

		header("Content-Disposition: attachment; filename=kart_personen.tex");
		header("Content-type: text/tex; encoding=utf-8");
		PersoonVerzamelingView::latex($kart->getPersonen());
		exit();
	}

	public static function ledencsv() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetPersonenHebben($kart);

		header("Content-Disposition: attachment; filename=kart_personen.csv");
		header("Content-type: text/csv; encoding=utf-8");
		PersoonVerzamelingView::csv($kart->getPersonen());
		exit();
	}

	public static function etiketten() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetPersonenHebben($kart);

		PersoonVerzamelingView::etiketten($kart->getPersonen());
		exit();
	}

	public static function telefoonlijst() {
		$kart = Kart::geef();

		//Ik wil een aspect zijn
		self::moetPersonenHebben($kart);

		PersoonVerzamelingView::telefoonlijst($kart->getPersonen());
	}

	//AARGH, ik wil aspects kunnen geven aan al die views
	private static function moetPersonenHebben($kart){
		if($kart->getPersonen()->aantal()==0) {
			Page::getInstance()
				->start()
				->add(new HtmlHeader(2, _('Er zitten geen mensen in je kart.')))
				->end();
		}
	}

	private static function moetCiesHebben($kart){
		if($kart->getCies()->aantal()==0) {
			Page::getInstance()
				->start()
				->add(new HtmlHeader(2, _('Er zitten geen commissies in je kart.')))
				->end();
		}
	}

	public static function fotoKart($kart) {
		$div = new HtmlDiv(null, 'kartpanel');
		if($kart->getMedia()->aantal() != 0) {
			$div->add(new HtmlHeader(3, _("Foto's in je kart")));
			$anch = new HtmlAnchor('#','je kart leegmaken');
			$anch->addClass('clickEmptyKart');
			$div->add(new HtmlDiv(new HtmlDiv(sprintf(_("Er zit op dit moment %s foto's in je kart. Klik %s. Je kunt ook %s. Uiteraard kun je ook een bewerking toepassen op de foto's in je kart."),$kart->getMedia()->aantal(),new HtmlAnchor('/FotoWeb/Kart',"om 't te bekijken"),$anch), 'panel-body'), 'panel panel-default'));

			$div->add($rowdiv = new HtmlDiv(null, 'row'));

			$rowdiv->add($divrechts = new HtmlDiv(null, 'col-md-6'));
			$rowdiv->add($divlinks = new HtmlDiv(null, 'col-md-6'));

			$divrechts->add(new HtmlHeader(4, _("Personen")));
			$divlinks->add(new HtmlHeader(4, _("Overig")));

			$divrechts->add($subtable = new HtmlTable());
			if(hasAuth('vicie')) {
				$subtable->add($row2 = new HtmlTableRow());
				$row2->add(new HtmlTableDataCell($form = new HtmlForm('POST')));
				$form->addClass('form-inline');
				$form->setAttribute('name','LidVerzamelingKartPanel');

				$form->add(_("Zet de fotograaf naar:"));
				$form->add($search = HtmlInput::makeSearch('LidVerzamelingZoeken'));
				$search->addClass('zoekLidVerzamelingFotoKart');
				$form->add($select = HtmlSelectbox::fromArray('zoeken',array(),array(),1));
				$select->setAttribute('id',"LidVerzamelingKartPanel_select");
				$form->add($button = HtmlInput::makeButton('submit', 'Doen'));
				$button->addClass('clickFotograafKartPanel');
			}

			$subtable->add($row2 = new HtmlTableRow());
			$row2->add(new HtmlTableDataCell($form = new HtmlForm('POST')));
			$form->addClass('form-inline');
			$form->setAttribute('name','LidVerzamelingKartPanelTag');

			$form->add(_("Tag een persoon:"));
			$form->add($search = HtmlInput::makeSearch('LidVerzamelingZoeken'));
			$search->addClass('zoekLidVerzamelingFotoKartTag');
			$form->add($select = HtmlSelectbox::fromArray('zoeken',array(),array(),1));
			$select->setAttribute('id',"LidVerzamelingKartPanelTag_select");
			$form->add($button = HtmlInput::makeButton('submit', '*Tag!*'));
			$button->addClass('clickTagKartPanel');

			if(hasAuth('vicie')) {
				$subtable->add($persoonrow = new HtmlTableRow());
				$persoonrow->add(new HtmlTableDataCell(_("Lock alle foto's:")));
				$persoonrow->add(new HtmlTableDataCell($anch = new HtmlAnchor('#',"Lock!")));
				$anch->addClass('clickLockKartPanel');
				$subtable->add($persoonrow = new HtmlTableRow());
				$persoonrow->add(new HtmlTableDataCell(_("Unlock alle foto's:")));
				$persoonrow->add(new HtmlTableDataCell($anch = new HtmlAnchor('#',"Unlock!")));
				$anch->addClass('clickUnlockKartPanel');
			}

			$divlinks->add($persoontable = new HtmlTable());

			$persoontable->add($persoonrow = new HtmlTableRow());
			$persoonrow->add(new HtmlTableDataCell(_("Basisacties:")));

			if(hasAuth('vicie')) {
				$persoonrow->add(new HtmlTableDataCell($span = new HtmlSpan()));
				$span->add($anch = new HtmlAnchor('#', HtmlSpan::fa('times', _('Verwijder'), 'text-danger')));
				$anch->addClass('clickDeleteKartPanel');
				$span->add($anch = new HtmlAnchor('#', HtmlSpan::fa('rotate-left', _('Roteer linksom'))));
				$anch->addClass('clickRotateLeftKartPanel');
				$span->add($anch = new HtmlAnchor('#', HtmlSpan::fa('rotate-right', _('Roteer rechtsom'))));
				$anch->addClass('clickRotateRightKartPanel');
			}

			$persoontable->add($persoonrow = new HtmlTableRow());
			$persoonrow->add(new HtmlTableDataCell(_("Collectie toevoegen:")));

			$collarray = CollectieVerzameling::maakCollectieArray();

			$persoonrow->add(new HtmlTableDataCell($select = HtmlSelectbox::fromArray('collecties',$collarray,array(),1)));
			$select->addClass('collectieKartPanel');
			$persoonrow->add(new HtmlTableDataCell($button = HtmlInput::makeButton('submit', 'Doen!')));
			$button->addClass('clickCollectieKartPanel');

			if(hasAuth('vicie')) {
				$persoontable->add($persoonrow = new HtmlTableRow());
				$persoonrow->add(new HtmlTableDataCell(_("Datum en tijd wijzigen:")));
				$persoonrow->add(new HtmlTableDataCell(HtmlInput::makeDatetimeLocal('datetime')));
				$persoonrow->add(new HtmlTableDataCell($button = HtmlInput::makeButton('submit','Doen!')));
				$button->addClass('clickDatetimeKartPanel');
			}

			$magVerschuiven = True;
			foreach($kart->getMedia() as $m) {
				$magVerschuiven = $magVerschuiven && $m->magWijzigen();
			}
			if($magVerschuiven) {
				$persoontable->add($persoonrow = new HtmlTableRow());
				$persoonrow->add(new HtmlTableDataCell(_("Datum en tijd verschuiven")));
				$array = array('min' => 'Minu(u)t(en)', 'uur' => 'Uur', 'dag' => 'Dag(en)', 'mnd' => 'Maand(en)', 'jaa' => 'Ja(a)r(en)');
				$persoonrow->add(new HtmlTableDataCell(array(HtmlInput::makeNumber('timeverschuif'),HtmlSelectbox::fromArray('watverschuif',$array,array(),1))));
				$persoonrow->add(new HtmlTableDataCell($button = HtmlInput::makeButton('submit','Doen!')));
				$button->addClass('clickTijdVerschuifKartPanel');
			}
		}
		return $div;
	}

}
