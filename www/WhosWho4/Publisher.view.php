<?php

abstract class PublisherView
	extends View
{
	/**
	 *  Maakt de publisher-home-pagina
	 */
	static public function home()
	{
		$page = Page::getInstance();
		$page->addFooterJS(Page::minifiedFile('publisher.js'));

		$page->start(_('Publisher'));

		$page->add(new HtmlHeader(3, sprintf(_('Welkom bij de %s!'), new HtmlEmphasis(_('Publisher')))))
			->add(sprintf(_('Met de %s beheer je de website van je eigen commissie of activiteit')
					, new HtmlEmphasis(_('Publisher'))));

		$page->add(self::cieActListing());

		// Als we promocierechten hebben laten we ook de publisher-dir van de root zien
		if(hasAuth('promocie')) {
			$page->add(new HtmlHR());
			$vfs = hrefToEntry('/');
			$page->add(self::publisherDir($vfs));
		}

		$page->end();
	}

	/**
	 *  Maakt een lijst met alle cies en bijbehorende activiteiten waar
	 *  het ingelogde lid een publishermap bij kan maken of die map is er al
	 *
	 *@return Een HtmlDiv met daarin de lijst als htmltable
	 */
	static public function cieActListing()
	{
		// Moeten we alle afgelopen activiteiten van een cie laten zien, zo nee
		// dan maken we een form aan om dat te selecten
		if(!tryPar('afgelopen', false))
		{
			$div = new HtmlForm('GET');
		}
		else
			$div = new HtmlDiv();

		$div->add($table = new HtmlTable(null, 'table-condensed'));

		// Haal de cies waarvan we afgelopen cies willen zien (als dat kan)
		$afgelopen = tryPar('afgelopen', false);
		$mijnCies = CommissieVerzameling::wijzigbare();

		foreach($mijnCies as $cienr=>$ciedata)
		{
			if($afgelopen && $afgelopen[$ciedata->getLogin()] == "0") {
				continue;
			}

			$name = $ciedata->getLogin();

			// Haal de entry op
			$entry = hrefToEntry("/Vereniging/Commissies/$name");
			$entryId = $entry && $entry->getType() != ENTRYVARIABLE ? $entry->getId() : NULL;

			// Voeg de rij toe aan de table
			$table->add(self::cieActRow($entry, $entryId, $name, 'cie'));

			// Haal alle activiteiten van de cie op
			if($afgelopen) {
				// recente activiteiten bovenaan
				$acts = ActiviteitVerzameling::vanCommissie($ciedata, 'oud');
				$acts->sorteer("BeginMoment", True);
			} else {
				$acts = ActiviteitVerzameling::vanCommissie($ciedata);
				$acts->union(ActiviteitVerzameling::vanCommissie($ciedata, 'huidige'));
				$acts->sorteer("BeginMoment");
			}

			// Loop over de acts
			CommissieActiviteitVerzameling::vanActiviteiten($acts); // cache
			foreach($acts as $actdata)
			{
				$actid = $actdata->getActiviteitID();

				// Haal de entry op
				$entry = hrefToEntry($actdata->url());
				$entryId = ( $entry
					&& $entry->getType() == ENTRYVARIABLE
					&& $entry->getSpecial() == '/WhosWho4/Controllers/Activiteiten.php:Activiteiten_Controller:commissieActiviteitNaamEntry'
					? $entry->getId()
					: NULL
				);

				// Voeg de rij toe aan de table
				$table->add(self::cieActRow($entry, $entryId, $actid, 'act', $actdata, ($acts->last() == $actdata)));
			}
		}

		$cellArr = array();

		if(!$afgelopen)
		{
			$cellArr[] = HtmlSpan::fa('long-arrow-up', 'arrow');
			$cellArr[] = HtmlInput::makeSubmitButton(
				_('Toon eerdere activiteiten van de geselecteerde commissie(s)')
				, 'afgelopenSubmit');
		}
		else
			$cellArr[] = HtmlAnchor::button(PUBLISHERBASE
				, _('Terug naar overzicht met al je commissies en komende activiteiten'));

		$row = $table->addRow();
		$row->addData($cellArr);
		$row->addData();

		return $div;
	}

	/**
	 *  Maakt een htmltablerow met daarin een cie of een act
	 *
	 * @param entry De vfs-entry om toe te voegen
	 * @param entryId Het id van de vfs-entry
	 * @param name De name die gebruikt wordt in de checkboxes
	 * @param cieAct Een string of we een cie of een act hebben (kan 'cie' of 'act' zijn)
	 * @param actdata Het activiteit-object indien van toepassing
	 * @param last Een bool of de act de laatste van alle acts is
	 *
	 * @return De gewenste htmltablerow
	 */
	static public function cieActRow($entry, $entryId, $name, $cieAct = 'cie'
		, $actdata = null, $last = false)
	{
		$row = new HtmlTableRow();

		$cellArr = array();
		if($cieAct == 'cie')
			// Geen checkbox als we afgelopen acts bekijken
			$cellArr[] = !tryPar('afgelopen', false) ? HtmlInput::makeCheckbox('afgelopen['.$name.']') : '';

		if($entryId)
		{
			$img = HtmlSpan::fa('folder', '[dir]');
			$cellArr[] = new HtmlAnchor($entry->publisherUrl(), $img);
		}
		else
			$cellArr[] = HtmlSpan::fa('folder', '[new]');

		if($cieAct == 'cie')
		{
			if($entryId)
				$cellArr[] = new HtmlAnchor($entry->publisherUrl(), $name);
			else
				$cellArr[] = $name;
		}
		else
		{
			if($entryId)
				$cellArr[] = new HtmlAnchor($entry->publisherUrl(), ActiviteitView::titel($actdata));
			else
				$cellArr[] = ActiviteitView::titel($actdata);
		}

		$cell = $row->addData($cellArr);
		if($cieAct == 'act')
		{
			// zet een stippellijntje a-la boonstructuur neer
			if($last)
				$cell->addClass('tree-dots-last');
			else
				$cell->addClass('tree-dots');
		}

		if(!$entryId)
		{
			// Maak een toevoeg-form aan
			$row->addData($form = HtmlForm::named('add', PUBLISHERBASE . 'Toevoegen'));
			$form->add(HtmlInput::makeHidden('add', $cieAct));
			$form->add(HtmlInput::makeHidden('name', $name));
			$form->add(HtmlInput::makeSubmitButton(_('Aanmaken'), 'submit', 'btn-xs'));
		}
		else
			$row->addData();

		return $row;
	}

	/**
	 *  Maakt een pagina voor een vfs-dir aan
	 *
	 * @param vfs De vfs om de pagina voor aan te maken
	 */
	static public function publisherDirPage($vfs)
	{
		// TODO: lelijke hack om de naam van de map te krijgen
		$path = $vfs->publisherRealUrl();

		$paths = explode('/', $path);
		$paths = array_slice($paths, 0, sizeof($paths)-1);

		$page = Page::getInstance()
			->addFooterJS(Page::minifiedFile('publisher.js'))
			->start($paths[sizeof($paths)-1]);
		$page->add(self::publisherDir($vfs))
			->end();
	}

	/**
	 *  Maakt een div aan voor het overzicht van een vfs-dir
	 *
	 * @param vfs De vfs-dir
	 *
	 * @return De htmldiv
	 */
	static public function publisherDir($vfs)
	{
		global $INDEX;

		$div = new HtmlDiv();

		$path = $vfs->publisherRealUrl();

		// mogen we de sorting aanpassen? en wat is de huidige sortering?
		if (strpos($path, "/Vereniging/Commissies") !== false
			|| strpos($path, "/Activiteiten") !== false) {
			$orderby = "name";
		} else {
			$orderby = 'rank';
		}

		// Voor de promocie komt er een link naar de publisherhome, voor niet-promocie
		// wordt die link in de breadcrumbs neergezet
		if(hasAuth('promocie') && $vfs->url() !== '/')
		{
			$div->add(HtmlAnchor::button(PUBLISHERBASE, _('Terug naar publisherhome')));
			$div->add($f = new HtmlForm('GET', PUBLISHERBASE));
			$f->add(_('Of ga direct naar:'));
			$f->add($i = HtmlInput::makeText('url', '', 30));
			$i->addClass('inline-input');
			$f->add(HtmlInput::makeSubmitButton(_('Ga!')));
			$div->add(new HtmlBreak());
			$div->add(new HtmlBreak());
		}

		//TODO: lelijke hacks om de breadcrumbs goed neer te zetten. Zonder publisherRealUrl
		//worden bij een activiteit de naam van de activiteit en de cie een * en dat kan
		//misschien voor sommige mensen verwarrend zijn
		$paths = explode('/', $path);
		$paths = array_slice($paths, 0, sizeof($paths)-1);

		$lis = array();
		$par = $vfs;
		$i = sizeof($paths)-1;
		while($par && hasPublisherAuth($path) && $par->getName() != 'ROOT')
		{
			$p = (empty($paths[$i]) ? 'site_live' : $paths[$i]);
			$lis[] = new HtmlListItem(new HtmlAnchor($par->publisherUrl(), $p));
			$i--;
			$par = $par->getParent();
			$path = $par->publisherRealUrl();
		}

		if(!hasAuth('promocie'))
			$lis[] = new HtmlListItem(new HtmlAnchor(PUBLISHERBASE, '..'));

		foreach($lis as $li)
			$li->setNoDefaultClasses();

		$ol = new HtmlList(true, array_reverse($lis), 'breadcrumb');
		$ol->setNoDefaultClasses();

		$div->add($ol);

		$div->add(new HtmlHeader(4, _('Bestanden op de website')));

		$div->add($table = new HtmlTable(null, 'table-condensed'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		// Definieer alvast alle icons die we mogelijk kunnen gebruiken
		$iconDir = HtmlSpan::fa('folder', _('Bevat andere bestanden'));
		$iconEdit = HtmlSpan::fa('pencil', _('Wijzig deze pagina'));
		$iconUpload = HtmlSpan::fa('file-text-o', _('Bestand (geüpload'));
		$iconHook = HtmlSpan::fa('file-code-o', _('PHP-haak'));
		$iconLink = HtmlSpan::fa('link', _('Verwijst naar elders'));
		$iconInclude = HtmlSpan::fa('file-word-o', _('Bestand (van FS)'));
		$iconVariable = HtmlSpan::fa('question', _('Variabel stukje URL'));
		$iconUnknown = HtmlSpan::fa('exclamation-triangle', _('Bestand van onbekend type'));
		$iconEye = HtmlSpan::fa('eye', _('Bestand is zichtbaar'));
		$iconEyeClosed = HtmlSpan::fa('eye-slash', _('Bestand is niet zichtbaar'));
		$iconRename = HtmlSpan::fa('text-height', _('Hernoem dit bestand'));
		$iconDelete = HtmlSpan::fa('times', _('Verwijder dit bestand'));
		$iconUp = HtmlSpan::fa('chevron-up', _('Omhoog'));
		$iconDown = HtmlSpan::fa('chevron-down', _('Omlaag'));

		// Lijst met bestanden
		$children = $vfs->getChildrenIds(false, $orderby);
		$nr = 0;
		foreach($children as $childid)
		{
			$child = vfs::get($childid);

			$name = $child->getName();

			$row = $tbody->addRow();

			// Icoontjes
			switch($child->getType()) {
			case ENTRYDIR:		$icon    = $iconDir;		break;
			case ENTRYFILE:		$icon    = $iconEdit;		break;
			case ENTRYINCLUDE:	$icon    = $iconInclude;	break;
			case ENTRYHOOK:		$icon    = $iconHook;		break;
			case ENTRYLINK:		$icon    = $iconLink;		break;
			case ENTRYUPLOADED:	$icon    = $iconUpload;		break;
			case ENTRYVARIABLE: $icon    = $iconVariable;	break;
			default:			$icon    = $iconUnknown;	break;
			}

			$cellArr = array();

			if(hasAuth('promocie'))
			{
				// Link voor het zichtbaar maken van entries
				if($child->isVisible()) {
					$cellArr[] = new HtmlAnchor($child->publisherUrl() . 'ToggleVisibility', $iconEye);
				} else {
					$cellArr[] = new HtmlAnchor($child->publisherUrl() . 'ToggleVisibility', $iconEyeClosed);
				}
			}

			$cellArr[] = new HtmlAnchor($child->publisherUrl(), $icon);

			// Titel toevoegen en form toevoegen om te renamen (wordt met js geactiveerd)
			$d = new HtmlSpan(null, 'renameTitel'.$childid);
			$d->add(new HtmlAnchor($child->url(), $name . ($child->isDir() ? '/' : '')));
			$cellArr[] = $d;

			$d = new HtmlSpan(null, 'rename'.$childid);
			$d->add($form = HtmlForm::named('rename', $child->publisherUrl() . 'Rename'));
			$form->add($t = HtmlInput::makeText('newname_'.$childid, $name));
			$t->addClass('input-sm inline-input');
			$form->add($b = HtmlInput::makeSubmitButton(_('Hernoem')));
			$form->setCssStyle('display: inline;');
			$d->setCssStyle('display: none;');
			$b->addClass('btn-xs');

			$cellArr[] = $d;

			$row->addData($cellArr);

			$cellArr = array();

			// Als we mogen aanpassen mogen we ook renamen en deleten
			if(hasPublisherAuth($child->publisherRealUrl()))
			{
				$cellArr[] = new HtmlAnchor('#', $iconRename, null, 'renameButton', $childid);
				$cellArr[] = new HtmlAnchor($child->publisherUrl() . 'Delete', $iconDelete, null, 'deleteButton');
			}

			$row->addData($cellArr);

			// Alleen promocie en hoger mag de volgorde aanpassen waar nodig
			if(hasAuth('promocie') && $orderby == 'rank')
			{
				$cellArr = array();
				if($nr != 0)
					$cellArr[] = new HtmlAnchor($child->publisherUrl() . 'Up', $iconUp);
				else
				{
					// TODO: lelijke hack, dezelfde image hidden maken zodat hij wel
					// dezelfde ruimte inneemt
					$i = clone($iconUp);
					$i->addClass('invisible');
					$cellArr[] = $i;
				}

				if($nr != sizeof($children) - 1)
					$cellArr[] = new HtmlAnchor($child->publisherUrl() . 'Down', $iconDown);
				else
				{
					// TODO: lelijke hack, dezelfde image hidden maken zodat hij wel
					// dezelfde ruimte inneemt
					$i = clone($iconDown);
					$i->addClass('invisible');
					$cellArr[] = $i;
				}

				$row->addData($cellArr);
			}

			$lastmodified = $child->getLastModified();
			$lastmodifier = Persoon::geef($child->getLastModifier());

			$cell = $row->addData(sprintf(_('Aangepast op %s[VOC: datum] door %s[VOC: persoonnaam]')
				, htmlspecialchars($lastmodified) . new HtmlBreak()
				, ($lastmodifier ? PersoonView::naam($lastmodifier) : _('Onbekend'))));

			$cell->setCssStyle('font-size: xx-small;');

			$size = "";
			if($child->getType() == ENTRYUPLOADED) {
				$size = show_readable_size(filesize($child->getFileAbsolute()));
			}
			$row->addData($size);

			$nr++;
		}

		$div->add(new HtmlBreak());

		// Voeg per toevoeg-optie een apart form toe
		$div->add($form = HtmlForm::named('bestandAanmaken'));

		$form->add(new HtmlHeader(4, _('Nieuw bestand aanmaken')));
		$form->add(_('HTML-pagina:'))
			->add($i = HtmlInput::makeText('newname', ($vfs->hasChild($INDEX)?'':$INDEX), 30))
			->add(HtmlInput::makeSubmitButton(_('Aanmaken')));
		$i->addClass('inline-input');

		$div->add($form = HtmlForm::named('mapAanmaken'));

		$form->add(new HtmlHeader(4, _('Nieuwe map aanmaken')))
			->add(_('Mapnaam:'))
			->add($i = HtmlInput::makeText('mapname', '', 30))
			->add(HtmlInput::makeSubmitButton(_('Toevoegen')));
		$i->addClass('inline-input');

		$div->add($form = HtmlForm::named('bestandUploaden'));

		$form->add(new HtmlHeader(4, _('Nieuwe bestand uploaden')))
			->add(_('Van je eigen computer:'))
			->add($i = HtmlInput::makeFile('fileupload', 40))
			->add(HtmlInput::makeSubmitButton(_('Uploaden!')))
			->add(new HtmlBreak());
		$i->addClass('inline-input');

		return $div;
	}

	/**
	 *  Maakt een pagina om een html-bestand te editen
	 *
	 * @param vfs De vfs-file om aan te passen
	 * @param show_error Bool of de error moet worden laten zien in het form
	 */
	static public function editPage($vfs, $show_error = false)
	{
		$div = new HtmlDiv();

		// Voeg handige knoppen toe aan het begin van de pagina
		$div->add($btnGroup = new HtmlDiv(null, 'btn-group'));
		$btnGroup->add(HtmlAnchor::button('?setlanguage='
				.(getLang() == 'nl' ? 'en' : 'nl')
				, sprintf(_('Bewerk in het %s[VOC: taalnaam]')
				, (getLang() == 'nl') ? _('Engels') : _('Nederlands'))))
			->add(HtmlAnchor::button($vfs->getParent()->publisherUrl(), _('Naar bestanden beheren')))
			->add(HtmlAnchor::button($vfs->publisherRealUrl(), _('Naar buiten de publisher')));


		// Voeg het form toe voor het aanpassen van de content
		$div->add(new HtmlDiv(null, null, 'publisher_vfs'));
		$div->add(new HtmlHeader(3, _('Pagina bewerken')));

		$div->add($form = HtmlForm::named('edit'));

		$form->add($titelbalk = new HtmlDiv(array(
			new HtmlSpan("Pagina titel:", "input-group-addon"),
			HtmlInput::makeText('displayName', $vfs->getDisplayName(), 30),
			new HtmlSpan(HtmlInput::makeSubmitButton("Wijzig", "submit", "btn btn-default"), "input-group-btn")
		), "input-group"));

		$form->add(new HtmlBreak());

		$form->add($textarea = self::genericDefaultFormHtml('content', htmlspecialchars(isset($content) ? $content : $vfs->getContent(getLang())), ''));
		$textarea->setId('editarea');

		$form->add($btn = new HtmlButton('button', 'Preview'));
		$btn->setId('previewButton');

		$form->add(HtmlInput::makeSubmitButton(_('Wijzig')));

		$page = Page::getInstance()
			->addFooterJs(Page::minifiedFile('publisher','js'));
		$page->start(sprintf('%s > %s', _('Publisher'), $vfs->getDisplayname()))
			->add($div)
			->end();
	}

	/**
	 *  Maakt een wiki-achtig knopje
	 *
	 * @param to De openingstab
	 * @param tc De sluittag
	 * @param sample, alt ?
	 * @param img De button-naam
	 * @param descr De image-title
	 *
	 * @return Een htmlanchor die het knopje is
	 */
	static public function publisherEditorKnopje($to, $tc, $sample, $img, $descr = null)
	{
		if(!$descr) $descr = $sample;

		$img = new HtmlImage('/Layout/Images/Buttons/'.$img, $descr, $descr);
		return new HtmlAnchor("javascript:insertTags('$to','$tc','$sample');", $img);
	}

	/**
	 *  Vervang relatieve paden met absolute paden in tagattributen.
	 *
	 * Omdat je bij het editen van een bestand niet in dezelfde map zit
	 * als waar de plaatjes in staan, of als waar de link heengaat.
	 *
	 * @param vfs De vfs waar we nu in werken
	 * @param txt De txt om te bewerken
	 *
	 * @return De geprepende text
	 */
	static public function prependSources($vfs, $txt)
	{
		$dom = new DOMDocument();
		// Een @ supprest errors, dat is in principe iets wat je nooit wilt
		// maar loadHTML gaat bokken zodra of $txt leeg is of zodra er een
		// foutje in de html staat. Het zijn alleen maar warnings, maar het
		// spamt de errorlogs wel vol.
		@$dom->loadHTML($txt);

		// de tags waarvan we de src met ->publisherRealUrl moeten uitbreiden
		$realTags = array("img", "iframe", "source", "input");
		foreach ($realTags as $tagName) {
			foreach ($dom->getElementsByTagName($tagName) as $element) {
				$src = $element->getAttribute("src");
				if (isAbsoluteUrl($src)) {
					continue;
				}
				$src = ROOT . $vfs->getParent()->publisherRealUrl() . $src;
				$element->setAttribute("src", $src);
			}
		}
		// de tags waarvan we de href met ->url() moeten uitbreiden
		// TODO: dit moet minder gedupliceerd kunnen...
		$urlTags = array("a");
		foreach ($urlTags as $tagName) {
			foreach ($dom->getElementsByTagName($tagName) as $element) {
				$href = $element->getAttribute("href");
				if (isAbsoluteUrl($href)) {
					continue;
				}
				$href = ROOT . $vfs->getParent()->url() . $href;
				$element->setAttribute("href", $href);
			}
		}

		return $dom->saveHTML();
	}
}
