#!/usr/bin/php
<?php

$files = array();
$handle = fopen("includes-generated.php", "r");
if ($handle) {
	while (($line = fgets($handle)) !== false) {
		if(!strpos($line, 'require_once'))
			continue;
		$file = explode("'", $line);
		if(sizeof($file) <= 1)
			continue;
		if(!strpos($file[1], 'Generated'))
			continue;
		$files[] = $file[1];
	}
}
fclose($handle);

$res = shell_exec("find . -name '*.gen.php' -o -name '*.api.php'");
$res = explode("./", $res);
foreach($res as $r) {
	$string = trim(preg_replace('/\s\s+/', ' ', $r));
	if(!in_array('WhosWho4/'.$string, $files)) {
		echo($string . "\n");
	} else {
		$key = array_search('WhosWho4/'.$string, $files);
		unset($files[$key]);
	}
}
