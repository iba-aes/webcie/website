<?
/**
 * $Id$
 */
class IOUBetalingVerzameling extends IOUBetalingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // IOUBetalingVerzameling_Generated
	}

	public static function alleVanPersoon(Persoon $persoon)
	{
		return IOUBetalingQuery::table()
			->whereProp('van', $persoon)
			->orWhereProp('naar', $persoon)
			->verzamel();
	}

	public static function alleVanTweePersonen(Persoon $persoon1, Persoon $persoon2)
	{
		return IOUBetalingQuery::table()
			->where(function ($q) use ($persoon1, $persoon2)
		{
				$q->whereProp('van', $persoon1)
					->whereProp('naar', $persoon2);
			})
			->orWhere(function ($q) use ($persoon1, $persoon2)
		{
				$q->whereProp('naar', $persoon1)
					->whereProp('van', $persoon2);
			})
			->orderByDesc('gewijzigdWanneer')
			->verzamel();
	}

	public static function balansVanTweePersonen(Persoon $persoon1, Persoon $persoon2)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('MAYBEVALUE SELECT SUM(`bedrag`) '
			. ' FROM `IOUBetaling`'
			. ' WHERE `van_contactID` = %i'
			. ' AND `naar_contactID` = %i'
			, $persoon1->geefID()
			, $persoon2->geefID());

		$ids -= $WSW4DB->q('MAYBEVALUE SELECT SUM(`bedrag`) '
			. ' FROM `IOUBetaling`'
			. ' WHERE `van_contactID` = %i'
			. ' AND `naar_contactID` = %i'
			, $persoon2->geefID()
			, $persoon1->geefID());

		return $ids;
	}

	/**
	 * @brief Verdeelt een bedrag 'eerlijk' over personen waarbij de restcenten ook bepaald worden.
	 *
	 * Eerst wordt iedereen evenveel centen $c$ gegeven zondanig dat:
	 *     1. $c * N \leq \mathrm{verdeelBedrag}$,
	 *     2. $c$ maximaal is.
	 *
	 * Vervolgens worden de restcenten ($\mathrm{verdeelBedrag} - c * N$)
	 * verdeeld over de personen die NIET het meeste hebben ingelegd.
	 * Dus degene die het MEESTE hebben ingelegd, hoeven het MINSTE te betalen,
	 * en krijgen dus het MEESTE terug van anderen.
	 *
	 * @param verdeelBedrag het totale bedrag in euros wat verdeeld moet worden.
	 * @param verdeelOver array met alle lidnummers waarover verdeeld moet worden
	 * @param teBetalen array met key: lidnummer en value: het te betalen bedrag in euros
	 * @param ingelegd array met key: lidnummer en value: hoeveel er ingelegd is door deze persoon
	 * @return De array teBetalen, met de verrekening erin toegepast
	 */
	public static function verdeelBedrag($verdeelBedrag, $verdeelOver, $teBetalen, $ingelegd)
	{
		if (sizeof($verdeelOver) == 0)
		{
			return $teBetalen;
		}

		// Stap 1: verdeel zoveel mogelijk over iedereen gelijk
		// Dit is $c$ in de formule hierboven.
		$brok = floor((100 * $verdeelBedrag) / sizeof($verdeelOver)) / 100;
		foreach ($verdeelOver as $id)
		{
			if (!isset($teBetalen[$id]))
			{
				$teBetalen[$id] = $brok;
			}
			else
			{
				$teBetalen[$id] += $brok;
			}

		}

		// Stap 2: het aantal centen is minder dan het aantal personen.
		// Verdeel de restcenten over de personen met de meeste inleg
		$rest = $verdeelBedrag - $brok * sizeof($verdeelOver);

		// Bepaal wie er gul was
		$meesteInleg = 0.0;
		$gulsteGevers = [];
		foreach ($verdeelOver as $id)
		{
			if (!array_key_exists($id, $ingelegd))
			{
				continue;
			}

			$delta = $ingelegd[$id] - $meesteInleg;
			if (!Money::isSignificant(abs($delta)))
			{
				// Dit is een vergelijkbare gulle gever!
				$gulsteGevers[] = $id;
			}
			elseif ($delta > 0)
			{
				$meesteInleg = $ingelegd[$id];
				// maak de array weer leeg (behalve $id) zodat vorige slechtere gevers
				// weg zijn.
				$gulsteGevers = [$id];
			}
		}

		if (count($gulsteGevers) == count($verdeelOver))
		{
			// Iedereen heeft evenveel betaald, dus iedereen krijgt evenveel restcenten
			$restOntvangers = $gulsteGevers;
		}
		else
		{
			// De gulle gevers hoeven niet extra restcenten te betalen.
			// Dus laat deze buiten deze ronde
			$restOntvangers = array_diff($verdeelOver, $gulsteGevers);
		}

		// Verdeel nu grof de rest over gulle gevers:
		$restBrok = floor((100 * $rest) / sizeof($restOntvangers)) / 100;
		foreach ($restOntvangers as $id)
		{
			$teBetalen[$id] += $restBrok;
		}

		// Als er nu nog mensen over zijn, doe het gebaseerd op het
		// 'first come, first server' principe.
		$laatsteRest = $rest - $restBrok * sizeof($restOntvangers);
		foreach ($restOntvangers as $id)
		{
			if (round($laatsteRest, 2) > 0)
			{
				$teBetalen[$id] += 0.01;
				$laatsteRest -= 0.01;
			}
			else
			{
				break;
			}

		}

		return $teBetalen;
	}

	public static function getBalans(Persoon $persoon)
	{
		$betalingen = IOUBetalingVerzameling::alleVanPersoon($persoon);

		$debcredarray = [];
		foreach ($betalingen as $betaling)
		{
			if ($persoon == $betaling->getNaar())
			{
				if (array_key_exists($betaling->getVanContactID(), $debcredarray))
				{
					$debcredarray[$betaling->getVanContactID()] += $betaling->getBedrag();
				}
				else
				{
					$debcredarray[$betaling->getVanContactID()] = $betaling->getBedrag();
				}

			}
			elseif ($persoon == $betaling->getVan())
			{
				if (array_key_exists($betaling->getNaarContactID(), $debcredarray))
				{
					$debcredarray[$betaling->getNaarContactID()] += -1 * $betaling->getBedrag();
				}
				else
				{
					$debcredarray[$betaling->getNaarContactID()] = -1 * $betaling->getBedrag();
				}

			}
		}

		return $debcredarray;
	}

	public function mailen()
	{
		$afzender = PersoonView::naam(Persoon::getIngelogd());
		$from = '"IOU A–Eskwadraat" <iou@a-eskwadraat.nl>';

		$omschrarray = [];

		foreach ($this as $betaling)
		{
			if (!$betaling->getBetalingId())
			{
				user_error('De betaling is nog niet opgeslagen!', E_USER_ERROR);
			}

			$omschr = $betaling->getOmschrijving();
			$omschrarray[] = $omschr;

			$persoon = $betaling->getVan();
			$tekst = sprintf(_('Beste %s,

Er is zojuist een verrekening ingevoerd in het A–Eskwadraat-I-Owe-U-systeem.

%s

Voor je persoonlijk financieel overzicht kun je, na inloggen,
terecht op %s

Groeten,

A–Eskwadraat "I Owe You",
namens %s')
				, PersoonView::naam($persoon, WSW_NAME_VOORNAAM)
				, $omschr
				, HTTPS_ROOT . IOUBASE
				, $afzender
			);
			sendmail($from, $persoon->getContactID(), _('I-Owe-You - verrekening'), $tekst);
		}

		$persoon = Persoon::getIngelogd();

		$tekst = sprintf(_('Beste %s,

Je hebt zojuist een of meerdere verrekening ingevoerd in het A–Eskwadraat I-Owe-U-systeem.

%s

Voor je persoonlijk financieel overzicht kun je, na inloggen,
terecht op %s

Groeten,

A–Eskwadraat "I Owe You",
namens %s')
			, PersoonView::naam($persoon, WSW_NAME_VOORNAAM)
			, implode("\n", $omschrarray)
			, HTTPS_ROOT . IOUBASE
			, $afzender
		);
		sendmail($from, $persoon->getContactID(), _('I-Owe-You - verrekening'), $tekst);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
