<?
abstract class IOUSplitVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de IOUSplitVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze IOUSplitVerzameling een IOUBonVerzameling.
	 *
	 * @return IOUBonVerzameling
	 * Een IOUBonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IOUSplitVerzameling.
	 */
	public function toBonVerzameling()
	{
		if($this->aantal() == 0)
			return new IOUBonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBonBonID()
			                      );
		}
		$this->positie = $origPositie;
		return IOUBonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze IOUSplitVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IOUSplitVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een IOUSplitVerzameling van Bon.
	 *
	 * @return IOUSplitVerzameling
	 * Een IOUSplitVerzameling die elementen bevat die bij de Bon hoort.
	 */
	static public function fromBon($bon)
	{
		if(!isset($bon))
			return new IOUSplitVerzameling();

		return IOUSplitQuery::table()
			->whereProp('Bon', $bon)
			->verzamel();
	}
	/**
	 * @brief Maak een IOUSplitVerzameling van Persoon.
	 *
	 * @return IOUSplitVerzameling
	 * Een IOUSplitVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new IOUSplitVerzameling();

		return IOUSplitQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
