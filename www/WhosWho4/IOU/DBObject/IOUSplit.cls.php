<?
/**
 * $Id$
 */
class IOUSplit
	extends IOUSplit_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // IOUSplit_Generated
	}

	public function magVerwijderen()
	{
		// Check of de split binnen 10 minuten is aangemaakt
		$now = new DateTimeLocale();
		if($now->getTimestamp() - $this->getGewijzigdWanneer()->getTimestamp() > 10 * 60)
			return false;

		return hasAuth('god') || Persoon::getIngelogd() == $this->getPersoon();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
