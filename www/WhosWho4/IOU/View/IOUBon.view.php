<?
/**
 * $Id$
 */
abstract class IOUBonView
	extends IOUBonView_Generated
{
	static public function bon(IOUBon $bon)
	{
		$page = Page::getInstance()->start(sprintf(_('IOU-bon #%s'),$bon->getBonID()));

		$page->add(IOUView::menu());

		$nu = new DateTimeLocale();
		if($nu->getTimestamp() - $bon->getGewijzigdWanneer()->getTimestamp() < 10 * 60)
		{
			$page->add(new HtmlHR());
			$page->add(new HtmlParagraph(
				array(
					_('Je bent nog binnen tien minuten van het aanmaken van de bon, je mag deze bon verwijderen'),
					HtmlAnchor::button($bon->verwijderURL(), _('Verwijder'))
				)
			));
		}

		$page->add(new HtmlHeader(2,sprintf(_("Details van bon #%s"),$bon->getBonID())));

		$page->add(new HtmlDiv(new HtmlDiv($table = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		$table->add(self::infoTRData(_('Ingevoerd op'), $bon->getGewijzigdWanneer()->format('Y-m-d H:i:s')));
		$table->add(self::infoTRData(_('Ingevoerd door'), PersoonView::makeIOULink($bon->getPersoon())));

		$bedrag = $bon->getBedrag();
		$table->add(self::infoTRData(_('Totaalbedrag'), Money::addPrice($bedrag)));
		$table->add(self::infoTRData(_('Omschrijving'),
			IOUBonView::waardeOmschrijving($bon) .
			(($bon->magWijzigen()) ? sprintf(" (%s)", new HtmlAnchor($bon->wijzigURL(), _('Aanpassen'))) : "")));

		$betrokkenen = $bon->getBetrokkenen();
		$cellstr = '';
		$f = true;
		foreach($betrokkenen as $b) {
			$persoon = Persoon::geef($b);
			if($f)
				$cellstr .= PersoonView::makeIOULink($persoon);
			else
				$cellstr .= "<br>" . PersoonView::makeIOULink($persoon);
			$f = false;
		}

		$table->add(self::infoTRData(_('Betrokkenen'), $cellstr));

		$page->add($row = new HtmlDiv(null, 'row'));

		$row->add($col_left = new HtmlDiv(null, 'col-md-6'))
			->add($col_right = new HtmlDiv(null, 'col-md-6'));

		$col_left->add(new HtmlHeader(3, _('Betalingsoverzicht')));
		$col_right->add(new HtmlHeader(3, _('Verrekenresultaat')));

		$col_left->add($bet = new HtmlTable());
		$col_right->add($ver = new HtmlTable());

		$splits = $bon->getSplits();

		$row = $bet->addRow();
		$row->addHeader(_('Wie'));
		$row->addHeader(_('Moet betalen'));
		$row->addHeader(_('Heeft ingelegd'));

		foreach($splits as $split) {
			$row = $bet->addRow();
			$row->addData(PersoonView::makeIOULink($split->getPersoon()));
			$row->addData(Money::addPrice($split->getMoetbetalen()));
			$row->addData(Money::addPrice($split->getHeeftbetaald()));
		}

		$betalingen = $bon->getBetalingen();

		$row = $ver->addRow();
		$row->addHeader(_('Wie'));
		$row->addHeader(_('Betaalt aan'));
		$row->addHeader(_('Hoeveel'));

		foreach($betalingen as $betaling) {
			if($betaling->getBedrag() < 0) {
				$row = $ver->addRow();
				$row->addData(PersoonView::makeIOULink($betaling->getNaar()));
				$row->addData(PersoonView::makeIOULink($betaling->getVan()));
				$row->addData(Money::addPrice(-1 * $betaling->getBedrag()));
			} else {
				$row = $ver->addRow();
				$row->addData(PersoonView::makeIOULink($betaling->getVan()));
				$row->addData(PersoonView::makeIOULink($betaling->getNaar()));
				$row->addData(Money::addPrice($betaling->getBedrag()));
			}
		}

		$page->end();
	}

	static public function bonWijzigen(IOUBon $bon, $show_error = false)
	{
		$page = Page::getInstance();

		$page->start();

		$page->add(new HtmlDiv(new HtmlDiv($table = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		$table->add(self::infoTRData(_('Ingevoerd op'), $bon->getGewijzigdWanneer()->format('Y-m-d H:i:s')));
		$table->add(self::infoTRData(_('Ingevoerd door'), PersoonView::makeIOULink($bon->getPersoon())));

		$bedrag = $bon->getBedrag();
		$table->add(self::infoTRData(_('Totaalbedrag'), Money::addPrice($bedrag)));

		$page->add($form = HtmlForm::named('wijzigenBon'));

		$form->add(self::wijzigTR($bon, 'omschrijving', $show_error));
		$form->add(HtmlInput::makeSubmitButton(_('Doe ut!')));

		$page->end();
	}

	static public function bonVerwijderen(IOUBon $bon, $show_error)
	{
		$page = Page::getInstance()->start(_('Bon verwijderen'));

		$page->add(self::verwijderForm($bon));

		$page->end();
	}

	/**
	 * Viewfunctie voor het aanmaken van een nieuwe bon
	 *
	 * @param betrokkenCie De cie waarvan mensen op de bon staan
	 * @param betrokkenen De mensen die op de bon staan
	 * @param kart De kart waarvan mensen op de bon staan
	 * @param omschrijving De omschrijving van de kart
	 * @param verdeelover Array van de mensen waarover het bedrag moet worden verdeeld
	 * @param verdeelbedrag Het bedrag wat moet worden verdeeld
	 * @param tebetalen Array van hoeveel ieder persoon moet betalen
	 * @param ingelegd Array hoeveel ieder persoon heeft ingelegd
	 */
	static public function bonNieuw($betrokkenCie, $betrokkenen, $kart, $omschrijving,
		$verdeelover, $verdeelbedrag, $tebetalen, $ingelegd)
	{
		global $auth;

		$page = Page::getInstance();

		$page->addFooterJS(Page::minifiedFile('knockout.js'))
			->addFooterJS(Page::minifiedFile('IOU.js'));

		$page->start(_("Nieuwe bon"));

		$data = array();
		$data['omschrijving'] = $omschrijving;
		$data['verdeelBedrag'] = $verdeelbedrag;
		$data['betalingen'] = array();

		foreach($betrokkenen as $persoon)
		{
			$betalingRow = array();
			$betalingRow['id'] = $persoon->geefID();
			$betalingRow['verdeling'] = (isset($verdeelover[$persoon->geefID()]));
			$betalingRow['naam'] = PersoonView::naam($persoon);
			$betalingRow['moetBetalen'] = (isset($tebetalen[$persoon->geefID()]) ? $tebetalen[$persoon->geefID()] : 0);
			$betalingRow['heeftIngelegd'] = (isset($ingelegd[$persoon->geefID()]) ? $ingelegd[$persoon->geefID()] : 0);
			$data['betalingen'][] = $betalingRow;
		}

		$page->add(HtmlScript::makeJavascript('var bonJSON = '.json_encode($data).';'));

		// Stoppen het ingelogde lidnr in een hidden, zodat js het op kan halen
		$page->add(HtmlInput::makeHidden('lidid',$auth->getLidnr()));

		$page->add(IOUView::menu());

		$page->add(new HtmlHeader(3,_("Nieuwe bon invoeren")));

		$page->add($form = HtmlForm::named('nieuweBon'));
		$form->addClass('form');

		$form->add($rowdiv = new HtmlDiv(null, 'row'));

		$rowdiv->add($links = new HtmlDiv(null, 'col-md-6'));
		$rowdiv->add($rechts = new HtmlDiv(null, 'col-md-6'));

		$links->add(new HtmlHeader(4, _("Betrokkenen selecteren:")));

		$mycies = CommissieVerzameling::vanPersoon(Persoon::getIngelogd());

		if($mycies->aantal() > 0)
		{
			$links->add(_('Commissie:'));
			$links->add(self::defaultFormCommissieVerzameling($mycies, 'betrokkenCie'));
		}

		$links->add(_("Mensen:"));
		$links->add(self::defaultFormPersoonVerzameling($betrokkenen, 'LidVerzameling', false, true));

		$rechts->add(new HtmlHeader(4, _("Bon invoeren:")));

		$rechts
			->add(_("Omschrijving:"))
			->add(new HtmlBreak())
			->add($text = new HtmlTextarea('bonOmschrijving', 72, 7))
			->add(new HtmlBreak())
			->add(new HtmlBreak())
			->add(new HtmlDiv($tbl = new HtmlTable(null, 'personen table-non-striped'), 'form-inline'));

		$text->setAttribute('data-bind', 'value: omschrijving, valueUpdate: "afterkeydown"');

		$rechts->add($table = new HtmlTable(null, 'table-non-striped'));
		$thead = $table->addHead();
		$tbody = $table->addBody();
		$tbody->addComment('ko foreach: betalingen');

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array('', _('Naam'), _('Moet betalen'), _('Heeft ingelegd')));

		$row = $tbody->addRow();

		$row->addData($input = HtmlInput::makeCheckbox('checkbox'));
		$input->setAttribute('data-bind', 'checked: verdeling, attr: { name: "bonVerdeelOver["+id+"]" }');
		$data = $row->addData();
		$data->setAttribute('data-bind', 'text: naam');
		$row->addData($input = self::genericDefaultFormMoney('betalen', 0, 'betalen'));
		$input->getChild(1)->setAttribute('data-bind', 'value: moetBetalen, '
			. 'attr: { name: "bonTebetalen["+id+"]" }, valueUpdate: "afterkeydown"');
		$row->addData($input = self::genericDefaultFormMoney('betalen', 0, 'betalen'));
		$input->getChild(1)->setAttribute('data-bind', 'value: heeftIngelegd, '
			. 'attr: { name: "bonIngelegd["+id+"]" }, valueUpdate: "afterkeydown"');

		$tbody->addComment('/ko');

		$row = $tbody->addRow();
		$row->addData($input = HtmlInput::makeCheckbox('checkbox-all'));
		$row->addData(_('Verdeel over de geselecteerde mensen:'));
		$row->adddata($input = self::genericDefaultFormMoney('bonVerdeelBedrag', 0.00, 'bonVerdeelBedrag'));
		$input->getChild(1)->setAttribute('data-bind', 'value: verdeelBedrag, valueUpdate: "afterkeydown"');
		$row->addData();

		$row = $tbody->addRow();
		$row->addData();
		$row->addData(_('Totaal'));
		$data = $row->addData();
		$data->setAttribute('data-bind', 'text: formatCurrency(totaalBetalen())');
		$data = $row->addData();
		$data->setAttribute('data-bind', 'text: formatCurrency(totaalIngelegd())');

		$rechts->add($btn = HtmlInput::makeSubmitButton(_('Invoeren'), 'nieuwebon'));
		$btn->setAttribute('data-bind', 'disable: kanNietInvoeren()');

		$btn->setAttribute('disabled', null);
		$rechts->add(new HtmlBreak());
		$rechts->add($span = new HtmlParagraph(_('Je hebt nog geen bedragen ingevuld!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: totaalIngelegd() == 0 && totaalBetalen() == 0');
		$rechts->add($span = new HtmlParagraph(_('Het moet-betalen-bedrag komt niet overeen met het ingelegde bedrag!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: totaalIngelegd() != totaalBetalen()');
		$rechts->add($span = new HtmlParagraph(_('Er moeten tenminste twee personen betrokken zijn bij de bon!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: betalingen().length < 2');
		$rechts->add($span = new HtmlParagraph(_('Je moet een omschrijving invullen!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: omschrijving().length == 0');
		$rechts->add($span = new HtmlParagraph(_('Geen negatieve bedragen invullen!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: heeftNegatief()');
		$rechts->add($span = new HtmlParagraph(_('Geen bedragen groter dan €999,999.99 invullen!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: heeftTeGroot()');
		$rechts->add($span = new HtmlParagraph(_('Je hebt geen mensen geselecteerd om het te verdelen bedrag over te verdelen!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: !ingelegdWordtVerdeeld()');
		$rechts->add($span = new HtmlParagraph(_('Je moet zelf bij de bon betrokken zijn!'), 'waarschuwing'));
		$span->setAttribute('data-bind', 'visible: !isBetrokken()');

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
