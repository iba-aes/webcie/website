<?
abstract class IOUBetalingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IOUBetalingView.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIOUBetaling(IOUBetaling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld betalingID.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld betalingID labelt.
	 */
	public static function labelBetalingID(IOUBetaling $obj)
	{
		return 'BetalingID';
	}
	/**
	 * @brief Geef de waarde van het veld betalingID.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld betalingID van het object obj
	 * representeert.
	 */
	public static function waardeBetalingID(IOUBetaling $obj)
	{
		return static::defaultWaardeInt($obj, 'BetalingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * betalingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld betalingID representeert.
	 */
	public static function opmerkingBetalingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bon.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bon labelt.
	 */
	public static function labelBon(IOUBetaling $obj)
	{
		return 'Bon';
	}
	/**
	 * @brief Geef de waarde van het veld bon.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bon van het object obj
	 * representeert.
	 */
	public static function waardeBon(IOUBetaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBon())
			return NULL;
		return IOUBonView::defaultWaardeIOUBon($obj->getBon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bon.
	 *
	 * @see genericFormbon
	 *
	 * @param IOUBetaling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bon staat en kan worden
	 * bewerkt. Indien bon read-only is betreft het een statisch html-element.
	 */
	public static function formBon(IOUBetaling $obj, $include_id = false)
	{
		return IOUBonView::defaultForm($obj->getBon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bon. In tegenstelling
	 * tot formbon moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formbon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bon staat en kan worden
	 * bewerkt. Indien bon read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBon($name, $waarde=NULL)
	{
		return IOUBonView::genericDefaultForm('Bon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bon representeert.
	 */
	public static function opmerkingBon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld van.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld van labelt.
	 */
	public static function labelVan(IOUBetaling $obj)
	{
		return 'Van';
	}
	/**
	 * @brief Geef de waarde van het veld van.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld van van het object obj
	 * representeert.
	 */
	public static function waardeVan(IOUBetaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVan())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getVan());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld van.
	 *
	 * @see genericFormvan
	 *
	 * @param IOUBetaling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld van staat en kan worden
	 * bewerkt. Indien van read-only is betreft het een statisch html-element.
	 */
	public static function formVan(IOUBetaling $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getVan());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld van. In tegenstelling
	 * tot formvan moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formvan
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld van staat en kan worden
	 * bewerkt. Indien van read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVan($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Van');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld van
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld van representeert.
	 */
	public static function opmerkingVan()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naar.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naar labelt.
	 */
	public static function labelNaar(IOUBetaling $obj)
	{
		return 'Naar';
	}
	/**
	 * @brief Geef de waarde van het veld naar.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naar van het object obj
	 * representeert.
	 */
	public static function waardeNaar(IOUBetaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getNaar())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getNaar());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naar.
	 *
	 * @see genericFormnaar
	 *
	 * @param IOUBetaling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naar staat en kan worden
	 * bewerkt. Indien naar read-only is betreft het een statisch html-element.
	 */
	public static function formNaar(IOUBetaling $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getNaar());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naar. In tegenstelling
	 * tot formnaar moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naar staat en kan worden
	 * bewerkt. Indien naar read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaar($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Naar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naar
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naar representeert.
	 */
	public static function opmerkingNaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrag.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrag labelt.
	 */
	public static function labelBedrag(IOUBetaling $obj)
	{
		return 'Bedrag';
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrag van het object obj
	 * representeert.
	 */
	public static function waardeBedrag(IOUBetaling $obj)
	{
		return static::defaultWaardeMoney($obj, 'Bedrag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrag.
	 *
	 * @see genericFormbedrag
	 *
	 * @param IOUBetaling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is betreft het een statisch html-element.
	 */
	public static function formBedrag(IOUBetaling $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Bedrag', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrag. In
	 * tegenstelling tot formbedrag moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbedrag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBedrag($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Bedrag');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bedrag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrag representeert.
	 */
	public static function opmerkingBedrag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(IOUBetaling $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param IOUBetaling $obj Het IOUBetaling-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(IOUBetaling $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param IOUBetaling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(IOUBetaling $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
}
