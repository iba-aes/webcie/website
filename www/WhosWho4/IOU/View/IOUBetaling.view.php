<?
/**
 * $Id$
 */
abstract class IOUBetalingView
	extends IOUBetalingView_Generated
{
	static public function betaling(IOUBetaling $betaling)
	{
		$page = Page::getInstance()->start(sprintf(_("IOU-betaling #%s"),$betaling->getBetalingID()));

		$page->add(IOUView::menu());

		$page->add(new HtmlHeader(2,sprintf(_("Overzicht van betaling #%s"),$betaling->getBetalingID())));

		$page->add(new HtmlDiv(new HtmlDiv($table = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		$table->add(self::infoTRData(_("Ingevoerd door"), PersoonView::makeLink(Persoon::geef($betaling->getGewijzigdWie()))))
			  ->add(self::infoTRData(_("Datum"), $betaling->getGewijzigdWanneer()->format('Y-m-d H:i:s')))
			  ->add(self::infoTRData(_("Bedrag"), Money::addPrice(abs($betaling->getBedrag()))))
			  ->add(self::infoTRData(_("Van"),
			($betaling->getBedrag() > 0)
				? PersoonView::makeLink($betaling->getVan())
				: PersoonView::makeLink($betaling->getNaar())
			))
			  ->add(self::infoTRData(_("Naar"),
			($betaling->getBedrag() > 0)
				? PersoonView::makeLink($betaling->getNaar())
				: PersoonView::makeLink($betaling->getVan())
			))
			  ->add(self::infoTRData(_("Omschrijving"), IOUBetalingView::waardeOmschrijving($betaling)));

		$page->end();
	}

	static public function betalingNieuw($debcredarray, $msg = array())
	{
		$debarray = array();
		$credarray = array();
		foreach($debcredarray as $key => $b) {
			if(round($b,2) == 0)
				continue;
			elseif(round($b, 2) > 0)
				$credarray[$key] = $b;
			elseif(round($b, 2) < 0)
				$debarray[$key] = $b;
		}

		$page = Page::getInstance()->start(_("Nieuwe verrekening(en)"));

		if(sizeof($msg) != 0) {
			$page->add(new HtmlSpan($msg, 'text-danger strongtext'));
		}

		$page->add(IOUView::menu());

		$page->add($form = HtmlForm::named('NieuweBetaling'));
		$form->addClass('form-inline');

		$form->add(HtmlInput::makeSubmitButton('Voer in'));
		$form->add(new HtmlBreak());
		$form->add(new HtmlBreak());

		$form->add($table = new HtmlTable());

		$row = $table->addRow();
		$row->addData();
		$row->addData(_("Met wie"));
		$row->addData(_("Krijgt hoeveel van wie"));
		$row->addData(_("Jij betaalt:"));
		$row->addData(_("Omschrijving (optioneel)"));

		$idarray = tryPar('check',array());

		if(!is_array($idarray))
		{
			$idarray = array($idarray);
		}

		if(sizeof($idarray) != 0)
			$post = true;
		else
			$post = false;

		foreach($debarray as $key => $bedrag) {
			if(round($bedrag,2) == 0)
				continue;

			$persoon = Persoon::geef($key);

			$row = $table->addRow();
			$row->addData(array(
				HtmlInput::makeHidden("naar[".$key."]", "ander")
				, HtmlInput::makeCheckbox('check['.$key.']'
					, ($post && array_key_exists($key, $idarray)) ? $idarray[$key] == "on" : false)
				, "<a name=$key></a>"));
			$row->addData(array(
				PersoonView::makeLink($persoon), new HtmlBreak(), PersoonView::waardeRekeningnummer($persoon)));
			$row->addData(
				sprintf(_("(krijgt nog %s[VOC: bedrag] van jou)")
					, Money::addPrice(abs($bedrag))));
			$row->addData(array(new HtmlLabel('bedrag_' . $key
				, _('Het bedrag van €[VOC:hierna volgt een input met een bedrag]'))
				, HtmlInput::makeText('bedrag_' . $key
				, Money::parseMoney(tryPar('bedrag_'.$key, abs($bedrag))))));
			if($post)
				$row->addData(
					$area = HtmlTextarea::withContent(tryPar('text_'.$key, '')
													  , 'text_'.$key
													  , 40 
													  , 1));
			else
				$row->addData($area = new HtmlTextarea('text_'.$key, 40 ,1));
			$area->setAttribute('style','width: 100%');
		}

		$form->add(HtmlInput::makeSubmitButton('Voer in'));
		$form->add(new HtmlBreak());
		$form->add(new HtmlBreak());

		$form->add($table = new HtmlTable());
		$row = $table->addRow();
		$row->addData();
		$row->addData(_("Met wie"));
		$row->addData(_("Krijgt hoeveel van wie"));
		$row->addData(_("Jij krijgt:"));
		$row->addData(_("Omschrijving (optioneel)"));

		foreach($credarray as $key => $bedrag) {
			if(round($bedrag,2) == 0)
				continue;

			$row = $table->addRow();
			$row->addData(array(
				HtmlInput::makeHidden("naar[".$key."]", "ingelogd")
				, HtmlInput::makeCheckbox('check['.$key.']'
					, ($post && array_key_exists($key, $idarray)) ? $idarray[$key] == "on" : false)
				, "<a name=$key></a>"));
			$row->addData(PersoonView::makeLink(Persoon::geef($key)));
			$row->addData(
				sprintf(_("(jij krijgt nog %s)")
					, Money::addPrice(abs($bedrag))));
			$row->addData(array(new HtmlLabel('bedrag_' . $key
				, _('Het bedrag van €[VOC:hierna volgt een input met een bedrag]'))
				, HtmlInput::makeText('bedrag_' . $key
				, Money::parseMoney(tryPar('bedrag_'.$key, abs($bedrag))))));
			$in = tryPar('richting_'.$key, 'in');
			if($post)
				$row->addData(
					$area = HtmlTextarea::withContent(tryPar('text_'.$key, '')
													  , 'text_'.$key
													  , 40 
													  , 1));
			else
				$row->addData(
					$area = new HtmlTextarea('text_'.$key, 40 ,1));
			$area->setAttribute('style','width: 100%');
		}

		$form->add(HtmlInput::makeSubmitButton('Voer in'));

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
