<html>

<head>
	<title>A&ndash;Eskwadraat</title>
	<link rel=StyleSheet href="/Layout/CSS/style_televisie.css" type="text/css">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="/javascript/jquery.cycle.all.js"></script>
	<script type="text/javascript" src="/javascript/jquery.color.js"></script>
	<script type="text/javascript" src="/javascript/televisie.js"></script>
</head>
<body>
	<div id="page">
		<div id="main">
			<div class="slideshow">
				<?php
				$alreadyUsed = array();
				foreach ($promos as $p) {
					foreach ($p['promos'] as $id => $act) {
						if (array_key_exists($id, $alreadyUsed)) continue;
						$alreadyUsed[$id] = TRUE;

						$url = $act->url() . '/ActPoster';
						$desc = htmlspecialchars($act);
						if ($act->posterPath()) {
							echo '<div><img src="' . $url . '" alt="' . $desc . "\" /></div>\n";
						} else {
							echo '<!-- Geen poster gevonden voor ' . $desc . " op " . $url . "-->\n";
						}
					}
				} ?>
			</div>
		</div>

		<div id="ticker">
			<div id="ticker_text" style="left: 140px; position:relative; text-align:center; width: 1150px;">
				Welkom bij A&ndash;Eskwadraat!
			</div>
		</div>

		<div id="left">
			<div id="text">
				<h1>Komende tijd</h1>
<?php foreach ($promos as $p) {
					$date = $p['date'];
					$promo = $p['promos'];
					?>
					<h2><?php echo ucfirst($date->strftime("%A")); ?></h2>
					<ul>
<?php 	foreach ($promo as $act) { ?>
						<li><?php echo ActiviteitView::titel($act); ?></li>
<?php 	} ?>
					</ul>
<?php } ?>
			</div>
			<div id="aeslogo"></div>
			<div id="tijd"></div>
		</div>
	</div>
</body>
</html>
