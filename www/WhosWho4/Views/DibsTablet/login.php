<!DOCTYPE html>
<html>
<head>
  <title>Dibs: ColaInterface</title>
</head>

<body>
	<div>
	<form action="<?php echo HTTPS_ROOT;?>/space/auth/dibstabletlogin.php" method="post" name="loginform">
		<input type="hidden" required placeholder="Login" size="14" id="loginnaam" name="loginnaam" value="DibsTablet" tabindex="1"/>
		<label for="password">Scan de tablet-tag</label>
		<br>
		<input type="password" required placeholder="Wachtwoord" size="14" id="password" name="password" tabindex="2" autofocus="true"/>
		<input type="hidden" value="/Leden/Dibs/DibsTablet/ToonTablet" name="next" />
	  </form>
	</div>
	<?php if(tryPar('loginfout', null) == 'true') { ?>
	<div>
		<p style="color: red;"><b>Inloggen mislukt!</b><p>
	</div>
	<?php }?>
</body>
</html> 
