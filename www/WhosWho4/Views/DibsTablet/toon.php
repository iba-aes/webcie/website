<!DOCTYPE html>
<html>
<head>
	<title>Dibs: ColaInterface</title>
	<script src="/javascript/StringBuffer.cls.js"></script>
	<script src="<? echo Page::minifiedFile('a-eskwadraat.js'); ?>"></script>
	<script src="DibsTablet.js"></script>
	<meta charset="UTF-8" />
	<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" /> 
	<link href="/Layout/CSS/style_dibstablet.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="wait"></div>
	<input type="button" value="dummy" id="dummy_button" />

	<div id="fout" class="page">
		<div class="header">
			<h1><?php echo _('Fout')?></h1>
		</div>
		<div class="content">
			<p id="fout_melding"></p>
		</div>
		<div class="footer">
			<a href="#" class="startschermTerug" ><?php echo _('Terug')?></a>
		</div>
	</div>

	<div id="startscherm" class="page no-footer">
		<div class="header">
			<h1><?php echo _('Startscherm')?></h1>
			<div id="vlaggetjes">
				<a href="#" class="vlaggetjeEngels">
					<img src="/Leden/Dibs/DibsTablet/eng.jpg" />
				</a>
				<a href="#" class="vlaggetjeNederlands">
					<img src="/Leden/Dibs/DibsTablet/ned.jpg" />
				</a>
			</div>
		</div>
		<div class="content">
			<table>
				<thead>
					<tr>
						<th><?php echo _('Product')?></th>
						<th><?php echo _('Prijs')?></th>
					</tr>
				</thead>
				<tbody>
<?php
$producten->sorteer('volgorde');
$alt = false;
foreach ($producten as $product) { ?>
					<tr<?php if($alt) echo ' class="striped"'; $alt = !$alt; ?>>
						<td><?php echo $product->getNaam(); ?></td>
						<td class="num"><?php echo $product->getKudos(); ?></td>
					</tr>
<?php } ?>
				</tbody>
			</table>
			<p><?php echo _('Houd je sleutelhanger voor de lezer')?></p>
		</div>
		<div id="input_buiten">
			<form class="rfid_form">
				<input type="text" id="startscherm_rfid" autocomplete="off" />
			</form>
		</div>
	</div>

	<div id="winkel" class="page">
		<div class="header">
			<h1 id="winkel_titel"><?php echo _('Winkel')?></h1>
		</div>
		<div class="content">
			<div class="topinfo">
				<div class="line">
					<p class="label"><?php echo _('Saldo')?>:</p>
					<p id="winkel_lidkudos"></p>
				</div>
				<div class="line">
					<p class="label"><?php echo _('Totaal')?>:</p>
					<p id="winkel_selkudos"></p>
				</div>
				<div class="bottom">
					<p class="label"><?php echo _('Over')?>:</p>
					<p id="winkel_overkudos"></p>
				</div>
			</div>
<?php foreach ($producten as $product) {
		$productnr = $product->getArtikelID(); ?>
			<div class="item">
				<div class="imgwrapper">
					<a href="#" id="<?php echo $productnr; ?>" class="product">
						<img src="/Leden/Dibs/ColaProductPlaatjes/<?php echo $productnr; ?>.jpg" class="item_img" alt="" />
						<span class="text"><?php echo $product->getNaam(); ?></span>
						<span class="prijs"><?php echo $product->getKudos(); ?></span>
						<div class="overlay"></div>
					</a>
				</div>
				<div class="itembottom">
					<span id="winkel_<?php echo $productnr; ?>" class="itemcount">0</span>
					<div class="rmbutton">
						<a href="#" id="<?php echo $productnr; ?>" class="removeProduct">&#10060;</a>
					</div>
				</div>
			</div>
<?php } ?>
			<div id="winkel_msg"></div>
		</div>
		<div class="footer">
				<a href="#" class="annulerenWinkel" ><?php echo _('Terug')?></a>
				<a href="#" class="verderWinkel" ><?php echo _('Bevestigen')?></a>
		</div>
	</div>

	<div id="confirmatie" class="page">
		<div class="header">
			<h1><?php echo _('Confirmatie')?></h1>
		</div>
		<div class="content">
			<div class="topinfo">
				<p><?php echo _('Saldo')?>: <span id="conf_lidkudos"></span> &#8594; <span id="conf_eindkudos"></span></p>

				<table>
					<thead>
						<tr>
							<th><?php echo _('Product')?></th>
							<th><?php echo _('Stukprijs')?></th>
							<th><?php echo _('Subtotaal')?></th>
						</tr>
					</thead>
					<tbody id="conf_tbody"><!-- wordt gevuld door handleWinkel --></tbody>
					<tfoot>
						<tr>
							<td colspan="2"><?php echo _('Totaal')?></td>
							<td id="conf_total" class="num"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div class="footer">
			<a href="#" class="annulerenConf" ><?php echo _('Terug naar winkel')?></a>
			<a href="#" class="verderConf" ><?php echo _('Betalen')?></a>
		</div>
	</div>

	<div id="status" class="page">
		<div class="header">
			<h1><?php echo _('Status')?></h1>
		</div>
		<div class="content">
			<div id="status_msg_wrapper">
				<p id="status_msg"></p>
			</div>
		</div>
		<div class="footer">
			<a href="#" class="annulerenEind" ><?php echo _('Terug naar startscherm')?></a>
		</div>
	</div>
</body>
</html>
