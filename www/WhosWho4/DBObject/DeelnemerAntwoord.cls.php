<?

/***
 * $Id$
 */
class DeelnemerAntwoord
	extends DeelnemerAntwoord_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	public function magVerwijderen()
	{
		if($this->getDeelnemer()->getPersoon()->magWijzigen() || $this->getDeelnemer()->getActiviteit()->magWijzigen())
			return true;

		return false;
	}

	public function valid ($welkeVelden = 'set')
	{
		$bool = parent::valid($welkeVelden);

		if($this->getVraag()->getType() == 'ENUM'
			&& !$this->getAntwoord() && $this->getVraag()->getVerplicht())
		{
			$this->errors['Antwoord'] = _('Selecteer een optie');
			$bool = false;
		}

		return $bool;
	}

	public function checkAntwoord()
	{
		// Merk op dat we niet de parent::checkAntwoord aanroepen!
		// Dit is omdat die het altijd verplicht maakt.

		if ($this->getAntwoord() == null && $this->getVraag()->getVerplicht())
		{
			return _('Dit veld is verplicht');
		}

		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
