<?

/**
 * $Id$
 */
class PlannerData
	extends PlannerData_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct();
	}

	public function magVerwijderen()
	{
		//Alleen bestuur en de maker van de planner kunnen verwijderen
		return hasAuth('bestuur') || Persoon::getIngelogd() == $this->getPlanner()->getPersoon();
	}

	public function deelnemers ()
	{
		return PlannerDeelnemerVerzameling::plannerData($this);
	}

	public function personenVoor ()
	{
		return PersoonVerzameling::plannerDataVoor($this);
	}

	public function getTotaalVoor ($deelnemers = null)
	{
		if (is_null($deelnemers))
			$deelnemers = $this->deelnemers();
		elseif ($deelnemers instanceof PersoonVerzameling)
		{
			$personen = $deelnemers;
			$deelnemers = array();
			foreach ($personen as $persoon)
				$deelnemers[] = PlannerDeelnemer::geef($this->geefID(), $persoon->geefID());
		}

		$voor = 0;
		foreach ($deelnemers as $deelnemer)
			if ($deelnemer->getPlannerData() === $this && $deelnemer->getAntwoord() == 'JA')
				$voor++;
		return $voor;
	}

	public function getTotaalTegen ($deelnemers = null)
	{
		if (is_null($deelnemers))
			$deelnemers = $this->deelnemers();
		elseif ($deelnemers instanceof PersoonVerzameling)
		{
			$personen = $deelnemers;
			$deelnemers = array();
			foreach ($personen as $persoon)
				$deelnemers[] = PlannerDeelnemer::geef($this->geefID(), $persoon->geefID());
		}

		$tegen = 0;
		foreach ($deelnemers as $deelnemer)
			if ($deelnemer->getPlannerData() === $this && $deelnemer->getAntwoord() == 'NEE')
				$tegen++;
		return $tegen;
	}

	public function getTotaalLieverNiet($deelnemers = null)
	{
		// TODO: deze code mag wel iets meer DRY...

		if (is_null($deelnemers))
			$deelnemers = $this->deelnemers();
		elseif ($deelnemers instanceof PersoonVerzameling)
		{
			$personen = $deelnemers;
			$deelnemers = array();
			foreach ($personen as $persoon)
				$deelnemers[] = PlannerDeelnemer::geef($this->geefID(), $persoon->geefID());
		}

		$lieverNiet = 0;
		foreach ($deelnemers as $deelnemer)
			if ($deelnemer->getPlannerData() === $this && $deelnemer->getAntwoord() == 'LIEVER_NIET')
				$lieverNiet++;
		return $lieverNiet;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
