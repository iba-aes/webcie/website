<?
class Register
	extends Register_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Register_Generated

		$this->id = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}

	public function magVerwijderen() {
		return hasAuth('bestuur');
	}
	public function magWijzigen() {
		return hasAuth('bestuur');
	}

	public static function heeftType($waarde, $type) {
		switch ($type) {
		case "STRING":
			return is_string($waarde);
		case "INT":
			return ctype_digit($waarde);
		case "BOOLEAN":
			return $waarde === "true" || $waarde === "false";
		case "FLOAT":
			return preg_match('/^[+-]?(([0-9]+)|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*)|(([0-9]+|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*))[eE][+-]?[0-9]+))$/', trim($waarde));
		default:
			return false;
		}
	}

	public static function waardeNaarType($waarde, $type) {
		switch ($type) {
		case "STRING":
			return $waarde;
		case "INT":
			return (int) $waarde;
		case "BOOLEAN":
			return $waarde === "true";
		case "FLOAT":
			return (float) $waarde;
		default:
			return NULL;
		}
	}

	public static function getValue($key, $defaultValue = NULL)
	{
		$entry = RegisterQuery::table()->whereProp('label', $key)->geef();
		if ($entry) {
			return Register::waardeNaarType($entry->getWaarde(), $entry->getType());
		} else {
			return $defaultValue;
		}
	}

	public static function updateRegister($key, $value) {
		$entry = RegisterQuery::table()->whereProp('label', $key)->geef();
		if (!$entry) {
			throw new Exception("Register entry does not exist for label " . htmlspecialchars($key));
		}
		$entry->updateValue($value);
		$entry->opslaan();
	}

	public function updateValue($value) {
		if (is_bool($value)) {
			$value = $value ? "true" : "false";
		}
		if (!Register::heeftType($value, $this->getType())) {
			return false;
		}
		$this->setWaarde((string) $value);
		return true;
	}
}
