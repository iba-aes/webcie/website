<?

/**
 * $Id$
 */
class ContactMailingList
	extends ContactMailingList_Generated
{
	public function __construct($contact = NULL, $mailingList = NULL)
	{
		parent::__construct($contact, $mailingList);
	}

	public function magVerwijderen ()
	{
		global $auth;
		if (hasAuth('bestuur') || $this->getContact() === Persoon::getIngelogd() || $auth->getLevel() == 'mollie')
			return true;
		return false;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
