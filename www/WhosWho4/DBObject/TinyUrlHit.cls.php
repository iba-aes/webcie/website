<?
/**
 * $Id$
 *
 *  Een hit op een TinyUrl
 */
class TinyUrlHit
	extends TinyUrlHit_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a = NULL)
	{
		parent::__construct(); // TinyUrlHit_Generated

		if(is_object($a) && $a instanceOf TinyUrl)
		{
			$this->tinyUrl = $a;
			$this->tinyUrl_id = NULL;
		}
		else
		if(isset($a))
		{
			$this->tinyUrl = NULL;
			$this->tinyUrl_id = $a;
		}
		else
		{
			$this->id = NULL;
		}
	}
	
	//Zorg er voor dat je als bestuur een TinyUrlHit mag verwijderen
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
