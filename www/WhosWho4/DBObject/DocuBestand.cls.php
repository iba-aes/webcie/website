<?
/**
 *
 *  'AT'AUTH_GET:ingelogd
 */
class DocuBestand
	extends DocuBestand_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DocuBestand_Generated

		$this->id = NULL;
	}

	/*
	 *  Geeft de url van het docubestandobject
	 */
	public function url()
	{
		return DOCUBASE . $this->geefID();
	}

	/*
	 *  Geeft true als het object verwijderd mag worden
	 */
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	/*
	 *  Geeft de bestandslocatie op het filesystem
	 */
	public function getBestand()
	{
		return DOCUMENTENROOT . $this->geefID() . "." . $this->getExtensie();
	}

	public function verwijderen($foreigncall = null)
	{
		global $filesystem;

		if(!DEBUG && !DEMO && $filesystem->has($this->getBestand()))
		{
			$filesystem->delete($this->getBestand());
		}

		return parent::verwijderen($foreigncall);
	}
}
