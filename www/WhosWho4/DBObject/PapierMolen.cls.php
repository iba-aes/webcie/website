<?
class PapierMolen
	extends PapierMolen_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($geplaatst = null)
	{
		parent::__construct($geplaatst); // PapierMolen_Generated

		$this->papierMolenID = NULL;
	}

	public function url()
	{
		return PAPIERMOLENBASE . $this->geefID() . '/';
	}

	public function magWijzigen()
	{
		return ($this->getLid() == Persoon::getIngelogd() || hasAuth('bestuur'));
	}

	public function magVerwijderen()
	{
		return $this->magWijzigen();
	}

	public function checkPrijs()
	{
		$val = parent::checkPrijs();

		if($val === false)
			if($this->getPrijs() == 0)
				return _('prijs mag niet nul zijn');
			else if($this->getPrijs() < 0)
				return _('prijs mag niet negatief zijn');
			else
				return false;
		else
			return $val;
	}

	public function checkEan()
	{
		$val = parent::checkEan();

		if($val === false)
			if($this->getEan() <= 0)
				return _('geen geldig isbn');
			else
				return false;
		else
			return $val;
	}

	public function checkVerloopdatum()
	{
		$val = parent::checkVerloopdatum();

		$date = new DateTimeLocale();
		$date2 = clone $date;
		$date2->modify('+1 year');
		if($val === false)
			if($this->getVerloopdatum() < $date || $this->getVerloopdatum() > $date2)
				return _('geen geldige verloopdatum');
			else
				return false;
		else
			return $val;
	}
}
