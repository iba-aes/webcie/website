<?
/**
 * $Id$
 */
class PersoonKart
	extends PersoonKart_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($persoon, $naam)
	{
		parent::__construct($persoon, $naam); // PersoonKart_Generated
	}

	public function magVerwijderen()
	{
		return Persoon::getIngelogd() == $this->getPersoon();
	}

	/**
	 *   geeft de url bij een PersoonKart
	 */
	public function url()
	{
		return "/Service/Kart/KartInfo?naam=".$this->getNaam();
	}

	/**
	 *   maakt van een PersoonKart de huidige kart
	 *
	 *  @param toevoegen is of de persoonkart aan de huidige kart moet worden toegevoegd of niet
	 */
	public function maakKart($toevoegen = false)
	{
		$personen = $this->getPersonen();
		$cies = $this->getCies();
		$media = $this->getMedia();

		$kart = Kart::geef();

		if($toevoegen) {
			$kart->voegPersonenToe($personen);
			$kart->voegCiesToe($cies);
			$kart->voegMediaToe($media);
		} else {
			$kart->setPersonen($personen);
			$kart->setCies($cies);
			$kart->setMedia($media);
		}
	}

	/**
	 *   geeft alle personen terug die in een opgeslagen kart staan
	 */
	public function getPersonen()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `persoon_contactID` FROM `KartVoorwerp` WHERE `persoonKart_persoon_contactID` = %i AND `persoonKart_naam` = %s",$this->getPersoonContactID(),$this->getNaam());

		return PersoonVerzameling::verzamel($ids);
	}

	/**
	 *   geeft alle cies terug die in een opgeslagen kart staan
	 */
	public function getCies()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `commissie_commissieID` FROM `KartVoorwerp` WHERE `persoonKart_persoon_contactID` = %i AND `persoonKart_naam` = %s",$this->getPersoonContactID(),$this->getNaam());

		return CommissieVerzameling::verzamel($ids);
	}

	/**
	 *   geeft alle foto's terug die in een opgeslagen kart staan
	 */
	public function getMedia()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `media_mediaID` FROM `KartVoorwerp` WHERE `persoonKart_persoon_contactID` = %i AND `persoonKart_naam` = %s",$this->getPersoonContactID(),$this->getNaam());

		return MediaVerzameling::verzamel($ids);
	}

	/**
	 *   geeft alle kartvoorwerpen die bij een persoonkart horen
	 */
	public function getKartVoorwerpen()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `KartVoorwerpID` FROM `KartVoorwerp` WHERE `persoonKart_persoon_contactID` = %i AND `persoonKart_naam` = %s",$this->getPersoonContactID(),$this->getNaam());

		return KartVoorwerpVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
