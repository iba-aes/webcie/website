<?

/***
 * $Id$
 */
class ContactTelnr
	extends ContactTelnr_Generated
{
	/** CONSTRUCTOR **/
	public function __construct ($contact = null, $pref = null)
	{
		parent::__construct($contact);

		if( !is_null($pref) && in_array($pref, self::enumsSoort()))
		{
			$this->soort = $pref;
		}
	}

	public function magVerwijderen()
	{
		global $auth;
		return hasAuth('bestuur') || $auth->getLevel() == 'mollie';
	}

	/**
	 * Naar string casten
	 */
	public function __toString ()
	{
		return $this->telefoonnummer;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
