<?

/**
 * $Id$
 */
class PlannerDeelnemer
	extends PlannerDeelnemer_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}
	public function magVerwijderen()
	{
		//Alleen bestuur en de maker van de planner kunnen verwijderen
		return hasAuth('bestuur') || Persoon::getIngelogd() == $this->getPlannerData()->getPlanner()->getPersoon();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
