<?

/***
 * $Id$
 */
class Contact
	extends Contact_Generated
{
	protected $telefoonnummers;
	protected $adressen;
	
	protected $nieuwadres;
	public $wijzigingen = array();

	public $requireEmail = false;

	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();

		$this->telefoonnummers = null;
		$this->adressen = null;
	}

	public static function zoekOpEmail($email)
	{
		global $WSW4DB;

		$id = $WSW4DB->q('MAYBEVALUE SELECT `ContactID`'
		                .' FROM `Contact`'
		                .' WHERE `email` = %s LIMIT 1'
		                , $email
		                );
		if(!$id)
			return NULL;

		return self::geef($id);
	}

	/**
	 * Zoek het Contact met een bepaald e-mailadres. Indien er geen of meerdere gevonden 
	 * worden wordt er NULL gereturned.
	 *
	 * @param email Een e-mailadres.
	 * @return Het unieke Contact met het gegeven e-mailadres, of NULL als deze niet (uniek) bepaald 
	 * is.
	 */
	public static function zoekOpUniekeEmail ($email)
	{
		global $WSW4DB;

		$res = $WSW4DB->q('COLUMN SELECT `Contact`.`contactID`
			FROM `Contact`
			WHERE `Contact`.`email` = %s',
			$email);

		if (count($res) == 1)
		{
			return Contact::geef($res[0]);
		}

		return NULL;
	}

	public function requireEmail ()
	{
		$this->requireEmail = true;
		return $this;
	}

	public function checkEmail ()
	{
		if (!$this->requireEmail && is_null($this->email))
			return parent::checkEmail();

		if (!checkEmailFormat($this->email))
		{
			return _('dit is geen geldig email-adres');
		}
		return parent::checkEmail();
	}

	/**
	 * @brief geeft terug of de ingelogde gebruiker de foto van deze persoon mag zien.
	 */
	public function magFotoWordenBekeken() {
		global $auth;
		return hasAuth('bestuur') || $auth->getLidnr() == $this->getContactID() || $this->magBekijken();
	}

	/**
	 * Overschrijft de defaultopslaan zodat ook telefoonnummers worden
	 * (mooi) meegenomen.
	 */
	public function opslaan ($classname = 'Contact')
	{
		global $WSW4DB;

		$new = !$this->inDB;

		parent::opslaan($classname);

		// sla de telefoonnummerlijst ook op
		if ($this->telefoonnummers instanceof ContactTelnrVerzameling && $this->telefoonnummers->allValid())
		{
			$this->telefoonnummers->opslaan();

			if (!$new)
				$WSW4DB->q('DELETE FROM `ContactTelnr`'
						. ' WHERE `contact_contactID` = %i'
						. (($this->telefoonnummers->aantal() > 0) ? ' AND (`telnrID`) NOT IN (%Ai)' : '%_'),
						$this->geefID(),
						$this->telefoonnummers->keys());
		}

		// sla de addressenlijst ook op
		if ($this->adressen instanceof ContactAdresVerzameling && $this->adressen->allValid())
		{
			$this->adressen->opslaan();

			if (!$new)
				$WSW4DB->q('DELETE FROM `ContactAdres`'
						. ' WHERE `contact_contactID` = %i'
						. (($this->adressen->aantal() > 0) ? ' AND (`adresID`) NOT IN (%Ai)' : '%_'),
						$this->geefID(),
						$this->adressen->keys());
		}
	}

	public function valid ($velden = 'set')
	{
		$bool = parent::valid($velden);

		$nrs = $this->getTelefoonNummers();
		if (!$nrs->allValid($velden))
		{
			$this->errors['Telefoonnummers'] = _('fouten in telefoonnummer-lijst');
			$bool = false;
		}

		$adressen = $this->getAdressen();
		if (!$adressen->allValid($velden) or ($this->nieuwadres instanceof ContactAdres and !$this->nieuwadres->valid()))
		{
			$this->errors['Adressen'] = _('fouten in adres-lijst');
			$bool = false;
		}

		return $bool;
	}

	public function getPrefTelnr ()
	{
		$nrs = $this->getTelefoonNummers();
		if ($nrs->aantal() == 0)
			return null;
		$keys = array();
		foreach ($nrs as $nr)
		{
			$keys[$nr->getSoort()] = $nr;
			if ($nr->getVoorkeur())
				return $nr;
		}
		if (isset($keys['MOBIEL']))
			return $keys['MOBIEL'];
		return array_shift($keys);
	}

	public function getTelefoonNummers ()
	{
		if (!$this->telefoonnummers instanceof ContactTelnrVerzameling)
			$this->telefoonnummers = ContactTelnrVerzameling::vanContact($this);
		return $this->telefoonnummers;
	}

	public function getAdressen ()
	{
		if (!$this->adressen instanceof ContactAdresVerzameling)
			$this->adressen = ContactAdresVerzameling::vanContact($this);
		return $this->adressen;
	}

	public function getEersteAdres ()
	{
		return ContactAdres::eersteBijContact($this);
	}

	public function getnieuwAdres()
	{
		if(!$this->nieuwadres instanceof ContactAdres)
			$this->nieuwadres = new ContactAdres($this);
		return $this->nieuwadres;	
	}

	public function toonEmail($als = 'view')
	{
		switch($als) {
		case 'label':	return _('Email');
		case 'view':	$eml = parent::toonEmail($als);
						return new HtmlAnchor('mailto:'.$eml, $eml);
		default:		return parent::toonEmail($als);
		}
	}

	/**
	 *  Bekijkt de informatie die beschikbaar is van dit Contact object
	 * en retourneert een array met problemen met die informatie. 
	 *
	 * De zaken die in het array genoemd worden kunnen gebruikt worden op de
	 * website om iemand attent te maken op ontbrekende persoonsgegevens. Het
	 * is de bedoeling dat deze functie overriden wordt in klassen als Persoon
	 * en Donateur om specifieke checks op de data uit die klassen te
	 * controleren. Het is dan natuurlijk belangrijk om alsnog de functie in
	 * deze klasse aan te roepen!
	 *
	 * @param positie bepaalt in welke persoon de tekst geschreven wordt: 2 =
	 * tweede persoon ("gefeliciteerd, je bent jarig"), 3 = derde persoon
	 * ("vergeet deze persoon niet te feliciteren, die is jarig!"), null =
	 * auto detect (default)
	 *
	 * @return Een array in het volgende formaat:
	 * $problemen = array (
	 * 		array ( "level" => "notice",
	 * 				"title" => "Gefeliciteerd!",
	 * 				"message" => "Volgens onze gegevens ben je vandaag jarig, gefeliciteerd!"
	 * 		),
	 * 		array ( "level" => "warning",
	 * 				"title" => "Geretourneerde e-mails",
	 * 				"message" => "Er zijn geretourneerde e-mails, controleer je adresgegevens!"
	 * 		)
	 * )
	**/
	public function controleerInformatie($positie = null){
		global $auth;

		$problemen = array();
		if ($positie != 2 && $positie != 3){
			if ($auth->getLidnr() == $this->geefID()) $persoon = 2;
			else $persoon = 3;
		}

		// Link naar pagina met persoonsgegevens
		// TODO: vallen alle contacten onder /Leden?
		$link = "/Leden/" . $this->geefID();
		$anchor = new HtmlAnchor($link, _("deze pagina"));
		$classname = strtolower(get_class($this));

		// TODO: bounces in objectsysteem administreren?

		if (!$this->getEmail()){
			// Geen e-mailadres opgegeven
			$prob['level'] = 'warning';
			$prob['title'] = _('Geen e-mailadres bekend');

			if ($persoon == 2){
				$prob['message'] = sprintf(_('In onze administratie is geen e-mailadres van jou bekend. '.
					'A–Eskwadraat verstuurt met enige regelmaat berichten aan leden en contacten over de vereniging, ' .
					'derhalve is het essentieel om over een correct e-mailadres te kunnen beschikken. Je kunt ' .
					'eventueel de frequentie en de aard van de berichten aanpassen in je persoonsvoorkeuren. ' .
					'Gelieve je persoonsgegevens te controleren op %s en waar nodig te corrigeren.'),
					$anchor
				);
			} else {
				$prob['message'] = sprintf(_('Van deze %s is geen e-mailadres bekend. ' .
					'Op %s kunnen de gegevens bekeken en gecorrigeerd worden.'),
					$classname, $anchor
				);
			}
			$problemen[] = $prob;
		} // else: wel een e-mailadres bekend
		
/*
		if (!($this->getAdres('THUIS') instanceof ContactAdres)){
			$prob['title'] = _('Geen thuisadres bekend');
			$prob['level'] = 'warning';
			
			if ($persoon == 2){
				$prob['message'] = sprintf(_('We hebben van jou geen thuisadres in onze ledenadministratie. ' .
					'Gelieve deze informatie in te vullen op %s.'), $anchor);
			} else {
				$prob['message'] = sprintf(_('Van deze persoon is geen thuisadres bekend in onze ledenadministratie. ' .
					'Gelieve deze informatie in te vullen op %s.'), $anchor);
			}

			$problemen[] = $prob;
		} else {
			$adres = $this->getAdres('THUIS');
			if (!$adres->getHuisnummer() || !$adres->getPostcode() || !$adres->getStraat1() || !$adres->getWoonplaats()){
				$prob['title'] = _('Onvolledig thuisadres bekend');
				$prob['level'] = 'warning';
				
				if ($persoon == 2){
					$prob['message'] = sprintf(_('We hebben van jou een onvolledig of onjuist thuisadres in ' .
						'onze ledenadministratie. Waarschijnlijk ben je verhuisd, of hebben we poststukken ' .
						'aan jou gericht onbestelbaar retour gekregen. Gelieve de gegevens op %s te controleren ' .
						'en waar nodig te corrigeren'), $anchor);
				} else {
					$prob['message'] = sprintf(_('Van deze persoon is een onvolledig of onjuist thuisadres bekend in onze ledenadministratie. ' .
						'Gelieve deze informatie in te vullen op %s.'), $anchor);
				}

				$problemen[] = $prob;
			} // else: adresinfo is volledig
		}
*/
		return $problemen;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
