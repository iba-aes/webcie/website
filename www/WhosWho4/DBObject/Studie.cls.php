<?
/**
 * $Id$
 */
class Studie
	extends Studie_Generated
{
	static public function zoek ($soort, $fase)
	{
		global $WSW4DB;

		$id = $WSW4DB->q('MAYBEVALUE SELECT `Studie`.`studieID` FROM `Studie` '
					   . 'WHERE `Studie`.`soort` = %s AND '
					   . '`Studie`.`fase` = %s LIMIT 1',
						$soort, $fase);

		if (!is_null($id))
			return self::geef($id);

		$studie = new Studie();
		$studie->setSoort($soort)
			   ->setFase($fase)
			   ->opslaan();

		return $studie;
	}

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Studie_Generated

		$this->studieID = NULL;
	}

	/**
	 * Geef de (hopelijk!) unieke (bachelor/master)studie met deze afkorting.
	 *
	 * Bijvoorbeeld: 'ic' geeft Informatica.
	 *
	 * We nemen aan dat er een unieke studie is per afkorting per fase.
	 * Mocht dit niet meer zo zijn, dan moeten de aanroepen aan de functie ook aangepakt worden,
	 * want die hebben dezelfde aannames.
	 *
	 * @param string $afkorting De afkorting van de studie, komt overeen met $studie::getSoort().
	 * Case-insensitive (maar de canonieke vorm is met hoofdletters).
	 * @param string $fase 'BA' voor Bachelor, 'MA' voor master.
.	 *
	 * @return Studie|null
	 * @throws InvalidArgumentException als de afkorting meerdere keren voorkomt.
	 */
	public static function metAfkorting($afkorting, $fase='BA')
	{
		$uitkomsten = StudieQuery::table()
			->whereProp('soort', strtoupper($afkorting))
			->whereProp('fase', $fase)
			->verzamel();

		if ($uitkomsten->aantal() == 0)
		{
			return null;
		}
		elseif ($uitkomsten->aantal() > 1)
		{
			// Als dit misloopt, zijn vermoedelijk ook de aannames bij aanroepen aan deze functie niet meer correct,
			// dus ik vrees dat je wat herprogrammeren te wachten staat.
			throw new InvalidArgumentException('Meerdere studies gevonden met afkorting ' . $afkorting . ' dus aannames van deze functies zijn weerlegd :O');
		}

		return $uitkomsten->first();
	}

	public function url ()
	{
		return '/Leden/Studie/' . $this->geefID();
	}

	public function magWijzigen()
	{
		if(hasAuth('bestuur'))
			return true;
		return false;
	}

	public function getAantalStudenten($status = NULL)
	{
		global $WSW4DB;
		if (!in_array($status, LidStudie::enumsStatus()))
			$status = 'STUDEREND';
		$aantal = $WSW4DB->q('VALUE SELECT COUNT(DISTINCT `LidStudie`.`lid_contactID`)
			FROM `Studie` JOIN `LidStudie` ON `Studie`.`studieID` = `LidStudie`.`studie_studieID`
			WHERE `Studie`.`studieID`=%i AND `LidStudie`.`status`=%s',
			$this->geefID(), $status);
		return $aantal;
	}

	public function getStudenten ($status = NULL)
	{
		return LidVerzameling::vanStudie($this, $status);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
