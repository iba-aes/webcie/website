<?

/**
 * $Id$
 *
 *  bedrijf/uu/zusje/...
 */
class Organisatie
	extends Organisatie_Generated
{
	protected $contactpersoon;

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Organisatie_Generated

		$this->contactID = NULL;
	}

	//Match exact op naam, we gaan er van uit dat we dan maar een resultaat krijgen.
	public static function geefMetNaam($naam)
	{
		global $WSW4DB;

		$id = $WSW4DB->q('COLUMN SELECT `contactID` FROM `Organisatie`
						WHERE `naam` = %s', $naam);

		return Organisatie::geef($id);
	}

	/**
		 Retourneert de URL waarop deze donateur te vinden is

		@returns Een URL (string) waarop informatie over deze donateur te
		vinden is
	**/
	public function url()
	{
		return '/Leden/Organisatie/' . $this->geefID();
	}

	public function getContactPersoon()
	{
		if (!$this->contactpersoon instanceof ContactPersoonVerzameling)
			$this->contactpersoon = ContactPersoonVerzameling::metConstraints($this);
		return $this->contactpersoon;
	}

	public function checkNaam()
	{
		@$zelfdenaam = Organisatie::geefMetNaam(OrganisatieView::waardeNaam($this));
		if ($zelfdenaam && $zelfdenaam->geefID() != $this->geefID())
			return _("Deze naam is al in gebruik");
		return false;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
