<?

/**
 * $Id$
 *
 *  Een persoon van een organisatie waar we contact mee hebben.
 */
class ContactPersoon
	extends ContactPersoon_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b, $c = 'NORMAAL')
	{
		parent::__construct($a, $b, $c); // ContactPersoon_Generated

	}

	/**
	 *  We gaan er van uit dat ieder persoon maximaal contactpersoon is van 1 organsiatie.
	 **/
	public static function vanPersoon(Persoon $pers, $type = "NORMAAL")
	{
		$persid = $pers->getContactID();
		global $WSW4DB;
		$rows = $WSW4DB->q('TABLE SELECT `organisatie_contactID` as orgid, `type` as type'
						. ' FROM `ContactPersoon`'
						. ' WHERE `persoon_contactID` = %i'
						. ' AND `type` = %s'
						. ' LIMIT 1'
						, $persid, $type);

		$ids = array();
		foreach($rows as $row) {
			$ids[] = array($persid, $row['orgid'], $row['type']);
			//We willen er maar 1!
			break;
		}

		if(count($ids) > 0)
			return self::geef(implode("_", $ids[0]));

		return NULL;
	}

	/**
	 * Returneert of het ingelogd lid het recht heeft om dit contactpersoon te wijzigen.
	 */
	public function magWijzigen ()
	{
		return ContactPersoon::magKlasseWijzigen()
			|| (hasAuth('spookweb') && (
				$this->getType() == "BEDRIJFSCONTACT" ||
				$this->getType() == "SPOOK" ||
				$this->getType() == "ALUMNUS" ||
				$this->getType() == "OVERIGSPOOKWEB" ));
	}
	public function magVerwijderen ()
	{
		if(hasAuth('bestuur') ||
			(hasAuth('spookweb') && (
				$this->getType() == "BEDRIJFSCONTACT" ||
				$this->getType() == "SPOOK" ||
				$this->getType() == "ALUMNUS" ||
				$this->getType() == "OVERIGSPOOKWEB" )
			)
		)
			return true;
		return false;
	}

	public function magBekijken()
	{
		return $this->magWijzigen();
	}

	public function url()
	{
		switch(strtoupper($this->getType()))
		{
			case 'SPOOK':
			case 'OVERIGSPOOKWEB':
				return SPOOKBASE . 'Spook/' . $this->getPersoonContactID() . "/" . $this->getOrganisatieContactID();
				break;
			case 'BEDRIJFSCONTACT':
				return SPOOKBASE . 'Bedrijfscontact/' . $this->getPersoonContactID(). "/" . $this->getOrganisatieContactID();
				break;
			case 'ALUMNUS':
				return SPOOKBASE . 'Alumnus/' . $this->getPersoonContactID(). "/" . $this->getOrganisatieContactID();
				break;

			default:
				return '/Leden/Organisatie/' . $this->getOrganisatieContactID() . "/Contactpersonen/" . $this->getPersoonContactID();
		}
	}

	//Check, speciaal voor Cindy, of de data wel een beetje kloppen.
	public function checkBeginDatum()
	{
		if ($this->getBeginDatum()->hasTime() && $this->getBeginDatum() > $this->getEindDatum())
			return _('iemand kan niet eerder stoppen dan dat die begint');
		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
