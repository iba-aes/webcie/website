<?
class RatingObject
	extends RatingObject_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // RatingObject_Generated

		$this->ratingObjectID = NULL;
	}

	//Geeft alle PersoonRating-objecten van een foto terug
	public function getAllRatings() {
		global $WSW4DB;

		$ids = $WSW4DB->q("TABLE SELECT `ratingObject_ratingObjectID`,`persoon_contactID` FROM `PersoonRating` WHERE `ratingObject_ratingObjectID`=%i",$this->getRatingObjectID());

		return PersoonRatingVerzameling::verzamel($ids);
	}
	
	//Functie die teruggeeft hoevaak een foto geratet is.
	public function getRatingCount() {
		global $WSW4DB;

		$value = $WSW4DB->q("MAYBEVALUE SELECT COUNT(`ratingObject_ratingObjectID`) FROM `PersoonRating` WHERE `ratingObject_ratingObjectID`=%i",$this->getRatingObjectID());

		return $value;
	}

	//Update de rating die in de media-tabel staat
	public function updateRating() {
		global $WSW4DB;

		$value = $WSW4DB->q("MAYBEVALUE SELECT AVG(`rating`) FROM `PersoonRating` WHERE `ratingObject_ratingObjectID` = %i GROUP BY `ratingObject_ratingObjectID`",$this->getRatingObjectID());

		if($value)
			$this->setRating($value * 10);
		else
			$this->setRating(5);

		return $this;
	}
	
	//Kijk of er is gerate
	public function checkForRating()
	{
		// Kijk voor elk van de 5 sterren of er op is gedrukt
		for($ster = 1; $ster <= 5; $ster++ )
		{
			if(trypar('rating' . $this->getRatingObjectID().'_'.$ster.'_x') != null)
			{
				if($persoon = Persoon::getIngelogd())
					$this->addRating($persoon, $ster * 2);
			}
		}
	}
	
	public function addRating($persoon, $ratingWaarde)
	{
		//Heeft de user niet al een keertje geratet?
		if(!($rating = PersoonRating::geef($this->geefID(),$persoon->getContactID())))
		{
			//We geven een persoonRating aan het rating object
			$rating = new PersoonRating($this,$persoon);
			$rating->setRating($ratingWaarde);
			$rating->opslaan();
			$this->updateRating();
			$this->opslaan();
		}
		else
		{
			// Anders passen we de huidige persoonRating gaan
			$rating->setRating($ratingWaarde);
			$rating->opslaan();
			$this->updateRating();
			$this->opslaan();
		}
		
		
		
	}
	
	

}
