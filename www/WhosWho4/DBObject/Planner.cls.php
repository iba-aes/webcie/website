<?

/**
 * $Id$
 */
class Planner
	extends Planner_Generated
{
	protected $deelnemers;
	protected $data;

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct();
	}

	public function magBekijken ()
	{
		return Planner::magKlasseBekijken() || $this->isDeelnemer(Persoon::getIngelogd());
	}
	public function magWijzigen ()
	{
		return Planner::magKlasseWijzigen() || $this->isDeelnemer(Persoon::getIngelogd());
	}
	public function magVerwijderen()
	{
		//Alleen bestuur en de maker van de planner kunnen verwijderen
		return (hasAuth('bestuur') || Persoon::getIngelogd() == $this->getPersoon());
	}

	/**
	 * Geef een url waar de planner te vinden is
	 */
	public function url ()
	{
		return '/Activiteiten/Planner/' . $this->geefID();
	}

	/**
	 * Geef een lijstje met alle data van een planner
	 */
	public function data ()
	{
		trigger_error('De methode data() is outdated, gebruik getData', E_USER_DEPRECATED);
		return $this->getDeelnemers();
	}
	public function getData ()
	{
		if (!$this->data instanceof PlannerDataVerzameling)
			$this->data = PlannerDataVerzameling::planner($this);
		return $this->data;
	}
	public function setData (PlannerDataVerzameling $data)
	{
		$this->data = $data;
		return $this;
	}

	/**
	 * Geef de laatste tijd die bij een planner hoort
	 */
	public function getLastDate()
	{
		$data = $this->getData();
		$datetime = new DateTime('00-00-0000 00:00:00');
		foreach($data as $d) {
			if($d->getMomentBegin() > $datetime)
				$datetime = $d->getMomentBegin();
		}

		return $datetime;
	}

	/**
	 * Geef een lijstje met alle deelnemers van een planner
	 */
	public function leden ()
	{
		trigger_error('De methode leden() is outdated, gebruik getDeelnemers', E_USER_DEPRECATED);
		return $this->getDeelnemers();
	}
	public function getDeelnemers ()
	{
		if (!$this->deelnemers instanceof PersoonVerzameling || $this->deelnemers->aantal() == 0)
			$this->deelnemers = PersoonVerzameling::planner($this);
		return $this->deelnemers;
	}
	public function setDeelnemers (PersoonVerzameling $deelnemers)
	{
		$this->deelnemers = $deelnemers;
		return $this;
	}

	/**
	 * Geef alle antwoorden van een lid
	 */
	public function deelnemer (Persoon $pers)
	{
		$data = $this->getData($this);
		$ids = array();
		foreach ($data as $datum)
			$ids[] = array($datum->geefID(), $pers->geefID());

		return PlannerDeelnemerVerzameling::verzamel($ids);
	}
	public function isDeelnemer (Persoon $lid)
	{
		return $this->getDeelnemers()->bevat($lid);
	}

	/**
	 * Verwijder jezelf uit de planner
	 */

	public function zelfVerwijderen ()
	{
		$deelnemers = $this->getDeelnemers();
		$zelf = Persoon::getIngelogd();
		$id= $zelf->geefID();
		$deelnemers->verwijder($id);
		$this->setDeelnemers($deelnemers);
	}

	/**
	 * Controleer of alles is ingevuld
	 * @return 0 indien niets ingevuld, 1 indien half ingevuld, 2 indien
	 *   helemaal ingevuld
	 */
	public function ingevuld (Lid $lid)
	{
		global $WSW4DB;

		$aantallen = $WSW4DB->q('KEYVALUETABLE SELECT `antwoord`, COUNT(`antwoord`) FROM `PlannerDeelnemer` '
							  . 'LEFT JOIN `PlannerData` ON `plannerData_plannerDataID` = `plannerDataID` '
							  . 'WHERE `PlannerData`.`planner_plannerID` = %i '
							  . 'AND `PlannerDeelnemer`.`persoon_contactID` = %i '
							  . 'GROUP BY `PlannerDeelnemer`.`antwoord` ',
						$this->geefID(), $lid->geefID()
						);

		// Merge zodat alle mogelijke antwoorden erin staan, met default 0
		$default_waarden = array_fill_keys(PlannerDeelnemer::enumsAntwoord(), 0);
		$merged_aantallen = array_merge($default_waarden, $aantallen);
		if ($merged_aantallen['ONBEKEND'] == 0)
		{
			return 2;
		}
		else if ($merged_aantallen['LIEVER_NIET'] == 0
			  && $merged_aantallen['NEE'] == 0
			  && $merged_aantallen['JA'] == 0)
		{
			return 0;
		}
		return 1;
	}

	/**
	* Controleer of alle deelnemers de planner op zijn minst deels hebben ingevuld
	* @return true als de planner in ieder geval deels is ingevuld door iedereen,
	*   false als een of meer personen de planner nog niet hebben ingevuld.
	*/
	public function isIngevuldDoorIedereen(){	
		foreach($this->getDeelnemers() as $deelnemer){
			if($this->ingevuld($deelnemer) == 0){
				return false;
			}
		}
		
		return true;
	}

	// stuur mail vanaf het systeem naar de eigenaar van de planner 
	// met aks booschap dat de planner door iedereen op zijn minst deels
	// is ingevuld door de deelnemers.
	public function mailEigenaarPlannerIsIngevuldDoorIedereen(){
		$verzender = "www@a-eskwadraat.nl";
		$mailtje = _("Beste %DEELNEMER%,

Jouw planner voor de volgende activiteit is door iedereen op zijn minst deels ingevuld.

Activiteit: %TITEL%
Omschrijving: 
%OMSCHRIJVING%

Link: http://www.A-Eskwadraat.nl/Activiteiten/Planner/%PLANID%

Veel plezier met het uitkiezen van een dag!

Deze mail is verstuurd door de WebCie via de Activiteitenplanner.");

		$vervangLijst = array(
			'TITEL'  => $this->getTitel(),
			'OMSCHRIJVING' => $this->getOmschrijving(),
			'PLANID' => $this->geefID(),
		);

		foreach($vervangLijst as $item => $vervang)
		{
			$mailtje = str_replace('%' . $item . '%', $vervang, $mailtje);
		}

		$contact = $this->getPersoon();
		$contactMail = $contact->getEmail();

		if($contactMail)
		{
			sendmail($verzender, $contact->geefID(), _("Plannen van ") . $this->getTitel(), str_replace('%DEELNEMER%',
				 $contact->getVoorNaam(), nl2br($mailtje)), '', '', '', true);
		}
	}


	/** Stuur mail dat er iemand uit de planner is gestapt
	**/

	public function mailIemandIsWeggegaan($uitstapper){
		$verzender = "www@a-eskwadraat.nl";
		$mailtje = _("Beste %DEELNEMER%,

Er heeft iemand gekozen om uit een van jouw planners te stappen.

Uitgestapte deelnemer: %UITSTAPPER%
Activiteit: %TITEL%
Omschrijving: 
%OMSCHRIJVING%

Link: %LINK%

Deze mail is verstuurd door de WebCie via de Activiteitenplanner.");

		$vervangLijst = array(
			'TITEL'  => $this->getTitel(),
			'OMSCHRIJVING' => $this->getOmschrijving(),
			'LINK' => HTTPS_ROOT . $this->url(),
			'UITSTAPPER' => $uitstapper->getNaam() . ' (' . $uitstapper->geefID() . ')' ,
		);

		foreach($vervangLijst as $item => $vervang)
		{
			$mailtje = str_replace('%' . $item . '%', $vervang, $mailtje);
		}

		$contact = $this->getPersoon();
		$contactMail = $contact->getEmail();

		if($contactMail)
		{
			sendmail($verzender, $contact->geefID(), _("Deelnemer uitgestapt bij ") . $this->getTitel(), str_replace('%DEELNEMER%',
				 $contact->getVoorNaam(), nl2br($mailtje)), '', '', '', true);
		}
	}


	public function opslaan ($classname = 'Planner')
	{
		global $WSW4DB;

		parent::opslaan($classname);

		// sla de data op
		if ($this->data instanceof PlannerDataVerzameling && $this->data->allValid())
			$WSW4DB->q('INSERT INTO `PlannerData` (`planner_plannerID`, `momentBegin`)'
					. ' VALUES %A{is}'
					. ' ON DUPLICATE KEY UPDATE `plannerDataID` = `plannerDataID`',
					$this->data->toSQLValues());

		// sla de deelnemers ook op
		if (!$this->deelnemers instanceof PersoonVerzameling)
			$this->getDeelnemers();

		foreach ($this->deelnemers as $deelnemer)
			$WSW4DB->q('INSERT INTO `PlannerDeelnemer` (`plannerData_plannerDataID`, `persoon_contactID`)'
					. ' SELECT `plannerDataID`, %i AS `persoon_contactID` FROM `PlannerData`'
					. ' WHERE `planner_plannerID` = %i'
					. ' ON DUPLICATE KEY UPDATE `plannerData_plannerDataID` = `plannerData_plannerDataID`',
					$deelnemer->geefID(),
					$this->geefID());
		
		$WSW4DB->q('DELETE FROM `PlannerDeelnemer`'
				. ' WHERE `plannerData_plannerDataID` IN ('
					. 'SELECT `plannerDataID` FROM `PlannerData` WHERE `planner_plannerID` = %i'
				. ')'
				. (($this->deelnemers->aantal() > 0) ? ' AND (`persoon_contactID`) NOT IN (%Ai)' : '%_'),
				$this->geefID(),
				$this->deelnemers->keys());

		// verwijder onnodige data
		if ($this->data instanceof PlannerDataVerzameling && $this->data->allValid() && $this->data->aantal() > 0)
		{
			$WSW4DB->q('DELETE FROM `PlannerDeelnemer`'
					. ' WHERE `plannerData_plannerDataID` IN ('
						. 'SELECT `plannerDataID` FROM `PlannerData` WHERE `planner_plannerID` = %i AND `momentBegin` NOT IN (%As)'
					. ')',
					$this->geefID(),
					$this->data->toSQLArrayMomentBegin());
			$WSW4DB->q('DELETE FROM `PlannerData`'
					. ' WHERE `planner_plannerID` = %i'
					. ' AND `momentBegin` NOT IN (%As)',
					$this->geefID(),
					$this->data->toSQLArrayMomentBegin());
		}
	}

	/**
	 * Stuur alle deelnemers van de planners een mailtje dat ze hem moeten invullen.
	 * @param ontvangers Indien niet NULL, een verzameling van degenen die gemaild moeten worden als ze deelnemer zijn.
	 * @param nietAanVerzender Indien waar, stuurt geen mailtje aan degene die ingelogd is, aangezien die op de verzendknop heeft gedrukt.
	 * @returns Een melding over wat er allemaal goed/foutging.
	 */
	function mailDeelnemers($ontvangers = NULL, $nietAanVerzender = false, $extrainfo = "")
	{
		$verzender = Persoon::getIngelogd();
		if(!$verzender->getEmail()){
			return _("Kon niet mailen want je hebt geen emailadres ingesteld!");
		}

		$mailtje = _("Beste %voornaam%,

In de Activiteitenplanner wordt een activiteit gepland.
De bedoeling is dat je in de planner aangeeft welke dagen je kunt. Je kan ook
andere tijdstippen voorstellen. Hieronder vind je de informatie:

Activiteit: %TITEL%
Omschrijving:
%OMSCHRIJVING%

%EXTRAINFO%
Link: http://www.A-Eskwadraat.nl/Activiteiten/Planner/%PLANID%
Data: %DATA% in totaal, van %BEGIN% tot %EIND%

Volg de link naar de planner en vul in welke data je het meest geschikt vindt.

Deze mail is verstuurd door %MAKER% via de Activiteitenplanner.");

		$extraTekst = empty($extrainfo) ? "" : ("Extra informatie: \n" . htmlspecialchars($extrainfo) . "\n");

		$vervangLijst = array(
			'TITEL'  => $this->getTitel(),
			'OMSCHRIJVING' => $this->getOmschrijving(),
			'EXTRAINFO' => $extraTekst,
			'PLANID' => $this->geefID(),
			'DATA'	 => $this->getData()->aantal(),
			'BEGIN'	 => $this->getData()->first()->getMomentBegin()->format("d-m-Y"),
			'EIND'	 => $this->getData()->last()->getMomentBegin()->format("d-m-Y"),
			'MAKER'	 => PersoonView::naam($verzender)
		);
		foreach($vervangLijst as $item => $vervang)
		{
			$mailtje = str_replace('%' . $item . '%', $vervang, $mailtje);
		}

		$teller = 0;
		$bonusMelding = '';

		$aantalData = PlannerDataQuery::table()
			->whereProp('planner', $this)
			->count();

		$plannerData = PlannerDeelnemerQuery::table()
			->select('PlannerDeelnemer.persoon_contactID', array('aantal' => 'COUNT(*)'))
			->join('PlannerData', 'PlannerDeelnemer.plannerData_plannerDataID', 'PlannerData.plannerDataID')
			->join('Planner', 'Planner.plannerID', 'PlannerData.planner_plannerID')
			->groupBy('PlannerDeelnemer.persoon_contactID')
			->whereNotProp('antwoord', 'ONBEKEND')
			->whereInt('Planner.plannerID', $this->getPlannerID())
			->get();

		$aantalIngevuld = array();
		foreach ($plannerData as $record) {
			$aantalIngevuld[$record['persoon_contactID']] = $record['aantal'];
		}

		$deelnemers = $this->getDeelnemers();
		$mailers = array();

		foreach($deelnemers as $contact)
		{
			// skip mensen die geen mailtje hoeven
			if (($nietAanVerzender && $verzender == $contact) || ($ontvangers && !$ontvangers->bevat($contact))) {
				continue;
			}

			if (array_key_exists($contact->getContactID(), $aantalIngevuld) && $aantalIngevuld[$contact->getContactID()] == $aantalData) {
				/* Bug #7574:
				 * deze persoon heeft al alle data ingevuld en zal dus niet blij worden als die een reminder gestuurd krijgt!
				 */
				continue;
			}


			$contactMail = $contact->getEmail();
			if($contactMail) {
				$mailers[] = $contact;
			} else {
				$bonusMelding .= '<br>' . sprintf(_("%s kon niet gemaild worden omdat er geen emailadres bekend was."), PersoonView::naam($contact));
			}
		}

		foreach ($mailers as $contact) {
			$mail = new Email();
			$mail->setTo($contact);
			$mail->setFrom($verzender);
			$mail->setSubject(sprintf(_('Plannen van %s'), $this->getTitel()));
			$mail->setBody(nl2br($mailtje));
			$mail->send();
			$teller++;
		}

		$teller = count($mailers);
		if ($nietAanVerzender) {
			if($teller == 1)
				return sprintf(_("%d ander iemand gemaild over de planner."), $teller) . $bonusMelding;
			else
				return sprintf(_("%d andere mensen gemaild over de planner."), $teller) . $bonusMelding;
		} else {
			if($teller == 1)
				return sprintf(_("%d iemand gemaild over de planner."), $teller) . $bonusMelding;
			else
				return sprintf(_("%d mensen gemaild over de planner."), $teller) . $bonusMelding;
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
