<?
/**
 * $Id$
 */
class BugToewijzingVerzameling
	extends BugToewijzingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugToewijzingVerzameling_Generated
	}

	/**
	 * Zoek elke BugToewijzing die bij een Bug hoort.
	 *
	 * @param bug Een Bug.
	 * @param actief Indien True, zoek alleen BugToewzijngen die nog actief 
	 * zijn (cq waarbij eind NULL is).
	 * @param personen Indien dit een PersoonVerzameling is die een of meer 
	 * elementen heeft, wordt alleen gezocht op toewijzingen aan personen in de 
	 * verzameling.
	 * @return Een BugToewijzingVerzameling.
	 */
	public static function geefVanBug (Bug $bug, $actief = True, PersoonVerzameling $personen = NULL)
	{
		if(!$personen)
			$personen = new PersoonVerzameling();

		$query = BugToewijzingQuery::table()
			->whereProp('bug', $bug)
			->whereInProp('persoon', $personen);
		if($actief)
			$query->wherePropNull('eind');

		return $query->verzamel();
	}

	/**
	 * Zoek elke BugToewijzing die bij een bepaald BugBericht is begonnen.
	 *
	 * @param bericht Een BugBericht.
	 * @return Een BugToewijzingVerzameling.
	 */
	public static function geefBeginVanBugBericht (BugBericht $bericht)
	{
		return BugToewijzingQuery::table()
			->whereProp('begin', $bericht)
			->verzamel();
	}

	/**
	 * Zoek elke BugToewijzing die bij een bepaald BugBericht is geëindigd.
	 *
	 * @param bericht Een BugBericht.
	 * @return Een BugToewijzingVerzameling.
	 */
	public static function geefEindVanBugBericht (BugBericht $bericht)
	{
		return BugToewijzingQuery::table()
			->whereProp('eind', $bericht)
			->verzamel();
	}

	/**
	 *  Geeft alle fixers van bugs met hoeveel ze gefixt hebben
	 *
	 * @param amount De hoeveelheid die gereturnd moet worden
	 * @return Een sorted array met als keys de lidids en als value de hoeveelheid opgelost
	 */
	static public function geefFixersMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `BugToewijzing`.`persoon_contactID` as pers,COUNT(`Bug`.`bugID`) as aantal '
			.'FROM `BugToewijzing` '
			.'LEFT JOIN `Bug` ON `Bug`.`bugID` = `BugToewijzing`.`bug_bugID` '
			.'WHERE `Bug`.`status` = "OPGELOST" '
			.'OR `Bug`.`status` = "GECOMMIT" '
			.'GROUP BY `BugToewijzing`.`persoon_contactID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["pers"]] = $id["aantal"];
		}
		$aantal = $WSW4DB->q('MAYBEVALUE SELECT COUNT(`Bug`.`bugID`) '
			.'FROM `Bug` '
			.'WHERE `Bug`.`status` = "OPGELOST" '
			.'AND `bugID` NOT IN (SELECT `bug_bugID` FROM `BugToewijzing`)');
		$ret[""] = $aantal;


		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
