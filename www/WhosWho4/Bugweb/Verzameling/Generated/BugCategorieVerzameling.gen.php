<?
abstract class BugCategorieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BugCategorieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BugCategorieVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BugCategorieVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BugCategorieVerzameling van Commissie.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new BugCategorieVerzameling();

		return BugCategorieQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
