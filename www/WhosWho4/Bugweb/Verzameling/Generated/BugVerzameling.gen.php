<?
abstract class BugVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BugVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BugVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BugVerzameling.
	 */
	public function toMelderVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getMelderContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getMelderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugVerzameling een BugCategorieVerzameling.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze BugVerzameling.
	 */
	public function toCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new BugCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCategorieBugCategorieID()
			                      );
		}
		$this->positie = $origPositie;
		return BugCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BugVerzameling van Melder.
	 *
	 * @return BugVerzameling
	 * Een BugVerzameling die elementen bevat die bij de Melder hoort.
	 */
	static public function fromMelder($melder)
	{
		if(!isset($melder))
			return new BugVerzameling();

		return BugQuery::table()
			->whereProp('Melder', $melder)
			->verzamel();
	}
	/**
	 * @brief Maak een BugVerzameling van Categorie.
	 *
	 * @return BugVerzameling
	 * Een BugVerzameling die elementen bevat die bij de Categorie hoort.
	 */
	static public function fromCategorie($categorie)
	{
		if(!isset($categorie))
			return new BugVerzameling();

		return BugQuery::table()
			->whereProp('Categorie', $categorie)
			->verzamel();
	}
}
