<?
abstract class BugToewijzingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BugToewijzingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BugToewijzingVerzameling een BugVerzameling.
	 *
	 * @return BugVerzameling
	 * Een BugVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze BugToewijzingVerzameling.
	 */
	public function toBugVerzameling()
	{
		if($this->aantal() == 0)
			return new BugVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBugBugID()
			                      );
		}
		$this->positie = $origPositie;
		return BugVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugToewijzingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BugToewijzingVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugToewijzingVerzameling een BugBerichtVerzameling.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze BugToewijzingVerzameling.
	 */
	public function toBeginVerzameling()
	{
		if($this->aantal() == 0)
			return new BugBerichtVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getBeginBugBerichtID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getBeginBugBerichtID()
			                      );
		}
		$this->positie = $origPositie;
		return BugBerichtVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugToewijzingVerzameling een BugBerichtVerzameling.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze BugToewijzingVerzameling.
	 */
	public function toEindVerzameling()
	{
		if($this->aantal() == 0)
			return new BugBerichtVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getEindBugBerichtID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getEindBugBerichtID()
			                      );
		}
		$this->positie = $origPositie;
		return BugBerichtVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BugToewijzingVerzameling van Bug.
	 *
	 * @return BugToewijzingVerzameling
	 * Een BugToewijzingVerzameling die elementen bevat die bij de Bug hoort.
	 */
	static public function fromBug($bug)
	{
		if(!isset($bug))
			return new BugToewijzingVerzameling();

		return BugToewijzingQuery::table()
			->whereProp('Bug', $bug)
			->verzamel();
	}
	/**
	 * @brief Maak een BugToewijzingVerzameling van Persoon.
	 *
	 * @return BugToewijzingVerzameling
	 * Een BugToewijzingVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new BugToewijzingVerzameling();

		return BugToewijzingQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een BugToewijzingVerzameling van Begin.
	 *
	 * @return BugToewijzingVerzameling
	 * Een BugToewijzingVerzameling die elementen bevat die bij de Begin hoort.
	 */
	static public function fromBegin($begin)
	{
		if(!isset($begin))
			return new BugToewijzingVerzameling();

		return BugToewijzingQuery::table()
			->whereProp('Begin', $begin)
			->verzamel();
	}
	/**
	 * @brief Maak een BugToewijzingVerzameling van Eind.
	 *
	 * @return BugToewijzingVerzameling
	 * Een BugToewijzingVerzameling die elementen bevat die bij de Eind hoort.
	 */
	static public function fromEind($eind)
	{
		if(!isset($eind))
			return new BugToewijzingVerzameling();

		return BugToewijzingQuery::table()
			->whereProp('Eind', $eind)
			->verzamel();
	}
}
