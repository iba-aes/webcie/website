<?
/**
 * $Id$
 */
class BugCategorie
	extends BugCategorie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugCategorie_Generated

		$this->bugCategorieID = NULL;
	}

	/**
	 *  Bepaal of de huidig ingelogde gebruiker deze BugCategorie mag bekijken.
	 *
	 * @return True indien deze BugCategorie actief is of de ingelogde persoon 
	 * in de bij deze BugCategorie horende Commissie zit of bestuursrechten 
	 * heeft; False anders.
	 */
	public function magBekijken ()
	{
		return $this->getActief() || $this->magWijzigen();
	}

	/**
	 *  Bepaal of de huidig ingelogde gebruiker deze BugCategorie mag wijzigen.
	 *
	 * @return True indien de ingelogde persoon in de bij deze BugCategorie 
	 * horende Commissie zit of bestuursrechten heeft; False anders.
	 */
	public function magWijzigen ()
	{
		$ingelogd = Persoon::getIngelogd();
		if (!$ingelogd)
			return False;

		$mijnCies = $ingelogd->getCommissies();
		return BugCategorie::magKlasseWijzigen() || $mijnCies->bevat($this->getCommissie());
	}

	public function magVerwijderen ()
	{
		return $this->magWijzigen() && $this->getAantalBugs(False) == 0;
	}

	/**
	 *  Bepaal of de huidig ingelogd gebruiker deze BugCategorie mag verwijderen.
	 *
	 * @return True indien de ingelogde persoon de BugCategorie mag wijzigen en 
	 * deze geen enkele Bug met zich geassocieerd heeft; False anders.
	 * @see magWijzigen()
	 */
	/*public function magVerwijderen ()
	{
		return $this->magWijzigen() && $this->getAantalBugs(False) == 0;
	}*/

	/**
	 *  Bepaal de url van deze BugCategorie.
	 *
	 * @return een string die de locale url naar deze BugCategorie 
	 * representeert.
	 */
	public function url ()
	{
		return BUGWEBBASE . '/Categorie/' . $this->geefID();
	}

	/**
	 *  Zoek uit of deze BugCategorie gedeactiveerd kan worden.
	 *
	 * @return True indien deze bugCategorie geen open bugs meer heeft; False anders.
	 * @see getAantalBugs()
	 */
	public function kanDeactiveren ()
	{
		return $this->getAantalBugs() == 0;
	}

	public static function velden($welke = NULL)
	{
		switch($welke)
		{
		case 'nieuw':
		case 'viewWijzig':
			return array('Naam', 'Commissie');
		default:
			return parent::velden($welke);
		}
	}

	/**
	 *  Zoek alle Bugs in deze BugCategorie.
	 *
	 * @param alleenOpen indien True, zoek alleen op open Bugs.
	 * @return een BugVerzameling die alle (al dan niet open) Bugs in deze 
	 * BugCategorie bevat.
	 */
	public function getBugs ($alleenOpen = True)
	{
		return BugVerzameling::geefInCategorie($this, $alleenOpen);
	}

	public function getAantalBugs ($alleenOpen = True)
	{
		$query = BugQuery::table()
			->whereProp('Categorie', $this);

		if($alleenOpen) {
			$query->whereInProp('status', Bug::geefStatusSet('OPENS'));
		}

		return $query->count();
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
