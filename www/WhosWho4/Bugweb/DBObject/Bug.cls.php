<?
/**
 * $Id$
 */
class Bug
	extends Bug_Generated
{
	/** Varaibele die bericht van een nieuwe Bug bijhoudt **/
	protected $bericht = NULL;

	/** Cache-variabele voor reacties **/
	protected $reacties = NULL;

	/*** CONSTRUCTOR ***/
	public function __construct($moment = null, $categorie = null)
	{
		parent::__construct($moment);

		if ($categorie instanceof BugCategorie)
		{
			$this->categorie_bugCategorieID = $categorie->getBugCategorieID();
		}
	}
		
	/**
	 *  Maak een nieuwe Bug aan.
	 *
	 * Preferabel ten opzichte van de constructor omdat er automatisch een 
	 * BugBericht wordt aangemaakt zodra deze Bug wordt opgeslagen.
	 *
	 * @param melder de Persoon die de melder is van de nieuwe Bug.
	 * @param titel de titel van de nieuwe Bug.
	 * @param categorie de BugCategorie waar de nieuwe bij hoort.
	 * @param status de status van de nieuwe Bug. Moet een van 'OPEN', 'BEZIG', 
	 * 'OPGELOST', 'UITGESTELD', 'GESLOTEN', 'BOGUS', 'DUBBEL', 'WANNAHAVE' of 
	 * 'FEEDBACK' zijn.
	 * @param prioriteit de prioriteit van de nieuwe Bug. Moet een van 'HOOG', 
	 * 'GEMIDDELD' of 'LAAG' zijn.
	 * @param niveau het niveau van de nieuwe Bug. Moet een van 'OPEN', 'INGELOGD'
	 * of 'CIE' zijn.
	 * @param bericht het bericht van de nieuwe Bug.
	 * @param moment een DateTimeLocale die het moment representeert waarop de nieuwe 
	 * Bug is ingediend.
	 * @return de nieuwe Bug.
	 */
	public static function nieuweBug (Persoon $melder, $titel, BugCategorie $categorie, $status, $prioriteit, $niveau, $level, $bericht, DateTimeLocale $moment)
	{
		$bug = new Bug($moment);

		$bug->setMelder($melder);
		$bug->nieuweBugSetVelden($titel, $categorie, $status, $prioriteit, $niveau, $level, $bericht);

		return $bug;
	}

	/**
	 * Geef een willekeurige bug (die de huidige gebruiker ook echt mag zien).
	 */
	static public function geefWillekeurig()
	{
		$res = BugVerzameling::zoeken(null, null, null, 'OPENS')->willekeurig();

		return $res;
	}

	/**
	 *  Maak een nieuwe Bug aan die door een e-mail is binnengekomen.
	 *
	 * Preferabel ten opzichte van de constructor omdat er automatisch een 
	 * BugBericht wordt aangemaakt zodra deze Bug wordt opgeslagen.
	 *
	 * @param melderEmail Het e-mailadres van de melder.
	 * @param titel de titel van de nieuwe Bug.
	 * @param categorie de BugCategorie waar de nieuwe bij hoort.
	 * @param status de status van de nieuwe Bug. Moet een van 'OPEN', 'BEZIG', 
	 * 'OPGELOST', 'UITGESTELD', 'GESLOTEN', 'BOGUS', 'DUBBEL', 'WANNAHAVE' of 
	 * 'FEEDBACK' zijn.
	 * @param prioriteit de prioriteit van de nieuwe Bug. Moet een van 'HOOG', 
	 * 'GEMIDDELD' of 'LAAG' zijn.
	 * @param niveau het niveau van de nieuwe Bug. Moet een van 'OPEN', 'INGELOGD'
	 * of 'CIE' zijn.
	 * @param bericht het bericht van de nieuwe Bug.
	 * @param moment een DateTimeLocale die het moment representeert waarop de nieuwe 
	 * Bug is ingediend.
	 * @return de nieuwe Bug.
	 */
	public static function nieuweBugVanEmail ($melderEmail, $titel, BugCategorie $categorie, $status, $prioriteit, $niveau, $bericht, DateTimeLocale $moment)
	{
		$bug = new Bug($moment);

		if ($melderEmail && checkEmailFormat($melderEmail))
		{
			$bug->setMelderEmail($melderEmail);
		}
		else
		{
			throw new InvalidArgumentException("Ongeldig e-mailadres $melderEmail");
		}

		$bug->nieuweBugSetVelden($titel, $categorie, $status, $prioriteit, $niveau, 'GEMIDDELD', $bericht);

		return $bug;
	}

	/**
	 * Stel de velden in van deze Bug; alleen te gebruiken indien deze nieuw 
	 * is. Submethode van nieuweBug() en nieuweBugVanEmail(), de parameters 
	 * komen daarmee overeen.
	 *
	 * @see nieuweBug
	 * @see nieuweBugVanEmail
	 */
	protected function nieuweBugSetVelden ($titel, BugCategorie $categorie, $status, $prioriteit, $niveau, $level, $bericht)
	{
		$this->setTitel($titel);
		$this->setCategorie($categorie);
		if (in_array($status, self::enumsStatus()))
		{
			$this->setStatus($status);
		}
		if (in_array($prioriteit, self::enumsPrioriteit()))
		{
			$this->setPrioriteit($prioriteit);
		}
		if (in_array($niveau, self::enumsNiveau()))
		{
			$this->setNiveau($niveau);
		}
		if (in_array($level, self::enumsLevel()))
		{
			$this->setLevel($level);
		}

		$this->bericht = $bericht;
	}

	/**
	 * Voer acties uit na het invoeren van een nieuwe Bug.
	 *
	 * Als er iemand ingelogd is, wordt die Persoon als volger van de Bug 
	 * ingesteld. Daarna worden er mails verstuurd naar de Commissie en alle 
	 * volgers.
	 */
	protected function postNieuweBug ()
	{
		if ($ingelogd = Persoon::getIngelogd())
		{
			$this->setVolger($ingelogd);
		}

		$bericht = $this->getEersteBericht();
		BugBerichtView::verstuurMails($bericht, True);
	}

	/**
	 * Voeg een BugBericht toe aan deze Bug.
	 *
	 * Deze methode handelt de caching van de huidige titel, categorie, 
	 * prioriteit en status van deze Bug af.
	 *
	 * @param bugBericht Een BugBericht.
	 */
	public function voegBugBerichtToe (BugBericht $bugBericht)
	{
		$titel = $bugBericht->getTitel();
		$categorieID = $bugBericht->getCategorieBugCategorieID();
		$prioriteit = $bugBericht->getPrioriteit();
		$status = $bugBericht->getStatus();
		$niveau = $bugBericht->getNiveau();
		$level = $bugBericht->getLevel();

		// Update de cache-info van de Bug
		$this->setTitel($titel);
		$this->setCategorie($categorieID);
		if (in_array($prioriteit, Bug::enumsPrioriteit()))
		{
			$this->setPrioriteit($prioriteit);
		}
		if (in_array($status, Bug::enumsStatus()))
		{
			$this->setStatus($status);
		}
		if (in_array($niveau, Bug::enumsNiveau()))
		{
			$this->setNiveau($niveau);
		}
		if (in_array($level, self::enumsLevel()))
		{
			$this->setLevel($level);
		}

		$this->postNieuwBugBericht($bugBericht);
	}

	protected function postNieuwBugBericht (BugBericht $bugBericht)
	{
		if ($ingelogd = Persoon::getIngelogd())
		{
			$this->setVolger($ingelogd);
		}

		BugBerichtView::verstuurMails($bugBericht, False);
	}


	/**
	 *  Bepaal of de huidig ingelogde gebruiker deze Bug mag bekijken.
	 *
	 * @return Of de BugCategorie van deze Bug bekeken mag worden.
	 */
	public function magBekijken ()
	{
		$niveau = false;
		switch ($this->getNiveau())
		{
		case 'OPEN':
			$niveau = true;
			break;
		case 'INGELOGD':
			if(Persoon::getIngelogd()) {
				$niveau = true;
			}
			break;
		case 'CIE':
			$cie = $this->getCategorie()->getCommissie();
			if($cie->hasLid(Persoon::getIngelogd()) || hasAuth('bestuur')) {
				$niveau = true;
			}
			break;
		default:
			$niveau = false;
			break;
		}
		$isMelder = $this->getMelder() == Persoon::getIngelogd();
		return (($this->getCategorie()->magBekijken() && $niveau) || $isMelder);
	}

	/**
	 *  Bepaal of de huidig ingelogde gebruiker deze bug mag wijzigen.
	 *
	 * Een bug mag gewijzigd worden als aan de volgende voorwaarden is voldaan:
	 * * De gebruiker is ingelogd.
	 * * De BugCategorie van de Bug is actief; of de gebruiker mag de Commissie 
	 * wijzigen.
	 * * De BugCategorie is actief; of de gebruiker mag de Commissie wijzigen.
	 *
	 * @return True indien de gebruiker de Bug mag wijzigen; False anders.
	 */
	public function magWijzigen ()
	{
		$categorie = $this->getCategorie();
		$cie = $this->getCategorie()->getCommissie();

		return hasAuth('ingelogd') &&
			(($categorie->getActief() || $cie->magWijzigen()) &&
			($cie->isActief() || $cie->magWijzigen()) || 
			$this->getMelder() == Persoon::getIngelogd());
	}

	/**
	 *  Bepaal of de huidige ingelogde gebruiker op de bug mag reageren.
	 *
	 * Iemand mag op een bug reageren als de persoon de bug mag wijzigen
	 * of als iemand de bug volgt.
	 *
	 * @return True indien er gereageerd mag worden, false anders.
	 */
	public function magReageren ()
	{
		// Omdat we naar GitLab overschakelen houden we momenteel reacties even tegen.
		return false;
		return hasAuth('ingelogd');
	}

	/**
	 *  Bepaal de url van deze Bug.
	 *
	 * @return een string die de locale url naar deze Bug representeert.
	 */
	public function url ()
	{
		return BUGWEBBASE . '/' . $this->geefID();
	}

	public function opslaan ($classname = 'Bug')
	{
		parent::opslaan($classname);

		// Sla ook automatisch een BugBericht op als een bericht is meegegeven
		if ($bericht = $this->bericht)
		{
			if (($melder = $this->getMelder()) instanceof Persoon)
			{
				$bugBericht = BugBericht::nieuwBugBericht($this, $melder, $this->getTitel(), $this->getCategorie(), $this->getStatus(), $this->getPrioriteit(), new PersoonVerzameling(), $this->getNiveau(), $bericht, $this->getMoment(), null, $this->getLevel());
			}
			else
			{
				$bugBericht = BugBericht::nieuwBugBerichtVanEmail($this, $this->getMelderEmail(), $this->getTitel(), $this->getCategorie(), $this->getStatus(), $this->getPrioriteit(), new PersoonVerzameling(), $this->getNiveau(), $bericht, $this->getMoment());
			}
			$bugBericht->opslaan();

			$this->postNieuweBug($bugBericht);
		}
	}

	public static function velden ($welke = NULL)
	{
		switch ($welke)
		{
		case 'viewWijzig':
		case 'set':
			return array('Titel', 'Categorie', 'Status', 'Prioriteit', 'Niveau', 'Level');
		case 'verplicht':
			return array('Titel', 'Categorie', 'Status', 'Prioriteit');
		default:
			return parent::velden($welke);
		}
	}

	/**
	 *  Bepaal of deze Bug open is.
	 *
	 * @return True indien de status van deze Bug een van 'OPEN', 'BEZIG', 
	 * 'UITGESTELD', 'WANNAHAVE' of 'FEEDBACK' is, False anders.
	 */
	public function isOpen ()
	{
		global $bugStatusSets;

		return in_array($this->getStatus(), $bugStatusSets['OPENS']);
	}

	/**
	 *  Geef het e-mailadres van deze Bug.
	 *
	 * @return Het e-mailadres van deze Bug, bijvoorbeeld:
	 * Bug1234@A-Eswkadraat.nl
	 */
	public function getEmail ()
	{
		return sprintf('Bugsysteem A–Eskwadraat <bug%d@A-Eskwadraat.nl>', $this->geefID());
	}

	/**
	 *  Geef een int die de prioriteit van deze Bug representeert.
	 *
	 * @return 0, 1 of 2 voor respectievelijk prioriteit prioriteit 'LAAG', 
	 * 'GEMIDDELD' en 'HOOG'.
	 */
	public function getPrioriteitInt ()
	{
		return array_search($this->getPrioriteit(), self::enumsPrioriteit());
	}

    /**
     * Geef een int die het niveau (level) van deze Bug representeert.
     *
     * @return 0, 1 of 2 voor respectievelijk level 'Beginner',
     * 'Gemiddeld' en 'Expert'
     */
	public function getLevelInt ()
	{
		return array_search($this->getLevel(), self::enumsLevel());
	}

	/**
	 *  Zoek alle berichten die bij deze Bug horen.
	 *
	 * @return een BugBerichtVerzameling met alle BugBerichten van deze Bug.
	 */
	public function getBerichten ()
	{
		return BugBerichtVerzameling::geefVanBug($this);
	}

	/**
	 *  Zoek alle reacties die bij deze Bug horen.
	 *
	 * @return een BugBerichtVerzameling met alle BugBerichten van deze Bug 
	 * behalve de eerste.
	 */
	public function getReacties ()
	{
		if (!($this->reacties instanceof BugBerichtVerzameling))
		{
			$this->reacties = BugBerichtVerzameling::geefReacties($this);
		}
		return $this->reacties;
	}

	/**
	 *  Zoek het oorspronkelijke bericht van deze Bug.
	 *
	 * @return het eerste BugBericht van deze Bug.
	 */
	public function getEersteBericht ()
	{
		return BugBericht::geefEersteVanBug($this);
	}

	/**
	 *  Zoek het laatste bericht van deze Bug.
	 *
	 * @return het laatste BugBericht van deze Bug.
	 */
	public function getLaatsteBericht ()
	{
		return BugBericht::geefLaatsteVanBug($this);
	}

	/**
	 *  Zoek alle BugToewijzing-objecten die bij deze Bug horen.
	 *
	 * @return Een BugToewijzingVerzameling met BugToewijzing-bject van deze 
	 * Bug.
	 */
	public function getToewijzingen (PersoonVerzameling $personen = NULL)
	{
		return BugToewijzingVerzameling::geefVanBug($this, False, $personen);
	}

	/**
	 *  Zoek alle actieve BugToewijzing-objecten die bij deze Bug horen.
	 *
	 * @return Een BugToewijzingVerzameling met BugToewijzing-bject van deze 
	 * Bug.
	 */
	public function getActieveToewijzingen (PersoonVerzameling $personen = NULL)
	{
		return BugToewijzingVerzameling::geefVanBug($this, True, $personen);
	}

	/**
	 *  Zoek alle Personen die aan deze Bug zijn toegewezen.
	 *
	 * @return een PersoonVerzameling met Personen die aan deze Bug zijn 
	 * toegewezen.
	 */
	public function getToewijzingPersonen ()
	{
		return PersoonVerzameling::getBugToewijzingen($this);
	}

	/**
	 *  Zoek alle Personen die deze Bug volgen.
	 *
	 * @return een PersoonVerzameling met Personen die deze Bug volgen.
	 */
	public function getVolgerPersonen ()
	{
		return PersoonVerzameling::getBugVolgers($this);
	}

	/**
	 *  Zoek de BugVolger bij deze Bug en een Persoon.
	 *
	 * @param persoon een Persoon.
	 * @return de BugVolger die bij deze Bug en persoon hoort.
	 */
	public function getVolger (Persoon $persoon)
	{
		$volger = BugVolger::geef($this, $persoon);
		if ($volger)
		{
			return $volger;
		}

		return new BugVolger($this, $persoon);
	}

	/**
	 *  Bepaal of iemand deze Bug volgt.
	 *
	 * @param persoon Een Persoon.
	 * @return True indien persoon deze Bug volgt; False anders.
	 */
	public function isVolger (Persoon $persoon)
	{
		if (BugVolger::geef($this, $persoon))
			return True;
		return False;
	}

	/**
	 *  Stel in of deze Bug door een bepaalde Persoon wordt gevolgd.
	 *
	 * @param persoon een Persoon.
	 * @param value True indien deze Bug door persoon wordt gevolgd; False anders.
	 */
	public function setVolger (Persoon $persoon, $value = True)
	{
		$volger = $this->getVolger($persoon);
		if ($value)
		{
			$volger->opslaan();
		}
		else
		{
			$returnVal = $volger->verwijderen();
			if(!is_null($returnVal)) {
				user_error($returnVal, E_USER_ERROR);
			}
		}
	}

	/**
	 *  Set het bericht van een bug
	 *
	 * @param bericht Een string wat het bericht wordt
	 */
	public function setBericht($bericht)
	{
		$this->bericht = $bericht;
	}

	/**
	 *  Geeft de bericht-property terug
	 *
	 * @return Een string die het bericht is
	 */
	public function getBericht()
	{
		return $this->bericht;
	}

	public static function geefStatusSet ($setnaam)
	{
		global $bugStatusSets;

		if (in_array($setnaam, self::enumsStatus()))
			return array($setnaam);

		if (array_key_exists($setnaam, $bugStatusSets))
			return $bugStatusSets[$setnaam];

		return array();
	}

	/**
	 * @param prioriteitComp hoe vergeleken moet worden: <=, =, =>
	 * @param prioriteit getal in de range: [ 0, ..., count(Bug::enumsPrioriteit()) )
	 */
	public static function prioriteiten($prioriteitComp, $prioriteit) {
		$prioriteiten = Bug::enumsPrioriteit();
		$result = array();
		switch ($prioriteitComp) {
		case "=":
			return array($prioriteiten[$prioriteit]);
		case "<=":
			foreach ($prioriteiten as $key => $value)
				if ($key <= $prioriteit) $result[] = $value;
			break;
		case ">=":
			foreach ($prioriteiten as $key => $value)
				if ($key >= $prioriteit) $result[] = $value;
			break;
		}
		return $result;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
