<?
/**
 * $Id$
 */
class BugBericht
	extends BugBericht_Generated
{
	protected $toewijzingen;

	/*** CONSTRUCTOR ***/
	/* Let op dat je alle velden die bij Bug zelf horen meegeeft, anders
	 * verander je misschien de verkeerde dingen. Bij BugBericht zelf horen
	 * i.i.g. $moment, $revisie, en $commit.
	 */
	public function __construct($bug = null, $titel = null, $categorie = null,
		$status = 'OPEN', $prioriteit = 'ONBENULLIG', $niveau = 'OPEN',
		$bericht = '', $moment = null, $revisie = null, $level = 'GEMIDDELD', $commit = null)
	{
		parent::__construct($bug, $titel, $categorie, $status, $prioriteit, $niveau
			, $bericht, $moment, $revisie, $level, $commit); // BugBericht_Generated
	}

	/**
	 *  Maak een nieuw BugBericht aan.
	 *
	 * Preferabel ten opzichte van de constructor omdat automatisch alle velden 
	 * worden ingesteld.
	 *
	 * @param bug De Bug waar dit BugBericht bij hoort.
	 * @param melder de Persoon die de melder is van de nieuwe Bug.
	 * @param titel de titel van de nieuwe Bug.
	 * @param categorie de BugCategorie waar de nieuwe bij hoort.
	 * @param status de status van de nieuwe Bug. Moet een van 'OPEN', 'BEZIG', 
	 * 'OPGELOST', 'UITGESTELD', 'GESLOTEN', 'BOGUS', 'DUBBEL', 'WANNAHAVE' of 
	 * 'FEEDBACK' zijn.
	 * @param prioriteit de prioriteit van de nieuwe Bug. Moet een van 'HOOG', 
	 * 'GEMIDDELD' of 'LAAG' zijn.
	 * @param toewijzingen PersoonVerzameling met als elementen de mensen die 
	 * vanaf nu zijn toegewezen aan de Bug.
	 * @param niveau Het zichtbaarheidsniveau bij dit Bugbericht
	 * @param bericht Het bericht van dit nieuwe BugBericht.
	 * @param moment Een DateTimeLocale die het moment representeert waarop dit
	 * BugBericht is ingediend.
	 * @param revisie Het getal van de revisie waardoor dit BugBericht is 
	 * aangemaakt
	 * @param level De moeilijkheid van de bug
	 * @return Het nieuwe BugBericht.
	 */
	public static function nieuwBugBericht (Bug $bug
		, Persoon $melder
		, $titel, BugCategorie $categorie
		, $status, $prioriteit
		, PersoonVerzameling $toewijzingen
		, $niveau
		, $bericht
		, DateTimeLocale $moment
		, $revisie  = NULL
		, $level = 'GEMIDDELD'
		, $commit = NULL)
	{
		$bugBericht = new BugBericht($bug, $titel, $categorie, $status, $prioriteit, $niveau, $bericht, $moment, $revisie, $level, $commit);
		$bugBericht->setMelder($melder);
		$isGelukt = $bugBericht->setToewijzingen($toewijzingen);

		return $bugBericht;
	}

	/**
	 *  Maak een nieuw BugBericht aan die via een e-mail is 
	 * binnengekomen.
	 *
	 * Preferabel ten opzichte van de constructor omdat automatisch alle velden 
	 * worden ingesteld.
	 *
	 * @param bug De Bug waar dit BugBericht bij hoort.
	 * @param melderEmail het e-mailadres van de melder.
	 * @param titel de titel van de nieuwe Bug.
	 * @param categorie de BugCategorie waar de nieuwe bij hoort.
	 * @param status de status van de nieuwe Bug. Moet een van 'OPEN', 'BEZIG', 
	 * 'OPGELOST', 'UITGESTELD', 'GESLOTEN', 'BOGUS', 'DUBBEL', 'WANNAHAVE' of 
	 * 'FEEDBACK' zijn.
	 * @param prioriteit de prioriteit van de nieuwe Bug. Moet een van 'HOOG', 
	 * 'GEMIDDELD' of 'LAAG' zijn.
	 * @param toewijzingen PersoonVerzameling met als elementen de mensen die 
	 * vanaf nu zijn toegewezen aan de Bug.
	 * @param bericht Het bericht van dit nieuwe BugBericht.
	 * @param moment Een DateTimeLocale die het moment representeert waarop dit
	 * @param niveau Het zichtbaarheidsniveau bij dit Bugbericht
	 * BugBericht is ingediend.
	 * @param revisie Het getal van de revisie waardoor dit BugBericht is 
	 * aangemaakt
	 * @param level De moeilijkheid van de bug
	 * @return Het nieuwe BugBericht.
	 */
	public static function nieuwBugBerichtVanEmail (Bug $bug, $melderEmail, $titel, BugCategorie $categorie, $status, $prioriteit, PersoonVerzameling $toewijzingen, $niveau = 'OPEN', $bericht, DateTimeLocale $moment, $revisie = NULL, $level = 'GEMIDDELD', $commit = null)
	{
		$bugBericht = new BugBericht($bug, $titel, $categorie, $status, $prioriteit, $niveau, $bericht, $moment, $revisie, $level, $commit);

		if ($melderEmail && checkEmailFormat($melderEmail))
		{
			$bugBericht->melderEmail = $melderEmail;
		}
		else
		{
			throw new InvalidArgumentException("Ongeldig e-mailadres $melderEmail");
		}
		$isGelukt = $bugBericht->setToewijzingen($toewijzingen);

		return $bugBericht;
	}

	/**
	 * @brief Maak een nieuw BugBericht die door een Git commit getriggerd is
	 * (bijvoorbeeld door een webhook).
	 *
	 * @param commit De commit die tot het plaatsen van dit bericht heeft geleid.
	 * @param bug De bug waaraan het bericht wordt toegevoegd.
	 * @param status De nieuwe status van de bug.
	 *
	 * @return `NULL` als er al een BugBericht bij deze bug is die naar $commit
	 * 		verwijst, anders het nieuwe BugBericht.
	 */
	public static function vanGitCommit(Bug $bug, $bericht, $commit, $email, $status)
	{
		$status = strtoupper($status); // Voor het gemak

		Page::setVoorkeur('text'); // Dit moet ook om de een of andere reden.
		$page = Page::getInstance('text');

		$commitEerderGenoemd = false;
		foreach ($bug->getBerichten() as $bugBericht) {
			// FIXME(?): Dit werkt niet goed als de bug vanuit een commit
			// gesloten is, en vervolgens handmatig heropend. Maar op dat punt
			// moet je het maar handmatig doen ofzo.
			if ($bugBericht->getCommit() == $commit
				&& !($bug->getStatus() == 'GECOMMIT' && $status != 'OPGELOST'))
			{
				$commitEerderGenoemd = true;
				break;
			}
		}

		if ($commitEerderGenoemd) {
			$page->add(
				"Bug " . $bug->getBugID() .  " is al " . strtolower($bug->getStatus()) . "."
			);
			return null;
		}

		if ($bug->getStatus() === 'OPGELOST')
			// In dit geval zou het gek zijn om de bug weer open te zetten.
			$status = 'OPGELOST';

		$committer = self::lidVanEmail($email);

		if (is_null($committer)) {
			$page->add("Kan geen lid vinden bij email '$email'");
			return null;
		}

		// Merk op dat we hier alle dingen van de bug overnemen, want anders
		// zetten we misschien de titel o.i.d. op NULL. $moment en $revisie etc.
		// zijn alleen voor bugberichten, dus die laten we gewoon leeg
		$bericht = new BugBericht(
			$bug, $bug->getTitel(), $bug->getCategorie(), $status,
			$bug->getPrioriteit(), $bug->getNiveau(), $bericht, null,
			null, $bug->getLevel(), $commit
		);
		$bericht->setMelder($committer);
		$toewijzingen = new PersoonVerzameling();
		$toewijzingen->voegToe($committer);
		$bericht->setToewijzingen($toewijzingen);

		return $bericht;
	}

	/**
	 * @brief Zoek een lid bij een bepaald e-emailadres.
	 *
	 * Dit adres moet óf het adres bij het account van de user zijn, óf hun
	 * e-mailadres op het systeem (`user@a-eskwadraat.nl`).
	 *
	 * @return Een Lid als die gevonden is, anders `NULL`.
	 */
	static function lidVanEmail($email) {
		$lid = Lid::zoekOpUniekeEmail($email);

		if (!is_null($lid))
			return $lid;

		try
		{
			$systeem = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return null;
		}

		if (preg_match("/([a-zA-Z0-9]+)@a-eskwadraat.nl/i", $email, $match) === 0)
			return null;

		$lidnr = $systeem->lidnrVanLogin($match[1]);

		if (is_null($lidnr))
			return null;

		return Lid::geef($lidnr);
	}

	/**
	 * Stel de toewijzingen van dit BugBericht in;
	 *
	 * @return True indien dit een nuttig BugBericht is
	 */
	protected function setToewijzingen(PersoonVerzameling $toewijzingen)
	{
		$this->toewijzingen = $toewijzingen;

		$oudeToewijzingen = $this->getBug()->getToewijzingPersonen();
		// oudeToewijzingen bevat hierna 0 elementen indien gelijk aan toewijzingen
		$oudeToewijzingen->difference($toewijzingen);

		// Check of het nieuwe BugBericht daadwerkelijk iets toevoegt aan de Bug
		if ($oudeToewijzingen->aantal() == 0)
			return False;

		return True;
	}

	/**
	 *  Bepaal of de huidig ingelogde gebruiker dit BugBericht mag 
	 * bekijken.
	 *
	 * @return Of de Bug, waar dit BugBericht bij hoort, bekeken mag worden.
	 */
	public function magBekijken ()
	{
		return $this->getBug()->magBekijken();
	}

	public function magWijzigen ()
	{
		return $this->getBug()->magWijzigen();
	}

	public static function velden ($welke = NULL)
	{
		switch ($welke)
		{
		case 'viewWijzig':
			return array('Titel', 'Categorie', 'Status', 'Prioriteit', 'Bericht');
		default:
			return parent::velden($welke);
		}
	}

	public function opslaan ($classname = 'BugBericht')
	{
		parent::opslaan($classname);

		// Sla ook meteen de toewijzingen op die meegegeven zijn
		$nieuweToewijzingPersonen = $this->toewijzingen;
		if ($nieuweToewijzingPersonen instanceof PersoonVerzameling)
		{
			$bug = $this->getBug();

			$huidigeToewijzingen = $bug->getActieveToewijzingen();
			$huidigeToewijzingPersonen = $huidigeToewijzingen->toPersoonVerzameling();

			foreach ($nieuweToewijzingPersonen as $persoon)
			{
				if (!$huidigeToewijzingPersonen->bevat($persoon->geefID()))
				{
					$toewijzing = BugToewijzing::nieuweBugToewijzing($persoon, $this);
					$toewijzing->opslaan();
				}
			}

			foreach ($huidigeToewijzingen as $toewijzing)
			{
				if (!$nieuweToewijzingPersonen->bevat($toewijzing->getPersoon()->geefID()))
				{
					$toewijzing->setEind($this);
					$toewijzing->opslaan();
				}
			}
		}
	}

	public function url ()
	{
		return BUGWEBBASE . '/Bericht/' . $this->getBugBerichtID();
	}

	public function getBeginToewijzingen ()
	{
		return BugToewijzingVerzameling::geefBeginVanBugBericht($this);
	}

	public function getEindToewijzingen ()
	{
		return BugToewijzingVerzameling::geefEindVanBugBericht($this);
	}

	/**
	 *  Stel de melder van dit BugBericht in.
	 *
	 * De melder van deze bug wordt persoon, de melderEmail wordt leeg.
	 *
	 * @param persoon De nieuwe melder.
	 */
	public function verifieerEmail (Persoon $persoon)
	{
		$this->setMelder($persoon);
		$this->setMelderEmail(NULL);
	}

	/**
	 *  Zoek het volgende BugBericht in de Bug.
	 *
	 * @return het BugBericht dat bij dezelfde Bug hoort als dit BugBericht met 
	 * het kleinste moment groter dan het moment van dit BugBericht, of NULL 
	 * als deze niet bestaat.
	 */
	public function volgende ()
	{
		return $this->aangrenzende(False);
	}

	/**
	 *  Zoek het vorige BugBericht in de Bug.
	 *
	 * @return het BugBericht dat bij dezelfde Bug hoort als dit BugBericht met 
	 * het grootste moment kleiner dan het moment van dit BugBericht, of NULL 
	 * als deze niet bestaat.
	 */
	public function vorige ()
	{
		return $this->aangrenzende(True);
	}

	/**
	 *  Zoek een chronologisch aangrenzend BugBericht in de Bug.
	 *
	 * @param vorige True om naar het vorige BugBericht te zoeken, False om 
	 * naar het volgende BugBericht te zoeken.
	 * @return het BugBericht dat bij dezelfde Bug hoort als dit BugBericht en 
	 * chronologisch direct voor of na dit BugBericht komt, of NULL als deze 
	 * niet bestaat.
	 */
	private function aangrenzende ($vorige = False)
	{
		return BugBerichtQuery::table()
			->whereProp('Bug', $this->getBug())
			->whereProp('moment', ($vorige ? '<' : '>'), $this->getMoment())
			->orderByDesc('moment')
			->limit(1)
			->geef();
	}

	/**
	 *  Zoek chronologisch het eerste BugBericht bij een Bug.
	 *
	 * @param bug een Bug.
	 * @return het BugBericht dat bij bug hoort met het kleinste moment.
	 */
	public static function geefEersteVanBug (Bug $bug)
	{
		return BugBerichtQuery::table()
			->whereProp('Bug', $bug)
			->orderByAsc('moment')
			->limit(1)
			->geef();
	}

	/**
	 *  Zoek chronologisch het laatste BugBericht bij een Bug.
	 *
	 * @param bug een Bug.
	 * @return het BugBericht dat bij bug hoort met het grootste moment.
	 */
	public static function geefLaatsteVanBug (Bug $bug)
	{
		return BugBerichtQuery::table()
			->whereProp('Bug', $bug)
			->orderByDesc('moment')
			->limit(1)
			->geef();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
