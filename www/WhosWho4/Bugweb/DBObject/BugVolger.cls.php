<?
/**
 * $Id$
 */
class BugVolger
	extends BugVolger_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // BugVolger_Generated
	}

	/**
	 * Bepaal of de huidig ingelogde gebruiker deze BugVolger mag bekijken.
	 *
	 * @return True indien de Persoon bij dit BugVolger-object de huidige 
	 * ingelogd persoon is of de gebruiker godrechten heeft; False anders.
	 */
	public function magBekijken ()
	{
		return $this->magVerwijderen();
	}

	/**
	 * Bepaal of de huidig ingelogde gebruiker deze BugVolger mag wijzigen.
	 *
	 * @return False.
	 */
	public function magWijzigen ()
	{
		return False;
	}

	/**
	 * Bepaal of de huidig ingelogde gebruiker deze BugVolger mag verwijderen.
	 *
	 * @return True indien de Persoon bij dit BugVolger-object de huidige 
	 * ingelogd persoon is of de gebruiker godrechten heeft; False anders.
	 */
	public function magVerwijderen ()
	{
		return hasAuth('god') || Persoon::getIngelogd() === $this->getPersoon();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
