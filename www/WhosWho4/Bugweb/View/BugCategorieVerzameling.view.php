<?
/**
 * $Id$
 */
abstract class BugCategorieVerzamelingView
	extends BugCategorieVerzamelingView_Generated
{

	/**
	 *  Maak een overzicht van BugCategorieen.
	 *
	 * @param cies De CommissieVerzameling die de Commissies bevat waarvoor 
	 * BugCategorieen worden opgezocht.
	 * @param toonInactieve Indien True, toon ook alle inactieve categorieën.
	 * @param magInactieveTonen Indien True, toon een formuliertje waarmee 
	 * gewisseld kan worden tussen alleen actieve en alle categorieën.
	 * @param magAanmaken Indien True, toon een knop die naar de pagina leidt 
	 * waar een nieuwe BugCategorie kan worden aangemaakt.
	 * @return Een HtmlDiv die een lijst van categorieën bevat.
	 * @see BugCategorieVerzameling::geefAlleBugCommissies
	 * @see BugCategorieVerzameling::geefVanCommissie
	 */
	public static function overzicht (CommissieVerzameling $cies, $toonInactieve, $magInactieveTonen, $magAanmaken)
	{
		$form = new HtmlForm('GET');
		$form->addClass('text-center');
		$form->add($btnBar = new HtmlDiv(NULL, "btn-group"));
		$btnBar->setAttribute('role', 'group');

		if ($magAanmaken) {
			$btnBar->add(HtmlAnchor::button(BUGWEBBASE . '/Categorie/Nieuw',
					HtmlSpan::fa('plus', _('Nieuw')) . '&nbsp;' . _('Maak een nieuwe categorie'), null, null, null, 'default'));
		}

		if ($magInactieveTonen) {
			$btnBar->add(new HtmlButton('submit', _('Toon actieve categorie&#235;n'), 'toonalles', ''));
			$btnBar->add(new HtmlButton('submit', _('Toon alle categorie&#235;n'), 'toonalles', 'true'));
		}

		if ($btnBar->numHtmlChildren() == 0) $form = NULL;

		$cats = new HtmlDiv(NULL, 'row');
		foreach ($cies as $cie) {
			$categorieen = BugCategorieVerzameling::geefVanCommissie($cie, $toonInactieve);
			if ($categorieen->aantal() > 0) {
				$cats->add(new HtmlDiv(array(
					new HtmlHeader(3, CommissieView::waardeNaam($cie)),
					self::lijst($categorieen)
				), 'col-md-4'));
			}
		}

		return array($form, $cats);
	}

	/**
	 *  Maak een lijst van een BugCategorieVerzameling.
	 *
	 * @param categorieen Een BugCategorieVerzameling.
	 * @return Een HtmlList met de namen van de categorieën in categorieen.
	 */
	public static function lijst (BugCategorieVerzameling $categorieen)
	{
		$ul = new HtmlList();
		foreach ($categorieen as $categorie) {
			$ul->add($li = new HtmlListItem(BugCategorieView::makeLink($categorie)));
			if (!$categorie->getActief()) {
				$li->addClass('disabled');
			}
		}
		return $ul;
	}

	/**
	 *  Maak een ComboBox met alle BugCategorieen die bij een van de 
	 * Commissies in cies horen.
	 *
	 * @param naam De input-naam van de nieuwe ComboBox.
	 * @param cies Een CommissieVerzameling die de Commissies bevat waarvan de 
	 * BugCategorieen getoond moeten worden. Indien NULL worden alle Commissies 
	 * met een BugCategorie getoond.
	 * @param geselecteerden De BugCategorieen die geselecteerd zijn.
	 * @param enAlles of 'alle categorieen' ook in de combobox mag voorkomen
	 * @return Een HtmlSelectbox, of NULL indien er geen Commissies in cies 
	 * zitten met bijbehorende BugCategorieen.
	 */
	public static function maakCiesComboBox ($naam, $geselecteerden = NULL, $enAlles = false, CommissieVerzameling $cies = NULL)
	{
		if ($geselecteerden === NULL || $geselecteerden instanceof BugCategorie) {
			$deGeselecteerden = new BugCategorieVerzameling();
			if ($geselecteerden instanceof BugCategorie) {
				$deGeselecteerden->voegtoe($geselecteerden);
			}
		} else {
			$deGeselecteerden = $geselecteerden;
		}

		if (!$cies) {
			$cies = BugCategorieVerzameling::geefActieveBugCommissies();
		}

		$opts = array();
		if ($enAlles) $opts[''] = "&lt;alle categori&euml;en&gt;";
		foreach ($cies as $cie) {
			$categorieen = BugCategorieVerzameling::geefVanCommissie($cie);
			if ($categorieen->aantal() > 0) {
				$opts[CommissieView::waardeNaam($cie)] = self::namenArray($categorieen);
			}
		}

		if (empty($opts)) return false;
		return HtmlSelectbox::fromArray($naam, $opts, $deGeselecteerden->keys());
	}

	/**
	 *  Maak een array die de ID van een BugCategorie mapt naar diens 
	 * naam.
	 *
	 * @param categorieen Een BugCategorieVerzameling.
	 * @return Een associatieve array die van elke BugCategorie in categorieen 
	 * de ID mapt naar de (gesanitizede) naam.
	 */
	public static function namenArray (BugCategorieVerzameling $categorieen)
	{
		$namen = array();
		foreach ($categorieen as $categorie) {
			$namen[$categorie->geefID()] = BugCategorieView::waardeNaam($categorie);
		}
		return $namen;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
