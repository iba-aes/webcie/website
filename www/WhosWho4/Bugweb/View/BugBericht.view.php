<?
/**
 * $Id$
 */
abstract class BugBerichtView
	extends BugBerichtView_Generated
{
	/**
	 * De lengte waarbij commithashes worden afgekapt.
	 *
	 * Dit zou misschien omhooggeschroefd moeten worden, als de commitgeschiedenis
	 * te groot wordt. Maar dat zal waarschijnlijk nog een tijdje duren.
	 */
	const HASH_ABBREV_LENGTH = 7;

	/**
	 * De regex waarmee we verwijzingen naar bugs in berichten vinden.
	 */
	const BUG_REGEX = '/\#(\d+)/';

	/**
	 * De regex waarme we commando's om bugs te sluiten vinden.
	 */
	const BUG_CLOSE_REGEX =
		'/closes:\\s*(?:bug)?\\#?\\s?\\d+(?:,\\s*(?:bug)?\\#?\\s?\\d+)*/i';

	/**
	 * Maak een visuele representatie van een BugBericht.
	 *
	 * @param bugBericht Een BugBericht.
	 * @param vorige Het BugBericht dat voor bugBericht kwam. Wordt gebruikt om 
	 * de verschillen tussen de twee te berekenen en tonen.
	 * @param vorigeToewijzingPersonen Een PersoonVerzameling die de Personen 
	 * bevat die ten tijde van vorige aan de Bug waren toegewezen.
	 * @param huidigeToewijzingPersonen Een PersoonVerzameling die de Personen 
	 * bevat die ten tijde van bugBericht aan de Bug zijn toegewezen. Wordt 
	 * samen met vorigeToewijzingPersonen gebruikt om de verschillen te 
	 * berekenen en tonen.
	 * @return Een HtmlDiv die een visuele representatie van bugBericht bevat, 
	 * inclusief de verschillen met vorige en de verschillen in toewijzingen.
	 */
	static public function toon (BugBericht $bugBericht, BugBericht $vorige = NULL, PersoonVerzameling $vorigeToewijzingPersonen = NULL, PersoonVerzameling $huidigeToewijzingPersonen = NULL)
	{
		$melderDiv = new HtmlDiv(null, 'col-md-3');
		$melderDiv->add(new HtmlSpan(self::waardeMelder($bugBericht), 'melder'));
		$melderDiv->add(new HtmlBreak());
		$melderDiv->add(new HtmlSpan(self::waardeMoment($bugBericht), 'moment'));
		if ($bugBericht->magWijzigen() && !$bugBericht->getMelder()) {
			$melderDiv->add($form = new HtmlForm('get', $bugBericht->url() . '/VerifieerEmail'));
			$form->add(new HtmlDiv(array(
				HtmlInput::makeSubmitButton(_('Dit ben ik'), 'deze'),
				HtmlInput::makeSubmitButton(_('(overal)'), 'alle')
			), 'btn-group'));
		}

		$diff = new HtmlParagraph();
		if ($vorige instanceof BugBericht) {
			// Genereer de diff tussen dit BugBericht en de vorige
			$veranderingen = self::veranderingen($vorige, $bugBericht);
	
			if ($dTitel = $veranderingen['titel']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Titel') . ':')) . vsprintf(' %s &#8594; %s', $dTitel));
				$diff->add(new HtmlBreak());
			}
			if ($dCategorie = $veranderingen['categorie']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Categorie') . ':')) . vsprintf(' %s &#8594; %s', $dCategorie));
				$diff->add(new HtmlBreak());
			}
			if ($dStatus = $veranderingen['status']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Status') . ':')) . vsprintf(' %s &#8594; %s', $dStatus));
				$diff->add(new HtmlBreak());
			}
			if ($dPrioriteit = $veranderingen['prioriteit']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Prioriteit') . ':')) . vsprintf(' %s &#8594; %s', $dPrioriteit));
				$diff->add(new HtmlBreak());
			}
			if ($dNiveau = $veranderingen['niveau']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Niveau') . ':')) . vsprintf(' %s &#8594; %s', $dNiveau));
				$diff->add(new HtmlBreak());
			}
			if ($dLevel = $veranderingen['level']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Moeilijkheid') . ':')) . vsprintf(' %s &#8594; %s', $dLevel));
				$diff->add(new HtmlBreak());
			}
		}

		if ($bugBericht->getRevisie())
		{
			$revisieLink = self::bugBerichtLinks(self::waardeRevisie($bugBericht));
			$diff->add(new HtmlSpan(new HtmlStrong(_('Gemeld in revisie') . ':'))
				. ' ' . $revisieLink);
			$diff->add(new HtmlBreak());
		}

		if ($bugBericht->getCommit())
		{
			$commitLink = self::bugBerichtLinks(self::waardeCommit($bugBericht));
			$diff->add(new HtmlSpan(new HtmlStrong(_('Gemeld in commit') . ':'))
				. ' ' . $commitLink);
			$diff->add(new HtmlBreak());
		}

		if ($vorigeToewijzingPersonen instanceof PersoonVerzameling
			&& $huidigeToewijzingPersonen instanceof PersoonVerzameling
			&& ($vorigeToewijzingPersonen->aantal() > 0 || $huidigeToewijzingPersonen->aantal() > 0)) {
			$vorigeToewijzingenTekst = $vorigeToewijzingPersonen->aantal() > 0 ? PersoonVerzamelingView::kort($vorigeToewijzingPersonen) : _('Niemand');
			$huidigeToewijzingenTekst = $huidigeToewijzingPersonen->aantal() > 0 ? PersoonVerzamelingView::kort($huidigeToewijzingPersonen) : _('Niemand');
			$diff->add(new HtmlSpan(new HtmlStrong(_('Toegewezen aan') . ':')) . sprintf(' %s &#8594; %s', $vorigeToewijzingenTekst, $huidigeToewijzingenTekst));
			$diff->add(new HtmlBreak());
		}

		if ($diff->numHtmlChildren() > 0) {
			$melderDiv->add($diff);
		}

		$berichtDiv = new HtmlDiv(self::waardeBericht($bugBericht), 'col-md-9 bericht monospace');
		return new HtmlDiv(array($melderDiv, $berichtDiv), 'row');
	}

	public static function toonEersteBericht (Bug $bug)
	{
		$bugBericht = $bug->getEersteBericht();

		$berichtDiv = new HtmlDiv(NULL, 'row');
		$berichtDiv->add($melderDiv = new HtmlDiv(NULL, 'col-md-3'));
		$melderDiv->add(new HtmlSpan(self::waardeMelder($bugBericht), 'melder'));
		$melderDiv->add(new HtmlBreak());
		$melderDiv->add(new HtmlSpan(self::waardeMoment($bugBericht), 'moment'));

		$berichtDiv->add(new HtmlDiv(BugBerichtView::waardeBericht($bugBericht), 'col-md-9 eersteBericht bericht monospace'));
		return $berichtDiv;
	}

	/**
	 *  Maak een formulier om een nieuw BugBericht bij een Bug te plaatsen.
	 *
	 * @param bug Een Bug.
	 * @param titel De titel die in het formulier komt te staan.
	 * @param categorieID De ID van de BugCategorie die in het formulier komt te staan.
	 * @param prioriteit De prioriteit die in het formulier komt te staan.
	 * @param status De status die in het formulier komt te staan.
	 * @param bericht Het Bericht dat in het formulier komt te staan.
	 * @param toewijzingIDs Een array met IDs van Personen aan wie de Bug na 
	 * die bericht is toegewezen.
	 * @param niveau Het nieuwe zichtbaarheidsniveau van de bug
	 * @param level De moeilijkheidsgraad van de bug
	 * @param toon_fouten Indien True, toon een kolom met foutmeldingen.
	 * @return Een HtmlForm om een nieuw BugBericht bij een Bug te plaatsen. De 
	 * informatie in de parameters is hierin ingevuld.
	 */
	public static function nieuwBericht (Bug $bug, $titel, $categorieID, $prioriteit, $status, $bericht, $toewijzingIDs, $niveau, $level, $toon_fouten)
	{
		global $request;

		$titel = $titel ?: $bug->getTitel();
		$categorie = BugCategorie::geef($categorieID) ?: $bug->getCategorie();
		$prioriteit = $prioriteit ?: $bug->getPrioriteit();
		$status = $status ?: $bug->getStatus();
		$niveau = $niveau ?: $niveau->getNiveau();
		$toewijzingIDs = $toewijzingIDs ?: $bug->getToewijzingPersonen();

		$mogelijkeToewijzingen = PersoonVerzameling::geefMogelijkeBugToewijzingen($bug);

		$cies = BugCategorieVerzameling::geefActieveBugCommissies();
		$cieLeden = [];

		foreach($cies as $cie) {
			$cieLeden[$cie->geefID()] = $cie->leden();
			$cieLeden[$cie->geefID()]->sorteer('Naam');
		}

		$form = HtmlForm::named('NieuwBugBericht');
		$form->addClass('form-inline bugs');

		$catCieIds = [];
		foreach($cies as $cie) {
			foreach(BugCategorieVerzameling::geefVanCommissie($cie) as $cat) {
				$catCieIds[$cat->geefID()] = $cie->geefID();
			}
		}

		$form->add(HtmlScript::makeJavascript('var catCies = '.json_encode($catCieIds).';'));

		// Deze post-var is om te controleren of er nog nieuwe bugberichten zijn
		// toegevoegd aan de bug terwijl de user het bericht aan het schrijven was
		$now = new DateTimeLocale();
		$form->add(HtmlInput::makeHidden('nieuwBerichtTime', $now->format('Y-m-d H:i:s')));

		$form->add(HtmlInput::makeText('BugBericht[Titel]', $titel, 50));
		$form->add($box = BugCategorieVerzamelingView::maakCiesComboBox('BugBericht[Categorie]', $categorie, false));

		$box->setAttribute('data-bind', 'value: categorie');

		$form->add(new HtmlBreak());

		$form->add(new HtmlLabel('BugBericht[Status]', _('Status:')));
		$form->add(self::maakStatusForm('BugBericht[Status]', $status));
		$form->add(new HtmlLabel('BugBericht[Prioriteit]', _('Prioriteit:')));
		$form->add(self::maakPrioriteitForm('BugBericht[Prioriteit]', $prioriteit));
		$cie = $bug->getCategorie()->getCommissie();
		if($cie->hasLid(Persoon::getIngelogd()) || hasAuth('bestuur')) {
			$form->add(new HtmlLabel('BugBericht[Niveau]', _('Niveau:')));
			$form->add(self::maakNiveauForm('BugBericht[Niveau]', $niveau));
		}
		$form->add(new HtmlLabel('BugBericht[Level]', _('Moeilijkheid:')));
		$form->add(self::maakLevelForm('BugBericht[Level]', $level));
		$form->add(new HtmlBreak());

		$form->add($textarea = HtmlTextarea::withContent($bericht, 'BugBericht[Bericht]', 80, 10));
		$textarea->addClass('monospace');

		$form->add(new HtmlLabel('BugBericht[Toewijzingen][]', _('Toegewezen aan:'), 'toegewezen'));

		foreach($cies as $cie)
		{
			$cieL = PersoonVerzameling::getBugToewijzingen($bug);
			$cieL->sorteer('Naam');
			$cieL->union($cieLeden[$cie->geefID()]);

			$form->add($wrapper = new HtmlDiv($box = PersoonVerzamelingView::htmlSelectBox(NULL, $cieL, NULL, $toewijzingIDs, true)));
			$box->setCaption(NULL);
			$box->setAttribute('name', 'BugBericht[Toewijzingen][]');
			$box->setAttribute('data-bind', 'attr: { name: curCie() == '.$cie->geefID().' ? "BugBericht[Toewijzingen][]" : "null" }');
			$wrapper->setAttribute('data-bind', 'visible: curCie() == '.$cie->geefID());
		}

		$form->add(new HtmlBreak());
		$form->add(HtmlInput::makeSubmitButton(_('Voer in')));

		if($request->server->get('HTTP_REFERER', null) || tryPar('referer'))
		{
			$form->add(HtmlInput::makeHidden('referer', tryPar('referer', $request->server->get('HTTP_REFERER'))));
		}

		return $form;
	}

	/**
	 * Verstuur mails na het aanmaken van een BugBericht.
	 *
	 * De mails worden verstuurd naar:
	 * * De Commissie die bij de BugCategorie van de Bug hoort.
	 * * Indien anders, de Commissie die voor het invoeren van het nieuwe 
	 * BugBericht bij de Bug hoorde.
	 * * Iedere BugVolger van de Bug.
	 *
	 * @param obj Een BugBericht.
	 * @param isNieuweBug Indien True worden de teksten van de mails erop 
	 * aangepast dat het een nieuwe Bug betreft; anders wordt er vanuit gegaan 
	 * dat het om een nieuw bericht bij een bestaande Bug gaat.
	 */
	public static function verstuurMails (BugBericht $obj, $isNieuweBug)
	{
		$bug = $obj->getBug();
		$cie = $bug->getCategorie()->getCommissie();
		$cieMailTo = new CommissieVerzameling();
		$cieMailTo->voegtoe($cie);

		$volgers = $bug->getVolgerPersonen();

		if ($vorige = $obj->vorige()) {
			$veranderingen = self::veranderingen($vorige, $obj);
			// Indien de commissie veranderd wordt, mail beide commissies
			$oudeCie = $vorige->getCategorie()->getCommissie();
			if ($oudeCie->geefID() != $cie->geefID()) {
				$cieMailTo->voegtoe($oudeCie);
			}
		} else {
			$veranderingen = array('titel' => False, 'categorie' => False, 'status' => False, 'prioriteit' => False, 'toewijzing' => False, 'niveau' => False, 'level' => False);
		}

		$waardeBugID = BugView::waardeBugID($bug);
		// we kunnen hier niet BugBerichtView::waardeTitel() gebruiken, want dan hebben we allerlei HTML-troep
		$subject = sprintf('Bug #%s: %s', $waardeBugID, $obj->getTitel());
		if ($isNieuweBug) {
			$mailbody = "<p>" . _('Er is een bug aan het bugsysteem van A-Eskwadraat toegevoegd.') . "</p>";
		} else {
			if ($melder = $obj->getMelder()) {
				$mailbody = "<p>" . sprintf(_('Er is een bug in het A-Eskwadraat bugsysteem gewijzigd door %s.'), PersoonView::naam($melder)) . "</p>";
			} else {
				$mailbody = "<p>" . sprintf(_('Er is een bug in het A-Eskwadraat bugsysteem gewijzigd door %s (niet geverifieerd).'), $obj->getMelderEmail()) . "</p>";
			}
		}

        //		$mailbody .= "\n" . BugView::makeLink($bug);
        $mailbody .= BugView::makeLink($bug, _('Klik op deze link voor meer informatie. Je kunt ook gewoon op dit mailtje replyen!'))->makeHtml();

		if ($dTitel = $veranderingen['titel']) {
			$infoTitel = _('Bug') . vsprintf(" $waardeBugID: %s -> %s", $dTitel);
		} else {
			$infoTitel = _('Bug') . " $waardeBugID: " . BugBerichtView::waardeTitel($obj);
		}
        $infoMelder = BugView::melder($bug);
        $infoWanneer = "Om: " . BugBerichtView::waardeMoment($obj);
		if ($dCategorie = $veranderingen['categorie']) {
			$infoCategorie = _('Categorie') . vsprintf(": %s -> %s", $dCategorie);
		} else {
			$infoCategorie = _('Categorie') . ': ' . BugCategorieView::waardeNaam($obj->getCategorie());//BugBerichtView::waardeCategorie($obj);
		}
		if ($dStatus = $veranderingen['status']) {
			$infoStatus = _('Status') . vsprintf(': %s -> %s', $dStatus);
		} else {
			$infoStatus = _('Status') . ': ' . BugBerichtView::waardeStatus($obj);
		}
		if ($dPrioriteit = $veranderingen['prioriteit']) {
			$infoPrioriteit = _('Prioriteit') . vsprintf(': %s -> %s', $dPrioriteit);
		} else {
			$infoPrioriteit = _('Prioriteit') . ': ' . BugBerichtView::waardePrioriteit($obj);
		}
		if ($dToewijzing = $veranderingen['toewijzing']) {
			$infoToewijzingen = _('Toegewezen aan') . vsprintf(': %s -> %s', $dToewijzing);
		} else {
			$infoToewijzingen = _('Toegewezen aan') . ': ' . BugView::waardeToewijzingen($bug);
		}
		if ($dNiveau = $veranderingen['niveau']) {
			$infoNiveau = _('Niveau') . vsprintf(': %s -> %s', $dNiveau);
		} else {
			$infoNiveau = _('Niveau') . ': ' . BugView::waardeNiveau($bug);
		}
		if ($dLevel = $veranderingen['level']) {
			$infoLevel = _('Moeilijkheid') . vsprintf(': %s -> %s', $dLevel);
		} else {
			$infoLevel = _('Moeilijkheid') . ': ' . BugView::waardeLevel($bug);
		}

		$mailbody .= sprintf("
<p><strong>
<hr />
%s<br />
%s<br />
%s<br />
%s<br />
%s<br />
%s<br />
%s<br />
%s<br />
%s<br />
</strong>",
			$infoTitel,
			$infoMelder,
			$infoWanneer,
			$infoCategorie,
			$infoStatus,
			$infoPrioriteit,
			$infoToewijzingen,
			$infoNiveau,
			$infoLevel);

		// stop de berichten van nieuw naar oud in de mail
		$huidige = $obj;
		do {
			$mailbody .= "</p><hr /><p>";
			if ($melder = $huidige->getMelder()) {
				$mailbody .= PersoonView::naam($melder);
			} else {
				$mailbody .= $huidige->getMelderEmail();
			}

			$mailbody .= " (".BugBerichtView::waardeMoment($huidige)."):<br />";

			if($huidige->vorige())
			{
				$diff = BugBerichtView::veranderingen($huidige->vorige(), $huidige);
				foreach($diff as $key => $d) {
					if(is_null($d)) {
						continue;
					}

					$func = 'label'.ucfirst($key);
					$mailbody .= "<b>".BugView::{$func}()."</b>: ";
					$mailbody .= vsprintf("%s -> %s<br />", $d);
				}
			}

			$mailbody .= BugBerichtView::waardeBericht($huidige);

		} while ($huidige = $huidige->vorige());
		$mailbody .= "</p>";

		foreach($volgers as $v) {
			$moetmailen = true;
			foreach($cieMailTo as $c) {
				if($c->hasLid($v)){
					$moetmailen = false;
					break;
				}
			}
			if(!$moetmailen)
				continue;

			$mail = new Email();
			$mail->setTo($v);
			$mail->setFrom($bug);
			$mail->setSubject($subject);
			$mail->setBody($mailbody);

			if ($mail->valid()) {
				$mail->send();
			}
		}

		foreach($cieMailTo as $c) {
			$cieMail = new Email();
			$cieMail->setTo($c);
			$cieMail->setFrom($bug);
			$cieMail->setSubject($subject);
			$cieMail->setBody($mailbody);
			if($cieMail->valid()) {
				$cieMail->send();
			}
		}
	}

	/**
	 * Bereken de veranderingen tussen twee instanties van BugBericht.
	 *
	 * @param oud Het eerste BugBericht.
	 * @param nieuw Het tweede BugBericht.
	 * @return Een associatieve array die als keys de strings 'titel', 
	 * 'categorie', 'status' en 'prioriteit' heeft en als waarden NULL indien 
	 * de desbetreffende velden gelijk zijn voor de twee objecten, en een array 
	 * met de ge-escapede waarden van het veld van oud respectievelijk nieuw.
	 */
	public static function veranderingen (BugBericht $oud, BugBericht $nieuw)
	{
		$diff = array('titel' => NULL, 'categorie' => NULL, 'status' => NULL, 
			'prioriteit' => NULL, 'toewijzing' => NULL, 'niveau' => NULL, 'level' => NULL);

		if ($oud->getTitel() != $nieuw->getTitel()) {
			$diff['titel'] = array(self::waardeTitel($oud), self::waardeTitel($nieuw));
		}
		if ($oud->getCategorieBugCategorieID() != $nieuw->getCategorieBugCategorieID()) {
			$diff['categorie'] = array(self::waardeCategorie($oud), self::waardeCategorie($nieuw));
		}
		if ($oud->getStatus() != $nieuw->getStatus()) {
			$diff['status'] = array(self::waardeStatus($oud), self::waardeStatus($nieuw));
		}
		if ($oud->getPrioriteit() != $nieuw->getPrioriteit()) {
			$diff['prioriteit'] = array(self::waardePrioriteit($oud), self::waardePrioriteit($nieuw));
		}

		$bug = $oud->getBug();
		$toewijzingen = $bug->getToewijzingen();
		$berichten = $bug->getBerichten();

		$berichtTeksten = array();

		$laatste = NULL;
		$vorige = $berichten->first();
		$huidigeToewijzingPersonen = $vorige->getBeginToewijzingen();
		foreach ($berichten as $bericht)
		{
			$berichtID = $bericht->getBugBerichtID();

			$vorigeToewijzingPersonen = clone $huidigeToewijzingPersonen;
			$toewijzingenGewijzigd = False;
			foreach ($toewijzingen as $toewijzing)
			{
				if ($toewijzing->getBeginBugBerichtID() == $berichtID) {
					$huidigeToewijzingPersonen->voegtoe($toewijzing);
					$toewijzingenGewijzigd = True;
				} else if ($toewijzing->getEindBugBerichtID() == $berichtID) {
					$huidigeToewijzingPersonen->verwijder($toewijzing);
					$toewijzingenGewijzigd = True;
				}
			}
			if($bericht == $nieuw) {
				break;
			}

			$vorige = $bericht;
		}

		if ($toewijzingenGewijzigd) {
			$diff['toewijzing'] = array(BugToewijzingVerzamelingView::toString($vorigeToewijzingPersonen)
				, BugToewijzingVerzamelingView::toString($huidigeToewijzingPersonen));
		}
		if ($oud->getNiveau() != $nieuw->getNiveau()) {
			$diff['niveau'] = array(self::waardeNiveau($oud), self::waardeNiveau($nieuw));
		}
		if ($oud->getLevel() != $nieuw->getLevel()) {
			$diff['level'] = array(self::waardeLevel($oud), self::waardeLevel($nieuw));
		}

		return $diff;
	}

	protected static function bugBerichtLinks ($tekst)
	{
		// Regex voor externe URLs (syntax: http://blablalba OF https://blablabla)
		$tekst = preg_replace('/(^|\s)(https?:\/\/[A-Za-z0-9\-]+\.[A-Za-z0-9\.\-]+[A-Za-z0-9\-\.\_~\:\?\&\#\=\;\+\'\!\@\$\%\/\[\]]*)/',
				'${1}<a href="${2}">${2}</a>', $tekst);
		// Regex voor interne URLs (syntax: /blablabla/meermeukvanjouwlink)
		$tekst = preg_replace('/(^|\s)'
			. '(\/[A-Za-z0-9\-\.\_~\:\?\&\#\=\;\+\'\!\@\$\%\/\[\]]+)/',
				'${1}<a href="'.HTTPS_ROOT.'${2}">${2}</a>', $tekst);

		// Bugs
		$tekst = preg_replace_callback(self::BUG_REGEX,
			function($matches)
			{
				$b = Bug::geef((int)$matches[1]);
				return ($b instanceof Bug ? BugView::makeLink($b, $matches[0]) : $matches[0]); },
			$tekst);

		// TODO: Git commits, en evt. oude SVN revisies. Zie #7389

		// Lid-nrs
		$tekst = preg_replace('/(^|(?<=\s))\+(\d+)/', '<a href="'.HTTPS_ROOT.'/Leden/${2}">${0}</a>', $tekst);

		return $tekst;
	}

	public static function waardeMelder(BugBericht $obj)
	{
		if ($melder = $obj->getMelder()) {
			return PersoonView::makeLink($melder);
		}

		return new HtmlSpan(self::waardeMelderEmail($obj));
	}

	public static function waardeRevisie(BugBericht $obj)
	{
		$tekst = parent::waardeRevisie($obj);
		return "r$tekst";
	}

	public static function waardeCommit(BugBericht $bericht)
	{
		$hash = parent::waardeCommit($bericht);
		$hashAbbrev = "^" . substr($hash, 0, self::HASH_ABBREV_LENGTH);
		$url = sprintf(GIT_COMMIT_FORMAT, $hash);
		return new HtmlAnchor($url, $hashAbbrev);
	}

	public static function maakStatusForm ($naam, $status)
	{
		$statussen = array();
		foreach (Bug::enumsStatus() as $st)
		{
			$statussen[$st] = self::labelEnumStatus($st);
		}

		$box = HtmlSelectbox::fromArray($naam, $statussen, $status);
		$box->setSize(1);
		return $box;
	}
	public static function maakPrioriteitForm ($naam, $prioriteit)
	{
		$prios = array();
		foreach (Bug::enumsPrioriteit() as $prio)
		{
			$prios[$prio] = self::labelEnumPrioriteit($prio);
		}

		$box = HtmlSelectbox::fromArray($naam, $prios, $prioriteit);
		$box->setSize(1);
		return $box;
	}
	public static function maakNiveauForm ($naam, $niveau)
	{
		$niveaus = array();
		foreach (Bug::enumsNiveau() as $nivo)
		{
			$niveaus[$nivo] = self::labelEnumNiveau($nivo);
		}

		$box = HtmlSelectbox::fromArray($naam, $niveaus, $niveau);
		$box->setSize(1);
		return $box;
	}
	public static function maakLevelForm ($naam, $l)
	{
		$levels = array();
		foreach (Bug::enumsLevel() as $level)
		{
			$levels[$level] = self::labelEnumLevel($level);
		}

		$box = HtmlSelectbox::fromArray($naam, $levels, $l);
		$box->setSize(1);
		return $box;
	}

	public static function labelEnumStatus($value)
	{
		return BugView::labelEnumStatus($value);
	}
	public static function labelEnumPrioriteit($value)
	{
		return BugView::labelEnumPrioriteit($value);
	}
	public static function labelEnumNiveau($value)
	{
		return BugView::labelEnumNiveau($value);
	}
	public static function labelEnumLevel($value)
	{
		return BugView::labelEnumLevel($value);
	}

	public static function waardeCategorie(BugBericht $obj)
	{
		if ($categorie = $obj->getCategorie())
			return BugCategorieView::waardeNaam($obj->getCategorie());
		return NULL;
	}
	public static function waardeBericht(BugBericht $obj)
	{
		$bericht = htmlspecialchars($obj->getBericht());
		return self::bugBerichtLinks(nl2br($bericht));
	}
	public static function waardeMoment(BugBericht $obj)
	{
		if (($moment = $obj->getMoment()) instanceof DateTimeLocale)
			return $moment->strftime('%Y-%m-%d %H:%M');
		return NULL;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
