<?
abstract class BugVolgerVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugVolgerVerzamelingView.
	 *
	 * @param BugVolgerVerzameling $obj Het BugVolgerVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugVolgerVerzameling(BugVolgerVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
