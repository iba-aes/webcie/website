<?
abstract class BugVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugVerzamelingView.
	 *
	 * @param BugVerzameling $obj Het BugVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugVerzameling(BugVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
