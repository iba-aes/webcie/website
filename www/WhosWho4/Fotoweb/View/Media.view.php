<?
/**
 * $Id$
 */
abstract class MediaView
	extends MediaView_Generated
{
	/**
	 *  De grote functie die de foto-informatie-tabel terug geeft
	 *
	 * @param foto is de foto/video waarvoor we de foto-info-tabel willen
	 *
	 */
	public static function maakFotoDiv(Media $foto) {
		global $FW_LICENCES;

		$foto->updateViews();

		$div = new HtmlDiv();
		$div->add(self::maakTagsDiv($foto));
		$div->add(self::maakCollectieDiv($foto));
		$div->add(self::maakRating($foto));
		$div->add(self::maakLock($foto));
		$div->add(self::maakFotoInfoTable($foto, false));

		if ($foto->getSoort() === "FOTO") {
			// Alleen foto's kunnen EXIF en IPTC data hebben.
			// Zie E1522075218, E1522075091 en meer
			// Closes: bug #8024
			$div->add(self::maakExifDiv($foto));
		}

		$div->add(self::bekijkOrigineel($foto));

		if($foto->magWijzigen()) {
			if($foto->getSoort() != "FILM") {
				$div->add(self::maakRotateL($foto));
				$div->add(self::maakRotateR($foto));
			}
			$div->add(self::maakDelete($foto));
		}
		return $div;
	}

	static public function maakExifDiv(Media $foto)
	{
		$hideablediv = new HtmlDiv(
			$algemeen = new HtmlTable(),null,$foto->geefID() . '-exif-info');

		// Haal de exif- en iptc-data van de foto op
		$exif = $foto->geefExif();
		$iptc = $foto->geefIPTC();

		// EXIF
		if ($exif){
			if (isset($exif["COMPUTED"]["Height"]) && isset($exif["COMPUTED"]["Width"])){
				$exif_aspect = $exif["COMPUTED"]["Height"] / $exif["COMPUTED"]["Width"];

				if (round($exif_aspect, 2) == round(4/3, 2)){
					$exif_aspect = "4:3";
				} elseif (round($exif_aspect, 2) == round(3/4, 2)){
					$exif_aspect = "3:4";
				} elseif (round($exif_aspect, 2) == round(3/2, 2)){
					$exif_aspect = "3:2";
				} elseif (round($exif_aspect, 2) == round(2/3, 2)){
					$exif_aspect = "2:3";
				} else {
					$exif_aspect = null;
				}
			} else {
				$exif_aspect = null;
			}

			$exif_afmet = @$exif["COMPUTED"]["Height"] . " bij " . @$exif["COMPUTED"]["Width"] . " pixels" or "onbekend";
			$exif_diafragma = @$exif["COMPUTED"]["ApertureFNumber"] or "onbekend";
			$exif_sluitertijd = @$exif["ExposureTime"] or "onbekend";
			$exif_merk = @$exif["Make"] or "onbekend";
			$exif_model = @$exif["Model"] or "onbekend";
			$exif_datetime = date('Y-m-d H:i:s',$exif["FileDateTime"]);
			$exif_focal = @$exif["FocalLength"] or "";
			$exif_focal35 = @$exif["FocalLengthIn35mmFilm"] or "";
			$exif_iso = @$exif["ISOSpeedRatings"] or "onbekend";

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Afmetingen'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars("$exif_afmet " . 
					($exif_aspect != null
						? "($exif_aspect)"
						: ""))
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Diafragma'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars($exif_diafragma)
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Sluitertijd'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars(
					$exif_sluitertijd == ""
						? "onbekend"
						: "$exif_sluitertijd seconde(n)")
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Brandpuntsafstand'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars(
					($exif_focal == "" 
						? "onbekend" 
						: "$exif_focal mm")
					. ($exif_focal35 == ""
						? ""
						: " ($exif_focal35 mm equivalent 35mm film)"))
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Datum'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars($exif_datetime)
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Merk camera'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars($exif_merk)
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('Model camera'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars($exif_model)
				, 'tagvalue');

			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(_('ISO-waarde'),'tagtype');
			$algemeenrow->addData(
				htmlspecialchars($exif_iso)
				, 'tagvalue');

			// Kijken of er IPTC-data beschikbaar is. Alleen tonen
			// als er ingelogd is (lid||oud-lid) omdat er af en toe
			// namen in voorkomen
			if (is_array($iptc) && (hasAuth("lid") || hasAuth("oud-lid"))){
				$first = true;

				foreach ($iptc as $description => $values) {
					$description = htmlspecialchars($description);

					if (is_string($values)) {
						// Momenteel is dit het gebruikte format:
						$value = $values;
					} else if (is_array($values)) {
						$value = implode(", ", $values);
					} else {
						// Niet te snappen!
						$value = NULL;
					}
					if ($value == NULL) continue;
					$value = htmlspecialchars($value);

					if ($first) {
						$algemeen->add($algemeenrow = new HtmlTableRow());
						$algemeenrow->add($algemeendata = new HtmlTableDataCell());
						$algemeendata->setAttribute('colspan',2);
						// $algemeendata->add(new HtmlHR());
						$first = false;
					}

					$algemeen->add($algemeenrow = new HtmlTableRow());
					$algemeenrow->addData(
						htmlspecialchars($description)
						, 'tagtype');
					$algemeenrow->addData(
						htmlspecialchars($value)
						, 'tagvalue');
				}
			}
		} else {
			// geen EXIF beschikbaar
			$algemeen->add($algemeenrow = new HtmlTableRow());
			$algemeenrow->addData(
				_('Geen EXIF- of IPTC-informatie beschikbaar bij deze foto')
				, 'tagtype');
		}

		return $hideablediv;
	}

	static public function maakCollectieDiv(Media $foto)
	{
		// Haal de collecties op
		$collecties = $foto->getCollecties();

		$collectiediv = new HtmlDiv(null, null, $foto->geefID() . '-collectie-info');

		$collectiediv->add($algemeen = new HtmlTable());

		$odd = true;

		if($collecties->aantal() == 0) {
			$algemeen->add($row = new HtmlTableRow());
			$row->addData(new HtmlEmphasis(_("Geen collecties getagd")));
		}

		if($foto->getLock() == 1) {
			// Als de foto gelocket is geven we alleen de anchors weer
			foreach($collecties as $c) {
				if($odd) 
					$algemeen->add($row = new HtmlTableRow());
				$odd = !$odd;

				$row->addData(new HtmlAnchor($c->url(),CollectieView::waardeNaam($c)));
			}
		} else {
			// Als de foto niet gelockt is kunnen we ook collecties toevoegen
			// en weghalen
			foreach($collecties as $c) {
				if($odd)
					$algemeen->add($row = new HtmlTableRow());
				$odd = !$odd;

				$img = HtmlSpan::fa('times', 'Verwijderen', 'verwijder text-danger');
				$img->setCssStyle('color: red !important;');
				$anch = new HtmlAnchor('#', $img);
				$anch->addClass('clickDelCollectie');
				$anch->setAttribute('id', $c->getCollectieID());
				$row->addData(array(
					self::verwijderKnop('clickDelCollectie', $c->getCollectieID()),
					new HtmlAnchor($c->url(), CollectieView::waardeNaam($c))
				));
			}

			// Haal de collecties op
			$collarray = CollectieVerzameling::maakCollectieArray();

			$collectiediv->add($form = new HtmlForm());

			$form->add($select = HtmlSelectbox::fromArray('collecties'
					, $collarray
					, array()
					, 1));
			$select->setAttribute('id','CollectieSelect');
			$form->add($button = HtmlInput::makeButton('invoeren','Toevoegen'));
			$button->addClass('buttonCollectie');
		}

		return $collectiediv;
	}

	static public function maakTagsDiv(Media $foto)
	{
		$tagsdiv = new HtmlDiv(null, null, $foto->geefID() . '-tags-info');

		// Haal de getagde personen op
		$tagsp = $foto->getTagsPersonen();

		$tagsdiv->add($algemeen = new HtmlTable());

		$isodd = true;
		if($tagsp->aantal() == 0) {
			$algemeen->add($row = new HtmlTableRow());
			$row->addData(new HtmlEmphasis(_("Geen mensen getagd")));
		}
		// Als de foto gelockt is, mogen de tags niet meer aangepast worden
		if($foto->getLock() == 1) {
			foreach($tagsp as $t) {
				// De isodd is voor het maken van twee kolommen
				if($isodd) {
					$algemeen->add($row = new HtmlTableRow());
				}
				$isodd = !$isodd;
				$row->addData(PersoonView::makeLink($t->getPersoon()));
			}
		} else {
			// Als de foto niet gelockt is, kunnen tags dus nog aangepast worden
			foreach($tagsp as $t) {
				// De isodd is voor het maken van twee kolommen
				if($isodd) {
					$algemeen->add($row = new HtmlTableRow());
					$isodd = false;
				}
				else
					$isodd = true;

				// Als we met een filmpje te maken hebben kunnen we natuurlijk geen selectie maken
				$row->addData(array(
					self::verwijderKnop('clickDelTag', $t->getTagID()),
					PersoonView::makeLink($t->getPersoon())
				));
			}

			// Hier maken we de array met standaard keuzes aan
			$selectarray = $foto->getActiviteit()->deelnemers()->toPersoonVerzameling();

			$tagsdiv->add($sbdiv = new HtmlForm());
			$sbdiv->setAttribute('onsubmit', 'return false;');
			$sbdiv->addClass('form-inline');
			$sbdiv->setName("LidVerzameling");
			$sbdiv->add(View::defaultFormPersoonVerzameling($selectarray, "LidVerzameling", false));
			$sbdiv->add($button = HtmlInput::makeButton('submit', 'Voeg toe'));
			$button->addClass('buttonLidVerzameling');
		}

		// Maak het deel voor de cie-tags aan
		// Haal de cie-tags op
		$tagsc = $foto->getTagsCommissies();

		// Standaard staat het tags toevoegen van cies en groepjes uit,
		// anders vreet het teveel van de cache
		if(!tryPar('toevoegen') && !$foto->getLock()) {
			$tagsdiv->add($anch = new HtmlAnchor('#'
				, _("Commissies of mentorgroepen taggen")));
			$anch->setAttribute('id','clickCiesEnMentorGroepen');
		}

		// Als er tags zijn of als we tags willen toevoegen
		if($tagsc->aantal() != 0 || tryPar('toevoegen')) {

			if($tagsc->aantal() != 0) {
				$tagsdiv->add(new HtmlBreak());
			}
			$tagsdiv->add($algemeen = new HtmlTable());

			$odd = true;
			if($foto->getLock() == 1) {
				// Als de foto is gelockt geven we alleen de anchors weer
				foreach($tagsc as $t) {
					if($odd) {
						$algemeen->add($row = new HtmlTableRow());
					}
					$odd = !$odd;
					$row->addData(CommissieView::makeLink($t->getCommissie()));
				}
			} else {
				// Anders kunnen de tags ook weggehaald worden ...
				foreach($tagsc as $t) {
					if($odd)
						$algemeen->add($row = new HtmlTableRow());
					$odd = !$odd;

					$img = HtmlSpan::fa('times', 'Verwijderen', 'verwijder text-danger');
					$img->setCssStyle('color: red !important;');
					$anch = new HtmlAnchor('#', HtmlSpan::fa('remove', _('Verwijder')));
					$anch->addClass('clickDelTag');
					$anch->setAttribute('id',$t->getTagID());

					$row->addData(array(
						self::verwijderKnop('clickDelTag', $t->getTagID()),
						CommissieView::makeLink($t->getCommissie())
					));
				}
				// .. en als we tags willen toevegen kan dat ook
				if(tryPar('toevoegen')) {
					// Split de selct op huidige cies en alle cies
					$ciearray = array();
					foreach(CommissieVerzameling::allemaal() as $c) {
						$ciearray[$c->getCommissieID()] = CommissieView::waardeNaam($c);
					}

					$ciehuidig = array();
					foreach(CommissieVerzameling::huidige() as $c) {
						$ciehuidig[$c->getCommissieID()] = CommissieView::waardeNaam($c);
					}

					$tagsdiv->add($sbdiv = new HtmlForm());

					$sbdiv->add($select = new HtmlSelectbox('zoeken'));
					$select->setSize(1);
					$select->add(HtmlOptGroup::fromArray('Huidige cies',$ciehuidig));
					$select->add(HtmlOptGroup::fromArray('Alle cies',$ciearray));
					$select->setAttribute('id',"CieZoeken");

					$sbdiv->add($button = HtmlInput::makeButton('submit', 'Voeg toe'));
					$button->addClass('buttonCieToevoegen');
				}
			}
		}

		// Haal de groep-tags op
		$tagsm = $foto->getTagsMentorGroepen();

		// Maak het gedeelte waar de tags inkomen
		//$tagsdiv->add(new HtmlHeader(4, sprintf(_("Mentorgroepen op de foto (%s)"),$tagsm->aantal())));

		// Als er tags zijn of als we tags willen toevoegen
		if($tagsm->aantal() != 0 || tryPar('toevoegen')) {

			if($tagsm->aantal() != 0) {
				$tagsdiv->add(new HtmlBreak());
			}
			$tagsdiv->add($algemeen = new HtmlTable());

			$odd = true;
			if($foto->getLock() == 1) {
				// Als de foto gelockt is geven we alleen de anchors weer
				foreach($tagsm as $t) {
					if($odd) {
						$algemeen->add($row = new HtmlTableRow());
					}
					$odd = !$odd;

					$row->addData(IntroGroepView::maakLinkUitgebreid($t->getIntroGroep()));
				}
			} else {
				// Anders kunnen we ze ook weer weghalen
				foreach($tagsm as $t) {
					if($odd)
						$algemeen->add($row = new HtmlTableRow());
					$odd = !$odd;

					$img = HtmlSpan::fa('times', 'Verwijderen', 'verwijder text-danger');
					$img->setCssStyle('color: red !important;');
					$anch = new HtmlAnchor('#',$img);
					$anch->addClass('clickDelTag');
					$anch->setAttribute('id',$t->getTagID());

					$row->addData(array(
						self::verwijderKnop('clickDelTag', $t->getTagID()),
						IntroGroepView::maakLinkUitgebreid($t->getIntroGroep())
					));
				}
				// Of als we tags willen toevoegen
				if(trypar('toevoegen')) {
					// Haal de intro-jaren op en maak er een select van
					$jaren = IntroClusterVerzameling::jarenMetCluster();
					arsort($jaren);

					$nullarray = array(null => null);
					$jaren = $nullarray + $jaren;

					$tagsdiv->add($sbdiv = new HtmlForm());

					$sbdiv->add($select = HtmlSelectbox::fromArray('zoeken'
								, $jaren
								, array()
								, 1));
					$select->setAttribute('id',"MentorgroepJaar");

					// Maak de array van de mentorgroepjes van de jaren
					$mgarray = array();
					foreach(IntroGroepVerzameling::groepenVanJaar($jaren) as $m) {
						$mgarray[$m->getGroepID()] = IntroGroepView::waardeNaam($m);
					}

					$sbdiv->add($select = HtmlSelectbox::fromArray('zoeken'
							, $mgarray
							, array()
							, 1));
					$select->setAttribute('id',"MentorGroepZoeken");
					$sbdiv->add($button = HtmlInput::makeButton('submit', 'Voeg toe'));
					$button->addClass('buttonMentorGroepToevoegen');
				}
			}
		}

		return $tagsdiv;
	}

	static public function maakFotoInfoTable(Media $foto)
	{
		global $FW_LICENCES;

		$div = new HtmlDiv(null, null, $foto->geefID() . '-info-info');

		$editing = tryPar('infowijzig', false);

		// Als we de foto-info gaan wijzigen, maak dan het form ervoor aan
		// anders, geef alleen een tabel terug
		if($editing) {
			$form = new HtmlForm();
			$form->add($algemeen = new HtmlTable());
			$form->setAttribute('id','editFotoInfoForm');
			$div->add($form);
		} else {
			$algemeen = new HtmlTable();
			$div->add($algemeen);
		}
		$algemeen->setNoDefaultClasses();
		$algemeen->addClass('table');

		// De row voor de datum/tijd van de foto
		$algemeen->add($algemeenrow = new HtmlTableRow());
		$algemeenrow->add(new HtmlTableDataCell(_('Datum/tijd')
												, 'tagtype'));
		if($editing) {
			$algemeenrow->add($algemeendata = new HtmlTableDataCell(
				HtmlInput::makeDateTime('datetime'
					, $foto->getGemaakt()
					, 'tagvalue')));
		} else {
			$algemeenrow->add($algemeendata = new HtmlTableDataCell(
				$foto->getGemaakt()->format('Y-m-d H:i:s')
				, 'tagvalue'));
		}

		// De row voor de fotograaf van de foto
		$algemeen->add($algemeenrow = new HtmlTableRow());
		$algemeenrow->add($algemeendata = new HtmlTableDataCell(_('Fotograaf'),'tagtype'));

		$fotograaf = $foto->getFotograaf();

		if($editing) {
			// Als we aan het wijzigen zijn willen we een array van alle
			// deelnemers waar we dan uit kunnen selecteren
			$fotograafarray = $foto->getActiviteit()->deelnemersArray();
			$fotograafarray[0] = null;
			
			if($fotograaf)
				$fotograafarray[$fotograaf->getContactID()] = PersoonView::naam($fotograaf);

			// Maak de selectbox
			$algemeenrow->add($algemeendata = new HtmlTableDataCell(
				HtmlSelectbox::fromArray('fotograaf'
					, $fotograafarray
					, array($fotograaf ? $fotograaf->getContactID() : 0)
					, 1)
				, 'tagvalue'));
		} else {
			// Anders geven we alleen de fotograaf weer
			$fotograafdata = $fotograaf ? new HtmlAnchor($fotograaf->fotograafUrl(), PersoonView::naam($fotograaf)) : "";
			$algemeenrow->add($algemeendata = new HtmlTableDataCell($fotograafdata, 'tagvalue'));
		}


		//De row voor de activiteit van de foto (als die bestaat)
		$algemeen->add($algemeenrow = new HtmlTableRow());
		$algemeenrow->add($algemeendata = new HtmlTableDataCell(_('Activiteit')
																,'tagtype'));
		if($foto->getActiviteit()->getActiviteitID()) {
			$algemeenrow->addData(ActiviteitView::makeLink($foto->getActiviteit()));
		}

		// De row voor de licentie
		$algemeen->add($algemeenrow = new HtmlTableRow());
		$algemeenrow->add($algemeendata = new HtmlTableDataCell(_('Licentie')
																,'tagtype'));
		$algemeenrow->add($algemeendata = new HtmlTableDataCell(
			new HtmlAnchor($FW_LICENCES[strtolower($foto->getLicence())]['url']
				, $FW_LICENCES[strtolower($foto->getLicence())]['name'])
				, 'tagvalue')
			);

		// De row voor de omschrijving van de foto
		$algemeen->add($algemeenrow = new HtmlTableRow());
		$algemeenrow->add($algemeendata = new HtmlTableDataCell(_('Omschrijving')
																,'tagtype'));
		if($editing) {
			// Als we aan het editen zijn geven we een input-veld...
			$algemeenrow->add($algemeendata = new HtmlTableDataCell(
				HtmlInput::makeText('omschrijving'
					, $foto->getOmschrijving()
				)
				, 'tagvalue')
			);
		} else {
			// ... en anders niet
			$algemeenrow->add($algemeendata = new HtmlTableDataCell(
				MediaView::waardeOmschrijving($foto),'tagvalue'));
		}

		if(!$foto->getLock()) {
			if($editing) {
				$div->add($button = HtmlInput::makeButton('submit', _("Ik zeg: doen!")));
				$button->addClass('infoSubmit');
			} else {
				//$div->add($button = HtmlInput::makeButton('wijzig', _("Wijzig")));
				//$button->addClass('infoWijzig');
			}
		}

		return $div;
	}

	static public function maakLock($foto) {
		$div = new HtmlDiv(null, null, $foto->geefID() . '-lock-info');

		$locked = $foto->getLock();

		if($locked) {
			$div->add(_("Deze foto is gelocked!"));
			if(hasAuth('vicie')) {
				$div->add($btn = HtmlInput::makeButton('buttonLock', _("Unlock")));
				$btn->addClass('buttonLock');
			}
		} else {
			$div->add(_("Deze foto is niet gelocked!"));
			if(hasAuth('vicie')) {
				$div->add($btn = HtmlInput::makeButton('buttonLock', _("Lock")));
				$btn->addClass('buttonLock');
			}
		}

		$div->add($lockValue = new HtmlDiv(($locked) ? 1 : 0, null, 'lockValue'));
		$lockValue->setCssStyle('display: none;');

		return $div;
	}

	/**
	 *  Geeft die leuke vijf sterretjes terug voor de rating
	 *
	 * @param foto De foto waar we de 5 sterretjes bij willen
	 */
	static public function maakRating($foto) {
		$rating = $foto->getRating();
		$rating = round($rating/10,1);
		$votes = $foto->getRatingCount();

		$wholestar = HtmlSpan::fa('star', _('Leeg'), 'text-warning');
		$halfstar = HtmlSpan::fa('star-half-o', _('Leeg'), 'text-warning');
		$emptystar = HtmlSpan::fa('star-o', _('Leeg'), 'text-warning');

		$div = new HtmlDiv($row = new HtmlDiv(null, 'row starRow'), null, $foto->geefID() . '-rating-info');
		/*$table = new HtmlTable(null,'fotorating');
		$table->add($row = new HtmlTableRow());*/
		$counter = 1;
		if(Persoon::getIngelogd()) {
			while($counter <= 5) {
				if($rating >= 2 * $counter)
					$row->add(new HtmlDiv(
						$anch = new HtmlAnchor('#',$wholestar), 'col-xs-2 clickStar'));
				elseif($rating >= (2 * $counter) -1)
					$row->add(new HtmlDiv(
						$anch = new HtmlAnchor('#',$halfstar), 'col-xs-2 clickStar'));
				else
					$row->add(new HtmlDiv(
						$anch = new HtmlAnchor('#',$emptystar), 'col-xs-2 clickStar'));
				$anch->setAttribute('id',$counter);
				$anch->addClass('star');
				$counter++;
			}
		} else {
			while($counter <= 5) {
				if($rating >= 2 * $counter)
					$row->add(new HtmlDiv($wholestar, 'col-xs-2'));
				elseif($rating >= (2 * $counter) -1)
					$row->add(new HtmlDiv($halfstar, 'col-xs-2'));
				else
					$row->add(new HtmlDiv($emptystar, 'col-xs-2'));
				$counter++;
			}
		}
		$row->add($ratingdiv = new HtmlDiv("(" . round($rating,1) . "/10)", 'col-xs-2', 'ratingValue'));
		$ratingdiv->setCssStyle('display: none;');

		$votes = $foto->getRatingCount();
		$div->add($row = new HtmlDiv());
		$row->add(sprintf(_("%s stem(men), %s keer bekeken")
			, $votes
			, $foto->getViews()));
		$row->setCssStyle('display: block; padding: 8px 12px;');
		return $div;
	}

	static public function bekijkOrigineel(Media $foto) {
		return new HtmlDiv(null, null, $foto->geefID() . "-bekijkorigineel-info");
	}

	static public function maakRotateL(Media $foto) {
		$div = new HtmlDiv(null, null, $foto->geefID() . "-rotateL-info");

		return $div;
	}

	static public function maakRotateR(Media $foto) {
		$div = new HtmlDiv(null, null, $foto->geefID() . "-rotateR-info");

		return $div;
	}

	static public function maakDelete(Media $foto) {
		$div = new HtmlDiv(null, null, $foto->geefID() . "-del-info");

		return $div;
	}

	/**
	 * Produceer een formulier waar de gebruiker een bestand kan uploaden of map in /scratch kan selecteren om naar fotoweb te uploaden
	 */
	static public function uploadFileSelectForm() {
		$form = HtmlForm::named('fotowebUpload');
		$form->setAttribute('enctype', 'multipart/form-data');

		// vertel de gebruiker hoe het werkt
		$form->add(new HtmlDiv(_("Je mag losse bestanden direct uploaden naar
			Fotoweb. Als je meerdere foto's tegelijk wilt uploaden, moeten ze
			in een map op het A–Eskwadraatsysteem aanwezig zijn. Deze map moet 
			in /scratch staan om leesbaar te zijn voor de website."
		), 'bs-callout bs-callout-info'));

		$show_error = false;
		// de uploadopties
		$form->add($div = new HtmlDiv);
		$div->add(self::wijzigTR(new Media(), 'locatie', $show_error));
		$div->add(self::wijzigTR(new Media(), 'directUpload', $show_error));
		$div->add(HtmlInput::makeFormSubmitButton('Check it out!'));

		// ga door naar de checkstap
		$form->add(HtmlInput::makeHidden("stap", "2"));

		return $form;
	}

	/**
	 * Vraag de uploadende gebruiker om bevestiging dat ze onze licentievoorwaarden accepteren
	 * @return Een HtmlObject om in je form te ->add()'en, inclusief "ik ga akkoord"-kop
	 */
	static public function creativeCommons()
	{
		$div = new HtmlDiv(null, 'well');

		$div->add($row = new HtmlDiv(null, 'row'));
		$row->add(new HtmlDiv(HtmlSpan::fa('cc', _('Creative Commons')), 'col-xs-1'));
		$row->add(new HtmlDiv(new HtmlEmphasis(_(
			"Met het drukken op de knop &quot;Uploaden&quot; gaat je ermee akkoord dat de foto's en/of video's worden opgeslagen en gepubliceerd onder <strong>een geamendeerde versie van de &quot;Creative Commons, attribution-noncommercial-share alike 3.0&quot;</strong>-licentie. Deze licentie houdt kortweg in dat de bestanden door A–Eskwadraat gebruikt, verspreid en/of bewerkt mogen worden, mits voor <strong>niet-commerci&euml;le doeleinden</strong>. Ook anderen mogen de bestanden gebruiken, mits er melding gemaakt wordt van de naam van de auteur. Deze licentie stelt A–Eskwadraat in staat om de foto's en/of video's in de toekomst bijvoorbeeld bij voorlichtingsactiviteiten ofvoor de ledenadministratie te gebruiken. ") .
			'<a rel="license" href="/www/fotoweb-licentie.html">Klik hier voor meer informatie (en juridische details) over de genoemde licentie en het amendement daarop</a>. ' .
			'Mocht je de foto\'s en/of video\'s expliciet onder een andere licentie willen publiceren, <a href="mailto:www@a-eskwadraat.nl">neem dan contact op met de WebCie</a> om de mogelijkheden hiertoe te verkennen.' .
			' Let op! Als je niet de rechthebbende bent van de mediabestanden die je uploadt, dien je dus de rechthebbende te vragen of die akkoord gaat met de voorwaarden van de bovengenoemde licentie.'), 'col-xs-11'));

		return $div;
	}

	static public function makeUploadForm($act = null, $cie = null)
	{
		$div = new HtmlDiv();

		$fotografen = array(null => null);
		$vicie = Commissie::cieByLogin('vicie');
		foreach($vicie->leden() as $lid)
			$fotografen[$lid->geefID()] = PersoonView::naam($lid);

		if($act)
		{
			$fotografen += $act->deelnemersArray();
		}
		else if($cie)
		{
			$leden = $cie->leden();
			foreach($leden as $lid)
				$fotografen[$lid->geefID()] = PersoonView::naam($lid);
		}

		$fotografen[Persoon::getIngelogd()->geefID()] = PersoonView::naam(Persoon::getIngelogd());

		$div->add(self::makeFormEnumRow('fotograaf', $fotografen, array(), _('Fotograaf')));

		$div->add(self::makeFormEnumRow('collectie', array(null => null) + CollectieVerzameling::maakCollectieArray(), array(), _('Collectie')));

		return $div;
	}

	static public function labelLocatie()
	{
		return _('Locatie');
	}
	static public function opmerkingLocatie()
	{
		return _('Wat is de locatie van de bestanden die je
			online wilt zetten? (Bijvoorbeeld: /scratch/Lustrumfotos)');
	}
	static public function formLocatie()
	{
		return HtmlInput::makeText('locatie',tryPar('locatie',''),40);
	}
	static public function labelDirectUpload()
	{
		return _('Bestand uploaden');
	}
	static public function opmerkingDirectUpload()
	{
		return '';
	}
	static public function formDirectUpload()
	{
		return HtmlInput::makeFile('directUpload', 1);
	}

	static private function verwijderKnop($class, $id)
	{
		$img = HtmlSpan::fa('times', _('Verwijderen'), 'verwijder text-danger');
		$img->setCssStyle('color: red !important;');
		return new HtmlAnchor('#',$img, NULL, $class, $id);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
