<?
abstract class TagView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TagView.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTag(Tag $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld tagID.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tagID labelt.
	 */
	public static function labelTagID(Tag $obj)
	{
		return 'TagID';
	}
	/**
	 * @brief Geef de waarde van het veld tagID.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tagID van het object obj
	 * representeert.
	 */
	public static function waardeTagID(Tag $obj)
	{
		return static::defaultWaardeInt($obj, 'TagID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tagID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tagID representeert.
	 */
	public static function opmerkingTagID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld media.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld media labelt.
	 */
	public static function labelMedia(Tag $obj)
	{
		return 'Media';
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld media van het object obj
	 * representeert.
	 */
	public static function waardeMedia(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMedia())
			return NULL;
		return MediaView::defaultWaardeMedia($obj->getMedia());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld media.
	 *
	 * @see genericFormmedia
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld media staat en kan worden
	 * bewerkt. Indien media read-only is betreft het een statisch html-element.
	 */
	public static function formMedia(Tag $obj, $include_id = false)
	{
		return MediaView::defaultForm($obj->getMedia());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld media. In
	 * tegenstelling tot formmedia moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmedia
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld media staat en kan worden
	 * bewerkt. Indien media read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMedia($name, $waarde=NULL)
	{
		return MediaView::genericDefaultForm('Media');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld media
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld media representeert.
	 */
	public static function opmerkingMedia()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Tag $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(Tag $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Persoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(Tag $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(Tag $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld introgroep.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld introgroep labelt.
	 */
	public static function labelIntrogroep(Tag $obj)
	{
		return 'Introgroep';
	}
	/**
	 * @brief Geef de waarde van het veld introgroep.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld introgroep van het object obj
	 * representeert.
	 */
	public static function waardeIntrogroep(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getIntrogroep())
			return NULL;
		return IntroGroepView::defaultWaardeIntroGroep($obj->getIntrogroep());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld introgroep.
	 *
	 * @see genericFormintrogroep
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld introgroep staat en kan
	 * worden bewerkt. Indien introgroep read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formIntrogroep(Tag $obj, $include_id = false)
	{
		return IntroGroepView::defaultForm($obj->getIntrogroep());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld introgroep. In
	 * tegenstelling tot formintrogroep moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formintrogroep
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld introgroep staat en kan
	 * worden bewerkt. Indien introgroep read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormIntrogroep($name, $waarde=NULL)
	{
		return IntroGroepView::genericDefaultForm('Introgroep');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * introgroep bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld introgroep representeert.
	 */
	public static function opmerkingIntrogroep()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld collectie.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld collectie labelt.
	 */
	public static function labelCollectie(Tag $obj)
	{
		return 'Collectie';
	}
	/**
	 * @brief Geef de waarde van het veld collectie.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld collectie van het object obj
	 * representeert.
	 */
	public static function waardeCollectie(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCollectie())
			return NULL;
		return CollectieView::defaultWaardeCollectie($obj->getCollectie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld collectie.
	 *
	 * @see genericFormcollectie
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld collectie staat en kan
	 * worden bewerkt. Indien collectie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCollectie(Tag $obj, $include_id = false)
	{
		return CollectieView::defaultForm($obj->getCollectie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld collectie. In
	 * tegenstelling tot formcollectie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcollectie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld collectie staat en kan
	 * worden bewerkt. Indien collectie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCollectie($name, $waarde=NULL)
	{
		return CollectieView::genericDefaultForm('Collectie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * collectie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld collectie representeert.
	 */
	public static function opmerkingCollectie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tagger.
	 *
	 * @param Tag $obj Het Tag-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tagger labelt.
	 */
	public static function labelTagger(Tag $obj)
	{
		return 'Tagger';
	}
	/**
	 * @brief Geef de waarde van het veld tagger.
	 *
	 * @param Tag $obj Het Tag-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tagger van het object obj
	 * representeert.
	 */
	public static function waardeTagger(Tag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTagger())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getTagger());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tagger.
	 *
	 * @see genericFormtagger
	 *
	 * @param Tag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tagger staat en kan worden
	 * bewerkt. Indien tagger read-only is betreft het een statisch html-element.
	 */
	public static function formTagger(Tag $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getTagger());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tagger. In
	 * tegenstelling tot formtagger moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtagger
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tagger staat en kan worden
	 * bewerkt. Indien tagger read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTagger($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Tagger');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tagger
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tagger representeert.
	 */
	public static function opmerkingTagger()
	{
		return NULL;
	}
}
