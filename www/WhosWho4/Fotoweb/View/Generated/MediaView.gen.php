<?
abstract class MediaView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MediaView.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMedia(Media $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mediaID.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mediaID labelt.
	 */
	public static function labelMediaID(Media $obj)
	{
		return 'MediaID';
	}
	/**
	 * @brief Geef de waarde van het veld mediaID.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mediaID van het object obj
	 * representeert.
	 */
	public static function waardeMediaID(Media $obj)
	{
		return static::defaultWaardeInt($obj, 'MediaID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld mediaID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mediaID representeert.
	 */
	public static function opmerkingMediaID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld activiteit.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld activiteit labelt.
	 */
	public static function labelActiviteit(Media $obj)
	{
		return 'Activiteit';
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteit van het object obj
	 * representeert.
	 */
	public static function waardeActiviteit(Media $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getActiviteit())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getActiviteit());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld activiteit.
	 *
	 * @see genericFormactiviteit
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld activiteit staat en kan
	 * worden bewerkt. Indien activiteit read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formActiviteit(Media $obj, $include_id = false)
	{
		return static::waardeActiviteit($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld activiteit. In
	 * tegenstelling tot formactiviteit moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formactiviteit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld activiteit staat en kan
	 * worden bewerkt. Indien activiteit read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormActiviteit($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteit representeert.
	 */
	public static function opmerkingActiviteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uploader.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uploader labelt.
	 */
	public static function labelUploader(Media $obj)
	{
		return 'Uploader';
	}
	/**
	 * @brief Geef de waarde van het veld uploader.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uploader van het object obj
	 * representeert.
	 */
	public static function waardeUploader(Media $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getUploader())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getUploader());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uploader.
	 *
	 * @see genericFormuploader
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uploader staat en kan
	 * worden bewerkt. Indien uploader read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUploader(Media $obj, $include_id = false)
	{
		return static::waardeUploader($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uploader. In
	 * tegenstelling tot formuploader moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuploader
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uploader staat en kan
	 * worden bewerkt. Indien uploader read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUploader($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uploader bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uploader representeert.
	 */
	public static function opmerkingUploader()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld fotograaf.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld fotograaf labelt.
	 */
	public static function labelFotograaf(Media $obj)
	{
		return 'Fotograaf';
	}
	/**
	 * @brief Geef de waarde van het veld fotograaf.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld fotograaf van het object obj
	 * representeert.
	 */
	public static function waardeFotograaf(Media $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getFotograaf())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getFotograaf());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld fotograaf.
	 *
	 * @see genericFormfotograaf
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld fotograaf staat en kan
	 * worden bewerkt. Indien fotograaf read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formFotograaf(Media $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getFotograaf());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld fotograaf. In
	 * tegenstelling tot formfotograaf moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formfotograaf
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld fotograaf staat en kan
	 * worden bewerkt. Indien fotograaf read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormFotograaf($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Fotograaf');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * fotograaf bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld fotograaf representeert.
	 */
	public static function opmerkingFotograaf()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Media $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(Media::enumsSoort() as $id)
			$soorten[$id] = MediaView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Media $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Media $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', Media::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld gemaakt.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld gemaakt labelt.
	 */
	public static function labelGemaakt(Media $obj)
	{
		return 'Gemaakt';
	}
	/**
	 * @brief Geef de waarde van het veld gemaakt.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld gemaakt van het object obj
	 * representeert.
	 */
	public static function waardeGemaakt(Media $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Gemaakt');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld gemaakt.
	 *
	 * @see genericFormgemaakt
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gemaakt staat en kan
	 * worden bewerkt. Indien gemaakt read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGemaakt(Media $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Gemaakt', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld gemaakt. In
	 * tegenstelling tot formgemaakt moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formgemaakt
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gemaakt staat en kan
	 * worden bewerkt. Indien gemaakt read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGemaakt($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Gemaakt');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld gemaakt
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld gemaakt representeert.
	 */
	public static function opmerkingGemaakt()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld licence.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld licence labelt.
	 */
	public static function labelLicence(Media $obj)
	{
		return 'Licence';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld licence.
	 *
	 * @param string $value Een enum-waarde van het veld licence.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumLicence($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld licence horen.
	 *
	 * @see labelenumLicence
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld licence
	 * representeren.
	 */
	public static function labelenumLicenceArray()
	{
		$soorten = array();
		foreach(Media::enumsLicence() as $id)
			$soorten[$id] = MediaView::labelenumLicence($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld licence.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld licence van het object obj
	 * representeert.
	 */
	public static function waardeLicence(Media $obj)
	{
		return static::defaultWaardeEnum($obj, 'Licence');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld licence.
	 *
	 * @see genericFormlicence
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld licence staat en kan
	 * worden bewerkt. Indien licence read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLicence(Media $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Licence', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld licence. In
	 * tegenstelling tot formlicence moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlicence
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld licence staat en kan
	 * worden bewerkt. Indien licence read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLicence($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Licence', Media::enumslicence());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld licence
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld licence representeert.
	 */
	public static function opmerkingLicence()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld premium.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld premium labelt.
	 */
	public static function labelPremium(Media $obj)
	{
		return 'Premium';
	}
	/**
	 * @brief Geef de waarde van het veld premium.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld premium van het object obj
	 * representeert.
	 */
	public static function waardePremium(Media $obj)
	{
		return static::defaultWaardeBool($obj, 'Premium');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld premium.
	 *
	 * @see genericFormpremium
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld premium staat en kan
	 * worden bewerkt. Indien premium read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPremium(Media $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Premium', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld premium. In
	 * tegenstelling tot formpremium moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpremium
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld premium staat en kan
	 * worden bewerkt. Indien premium read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPremium($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Premium');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld premium
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld premium representeert.
	 */
	public static function opmerkingPremium()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld views.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld views labelt.
	 */
	public static function labelViews(Media $obj)
	{
		return 'Views';
	}
	/**
	 * @brief Geef de waarde van het veld views.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld views van het object obj
	 * representeert.
	 */
	public static function waardeViews(Media $obj)
	{
		return static::defaultWaardeInt($obj, 'Views');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld views.
	 *
	 * @see genericFormviews
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld views staat en kan worden
	 * bewerkt. Indien views read-only is betreft het een statisch html-element.
	 */
	public static function formViews(Media $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Views', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld views. In
	 * tegenstelling tot formviews moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formviews
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld views staat en kan worden
	 * bewerkt. Indien views read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormViews($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Views');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld views
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld views representeert.
	 */
	public static function opmerkingViews()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lock.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lock labelt.
	 */
	public static function labelLock(Media $obj)
	{
		return 'Lock';
	}
	/**
	 * @brief Geef de waarde van het veld lock.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lock van het object obj
	 * representeert.
	 */
	public static function waardeLock(Media $obj)
	{
		return static::defaultWaardeBool($obj, 'Lock');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lock.
	 *
	 * @see genericFormlock
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lock staat en kan worden
	 * bewerkt. Indien lock read-only is betreft het een statisch html-element.
	 */
	public static function formLock(Media $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Lock', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lock. In tegenstelling
	 * tot formlock moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formlock
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lock staat en kan worden
	 * bewerkt. Indien lock read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLock($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Lock');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lock
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lock representeert.
	 */
	public static function opmerkingLock()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rating.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rating labelt.
	 */
	public static function labelRating(Media $obj)
	{
		return 'Rating';
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rating van het object obj
	 * representeert.
	 */
	public static function waardeRating(Media $obj)
	{
		return static::defaultWaardeInt($obj, 'Rating');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rating.
	 *
	 * @see genericFormrating
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is betreft het een statisch html-element.
	 */
	public static function formRating(Media $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Rating', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rating. In
	 * tegenstelling tot formrating moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrating
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormRating($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Rating');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld rating
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rating representeert.
	 */
	public static function opmerkingRating()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Media $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Media $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Media $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld breedte.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld breedte labelt.
	 */
	public static function labelBreedte(Media $obj)
	{
		return 'Breedte';
	}
	/**
	 * @brief Geef de waarde van het veld breedte.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld breedte van het object obj
	 * representeert.
	 */
	public static function waardeBreedte(Media $obj)
	{
		return static::defaultWaardeInt($obj, 'Breedte');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld breedte.
	 *
	 * @see genericFormbreedte
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld breedte staat en kan
	 * worden bewerkt. Indien breedte read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBreedte(Media $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Breedte', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld breedte. In
	 * tegenstelling tot formbreedte moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbreedte
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld breedte staat en kan
	 * worden bewerkt. Indien breedte read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBreedte($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Breedte');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld breedte
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld breedte representeert.
	 */
	public static function opmerkingBreedte()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld hoogte.
	 *
	 * @param Media $obj Het Media-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld hoogte labelt.
	 */
	public static function labelHoogte(Media $obj)
	{
		return 'Hoogte';
	}
	/**
	 * @brief Geef de waarde van het veld hoogte.
	 *
	 * @param Media $obj Het Media-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld hoogte van het object obj
	 * representeert.
	 */
	public static function waardeHoogte(Media $obj)
	{
		return static::defaultWaardeInt($obj, 'Hoogte');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld hoogte.
	 *
	 * @see genericFormhoogte
	 *
	 * @param Media $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld hoogte staat en kan worden
	 * bewerkt. Indien hoogte read-only is betreft het een statisch html-element.
	 */
	public static function formHoogte(Media $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Hoogte', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld hoogte. In
	 * tegenstelling tot formhoogte moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formhoogte
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld hoogte staat en kan worden
	 * bewerkt. Indien hoogte read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormHoogte($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Hoogte');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld hoogte
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld hoogte representeert.
	 */
	public static function opmerkingHoogte()
	{
		return NULL;
	}
}
