<?
abstract class CollectieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CollectieView.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCollectie(Collectie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld collectieID.
	 *
	 * @param Collectie $obj Het Collectie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld collectieID labelt.
	 */
	public static function labelCollectieID(Collectie $obj)
	{
		return 'CollectieID';
	}
	/**
	 * @brief Geef de waarde van het veld collectieID.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld collectieID van het object
	 * obj representeert.
	 */
	public static function waardeCollectieID(Collectie $obj)
	{
		return static::defaultWaardeInt($obj, 'CollectieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * collectieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld collectieID representeert.
	 */
	public static function opmerkingCollectieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld parentCollectie.
	 *
	 * @param Collectie $obj Het Collectie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld parentCollectie labelt.
	 */
	public static function labelParentCollectie(Collectie $obj)
	{
		return 'ParentCollectie';
	}
	/**
	 * @brief Geef de waarde van het veld parentCollectie.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld parentCollectie van het
	 * object obj representeert.
	 */
	public static function waardeParentCollectie(Collectie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getParentCollectie())
			return NULL;
		return CollectieView::defaultWaardeCollectie($obj->getParentCollectie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld parentCollectie.
	 *
	 * @see genericFormparentCollectie
	 *
	 * @param Collectie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld parentCollectie staat en
	 * kan worden bewerkt. Indien parentCollectie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formParentCollectie(Collectie $obj, $include_id = false)
	{
		return CollectieView::defaultForm($obj->getParentCollectie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld parentCollectie. In
	 * tegenstelling tot formparentCollectie moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formparentCollectie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld parentCollectie staat en
	 * kan worden bewerkt. Indien parentCollectie read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormParentCollectie($name, $waarde=NULL)
	{
		return CollectieView::genericDefaultForm('ParentCollectie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * parentCollectie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld parentCollectie representeert.
	 */
	public static function opmerkingParentCollectie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Collectie $obj Het Collectie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Collectie $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Collectie $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Collectie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Collectie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Collectie $obj Het Collectie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Collectie $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Collectie $obj)
	{
		return static::defaultWaardeString($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Collectie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Collectie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld actief.
	 *
	 * @param Collectie $obj Het Collectie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld actief labelt.
	 */
	public static function labelActief(Collectie $obj)
	{
		return 'Actief';
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @param Collectie $obj Het Collectie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld actief van het object obj
	 * representeert.
	 */
	public static function waardeActief(Collectie $obj)
	{
		return static::defaultWaardeBool($obj, 'Actief');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld actief.
	 *
	 * @see genericFormactief
	 *
	 * @param Collectie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is betreft het een statisch html-element.
	 */
	public static function formActief(Collectie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Actief', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld actief. In
	 * tegenstelling tot formactief moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formactief
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormActief($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Actief');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld actief
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld actief representeert.
	 */
	public static function opmerkingActief()
	{
		return NULL;
	}
}
