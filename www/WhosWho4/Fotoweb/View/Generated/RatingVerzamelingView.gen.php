<?
abstract class RatingVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in RatingVerzamelingView.
	 *
	 * @param RatingVerzameling $obj Het RatingVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeRatingVerzameling(RatingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
