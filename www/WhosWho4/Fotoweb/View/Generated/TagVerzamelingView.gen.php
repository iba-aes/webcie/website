<?
abstract class TagVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TagVerzamelingView.
	 *
	 * @param TagVerzameling $obj Het TagVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTagVerzameling(TagVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
