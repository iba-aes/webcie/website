<?
abstract class CommissieFotoVerzamelingView
	extends CommissieFotoVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in CommissieFotoVerzamelingView.
	 *
	 * @param obj Het CommissieFotoVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieFotoVerzameling(CommissieFotoVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
