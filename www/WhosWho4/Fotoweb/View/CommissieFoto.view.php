<?
abstract class CommissieFotoView
	extends CommissieFotoView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in CommissieFotoView.
	 *
	 * @param obj Het CommissieFoto-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieFoto(CommissieFoto $obj)
	{
		return self::defaultWaarde($obj);
	}

}
