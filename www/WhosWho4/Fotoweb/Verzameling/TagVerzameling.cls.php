<?
/**
 * $Id$
 */
class TagVerzameling
	extends TagVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TagVerzameling_Generated
	}

	/**
	 *  Geeft de meest getagtde leden
	 *
	 * @param aantal Het aantal leden dat we willen
	 * @param jaren Bool of we wel of niet alleen van de laatste 2 jaren willen
	 */
	public static function getTopGetagde($aantal = 15, $jaren = null) {
		global $WSW4DB;

		if($jaren) {
			$ids = $WSW4DB->q("TABLE SELECT `Tag`.`persoon_contactID`"
							. ",count(`Tag`.`persoon_contactID`) AS counted "
							. "FROM `Tag` "
							. "LEFT JOIN `Media` ON `Media`.`mediaID` = `Tag`.`media_mediaID` "
							. "WHERE `Media`.`gemaakt` > DATE_SUB(NOW(),INTERVAL %i YEAR) "
							. "GROUP BY `Tag`.`persoon_contactID` "
							. "ORDER BY `counted` DESC "
							. "LIMIT %i"
							, $jaren, $aantal);
		}
		else
			$ids = $WSW4DB->q("TABLE SELECT `persoon_contactID`"
							. ",count(`persoon_contactID`) AS counted "
							. "FROM `Tag` "
							. "GROUP BY `persoon_contactID` "
							. "ORDER BY `counted` DESC "
							. "LIMIT %i"
							, $aantal);

		return $ids;
	}

	/**
	 *  Geeft de meest taggende leden
	 *
	 * @param aantal Het aantal leden dat we willen
	 * @param jaren Bool of we wel of niet alleen van de laatste 2 jaren willen
	 */
	public static function getTopTaggers($aantal = 15, $jaren = null) {
		global $WSW4DB;

		if($jaren) {
			$ids = $WSW4DB->q("TABLE SELECT `tagger_contactID`"
							. ",count(`tagger_contactID`) AS counted "
							. "FROM `Tag` "
							. "WHERE `gewijzigdWanneer` > DATE_SUB(NOW(),INTERVAL %i YEAR) "
							. "AND `collectie_collectieID` IS NULL "
							. "GROUP BY `tagger_contactID` "
							. "ORDER BY `counted` DESC "
							. "LIMIT %i"
							, $jaren, $aantal);
		}
		else
			$ids = $WSW4DB->q("TABLE SELECT `tagger_contactID`"
							. ",count(`tagger_contactID`) AS counted "
							. "FROM `Tag` "
							. "WHERE `collectie_collectieID` IS NULL "
							. "GROUP BY `tagger_contactID` "
							. "ORDER BY `counted` DESC "
							. "LIMIT %i"
							, $aantal);

		return $ids;
	}

	/**
	 *  Geeft alle getagde commissies terug
	 */
	public static function geefAlleGetagdeCommissie() {
		global $WSW4DB;

		$ids = $WSW4DB->q("TABLE SELECT `tagID`,`commissie_commissieID` "
						. "FROM `Tag` "
						. "WHERE `commissie_commissieID` IS NOT NULL");

		return $ids;
	}

	/**
	 *  Geeft alle getagde mentorgroepen terug
	 */
	public static function geefAlleGetagdeMentorgroepen() {
		global $WSW4DB;

		$ids = $WSW4DB->q("TABLE SELECT `tagID`,`introgroep_groepID` "
						. "FROM `Tag` "
						. "WHERE `introgroep_groepID` IS NOT NULL");

		return $ids;
	}

	/**
	 *  Geeft alle getagde personen terug
	 */
	public static function geefAlleGetagdePersonen() {
		global $WSW4DB;

		$ids = $WSW4DB->q("TABLE SELECT `tagID`,`persoon_contactID` "
						. "FROM `Tag` "
						. "WHERE `persoon_contactID` IS NOT NULL");

		return $ids;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
