<?
abstract class RatingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de RatingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze RatingVerzameling een MediaVerzameling.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze RatingVerzameling.
	 */
	public function toMediaVerzameling()
	{
		if($this->aantal() == 0)
			return new MediaVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMediaMediaID()
			                      );
		}
		$this->positie = $origPositie;
		return MediaVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze RatingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze RatingVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een RatingVerzameling van Media.
	 *
	 * @return RatingVerzameling
	 * Een RatingVerzameling die elementen bevat die bij de Media hoort.
	 */
	static public function fromMedia($media)
	{
		if(!isset($media))
			return new RatingVerzameling();

		return RatingQuery::table()
			->whereProp('Media', $media)
			->verzamel();
	}
	/**
	 * @brief Maak een RatingVerzameling van Persoon.
	 *
	 * @return RatingVerzameling
	 * Een RatingVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new RatingVerzameling();

		return RatingQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
