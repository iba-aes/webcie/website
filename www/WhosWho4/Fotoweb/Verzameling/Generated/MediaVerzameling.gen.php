<?
abstract class MediaVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de MediaVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze MediaVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze MediaVerzameling.
	 */
	public function toActiviteitVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getActiviteitActiviteitID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getActiviteitActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze MediaVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze MediaVerzameling.
	 */
	public function toUploaderVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getUploaderContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getUploaderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze MediaVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze MediaVerzameling.
	 */
	public function toFotograafVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getFotograafContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getFotograafContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een MediaVerzameling van Activiteit.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die bij de Activiteit hoort.
	 */
	static public function fromActiviteit($activiteit)
	{
		if(!isset($activiteit))
			return new MediaVerzameling();

		return MediaQuery::table()
			->whereProp('Activiteit', $activiteit)
			->verzamel();
	}
	/**
	 * @brief Maak een MediaVerzameling van Uploader.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die bij de Uploader hoort.
	 */
	static public function fromUploader($uploader)
	{
		if(!isset($uploader))
			return new MediaVerzameling();

		return MediaQuery::table()
			->whereProp('Uploader', $uploader)
			->verzamel();
	}
	/**
	 * @brief Maak een MediaVerzameling van Fotograaf.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die bij de Fotograaf hoort.
	 */
	static public function fromFotograaf($fotograaf)
	{
		if(!isset($fotograaf))
			return new MediaVerzameling();

		return MediaQuery::table()
			->whereProp('Fotograaf', $fotograaf)
			->verzamel();
	}
}
