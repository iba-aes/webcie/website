<?
/**
 * $Id$
 */
class MediaVerzameling
	extends MediaVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // MediaVerzameling_Generated
	}

	/**
	 *  Telt hoeveel fotos er zijn
	 */
	static public function telAlle()
	{
		return MediaQuery::table()->count();
	}

	/**
	 *  Functie die een array geeft met alle foto-id's behorende aan een hele rits aan mogelijke selectie-opties
	 *
	 * @param type Het soort om fotos bij te geven als string
	 * @param object Het object om de fotos bij te geven (leeg bij notlocked en untagged)
	 *
	 * @return Een array met per foto het id, de hoogte en breedte, gemaakt en de soort.
	 */
	static public function geefFotos($type, $object = null)
	{
		$query = MediaQuery::table();

		switch(strtolower($type))
		{
		case 'activiteit':
			$query->whereProp('Activiteit', $object);
			break;
		case 'contact':
		case 'persoon':
		case 'lid':
			$query->join('Tag')
				->whereProp('Persoon', $object);
			break;
		case 'commissie':
			$query->join('Tag')
				->whereProp('Commissie', $object);
			break;
		case 'fotograaf':
			$query->whereProp('Fotograaf', $object);
			break;
		case 'collectie':
			$query->join('Tag')
				->whereProp('Collectie', $object);
			break;
		case 'mentorgroep':
			$query->join('Tag')
				->whereProp('IntroGroep', $object);
			break;
		case 'notlocked':
			$query->whereProp('lock', false);
			break;
		case 'untagged':
			$query->join('Tag')
				->whereNull('Tag.media_mediaID')
				->whereProp('lock', false);
			break;
		default:
			throw new BadMethodCallException(_('Geen geldig type '.$type));
		}

		$ids = $query->setFetchType('TABLE')
			->select('mediaID', 'gemaakt', 'soort', 'breedte', 'hoogte')
			->orderByDesc('gemaakt')
			->limit(20000)
			->get();

		$serialized = array_map('serialize', $ids);
		$unique = array_unique($serialized);
		$ids = array_map('unserialize', $unique);

		//We willen hier geen objecten terug geven, bij te grote hoeveelheiden
		//foto's kan de website die hoeveelheid gecachete object-data niet aan.
		return $ids;
	}

	static public function geefSelectieKartPersonen()
	{
		$kart = Kart::geef();
		$kartpersonen = $kart->getPersonen();

		$ids = MediaQuery::table()
			->join('Tag')
			->whereInProp('Tag.persoon', $kartpersonen)
			->orderByDesc('gemaakt')
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();

		//We willen hier geen objecten terug geven, bij te grote hoeveelheiden foto's kan de website die hoeveelheid gecachete data niet aan.
		return array_unique($ids);
	}

	/**
	 *  Geeft de top x aantal fotografen
	 *
	 * @param aantal De hoeveelheid x die we willen hebben
	 * @param jaren Over hoeveel van de afgelopen jaren je het wilt hebben
	 */
	static public function getTopFotografen($aantal = 15, $jaren = null)
	{
		global $WSW4DB;

		if($jaren)
		{
			$datum = new DateTimeLocale();
			$datum->sub(new DateInterval("P{$jaren}Y"));

			$ids = MediaQuery::table()
				->setFetchType('TABLE')
				->select('fotograaf_contactID', ['counted' => 'COUNT(fotograaf_contactID)'])
				->whereProp('gemaakt', '>', $datum)
				->groupBy('fotograaf_contactID')
				->orderByDesc('counted')
				->limit($aantal)
				->get();
		}
		else
		{
			$ids = MediaQuery::table()
				->setFetchType('TABLE')
				->select('fotograaf_contactID', ['counted' => 'COUNT(fotograaf_contactID)'])
				->groupBy('fotograaf_contactID')
				->orderByDesc('counted')
				->limit($aantal)
				->get();
		}

		return $ids;
	}

	/**
	 *  Geeft alle fotograaf-ids met hoeveelheid foto's gesorteerd terug
	 */
	static public function getAllFotografen()
	{
		return MediaQuery::table()
			->setFetchType('TABLE')
			->select('fotograaf_contactID', ['counted' => 'COUNT(fotograaf_contactID)'])
			->groupBy('fotograaf_contactID')
			->orderByDesc('counted')
			->get();
	}

	/**
	 *  Geeft de hoeveelheid untagged foto's
	 */
	static public function getTotaalAantalUntagged()
	{
		return MediaQuery::table()
			->setFetchType('MAYBEVALUE')
			->select('COUNT(mediaID)')
			->join('Tag')
			->whereNull('Tag.media_mediaID')
			->whereProp('lock', false)
			->get();
	}

	/**
	 *  Geeft de hoeveelheid notlocked foto's
	 */
	static public function getTotaalAantalNotlocked()
	{
		return MediaQuery::table()
			->whereProp('lock', false)
			->count();
	}

	/**
	 *  Geeft alle video-ids terug
	 */
	static public function geefVideos()
	{
		return MediaQuery::table()
			->setFetchType('COLUMN')
			->select('mediaID')
			->whereProp('soort', 'FILM')
			->get();
	}

	/**
	 *  Geeft alle foto-(niet-video)-ids terug
	 */
	static public function geefNietVideos()
	{
		return MediaQuery::table()
			->setFetchType('COLUMN')
			->select('mediaID')
			->whereProp('soort', 'FOTO')
			->get();
	}

	/**
	 *  Geeft alle media-ids die in de db staan terug
	 */
	static public function geefMedias()
	{
		return MediaQuery::table()
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();
	}

	/**
	 *  Geeft de toprated foto-ids bij een act
	 *
	 * @param act De activiteit waarvan we de foto's willen
	 * @param aantal Het aantal foto's wat we willen
	 */
	static public function getTopratedByAct($act, $aantal = 3)
	{
		return MediaQuery::table()
			->whereProp('Activiteit', $act)
			->orderByAsc('rating')
			->limit($aantal)
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();
	}

	/**
	 *  Geeft de toprated foto-ids bij een cie
	 *
	 * @param cie De cie waar we de foto's van willen
	 * @param aantal Het aantal foto's wat we willen
	 */
	static public function getTopratedByCie($cie, $aantal = 3)
	{
		return MediaQuery::table()
			->join('Tag')
			->whereProp('Commissie', $cie)
			->orderByAsc('rating')
			->limit($aantal)
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();
	}

	/**
	 *  Geeft de toprated foto-ids bij een mentorgroepje
	 *
	 * @param groep De groep waar we de foto's van willen
	 * @param aantal Het aantal foto's wat we willen
	 */
	static public function getTopratedByMentorgroep($groep, $aantal = 3)
	{
		return MediaQuery::table()
			->join('Tag')
			->whereProp('Introgroep', $groep)
			->orderByAsc('rating')
			->limit($aantal)
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();
	}

	/**
	 *  Geeft de toprated foto-ids bij een persoon
	 *
	 * @param persoon De persoon waar we de toprated van willen
	 * @param aantal Het aantal foto's wat we willen
	 */
	static public function getTopratedByPersoon($persoon, $aantal = 3)
	{
		return MediaQuery::table()
			->join('Tag')
			->whereProp('Persoon', $persoon)
			->orderByAsc('rating')
			->limit($aantal)
			->setFetchType('COLUMN')
			->select('mediaID')
			->get();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
