<?
class CommissieFoto
	extends CommissieFoto_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	public function __construct($commissie, $media)
	{
		parent::__construct($commissie, $media); // CommissieFoto_Generated
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}

	public function magBekijken()
	{
		return hasAuth('ingelogd');
	}

	public function magVerwijderen()
	{
		return $this->getCommissie()->magWijzigen();
	}

	static public function zoekHuidig(Commissie $cie)
	{
		return CommissieFotoQuery::Table()
			->whereProp('Commissie', $cie)
			->whereProp('status', 'HUIDIG')
			->geef();
	}
}
