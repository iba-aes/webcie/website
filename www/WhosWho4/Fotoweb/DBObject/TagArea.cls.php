<?
/**
 * $Id$
 */
class TagArea
	extends TagArea_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a)
	{
		parent::__construct($a); // TagArea_Generated
	}

	public function magVerwijderen()
	{
		return hasAuth('ingelogd');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
