<?
class ContactPersoonQuery
	extends ContactPersoonQuery_Generated
{
	public static function metConstraints(Organisatie $org = null, $functie = null, $status = 'huidig', $persoon = null, $type = "")
	{
		$q = ContactPersoonQuery::table()->orderByAsc('type');
		if ($functie) {
			$q->whereProp('functie', $functie);
		}
		if ($org) {
			$q->whereProp('Organisatie', $org);
		}
		if ($persoon) {
			$q->whereProp('Persoon', $persoon);
		}
		if ($type) {
			$q->whereProp('type', $type);
		}

		$status = strtolower($status);
		if ($status == 'oud') {
			$q->whereProp('einddatum', '<', 'NOW()');
		} else if ($status != 'alle') {
			$q->where(function($q) {
				$q->whereNull('einddatum')->orWhereProp('einddatum', '>', 'NOW()');
			})->where(function($q) {
				$q->whereNull('begindatum')->orWhereProp('begindatum', '<', 'NOW()');
			});
		}
		return $q;
	}
}
