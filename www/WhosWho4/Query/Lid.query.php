<?
class LidQuery
	extends LidQuery_Generated
{
	static public function filterMagBekijken()
	{
		$return = parent::filterMagBekijken();

		$where = function($q) {

			if(hasAuth('intro'))
				$q->orWhereProp('Lid.LidToestand', 'BIJNALID');

			$q->orWhere(function($qa) {
				if(hasAuth('lid'))
					$qa->whereProp('Lid.Opzoekbaar', 'A')
					  ->orWhere(function ($qu) {
						  $qu->whereProp('Lid.Opzoekbaar', 'J')
							 ->whereInString('lidToestand', array('LID', 'BAL', 'LIDVANVERDIENSTE', 'ERELID'));
					  });
				else if(hasAuth('oud-lid'))
					$qa->whereProp('Lid.Opzoekbaar', 'A');
				else
					$qa->whereProp('Lid.Opzoekbaar', 'B');
			});
		};

		if(isset($return['wheres']) && is_array($return['wheres']))
			$return['wheres'][] = $where;
		else if(isset($return['wheres']))
			$return['wheres'] = array($return['wheres'], $where);
		else
			$return['wheres'] = $where;

		return $return;
	}

	/**
	 * Voeg een where toe aan de query die kijkt of de lidtoestand een huidig lid is.
	 * (Huidig lid: lidtoestand 'LID', 'BAL', 'LIDVANVERDIENSTE', 'ERELID'),
	 *
	 * @return $this
	 */
	public function whereHuidigLid()
	{
		$this->where(function($q) {
			/** @var LidQuery $q */
			$q->whereProp('lidToestand', 'LID')
				->orWhereProp('lidToestand', 'BAL')
				->orWhereProp('lidToestand', 'LIDVANVERDIENSTE')
				->orWhereProp('lidToestand', 'ERELID');
		});

		return $this;
	}

	/**
	 * @brief Geef een query die alle bijnaleden selecteert.
	 */
	static public function bijnaleden()
	{
		return static::table()->whereProp('Lid.LidToestand', 'BIJNALID');
	}
}
