<?

/***
 * $Id$
 * 
 * Klasse die een mail (bijv. in MIME formaat) kan parsen
 */
class MailParser
{
	private $_contents;
	private $_mimePart = null;

	/** 
		 Constructor, verwacht een array van lines
	**/
	public function __construct($contentsarr)
	{
		$this->_contents = $contentsarr;

		// Als dit een multipart/MIME message is, dan retourneert onderstaande functie een HtmlMimeBlock-object
		$this->_mimePart = MailMimePart::parseMailData($contentsarr);
	}

	public function isMultipartMIME(){
		return ($this->_mimePart instanceof MailMimePart);
	}
	
	/**
		 Retourneert een HTML-versie van deze mailing, als deze beschikbaar is.

		@param baseurl_mimeblocks Basis van de URL waarop MIME blocks van deze
		mail beschikbaar zijn, bijv. /Service/MailingWeb/101/Sent/MIME
	**/
	public function getHtml($baseurl_mimeblocks = null){
		if ($this->isMultipartMIME()){
			$parts = $this->_mimePart->getParts(true);
			foreach($parts as $part){
				if ($part->getContentType() == "text/html"){
					// Dit is het gezochte part! Even alle referenties naar
					// MIME parts te grazen nemen... Dan kunnen we alle interne
					// referenties naar plaatjes in attachments tenminste
					// resolven en tonen

					if ($baseurl_mimeblocks){
						$partbody = $part->getBody();
						preg_match_all('/src="cid:(.*?)"/', implode("\n", $partbody), $srcs);
						if (is_array($srcs) && sizeof($srcs) >= 1){
							foreach ($srcs[0] as $src){
								// $src contains: 'src="cid:bladiebla"'
								$cid = substr($src, 9, strlen($src) - 10);
								$hash = md5($cid);

								$link = $baseurl_mimeblocks . "/$hash";
								$partbody = str_replace($src, "src=\"$link\"", $partbody);
							}
						} // else: geen matches op 'src="cid:bladiebla"'
					} // else: geen base URL voor MIME blocks beschikbaar. Zie functie-omschrijving van deze functie
					
					return $partbody;
				}
			}
		}

		// Reguliere mail, geen multipart/MIME
		return false;
	}

	public function getPlaintext(){
		if (strpos($this->getHeaderValue("Content-Type"), "text/plain") === 0){
			// Reguliere plaintext mail, zonder MIME parts
			return $this->getBody();
		} elseif ($this->isMultipartMIME()){
			$parts = $this->_mimePart->getParts(true);
			foreach($parts as $part){
				if ($part->getContentType() == "text/plain") return $part->getBody();
			}
		}
		return false;
	}

	/**
		 Retourneert data van een MIME block op basis van de hash van de
		ID van de content ID van dat block

		Deze functie is vooral handig voor gebruik i.c.m. URLs: een hash van
		een content ID is gegarandeert URL safe
	**/
	public function getMIMEPartByContentIDHash($hash){
		if ($this->isMultipartMIME()){
			$parts = $this->_mimePart->getParts(true);
			foreach($parts as $part){
				$contentid = $part->getHeaderValue("Content-ID");
				if ($contentid){
					// Content-ID header van MIME block gevonden
					$contentid = str_replace("<", "", $contentid);
					$contentid = str_replace(">", "", $contentid);

					$currHash = md5($contentid);
					if ($currHash === $hash) return $part;
				}
			}
		}

		return false;
	}

	public function containsHeader($header){
		return in_array($header, $this->getHeaders());
	}

	/**
		 Retourneert een array met headers
	**/
	public function getHeaders(){
		return $this->getContents("headers");
	}

	public function hasHtml(){
		return $this->getHtml() !== false;
	}

	public function hasPlainText(){
		return $this->getPlainText() !== false;
	}

	public function getHeaderValue($header){
		$headers = $this->getHeaders();
		foreach ($headers as $currHeader){
			if (stripos($currHeader . ":", $header) === 0) return trim(substr($currHeader, strpos($currHeader, ":") + 1));
		}
		return false;
	}

	public function getBody(){
		return $this->getContents("body");
	}

	public function getContents($part = "all"){
		$res = array();
		$atBody = false;
		foreach ($this->_contents as $line){
			if ($line === "" && $atBody === false){
				$atBody = true;
				if ($part === "headers") break;
				if ($part === "body") continue;
			}

			if ($part === "body" && !$atBody) continue;
			$res[] = $line;
		}

		return $res;
	}

	public function getMIMEPart(){
		return $this->_mimePart;
	}

	public static function parseFile($filename){
		if (!file_exists($filename)) return false;

		return new MailParser(file($filename, FILE_IGNORE_NEW_LINES));
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
