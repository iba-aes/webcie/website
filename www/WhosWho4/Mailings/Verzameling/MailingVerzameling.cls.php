<?

/***
 * $Id$
 * 
 * Representeert een lijst van Mailing-objecten
 */
class MailingVerzameling
	extends MailingVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	/**
	 * Retourneert een verzameling met alle onverzonden mailings uit de
	 * database. Sortering vindt plaats op moment waarop de mailing staat
	 * ingepland, gevolgd door mailingID.
	 */
	static public function onverzondenMailings()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingID`'
						.' FROM `Mailing`'
						.' WHERE `momentVerwerkt` IS NULL'
						.' ORDER BY `momentIngepland`, `mailingID`'
						);
		return MailingVerzameling::verzamel($ids);
	}

	static public function teVerzendenMailings()
	{
		global $WSW4DB;

		// Welke mailings zijn nog niet verwerkt, maar wel staan ingepland
		// later dan een dag geleden en uiterlijk nu?
		$ids = $WSW4DB->q('COLUMN SELECT `mailingID`'
						.' FROM `Mailing`'
						.' WHERE `momentVerwerkt` IS NULL'
						.'   AND `momentIngepland` > ADDDATE(NOW(), -1)'
						.'   AND `momentIngepland` < NOW()'
						.'   AND `isDefinitief` = 1'
						);
		return MailingVerzameling::verzamel($ids);
	}

	/**
	 * Retourneert een verzameling met alle (of een aantal) verzonden mailings
	 * uit de database. Sortering vindt plaats op verzenddatum op omgekeerde
	 * volgorde (jongste mailing eerst).
	 */
	static public function verzondenMailings($maxaantal = null)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingID`'
						.' FROM `Mailing`'
						.' WHERE `momentVerwerkt` IS NOT NULL'
						.' ORDER BY `momentVerwerkt` DESC, `mailingID` DESC'
						. (is_numeric($maxaantal) ? ' LIMIT %i' : ' %_')
						, $maxaantal
						);
		return MailingVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
