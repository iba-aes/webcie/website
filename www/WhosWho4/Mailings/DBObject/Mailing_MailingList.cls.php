<?

/***
 * $Id$
 * 
 * Koppeling Mailings-Mailinglists
 */
class Mailing_MailingList
	extends Mailing_MailingList_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	public function setMailingID($mailingID){
		$this->mailingID = $mailingID;
	}

	public function setMailingListID($mailingListID){
		$this->mailingListID = $mailingListID;
	}

    public function magVerwijderen()
    {
        return hasAuth('mailings');
    }
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
