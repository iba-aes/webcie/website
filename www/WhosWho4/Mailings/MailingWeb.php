<?php
abstract class MailingWeb_Controller
{
	/**
	 * Geeft de home van mailingweb
	 */
	static public function home()
	{
		MailingVerzamelingView::home();
	}

	/**
	 * Geeft de pagina met alle mailings
	 */
	static public function mailings()
	{
		$page = Page::getInstance()->start('Mailings');
		$page->add(MailingVerzamelingView::makeMailingsOverzicht());
		$page->end();
	}

	/**
	 * Maakt een pagina voor een nieuwe mailing
	 */
	static public function nieuweMailing()
	{
		if(!hasAuth('mailings')) {
			spaceHttp(403);
		}

		$mailing = new Mailing();
		$show_error = false;

		if(Token::processNamedForm() == 'MailingToevoegen') {
			MailingView::processNieuwForm($mailing);
			if($mailing->valid()) {
				$mailing->opslaan();

				// Sla de mailinglists op;
				$lists = tryPar('Lists', array());
				foreach($lists as $list) {
					$list = MailingList::geef($list);
					$listObj = new Mailing_MailingList($mailing, $list);
					$listObj->opslaan();
					}

				// Sla de plaintext op
				$mailing->setMailBody(tryPar('Mailing[MailBody]'));

				Page::redirectMelding($mailing->url(), _("Mailing aangemaakt"));
			} else {
				Page::addMelding(_("Er waren fouten"), 'fout');
				$show_error = true;
			}
		}

		MailingView::wijzigForm($mailing, true, $show_error);
	}

	static public function mailingEntry ($args)
	{
		$mailing = Mailing::geef($args[0]);
		if (!$mailing) return false;

		return array( 'name' => $mailing->geefID()
					, 'displayName' => $mailing->geefID()
					, 'access' => true); 
	}

	/**
	 * Geeft de pagina met de informatie over de mailing
	 */
	static public function mailingDetails ()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken()) {
			spaceHttp(403);
		}

		$page = Page::getInstance();
		if($mailing->magWijzigen()) {
			$page->setEditUrl("Edit");
		}
		if($mailing->magVerwijderen()) {
			$page->setDeleteUrl("Delete");
		}

		MailingView::makeMailingDetailsInterface($mailing, false); // niet editable
	}

	/**
	 * Geeft de pagina voor het wijzigen van een mailing
	 */
	static public function wijzigMailing()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magWijzigen()) {
			spaceHttp(403);
		}

		$show_error = false;

		if(Token::processNamedForm() == 'MailingWijzigen') {
			MailingView::processNieuwForm($mailing);
			if($mailing->valid()) {
				$mailing->opslaan();

				$mailingLists = $mailing->getMailingLists();
				$oldLists = array();
				foreach($mailingLists as $list) {
					$oldLists[$list->geefID()] = $list->geefID();
				}

				// Sla de mailinglists op;
				$lists = tryPar('Lists', array());
				foreach($lists as $list) {
					if(in_array($list, $oldLists)) {
						unset($oldLists[$list]);
						continue;
					}
					$list = MailingList::geef($list);
					$listObj = new Mailing_MailingList($mailing, $list);
					$listObj->opslaan();
				}

				$returnVal = NULL;
				// De oude mailinglijsten verwijderen
				foreach($oldLists as $list) {
					$list = MailingList::geef($list);
					$listList = Mailing_MailingList::geef($mailing, $list);
					$returnVal = $listList->verwijderen();
					if(!is_null($returnVal)) {
						break;
					}
				}

				if(!is_null($returnVal)) {
					Page::addMeldingArray($returnVal, 'fout');
				} else {
					// Sla de plaintext op
					$mailing->setMailBody(tryPar('Mailing[MailBody]'));

					Page::redirectMelding($mailing->url(), _("Mailing gewijzigd"));
				}
			} else {
				Page::addMelding(_("Er waren fouten"), 'fout');
				$show_error = true;
			}
		}

		MailingView::wijzigForm($mailing, false, $show_error);
	}

	/**
	 * Geeft de pagina voor het versturen van een preview
	 */
	static public function previewMailing()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken()) {
			spaceHttp(403);
		}

		$msg = array();

		if(Token::processNamedForm('preview')) {
			$recipients = array_filter(explode(",", str_replace(" ", "", tryPar('preview_recipients'))));

			if(!is_array($recipients) || sizeof($recipients) == 0) {
				Page::addMelding(_("Je hebt geen e-mailadressen opgegeven"), 'fout');
			} else {
				$mailing->verstuurMailingPreview($recipients);

				Page::redirectMelding($mailing->url(), _("Preview is verstuurd!"));
			}
		}

		MailingView::makePreviewInterface($mailing);
	}

	/**
	 * Geeft de pagina voor het versturen van een mailing
	 * Alleen te gebruiken voor goden!
	 */
	static public function verstuurMailing()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken()) {
			spaceHttp(403);
		}

		$page = Page::getInstance()->start('Mailing versturen');
		$mailing->verstuurMailing();
		$page->end();
	}

	/**
	 * Geeft de pagina voor het verwijderen van een mailing
	 */
	static public function verwijderMailing()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken() || !$mailing->magVerwijderen()) {
			spaceHttp(403);
		}

		if(Token::processNamedForm() == 'MailingVerwijderen') {
			$returnVal = $mailing->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(MAILINGWEB, _("Verwijderen gelukt"));
			}
		}

		$page = Page::getInstance()->start(sprintf(_("Mailing %s verwijderen"), $mailing->geefID()));
		$page->add(MailingView::verwijderForm($mailing));
		$page->end();
	}

	/**
	 * Geeft de pagina voor het importeren van een extern bestand voor
	 * een mailing
	 */
	static public function importEML()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magWijzigen()) {
			spaceHttp(403);
		}

		if(Token::processNamedForm() == 'importeren') {
			if($eml_path = tryPar('eml_path')) {
				if(!file_exists($eml_path)) {
					Page::addMelding(_("Het bestand wat je opgeeft is niet leesbaar"), 'fout');
				} else {
					$res = $mailing->parseEML($eml_path);
					if(!$res) {
						Page::redirectMelding($mailing->url(), _("Gelukt!"));
					} else {
						Page::addMelding(_("Er waren fouten bij het importeren"), 'fout');
					}
				}
			} else {
				Page::addMelding(_("Je moet een bestand meegeven!"), 'fout');
			}

		}

		MailingView::makeEMLImportInterface($mailing);
	}

	/**
	 * TODO: Deze pagina moet nog gefixt worden
	 */
	static public function sentMailing()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		// Mailing tonen zoals deze verzonden is
		if (!$mailing->isVerzonden()) spaceHTTP(404);

		$type = tryPar('type', null);

		//Dit werkt allemaal niet?!
		if ($type == 'MIME'){
			MailingView::printMIMEContentBlock($mailing, null);
			die();
		} elseif ($type){
			MailingView::printSentMailing($mailing, $type);
			die();
		} else {
			Page::getInstance()->start('MailingWeb: mailing bekijken');
			echo MailingView::makeSentMailingInterface($mailing);
		}
		Page::getInstance()->end();
	}

	/**
	 * Geeft de pagina met alle mailinglists weer
	 */
	static public function mailinglists()
	{
		MailingListVerzamelingView::mailingListsOverzicht();
	}

	/**
	 * Geeft een pagina voor het aanmaken van een nieuwe mailinglist
	 */
	static public function nieuweMailinglist()
	{
		if(!hasAuth('bestuur')) {
			spaceHttp(403);
		}

		$list = new MailingList();

		$show_error = false;

		if(Token::processNamedForm() == 'NieuweMailingList') {
			MailingListView::processForm($list);
			if($list->valid()) {
				$list->opslaan();
				Page::redirectMelding($list->url(), _("Toevoegen gelukt"));
			} else {
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		MailingListView::wijzigForm($list, true, $show_error);
	}

	static public function mailinglistEntry ($args)
	{
		$list = MailingList::geef($args[0]);
		if (!$list) return false;

		return array( 'name' => $list->geefID()
					, 'displayName' => $list->geefID()
					, 'access' => true); 
	}

	/**
	 * Geeft de details van een mailinglist
	 */
	static public function mailinglistDetails ()
	{
		$listid = (int) vfsVarEntryName();
		$list = MailingList::geef($listid);

		if(!$list || !$list->magBekijken()) {
			spaceHttp(403);
		}

		MailingListView::mailingListDetailsInterface($list);
	}

	/**
	 * Geeft de pagina voor het wijzigen van een mailinglist
	 */
	static public function wijzigMailinglist()
	{
		$listid = (int) vfsVarEntryName();
		$list = MailingList::geef($listid);

		if(!$list || !$list->magBekijken() || !$list->magWijzigen()) {
			spaceHttp(403);
		}

		$show_error = false;

		if(Token::processNamedForm() == 'WijzigMailingList') {
			MailingListView::processForm($list);
			if($list->valid()) {
				$list->opslaan();
				Page::redirectMelding($list->url(), _("Gelukt!"));
			} else {
				$show_error = true;
				Page::addMelding(_("Er zijn fouten"), 'fout');
			}
		}

		MailingListView::wijzigForm($list, false, $show_error);
	}

	/**
	 *	 Laat zien welke urls er veranderd zijn in TinyUrls, gebruik regex om alle href= links te vinden
	 *	 en bied deze aan om te converteren. Dit is expres redelijk beperkt omdat puur op urls gaan zoeken
	 *	 te makkelijk te veel matcht (alles met een punt er in kan een url zijn! voor een plaatje!)
	 **/
	public static function Urls()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken()) {
			spaceHttp(403);
		}

		MailingView::urls($mailing);
	}

	/**
	 * Geeft de pagina met het overzicht van de attachments van de mailing
	 */
	public static function Attachments()
	{
		$mailingid = (int) vfsVarEntryName();
		$mailing = Mailing::geef($mailingid);

		if(!$mailing || !$mailing->magBekijken()) {
			spaceHttp(403);
		}

		MailingView::attachments($mailing);
	}

}

