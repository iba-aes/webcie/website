<?

/**
 * $Id$
 */
abstract class MailingListView
	extends MailingListView_Generated
{
	static public function labelAbonnees(MailingList $list)
	{
		return _("Abonnees");
	}

	static public function waardeAbonnees(MailingList $list)
	{
		return $list->abonnees()->aantal();
	}

	static public function makeLink(MailingList $list)
	{
		return new HtmlAnchor($list->url(), self::waardeNaam($list));
	}

	/**
		Maakt interface om details van een mailinglist te bekijken en/of
		bewerken.  De boolean $editable geeft aan of velden door forms
		vervangen moeten worden om ze editable te maken. Eventuele wijzigingen
		worden ook door deze methode in het MailingList-object doorgevoerd.

		Bij $mailinglist == null wordt een interface getoond voor het aanmaken
		van een nieuwe mailinglist.
	**/
	static public function mailingListDetailsInterface($mailinglist)
	{
		$page = Page::getInstance();

		if($mailinglist->magWijzigen()) {
			$page->setEditUrl($mailinglist->url() . "Edit");
		}

		$page->start(sprintf(_("Details van mailinglist %s"),
				MailingListView::waardeNaam($mailinglist)));

		$page->add($table = new HtmlTable());

		// Naam
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelNaam($mailinglist));
		$row->addData(MailingListView::waardeNaam($mailinglist));

		// Omschrijving
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelOmschrijving($mailinglist));
		$row->addData(MailingListView::waardeOmschrijving($mailinglist));

		// Subject prefix
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelSubjectPrefix($mailinglist));
		$row->addData(MailingListView::waardeSubjectPrefix($mailinglist));

		// Body prefix
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelBodyPrefix($mailinglist));
		$cell = $row->addData(MailingListView::waardeBodyPrefix($mailinglist));
		$cell->setCssStyle('font-family: mono;');

		// body suffix
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelBodySuffix($mailinglist));
		$cell = $row->addData(MailingListView::waardeBodySuffix($mailinglist));
		$cell->setCssStyle('font-family: mono;');

		// Abonnees
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelAbonnees($mailinglist));
		$row->addData(MailingListView::waardeAbonnees($mailinglist));

		// Verborgen
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelVerborgen($mailinglist));
		$row->addData(MailingListView::waardeVerborgen($mailinglist));

		// Query
		$table->add($row = new HtmlTableRow());
		$row->addData(MailingListView::labelQuery($mailinglist));
		$row->addData(MailingListView::waardeQuery($mailinglist));

		$page->add(new HtmlHeader(3, _("Reeds verzonden mailings")));

		// Recente mailings
		$page->add($table = new HtmlTable());
		$table->add($row = new HtmlTableRow());
		$row->addHeader(MailingView::labelMailSubject(new Mailing()));
		$row->addHeader(MailingView::labelMomentIngepland(new Mailing()));
		$row->addHeader(MailingView::labelMomentVerwerkt(new Mailing()));

		foreach ($mailinglist->getMailings() as $mailing)
		{
			$table->add($row = new HtmlTableRow());
			$row->addData(MailingView::waardeMailSubject($mailing));
			$row->addData(MailingView::waardeMomentIngepland($mailing));
			$row->addData(MailingView::waardeMomentVerwerkt($mailing));
		}

		$page->add(HtmlAnchor::button(MAILINGWEB . "Mailinglists", _("Terug naar het overzicht")));

		$page->end();
	}

	/**
	 * Maakt een form voor een nieuwe mailinglist en voor het wijzigen van een
	 * al bestaande mailinglist
	 */
	static public function wijzigForm(MailingList $list, $nieuw = false, $show_error = false)
	{
		$page = Page::getInstance();

		if($nieuw) {
			$page->start(_("Nieuwe mailinglijst maken"));
		} else {
			$page->start(sprintf(_("Mailinglijst %s wijzigen"),
				MailingListView::waardeNaam($list)));
		}

		if($nieuw) {
			$page->add($form = HtmlForm::named("NieuweMailingList"));
		} else {
			$page->add($form = HtmlForm::named("WijzigMailingList"));
		}

		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($list, 'naam', $show_error));
		$div->add(self::wijzigTR($list, 'Omschrijving', $show_error));
		$div->add(self::wijzigTR($list, 'SubjectPrefix', $show_error));
		$div->add(self::wijzigTR($list, 'BodyPrefix', $show_error));
		$div->add(self::wijzigTR($list, 'BodySuffix', $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Doe!")));

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
