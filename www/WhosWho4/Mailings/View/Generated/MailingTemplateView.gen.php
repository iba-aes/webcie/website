<?
abstract class MailingTemplateView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingTemplateView.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingTemplate(MailingTemplate $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailingTemplateID.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld mailingTemplateID labelt.
	 */
	public static function labelMailingTemplateID(MailingTemplate $obj)
	{
		return 'MailingTemplateID';
	}
	/**
	 * @brief Geef de waarde van het veld mailingTemplateID.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingTemplateID van het
	 * object obj representeert.
	 */
	public static function waardeMailingTemplateID(MailingTemplate $obj)
	{
		return static::defaultWaardeInt($obj, 'MailingTemplateID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingTemplateID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingTemplateID representeert.
	 */
	public static function opmerkingMailingTemplateID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(MailingTemplate $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(MailingTemplate $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param MailingTemplate $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(MailingTemplate $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld standaardPreviewers.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld standaardPreviewers labelt.
	 */
	public static function labelStandaardPreviewers(MailingTemplate $obj)
	{
		return 'StandaardPreviewers';
	}
	/**
	 * @brief Geef de waarde van het veld standaardPreviewers.
	 *
	 * @param MailingTemplate $obj Het MailingTemplate-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld standaardPreviewers van het
	 * object obj representeert.
	 */
	public static function waardeStandaardPreviewers(MailingTemplate $obj)
	{
		return static::defaultWaardeString($obj, 'StandaardPreviewers');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld standaardPreviewers.
	 *
	 * @see genericFormstandaardPreviewers
	 *
	 * @param MailingTemplate $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld standaardPreviewers staat
	 * en kan worden bewerkt. Indien standaardPreviewers read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formStandaardPreviewers(MailingTemplate $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'StandaardPreviewers', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld standaardPreviewers.
	 * In tegenstelling tot formstandaardPreviewers moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formstandaardPreviewers
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld standaardPreviewers staat
	 * en kan worden bewerkt. Indien standaardPreviewers read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormStandaardPreviewers($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'StandaardPreviewers', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * standaardPreviewers bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld standaardPreviewers representeert.
	 */
	public static function opmerkingStandaardPreviewers()
	{
		return NULL;
	}
}
