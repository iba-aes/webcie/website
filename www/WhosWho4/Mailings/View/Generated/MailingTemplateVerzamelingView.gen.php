<?
abstract class MailingTemplateVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingTemplateVerzamelingView.
	 *
	 * @param MailingTemplateVerzameling $obj Het MailingTemplateVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingTemplateVerzameling(MailingTemplateVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
