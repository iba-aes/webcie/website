<?
abstract class Mailing_MailingListView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in Mailing_MailingListView.
	 *
	 * @param Mailing_MailingList $obj Het Mailing_MailingList-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailing_MailingList(Mailing_MailingList $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailing.
	 *
	 * @param Mailing_MailingList $obj Het Mailing_MailingList-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailing labelt.
	 */
	public static function labelMailing(Mailing_MailingList $obj)
	{
		return 'Mailing';
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @param Mailing_MailingList $obj Het Mailing_MailingList-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailing van het object obj
	 * representeert.
	 */
	public static function waardeMailing(Mailing_MailingList $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMailing())
			return NULL;
		return MailingView::defaultWaardeMailing($obj->getMailing());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld mailing
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailing representeert.
	 */
	public static function opmerkingMailing()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailingList.
	 *
	 * @param Mailing_MailingList $obj Het Mailing_MailingList-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailingList labelt.
	 */
	public static function labelMailingList(Mailing_MailingList $obj)
	{
		return 'MailingList';
	}
	/**
	 * @brief Geef de waarde van het veld mailingList.
	 *
	 * @param Mailing_MailingList $obj Het Mailing_MailingList-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingList van het object
	 * obj representeert.
	 */
	public static function waardeMailingList(Mailing_MailingList $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMailingList())
			return NULL;
		return MailingListView::defaultWaardeMailingList($obj->getMailingList());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingList bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingList representeert.
	 */
	public static function opmerkingMailingList()
	{
		return NULL;
	}
}
