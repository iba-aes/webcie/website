<?
abstract class Mailing_MailingListVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in Mailing_MailingListVerzamelingView.
	 *
	 * @param Mailing_MailingListVerzameling $obj Het
	 * Mailing_MailingListVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailing_MailingListVerzameling(Mailing_MailingListVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
