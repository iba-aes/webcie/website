<?
abstract class TinyMailingUrlVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TinyMailingUrlVerzamelingView.
	 *
	 * @param TinyMailingUrlVerzameling $obj Het TinyMailingUrlVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTinyMailingUrlVerzameling(TinyMailingUrlVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
