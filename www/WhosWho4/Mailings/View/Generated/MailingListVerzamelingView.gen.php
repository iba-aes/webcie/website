<?
abstract class MailingListVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingListVerzamelingView.
	 *
	 * @param MailingListVerzameling $obj Het MailingListVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingListVerzameling(MailingListVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
