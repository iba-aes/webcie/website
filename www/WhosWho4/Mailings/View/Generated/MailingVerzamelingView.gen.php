<?
abstract class MailingVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingVerzamelingView.
	 *
	 * @param MailingVerzameling $obj Het MailingVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingVerzameling(MailingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
