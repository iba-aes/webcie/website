<?php

/**
	 Class die een MIME part representeert.

	Een MIME part bestaat uit twee delen
	 - header
	 - contents

	De contents kunnen een reeks MIME parts op zich zijn. In dat geval moet de
	header aangeven dat het Content-Type multipart is
**/
class MailMimePart {
	private $_headers = array();
	private $_body = array();
	private $_isMultipart;
	private $_multipartBoundary;
	private $_parts = array();

	public function __construct($arrdata){
		$this->_headers = array();
		$this->_body = array();

		$inbody = false;
		$prevhdrtype = null;
		$currpartdata = null;
		foreach ($arrdata as $line){
			if (!$inbody){
				// Header
				if (trim($line) == ""){
					$inbody = true;

					if (substr($this->getContentType(), 0, 10) == "multipart/"){
						// Dit MIME part bevat zelf een reeks MIME parts!
						$this->_isMultipart = true;

						// Wat is de boundary?
						$ext_ct = $this->getExtendedContentType();
						$regex_pattern_b = '/boundary=(.*)$/';

						// Met preg_match even de boundary opzoeken
						if (!preg_match($regex_pattern_b, $ext_ct, $match_b)){
							throw new Exception("Boundary van multipart mime part niet gedefinieerd? $line");
						}

						$match_b[1] = str_replace('"', '', $match_b[1]);
						$this->_multipartBoundary = $match_b[1];
					} // else: geen multi-part MIME part

					continue; // deze lege regel negeren
				}

				if (strpos($line, ":") === false){
					// Dit stukje header hoort nog bij de vorige header
					if ($prevhdrtype){
						$this->_headers[$prevhdrtype] .= trim($line);
					} else {
						$logger->info('Mailheaders: ' . print_r($arrdata));
						throw new Exception("Header van vreemd formaat? $line");
					}
				} else {
					$hdrType = trim(substr($line, 0, strpos($line, ":")));
					$hdrValue = trim(substr($line, strpos($line, ":") + 1));
					$this->_headers[$hdrType] = $hdrValue;
					$prevhdrtype = $hdrType;
				}
			} else {
				// Klaar met de headers, nu is de body aan de beurt
				if ($this->isMultipart()){
					// Losse parts parsen, geen reguliere data
					$boundary = $this->_multipartBoundary;

					if ($line == "--$boundary"){
						// Nieuw part, oude part even afsluiten?
						if (is_array($currpartdata)){
							$this->_parts[] = new MailMimePart($currpartdata);
						}
						$currpartdata = array();
						continue;
					} elseif ($line == "--$boundary--"){
						// Einde van de body van dit multipart part, laatste
						// child even afsluiten
						if (is_array($currpartdata)){
							$this->_parts[] = new MailMimePart($currpartdata);
						}
						break;
					}

					if (is_array($currpartdata)){
						$currpartdata[] = $line;
					} else {
						$this->_body[] = $line;
					}
				} else {
					// Gewoon data parsen, niks ingewikkelds met multiparts
					$this->_body[] = $line;
				}
			}
		}
	}

	public function isMultipart(){
		return $this->_isMultipart;
	}

	/**
		 Retourneert alle parts binnen deze multipart.

		Als dit geen multipart is, dan valt er een exception!

		@param flatten Geeft aan of de boomstructuur platgeslagen moet worden:
		alle multiparts gaan er uit, alle 'leafs' komen in een platte array
		terecht
	**/
	public function getParts($flatten = false){
		if (!$this->isMultipart()){
			throw new Exception("Part is not multipart!");
		}

		if ($flatten){
			$res = array();

			foreach ($this->_parts as $part){
				if ($part->isMultipart()){
					$res = array_merge($res, $part->getParts(true));
				} else {
					$res[] = $part;
				}
			}
			return $res;
		} else {
			return $this->_parts;
		}
	}

	/**
		 Retourneert verkort content type, zoals "text/html"

		@see getExtendedContentType
	**/
	public function getContentType(){
		$ext = $this->getExtendedContentType();
		if (!$ext) return null;

		$semicolon = strpos($ext, ";");
		return substr($ext, 0, $semicolon);
	}

	/**
		 Retourneert uitgebreid content type, zoals "text/html; charset=windows-1252"
	**/
	public function getExtendedContentType(){
		$val = $this->getHeaderValue("Content-Type");
		if (!$val) return null;
		return $val;
	}

	public function getBody(){
		return $this->_body;
	}

	public function setBody($bodyarr){
		if (!is_array($bodyarr)) throw new Exception("Expecting array");
		$this->_body = $bodyarr;
	}

	public function getHeaderValue($header){
		if (!isset($this->_headers[$header])) return null;
		return $this->_headers[$header];
	}

	public function toString($suppressTopLevelHeader = false){
		$res = "";

		if (!$suppressTopLevelHeader){
			foreach ($this->_headers as $key=>$value){
				$res .= "$key: $value\n";
			}
			$res .= "\n";
		}

		if (sizeof($this->_body) > 0){
			$res .= implode("\n", $this->_body);
			$res .= "\n";
		}

		if ($this->isMultipart()){
			foreach ($this->_parts as $part){
				$res .= "--" . $this->_multipartBoundary . "\n";
				$res .= $part->toString();
			}
			$res .= "--" . $this->_multipartBoundary . "--\n";
		}

		return $res;
	}

	/**
		 Bepaalt of er iets met Content-Type multipart/... is, zo ja: retourneert MailMimePart. Zo nee: retourneert false
	**/
	public static function parseMailData($contentsarr){
		foreach ($contentsarr as $line){
			if (trim($line) == "") break; // einde header, stop parsen
			if (strpos($line, "Content-Type: multipart/") === 0) return new MailMimePart($contentsarr);
		}
		return NULL;
	}
}
