<?
class VakStudieQuery
	extends VakStudieQuery_Generated
{
	public static function isVakVanStudie(Vak $vakID, Studie $studieID) {
		return VakStudieQuery::table()
			->whereProp('vakID', $vakID)
			->whereProp('studieID', $studieID)
			->count() === 1;
	}
}
