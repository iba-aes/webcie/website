<html>

<head>
	<title><?php echo _('Verkoopvenster'); ?></title>
</head>

<body>
<form method='post' name='venster'>
	<?php if ($succes || !($contact instanceof Contact || $contact == BOEKEN_ANONIEM)) { ?>
	<?php if ($succes) { ?>
	<div>
		<?php echo _("De transactie was succesvol!"); ?>
	</div>
	<?php } ?>
	<input type='hidden' name='pin' value='<?php echo tryPar('pin', 0); ?>' />
	<h1><?php echo _('Selecteer een contact'); ?></h1>
	<p><?php echo _('Aan wie wil je iets verkopen?'); ?>
		<input type='text' name='contact' value='' /></p>
	<p><a href='#' onclick="document.forms['venster'].contact.value='<?php echo BOEKEN_ANONIEM; ?>';document.forms['venster'].submit()">
		<?php echo _('Maak een anonieme transactie.'); ?></a></p>
	<?php if ($personen->aantal() > 0) { ?>
	<p><?php echo _('Gevonden personen:'); ?></p>
	<p><ul>
		<?php foreach ($personen as $persoon) { ?>
		<li><a href='#' onclick="document.forms['venster'].contact.value='id:<?php echo $persoon->geefID(); ?>';document.forms['venster'].submit()">
		<?php echo PersoonView::naam($persoon); ?></a></li>
		<?php } ?>
	</ul></p>
	<?php } ?>
	<?php } else { ?>
	<input type='hidden' name='contact' value='<?php echo tryPar('contact'); ?>' />
	<div>
		<h1>Menu</h1>
		<a href='<?php echo BOEKEN_URL; ?>/Verkoop/Venster'>
			<?php echo _('Terug'); ?></a>
	</div>
	<div>
		<h1>Contact</h1>
		<?php if ($contact instanceof Contact) { ?>
		<?php printf(_('Geselecteerd contact: %s'), ContactView::naam($contact)); ?>
		<?php } else { ?>
		<?php echo _('Bezig met een anonieme transactie.'); ?>
		<?php } ?>
	</div>
	<div>
		<h1>Winkel</h1>
		<input type='hidden' name='naarKart' value='0' />
		<?php $dictaten = DictaatVerzameling::verkoopbaarInBoekenhok(); if ($dictaten->aantal() > 0) { ?>
		<h2><?php echo _('Dictaten'); ?></h2>
		<table>
			<?php foreach ($dictaten as $dictaat) { ?>
			<?php foreach ($dictaat->voorradenInBoekenhok() as $voorraad) { ?>
			<tr>
				<td><select name='artikel_<?php echo $voorraad->geefID(); ?>'>
					<?php for ($i = 1; $i <= $voorraad->getAantal() && $i < 50; $i++) { ?>
					<option value='<?php echo $i; ?>'><?php echo $i; ?></option>
					<?php } ?>
				</select></td>
				<td><input type='button' value='<?php echo _('Naar kart'); ?>'
					onclick="document.forms['venster'].naarKart.value='<?php echo $voorraad->geefID(); ?>';document.forms['venster'].submit()" /></td>
				<td><?php echo VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs()); ?></td>
				<td><?php echo DictaatView::waardeNaam($dictaat); ?></td>
				<td><?php echo VoorraadView::waardeOmschrijving($voorraad); ?></td>
			</tr>
			<?php } ?>
			<?php } ?>
		</table>
		<?php } ?>
		<?php $dibsproducten = DibsProductVerzameling::verkoopbaar(); if ($dibsproducten->aantal() > 0) { ?>
		<h2><?php echo _('Dibsproducten'); ?></h2>
		<table>
			<?php foreach ($dibsproducten as $dibsproduct) { ?>
			<?php foreach ($dibsproduct->getVoorraden() as $voorraad) {?>
			<tr>
				<td><select name='artikel_<?php echo $voorraad->geefID(); ?>'>
					<?php for ($i = 1; $i <= $voorraad->getAantal() && $i < 50; $i++) { ?>
					<option value='<?php echo $i; ?>'><?php echo $i; ?></option>
					<?php } ?>
				</select></td>
				<td><input type='button' value='<?php echo _('Naar kart'); ?>'
					onclick="document.forms['venster'].naarKart.value='<?php echo $voorraad->geefID(); ?>';document.forms['venster'].submit()" /></td>
				<td><?php echo VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs()); ?></td>
				<td><?php echo DibsProductView::waardeNaam($dibsproduct); ?></td>
				<td>(<?php echo VoorraadView::waardeOmschrijving($voorraad); ?>)</td>
			</tr>
			<?php } ?>
			<?php } ?>
		</table>
		<?php } ?>
	</div>
	<div>
		<?php if (count($kart) > 0) { ?>
		<h1>Kart</h1>
		<input type='hidden' name='uitKart' value='0' />
		<table>
			<?php foreach ($kart as $id => $aantal) { ?>
			<tr>
				<td><input type='hidden' name='kart[<?php echo $id; ?>]' value='<?php echo $aantal; ?>' />
					<?php echo VoorraadView::waardePrijs(Voorraad::geef($id), $aantal); ?></td>
				<td>
					<?php echo $aantal; ?></td>
				<td><?php echo ArtikelView::waardeNaam(Voorraad::geef($id)->getArtikel()); ?></td>
				<td><input type='button' value='<?php echo _('Uit kart'); ?>'
					onclick="document.forms['venster'].uitKart.value='<?php echo $id; ?>';document.forms['venster'].submit()" /></td>
			</tr>
			<?php } ?>
		</table>
		<p><?php echo _('Totaal') . ': ' . Money::addPrice($prijs); ?></p>
		<h2>Afrekenen</h2>
		<input type='hidden' name='afrekenen' value='0' />
		<table>
			<tr>
				<td><?php echo _('Welk pin-apparaat is gebruikt?'); ?></td>
				<td><ul>
					<?php foreach (PinView::labelenumApparaatArray() as $id => $label) { ?>
					<li><input type="radio" name="pin" value="<?php echo $id; ?>" <?php if (tryPar('pin') == $id) echo 'checked'; ?> />
						<?php echo $label; ?></li>
					<?php } ?>
				</ul></td>
			</tr>
		</table>
		<p><input type='button' value='<?php echo _('Afrekenen'); ?>'
			onclick="document.forms['venster'].afrekenen.value='1';document.forms['venster'].submit()" /></p>
		<?php } ?>
	</div>
	<?php } ?>
	</form>
</body>

</html>
