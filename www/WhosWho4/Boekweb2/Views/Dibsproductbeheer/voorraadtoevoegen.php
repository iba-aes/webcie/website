<h1><?php printf(_('Meer producten van "%s" toevoegen'), $dibsproduct->getNaam());?></h1>

<p>
	<?php printf(_('Klik %s om terug te gaan'), '<a href=\'' . $dibsproduct->url() .'\'>hier</a>');?>
</p>

<form method="post">

<?php if(isset($msg)) {
	foreach($msg as $m) {?>
		<p>
			<span class="negatief"><?php echo $m;?></span>
		</p>
<?php }
}?>

<table>
	<tr>
		<td><b><?php echo _('Naam:');?></b></td>
		<td><b><?php echo $dibsproduct->getNaam();?></b></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel erbij?');?></td>
		<td><input type="number" name="aantal"></td>
	</tr>
	<tr>
		<td><?php echo _('In de voorraad:');?></td>
		<td><?php echo $voorraad->getLocatie()?></td>
	</tr>
</table>

<p><input type="submit" name="submit" value="<?php echo _('Voeg toe');?>"/></p>

</form>
