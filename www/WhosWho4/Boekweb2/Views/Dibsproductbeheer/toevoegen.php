<h1>Voeg een nieuw dibsproduct toe</h1>

<p>
	<?php printf(_('Klik %s om terug te gaan naar het dibsproductoverzicht.'), '<a href="/Onderwijs/Boeken2/Dibsproductbeheer/">hier</a>');?>
</p>

<form method="post">

<?php if(isset($msg)) {
	foreach($msg as $m) {?>
		<p>
			<span class="negatief"><?php echo $m;?></span>
		</p>
<?php	}
}?>

<table>
	<tr>
		<td><?php echo _('Hoe heet het dibsproduct?')?></td>
		<td><input type="text" name="naam"></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel kudos kost het dibsproduct?')?></td>
		<td><input type="number" name="kudos"></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel euros kost het dibsproduct?')?></td>
		<td><input type="number" name="prijs" min="0" step="any"></td>
	</tr>
	<tr>
		<td><?php echo _('Wat is de waarde van het dibsproduct?')?></td>
		<td><input type="number" name="waarde" min="0" step="any"></td>
	</tr>
	<tr>
		<td><?php echo _('Is het dibsproduct verkoopbaar?')?></td>
		<td><ul><li><input type="radio" name="verkoopbaar" value="1"/><?php echo _('Ja')?></li>
				<li><input type="radio" name="verkoopbaar" value="0"/><?php echo _('Nee')?></li></ul></td>
	<tr>
		<td><?php echo _('In welke voorraad komt het dibsproduct?')?></td>
		<td><ul><?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
			<li><input type="radio" name="locatie" value="<?php echo $id;?>"/><?php echo $label;?></li>
			<?php } ?></ul></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel in de voorraad?')?></td>
		<td><input type="number" name="aantal"></td>
	</tr>
	<tr>
		<td><?php echo _('Vul eventueel een omschrijving in:')?></td>
		<td><textarea name="omschrijving" rows=4 cols=20></textarea></td>
	</tr>
</table>

<p><input type="submit" name="submit" value="<?php echo _('Maak het product aan');?>"/></p>

</form>
