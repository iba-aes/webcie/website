<h1><?php echo DibsProductView::waardeNaam($dibsproduct); ?></h1>

<p>
	<?php printf(_("Klik %shier%s om terug naar de dibsproduct-pagina te gaan."), '<a href=\''.$dibsproduct->url().'\'>', '</a>'); ?>
</p>

<h2><?php echo _("Mutaties geaggregeerd van alle varianten"); ?></h2>

<table>
	<tr>
		<th><?php echo _("Wanneer"); ?></th>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $label) { ?>
		<th><?php echo $label; ?></th>
		<?php } ?>
		<th><?php echo _("Omschrijving"); ?></th>
	</tr>
	<?php foreach ($dibsproduct->getVoorraadMutatieRelaties() as $mutatie) { ?>
	<tr>
		<td><?php echo VoorraadMutatieView::waardeWanneer($mutatie); ?></td>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
		<td><?php if ($mutatie->getVoorraad()->getLocatie() == $id) { echo VoorraadMutatieView::waardeAantal($mutatie); } ?></td>
		<?php } ?>
		<td><?php echo VoorraadMutatieView::type($mutatie); ?></td>
	</tr>
	<?php } ?>
	<tr>
		<th><?php echo _("Totaal"); ?></th>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
		<td><?php echo VoorraadView::waardeAantalArtikel($dibsproduct, $id); ?></td>
		<?php } ?>
		<td></td>
	</tr>
</table>

<h2><?php echo _("Mutaties per variant"); ?></h2>

<?php foreach ($dibsproduct->getVoorraden() as $voorraad) { ?>
<p><?php echo VoorraadView::waardeOmschrijving($voorraad); ?></p>
<table>
	<tr>
		<th><?php echo _("Wanneer"); ?></th>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $label) { ?>
		<th><?php echo $label; ?></th>
		<?php } ?>
		<th><?php echo _("Omschrijving"); ?></th>
	</tr>
	<?php foreach ($voorraad->getVoorraadMutatieRelaties() as $mutatie) { ?>
	<tr>
		<td><?php echo VoorraadMutatieView::waardeWanneer($mutatie); ?></td>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
		<td><?php if ($mutatie->getVoorraad()->getLocatie() == $id) { echo VoorraadMutatieView::waardeAantal($mutatie); } ?></td>
		<?php } ?>
		<td><?php echo VoorraadMutatieView::type($mutatie); ?></td>
	</tr>
	<?php } ?>
	<tr>
		<th><?php echo _("Totaal"); ?></th>
		<?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
		<td><?php echo VoorraadView::waardeAantal($voorraad); ?></td>
		<?php } ?>
		<td></td>
	</tr>
</table>
<?php } ?>
