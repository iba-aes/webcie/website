<?php

abstract class Dibsproductbeheer_Controller
{
	static public function toevoegen () {
		if (tryPar('submit', false))
			$msg = self::toevoegenValidator();

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dibsproductbeheer/toevoegen.php");
		Page::getInstance()->end();
	}

	static protected function toevoegenValidator() {
		$naam=tryPar('naam', NULL);
		$kudos=tryPar('kudos', 0);
		$prijs=trypar('prijs', 0);
		$waarde=trypar('waarde', 0);
		$verkoopbaar=tryPar('verkoopbaar', NULL);
		$locatie=tryPar('locatie', NULL);
		$aantal=tryPar('aantal', NULL);
		$omschrijving=tryPar('omschrijving', NULL);

		$return = array();

		$dp = new DibsProduct();
		$dp->setNaam($naam);
		$dp->setKudos($kudos);

		if(!$verkoopbaar)
			$return[] = _('Geef aan of het product verkoopbaar is of niet');
		$voorraad = new Voorraad($dp);
		$voorraad->setOmschrijving($omschrijving)
			->setVerkoopbaar($verkoopbaar);

		if($prijs<0)
			$return[] = _('Vul een positieve prijs in AUB.');
		elseif($prijs>0) {
			$verkoopprijs = new VerkoopPrijs($voorraad, new DateTimeLocale(), $prijs);
		}
		if($waarde!=0)
			$voorraad->setWaardePerStuk($waarde);

		if(!$locatie)
			$return[] = _('Vink een geldige locatie aan.');
		$voorraad->setLocatie($locatie);

		if($aantal<0)
			$return[] = _('Lolbroek, een beetje een negatief aantal in de voorraad zetten...');
		elseif($aantal>0) {
			$voorraadmutatie = new VoorraadMutatie($voorraad, new DateTimeLocale(), $aantal);
		}

		if(count($return)>0)
			return $return;

		$dp->opslaan();
		$voorraad->opslaan();
		if($prijs>0)
			$verkoopprijs->opslaan();
		if($aantal>0)
			$voorraadmutatie->opslaan();

		Page::redirectMelding('/Onderwijs/Boeken2/Dibsproductbeheer/', 'Glückt!');
	}

	static public function lijst () {
		$dibsproducten = DibsProductVerzameling::alle();

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dibsproductbeheer/lijst.php");
		Page::getInstance()->end();
	}

	static public function dibsproductEntry ($args) {
		$dibsproduct = DibsProduct::geef($args[0]);
		if (!$dibsproduct)
			return false;

		return array( 'name' => $dibsproduct->getArtikelID()
			, 'access' => true);
	}

	static public function details ()
	{
		$id = (int)vfsVarEntryName();
		$dibsproduct = DibsProduct::geef($id);

		Page::getInstance()->start();
		include('WhosWho4/Boekweb2/Views/Dibsproductbeheer/details.php');
		Page::getInstance()->end();
	}

	static public function voorraadEntry ($args)
	{
		$voorraad = Voorraad::geef($args[0]);
		if (!$voorraad) return false;

		return array('name' => $voorraad->geefID(),
			'access' => true);
	}

	static public function nieuweLevering ()
	{
		if (tryPar('submit', false))
			$msg = self::nieuweLeveringFormValidator();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[0];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dibsproductbeheer/nieuwelevering.php");
		Page::getInstance()->end();
	}

	static protected function nieuweLeveringFormValidator()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[0];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		$voorraad = new Voorraad($dibsproduct);
		$voorraad->setOmschrijving(tryPar('omschrijving'))
			->setVerkoopbaar(tryPar('verkoopbaar'));

		$prijs = null;
		if (!is_null(tryPar('prijs')))
			$prijs = new VerkoopPrijs($voorraad, new DateTimeLocale(), tryPar('prijs'));

		if (!$artikel->valid() || (!is_null($prijs) && !$prijs->valid()))
			$return[] = _('Er klopt iets niet!');

		$voorraad = Voorraad::zoek($artikel, tryPar('locatie'));
		$datum = new DateTimeLocale();
		$leverancier = Leverancier::geef(8519);
		$levering = new Levering($voorraad, $datum, tryPar('aantal'), null, $leverancier);

		if(!$levering->valid())
			$return[] = _('Er is iets niet in de haak');

		if(count($return)>0)
			return $return;

		$artikel->opslaan();
		if($prijs instanceof VerkoopPrijs)
			$prijs->opslaan();

		$levering->opslaan();

		Page::redirectMelding($dibsproduct->url(),
			_("De levering is gemaakt."));

		return;
	}

	static public function voorraadToevoegen ()
	{
		if(tryPar('submit', false))
			$msg = self::voorraadToevoegenFormValidator();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[1];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dibsproductbeheer/voorraadtoevoegen.php");
		Page::getInstance()->end();
	}

	static protected function voorraadToevoegenFormValidator()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[1];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		$datum = new DateTimeLocale();
		$leverancier = Leverancier::geef(8519);
		$levering = new Levering($voorraad, $datum, tryPar('aantal'), null, $leverancier);

		if(!$levering->valid())
			$return[] = _('Er is iets niet in de haak');

		if(count($return)>0)
			return $return;

		$levering->opslaan();

		Page::redirectMelding($dibsproduct->url(),
			_("De producten zijn toegevoegd."));

		return;
	}

	static public function voorraadVerplaatsen ()
	{
		if (tryPar('submit', false))
			$msg = self::voorraadVerplaatsFormValidator();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[1];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dibsproductbeheer/voorraadverplaats.php");
		Page::getInstance()->end();
	}

	static protected function voorraadVerplaatsFormValidator ()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dibsproductID = (int) $entryData[1];
		$dibsproduct = DibsProduct::geef($dibsproductID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		$tegenVoorraad = Voorraad::zoek($voorraad->getArtikel(), tryPar('locatie'));

		$mutatie = new Verplaatsing($tegenVoorraad, new DateTimeLocale(),
			tryPar('aantal'), $voorraad);

		if (!$mutatie->valid())
			$return[] = _('Iets met mutaties klopt niet');

		if (count($return) > 0)
			return $return;

		$mutatie->opslaan();

		Page::redirectMelding($dibsproduct->url(),
			_("De voorraad is verplaatst."));

		return;
	}

	static public function voorraadMutaties()
	{
		$entryData = vfsVarEntryName();

		$dibsproductID = (int) $entryData;
		$dibsproduct = DibsProduct::geef($dibsproductID);

		Page::getInstance()->start();
		include('WhosWho4/Boekweb2/Views/Dibsproductbeheer/voorraadmutaties.php');
		Page::getInstance()->end();
	}
}
