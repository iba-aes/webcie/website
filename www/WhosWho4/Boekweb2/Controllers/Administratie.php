<?php

namespace WhosWho4\Boekweb2;

use Controller;
use Symfony\Component\HttpFoundation\Response;

setZeurmodus();

/**
 * Controller voor administratie, zoals leden exporten naar andere systemen.
 */
class AdministratieController extends Controller
{
	/**
	 * Poep een CSV-bestand van huidige leden voor Loyverse,
	 * een (boek-)verkoopsysteem.
	 *
	 * @site {/Onderwijs/Boekweb/LoyverseExport}
	 *
	 * @throws \HTTPStatusException
	 *
	 * @return Response
	 */
	public function loyverseExport()
	{
		$this->requireAuth('bestuur');

		$kwerrie = \LidQuery::table()->whereHuidigLid();
		/** @var \LidVerzameling $leden */
		$leden = $kwerrie->verzamel();

		$resultaat = [];
		foreach ($leden as $lid)
		{
			/** @var \Lid $lid */

			// We hoeven alleen de lidnaam te geven voor het opzoeken,
			// en een lidnummer voor de terugkoppeling naar WhosWho4,
			// de rest mogen we overslaan.
			$resultaat[] = [
				'Klantnummer' => '', // Automatisch gegenereerd door Loyverse, moet (mag?) leeg blijven.
				'Naam klant' => $lid->getNaam(),
				'E-mail' => '',
				'Nummer' => '', // Telefoonnummer.
				'Punten' => '0', // Geen idee wat dit betekent, maar volgens de Loyverseimporter is de default 0.
				'Notitie' => $lid->getContactID(), // Voor de terugkoppeling loyverse -> whoswho
			];
		}

		// Hang een headertje boven de resultaten.
		array_unshift($resultaat, array_keys($resultaat[0]));
		return new \CSVResponse($resultaat, 'loyverse-leden-import.csv');
	}
}
