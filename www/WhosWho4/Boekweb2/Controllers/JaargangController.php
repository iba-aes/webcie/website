<?php
declare(strict_types=1);
namespace WhosWho4\Boekweb2;

setZeurmodus();

use \Symfony\Component\HttpFoundation\Response;

use \Controller;
use \HTMLPage;
use \HTTPStatusException;
use \Page;
use \Token;

use \Artikel;
use \Jaargang;
use \JaargangArtikel;
use \JaargangArtikelView;
use \JaargangView;
use \JaargangVerzameling;
use \JaargangVerzamelingView;
use \Vak;

class JaargangController extends Controller
{
	/**
	 * HTML-Endpoint voor het tonen van alle jaargangen van een Vak.
	 *
	 * @site{/Onderwijs/Vak/ * / Jaargang/index.html}
	 *
	 * @return Response
	 * @throws HTTPStatusException
	 */
	public function jaargangenVoorVak()
	{
		$vakid = vfsVarEntryName();

		if (!($vak = Vak::geef($vakid)))
		{
			return $this->responseUitStatus(403);
		}

		$jaargangen = JaargangVerzameling::geefVanVak($vak);

		$page = JaargangVerzamelingView::infopagina($jaargangen, $vak);
		return $this->pageResponse($page);
	}

	/**
	 * Variabele entry voor een Jaargang.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ *}
	 *
	 * @param string[] $args
	 * De stukjes van de URL (die dus gescheiden zijn door '/'),
	 * in omgekeerde volgorde: $args[0] komt in de plaats van de laatste ster, etc.
	 *
	 * @return array
	 */
	public function jaargangEntry($args) {
		$vakid = $args[2];
		$vak = Vak::geef($vakid);
		$jaargid = $args[0];
		$jaargang = Jaargang::geef($jaargid);

		if (!$vak || !$jaargang)
		{
			return [
				'access' => false,
				'displayName' => 'Jaargang',
				'name' => $jaargid,
			];
		}
		if (!$vak->magBekijken() || !$jaargang->magBekijken())
		{
			return [
				'access' => false,
				'displayName' => 'Jaargang',
				'name' => $jaargid,
			];
		}
		if ($jaargang->getVak() != $vak)
		{
			return [
				'access' => true,
				'displayName' => $vak->getNaam(),
				'name' => $jaargid,
				'rewrite' => $jaargang->url()
			];
		}

		return [
			'access' => true,
			'displayName' => $vak->getNaam(),
			'name' => $jaargid,
		];
	}

	/**
	 * HTML-endpoint om alle informatie van een Jaargang te tonen.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ * /index.html}
	 *
	 * @return Response
	 * @throws HTTPStatusException
	 */
	public function jaargangPagina()
	{
		$jaargid = vfsVarEntryName();
		if (!($jaargang = Jaargang::geef($jaargid)))
		{
			return $this->responseUitStatus(403);
		}

		return $this->pageResponse(JaargangView::infopagina($jaargang));
	}

	/**
	 * Variabele entry voor een JaargangArtikel.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ * /Artikel/ *}
	 *
	 * @param string[] $args
	 * De stukjes van de URL (die dus gescheiden zijn door '/'),
	 * in omgekeerde volgorde: $args[0] komt in de plaats van de laatste ster, etc.
	 *
	 * @return array
	 */
	public function jaargangArtikelEntry($args) {
		$jaargid = $args[2];
		$jaargang = Jaargang::geef($jaargid);
		$artid = $args[0];
		$artikel = Artikel::geef($artid);

		if (!$jaargang || !$artikel)
		{
			return [
				'access' => false,
				'displayName' => 'JaargangArtikel',
				'name' => $artid,
			];
		}
		if (!$artikel->magBekijken() || !$jaargang->magBekijken())
		{
			return [
				'access' => false,
				'displayName' => 'JaargangArtikel',
				'name' => $artid,
			];
		}

		$jaargangArtikel = JaargangArtikel::geef($jaargang, $artikel);
		if (!$jaargangArtikel || !$jaargangArtikel->magBekijken())
		{
			return [
				'access' => true,
				'displayName' => $artikel->getNaam(),
				'name' => $artid,
				'rewrite' => $jaargangArtikel->url(),
			];
		}

		return [
			'access' => true,
			'displayName' => $artikel->getNaam(),
			'name' => $artid,
		];
	}

	/**
	 * HTML-endpoint om alle informatie van een JaargangArtikel te tonen.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ * /Artikel/ * /index.html}
	 *
	 * @return Response
	 */
	public function jaargangArtikelInfo()
	{
		// Merk op dat de indices in omgekeerde volgorde staan dan de URL.
		$varNames = vfsVarEntryNames();
		$jgid = $varNames[1];
		$artid = $varNames[0];

		$jaargangArtikel = JaargangArtikel::geef($jgid, $artid);
		if (!$jaargangArtikel || !$jaargangArtikel->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		return $this->pageResponse(JaargangArtikelView::infopagina($jaargangArtikel));
	}

	/**
	 * HTML-endpoint om een JaargangArtikel te wijzigen.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ * /Artikel/ * /Wijzig}
	 *
	 * @return Response
	 */
	public function jaargangArtikelWijzigen()
	{
		// Merk op dat de indices in omgekeerde volgorde staan dan de URL.
		$varNames = vfsVarEntryNames();
		$jgid = $varNames[1];
		$artid = $varNames[0];

		$jaargangArtikel = JaargangArtikel::geef($jgid, $artid);
		if (!$jaargangArtikel || !$jaargangArtikel->magWijzigen())
		{
			return responseUitStatusCode(403);
		}

		$show_errors = false;
		if (Token::processNamedForm() == 'jaargangArtikelWijzigen')
		{
			JaargangArtikelView::processForm($jaargangArtikel);
			if ($jaargangArtikel->valid())
			{
				$jaargangArtikel->opslaan();
				return Page::responseRedirectMelding($jaargangArtikel->url(), _("Wijzigen gelukt!"));
			}

			$show_errors = true;
		}

		$page = JaargangArtikelView::wijzigPagina($jaargangArtikel, 'jaargangArtikelWijzigen', $show_errors);
		return $this->pageResponse($page);
	}

	/**
	 * HTML-endpoint om een JaargangArtikel te verwijderen.
	 *
	 * @site{/Onderwijs/Vak/ * /Jaargang/ * /Artikel/ * /Verwijder}
	 *
	 * @return Response
	 */
	public function jaargangArtikelVerwijderen()
	{
		// Merk op dat de indices in omgekeerde volgorde staan dan de URL.
		$varNames = vfsVarEntryNames();
		$jgid = $varNames[1];
		$artid = $varNames[0];

		$jaargangArtikel = JaargangArtikel::geef($jgid, $artid);
		if (!$jaargangArtikel || !$jaargangArtikel->magVerwijderen())
		{
			return responseUitStatusCode(403);
		}

		if (Token::processNamedForm('JaargangArtikelVerwijderen'))
		{
			// Redirect naar de Jaargang na het verwijderen, dus bekom nu alvast de URL.
			$jaargangURL = $jaargangArtikel->getJaargang()->url();

			$jaargangArtikel->verwijderen();
			return Page::responseRedirectMelding($jaargangURL,
				_("Het object is ontslagen door de databaas!")
			);
		}

		$page = new HTMLPage();
		$page->start();
		$page->add(JaargangArtikelView::verwijderForm($jaargangArtikel));
		return $this->pageResponse($page);
	}
}
