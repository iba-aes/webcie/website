<?php
abstract class Telling_Controller
{
	static public function doeTelling()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$artikelen = ArtikelVerzameling::geefAlleBoekwebArtikelen();

		if(Token::processNamedForm() == "Telling") {
			$voorraden = tryPar('voorraad', array());

			if(sizeof($voorraden) == 0) {
				Page::addMelding(_("Er zijn geen voorraden geteld jôh!"), 'fout');
			} else {
				$telling = new Telling(new DateTimeLocale(), Persoon::getIngelogd());
				$telling->setOpmerking(tryPar('opmerking'));
				$telling->opslaan();

				foreach($voorraden as $voorraadID => $aantal) {
					$voorraad = Voorraad::geef($voorraadID);

					$tellingItem = new TellingItem($telling, $voorraad);
					$tellingItem->setAantal($aantal);
					$tellingItem->opslaan();
				}

				Page::redirectMelding(BOEKWEBBASE, _("Teltje ingevoerd!"));
			}
		}

		TellingView::doeTelling($artikelen);
	}

	static public function tellingen()
	{
		$tellingen = TellingVerzameling::geefAlleTellingen();

		TellingVerzamelingView::tellingen($tellingen);
	}

	static public function tellingEntry($args)
	{
		$telling = Telling::geef($args[0]);

		if(!$telling)
			return false;

		return array( 'name' => $telling->geefID()
			, 'displayName' => _('Telling')
			, 'access' => True);
	}

	static public function telling()
	{
		$id = vfsVarEntryName();
		$telling = Telling::geef($id);

		if(!$id)
			spaceHttp(403);

		TellingView::telling($telling);
	}
}
