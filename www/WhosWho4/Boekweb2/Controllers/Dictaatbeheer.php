<?php
define('BOEKEN_UPLOADFORM_SUCCES', 0);
define('BOEKEN_UPLOADFORM_CONTACT', 1);
define('BOEKEN_UPLOADFORM_VAK', 2);
define('BOEKEN_UPLOADFORM_DICTAAT', 3);
define('BOEKEN_UPLOADFORM_VAKDICTAAT', 4);
define('BOEKEN_UPLOADFORM_UPLOAD', 5);

define('BOEKEN_WIJZIGWAARDEORDERFORM_ORDER', 1);

define('BOEKEN_LOSSELEVERINGFORM_LEVERING', 1);

define('BOEKEN_LEVERINGFORM_VOORRAAD', 1);
define('BOEKEN_LEVERINGFORM_LEVERING', 2);
define('BOEKEN_LEVERINGFORM_ORDER', 3);

define('BOEKEN_VERPLAATSFORM_VOORRAAD', 1);

abstract class Dictaatbeheer_Controller
{
	static public function uploaden ()
	{
		if(Token::processNamedForm() == 'DictaatUploaden')
		{
			$return = array();

			$email = tryPar('auteurEmail');
			$contact = Contact::zoekOpEmail($email);
			if (!$contact instanceof Contact)
			{
				$contact = new Persoon();

				if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $email))
					$return[] = _('Uw emailadres kon niet goed verwerkt worden.');
				else
				{
					$contact->setEmail($email)
						->setVoorletters(tryPar('auteurVoorletters'))
						->setAchternaam(tryPar('auteurNaam'));
					if (!$contact->valid())
					{
						$return[] = _('Er waren problemen met het verwerken van uw contactgegevens.');
					}
					else
					{
						if (is_null($contact->geefID()))
							$contact->opslaan();
					}
				}
			}
			else
				$contact = Persoon::geef($contact->geefID());

			if(sizeof($return) > 0)
				goto gotoLabel;

			$vakcode = str_replace(array(' ', '-'), '', strtoupper(tryPar('vakcode')));
			if (!($vak = Vak::geefDoorCode($vakcode)))
				$vak = new Vak();
			$vak->setCode($vakcode)
				->setDepartement(tryPar('departement'));
			if (!$vak->valid())
				$return[] = _('Er waren problemen met het verwerken van de vakgegevens.');

			if(sizeof($return) > 0)
				goto gotoLabel;

			$volgendePeriode = Periode::vanDatum()->volgende();
			$begindatum = $volgendePeriode->getDatumBegin();
			$einddatum = $volgendePeriode->getDatumEind();
			if (!($jaargang = Jaargang::geefDoorDatumBeginEnEind($begindatum, $einddatum, $vak)))
			{
				$jaargang = new Jaargang($vak);
				$jaargang->setDatumBegin($begindatum);
				$jaargang->setDatumEinde($einddatum);
			}
			if (!isset($datumeind))
				$jaargang->setContactPersoon($contact);
			if (!$jaargang->valid())
				$return[] = _('Er waren problemen met het verwerken van de vakgegevens.');

			if(sizeof($return) > 0)
				goto gotoLabel;

			$dictaat = new Dictaat();
			$dictaat->setNaam(tryPar('naam'))
				->setAuteur($contact)
				->setUitgaveJaar(tryPar('jaar'))
				->setDigitaalVerspreiden(tryPar('digitaalVerspreiden') == 'true')
				->setCopyrightOpmerking(tryPar('copyrightOpmerking'))
				->setBeginGebruik($begindatum)
				->setEindeGebruik($einddatum);
			if (!$dictaat->valid())
				$return[] = _('Er waren problemen met het verwerken van de dictaatgegevens.');

			if(sizeof($return) > 0)
				goto gotoLabel;

			$jaargangartikel = new JaargangArtikel($jaargang, $dictaat);
			$jaargangartikel->setSchattingNodig(tryPar('schatting'))
				->setOpmerking(tryPar('weggeefOpmerking'));
			if (!$jaargangartikel->valid())
				$return[] = _('Er waren problemen met het koppelen van het dictaat met het vak.');

			if (!is_uploaded_file($_FILES['dictaat']['tmp_name']))
				$return[] = _('Er is geen upload meegeleverd.');
			else
			{
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				if (finfo_file($finfo, $_FILES['dictaat']['tmp_name']) != 'application/pdf')
					$return[] = _('Het geüploade bestand is geen pdf.');
			}

			if (count($return) > 0)
			{
				gotoLabel:
				Page::addMeldingArray($return, 'fout');
			}
			else
			{
				$vak->opslaan();
				if (is_null($jaargang->geefID()))
					$jaargang->opslaan();

				$dictaat->opslaan();
				$jaargangartikel->opslaan();

				if (!DEBUG)
					move_uploaded_file($_FILES['dictaat']['tmp_name'], FILESYSTEM_PREFIX . $dictaat->geefAbsoluutPad());

				sendmail('www-data@a-eskwadraat.nl', 'boeken@a-eskwadraat.nl', 'Nieuw dictaat',
					'Beste Boekencommissaris,'."\n\n".
					'Er is een nieuw dictaat aangeleverd door '
					.PersoonView::naam($contact).' voor het vak '
					.VakView::waardeCode($vak).'.'."\n\n".
					(tryPar('opmerking',false) ?
					'Hierbij is de volgende opmerking gemaakt:'."\n".
					tryPar('opmerking')."\n\n" : '').
					'Met vriendelijke groeten,'."\n".
					'Het BoekenSysteem', 'boeken@a-eskwadraat.nl');

				Page::redirectMelding('/Onderwijs/dictaat.html', _("Het dictaat is succesvol ontvangen."));
			}
		}

		DictaatView::uploaden();
	}

	static public function lijst ()
	{
		$dictaten = DictaatVerzameling::alle();
		$dictaten->toAuteurVerzameling(); // Vul de cache alvast

		DictaatVerzamelingView::lijst($dictaten);
	}

	static public function DictaatEntry ($args)
	{
		$dictaat = Dictaat::geef($args[0]);
		if (!$dictaat) return false;

		return array('name' => $dictaat->geefID(),
			'access' => true);
	}

	static public function details ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);
		$dictaat->getJaargangArtikelRelaties()->toJaargangVerzameling()->toVakVerzameling(); // Vul de cache alvast

		DictaatView::details($dictaat);
	}

	/**
	 * @brief Download het pdf-bestand van het dictaat.
	 *
	 * @site{/Onderwijs/Boekweb/Artikel/ * /Dictaat/Bestand}
	 */
	static public function downloaden ()
	{
		global $filesystem;

		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		if(!$dictaat) {
			return spaceHttp(403);
		}

		$bestand = $dictaat->geefAbsoluutPad();

		if ($filesystem->has($bestand) &&
			($dictaat->getDigitaalVerspreiden() || hasAuth('bestuur')))
		{
			return sendfilefromfs($bestand, $id . ".pdf");
		}
		else
		{
			return DictaatView::bestandFout($dictaat);
		}
	}

	static public function plaatsOrder ()
	{
		requireAuth('bestuur');

		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		if(!$dictaat)
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'PlaatsOrder')
		{
			$order = DictaatView::dictaatOrderFormProcess($dictaat, $order);

			if ($order->valid())
			{
				$order->opslaan();

				Page::redirectMelding($dictaat->url().'/Dictaat/Orders',
					_("De order is geplaatst."));
			} else {
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = false;
			}
		} else {
			$order = new DictaatOrder($dictaat);
		}

		DictaatView::plaatsOrder($dictaat, $order, $show_error);
	}

	static public function orders ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		DictaatView::orders($dictaat);
	}

	static public function losseLevering ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		if(!$dictaat)
			spaceHttp(403);

		if(Token::processNamedForm() == 'nieuweLevering')
		{
			// Maak eerst een order aan.
			$order = DictaatView::dictaatOrderFormProcess($dictaat);

			$voorraad = Voorraad::zoek($dictaat, tryPar('locatie'), $order->getWaardePerStuk(), Register::getValue('bw2_dictaatBTW'));
			$beginDatum = new DateTimeLocale(tryPar('binnen_datum').' '.tryPar('binnen_tijd'));
			$leverancier = Leverancier::geef(tryPar('leverancier'));
			$levering = new Levering($voorraad, $beginDatum, $order->getAantal(), $order, $leverancier);

			// TODO: alle fouten die nuttig zijn goed weergeven
			if (!$order->valid()) {
				Page::addMelding(_('Er waren fouten.'), 'fout');
				Page::addMeldingArray($order->getErrors(), 'fout');
			} elseif (!$levering->valid()) {
				Page::addMelding(_('Er waren fouten.'), 'fout');
				Page::addMeldingArray($levering->getErrors(), 'fout');
			} else {
				$order->opslaan();
				$levering->opslaan();

				Page::redirectMelding($dictaat->url(),
					_("De levering is verwerkt."));
			}
		}

		DictaatView::nieuweLevering($dictaat, null);
	}


	static public function verkoop ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);
		$verkopen = $dictaat->getVerkoopRelaties();
		$verkopen->toTransactieVerzameling(); // vul de cache

		VerkoopVerzamelingView::verkoop($dictaat, $verkopen);
	}

	static public function OrderEntry ($args)
	{
		$order = Order::geef($args[0]);
		if (!$order) return false;

		return array('name' => $order->geefID(),
			'access' => true);
	}

	static public function annuleer ()
	{
		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$orderID = (int) $entryData[0];
		$order = Order::geef($orderID);

		$order->setGeannuleerd(!$order->getGeannuleerd())
			->opslaan();

		if ($order->getGeannuleerd())
			Page::redirectMelding($dictaat->url().'/Dictaat/Orders',
				_("De order is nu geannuleerd."));
		else
			Page::redirectMelding($dictaat->url().'/Dictaat/Orders',
				_("De order is nu niet langer geannuleerd."));
	}

	static public function wijzigWaardeOrder ()
	{
		if (tryPar('submit', false))
			$msg = self::wijzigWaardeOrderFormValidator();

		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$orderID = (int) $entryData[0];
		$order = Order::geef($orderID);

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Dictaatbeheer/wijzigwaarde.php");
		Page::getInstance()->end();
	}

	static protected function wijzigWaardeOrderFormValidator ()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$orderID = (int) $entryData[0];
		$order = Order::geef($orderID);

		$order->setWaarde(tryPar('waarde'));

		if (!$order->valid())
			$return[] = BOEKEN_WIJZIGWAARDEORDERFORM_ORDER;

		if (count($return) > 0)
			return $return;

		$order->opslaan();

		Page::redirectMelding($dictaat->url().'/Dictaat/Orders',
			_("De order is gewijzigd."));
	}

	static public function levering ()
	{
		if (tryPar('submit', false)) {
			$msg = self::leveringFormValidator();
		} else {
			$msg = array();
		}
		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$orderID = (int) $entryData[0];
		$order = Order::geef($orderID);

		DictaatView::nieuweLevering($dictaat, $order, $msg);
	}

	/**
	 *  Verwek het "er is een levering binnen"-formulier.
	 * Indien succesvol, redirect naar de Artikelpagina.
	 * Indien iets foutging, zie de returnwaarde.
	 * @return Een array(veld => foutmelding of false).
	 */
	static protected function leveringFormValidator ()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$orderID = (int) $entryData[0];
		$order = Order::geef($orderID);

		$omschrijving = tryPar('omschrijving');
		if (empty($omschrijving))
			$omschrijving = tryPar('omschrijving_nieuw');

		$voorraad = Voorraad::zoek($dictaat, tryPar('locatie'), $order->getWaardePerStuk(), Register::getValue('bw2_dictaatBTW'));
		$beginDatum = new DateTimeLocale(tryPar('binnen_datum').' '.tryPar('binnen_tijd'));

		$deelOrder = null;
		if (tryPar('aantal') < $order->getAantal())
		{
			$deelOrder = new Order($dictaat);
			$deelOrder->setDatumGeplaatst($order->getDatumGeplaatst())
				->setLeverancier($order->getLeverancier())
				->setAantal(tryPar('aantal'))
				->setDatumBinnen(new DateTimeLocale());

			$order->setAantal($order->getAantal() - tryPar('aantal'));
		}

		$levering = new Levering($voorraad, $beginDatum, tryPar('aantal'),
			((is_null($deelOrder)) ? $order : $deelOrder));

		if (!is_null($deelOrder)) {
			if (!$deelOrder->valid()) {
				return $deelOrder->getErrors();
			}
		}
		if (!$order->valid()) {
			return $order->getErrors();
		}
		if (!$levering->valid()) {
			return $levering->getErrors();
		}

		$order->opslaan();

		if (!is_null($deelOrder))
			$deelOrder->opslaan();

		$levering->opslaan();

		Page::redirectMelding($dictaat->url(),
			_("De levering is verwerkt."));
	}

	static public function voorraad ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		DictaatView::voorraden($dictaat);
	}

	static public function voorraadMutaties ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);
		$mutaties = $dictaat->getVoorraadMutatieRelaties();
		$mutaties->toVoorraadVerzameling(); // vul de cache

		VoorraadMutatieVerzamelingView::dictaatMutaties($dictaat);
	}

	static public function voorraadEntry ($args)
	{
		$voorraad = Voorraad::geef($args[0]);
		if (!$voorraad) return false;

		return array('name' => $voorraad->geefID(),
			'access' => true);
	}

	static public function voorraadDetails ()
	{
		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		VoorraadView::details($voorraad, $dictaat);
	}

	static public function voorraadVerplaatsen ()
	{
		if (tryPar('submit', false))
			$msg = self::voorraadVerplaatsFormValidator();

		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		VoorraadView::verplaatsVoorraad($voorraad, $dictaat);
	}

	static protected function voorraadVerplaatsFormValidator ()
	{
		$return = array();

		$entryData = vfsVarEntryNames();

		$dictaatID = (int) $entryData[1];
		$dictaat = Dictaat::geef($dictaatID);

		$voorraadID = (int) $entryData[0];
		$voorraad = Voorraad::geef($voorraadID);

		$tegenVoorraad = Voorraad::zoek($dictaat, tryPar('locatie'));

		$mutatie = new Verplaatsing($tegenVoorraad, new DateTimeLocale(),
			tryPar('aantal'), $voorraad);

		if (!$mutatie->valid())
			$return[] = BOEKEN_VERPLAATSFORM_VOORRAAD;

		if (count($return) > 0)
			return $return;

		$mutatie->opslaan();

		Page::redirectMelding($dictaat->url().'/Voorraad/',
			_("De voorraad is verplaatst."));

		return;
	}

	static public function veranderBestand ()
	{
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		if(!$dictaat)
			spaceHttp(403);

		if(Token::processNamedForm() == 'veranderBestand')
		{
			$return = array();

			$id = (int) vfsVarEntryName();
			$dictaat = Dictaat::geef($id);

			if (!is_uploaded_file($_FILES['dictaat']['tmp_name']))
				$return[] = _('Er is geen bestand meegestuurd');
			else
			{
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				if (finfo_file($finfo, $_FILES['dictaat']['tmp_name']) != 'application/pdf')
					$return[] = _('Het meegestuurde bestand is geen pdf');
			}

			if (count($return) > 0)
			{
				Page::addMeldingArray($return, 'fout');
			}
			else
			{
				if (!DEBUG)
				{
					unlink(FILESYSTEM_PREFIX . $dictaat->geefAbsoluutPad());
					move_uploaded_file($_FILES['dictaat']['tmp_name'], FILESYSTEM_PREFIX . $dictaat->geefAbsoluutPad());
				}

				sendmail('www-data@a-eskwadraat.nl', 'boeken@a-eskwadraat.nl', 'Dictaat bestandsverandering',
					'Beste Boekencommissaris,'."\n\n".
					'Voor dictaat "'.DictaatView::waardeNaam($dictaat).'" is het bijbehorende pdf-bestand veranderd.'."\n\n".
					'Natuurlijk kan je het oude bestand altijd nog terug vinden in de backups van sysop.'."\n\n".
					'Met vriendelijke groeten,'."\n".
					'Het BoekenSysteem', 'boeken@a-eskwadraat.nl');

				Page::redirectMelding($dictaat->url(),
					_("Het bestand is succesvol veranderd.").
					((DEBUG)?
					' ' . _('Houd er rekening mee dat je op een debug omgeving '
					. 'zit, hier werkt bestanden uploaden niet.')
					:''));
			}
		}

		DictaatView::veranderBestand($dictaat);
	}
}
