<?php

abstract class Levering_Controller {
	static public function leveringOverzicht()
	{
		global $WSW4DB; // FIXME: zet dit in leveringverzameling ofzo

		// we checken in het begin of we een csv-bestand moeten uitpoepen,
		// want anders zijn we al een pagina in elkaar aan het zetten
		$csvEditie = (bool)tryPar("submitCSV", false);

		// formuliertje om te kiezen over welke periodes je het overzicht wil
		$curDate = new DateTimeLocale(); // Temp om begin/einddatum bij te houden.
		$begindatum = tryPar("begindatum", $curDate->strftime("%Y-%m-01")); // 1ste van de maand
		$curDate = new DateTimeLocale($begindatum);
		$curDate->add(new DateInterval("P1M"));
		$einddatum = tryPar("einddatum", $curDate->strftime("%Y-%m-01")); // 1ste van de volgende maand
		$soort = tryPar('soort', -1);

		$extensies = Artikel::geefExtensies();

		if (!$csvEditie) {
			$page = Page::getInstance()->start(_("Overzicht van leveringen"));

			$page->add($form = new HtmlForm("GET")); // we doen GET want het mag in de geschiedenis blijven
			$form->addClass('form-inline');
			$form->add(new HtmlLabel("begindatum", _("Begindatum:")));
			$form->add(HtmlInput::makeDate("begindatum", $begindatum, null, "begindatum"));
			$form->add(new HtmlLabel("einddatum", _("einddatum:")));
			$form->add(HtmlInput::makeDate("einddatum", $einddatum, null, "einddatum"));
			// We geven hier de index van NULL -1, omdat met een index van NULL de geselecteerde optie NULL
			// niet bij NULL kwam maar bij index 0
			$form->add(HtmlSelectbox::fromArray('soort', array(-1 => null) + $extensies, array(tryPar('soort', -1))));
			$form->add(HtmlInput::makeSubmitButton(_("Toon op het scherm"), "submitHTML"));

			// formuliertje om dit uit te poepen in csv-vorm
			$form->add(HtmlInput::makeSubmitButton(_("Poep een CSV-bestand uit"), "submitCSV"));
		}

		// overzichtje van alle leveringen in die periodes
		/* // TODO: als de querybuilder bestaat, doe het dan gewoon zo:
		$leveringen = LeveringQuery::table()
			->whereProp("wanneer", ">=", new DateTimeLocale($begindatum))
			->whereProp("wanneer", "<", new DateTimeLocale($einddatum))
			->verzamel();
		*/
		$leveringIDs = $WSW4DB->q("COLUMN SELECT `VoorraadMutatie`.`voorraadMutatieID` "
			. "FROM `VoorraadMutatie` "
			. "LEFT JOIN `Voorraad` ON `Voorraad`.`voorraadID` = `VoorraadMutatie`.`voorraad_voorraadID` "
			. "LEFT JOIN `Artikel` ON `Artikel`.`artikelID` = `Voorraad`.`artikel_artikelID` "
			. "WHERE `VoorraadMutatie`.`overerving` = 'Levering' "
			. ($soort != -1 ? "AND `Artikel`.`overerving` = %s " : "%_ ")
			. "AND `VoorraadMutatie`.`wanneer` >= %s "
			. "AND `VoorraadMutatie`.`wanneer` < %s "
			, ($soort == -1) ? -1 : $extensies[$soort]
			, $begindatum
			, $einddatum
		);
		$leveringen = LeveringVerzameling::verzamel($leveringIDs);
		$leveringen->sorteer("wanneer", true);
		
		if ($csvEditie) {
			header("Content-Disposition: attachment; filename=leveringen.csv");
			header("Content-type: text/csv; encoding=utf-8");
			$file = fopen('php://output', 'r+');
			LeveringVerzamelingView::toCSV($leveringen, $file);
			fclose($file);
			exit();
		} else {
			$page->add(LeveringVerzamelingView::toTable($leveringen));
			$page->end();
		}
	}
}
