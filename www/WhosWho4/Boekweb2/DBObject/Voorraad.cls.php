<?
/**
 * $Id$
 */
class Voorraad
	extends Voorraad_Generated
{
	/**
	 * @brief Maak een Voorraad-object aan op basis van wat parameters.
	 *
	 * @param artikel Het artikel waar de voorraad bijhoort.
	 * Mag een Artikel-object zijn, een artikelid, of een array met $artikel[0] als artikelid.
	 *
	 * @param locatie De huidige locatie van de voorraad (optioneel).
	 * @param waarde De huidige waarde van de voorraad (optioneel).
	 * @param btw De BTW-categorie van de voorraad (optioneel).
	 **/
	public function __construct($artikel, $locatie = null, $waarde = NULL, $btw = NULL)
	{
		parent::__construct($artikel); // Voorraad_Generated
		$this->setLocatie($locatie);
		$this->setWaardePerStuk($waarde);
		$this->setBtw($btw);
	}

	public function checkVerwijderen()
	{
		$rel = $this->getVoorraadMutatieRelaties();
		if($rel->aantal() != 0)
			return _("Deze voorraad heeft mutaties!");
		return false;
	}

	public function magVerwijderen()
	{
		if(!hasAuth('bestuur'))
			return false;
		$rel = $this->getVoorraadMutatieRelaties();
		if($rel->aantal() != 0)
			return false;
		return true;
	}

	static public function waardeAantalArtikel (Artikel $artikel, $locatie)
	{
		return VoorraadMutatieQuery::table()
			->join('Voorraad')
			->whereProp('Artikel', $artikel)
			->whereProp('locatie', $locatie)
			->select('SUM(VoorraadMutatie.aantal)')
			->setFetchType('MAYBEVALUE')
			->get();
	}

	/**
	 *  Vind het Voorraadobject met alle gegeven eigenschappen.
	 *
	 * Indien niet gevonden, construeer een nieuwe met precies die eigenschappen.
	 * Eigenschappen zijn dus de argumenten van de Voorraad-constructor.
	 * @see Voorraad.__construct
	 *
	 * @param artikel Het artikel dat op voorraad is.
	 * @param locatie Waar de voorraad te vinden is.
	 * @param waardePerStuk Hoeveel een artikel in de voorraad kost.
	 * @param btw De btw-code van de artikels op voorraad.
	 *
	 * @return De gewenste voorraad of een nieuwe als die nog niet bestond.
	 */
	static public function zoek (Artikel $artikel, $locatie, $waardePerStuk, $btw)
	{
		$id = VoorraadQuery::table()
			->whereProp('Artikel', $artikel)
			->whereProp('locatie', $locatie)
			->whereProp('waardePerStuk', $waardePerStuk)
			->whereProp('btw', $btw)
			->setFetchType('MAYBEVALUE')
			->select('voorraadID')
			->get();

		if (!is_null($id))
			return Voorraad::geef($id);

		return new Voorraad($artikel, $locatie, $waardePerStuk, $btw);
	}

	public function url()
	{
		return $this->getArtikel()->url() . '/Voorraad/' . $this->geefID();
	}

	public function isVirtueel ()
	{
		return $this->getLocatie() == 'VIRTUEEL';
	}

	public function getAantal ()
	{
		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $this)
			->select('SUM(aantal)')
			->setFetchType('MAYBEVALUE')
			->get();
	}

	public function getEersteMutatie ()
	{
		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $this)
			->select('wanneer')
			->setFetchType('MAYBEVALUE')
			->orderByAsc('wanneer')
			->limit(1)
			->get();
	}

	public function getLaatsteMutatie ()
	{
		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $this)
			->select('wanneer')
			->setFetchType('MAYBEVALUE')
			->orderByDesc('wanneer')
			->limit(1)
			->get();
	}

	public function getVoorraadMutatieRelaties ()
	{
		return VoorraadMutatieVerzameling::vanVoorraad($this);
	}

	public function getVerkoopPrijs()
	{
		$res = VerkoopPrijsQuery::table()
			->whereProp('Voorraad', $this)
			->select('voorraad_voorraadID', 'wanneer')
			->setFetchType('MAYBETUPLE')
			->orderByDesc('wanneer')
			->limit(1)
			->get();

		if($res)
			return VerkoopPrijs::geef($res[0], $res[1]);
		else
			return new VerkoopPrijs($this, new DateTimeLocale());
	}

	public function getBarcodes ()
	{
		return BarcodeQuery::table()
			->whereProp('Voorraad', $this)
			->verzamel();
	}

	public function kaartjeUrl()
	{
		$artikel = $this->getArtikel();
		if($artikel instanceof iDealKaartje) {
			$act = $artikel->getActiviteit();
			return $act->url() . "/KaartjeVariant/" . $this->geefId();
		}
	}

	public function getErrors($welkeVelden = 'set')
	{
		$this->errors['locatie'] = $this->checkLocatie();
		$this->errors['btw'] = $this->checkBtw();
		$this->errors['waardePerStuk'] = $this->checkWaardePerStuk();
		$this->errors['verkoopPrijs'] = $this->checkVerkoopPrijs();
		return parent::getErrors($welkeVelden);
	}

	public function checkLocatie()
	{
		if(is_null($this->getLocatie()))
			return _("Dit is een verplicht veld");
		if(!in_array($this->getLocatie(), Voorraad::enumsLocatie()))
			return _("De locatie bestaat niet");
		return false;
	}

	public function checkBtw()
	{
		if(is_null($this->getBtw()))
			return _("Dit is een verplicht veld");
		if(!in_array($this->getBtw(), Voorraad::enumsBtw()))
			return _("De btw-code bestaat niet");
		return false;
	}

	public function checkWaardePerStuk()
	{
		if(is_null($this->getWaardePerStuk()))
			return _("Dit is een verplicht veld");
		if($this->getWaardePerStuk() < 0)
			return _("Dit veld moet positief zijn");
		return false;
	}

	public function checkVerkoopPrijs()
	{
		if(is_null($this->getVerkoopPrijs()->getPrijs()))
			return _("Dit is een verplicht veld");
		if($this->getVerkoopPrijs()->getPrijs() < 0)
			return _("Dit veld moet positief zijn");
		return false;
	}

	public function checkVerkoopbaar()
	{
		if (is_null($this->getArtikel())) {
			return _('voorraad zonder artikel kun je niet verkopen');
		}
		if ($this->getVerkoopbaar() && $this->getArtikel()->verkopenVerboden()) {
			return $this->getArtikel()->verkopenVerboden();
		}
		return parent::checkVerkoopbaar();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
