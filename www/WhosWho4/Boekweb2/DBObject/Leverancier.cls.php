<?
/**
 * $Id$
 */
class Leverancier
	extends Leverancier_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Leverancier_Generated

		$this->contactID = NULL;
	}

	public function magVerwijderen()
	{
		if(hasAuth('bestuur'))
			return true;
		return false;
	}

	public function checkVerwijderen()
	{
		if($error = parent::checkVerwijderen())
			return $error;
		if($this->getAantalLeveringen() != 0)
			return _('Er zijn leveringen bij deze leverancier');
		return false;
	}

	public function url ()
	{
		return BOEKWEBBASE . 'Leverancier/' . $this->geefID();
	}

	public function getAantalLeveringen ()
	{
		return LeveringQuery::table()
			->whereProp('Leverancier', $this)
			->count();
	}

	public function getEersteLevering ()
	{
		return LeveringQuery::table()
			->join('VoorraadMutatie')
			->whereProp('Leverancier', $this)
			->orderByAsc('VoorraadMutatie.wanneer')
			->geef();
	}

	public function getLaatsteLevering ()
	{
		return LeveringQuery::table()
			->join('VoorraadMutatie')
			->whereProp('Leverancier', $this)
			->orderByDesc('VoorraadMutatie.wanneer')
			->geef();
	}

	public function getOpenOrders ()
	{
		return OrderQuery::table()
			->whereProp('Leverancier', $this)
			->wherePropNull('datumBinnen')
			->whereProp('geannuleerd', 0)
			->verzamel();
	}

	public function getGeslotenOrders ()
	{
		return OrderQuery::table()
			->whereProp('Leverancier', $this)
			->wherePropNotNull('datumBinnen')
			->whereProp('geannuleerd', 0)
			->verzamel();
	}

	public function getGeannuleerdOrders ()
	{
		return OrderQuery::table()
			->whereProp('Leverancier', $this)
			->whereProp('Geannuleerd', 1)
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
