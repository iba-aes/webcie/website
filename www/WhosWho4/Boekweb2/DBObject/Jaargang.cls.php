<?php
declare(strict_types=1);

class Jaargang
	extends Jaargang_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($vak = null)
	{
		parent::__construct($vak); // Jaargang_Generated
	}

	public static function geefDoorDatumBeginEnEind ($begin, $eind, Vak $vak)
	{
		return JaargangQuery::table()
			->whereProp('datumBegin', new DateTimeLocale($begin))
			->whereProp('datumEinde', new DateTimeLocale($eind))
			->whereProp('Vak', $vak)
			->geef();
	}

	/**
	 * @inheritdoc
	 */
	public static function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @inheritdoc
	 */
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @inheritdoc
	 */
	public function magWijzigen()
	{
		return hasAuth('bestuur');
	}

	/**
	 * @inheritdoc
	 */
	public function url()
	{
		return '/Onderwijs/Vak/' . $this->getVak()->geefID() . '/Jaargang/' . $this->geefID();
	}
}
