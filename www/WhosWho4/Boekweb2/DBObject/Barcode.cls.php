<?
/**
 * $Id$
 */
class Barcode
	extends Barcode_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct(Voorraad $voorraad = null, $barcode = null, $wanneer = null)
	{
		parent::__construct($voorraad, $barcode, $wanneer); // Barcode_Generated
	}

	public static function getByBarcode($barcode)
	{
		return BarcodeQuery::table()
			->whereProp('barcode', $barcode)
			->geef();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
