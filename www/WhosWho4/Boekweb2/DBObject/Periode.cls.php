<?
class Periode
	extends Periode_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Periode_Generated

		$this->periodeID = NULL;
	}

	/**
	 * \brief Geef de zoveelste periode van het collegejaar.
	 *
	 * \param coljaar Indien NULL, wordt het dit jaar. Anders het collegejaar waarvan de periode nodig is.
	 * \param nummer Het nummer van de periode binnen het collegejaar. Indien NULL, de eerste periode binnen het collegejaar.
	 *
	 * \returns De gevraagde periode of NULL indien die niet bestaat.
	 */
	public static function vanColjaar($coljaar = NULL, $nummer = NULL) {
		if (is_null($coljaar)) {
			$coljaar = coljaar();
		}

		$query = PeriodeQuery::table()
			->whereProp("collegejaar", "=", $coljaar);

		// Bepaal of we de eerste of de juiste willen.
		if (is_null($nummer)) {
			$query->orderByAsc("periodeNummer")->limit(1);
		} else {
			$query->whereProp("periodeNummer", "=", $nummer);
		}

		return $query->geef();
	}

	/**
	 * \brief Bepaal de periode waarin een bepaalde datum zich bevindt.
	 *
	 * \param datum Indien NULL, wordt het vandaag. Anders de datum waarvan de periode nodig is.
	 *
	 * \sa vanColjaar
	 *
	 * \returns De gevraagde periode of NULL indien die niet bestaat.
	 */
	public static function vanDatum($datum = NULL) {
		if (is_null($datum)) {
			$datum = new DateTimeLocale();
		}

		$query = PeriodeQuery::table()
			->whereProp("datumBegin", "<", $datum)
			->orderByDesc("datumBegin")
			->limit(1);
		return $query->geef();
	}

	/**
	 * \brief Geef de eerste dag van de periode na de gegeven periode.
	 *
	 * \sa volgende
	 *
	 * \returns Een DateTimeLocale-object of NULL indien er geen volgende periode bestaat.
	 */
	public function getDatumEind() {
		$volgende = $this->volgende();
		if ($volgende) {
			return $volgende->getDatumBegin();
		} else {
			// hee, dat is raar, er is geen volgende periode gedefinieerd
			return NULL;
		}
	}

	/**
	 * \brief Geef de periode die begint na deze.
	 *
	 * \returns Een Periode-object of NULL indien er geen volgende periode bestaat.
	 */
	public function volgende() {
		$ditColjaar = self::vanColjaar($this->getCollegejaar(), $this->getPeriodeNummer() + 1);
		if (!is_null($ditColjaar)) {
			return $ditColjaar;
		}
		$volgendColjaar = self::vanColjaar($this->getCollegejaar() + 1);
		if (!is_null($volgendColjaar)) {
			return $volgendColjaar;
		}

		return NULL;
	}

	/**
	 * @brief Bevat deze periode nog een dag in de toekomst?
	 * De huidige periode is dus ook toekomstig.
	 */
	public function isToekomstig() {
		return $this->getDatumEind() > new DateTimeLocale();
	}

	/**
	 * @brief Mag de huidige gebruiker deze periode verwijderen?
	 * @returns Een bool of je het mag verwijderen.
	 */
	public function magVerwijderen() {
		// TODO: denk even na wanneer dit geoorloofd is,
		// bijvoorbeeld niet als de periode al bezig is/gekoppeld aan een vak
		return hasAuth('bestuur');
	}
}
