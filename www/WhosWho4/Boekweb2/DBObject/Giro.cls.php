<?
/**
 * $Id$
 */
class Giro
	extends Giro_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($rubriek, $soort, $contact = null, $bedrag, $uitleg = null, $wanneer = null, $rekening = null, $tegenrekening = null)
	{
		parent::__construct ($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer);

		$this->transactieID = NULL;
		$this->rekening = $rekening;
		$this->tegenrekening = $tegenrekening;

		$this->setRekening($rekening);
		$this->setTegenrekening($tegenrekening);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
