<?
/**
 * $Id$
 */
class Dictaat
	extends Dictaat_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Dictaat_Generated

		$this->dictaatID = NULL;
	}

	public function url ()
	{
		return BOEKWEBBASE . 'Artikel/' . $this->geefID();
	}

	public function downloadUrl()
	{
		return $this->url() . '/Dictaat/Downloaden';
	}

	public function checkVerwijderen()
	{
		global $filesystem;
		$res = parent::checkVerwijderen();
		if(!$res) {
			if($filesystem->has($this->geefAbsoluutPad())) {
				$res = _("Aan dit dictaat is een bestaand bestand gekoppeld.");
			}
		}
		return $res;
	}

	public function magVerwijderen()
	{
		global $filesystem;
		$res = parent::magVerwijderen();
		if($filesystem->has($this->geefAbsoluutPad()))
			$res = false;
		return $res;
	}

	/**
	 * @brief Geef het pad naar dit dictaat.
	 *
	 * Het pad is absoluut in de zin dat er nog FILESYSTEM_PREFIX voor moet,
	 * wat de globale $filesystem ook al automatisch doet.
	 *
	 * Vereist dat het dictaat al in de database staat.
	 *
	 * @returns Een string, of NULL als het dictaat niet in de db staat.
	 */
	public function geefAbsoluutPad ()
	{
		if (is_null($this->geefID())) {
			return null;
		}
		return DICTATEN_DIR . $this->geefID() . '.pdf';
	}

	/**
	 *  Maak de nieuwe order aan die hoort bij dit soort Artikels.
	 * Override omdat we in dit geval een DictaatOrder nodig hebben.
	 * @param leverancier De leverancier van deze order, of NULL.
	 * @returns Een instance van Order of een subklasse daarvan.
	 */
	public function nieuweOrder($leverancier)
	{
		return new DictaatOrder($this, $leverancier);
	}
}
