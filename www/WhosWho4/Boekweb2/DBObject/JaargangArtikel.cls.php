<?php
declare(strict_types=1);

class JaargangArtikel
	extends JaargangArtikel_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a,$b); // JaargangArtikel_Generated
	}

	/**
	 * @inheritdoc
	 */
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenJaargangArtikel($welke = NULL)
	{
		// Neem de parentvelden over...
		$velden = parent::veldenJaargangArtikel($welke);

		// ... en voeg extra velden toe.
		switch ($welke)
		{
			case 'viewInfo':
				array_unshift($velden, 'Artikel');
				array_unshift($velden, 'Jaargang');
				break;
		}

		return $velden;
	}

	/**
	 * @inheritdoc
	 */
	public function url()
	{
		return $this->getJaargang()->url() . '/Artikel/' . $this->getArtikel()->geefID();
	}
}
