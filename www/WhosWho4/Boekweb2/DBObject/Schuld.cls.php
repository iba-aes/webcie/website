<?
/**
 * $Id$
 */
class Schuld
	extends Schuld_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer)
	{
		parent::__construct($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer); // Schuld_Generated

		$this->transactieID = NULL;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
