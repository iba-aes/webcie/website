<?
class VakStudie
	extends VakStudie_Generated
{
	/*** CONSTRUCTOR ***/
	/**
	 * \param a Vak ( Vak OR Array(vak_vakID) OR vak_vakID )
	 * \param b Studie ( Studie OR Array(studie_studieID) OR studie_studieID )
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct($a, $b); // VakStudie_Generated
	}

	public function magVerwijderen()
	{
		return hasAuth('tbc');
	}
}
