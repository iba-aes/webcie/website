<?
class Boekverkoop
	extends Boekverkoop_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b = NULL, $c = 1)
	{
		parent::__construct($a, $b, $c); // Boekverkoop_Generated
	}

	static public function geefLaatsteBoekverkoop($date = null)
	{
		return BoekverkoopQuery::table()
			->whereProp('verkoopdag', new DateTimeLocale($date))
			->orderByDesc('volgnummer')
			->geef();
	}

	public function url()
	{
		return BOEKWEBBASE . "Verkoop/" . $this->getVerkoopDag();
	}
}
