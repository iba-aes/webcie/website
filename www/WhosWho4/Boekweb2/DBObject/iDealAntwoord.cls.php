<?
class iDealAntwoord
	extends iDealAntwoord_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // iDealAntwoord_Generated
	}

	public function setAntwoord($waarde)
	{
		parent::setAntwoord($waarde);

		// Override het geval dat het antwoord op 'null' geset wordt.
		// Dit zorgt ervoor dat we geen errors van MySQL krijgen.
		if (is_null($this->antwoord))
		{
			$this->antwoord = '';
			$this->gewijzigd();
		}
		return $this;
	}

	public function valid ($welkeVelden = 'set')
	{
		$bool = parent::valid($welkeVelden);

		if($this->getVraag()->getType() == 'ENUM'
			&& !$this->getAntwoord() && $this->getVraag()->getVerplicht())
		{
			$this->errors['Antwoord'] = _('Selecteer een optie');
			$bool = false;
		}

		return $bool;
	}

	public function checkAntwoord()
	{
		// Merk op dat we niet de parent::checkAntwoord aanroepen!
		// Dit is omdat die het altijd verplicht maakt.

		if ($this->getAntwoord() == null && $this->getVraag()->getVerplicht())
		{
			return _('Dit veld is verplicht');
		}

		return false;
	}
}
