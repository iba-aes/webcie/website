<?
/**
 * $Id$
 */
class Transactie
	extends Transactie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($rubriek = 'BOEKWEB', $soort = 'VERKOOP', $contact = null, $bedrag = 0, $uitleg = null, $wanneer = null)
	{
		parent::__construct($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer); // Transactie_Generated
	}

	public static function isStatusGeslaagd($status) {
		return $status == 'SUCCESS';
	}

	public static function isStatusGefaald($status) {
		return in_array($status, ['FAILURE', 'CANCELLED', 'EXPIRED']);
	}

	public function getVerkopen ()
	{
		return VerkoopVerzameling::vanTransactie($this);
	}

	public function url()
	{
		return BOEKWEBBASE . "Transactie/" . $this->geefID();
	}
}
