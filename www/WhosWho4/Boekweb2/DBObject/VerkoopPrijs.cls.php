<?
class VerkoopPrijs
	extends VerkoopPrijs_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($voorraad = null, $wanneer = null, $prijs = 0)
	{
		parent::__construct($voorraad, $wanneer); // VerkoopPrijs_Generated

		$this->prijs = (float)$prijs;
	}

	public function magVerwijderen()
	{
		if(!hasAuth('bestuur'))
			return false;
		return true;
	}

	public function checkPrijs()
	{
		if($this->getPrijs() <= 0)
			return _("Prijs moet groter dan 0 zijn");
		return false;
	}

	public function getErrors($welkeVelden = 'set')
	{
		$this->errors['prijs'] = $this->checkPrijs();
		return parent::getErrors($welkeVelden);
	}
}
