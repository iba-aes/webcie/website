<?
abstract class TellingView
	extends TellingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function doeTelling($artikelen)
	{
		$page = Page::getInstance()->start(_("Doe telling"));

		$page->add($form = HtmlForm::named("Telling"));
		$form->addClass('form-inline');

		$form->add($table = new HtmlTable(NULL, 'table-condensed sortable'));

		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_("Artikel"));
		$row->addHeader(_("Auteur/Cie"));
		$row->addHeader(_("In Databanaan"));
		$row->addHeader(_("Geteld"));

		foreach($artikelen as $artikel)
		{
			$voorraden = VoorraadVerzameling::voorradenInBoekenhok($artikel);

			foreach($voorraden as $voorraad)
			{
				if($voorraad->getAantal() == 0)
					continue;
				$tbody->add($row = new HtmlTableRow());
				$row->addData(ArtikelView::waardeNaam($artikel)
					. " " . sprintf(_("(IN %s)"), VoorraadView::waardeLocatie($voorraad)));

				if(get_class($artikel) == "Boek")
					$row->addData(BoekView::waardeAuteur($artikel));
				else if(get_class($artikel) == "AesArtikel")
					$row->addData(AesArtikelView::waardeCommissie($artikel));
				else if(get_class($artikel) == "Dictaat")
					$row->addData(DictaatView::waardeAuteur($artikel));
				else
					$row->addData();

				$row->addData($voorraad->getAantal());
				$row->addData(HtmlInput::makeNumber('voorraad[' . $voorraad->geefID() . ']', $voorraad->getAantal()));
			}
		}

		$form->add(new HtmlTextarea('opmerking', 8, 10));

		$form->add(HtmlInput::makeSubmitButton(_("Voer teltje in")));

		$page->end();
	}

	static public function telling(Telling $telling)
	{
		$items = $telling->geefItems();

		$page = Page::getInstance()->start(sprintf('%s %s', _('Telling'), $telling->geefID()));

		$page->add($list = new HtmlList());
		$list->addChild(sprintf('%s: %s', new HtmlStrong(_('Teller')), TellingView::waardeTeller($telling)));
		$list->addChild(sprintf('%s: %s', new HtmlStrong(_('Telmoment')), TellingView::waardeWanneer($telling)));

		$page->add($table = new HtmlTable());
		$head = $table->addHead();
		$body = $table->addBody();

		$row = $head->addRow();
		$row->makeHeaderFromArray(array(_('Artikel'), _('Locatie'), _('Geteld'), _('Op dat moment in voorraad')));

		foreach($items as $item)
		{
			$row = $body->addRow();

			$row->addData(ArtikelView::detailsLink($item->getVoorraad()->getArtikel()));
			$row->addData(VoorraadView::waardeLocatie($item->getVoorraad()));
			$row->addData(TellingItemView::waardeAantal($item));
			$row->addData(/*TODO op dat moment in voorraad */);
		}

		$page->end();
	}

	static public function waardeLink(Telling $telling)
	{
		return new HtmlAnchor($telling->url(), $telling->geefID());
	}
}
