<?
abstract class BestellingVerzamelingView
	extends BestellingVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * Maak een tabelletje met alle gegevens van de gegeven verzameling
	 */
	public static function tabel($bestellingen) {
		$table = new HtmlTable();
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add(BestellingView::tableHeaderRow());

		foreach ($bestellingen as $bestelling) {
			$tbody->add(BestellingView::tableRow($bestelling));
		}

		return $table;
	}

	/**
	 * Maak een pagina met een tabel van de gegeven bestellingen
	 */
	public static function BestellingOverzicht($bestellingen) {
		$page = Page::getInstance();

		$page->start("Bestellingoverzicht");

		$page->add(self::tabel($bestellingen));

		$page->end();
	}
}
