<?
/**
 * $Id$
 */
abstract class VoorraadVerzamelingView
	extends VoorraadVerzamelingView_Generated
{
	/**
	 * @brief Geef een overzichtspagina van alle voorraden in de verzameling.
	 *
	 * Maakt ook een link aan naar een CSV-vormig overzicht (inden overzichtCSV niet falsy is).
	 *
	 * @param maand De huidig geselecteerde maand.
	 * @param jaar Het huidig geselecteerde jaar.
	 * @param overzichtCSV Indien truthy, pagina die dit overzicht als CSV geeft.
	 * (Default is truthy en gelijk aan 'OverzichtCSV'.)
	 *
	 * @return Een HTMLForm om in je pagina toe te voegen.
	 */
	public static function overzichtForm($maand, $jaar, $overzichtCSV = 'OverzichtCSV')
	{
		global $MAANDEN;
		$jaren = array();
		foreach (range(DIBS_STARTJAAR, date('Y')) as $j)
		{
			$jaren[$j] = "$j";
		}

		$form = new HtmlForm('get');

		$maandselect = HtmlSelectbox::fromArray('maand', $MAANDEN, $maand, 1, true)
			->setMultiple(false);
		$jaarselect = HtmlSelectbox::fromArray('jaar', $jaren, $jaar, 1, true)
			->setMultiple(false);

		$form->add($maandselect)
			->add($jaarselect);
		if ($overzichtCSV) {
			$form->add(new HtmlAnchor("OverzichtCSV?maand=$maand&jaar=$jaar", 'csv'));
		}
		return $form;
	}

	/**
	 * @brief Geef een overzichtspagina van alle voorraden in de verzameling.
	 *
	 * Maakt ook een link aan naar een CSV-vormig overzicht (inden overzichtCSV niet falsy is).
	 *
	 * @param voorraden De VoorraadVerzameling om te vertonen.
	 * @param maand De maand waarvan we de mutaties willen.
	 * @param jaar Het jaar waarvan we de mutaties willen.
	 * @param overzichtCSV Indien truthy, pagina die dit overzicht als CSV geeft.
	 * (Default is truthy en gelijk aan 'OverzichtCSV'.)
	 *
	 * @return HTMLPage Een HTMLPage om in je PageResponse te stoppen.
	 */
	public static function overzicht (VoorraadVerzameling $voorraden, $maand, $jaar, $overzichtCSV = 'OverzichtCSV')
	{
		global $MAANDEN;

		$body = new HtmlDiv();

		$topdiv = new HtmlDiv(static::overzichtForm($maand, $jaar, $overzichtCSV));

		$van = new DateTimeLocale("$jaar-$maand-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1M"));
		$tablediv = new HtmlDiv($table = new HtmlTable(null, 'sortable'));
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_('Artikel'));
		$row->addHeader(_('Voorraad'));
		$row->addHeader(_('Aantal verkocht'));

		$mutaties = $voorraden->mutatiesPerVoorraad($van, $tot);
		$totaal = 0;
		foreach ($mutaties as $voorraadID => $voorraadmutaties)
		{
			$voorraad = Voorraad::geef($voorraadID);
			$artikel = $voorraad->getArtikel();
			$subtotaal = -$voorraadmutaties->totaal();
			$totaal += $subtotaal;

			$tbody->add($row = new HtmlTableRow());
			$row->addData(ArtikelView::detailsLink($artikel));
			$row->addData(VoorraadView::detailsLink($voorraad));
			$row->addData($subtotaal);
		}
		$tfoot = $table->addFoot();
		$tfoot->add($row = new HtmlTableRow());
		$row->addData(_('Totaal'));
		$row->addData($totaal);

		$body->add($topdiv)
			 ->add(new HtmlBreak())
			 ->add($tablediv);

		$page = new HTMLPage();
		$page->start(_('Productenoverzicht'))
			->add(new HtmlHeader(2, _('Verkochte producten') . ": $MAANDEN[$maand] $jaar"))
			->add($body);
		return $page;
	}

	static public function table(VoorraadVerzameling $voorraden)
	{
		$table = new HtmlTable();

		$table->add(VoorraadView::tableHeader());

		foreach($voorraden as $voorraad)
		{
			$table->add(VoorraadView::tableRow($voorraad));
		}

		return $table;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
