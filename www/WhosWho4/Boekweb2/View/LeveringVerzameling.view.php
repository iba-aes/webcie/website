<?
/**
 * $Id$
 */
abstract class LeveringVerzamelingView
	extends LeveringVerzamelingView_Generated
{
	/**
	 *  Geef een HTMLTable-weergave van deze verzameling.
	 * @param lvs Een verzameling van Leveringen.
	 * @return Een HTMLTable.
	 */
	static public function toTable($lvs) {
		return static::makeTable($lvs, Levering::velden());
	}

	/**
	 *  Schrijf een CSV-weergave van deze verzameling.
	 * Deze weergave heeft de bedoeling om geladen te worden in Exact of een ander boekhoudprogramma.
	 * Zie ook bug #7199 voor de specifieke eisen aan deze output.
	 * @param lvs Een verzameling van Leveringen.
	 * @param file Een filehandle waar de csv in geschreven worden.
	 * @return void
	 */
	static public function toCSV($lvs, $file) {
		$velden = array("Boekdatum", "Grootboek", "Omschrijving", "Bedrag", "Alt");
		fputcsv($file, $velden);

		// Houd een alternator bij zodat Exact weet welke transacties bij elkaar horen.
		$alternator = 0;

		foreach ($lvs as $levering) {
			$order = $levering->getOrder();
			if (is_null($order)) {
				continue; // want zonder order hebben we geen idee wat dit kost
			}
			$naam = $levering->getArtikel()->getNaam();
			$aantal = $levering->getAantal();
			$boekdatum = $levering->getWanneer()->strftime("%Y-%m-%d");
			$prijs = $levering->getVoorraad()->getWaardePerStuk(); // merk op dat dit niet altijd gelijk is aan waarde bij het leveren!
			$omschrijving = sprintf(_("[VOC:Artikelnaam]%s [VOC:aantal in levering]%d stuks a [VOC:waarde per stuk]%.2f excl btw"),
				$naam,
				$aantal,
				$prijs
			);
			$orderView = $order->getView();
			$orderView::toCsv($order, $file, $velden, $aantal, $boekdatum, $omschrijving, $alternator);
			$alternator++;
		}
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
