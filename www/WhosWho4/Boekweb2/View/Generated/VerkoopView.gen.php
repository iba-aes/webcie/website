<?
abstract class VerkoopView_Generated
	extends VoorraadMutatieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerkoopView.
	 *
	 * @param Verkoop $obj Het Verkoop-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerkoop(Verkoop $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld transactie.
	 *
	 * @param Verkoop $obj Het Verkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld transactie labelt.
	 */
	public static function labelTransactie(Verkoop $obj)
	{
		return 'Transactie';
	}
	/**
	 * @brief Geef de waarde van het veld transactie.
	 *
	 * @param Verkoop $obj Het Verkoop-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld transactie van het object obj
	 * representeert.
	 */
	public static function waardeTransactie(Verkoop $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTransactie())
			return NULL;
		return TransactieView::defaultWaardeTransactie($obj->getTransactie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld transactie.
	 *
	 * @see genericFormtransactie
	 *
	 * @param Verkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld transactie staat en kan
	 * worden bewerkt. Indien transactie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTransactie(Verkoop $obj, $include_id = false)
	{
		return static::waardeTransactie($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld transactie. In
	 * tegenstelling tot formtransactie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtransactie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld transactie staat en kan
	 * worden bewerkt. Indien transactie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTransactie($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * transactie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld transactie representeert.
	 */
	public static function opmerkingTransactie()
	{
		return NULL;
	}
}
