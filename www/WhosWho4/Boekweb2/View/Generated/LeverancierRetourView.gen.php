<?
abstract class LeverancierRetourView_Generated
	extends VoorraadMutatieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LeverancierRetourView.
	 *
	 * @param LeverancierRetour $obj Het LeverancierRetour-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLeverancierRetour(LeverancierRetour $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld order.
	 *
	 * @param LeverancierRetour $obj Het LeverancierRetour-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld order labelt.
	 */
	public static function labelOrder(LeverancierRetour $obj)
	{
		return 'Order';
	}
	/**
	 * @brief Geef de waarde van het veld order.
	 *
	 * @param LeverancierRetour $obj Het LeverancierRetour-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld order van het object obj
	 * representeert.
	 */
	public static function waardeOrder(LeverancierRetour $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getOrder())
			return NULL;
		return OrderView::defaultWaardeOrder($obj->getOrder());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld order.
	 *
	 * @see genericFormorder
	 *
	 * @param LeverancierRetour $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld order staat en kan worden
	 * bewerkt. Indien order read-only is betreft het een statisch html-element.
	 */
	public static function formOrder(LeverancierRetour $obj, $include_id = false)
	{
		return static::waardeOrder($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld order. In
	 * tegenstelling tot formorder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formorder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld order staat en kan worden
	 * bewerkt. Indien order read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOrder($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld order
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld order representeert.
	 */
	public static function opmerkingOrder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld leverancier.
	 *
	 * @param LeverancierRetour $obj Het LeverancierRetour-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld leverancier labelt.
	 */
	public static function labelLeverancier(LeverancierRetour $obj)
	{
		return 'Leverancier';
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @param LeverancierRetour $obj Het LeverancierRetour-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld leverancier van het object
	 * obj representeert.
	 */
	public static function waardeLeverancier(LeverancierRetour $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLeverancier())
			return NULL;
		return LeverancierView::defaultWaardeLeverancier($obj->getLeverancier());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld leverancier.
	 *
	 * @see genericFormleverancier
	 *
	 * @param LeverancierRetour $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLeverancier(LeverancierRetour $obj, $include_id = false)
	{
		return static::waardeLeverancier($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld leverancier. In
	 * tegenstelling tot formleverancier moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formleverancier
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLeverancier($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * leverancier bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld leverancier representeert.
	 */
	public static function opmerkingLeverancier()
	{
		return NULL;
	}
}
