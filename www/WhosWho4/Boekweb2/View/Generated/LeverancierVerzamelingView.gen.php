<?
abstract class LeverancierVerzamelingView_Generated
	extends ContactVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LeverancierVerzamelingView.
	 *
	 * @param LeverancierVerzameling $obj Het LeverancierVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLeverancierVerzameling(LeverancierVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
