<?
abstract class PinView_Generated
	extends TransactieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PinView.
	 *
	 * @param Pin $obj Het Pin-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePin(Pin $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld rekening.
	 *
	 * @param Pin $obj Het Pin-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rekening labelt.
	 */
	public static function labelRekening(Pin $obj)
	{
		return 'Rekening';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld rekening.
	 *
	 * @param string $value Een enum-waarde van het veld rekening.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumRekening($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld rekening horen.
	 *
	 * @see labelenumRekening
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld rekening
	 * representeren.
	 */
	public static function labelenumRekeningArray()
	{
		$soorten = array();
		foreach(Pin::enumsRekening() as $id)
			$soorten[$id] = PinView::labelenumRekening($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld rekening.
	 *
	 * @param Pin $obj Het Pin-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rekening van het object obj
	 * representeert.
	 */
	public static function waardeRekening(Pin $obj)
	{
		return static::defaultWaardeEnum($obj, 'Rekening');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rekening.
	 *
	 * @see genericFormrekening
	 *
	 * @param Pin $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekening staat en kan
	 * worden bewerkt. Indien rekening read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRekening(Pin $obj, $include_id = false)
	{
		return static::waardeRekening($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rekening. In
	 * tegenstelling tot formrekening moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formrekening
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekening staat en kan
	 * worden bewerkt. Indien rekening read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRekening($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * rekening bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rekening representeert.
	 */
	public static function opmerkingRekening()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld apparaat.
	 *
	 * @param Pin $obj Het Pin-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld apparaat labelt.
	 */
	public static function labelApparaat(Pin $obj)
	{
		return 'Apparaat';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld apparaat.
	 *
	 * @param string $value Een enum-waarde van het veld apparaat.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumApparaat($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld apparaat horen.
	 *
	 * @see labelenumApparaat
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld apparaat
	 * representeren.
	 */
	public static function labelenumApparaatArray()
	{
		$soorten = array();
		foreach(Pin::enumsApparaat() as $id)
			$soorten[$id] = PinView::labelenumApparaat($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld apparaat.
	 *
	 * @param Pin $obj Het Pin-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld apparaat van het object obj
	 * representeert.
	 */
	public static function waardeApparaat(Pin $obj)
	{
		return static::defaultWaardeEnum($obj, 'Apparaat');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld apparaat.
	 *
	 * @see genericFormapparaat
	 *
	 * @param Pin $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld apparaat staat en kan
	 * worden bewerkt. Indien apparaat read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formApparaat(Pin $obj, $include_id = false)
	{
		return static::waardeApparaat($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld apparaat. In
	 * tegenstelling tot formapparaat moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formapparaat
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld apparaat staat en kan
	 * worden bewerkt. Indien apparaat read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormApparaat($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * apparaat bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld apparaat representeert.
	 */
	public static function opmerkingApparaat()
	{
		return NULL;
	}
}
