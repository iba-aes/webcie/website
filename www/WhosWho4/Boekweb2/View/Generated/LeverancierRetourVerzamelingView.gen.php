<?
abstract class LeverancierRetourVerzamelingView_Generated
	extends VoorraadMutatieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LeverancierRetourVerzamelingView.
	 *
	 * @param LeverancierRetourVerzameling $obj Het LeverancierRetourVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLeverancierRetourVerzameling(LeverancierRetourVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
