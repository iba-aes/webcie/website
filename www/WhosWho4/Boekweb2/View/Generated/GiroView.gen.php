<?
abstract class GiroView_Generated
	extends TransactieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GiroView.
	 *
	 * @param Giro $obj Het Giro-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGiro(Giro $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld rekening.
	 *
	 * @param Giro $obj Het Giro-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rekening labelt.
	 */
	public static function labelRekening(Giro $obj)
	{
		return 'Rekening';
	}
	/**
	 * @brief Geef de waarde van het veld rekening.
	 *
	 * @param Giro $obj Het Giro-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rekening van het object obj
	 * representeert.
	 */
	public static function waardeRekening(Giro $obj)
	{
		return static::defaultWaardeString($obj, 'Rekening');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rekening.
	 *
	 * @see genericFormrekening
	 *
	 * @param Giro $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekening staat en kan
	 * worden bewerkt. Indien rekening read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRekening(Giro $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Rekening', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rekening. In
	 * tegenstelling tot formrekening moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formrekening
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekening staat en kan
	 * worden bewerkt. Indien rekening read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRekening($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Rekening', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * rekening bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rekening representeert.
	 */
	public static function opmerkingRekening()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tegenrekening.
	 *
	 * @param Giro $obj Het Giro-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tegenrekening labelt.
	 */
	public static function labelTegenrekening(Giro $obj)
	{
		return 'Tegenrekening';
	}
	/**
	 * @brief Geef de waarde van het veld tegenrekening.
	 *
	 * @param Giro $obj Het Giro-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tegenrekening van het object
	 * obj representeert.
	 */
	public static function waardeTegenrekening(Giro $obj)
	{
		return static::defaultWaardeString($obj, 'Tegenrekening');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tegenrekening.
	 *
	 * @see genericFormtegenrekening
	 *
	 * @param Giro $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tegenrekening staat en kan
	 * worden bewerkt. Indien tegenrekening read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTegenrekening(Giro $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Tegenrekening', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tegenrekening. In
	 * tegenstelling tot formtegenrekening moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtegenrekening
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tegenrekening staat en kan
	 * worden bewerkt. Indien tegenrekening read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTegenrekening($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Tegenrekening', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tegenrekening bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tegenrekening representeert.
	 */
	public static function opmerkingTegenrekening()
	{
		return NULL;
	}
}
