<?
abstract class SchuldVerzamelingView_Generated
	extends TransactieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in SchuldVerzamelingView.
	 *
	 * @param SchuldVerzameling $obj Het SchuldVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeSchuldVerzameling(SchuldVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
