<?
abstract class JaargangArtikelVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in JaargangArtikelVerzamelingView.
	 *
	 * @param JaargangArtikelVerzameling $obj Het JaargangArtikelVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeJaargangArtikelVerzameling(JaargangArtikelVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
