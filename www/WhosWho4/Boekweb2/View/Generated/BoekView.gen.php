<?
abstract class BoekView_Generated
	extends ArtikelView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BoekView.
	 *
	 * @param Boek $obj Het Boek-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBoek(Boek $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld isbn.
	 *
	 * @param Boek $obj Het Boek-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld isbn labelt.
	 */
	public static function labelIsbn(Boek $obj)
	{
		return 'Isbn';
	}
	/**
	 * @brief Geef de waarde van het veld isbn.
	 *
	 * @param Boek $obj Het Boek-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld isbn van het object obj
	 * representeert.
	 */
	public static function waardeIsbn(Boek $obj)
	{
		return static::defaultWaardeString($obj, 'Isbn');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld isbn.
	 *
	 * @see genericFormisbn
	 *
	 * @param Boek $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld isbn staat en kan worden
	 * bewerkt. Indien isbn read-only is betreft het een statisch html-element.
	 */
	public static function formIsbn(Boek $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Isbn', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld isbn. In tegenstelling
	 * tot formisbn moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formisbn
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld isbn staat en kan worden
	 * bewerkt. Indien isbn read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormIsbn($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Isbn', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld isbn
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld isbn representeert.
	 */
	public static function opmerkingIsbn()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld auteur.
	 *
	 * @param Boek $obj Het Boek-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld auteur labelt.
	 */
	public static function labelAuteur(Boek $obj)
	{
		return 'Auteur';
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @param Boek $obj Het Boek-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld auteur van het object obj
	 * representeert.
	 */
	public static function waardeAuteur(Boek $obj)
	{
		return static::defaultWaardeString($obj, 'Auteur');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld auteur.
	 *
	 * @see genericFormauteur
	 *
	 * @param Boek $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is betreft het een statisch html-element.
	 */
	public static function formAuteur(Boek $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Auteur', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld auteur. In
	 * tegenstelling tot formauteur moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formauteur
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAuteur($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Auteur', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld auteur
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld auteur representeert.
	 */
	public static function opmerkingAuteur()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld druk.
	 *
	 * @param Boek $obj Het Boek-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld druk labelt.
	 */
	public static function labelDruk(Boek $obj)
	{
		return 'Druk';
	}
	/**
	 * @brief Geef de waarde van het veld druk.
	 *
	 * @param Boek $obj Het Boek-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld druk van het object obj
	 * representeert.
	 */
	public static function waardeDruk(Boek $obj)
	{
		return static::defaultWaardeString($obj, 'Druk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld druk.
	 *
	 * @see genericFormdruk
	 *
	 * @param Boek $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld druk staat en kan worden
	 * bewerkt. Indien druk read-only is betreft het een statisch html-element.
	 */
	public static function formDruk(Boek $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Druk', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld druk. In tegenstelling
	 * tot formdruk moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formdruk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld druk staat en kan worden
	 * bewerkt. Indien druk read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormDruk($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Druk', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld druk
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld druk representeert.
	 */
	public static function opmerkingDruk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uitgever.
	 *
	 * @param Boek $obj Het Boek-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitgever labelt.
	 */
	public static function labelUitgever(Boek $obj)
	{
		return 'Uitgever';
	}
	/**
	 * @brief Geef de waarde van het veld uitgever.
	 *
	 * @param Boek $obj Het Boek-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitgever van het object obj
	 * representeert.
	 */
	public static function waardeUitgever(Boek $obj)
	{
		return static::defaultWaardeString($obj, 'Uitgever');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uitgever.
	 *
	 * @see genericFormuitgever
	 *
	 * @param Boek $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgever staat en kan
	 * worden bewerkt. Indien uitgever read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUitgever(Boek $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Uitgever', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uitgever. In
	 * tegenstelling tot formuitgever moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuitgever
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgever staat en kan
	 * worden bewerkt. Indien uitgever read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUitgever($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Uitgever', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uitgever bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitgever representeert.
	 */
	public static function opmerkingUitgever()
	{
		return NULL;
	}
}
