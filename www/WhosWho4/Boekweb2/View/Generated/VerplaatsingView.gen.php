<?
abstract class VerplaatsingView_Generated
	extends VoorraadMutatieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerplaatsingView.
	 *
	 * @param Verplaatsing $obj Het Verplaatsing-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerplaatsing(Verplaatsing $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld tegenVoorraad.
	 *
	 * @param Verplaatsing $obj Het Verplaatsing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld tegenVoorraad labelt.
	 */
	public static function labelTegenVoorraad(Verplaatsing $obj)
	{
		return 'TegenVoorraad';
	}
	/**
	 * @brief Geef de waarde van het veld tegenVoorraad.
	 *
	 * @param Verplaatsing $obj Het Verplaatsing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tegenVoorraad van het object
	 * obj representeert.
	 */
	public static function waardeTegenVoorraad(Verplaatsing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTegenVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getTegenVoorraad());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tegenVoorraad.
	 *
	 * @see genericFormtegenVoorraad
	 *
	 * @param Verplaatsing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tegenVoorraad staat en kan
	 * worden bewerkt. Indien tegenVoorraad read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTegenVoorraad(Verplaatsing $obj, $include_id = false)
	{
		return static::waardeTegenVoorraad($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tegenVoorraad. In
	 * tegenstelling tot formtegenVoorraad moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtegenVoorraad
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tegenVoorraad staat en kan
	 * worden bewerkt. Indien tegenVoorraad read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTegenVoorraad($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tegenVoorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tegenVoorraad representeert.
	 */
	public static function opmerkingTegenVoorraad()
	{
		return NULL;
	}
}
