<?
abstract class iDealAntwoordVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealAntwoordVerzamelingView.
	 *
	 * @param iDealAntwoordVerzameling $obj Het iDealAntwoordVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealAntwoordVerzameling(iDealAntwoordVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
