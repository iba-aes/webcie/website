<?
abstract class TellingItemView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TellingItemView.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTellingItem(TellingItem $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld telling.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld telling labelt.
	 */
	public static function labelTelling(TellingItem $obj)
	{
		return 'Telling';
	}
	/**
	 * @brief Geef de waarde van het veld telling.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld telling van het object obj
	 * representeert.
	 */
	public static function waardeTelling(TellingItem $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTelling())
			return NULL;
		return TellingView::defaultWaardeTelling($obj->getTelling());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld telling
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld telling representeert.
	 */
	public static function opmerkingTelling()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorraad.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraad labelt.
	 */
	public static function labelVoorraad(TellingItem $obj)
	{
		return 'Voorraad';
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraad van het object obj
	 * representeert.
	 */
	public static function waardeVoorraad(TellingItem $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getVoorraad());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraad representeert.
	 */
	public static function opmerkingVoorraad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aantal.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aantal labelt.
	 */
	public static function labelAantal(TellingItem $obj)
	{
		return 'Aantal';
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @param TellingItem $obj Het TellingItem-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aantal van het object obj
	 * representeert.
	 */
	public static function waardeAantal(TellingItem $obj)
	{
		return static::defaultWaardeInt($obj, 'Aantal');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aantal.
	 *
	 * @see genericFormaantal
	 *
	 * @param TellingItem $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is betreft het een statisch html-element.
	 */
	public static function formAantal(TellingItem $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Aantal', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aantal. In
	 * tegenstelling tot formaantal moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formaantal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAantal($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Aantal');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld aantal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aantal representeert.
	 */
	public static function opmerkingAantal()
	{
		return NULL;
	}
}
