<?
abstract class OrderVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in OrderVerzamelingView.
	 *
	 * @param OrderVerzameling $obj Het OrderVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeOrderVerzameling(OrderVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
