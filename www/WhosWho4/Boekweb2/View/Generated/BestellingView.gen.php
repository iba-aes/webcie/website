<?
abstract class BestellingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BestellingView.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBestelling(Bestelling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bestellingID.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bestellingID labelt.
	 */
	public static function labelBestellingID(Bestelling $obj)
	{
		return 'BestellingID';
	}
	/**
	 * @brief Geef de waarde van het veld bestellingID.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bestellingID van het object
	 * obj representeert.
	 */
	public static function waardeBestellingID(Bestelling $obj)
	{
		return static::defaultWaardeInt($obj, 'BestellingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bestellingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bestellingID representeert.
	 */
	public static function opmerkingBestellingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Bestelling $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Bestelling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(Bestelling $obj, $include_id = false)
	{
		return static::waardePersoon($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld artikel.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld artikel labelt.
	 */
	public static function labelArtikel(Bestelling $obj)
	{
		return 'Artikel';
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld artikel van het object obj
	 * representeert.
	 */
	public static function waardeArtikel(Bestelling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getArtikel())
			return NULL;
		return ArtikelView::defaultWaardeArtikel($obj->getArtikel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld artikel.
	 *
	 * @see genericFormartikel
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formArtikel(Bestelling $obj, $include_id = false)
	{
		return static::waardeArtikel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld artikel. In
	 * tegenstelling tot formartikel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formartikel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormArtikel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld artikel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld artikel representeert.
	 */
	public static function opmerkingArtikel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumGeplaatst.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumGeplaatst labelt.
	 */
	public static function labelDatumGeplaatst(Bestelling $obj)
	{
		return 'DatumGeplaatst';
	}
	/**
	 * @brief Geef de waarde van het veld datumGeplaatst.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumGeplaatst van het object
	 * obj representeert.
	 */
	public static function waardeDatumGeplaatst(Bestelling $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumGeplaatst');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumGeplaatst.
	 *
	 * @see genericFormdatumGeplaatst
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeplaatst staat en
	 * kan worden bewerkt. Indien datumGeplaatst read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumGeplaatst(Bestelling $obj, $include_id = false)
	{
		return static::waardeDatumGeplaatst($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumGeplaatst. In
	 * tegenstelling tot formdatumGeplaatst moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumGeplaatst
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeplaatst staat en
	 * kan worden bewerkt. Indien datumGeplaatst read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumGeplaatst($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumGeplaatst bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumGeplaatst representeert.
	 */
	public static function opmerkingDatumGeplaatst()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(Bestelling $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(Bestelling::enumsStatus() as $id)
			$soorten[$id] = BestellingView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(Bestelling $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(Bestelling $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', Bestelling::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aantal.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aantal labelt.
	 */
	public static function labelAantal(Bestelling $obj)
	{
		return 'Aantal';
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aantal van het object obj
	 * representeert.
	 */
	public static function waardeAantal(Bestelling $obj)
	{
		return static::defaultWaardeInt($obj, 'Aantal');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aantal.
	 *
	 * @see genericFormaantal
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is betreft het een statisch html-element.
	 */
	public static function formAantal(Bestelling $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Aantal', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aantal. In
	 * tegenstelling tot formaantal moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formaantal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAantal($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Aantal');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld aantal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aantal representeert.
	 */
	public static function opmerkingAantal()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vervalDatum.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vervalDatum labelt.
	 */
	public static function labelVervalDatum(Bestelling $obj)
	{
		return 'VervalDatum';
	}
	/**
	 * @brief Geef de waarde van het veld vervalDatum.
	 *
	 * @param Bestelling $obj Het Bestelling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vervalDatum van het object
	 * obj representeert.
	 */
	public static function waardeVervalDatum(Bestelling $obj)
	{
		return static::defaultWaardeDate($obj, 'VervalDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vervalDatum.
	 *
	 * @see genericFormvervalDatum
	 *
	 * @param Bestelling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vervalDatum staat en kan
	 * worden bewerkt. Indien vervalDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVervalDatum(Bestelling $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'VervalDatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vervalDatum. In
	 * tegenstelling tot formvervalDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvervalDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vervalDatum staat en kan
	 * worden bewerkt. Indien vervalDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVervalDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'VervalDatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * vervalDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vervalDatum representeert.
	 */
	public static function opmerkingVervalDatum()
	{
		return NULL;
	}
}
