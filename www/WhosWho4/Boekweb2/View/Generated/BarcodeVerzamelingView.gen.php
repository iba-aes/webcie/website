<?
abstract class BarcodeVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BarcodeVerzamelingView.
	 *
	 * @param BarcodeVerzameling $obj Het BarcodeVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBarcodeVerzameling(BarcodeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
