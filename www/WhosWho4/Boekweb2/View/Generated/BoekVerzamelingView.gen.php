<?
abstract class BoekVerzamelingView_Generated
	extends ArtikelVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BoekVerzamelingView.
	 *
	 * @param BoekVerzameling $obj Het BoekVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBoekVerzameling(BoekVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
