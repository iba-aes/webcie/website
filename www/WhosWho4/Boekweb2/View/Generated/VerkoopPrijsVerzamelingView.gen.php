<?
abstract class VerkoopPrijsVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerkoopPrijsVerzamelingView.
	 *
	 * @param VerkoopPrijsVerzameling $obj Het VerkoopPrijsVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerkoopPrijsVerzameling(VerkoopPrijsVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
