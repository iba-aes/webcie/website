<?
abstract class BoekverkoopView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BoekverkoopView.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBoekverkoop(Boekverkoop $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld verkoopdag.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld verkoopdag labelt.
	 */
	public static function labelVerkoopdag(Boekverkoop $obj)
	{
		return 'Verkoopdag';
	}
	/**
	 * @brief Geef de waarde van het veld verkoopdag.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verkoopdag van het object obj
	 * representeert.
	 */
	public static function waardeVerkoopdag(Boekverkoop $obj)
	{
		return static::defaultWaardeDate($obj, 'Verkoopdag');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verkoopdag bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verkoopdag representeert.
	 */
	public static function opmerkingVerkoopdag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld volgnummer.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld volgnummer labelt.
	 */
	public static function labelVolgnummer(Boekverkoop $obj)
	{
		return 'Volgnummer';
	}
	/**
	 * @brief Geef de waarde van het veld volgnummer.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld volgnummer van het object obj
	 * representeert.
	 */
	public static function waardeVolgnummer(Boekverkoop $obj)
	{
		return static::defaultWaardeInt($obj, 'Volgnummer');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * volgnummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld volgnummer representeert.
	 */
	public static function opmerkingVolgnummer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opener.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opener labelt.
	 */
	public static function labelOpener(Boekverkoop $obj)
	{
		return 'Opener';
	}
	/**
	 * @brief Geef de waarde van het veld opener.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opener van het object obj
	 * representeert.
	 */
	public static function waardeOpener(Boekverkoop $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getOpener())
			return NULL;
		return LidView::defaultWaardeLid($obj->getOpener());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opener.
	 *
	 * @see genericFormopener
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opener staat en kan worden
	 * bewerkt. Indien opener read-only is betreft het een statisch html-element.
	 */
	public static function formOpener(Boekverkoop $obj, $include_id = false)
	{
		return static::waardeOpener($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opener. In
	 * tegenstelling tot formopener moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formopener
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opener staat en kan worden
	 * bewerkt. Indien opener read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOpener($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld opener
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opener representeert.
	 */
	public static function opmerkingOpener()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld open.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld open labelt.
	 */
	public static function labelOpen(Boekverkoop $obj)
	{
		return 'Open';
	}
	/**
	 * @brief Geef de waarde van het veld open.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld open van het object obj
	 * representeert.
	 */
	public static function waardeOpen(Boekverkoop $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Open');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld open.
	 *
	 * @see genericFormopen
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld open staat en kan worden
	 * bewerkt. Indien open read-only is betreft het een statisch html-element.
	 */
	public static function formOpen(Boekverkoop $obj, $include_id = false)
	{
		return static::waardeOpen($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld open. In tegenstelling
	 * tot formopen moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formopen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld open staat en kan worden
	 * bewerkt. Indien open read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOpen($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld open
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld open representeert.
	 */
	public static function opmerkingOpen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld sluiter.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld sluiter labelt.
	 */
	public static function labelSluiter(Boekverkoop $obj)
	{
		return 'Sluiter';
	}
	/**
	 * @brief Geef de waarde van het veld sluiter.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld sluiter van het object obj
	 * representeert.
	 */
	public static function waardeSluiter(Boekverkoop $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getSluiter())
			return NULL;
		return LidView::defaultWaardeLid($obj->getSluiter());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld sluiter.
	 *
	 * @see genericFormsluiter
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld sluiter staat en kan
	 * worden bewerkt. Indien sluiter read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formSluiter(Boekverkoop $obj, $include_id = false)
	{
		return LidView::defaultForm($obj->getSluiter());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld sluiter. In
	 * tegenstelling tot formsluiter moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsluiter
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld sluiter staat en kan
	 * worden bewerkt. Indien sluiter read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormSluiter($name, $waarde=NULL)
	{
		return LidView::genericDefaultForm('Sluiter');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld sluiter
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld sluiter representeert.
	 */
	public static function opmerkingSluiter()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld pin1.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld pin1 labelt.
	 */
	public static function labelPin1(Boekverkoop $obj)
	{
		return 'Pin1';
	}
	/**
	 * @brief Geef de waarde van het veld pin1.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld pin1 van het object obj
	 * representeert.
	 */
	public static function waardePin1(Boekverkoop $obj)
	{
		return static::defaultWaardeMoney($obj, 'Pin1');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld pin1.
	 *
	 * @see genericFormpin1
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pin1 staat en kan worden
	 * bewerkt. Indien pin1 read-only is betreft het een statisch html-element.
	 */
	public static function formPin1(Boekverkoop $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Pin1', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld pin1. In tegenstelling
	 * tot formpin1 moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formpin1
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pin1 staat en kan worden
	 * bewerkt. Indien pin1 read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPin1($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Pin1');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld pin1
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld pin1 representeert.
	 */
	public static function opmerkingPin1()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld pin2.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld pin2 labelt.
	 */
	public static function labelPin2(Boekverkoop $obj)
	{
		return 'Pin2';
	}
	/**
	 * @brief Geef de waarde van het veld pin2.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld pin2 van het object obj
	 * representeert.
	 */
	public static function waardePin2(Boekverkoop $obj)
	{
		return static::defaultWaardeMoney($obj, 'Pin2');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld pin2.
	 *
	 * @see genericFormpin2
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pin2 staat en kan worden
	 * bewerkt. Indien pin2 read-only is betreft het een statisch html-element.
	 */
	public static function formPin2(Boekverkoop $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Pin2', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld pin2. In tegenstelling
	 * tot formpin2 moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formpin2
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pin2 staat en kan worden
	 * bewerkt. Indien pin2 read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPin2($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Pin2');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld pin2
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld pin2 representeert.
	 */
	public static function opmerkingPin2()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kasverschil.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld kasverschil labelt.
	 */
	public static function labelKasverschil(Boekverkoop $obj)
	{
		return 'Kasverschil';
	}
	/**
	 * @brief Geef de waarde van het veld kasverschil.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kasverschil van het object
	 * obj representeert.
	 */
	public static function waardeKasverschil(Boekverkoop $obj)
	{
		return static::defaultWaardeMoney($obj, 'Kasverschil');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kasverschil.
	 *
	 * @see genericFormkasverschil
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kasverschil staat en kan
	 * worden bewerkt. Indien kasverschil read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKasverschil(Boekverkoop $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Kasverschil', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kasverschil. In
	 * tegenstelling tot formkasverschil moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formkasverschil
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kasverschil staat en kan
	 * worden bewerkt. Indien kasverschil read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKasverschil($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Kasverschil');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * kasverschil bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kasverschil representeert.
	 */
	public static function opmerkingKasverschil()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld gesloten.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld gesloten labelt.
	 */
	public static function labelGesloten(Boekverkoop $obj)
	{
		return 'Gesloten';
	}
	/**
	 * @brief Geef de waarde van het veld gesloten.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld gesloten van het object obj
	 * representeert.
	 */
	public static function waardeGesloten(Boekverkoop $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Gesloten');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld gesloten.
	 *
	 * @see genericFormgesloten
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gesloten staat en kan
	 * worden bewerkt. Indien gesloten read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGesloten(Boekverkoop $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Gesloten', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld gesloten. In
	 * tegenstelling tot formgesloten moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgesloten
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gesloten staat en kan
	 * worden bewerkt. Indien gesloten read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGesloten($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Gesloten');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * gesloten bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld gesloten representeert.
	 */
	public static function opmerkingGesloten()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(Boekverkoop $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param Boekverkoop $obj Het Boekverkoop-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(Boekverkoop $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param Boekverkoop $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(Boekverkoop $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
}
