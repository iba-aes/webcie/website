<?
abstract class PeriodeVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PeriodeVerzamelingView.
	 *
	 * @param PeriodeVerzameling $obj Het PeriodeVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePeriodeVerzameling(PeriodeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
