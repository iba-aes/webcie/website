<?
abstract class OrderView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in OrderView.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeOrder(Order $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld orderID.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld orderID labelt.
	 */
	public static function labelOrderID(Order $obj)
	{
		return 'OrderID';
	}
	/**
	 * @brief Geef de waarde van het veld orderID.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld orderID van het object obj
	 * representeert.
	 */
	public static function waardeOrderID(Order $obj)
	{
		return static::defaultWaardeInt($obj, 'OrderID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld orderID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld orderID representeert.
	 */
	public static function opmerkingOrderID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld artikel.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld artikel labelt.
	 */
	public static function labelArtikel(Order $obj)
	{
		return 'Artikel';
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld artikel van het object obj
	 * representeert.
	 */
	public static function waardeArtikel(Order $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getArtikel())
			return NULL;
		return ArtikelView::defaultWaardeArtikel($obj->getArtikel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld artikel.
	 *
	 * @see genericFormartikel
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formArtikel(Order $obj, $include_id = false)
	{
		return static::waardeArtikel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld artikel. In
	 * tegenstelling tot formartikel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formartikel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormArtikel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld artikel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld artikel representeert.
	 */
	public static function opmerkingArtikel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld leverancier.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld leverancier labelt.
	 */
	public static function labelLeverancier(Order $obj)
	{
		return 'Leverancier';
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld leverancier van het object
	 * obj representeert.
	 */
	public static function waardeLeverancier(Order $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLeverancier())
			return NULL;
		return LeverancierView::defaultWaardeLeverancier($obj->getLeverancier());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld leverancier.
	 *
	 * @see genericFormleverancier
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLeverancier(Order $obj, $include_id = false)
	{
		return LeverancierView::defaultForm($obj->getLeverancier());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld leverancier. In
	 * tegenstelling tot formleverancier moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formleverancier
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLeverancier($name, $waarde=NULL)
	{
		return LeverancierView::genericDefaultForm('Leverancier');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * leverancier bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld leverancier representeert.
	 */
	public static function opmerkingLeverancier()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld waardePerStuk.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld waardePerStuk labelt.
	 */
	public static function labelWaardePerStuk(Order $obj)
	{
		return 'WaardePerStuk';
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld waardePerStuk van het object
	 * obj representeert.
	 */
	public static function waardeWaardePerStuk(Order $obj)
	{
		return static::defaultWaardeMoney($obj, 'WaardePerStuk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld waardePerStuk.
	 *
	 * @see genericFormwaardePerStuk
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWaardePerStuk(Order $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'WaardePerStuk', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld waardePerStuk. In
	 * tegenstelling tot formwaardePerStuk moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwaardePerStuk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWaardePerStuk($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'WaardePerStuk');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * waardePerStuk bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld waardePerStuk representeert.
	 */
	public static function opmerkingWaardePerStuk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumGeplaatst.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumGeplaatst labelt.
	 */
	public static function labelDatumGeplaatst(Order $obj)
	{
		return 'DatumGeplaatst';
	}
	/**
	 * @brief Geef de waarde van het veld datumGeplaatst.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumGeplaatst van het object
	 * obj representeert.
	 */
	public static function waardeDatumGeplaatst(Order $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumGeplaatst');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumGeplaatst.
	 *
	 * @see genericFormdatumGeplaatst
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeplaatst staat en
	 * kan worden bewerkt. Indien datumGeplaatst read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumGeplaatst(Order $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumGeplaatst', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumGeplaatst. In
	 * tegenstelling tot formdatumGeplaatst moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumGeplaatst
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumGeplaatst staat en
	 * kan worden bewerkt. Indien datumGeplaatst read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumGeplaatst($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumGeplaatst');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumGeplaatst bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumGeplaatst representeert.
	 */
	public static function opmerkingDatumGeplaatst()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld geannuleerd.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld geannuleerd labelt.
	 */
	public static function labelGeannuleerd(Order $obj)
	{
		return 'Geannuleerd';
	}
	/**
	 * @brief Geef de waarde van het veld geannuleerd.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld geannuleerd van het object
	 * obj representeert.
	 */
	public static function waardeGeannuleerd(Order $obj)
	{
		return static::defaultWaardeBool($obj, 'Geannuleerd');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld geannuleerd.
	 *
	 * @see genericFormgeannuleerd
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geannuleerd staat en kan
	 * worden bewerkt. Indien geannuleerd read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGeannuleerd(Order $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Geannuleerd', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld geannuleerd. In
	 * tegenstelling tot formgeannuleerd moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgeannuleerd
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld geannuleerd staat en kan
	 * worden bewerkt. Indien geannuleerd read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGeannuleerd($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Geannuleerd');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * geannuleerd bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld geannuleerd representeert.
	 */
	public static function opmerkingGeannuleerd()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aantal.
	 *
	 * @param Order $obj Het Order-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aantal labelt.
	 */
	public static function labelAantal(Order $obj)
	{
		return 'Aantal';
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @param Order $obj Het Order-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aantal van het object obj
	 * representeert.
	 */
	public static function waardeAantal(Order $obj)
	{
		return static::defaultWaardeInt($obj, 'Aantal');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aantal.
	 *
	 * @see genericFormaantal
	 *
	 * @param Order $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is betreft het een statisch html-element.
	 */
	public static function formAantal(Order $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Aantal', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aantal. In
	 * tegenstelling tot formaantal moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formaantal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAantal($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Aantal');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld aantal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aantal representeert.
	 */
	public static function opmerkingAantal()
	{
		return NULL;
	}
}
