<?
abstract class VakStudieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VakStudieView.
	 *
	 * @param VakStudie $obj Het VakStudie-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVakStudie(VakStudie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld vak.
	 *
	 * @param VakStudie $obj Het VakStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vak labelt.
	 */
	public static function labelVak(VakStudie $obj)
	{
		return 'Vak';
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @param VakStudie $obj Het VakStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vak van het object obj
	 * representeert.
	 */
	public static function waardeVak(VakStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVak())
			return NULL;
		return VakView::defaultWaardeVak($obj->getVak());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vak
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vak representeert.
	 */
	public static function opmerkingVak()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld studie.
	 *
	 * @param VakStudie $obj Het VakStudie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studie labelt.
	 */
	public static function labelStudie(VakStudie $obj)
	{
		return 'Studie';
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @param VakStudie $obj Het VakStudie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studie van het object obj
	 * representeert.
	 */
	public static function waardeStudie(VakStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getStudie())
			return NULL;
		return StudieView::defaultWaardeStudie($obj->getStudie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld studie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studie representeert.
	 */
	public static function opmerkingStudie()
	{
		return NULL;
	}
}
