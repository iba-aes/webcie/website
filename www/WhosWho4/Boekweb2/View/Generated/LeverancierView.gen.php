<?
abstract class LeverancierView_Generated
	extends ContactView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LeverancierView.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLeverancier(Leverancier $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Leverancier $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Leverancier $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Leverancier $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Leverancier $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld dictaten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld dictaten labelt.
	 */
	public static function labelDictaten(Leverancier $obj)
	{
		return 'Dictaten';
	}
	/**
	 * @brief Geef de waarde van het veld dictaten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld dictaten van het object obj
	 * representeert.
	 */
	public static function waardeDictaten(Leverancier $obj)
	{
		return static::defaultWaardeBool($obj, 'Dictaten');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld dictaten.
	 *
	 * @see genericFormdictaten
	 *
	 * @param Leverancier $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dictaten staat en kan
	 * worden bewerkt. Indien dictaten read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDictaten(Leverancier $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Dictaten', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld dictaten. In
	 * tegenstelling tot formdictaten moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdictaten
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dictaten staat en kan
	 * worden bewerkt. Indien dictaten read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDictaten($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Dictaten');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * dictaten bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld dictaten representeert.
	 */
	public static function opmerkingDictaten()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld dictatenIntern.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld dictatenIntern labelt.
	 */
	public static function labelDictatenIntern(Leverancier $obj)
	{
		return 'DictatenIntern';
	}
	/**
	 * @brief Geef de waarde van het veld dictatenIntern.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld dictatenIntern van het object
	 * obj representeert.
	 */
	public static function waardeDictatenIntern(Leverancier $obj)
	{
		return static::defaultWaardeBool($obj, 'DictatenIntern');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld dictatenIntern.
	 *
	 * @see genericFormdictatenIntern
	 *
	 * @param Leverancier $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dictatenIntern staat en
	 * kan worden bewerkt. Indien dictatenIntern read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDictatenIntern(Leverancier $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'DictatenIntern', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld dictatenIntern. In
	 * tegenstelling tot formdictatenIntern moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdictatenIntern
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dictatenIntern staat en
	 * kan worden bewerkt. Indien dictatenIntern read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDictatenIntern($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'DictatenIntern');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * dictatenIntern bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld dictatenIntern representeert.
	 */
	public static function opmerkingDictatenIntern()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld dibsproducten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld dibsproducten labelt.
	 */
	public static function labelDibsproducten(Leverancier $obj)
	{
		return 'Dibsproducten';
	}
	/**
	 * @brief Geef de waarde van het veld dibsproducten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld dibsproducten van het object
	 * obj representeert.
	 */
	public static function waardeDibsproducten(Leverancier $obj)
	{
		return static::defaultWaardeBool($obj, 'Dibsproducten');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld dibsproducten.
	 *
	 * @see genericFormdibsproducten
	 *
	 * @param Leverancier $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dibsproducten staat en kan
	 * worden bewerkt. Indien dibsproducten read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDibsproducten(Leverancier $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Dibsproducten', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld dibsproducten. In
	 * tegenstelling tot formdibsproducten moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdibsproducten
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld dibsproducten staat en kan
	 * worden bewerkt. Indien dibsproducten read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDibsproducten($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Dibsproducten');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * dibsproducten bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld dibsproducten representeert.
	 */
	public static function opmerkingDibsproducten()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld colaproducten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld colaproducten labelt.
	 */
	public static function labelColaproducten(Leverancier $obj)
	{
		return 'Colaproducten';
	}
	/**
	 * @brief Geef de waarde van het veld colaproducten.
	 *
	 * @param Leverancier $obj Het Leverancier-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld colaproducten van het object
	 * obj representeert.
	 */
	public static function waardeColaproducten(Leverancier $obj)
	{
		return static::defaultWaardeBool($obj, 'Colaproducten');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld colaproducten.
	 *
	 * @see genericFormcolaproducten
	 *
	 * @param Leverancier $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld colaproducten staat en kan
	 * worden bewerkt. Indien colaproducten read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formColaproducten(Leverancier $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Colaproducten', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld colaproducten. In
	 * tegenstelling tot formcolaproducten moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcolaproducten
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld colaproducten staat en kan
	 * worden bewerkt. Indien colaproducten read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormColaproducten($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Colaproducten');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * colaproducten bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld colaproducten representeert.
	 */
	public static function opmerkingColaproducten()
	{
		return NULL;
	}
}
