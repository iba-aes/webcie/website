<?
abstract class iDealKaartjeCodeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealKaartjeCodeView.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealKaartjeCode(iDealKaartjeCode $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(iDealKaartjeCode $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(iDealKaartjeCode $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kaartje.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld kaartje labelt.
	 */
	public static function labelKaartje(iDealKaartjeCode $obj)
	{
		return 'Kaartje';
	}
	/**
	 * @brief Geef de waarde van het veld kaartje.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kaartje van het object obj
	 * representeert.
	 */
	public static function waardeKaartje(iDealKaartjeCode $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getKaartje())
			return NULL;
		return iDealKaartjeView::defaultWaardeiDealKaartje($obj->getKaartje());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kaartje.
	 *
	 * @see genericFormkaartje
	 *
	 * @param iDealKaartjeCode $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kaartje staat en kan
	 * worden bewerkt. Indien kaartje read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKaartje(iDealKaartjeCode $obj, $include_id = false)
	{
		return static::waardeKaartje($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kaartje. In
	 * tegenstelling tot formkaartje moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formkaartje
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kaartje staat en kan
	 * worden bewerkt. Indien kaartje read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKaartje($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kaartje
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kaartje representeert.
	 */
	public static function opmerkingKaartje()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld code.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld code labelt.
	 */
	public static function labelCode(iDealKaartjeCode $obj)
	{
		return 'Code';
	}
	/**
	 * @brief Geef de waarde van het veld code.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld code van het object obj
	 * representeert.
	 */
	public static function waardeCode(iDealKaartjeCode $obj)
	{
		return static::defaultWaardeString($obj, 'Code');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld code.
	 *
	 * @see genericFormcode
	 *
	 * @param iDealKaartjeCode $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is betreft het een statisch html-element.
	 */
	public static function formCode(iDealKaartjeCode $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Code', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld code. In tegenstelling
	 * tot formcode moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formcode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormCode($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Code', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld code
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld code representeert.
	 */
	public static function opmerkingCode()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld transactie.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld transactie labelt.
	 */
	public static function labelTransactie(iDealKaartjeCode $obj)
	{
		return 'Transactie';
	}
	/**
	 * @brief Geef de waarde van het veld transactie.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld transactie van het object obj
	 * representeert.
	 */
	public static function waardeTransactie(iDealKaartjeCode $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTransactie())
			return NULL;
		return TransactieView::defaultWaardeTransactie($obj->getTransactie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld transactie.
	 *
	 * @see genericFormtransactie
	 *
	 * @param iDealKaartjeCode $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld transactie staat en kan
	 * worden bewerkt. Indien transactie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTransactie(iDealKaartjeCode $obj, $include_id = false)
	{
		return TransactieView::defaultForm($obj->getTransactie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld transactie. In
	 * tegenstelling tot formtransactie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtransactie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld transactie staat en kan
	 * worden bewerkt. Indien transactie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTransactie($name, $waarde=NULL)
	{
		return TransactieView::genericDefaultForm('Transactie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * transactie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld transactie representeert.
	 */
	public static function opmerkingTransactie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld gescand.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld gescand labelt.
	 */
	public static function labelGescand(iDealKaartjeCode $obj)
	{
		return 'Gescand';
	}
	/**
	 * @brief Geef de waarde van het veld gescand.
	 *
	 * @param iDealKaartjeCode $obj Het iDealKaartjeCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld gescand van het object obj
	 * representeert.
	 */
	public static function waardeGescand(iDealKaartjeCode $obj)
	{
		return static::defaultWaardeBool($obj, 'Gescand');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld gescand.
	 *
	 * @see genericFormgescand
	 *
	 * @param iDealKaartjeCode $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gescand staat en kan
	 * worden bewerkt. Indien gescand read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGescand(iDealKaartjeCode $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Gescand', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld gescand. In
	 * tegenstelling tot formgescand moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formgescand
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld gescand staat en kan
	 * worden bewerkt. Indien gescand read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGescand($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Gescand');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld gescand
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld gescand representeert.
	 */
	public static function opmerkingGescand()
	{
		return NULL;
	}
}
