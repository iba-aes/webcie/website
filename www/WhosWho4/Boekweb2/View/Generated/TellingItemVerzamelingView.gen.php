<?
abstract class TellingItemVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TellingItemVerzamelingView.
	 *
	 * @param TellingItemVerzameling $obj Het TellingItemVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTellingItemVerzameling(TellingItemVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
