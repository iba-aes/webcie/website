<?
abstract class VerkoopPrijsView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerkoopPrijsView.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerkoopPrijs(VerkoopPrijs $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld voorraad.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld voorraad labelt.
	 */
	public static function labelVoorraad(VerkoopPrijs $obj)
	{
		return 'Voorraad';
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraad van het object obj
	 * representeert.
	 */
	public static function waardeVoorraad(VerkoopPrijs $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getVoorraad());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraad representeert.
	 */
	public static function opmerkingVoorraad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(VerkoopPrijs $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(VerkoopPrijs $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prijs.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld prijs labelt.
	 */
	public static function labelPrijs(VerkoopPrijs $obj)
	{
		return 'Prijs';
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @param VerkoopPrijs $obj Het VerkoopPrijs-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prijs van het object obj
	 * representeert.
	 */
	public static function waardePrijs(VerkoopPrijs $obj)
	{
		return static::defaultWaardeMoney($obj, 'Prijs');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prijs.
	 *
	 * @see genericFormprijs
	 *
	 * @param VerkoopPrijs $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is betreft het een statisch html-element.
	 */
	public static function formPrijs(VerkoopPrijs $obj, $include_id = false)
	{
		return static::waardePrijs($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prijs. In
	 * tegenstelling tot formprijs moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formprijs
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPrijs($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld prijs
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prijs representeert.
	 */
	public static function opmerkingPrijs()
	{
		return NULL;
	}
}
