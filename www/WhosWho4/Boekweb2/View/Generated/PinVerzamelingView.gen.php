<?
abstract class PinVerzamelingView_Generated
	extends TransactieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PinVerzamelingView.
	 *
	 * @param PinVerzameling $obj Het PinVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePinVerzameling(PinVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
