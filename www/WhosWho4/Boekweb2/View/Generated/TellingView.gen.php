<?
abstract class TellingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TellingView.
	 *
	 * @param Telling $obj Het Telling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTelling(Telling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld tellingID.
	 *
	 * @param Telling $obj Het Telling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tellingID labelt.
	 */
	public static function labelTellingID(Telling $obj)
	{
		return 'TellingID';
	}
	/**
	 * @brief Geef de waarde van het veld tellingID.
	 *
	 * @param Telling $obj Het Telling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tellingID van het object obj
	 * representeert.
	 */
	public static function waardeTellingID(Telling $obj)
	{
		return static::defaultWaardeInt($obj, 'TellingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tellingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tellingID representeert.
	 */
	public static function opmerkingTellingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param Telling $obj Het Telling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(Telling $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param Telling $obj Het Telling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(Telling $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wanneer.
	 *
	 * @see genericFormwanneer
	 *
	 * @param Telling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWanneer(Telling $obj, $include_id = false)
	{
		return static::waardeWanneer($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wanneer. In
	 * tegenstelling tot formwanneer moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwanneer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWanneer($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld teller.
	 *
	 * @param Telling $obj Het Telling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld teller labelt.
	 */
	public static function labelTeller(Telling $obj)
	{
		return 'Teller';
	}
	/**
	 * @brief Geef de waarde van het veld teller.
	 *
	 * @param Telling $obj Het Telling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld teller van het object obj
	 * representeert.
	 */
	public static function waardeTeller(Telling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTeller())
			return NULL;
		return LidView::defaultWaardeLid($obj->getTeller());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld teller.
	 *
	 * @see genericFormteller
	 *
	 * @param Telling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld teller staat en kan worden
	 * bewerkt. Indien teller read-only is betreft het een statisch html-element.
	 */
	public static function formTeller(Telling $obj, $include_id = false)
	{
		return static::waardeTeller($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld teller. In
	 * tegenstelling tot formteller moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formteller
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld teller staat en kan worden
	 * bewerkt. Indien teller read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTeller($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld teller
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld teller representeert.
	 */
	public static function opmerkingTeller()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld Opmerking.
	 *
	 * @param Telling $obj Het Telling-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld Opmerking labelt.
	 */
	public static function labelOpmerking(Telling $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld Opmerking.
	 *
	 * @param Telling $obj Het Telling-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(Telling $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld Opmerking.
	 *
	 * @see genericFormOpmerking
	 *
	 * @param Telling $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Opmerking staat en kan
	 * worden bewerkt. Indien Opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(Telling $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld Opmerking. In
	 * tegenstelling tot formOpmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formOpmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld Opmerking staat en kan
	 * worden bewerkt. Indien Opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * Opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
}
