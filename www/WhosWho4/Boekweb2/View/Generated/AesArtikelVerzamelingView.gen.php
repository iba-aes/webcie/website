<?
abstract class AesArtikelVerzamelingView_Generated
	extends ArtikelVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in AesArtikelVerzamelingView.
	 *
	 * @param AesArtikelVerzameling $obj Het AesArtikelVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeAesArtikelVerzameling(AesArtikelVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
