<?
/**
 * $Id$
 */
abstract class iDealVerzamelingView
	extends iDealVerzamelingView_Generated
{
	static public function alleTransacties($artikelen, $transacties, $status, $id, $maand, $jaar)
	{
		global $MAANDEN;

		$page = Page::getInstance();
		$page->start();

		$page->add(new HtmlHeader(1, _("Overzicht iDEAL-transacties")));
		$page->add($form = new HtmlForm("GET"));

		$form->add(new HtmlSpan(_("Laat alle transacties zien met eindstatus: ")));
		$form->add($box = new HtmlSelectbox("status"));
		$box->setAutosubmit();
		$box->add(new HtmlSelectboxOption("*", "*"))
			->add(new HtmlSelectboxOption("gelukt", _("gelukt")))
			->add(new HtmlSelectboxOption("mislukt", _("mislukt")))
			->add(new HtmlSelectboxOption("open", _("open")))
			->add(new HtmlSelectboxOption("inactief", _("inactief")));
		$box->select($status);

		$form->add(new HtmlSpan(_("en artikel: ")));
		$form->add($box = new HtmlSelectbox("artikel"));
		$box->setAutosubmit();
		$box->add(new HtmlSelectboxOption("*", "*"));

		foreach($artikelen as $a) {
			$box->add(new HtmlSelectboxOption($a->getArtikelID(), ArtikelView::waardeNaam($a)));
		}

		$box->select($id);

		$jaren = array();
		foreach(range(2013, date('Y')) as $j)
			$jaren[$j] = "$j";

		$form->add(new HtmlSpan(_("in maand ")));
		$form->add(HtmlSelectbox::fromArray("maand", $MAANDEN, $maand, 1, true)->setMultiple(false));
		$form->add(new HtmlSpan(_("en jaar ")));
		$form->add(HtmlSelectbox::fromArray("jaar", $jaren, $jaar, 1, true)->setMultiple(false));

		$page->add($form = new HtmlForm());
		$form->add(HtmlInput::makeSubmitButton("CSV", "CSV"));

		$page->add(new HtmlHeader(3, sprintf("%s transacties gevonden in de afgelopen maand", $transacties->aantal())));

		$page->add($table = new HtmlTable(NULL, 'sortable' ));
		$table->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("ID")));
		$row->add(new HtmlTableHeaderCell(_("Laatste wijziging")));
		$row->add(new HtmlTableHeaderCell(_("Transactie-ID (ING)")));
		$row->add(new HtmlTableHeaderCell(_("Status")));
		$row->add(new HtmlTableHeaderCell(_("€")));
		$row->add(new HtmlTableHeaderCell(_("Titel")));
		$row->add(new HtmlTableHeaderCell(_("Artikel")));
		$row->add(new HtmlTableHeaderCell(_("Naam")));
		$row->add(new HtmlTableHeaderCell(_("Rekening")));
		$row->add(new HtmlTableHeaderCell(_("E-mailadres")));

		foreach($transacties as $t) {
			$table->add($row = new HtmlTableRow());
			$transactieIDCel = TransactieView::waardeTransactieID($t);
			// Bestuur mag meteen naar de kaartjes-kwijt-pagina
			if (hasAuth('bestuur') && $t->getVoorraad()->getArtikel() instanceof iDealKaartje) {
				$transactieIDCel = new HtmlAnchor(
					sprintf("/Service/iDEALbeheer/kaartjeKwijt?transactieID=%d", $t->getTransactieID()),
					$transactieIDCel
				);
			}
			$row->add(new HtmlTableDataCell($transactieIDCel))
				->add(new HtmlTableDataCell(TransactieView::waardeGewijzigdWanneer($t)))
				->add(new HtmlTableDataCell(($t->getExterneID()) ? iDealView::waardeExterneID($t) : "TO BE ASSIGNED"))
				->add(new HtmlTableDataCell(TransactieView::waardeStatus($t)))
				->add(new HtmlTableDataCell(TransactieView::waardeBedrag($t)))
				->add(new HtmlTableDataCell(iDealView::waardeTitel($t)))
				->add(new HtmlTableDataCell($t->getVoorraad() ? ArtikelView::waardeNaam($t->getVoorraad()->getArtikel()) : '(onbekend)'))
				->add(new HtmlTableDataCell(iDealView::waardeKlantNaam($t)))
				->add(new HtmlTableDataCell(GiroView::waardeTegenrekening($t)))
				->add(new HtmlTableDataCell(iDealView::waardeEmailadres($t)));
		}

		$page->end();
	}

	static public function csv($transacties)
	{
		$csvarray = array();
		foreach($transacties as $t)
			$csvarray[] = iDealView::alsCSVArray($t);

		$output = fopen('php://output' ,'r+');

		if(count($csvarray) > 0)
			fputcsv($output, array_keys($csvarray[0]));
		else
			fwrite($output, _("Geen Data."));

		foreach($csvarray as $ideal)
			fputcsv($output, $ideal);

		fclose($output);
	}

	public static function maakTabelletje($lijst, $vanWie)
	{
		$body = new HtmlElementCollection();

		$table = new HtmlTable(null, 'sortable');
		$table->add($thead = new HtmlTableHead());
		$thead->add($row = new HtmlTableRow());
		$row->addHeader("TransactieID");
		$row->addHeader("Naam $vanWie");
		$row->addHeader("Bedrag $vanWie");
		$row->addHeader("Status $vanWie");
		if($vanWie == 'ING')
		{
			$row->addHeader("Gecheckt");
			$row->addHeader("BatchId");
		}

		//Vertaaltabel voor de status
		//$ingStatus = IDEALTransactieView::getINGStatusArray();

		//Vul de tabel
		foreach($lijst as $key=>$item)
		{
			$row = $table->addRow();
			$row->addData($key);
			$row->addData($item[0]);
			$row->addData($item[1]);
			if($vanWie == 'ING')
			{
				$row->addData($item[2] . '-' . $ingStatus[$item[2]]);
				$row->addData($item[3]);
				$row->addData($item[4]);
			}
			else
			{
				$row->addData($item[2]);
			}
		}
		
		return $body->add($table);
	}
	
	public static function prepareerVergelijkTabel()
	{
		$table = new HtmlTable(null, 'sortable');
		$table->add($thead = new HtmlTableHead());
		$thead->add($row = new HtmlTableRow());
		$row->addHeader("TransactieID");
		$row->addHeader("Naam A-Es");
		$row->addHeader("Naam ING");
		$row->addHeader("Bedrag A-Es");
		$row->addHeader("Bedrag ING");
		$row->addHeader("Status A-Es");
		$row->addHeader("Status ING");
		$row->addHeader("Gecheckt");
		
		return $table;
	}
	
	public static function nieuweRijInVergelijkTabel($key, $aes, $ing, $failstatus)
	{
		$row = new HtmlTableRow();
		$row->addData($key);
		$row->addData(new HtmlSpan($aes[0], $failstatus[1]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($ing[0], $failstatus[1]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($aes[1], $failstatus[2]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($ing[1], $failstatus[2]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($aes[2], $failstatus[0]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($ing[2], $failstatus[0]?'text-danger strongtext':''));
		$row->addData(new HtmlSpan($ing[3], $failstatus[3]?'text-danger strongtext':''));
		
		return $row;
	}

	public static function batchTabel($bekendeArtikelen, $batchBedragen)
	{
		//Maak een mooie tabel van batch-uitbetalingen, ook uitgesplitst per artikel
		$batchTable = new HtmlTable();
		$batchTable->add($batchHeader1 = new HtmlTableRow());
		$batchTable->add($batchHeader2 = new HtmlTableRow());
		$batchHeader1->addHeader("BatchID");
		$batchHeader1->addHeader("Totaal");
		$batchHeader1->addHeader("#");
		$batchHeader2->addEmptyCell();
		$batchHeader2->addEmptyCell();
		$batchHeader2->addEmptyCell();
		foreach($bekendeArtikelen as $voorraadId)
		{
			if($voorraadId != 0) {
				$voorraad = Voorraad::geef($voorraadId);
				$artikel = $voorraad->getArtikel();
				$batchHeader1->addHeader(ArtikelView::waardeNaam($artikel) . " (" . VoorraadView::waardeOmschrijving($voorraad) . ")")->setAttribute('colspan',2);
			} else {
				$batchHeader1->addHeader("Onbekend artikel")->setAttribute('colspan',2);
			}
			$batchHeader2->addHeader("#");
			$batchHeader2->addHeader("&euro;");
		}
		// Eventuele opmerkingen
		$batchHeader1->addHeader(_("Opmerking"));
		$batchHeader2->addEmptyCell();

		//Houd ondertussen wat totalen bij
		$totaalGeld = 0;
		$totaalTransacties = 0;
		$totaalGeldPerBatch = array_fill_keys($bekendeArtikelen, 0); //lang leve php magische functies
		$totaalTransactiesPerBatch = array_fill_keys($bekendeArtikelen, 0);
		//Daar gaan we!
		foreach($batchBedragen as $id=>$batchinfo)
		{
			$batchTable->add($row = new HtmlTableRow());
			$row->addData($id);
			$row->addData('&euro;' . $batchinfo['totaal']/100);
			$row->addData($batchinfo['aantal']);
			foreach($bekendeArtikelen as $artikel)
			{
				if(isset($batchinfo["batches"][$artikel]))
				{
					$row->addData($batchinfo["batches"][$artikel]["aantal"]);
					$totaalTransactiesPerBatch[$artikel] += $batchinfo["batches"][$artikel]["aantal"];
					$row->addData($batchinfo["batches"][$artikel]["bedrag"]/100);
					$totaalGeldPerBatch[$artikel] += $batchinfo["batches"][$artikel]["bedrag"];
				}
				else
				{
					$row->addData("0");
					$row->addData("0");
				}
			}
			$row->addData($batchinfo['opmerking']);
			$totaalGeld += $batchinfo['totaal'];
			$totaalTransacties += $batchinfo['aantal'];
		}

		//De totalen
		$batchTable->add($row = new HtmlTableRow());
		$row->addHeader("Totaal");
		$row->addHeader("&euro;" . $totaalGeld/100);
		$row->addHeader($totaalTransacties);
 		foreach($bekendeArtikelen as $artikel)
		{
			$row->addHeader($totaalTransactiesPerBatch[$artikel]);
			$row->addHeader($totaalGeldPerBatch[$artikel]/100);
		}
		$row->addHeader(""); // geen opmerking bij totaal
		return $batchTable;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
