<?
/**
 * $Id$
 */
abstract class ArtikelView
	extends ArtikelView_Generated
{
	static public function tableRow($artikel)
	{
		$tr = new HtmlTableRow();
		$tr->addData(static::detailsLink($artikel));

		return $tr;
	}

	static public function tableHeaderRow()
	{
		$tr = new HtmlTableRow();
		$tr->addHeader(_('Naam'));

		return $tr;
	}

	static public function detailsLink (Artikel $product)
	{
		if ($product->magBekijken()) {
			return new HtmlAnchor($product->url(),
				self::waardeNaam($product));
		} else {
			return self::waardeNaam($product);
		}
	}

	static public function artikel(Artikel $artikel)
	{
		$class = get_class($artikel) . 'View';

		$page = $class::pageLinks($artikel);
		$page->start(sprintf(_("Artikel %s"), $artikel->geefID()));

		$page->add(new HtmlHeader(3, sprintf(_("Algemene %s-informatie"), get_class($artikel))));
		$page->add($class::viewInfo($artikel, true));

		// BoekCom-specifiekere rechten
		if (hasAuth('bestuur')) {
			$page->add(new HtmlHeader(3, _("Dit artikel wordt gebruikt bij:")));
			$jaargangArts = JaargangArtikelVerzameling::fromArtikel($artikel);
			$page->add(JaargangArtikelVerzamelingView::infoTable($jaargangArts));

			$page->add(new HtmlHeader(3, sprintf(_("Boekcomacties op %s"), get_class($artikel))));
			$page->add($class::boekcomActies($artikel));

			$page->add($class::toonVoorraden($artikel));
		}

		$page->end();
	}

	/**
	 *  Maak een lijst met alle acties die de boekcom kan uitvoeren op dit Artikel.
	 * Breid deze lijst uit als je custom dingen wilt doen (bijvoorbeeld een dictaat bestellen).
	 * @param artikel Het artikel waar het om gaat.
	 * @returns een HtmlList met een ListItem per actie.
	 */
	static public function boekcomActies($artikel)
	{
		$list = new HtmlList();
		// Geen acties hiermee.
		return $list;
	}

	/**
	 *  Maak een div om de voorraden bij dit artikel te bekijken.
	 * @param artikel Het artikel om de voorraden van te bekijken.
	 * @returns een HtmlDiv met een header en een tabel van voorraden.
	 */
	static public function toonVoorraden(Artikel $artikel)
	{
		$voorraden = $artikel->getVoorraden();
		$divje = new HtmlDiv();

		$divje->add(new HtmlHeader(3, _("Alle voorraden")));
		$divje->add(HtmlAnchor::button($artikel->url() . '/Voorraad/Nieuw', _("Voorraad toevoegen")));
		$divje->add(VoorraadVerzamelingView::table($voorraden));
		return $divje;
	}

	static public function wijzigForm($obj, $nieuw = false, $show_error = true)
	{
		if($nieuw)
			$page = Page::getInstance()->start(_("Nieuw artikel toevoegen"));
		else
			$page = Page::getInstance()->start(sprintf(_("Wijzigen van %s"), ArtikelView::waardeNaam($obj)));

		$page->add($form = new HtmlForm("GET"));

		if($nieuw)
		{
			$selects = array();
			$selects['Artikel'] = _('Artikel');
			$selects['AesArtikel'] = _('AesArtikel');
			$selects['Boek'] = _('Boek');
			$selects['DibsProduct'] = _('DibsProduct');
			$selects['Dictaat'] = _('Dictaat');
			$selects['iDealKaartje'] = _('iDealKaartje');

			$form->add(self::makeFormEnumRow('artikel', $selects, null
				, _('Welk soort artikel wil je toevoegen'), null, true));
		}

		$page->add($form = HtmlForm::named('artikelForm'));

		$class = get_class($obj) . "View";

		$form->add($class::wijzigTable($obj, $nieuw, $show_error));
		$form->add(HtmlInput::makeFormSubmitButton(_("Voer in")));

		$page->end();
	}

	static public function zoekForm()
	{
		$form = new HtmlForm('GET');

		$form->add(_("voorraaID:"));
		$form->add(HtmlInput::makeText('id'));
		$form->add(HtmlInput::makeSubmitButton(_("Zoek!")));

		return $form;
	}

	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$tbody = new HtmlDiv();

		$tbody->add(self::wijzigTR($obj, 'naam', $show_error));

		return $tbody;
	}

	static public function processNieuwForm($obj)
	{
		if(!$data = tryPar(self::formobj($obj), NULL))
			return false;

		self::processForm($obj);
	}

	static public function processWijzigForm($obj)
	{
		if(!$data = tryPar(self::formobj($obj), NULL))
			return false;

		self::processForm($obj);
	}

	static public function processZoekForm()
	{
		$id = tryPar('id');
		$voorraad = Voorraad::geef($id);

		return $voorraad;
	}

	static public function zoeken(ArtikelVerzameling $artikelen)
	{
		$page = Page::getInstance()->start(_('Zoek naar een artikel'));

		$page->add($form = HtmlForm::named('BoekZoek'));
		$form->add(self::makeFormStringRow('zoekStr', tryPar('zoekStr')
			, _('Zoekstring')));
		$form->add(HtmlInput::makeFormSubmitButton(_('Zoek')));

		if($artikelen->aantal() != 0)
		{
			$page->add(new HtmlHR());

			$page->add($table = new HtmlTable());
			foreach($artikelen as $artikel)
			{
				$row = $table->addRow();
				$row->addData(self::detailsLink($artikel));
				$row->addData(get_class($artikel));
			}
		}

		$page->end();
	}

	static public function labelNaam(Artikel $obj = NULL)
	{
		return _('Naam');
	}

	static public function defaultWaardeArtikel(Artikel $obj)
	{
		return new HtmlAnchor($obj->url(), self::waardeNaam($obj));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
