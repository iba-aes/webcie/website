<?php

abstract class DictaatOrderView extends DictaatOrderView_Generated {
	/**
	 *  Formulieronderdeel voor een order die hoort bij een losse levering.
	 *
	 * Bevat velden die specifiek voor dictaten zijn, zoals marges en kaften.
	 */
	static public function losseLeveringOrder(Order $order, $show_error)
	{
		$tbody = new HtmlDiv();

		$tbody->add(self::makeFormMoneyRow('totaalWaarde', tryPar('totaalWaarde'), NULL, NULL, _("Totaal gefactureerd")));

		$tbody->add(self::wijzigTR($order, 'datumGeplaatst', $show_error));
		// Worden door Javascript automatisch gevuld op basis van totaalWaarde
		$tbody->add(self::wijzigTR($order, 'waardePerStuk', $show_error));
		$tbody->add(self::makeFormMoneyRow('factuurVerschil', tryPar('factuurVerschil'), NULL, NULL, _("Factuurverschil"), _("Wordt veroorzaakt door afrondingsproblemen.")));

		// Dit vond ik de netste manier om deze leverancieren over te sturen naar de javascript
		$isLeverancierIntern = array();
		foreach (LeverancierVerzameling::dictatenLeveranciers() as $leverancier) {
			$isLeverancierIntern[$leverancier->geefId()] = $leverancier->getDictatenIntern();
		}
		$tbody->addImmutable(HtmlScript::makeJavascript(
			"var leverancierArray = " . json_encode($isLeverancierIntern) . ";\n"
		));

		$tbody->addImmutable(self::wijzigTR($order, 'marge', $show_error));
		$tbody->addImmutable(self::makeFormBoolRow('icaKaft', tryPar('icaKaft'), _('Heeft dit dictaat een fancy informaticadepartementkaft?')));

		// Worden door javascript gebruikt voor automatische margeberekening
		$tbody->addImmutable(HtmlInput::makeHidden('absoluteMargeFactor', Register::getValue('bw2_dictaatAbsoluteMarge')));
		$tbody->addImmutable(HtmlInput::makeHidden('afschrijvingMargeFactor', Register::getValue('bw2_dictaatAfschrijvingMarge')));
		$tbody->addImmutable(HtmlInput::makeHidden('icaKaftPrijs', Register::getValue('bw2_icaKaftPrijs')));

		return $tbody;
	}

	/**
	 *  Verwerk het formulieronderdeel voor een losse levering.
	 * Vult de velden van losseLeveringOrder in in het DictaatOrderobject.
	 * De order moet al een Artikel en Leverancier hebben.
	 *
	 * @see losseLeveringOrder
	 *
	 * @returns een ingevulde DictaatOrder
	 */
	static public function processLosseLeveringOrder(Order $order) {
		parent::processLosseLeveringOrder($order);
		// We halen de prijs van informaticakaften uit het register want anders wordt boekcom sip
		if (tryPar('icaKaft', false)) {
			$icaKaft = Register::getValue('bw2_icaKaftPrijs');
		} else {
			$icaKaft = 0;
		}
		$marge = tryPar('DictaatOrder[Marge]', 0);
		$factuurverschil = tryPar('factuurVerschil', 0);
		$order->setKaftPrijs($icaKaft);
		$order->setMarge($marge);
		$order->setFactuurverschil($factuurverschil);

		return $order;
	}

	static public function opmerkingMarge() {
		return _('Hoe veel marge rekenen we (per stuk)?');
	}

	static public function toCSV($order, $file, $velden, $aantal, $boekdatum, $omschrijving, $alternator)
	{
		$drukprijsPerStuk = $order->getWaardePerStuk();
		$margePerStuk = $order->getMarge();
		$totaalPerStuk = $drukprijsPerStuk + $margePerStuk;

		// Deze regel geeft aan hoeveel we in voorraad hebben.
		static::schrijfCSVRegel(array(
			"Boekdatum" => $boekdatum,
			"Grootboek" => Register::getValue('bw2_voorraadGrootboek'),
			"Omschrijving" => $omschrijving,
			"Bedrag" => $totaalPerStuk * $aantal,
			"Alt" => $alternator,
		), $velden, $file);

		// Deze regel geeft aan hoeveel we aan de drukker hebben betaald.
		static::schrijfCSVRegel(array(
			"Boekdatum" => $boekdatum,
			"Grootboek" => Register::getValue('bw2_dictaatDrukkerGrootboek'),
			"Omschrijving" => $omschrijving,
			"Bedrag" => -$drukprijsPerStuk * $aantal,
			"Alt" => $alternator,
		), $velden, $file);

		// Deze regel geeft aan hoeveel marge we in het boekenfonds stoppen.
		static::schrijfCSVRegel(array(
			"Boekdatum" => $boekdatum,
			"Grootboek" => Register::getValue('bw2_boekenfondsGrootboek'),
			"Omschrijving" => $omschrijving,
			"Bedrag" => -$margePerStuk * $aantal,
			"Alt" => $alternator,
		), $velden, $file);

		if ($kaftprijs = $order->getKaftPrijs()) {
			// Deze regel geeft aan hoeveel extra we betalen voor een kaft van het departement informatica.
			static::schrijfCSVRegel(array(
				"Boekdatum" => $boekdatum,
				"Grootboek" => Register::getValue('bw2_icaKaftGrootboek'),
				"Omschrijving" => $omschrijving,
				"Bedrag" => -$kaftprijs * $aantal,
				"Alt" => $alternator,
			), $velden, $file);
		}
		if ($factuurverschil = $order->getFactuurverschil()) {
			// Deze regel geeft aan of er nog wat geld overblijft door deelbaarheidsproblemen
			static::schrijfCSVRegel(array(
				"Boekdatum" => $boekdatum,
				"Grootboek" => Register::getValue('bw2_factuurverschilGrootboek'),
				"Omschrijving" => sprintf(_("Afrondingsverschillen %s"), $omschrijving),
				"Bedrag" => $factuurverschil,
				"Alt" => $alternator,
			), $velden, $file);
		}
	}
}
