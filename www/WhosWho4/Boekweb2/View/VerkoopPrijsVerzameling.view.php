<?
abstract class VerkoopPrijsVerzamelingView
	extends VerkoopPrijsVerzamelingView_Generated
{
	static public function voorraadGeschiedenis(VerkoopPrijsVerzameling $vps, Voorraad $voorraad)
	{
		$page = Page::getInstance()->start(_("Geschiedenis van de verkoopprijs"));

		$page->add(new HtmlAnchor($voorraad->url(), _("Terug naar de voorraad")));

		$page->add($table = new HtmlTable());
		$table->add($head = new HtmlTableHead());
		$table->add($body = new HtmlTableBody());

		$head->add($row = new HtmlTableRow());
		$row->addHeader(_("Prijs"));
		$row->addHeader(_("Wanneer"));
		$row->addHeader(_("Wie"));

		foreach($vps as $vp) {
			$body->add($row = new HtmlTableRow());
			$row->addData(VerkoopPrijsView::waardePrijs($vp));
			$row->addData(VerkoopPrijsView::waardeWanneer($vp));
			$persoon = Persoon::geef($vp->getGewijzigdWie());
			$row->addData(PersoonView::makelink($persoon));
		}

		$page->end();
	}
}
