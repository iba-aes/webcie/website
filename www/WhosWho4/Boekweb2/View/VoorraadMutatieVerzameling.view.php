<?
/**
 * $Id$
 */
abstract class VoorraadMutatieVerzamelingView
	extends VoorraadMutatieVerzamelingView_Generated
{
	static public function dictaatMutaties (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("Voorraaddetails %s"), DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));

		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar de dictaatpagina')));
		$div->add(HtmlAnchor::button($dictaat->url() . '/Voorraad', _('Terug naar de actuele-voorraadpagina')));

		$page->add(new HtmlHeader(3, _("Mutaties geaggregeerd van alle varianten")));
		$page->add($table = new HtmlTable());
		$table->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("Wanneer")));
		foreach(VoorraadView::labelenumLocatieArray() as $label) {
			$row->add(new HtmlTableHeaderCell($label));
		}
		$row->add(new HtmlTableHeaderCell(_("Omschrijving")));

		foreach($dictaat->getVoorraadMutatieRelaties() as $mutatie) {
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableDataCell(VoorraadMutatieView::waardeWanneer($mutatie)));
			foreach(VoorraadView::labelenumLocatieArray() as $id => $label) {
				if($mutatie->getVoorraad()->getLocatie() == $id) {
					$row->add(new HtmlTableDataCell(VoorraadMutatieView::waardeAantal($mutatie)));
				} else {
					$row->add(new HtmlTableDataCell());
				}
			}
			$row->add(new HtmlTableDataCell(VoorraadMutatieView::type($mutatie)));
		}

		$table->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("Totaal")));
		foreach(VoorraadView::labelenumLocatieArray() as $id => $label) {
			$row->add(new HtmlTableHeaderCell(VoorraadView::waardeAantalArtikel($dictaat, $id)));
		}
		$row->add(new HtmlTableHeaderCell());

		$page->add(new HtmlHeader(3, _("Mutaties per variant")));

		foreach($dictaat->getVoorraden() as $voorraad) {
			$page->add(new HtmlParagraph(sprintf(_("Voorraad %s: %s"), VoorraadView::waardeVoorraadID($voorraad), VoorraadView::waardeOmschrijving($voorraad))));
			$page->add($table = new HtmlTable());
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Wanneer")));
			foreach(VoorraadView::labelenumLocatieArray() as $label) {
				$row->add(new HtmlTableHeaderCell($label));
			}
			$row->add(new HtmlTableHeaderCell(_("Omschrijving")));

			foreach($voorraad->getVoorraadMutatieRelaties() as $mutatie) {
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(VoorraadMutatieView::waardeWanneer($mutatie)));
				foreach(VoorraadView::labelenumLocatieArray() as $id => $label) {
					if($mutatie->getVoorraad()->getLocatie() == $id) {
						$row->add(new HtmlTableDataCell(VoorraadMutatieView::waardeAantal($mutatie)));
					} else {
						$row->add(new HtmlTableDataCell());
					}
				}
				$row->add(new HtmlTableDataCell(VoorraadMutatieView::type($mutatie)));
			}

			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Totaal")));
			foreach(VoorraadView::labelenumLocatieArray() as $id => $label) {
				$row->add(new HtmlTableHeaderCell(VoorraadView::waardeAantalArtikel($dictaat, $id)));
			}
			$row->add(new HtmlTableHeaderCell());
		}

		$page->end();
	}

	static public function toTable($vms)
	{
		$table = new HtmlTable(null, 'sortable table-condensed');

		// TODO: zoek uit waarom dit bestaat en waarom het zo nutteloos is, en of het niet gewoon weg mag

		return $table;
	}

	static public function perProduct($res)
	{
		$page = Page::getInstance()->start(_("Verkopen per product"));

		$page->add(ArtikelView::zoekForm());

		$page->add(new HtmlHR());

		if($res instanceof Voorraad) {
			$vms = $res->getVoorraadMutatieVerkopen();
			$page->add(self::toTable($vms));
		}

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
