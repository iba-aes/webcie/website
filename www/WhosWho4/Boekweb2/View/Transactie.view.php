<?
/**
 * $Id$
 */
abstract class TransactieView
	extends TransactieView_Generated
{
	static public function transactie(Transactie $transactie)
	{
		$class = get_class($transactie) . 'View';

		$page = Page::getInstance();
		$page->start(sprintf(_("Transactie %s"), $transactie->geefID()));

		$page->add($class::viewInfo($transactie, true));

		$page->add(new HtmlHeader(3, _("Verkochte artikelen")));

		$verkopen = $transactie->getVerkopen();

		$page->add($table = new HtmlTable(null, 'table-condensed'));

		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_("Voorraad"));
		$row->addHeader(_("Artikel"));
		$row->addHeader(_("Aantal"));

		foreach($verkopen as $verkoop) {
			$tbody->add($row = new HtmlTableRow());
			$row->addData(new HtmlAnchor($verkoop->getVoorraad()->url(), VoorraadView::waardeVoorraadID($verkoop->getVoorraad())));
			$row->addData(ArtikelView::waardeNaam($verkoop->getVoorraad()->getArtikel()));
			$row->addData(-1*VerkoopView::waardeAantal($verkoop));
		}

		$page->end();
	}

	static public function waardeArtikelen (Transactie $obj)
	{
		$verkopen = $obj->getVerkopen();
		if (!$verkopen->aantal())
			return '';
		$arr = array();
		foreach ($verkopen as $verkoop)
		{
			$artikel = $verkoop->getArtikel();
			$naam = ArtikelView::waardeNaam($verkoop->getArtikel());
			if ($artikel instanceof ColaProduct && hasAuth('bestuur'))
				$naam = new HtmlAnchor($artikel->url(), $naam);
			$aantal = -VerkoopView::waardeAantal($verkoop);
			$arr[] = "$aantal $naam";
		}
		return implode(', ', $arr);
	}

	static public function waardeAantal (Transactie $obj) {
		$verkopen = $obj->getVerkopen();

		$aantal = 0;

		foreach($verkopen as $verkoop) {
			$aantal += $verkoop->getAantal();
		}

		return -1 * $aantal;
	}

	static public function labelUitleg (Transactie $obj = NULL)
	{
		return _('Uitleg');
	}
	static public function labelWanneer (Transactie $obj = NULL)
	{
		return _('Wanneer');
	}
	static public function labelArtikelen (Transactie $obj = NULL)
	{
		return _('Artikelen');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
