<?

abstract class BoekverkoopView
	extends BoekverkoopView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde($obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function tableHeaderRow()
	{
		$tr = new HtmlTableRow();
		$tr->addHeader(_("Verkoopdag"));
		$tr->addHeader(_("Volgnummer"));
		$tr->addHeader(_("Open"));
		$tr->addHeader(_("Opener"));
		$tr->addHeader(_("Gesloten"));
		$tr->addHeader(_("Sluiter"));
		$tr->addHeader(_("Pin1"));
		$tr->addHeader(_("Pin2"));
		$tr->addHeader(_("Kasverschil"));
		$tr->addHeader(_("Opmerking"));

		return $tr;
	}

	static public function tableRow(Boekverkoop $verkoop)
	{
		$tr = new HtmlTableRow();

		$tr->addData(new HtmlAnchor($verkoop->url(), self::waardeVerkoopdag($verkoop)));
		$tr->addData(self::waardeVolgnummer($verkoop));
		$tr->addData(self::waardeOpen($verkoop));
		$tr->addData(self::waardeOpener($verkoop));
		$tr->addData(self::waardeGesloten($verkoop));
		$tr->addData(self::waardeSluiter($verkoop));
		$tr->addData(self::waardePin1($verkoop));
		$tr->addData(self::waardePin2($verkoop));
		$tr->addData(self::waardeKasverschil($verkoop));
		$tr->addData(self::waardeOpmerking($verkoop));

		return $tr;
	}

	static public function processNieuwForm(Boekverkoop $verkoop)
	{
		if (!$data = tryPar(self::formobj($verkoop), NULL))
		{
			return false;
		}

		self::processForm($verkoop);
	}

	static public function open(Boekverkoop $boekverkoop, $alGeopend, $show_error = false)
	{
		$page = Page::getInstance()->start(_("Open boekverkoop"));

		if ($alGeopend)
		{
			$page->add(new HtmlDiv(new HtmlParagraph(
				"De boekverkoop is vandaag al een keer geopend!"
			), 'bs-callout bs-callout-danger'));
		}

		$page->add(new HtmlParagraph(_("Weet je zeker dat je de boekverkoop wilt openen?")));
		$page->add($form = HtmlForm::named("OpenBoekverkoop"));

		$form->add(self::wijzigTR($boekverkoop, 'Opmerking', $show_error));
		$form->add(HtmlInput::makeFormSubmitButton("Jazeker! Open die shit!"));

		$page->end();
	}

	static public function verkoop(Boekverkoop $verkoop, ArtikelVerzameling $artikelen)
	{
		global $session;

		$verkoopURL = BOEKWEBBASE . 'Verkoop/';
		$page = Page::getInstance()->start(_("Boekverkoop"));

		$page->add($winkeldiv = new HtmlDiv(null, 'row'));
		$winkeldiv->add($klantdiv = new HtmlDiv(null, 'col-md-6'));
		$winkeldiv->add($kassadiv = new HtmlDiv(null, 'col-md-6'));

		$klant = Persoon::geef($session->get('boekverkoopklant'));

		$klantdiv->add(new HtmlHeader(3, _("Klant")));
		$klantdiv->add($form = HtmlForm::named('LidWijzigen', $verkoopURL));
		$form->addClass('form-inline');

		$form->add(HtmlInput::makeText('lid'));
		$form->add(HtmlInput::makeSubmitButton('Wijzig'));
		$klantdiv->add(new HtmlBreak());
		//$klantdiv->add($infodiv = new HtmlDiv(null, 'row'));
		$klantdiv->add(new HtmlDiv(PersoonView::kop($klant), 'col-md-3'));
		$lidstudies = LidStudieVerzameling::vanLid($klant, true);
		$klantdiv->add($divrechts = new HtmlDiv(null, 'col-md-9'));
		if ($lidstudies->aantal() == 0)
		{
			$divrechts->add(new HtmlSpan(_("Persoon heeft geen studies"), 'text-danger strongtext'));
		}
		else
		{
			foreach ($lidstudies as $lidstudie)
			{
				$divrechts->add(LidStudieView::toString($lidstudie));
				$divrechts->add(new HtmlBreak());
			}
		}
		if ($opm = $klant->getOpmerkingen())
		{
			$divrechts->add(new HtmlBreak());
			$divrechts->add(new HtmlSpan(PersoonView::waardeOpmerkingen($klant), 'text-danger strongtext'));
		}

		$kassadiv->add(new HtmlHeader(3, _("Kassa")));
		if ($session->has('boekverkoopwinkel'))
		{
			$kassadiv->add($table = new HtmlTable());
			$total = 0;
			$totalbtw = 0;
			foreach ($session->get('boekverkoopwinkel') as $id => $aantal)
			{
				$table->add($tr = new HtmlTableRow());
				$voorraad = Voorraad::geef($id);
				$tr->addData($aantal);
				$tr->addData(array(ArtikelView::waardeNaam($voorraad->getArtikel()),
					new HtmlBreak(),
					$span = new HtmlSpan(sprintf(_('Uit voorraad: %s'),
						VoorraadView::waardeLocatie($voorraad)))));
				$span->setCssStyle('font-size: 75%');
				$tr->addData(Money::addPrice($aantal * $voorraad->getVerkoopPrijs()->getPrijs()));
				$tr->addData(VoorraadView::waardeBtw($voorraad));
				$btw = $voorraad->getBtw() * 0.01 * $aantal;
				$tr->addData(Money::addPrice($btw * $voorraad->getVerkoopPrijs()->getPrijs()));
				$tr->addData($form = HtmlForm::named('Delete', $verkoopURL));
				$form->add(HtmlInput::makeSubmitButton('X'));
				$form->add(HtmlInput::makeHidden('voorraad', $voorraad->geefID()));
				$total += $aantal * $voorraad->getVerkoopPrijs()->getPrijs();
				$totalbtw += $btw * $voorraad->getVerkoopPrijs()->getPrijs();
			}

			$table->add($tr = new HtmlTableRow());
			$tr->addData();
			$tr->addData(_("Totaal:"));
			$tr->addData(Money::addPrice($total));
			$tr->addData(_("BTW:"));
			$tr->addData(Money::addPrice($totalbtw));

			$kassadiv->add($form = HtmlForm::named('Verkopen'));
			$form->add($divje = new HtmlDiv(null, 'row'));
			$divje->add(new HtmlDiv(HtmlInput::makeSubmitButton('Pinautomaat 1', 'betaal[pin1]'), 'col-md-6'));
			$divje->add(new HtmlDiv(HtmlInput::makeSubmitButton('Pinautomaat 2', 'betaal[pin2]'), 'col-md-6'));
			$form->add(HtmlInput::makeCheckbox('bevestiging', ($total > 15 ? true : false), null, null, _("Verstuur aankoopbevestiging")));
		}

		$page->add($artikeldiv = new HtmlDiv(null, 'row'));

		$artikeldiv->add($dictaatdiv = new HtmlDiv(null, 'col-md-6'));
		$dictaatdiv->add(new HtmlHeader(3, _("Dictaten")));
		$dictaatdiv->add($dictaatTable = new HtmlTable(null, 'table-condensed'));

		$artikeldiv->add($restdiv = new HtmlDiv(null, 'col-md-6'));
		$restdiv->add(new HtmlHeader(3, _("Overigen")));
		$restdiv->add($restTable = new HtmlTable(null, 'table-condensed'));

		foreach ($artikelen as $artikel)
		{
			$voorraden = VoorraadVerzameling::verkoopbaarVanArtikel($artikel);
			foreach ($voorraden as $voorraad)
			{
				if ($artikel instanceof Dictaat)
				{
					$table = $dictaatTable;
				}
				else
				{
					$table = $restTable;
				}
				$prijs = $voorraad->getVerkoopPrijs();
				$select = new HtmlSelectbox('aantal');
				if ($session->has('boekverkoopwinkel/' . $voorraad->geefID()))
				{
					$inwinkel = $session->get('boekverkoopwinkel/' . $voorraad->geefID());
				}
				else
				{
					$inwinkel = 0;
				}
				if ($voorraad->getAantal() - $inwinkel == 0)
				{
				}
				else
				{
					for ($i = 1; $i <= min($voorraad->getAantal() - $inwinkel, 50); $i++)
					{
						$select->add(new HtmlSelectboxOption($i, $i));
					}
				}
				$table->add($row = new HtmlTableRow());
				$row->addData($form = HtmlForm::named('Artikelen', $verkoopURL));
				$form->add($select);
				$form->add(HtmlInput::makeSubmitButton(_("Naar winkel"), $voorraad->geefID()));
				$row->addData(array(ArtikelView::waardeNaam($artikel) . " (" . VerkoopPrijsView::waardePrijs($prijs) . ")",
					new HtmlBreak(), $span = new HtmlSpan(sprintf(_('Uit voorraad: %s'),
						VoorraadView::waardeLocatie($voorraad)))));
				$span->setCssStyle('font-size: 75%');
				$form->add(HtmlInput::makeHidden('voorraad', $voorraad->geefID()));
			}
		}

		$page->add($a = new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Sluitverkoop', _('Sluit de boekverkoop')));
		$a->addClass('btn btn-primary');

		$page->end();
	}

	static public function sluitVerkoop(Boekverkoop $verkoop, $verkopenPin, $msg)
	{
		$page = Page::getInstance();

		if ($verkoop->getSluiter())
		{
			Page::addMelding(_("Deze boekverkoop is gesloten"), 'succes');
		}

		$page->start(_("Sluit boekverkoop"));

		$page->add($form = HtmlForm::named('sluit'));
		$form->add($table = new HtmlTable());
		$form->addClass('form-inline');
		$table->add($row = new HtmlTableRow());
		$row->addData(_("Pin 1:"));
		$row->addData(BoekverkoopView::formPin1($verkoop));
		$table->add($row = new HtmlTableRow());
		$row->addData(_("Pin 2:"));
		$row->addData(BoekverkoopView::formPin2($verkoop));

		if (!($verkoop->getSluiter()))
		{
			$form->add(HtmlInput::makeSubmitButton(_('Sluit de boekverkoop')));
		}
		else
		{
			$form->add(HtmlInput::makeSubmitButton(_('Corrigeer de getalen')));
		}

		if (sizeof($msg) != 0 || $verkoop->getSluiter())
		{
			$page->add(new HtmlHeader(3, sprintf(_("Boekverkoop geopend op %s door %s"), BoekverkoopView::waardeOpen($verkoop), PersoonView::naam($verkoop->getOpener()))));

			$page->add(new HtmlHeader(4, _("Verkochte artikelen")));

			$page->add($verkochtTable = new HtmlTable());
			$verkochtTable->add($thead = new HtmlTableHead());
			$verkochtTable->add($verkochtBody = new HtmlTableBody());

			$thead->add($row = new HtmlTableRow());
			$row->addHeader(_("VoorraadID"));
			$row->addHeader(_("Artikel"));
			$row->addHeader(_("Aantal"));
			$row->addHeader(_("Prijs"));
			$row->addHeader(_("Totaal"));

			$verkocht = array();

			foreach ($verkopenPin as $id => $verkoopPin)
			{
				$page->add(new HtmlHeader(4, sprintf(_("Pin%s"), $id)));

				$page->add($table = new HtmlTable());

				$table->add($thead = new HtmlTableHead());
				$table->add($tbody = new HtmlTableBody());

				$thead->add($row = new HtmlTableRow());
				$row->addHeader(_("Wanneer"));
				$row->addHeader(_("Soort"));
				$row->addHeader(_("Contact"));
				$row->addHeader(_("Bedrag"));
				$row->addHeader(_("Uitleg"));
				$row->addHeader(_("Status"));

				foreach ($verkoopPin as $trans)
				{
					$tbody->add($row = new HtmlTableRow());
					$row->addData(TransactieView::waardeWanneer($trans));
					$row->addData(TransactieView::waardeSoort($trans));
					$row->addData(TransactieView::waardeContact($trans));
					$row->addData(TransactieView::waardeBedrag($trans));
					$row->addData(TransactieView::waardeUitleg($trans));
					$row->addData(TransactieView::waardeStatus($trans));
					$verkopen = VerkoopVerzameling::vanTransactie($trans);
					$row->addData($innerTable = new HtmlTable());
					$totaal = 0;
					foreach ($verkopen as $verk)
					{
						$voorraad = $verk->getVoorraad();
						$artikel = $voorraad->getArtikel();

						if (!isset($verkocht[$voorraad->geefID()]))
						{
							$verkocht[$voorraad->geefID()] = $verk->getAantal();
						}
						else
						{
							$verkocht[$voorraad->geefID()] += $verk->getAantal();
						}

						$innerTable->add($row = new HtmlTableRow());
						$row->addData(ArtikelView::waardeNaam($artikel));
						$row->addData(-1 * VerkoopView::waardeAantal($verk));
						$row->addData(VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs()));
						$verkTot = -1 * $voorraad->getVerkoopPrijs()->getPrijs() * $verk->getAantal();
						$totaal += $verkTot;
						$row->addData(Money::addPrice($verkTot));
					}
					$innerTable->add($row = new HtmlTableRow());
					$row->addData(new HtmlStrong(_("Totaal:")));
					$row->addData();
					$row->addData();
					$row->addData(new HtmlStrong(Money::addPrice($totaal)));
				}
			}

			$totaal = 0;
			foreach ($verkocht as $id => $aantal)
			{
				$verkochtBody->add($row = new HtmlTableRow());
				$voorraad = Voorraad::geef($id);
				$artikel = $voorraad->getArtikel();
				$row->addData(VoorraadView::detailsLink($voorraad));
				$row->addData(ArtikelView::waardeNaam($artikel));
				$row->addData(-1 * $aantal);
				$row->addData(VerkoopPrijsView::waardePrijs($prijs = $voorraad->getVerkoopPrijs()));
				$totaal += $prijs->getPrijs() * $aantal * -1;
				$row->addData(Money::addPrice($prijs->getPrijs() * $aantal * -1));
			}

			$verkochtBody->add($row = new HtmlTableRow());
			$row->addData();
			$row->addData();
			$row->addData();
			$row->addData(new HtmlStrong(_("Totaal:")));
			$row->addData(new HtmlStrong(Money::addPrice($totaal)));
		}

		$page->end();
	}
}

