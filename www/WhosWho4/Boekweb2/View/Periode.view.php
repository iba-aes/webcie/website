<?
abstract class PeriodeView
	extends PeriodeView_Generated
{
	/**
	 * \brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * \param obj Het -object waarvan de waarde kregen moet worden.
	 * \return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde($obj)
	{
		return self::naam($obj);
	}

	/**
	 * @brief Een menselijk leesbare omschrijving van de periode.
	 * @param per De periode waarvan we de omschrijving willen.
	 * @returns Een string met menselijk leesbare naam.
	 */
	public static function naam (Periode $per) {
		return self::waardeCollegejaar($per) . " " . self::waardePeriodeNummer($per);
	}

	/**
	 * \brief geef een formulier om een nieuwe periode te maken.
	 * Indien de periode null is, wordt die automagisch aangemaakt.
	 * @param obj De periode waar de inhoud gezet moet worden.
	 * @returns een html-formulier om in je pagina te zetten.
	 */
	public static function nieuwForm(Periode $obj = null) {
		if (is_null($obj)) {
			$obj = new Periode();
		}

		$form = HtmlForm::named("nieuwePeriode", BOEKWEBBASE."Periode/Nieuw");
		$form->add(self::createForm($obj));
		$form->add(HtmlInput::makeSubmitButton(_("Nieuwe periode")));
		return $form;
	}

	/**
	 *  Geef een formulier om een periode te wijzigen.
	 * @param obj De periode waar de inhoud gezet moet worden.
	 * @returns Een HTML-formulier om in je pagina te zetten.
	 */
	public static function wijzigForm(Periode $obj) {
		$form = HtmlForm::named("wijzigPeriode");
		$form->add(self::createForm($obj));
		$form->add(HtmlInput::makeSubmitButton(_("Wijzig periode")));
		return $form;
	}
}
