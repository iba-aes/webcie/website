<?
/**
 * $Id$
 */
abstract class iDealView
	extends iDealView_Generated
{
	public static function geefFrame($titel, $inhoud)
	{
		// LET OP: hier doen we de css in de html omdat we in cleanhtml-pagina's
		// geen css-bestanden hebben
		$idealDiv = new HtmlDiv();
		$idealDiv->setAttribute('style', 'border: solid 4px #CC0066; padding: 10px');
		$idealDiv->add($img = new HtmlImage('/Layout/Images/ideal.gif', 'iDEAL'));
		$img->setCssStyle('float: right');
		$idealDiv->add($img = new HtmlImage('/Layout/Images/logo.png', 'A-Eskwadraat'));
		$img->setCssStyle('float: right');

		return $idealDiv->add(new HtmlHeader(2, $titel))->add($inhoud);
	}

	public static function geefInfoBox($transactie, $showResult = false)
	{
		$table = new HtmlTable();

		if(DEBUG)
		{
			$row = $table->addRow();
			$row->addData(_('Interne ID: '));
			$row->addData(iDealView::waardeTransactieID($transactie));

			$row = $table->addRow();
			$row->addData(_('Externe ID: '));

			if($transactie->getExterneID()) {
				$row->addData(iDealView::waardeExterneID($transactie));
			} else {
				$row->addData(_('TO BE ASSIGNED'));
			}

			$row = $table->addRow();
			$row->addData(_('Status: '));
			$row->addData(iDealView::waardeStatus($transactie));

			$row = $table->addRow();
			$row->addData(_('E-mailadres: '));
			$row->addData(iDealView::waardeEmailadres($transactie));
		}

		// Externe info
		$row = $table->addRow();
		$row->addData(_('Transactie: '));
		$row->addData(iDealView::waardeTitel($transactie));

		$row = $table->addRow();
		$row->addData(_('Omschrijving: '));
		$row->addData(iDealView::waardeUitleg($transactie));

		$row = $table->addRow();
		$row->addData(_('Bedrag: '));
		$row->addData(Money::addPrice($transactie->getBedrag()));

		if($showResult)
		{
			$row = $table->addRow();
			$row->addData(new HtmlStrong(_('Uw gegevens: ')));

			$row = $table->addRow();
			$row->addData( _("Rekeningnummer: "));
			$row->addData($transactie->getTegenRekening() . _(" t.n.v. ") . $transactie->getKlantNaam());
		}

		return $table;
	}

	public static function toonOverzicht($transactie, $errMsg = '')
	{
		$idealDiv = new HtmlElementCollection();
		$idealDiv->add(self::geefInfoBox($transactie));

		if(!empty($errMsg)) {
			$idealDiv->add($span = new HtmlSpan($errMsg, 'text-danger strongtext'));
			$span->setCssStyle('color: red; font-weight: bold');
		}

		//Let op: volgens het protocol moet de eerste optie "Kies uw bank..." zijn
		if(tryPar('cleanhtml'))
			$form = new HtmlForm('post', HTTPS_ROOT . '/Service/IDEAL/bankGekozen?cleanhtml=1');
		else
			$form = new HtmlForm('post', HTTPS_ROOT . '/Service/IDEAL/bankGekozen');

		$form->setToken(new Token("kiesBank"));
		$form->add(new HtmlParagraph(_('Kies uw bank om de betaling te voltooien: ')));
		$form->add(HtmlInput::makeHidden('transid', $transactie->getTransactieID()));

		if(tryPar('cleanhtml'))
			$form->add(HtmlInput::makeHidden('cleanhtml',1));

		$form->add(HtmlInput::makeSubmitButton(_('Start betalen')));

		$idealDiv->add($form);

		return self::geefFrame(_("iDEAL Transactie-overzicht"), $idealDiv);
	}

	public static function toon($transactie)
	{
		switch($transactie->getStatus())
		{
		case 'OPEN':
			return self::toonOpen($transactie, 5);
			break;
		case 'FAILURE':
		case 'CANCELLED':
			return self::toonFaal($transactie);
			break;
		case 'SUCCESS':
			return self::toonSuccess($transactie);
			break;
		}
	}

	public static function toonOpen($transactie, $tijd)
	{
		$idealDiv = new HtmlElementCollection();

		$idealDiv->add(self::geefInfoBox($transactie));
		$idealDiv->add(new HtmlParagraph(sprintf(_("Het is nog niet gelukt om uw transactie "
			. "af te handelen, u zult spoedig hierover bericht moeten krijgen in uw inbox. "
			. "Mocht dit na maximaal een kwartier nog steeds niet het geval zijn, "
			. "neem dan contact op met %s."), new HtmlAnchor('mailto:webcie@a-eskwadraat.nl', _('WebCie')))));

		return self::geefFrame(_("De transactie is nog niet verwerkt"), $idealDiv);
	}

	public static function toonFaal($transactie)
	{
		$idealDiv = new HtmlElementCollection();
		$idealDiv->add(self::geefInfoBox($transactie));

		$status = $transactie->getStatus();
		switch($status)
		{
			case 'CANCELLED':
				$msg = _("U heeft de transactie geannuleerd.");
				break;
			case 'FAILURE':
				$msg = _("De transactie is mislukt.");
				break;
			case 'EXPIRED':
				$msg = _("De transactie is verlopen voordat deze voltooid is.");
				break;
		}

		$idealDiv->add(new HtmlParagraph($msg, 'text-danger strongtext'));

		$form = new HtmlForm('post', HTTPS_ROOT . '/Service/IDEAL/nieuwePoging');
		$form->setToken(new Token("nieuwePoging"));
		$form->add(new HtmlParagraph(_('Druk op de knop om opnieuw te proberen betalen:')));
		$form->add(HtmlInput::makeHidden('transid', $transactie->geefID()));
		$form->add(HtmlInput::makeSubmitButton(_('Probeer opnieuw')));

		if(tryPar('cleanhtml'))
			$form->add(HtmlInput::makeHidden('cleanhtml',1));

		$idealDiv->add($form);

		$idealDiv->add(new HtmlParagraph(sprintf(_("Als het u niet lukt te "
			. "betalen kunt u lezen hoe het verder moet op %s[VOC: 'deze pagina']."),
			new HtmlAnchor($transactie->getReturnUrl() . "mislukt'", _("deze pagina")))));

		return self::geefFrame(_("De transactie is mislukt!"), $idealDiv);
	}

	public static function toonSuccess($transactie)
	{
		$idealDiv = new HtmlElementCollection();
		$idealDiv->add(self::geefInfoBox($transactie));

		if($transactie->getReturnURL())
			$idealDiv->add(new HtmlParagraph(sprintf(_('U kunt %s[VOC: "hier"] terug gaan'),
				new HtmlAnchor($transactie->getReturnURL(), _('hier')))));

		return self::geefFrame(_('De transactie is gelukt!'), $idealDiv);
	}

	public static function klantInfo($transactie)
	{
		return _('Transactie: ') . htmlspecialchars(iDealView::waardeTitel($transactie)) . "\r\n" . 
				_("Omschrijving: ") . htmlspecialchars(iDealView::waardeUitleg($transactie)) . "\r\n" . 
//				_("Artikel-ID: ") . htmlspecialchars(ArtikelView::waardeNaam($transactie->getArtikel())) . "\r\n" . 
				_("Bedrag: ") . Money::addPrice($transactie->getBedrag());
	}

	public static function algemeneInfo()
	{
		return self::geefFrame(_("iDEAL bij A&ndash;Eskwadraat"), new HtmlParagraph(_("Heb je vragen? Mail dan het bestuur of de WebCie!")));
	}

	public static function alsCSVArray($transactie)
	{
		return array(
			"ID" 					=> TransactieView::waardeTransactieID($transactie),
			"Laatste wijziging" 	=> TransactieView::waardeGewijzigdWanneer($transactie),
			"Transactie-ID (ING)" 	=> ($transactie->getExterneID()) ? iDealView::waardeExterneID($transactie) : "TO BE ASSIGNED",
			"Status" 				=> TransactieView::waardeStatus($transactie),
			"Euro" 					=> $transactie->getBedrag(),
			"Titel" 				=> iDealView::waardeTitel($transactie),
			"Artikel" 				=> ArtikelView::waardeNaam($transactie->getVoorraad()->getArtikel()),
			"Naam" 					=> iDealView::waardeKlantNaam($transactie),
			"Rekening" 				=> GiroView::waardeTegenrekening($transactie),
			"E-mailadres" 			=> iDealView::waardeEmailadres($transactie)
		);
	}

	public static function getINGStatusArray()
	{
		//De codes die de ING gebruikt in zijn CSV's
		return array(
			'000' => 'Betalingsverzoek ontvangen',
			'001' => 'Betalingsverzoek verwerkt',
			'002' => 'Betalingsverzoek niet verwerkt',
			'003' => 'Betaling bevestigd door bank',
			'004' => 'Betaling geannuleerd door koper',
			'005' => 'Betalingsverzoek verlopen',
			'006' => 'Betaling mislukt',
			'007' => 'Betaling gereconcilieerd',
			'008' => 'Betaling niet gereconcilieerd',
			'009' => 'Uitbetaald',
			'010' => 'Niet uitbetaald'
		);
	}

	public static function geefGeneriekeMeldingEnStop($titel, $tekst, $t = null)
	{
		$idealDiv = iDealView::geefFrame($titel, new HtmlSpan($tekst, 'text-danger strongtext'));
		if($t == null)
		{
			//contactinformatie niet bekend
			$idealDiv->add(new HtmlParagraph(_("Voor meer informatie kan je mailen naar het bestuur: bestuur@a-eskwadraat.nl")));
		}
		else
		{
			//contactinformatie bekend
			$idealDiv->add(new HtmlParagraph(_("Verdere instructies kun je vinden "
				. "op de pagina van de organiserende commissie.")))
				->add(new HtmlAnchor($t->getReturnUrl() . "mislukt", _('Klik')));
		}
		if(tryPar('cleanhtml')) {
    		Page::getInstance('cleanhtml')->start();
		    Page::getInstance('cleanhtml')->add($idealDiv)->end();
        } else {
    		Page::getInstance()->start();
		    Page::getInstance()->add($idealDiv)->end();
        }
		exit();
	}

	static public function transactieDiv($transactie)
	{
		$div = new HtmlDiv();
		$div->add(iDealView::geefInfoBox($transactie));

		$action = "/Service/IDEAL/bankGekozen";

		$div->add($form = HtmlForm::named('kiesBank', $action));
		$form->add(new HtmlParagraph(_('Kies uw bank om de betaling te voltooien')));

		$form->add($select);
		$form->add(HtmlInput::makeHidden('transid', $transactie->geefID()));

		if(tryPar('cleanhtml'))
			$form->add(HtmlInput::makeHidden('cleanhtml', 1));

		$form->add(HtmlInput::makeSubmitButton(_('Start betalen')));

		if(sizeof($problemen) != 0)
		{
			$div->add($probDiv = new HtmlDiv());
			//Even geen nette class, want we hebben niet altijd een stylesheet
			$probDiv->setAttribute('style', 'color:red');
			$probDiv->add(new HtmlParagraph(_("Let op! Op dit moment zijn er problemen in het online betaalverkeer:")))
				->add($ul = new HtmlList());
			foreach($problemen as $bank => $probleem)
				$ul->add(new HtmlListItem($bank .": " . $probleem));
		}

		return iDealView::geefFrame(_('iDEAL-transactie-overzicht'), $div);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
