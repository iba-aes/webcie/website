<?
/**
 * $Id$
 */
abstract class TransactieVerzamelingView
	extends TransactieVerzamelingView_Generated
{
	/**
	 *  Produceer een pagina met een overzicht van alle transacties in de verzameling.
	 * Dit overzicht bevat ook een formpje om een ander overzicht te maken.
	 * @param transacties De transacties om te tonen.
	 * @param begin De geselecteerde begindatum.
	 * @param eind De geselecteerde einddatum.
	 * @param rubriek De (index van) geselecteerde rubriek (COLAKAS, BOEKWEB, ...)
	 * @param soort De (index van) geselecteerde soort (VERKOOP, DONATIE, ...)
	 * @return Helemaal niets, doet alles via Page.
	 */
	static public function overzicht(TransactieVerzameling $transacties, $begin, $eind, $rubriek, $soort)
	{
		$page = Page::getInstance();

		$page->start(_("Transactieoverzicht"));

		$page->add($form = new HtmlForm('get'));
		$form->addClass('form-inline');
		$form->setName('transacties');
		$form->add(new HtmlSpan(_("Bekijk alle transacties van ")));
		$form->add(HtmlInput::makeDate('begin', $begin));
		$form->add(new HtmlSpan(_(" tot ")));
		$form->add(HtmlInput::makeDate('eind', $eind));
		$form->add(HtmlInput::makeSubmitButton("Go!"));
		$form->add(new HtmlBreak());
		$form->add(new HtmlSpan(_("Of filter op rubriek: ")));
		$form->add($select = HtmlSelectbox::fromArray('rubriek', array(null => null) + Transactie::enumsRubriek(), is_null($rubriek) ? array() : $rubriek, 1));
		$form->add(new HtmlSpan(_(" soort: ")));
		$form->add($select = HtmlSelectbox::fromArray('soort', array(null => null) + Transactie::enumsSoort(), is_null($soort) ? array() : $soort, 1));

		$page->add(self::makeTable($transacties,
			array('TransactieID', 'Rubriek', 'Soort', 'Contact', 'Bedrag', 'Aantal', 'Uitleg', 'Wanneer', 'Status'),
			array(), 'sortable table-condensed', true));
		$page->end();
	}

	static public function verkoopOverzicht(TransactieVerzameling $transacties, $begin, $eind)
	{
		$page = Page::getInstance();
		$page->start(sprintf(_("Verkoopoverzicht van %s"), $begin->format("d-m-Y")));

		$page->add(new HtmlHeader(4, _("Verkochte artikelen")));

		$page->add(self::productenTable($transacties));

		$page->end();
	}

	static public function productenTable(Transacties $transacties)
	{
		$verkopen = $transacties->getVerkopen();

		$voorraden = new VoorraadVerzameling();

		foreach($verkopen as $verkoop) {
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
