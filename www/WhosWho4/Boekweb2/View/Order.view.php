<?
/**
 * $Id$
 */
abstract class OrderView
	extends OrderView_Generated
{
	/**
	 *  Formulieronderdeel voor een order die hoort bij een losse levering.
	 */
	static public function losseLeveringOrder(Order $order, $show_error)
	{
		$tbody = new HtmlDiv();

		$tbody->add(self::wijzigTR($order, 'datumGeplaatst', $show_error));
		$tbody->add(self::wijzigTR($order, 'waardePerStuk', $show_error));

		return $tbody;
	}

	/**
	 *  Verwerk het formulieronderdeel voor een losse levering.
	 * Vult de velden van losseLeveringOrder in in het Orderobject.
	 * De order moet al een Artikel en Leverancier hebben.
	 *
	 * @see losseLeveringOrder
	 *
	 * @returns een ingevulde Order.
	 */
	static public function processLosseLeveringOrder(Order $order) {
		self::processForm($order);
		// haal het aantal uit de leveringform
		$order->setAantal(tryPar('aantal', 0));

		return $order;
	}

	static public function waardeLeverancier (Order $order, $link = true)
	{
		if ($order->getLeverancier() instanceof Leverancier)
			if ($link)
				return LeverancierView::detailsLink($order->getLeverancier());
			else
				return LeverancierView::waardeNaam($order->getLeverancier());
		return null;
	}

	static public function waardeArtikel (Order $order, $link = true)
	{
		if ($order->getArtikel() instanceof Artikel)
			if ($link)
				return ArtikelView::detailsLink($order->getArtikel());
			else
				return ArtikelView::waardeNaam($order->getArtikel());
		return null;
	}

	static public function toCSV($order, $file, $velden, $aantal, $boekdatum, $omschrijving, $alternator)
	{
		$prijsPerStuk = $order->getWaardePerStuk();

		// Deze regel geeft aan hoeveel we in voorraad hebben.
		static::schrijfCSVRegel(array(
			"Boekdatum" => $boekdatum,
			"Grootboek" => Register::getValue('bw2_voorraadGrootboek'),
			"Omschrijving" => $omschrijving,
			"Bedrag" => $prijsPerStuk * $aantal,
			"Alt" => $alternator,
		), $velden, $file);

		// Deze regel geeft aan hoeveel we aan de leverancier hebben betaald.
		static::schrijfCSVRegel(array(
			"Boekdatum" => $boekdatum,
			"Grootboek" => Register::getValue('bw2_dictaatDrukkerGrootboek'),
			"Omschrijving" => $omschrijving,
			"Bedrag" => -$prijsPerStuk * $aantal,
			"Alt" => $alternator,
		), $velden, $file);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
