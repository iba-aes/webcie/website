<?
abstract class VakStudieView
	extends VakStudieView_Generated
{
	/**
	 * \brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in VakStudieView.
	 *
	 * \param obj Het VakStudie-object waarvan de waarde kregen moet worden.
	 * \return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVakStudie(VakStudie $obj)
	{
		return self::defaultWaarde($obj);
	}

}
