<?
abstract class PeriodeVerzamelingView
	extends PeriodeVerzamelingView_Generated
{
	/**
	 * \brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * \param obj Het -object waarvan de waarde kregen moet worden.
	 * \return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Toon alle (gegeven) periodes in een mooi tabelletje.
	 * @param pers Indien NULL, laad ze allemaal uit de databaas.
	 * @returns Een HtmlTable
	 */
	public static function periodeOverzicht(PeriodeVerzameling $pers = null) {
		if (is_null($pers)) {
			$pers = PeriodeQuery::table()->verzamel();
		}
		$tabel = new HtmlTable();
		$tabel->add(new HtmlTableHead($row = new HtmlTableRow()));
		$row->addHeader(_("Collegejaar"));
		$row->addHeader(_("Nummer"));
		$row->addHeader(_("Begindatum"));
		$row->addHeader(); // wijzig
		$row->addHeader(); // verwijder

		foreach ($pers as $per) {
			$perID = $per->geefID();
			$row = new HtmlTableRow();
			$row->addData(PeriodeView::waardeCollegejaar($per));
			$row->addData(PeriodeView::waardePeriodeNummer($per));
			$row->addData(PeriodeView::waardeDatumBegin($per));
			$row->addData(HtmlAnchor::button(BOEKWEBBASE."Periode/".$perID."/Wijzig", _("Wijzig")));
			$row->addData(HtmlAnchor::button(BOEKWEBBASE."Periode/".$perID."/Verwijder", _("Verwijder")));

			$tabel->addImmutable($row);
		}

		return $tabel;
	}
}
