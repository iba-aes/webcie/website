<?
/**
 * $Id$
 */
abstract class VoorraadView
	extends VoorraadView_Generated
{
	static public function tableHeader()
	{
		$row = new HtmlTableRow();
		$row->addHeader(_("ID"));
		$row->addHeader(_("Locatie"));
		$row->addHeader(_("In voorraad"));
		$row->addHeader(_("Waarde per stuk"));
		$row->addHeader(_("VerkoopPrijs"));
		$row->addHeader(_("BTW"));
		$row->addHeader(_("Verkoopbaar"));
		$row->addHeader(_("Omschrijving"));
		return $row;
	}

	static public function tableRow(Voorraad $voorraad)
	{
		$row = new HtmlTableRow();
		$row->addData(self::detailsLink($voorraad));
		$row->addData(self::waardeLocatie($voorraad));
		$row->addData(self::waardeAantal($voorraad));
		$row->addData(self::waardeWaardePerStuk($voorraad));
		$row->addData(self::waardePrijs($voorraad));
		$row->addData(self::waardeBtw($voorraad));
		$row->addData(self::waardeVerkoopbaar($voorraad));
		$row->addData(self::waardeOmschrijving($voorraad));
		return $row;
	}

	static public function wijzigForm(Voorraad $voorraad, $nieuw = false, $show_error = false)
	{
		$page = Page::getInstance();
		if($nieuw)
			$page->start(_("Voorraad toevoegen"));
		else
			$page->start(_("Voorraad wijzigen"));

		$page->add($form = HtmlForm::named('voorraadForm'));

		$form->add(self::wijzigTable($voorraad, $nieuw, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Voer in")));

		$page->end();
	}

	static public function wijzigTable(Voorraad $voorraad, $nieuw = false, $show_error)
	{
		$tbody = new HtmlDiv();

		$tbody->add(self::infoTR($voorraad, 'artikel'));
		if($nieuw) {
			$tbody->add(self::wijzigTR($voorraad, 'locatie', $show_error));
			$tbody->add(self::wijzigTR($voorraad, 'waardePerStuk', $show_error));
			$tbody->add(self::wijzigTR($voorraad, 'btw', $show_error));
		} else {
			$tbody->add(self::infoTR($voorraad, 'locatie'));
			$tbody->add(self::infoTR($voorraad, 'waardePerStuk'));
			$tbody->add(self::infoTR($voorraad, 'btw'));
		}
		$tbody->add(self::wijzigTR($voorraad, 'omschrijving', $show_error));
		$tbody->add(self::wijzigTR($voorraad, 'verkoopbaar', $show_error));

		return $tbody;
	}

	static public function processNieuwForm(Voorraad $voorraad)
	{
		$vPar = tryPar('Voorraad', null);
		$voorraad = new Voorraad($voorraad->getArtikel(), tryPar('locatie', NULL), $vPar['WaardePerStuk'], tryPar('btw', NULL));

		if(!$data = tryPar(self::formobj($voorraad), NULL))
			return false;

		self::processForm($voorraad, 'set');

		return $voorraad;
	}

	static public function processWijzigForm(Voorraad $voorraad)
	{
		if(!$data = tryPar(self::formobj($voorraad), NULL))
			return false;

		self::processForm($voorraad);
	}

	static public function details(Voorraad $voorraad)
	{
		$page = self::pageLinks($voorraad);
		$page->start(sprintf(_("Voorraaddetails %s"), ArtikelView::waardeNaam($voorraad->getArtikel())));

		$page->add($div = new HtmlDiv());
		$div->add(self::viewInfo($voorraad, true));

		$div->add(HtmlAnchor::button($voorraad->url() . '/GeschiedenisVerkoopPrijs', _("Bekijk hier de verkoopprijsgeschiedenis")));
		$div->add(HtmlAnchor::button($voorraad->url() . '/WijzigVerkoopPrijs', _("Verander de verkoopprijs")));

		$page->add(new HtmlHeader(3, sprintf(_("Mutaties (%s, %s)"), new HtmlAnchor($voorraad->url() . '/NieuweLevering', _("Nieuwe levering")), new HtmlAnchor($voorraad->url() . '/Verplaatsen', _("Voorraad verplaatsen")))));
		$page->add($textarea = new HtmlTextarea("", 50, 10));
		foreach($voorraad->getVoorraadMutatieRelaties() as $mutatie) {
			$textarea->add(VoorraadMutatieView::uitgebreideRegel($mutatie));
		}

		$page->end();
	}

	static public function defaultWaardeVoorraad(Voorraad $voorraad)
	{
		return sprintf(_("%s bij %s"), new HtmlAnchor($voorraad->url(), _("Voorraad ") . self::waardeVoorraadID($voorraad)), new HtmlAnchor($voorraad->getArtikel()->url(), ArtikelView::waardeNaam($voorraad->getArtikel())));
	}

	static public function detailsLink(Voorraad $voorraad)
	{
		return new HtmlAnchor($voorraad->url(), self::waardeVoorraadID($voorraad));
	}

	static public function waardeAantal (Voorraad $voorraad)
	{
		return static::defaultWaardeInt($voorraad, 'Aantal');
	}

	static public function waardeAantalArtikel (Artikel $artikel, $locatie)
	{
		return Voorraad::waardeAantalArtikel($artikel, $locatie);
	}

	static public function waardeEersteMutatie (Voorraad $voorraad)
	{
		return static::defaultWaardeDate($voorraad, 'EersteMutatie');
	}

	static public function waardeLaatsteMutatie (Voorraad $voorraad)
	{
		return static::defaultWaardeDate($voorraad, 'LaatsteMutatie');
	}

	static public function waardePrijs (Voorraad $voorraad, $aantal = 1)
	{
		return Money::addPrice($voorraad->getVerkoopPrijs()->getPrijs() * $aantal);
	}

	static public function waardeVerkoopPrijs (Voorraad $voorraad)
	{
		return self::waardePrijs($voorraad);
	}

	static public function waardeBtw(Voorraad $voorraad)
	{
		$btw = $voorraad->getBtw();
		if(empty($btw) || $btw === "NULL") {
			return new HtmlEmphasis(_("Geen btw"));
		} else {
			return parent::waardeBtw($voorraad) . " %";
		}
	}

	static public function labelAantal()
	{
		return _("Aantal");
	}

	static public function labelVerkoopPrijs()
	{
		return _("Verkoopprijs");
	}

	static public function opmerkingVerkoopPrijs()
	{
		return _("in euro's");
	}

	static public function opmerkingWaardePerStuk()
	{
		return _("in euro's");
	}

	static public function formLocatie(Voorraad $obj, $include_id = false)
	{
		$waarde = $obj->getLocatie();
		return HtmlSelectbox::fromArray('locatie', VoorraadView::labelenumLocatieArray(), tryPar('locatie', $waarde), 1);
	}

	static public function formWaardePerStuk(Voorraad $obj, $include_id = false)
	{
		return self::defaultFormMoney($obj, 'WaardePerStuk', null, null);
	}

	static public function formBtw(Voorraad $obj, $include_id = false)
	{
		$waarde = $obj->getBtw();;
		return HtmlSelectbox::fromArray('btw', VoorraadView::labelenumBtwArray(), tryPar('btw', $waarde), 1);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
