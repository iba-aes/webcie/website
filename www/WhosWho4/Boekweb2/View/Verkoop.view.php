<?
/**
 * $Id$
 */
abstract class VerkoopView
	extends VerkoopView_Generated
{
	static public function aantalVerkocht (Verkoop $verkoop)
	{
		return -1 * $verkoop->getAantal();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
