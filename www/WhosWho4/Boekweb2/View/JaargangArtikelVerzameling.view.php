<?php
declare(strict_types=1);

abstract class JaargangArtikelVerzamelingView
	extends JaargangArtikelVerzamelingView_Generated
{
	/**
	 * Geef een HTML-tabel van deze artikels.
	 *
	 * De velden in deze tabel zijn de viewInfo-velden van een JaargangArtikel.
	 * @see JaargangArtikel::velden
	 *
	 * @param JaargangArtikelVerzameling $jgArtikels
	 * De artikels om te tonen.
	 *
	 * @return HtmlElement
	 */
	public static function infoTable(JaargangArtikelVerzameling $jgArtikels)
	{
		return static::makeTable($jgArtikels, JaargangArtikel::velden('viewInfo'));
	}
}
