<?
abstract class AesArtikelView
	extends AesArtikelView_Generated
{
	static public function tableRow($aesartikel)
	{
		$tr = parent::tableRow($aesartikel);
		$tr->addData(self::waardeCommissie($aesartikel));
		return $tr;
	}

	static public function tableHeaderRow()
	{
		$tr = parent::tableHeaderRow();
		$tr->addHeader(_('Commissie'));
		return $tr;
	}

	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$tbody = parent::wijzigTable($obj, $nieuw, $show_error);
		if($nieuw)
			$tbody->add(self::wijzigTR($obj, 'commissie', $show_error));
		else
			$tbody->add(self::infoTR($obj, 'commissie'));
		return $tbody;
	}

	static public function processNieuwForm($obj)
	{
		$cie = Commissie::geef(tryPar('AesArtikel[commissie]'));
		$obj = new AesArtikel($cie);
		parent::processNieuwForm($obj);
		return $obj;
	}

	static public function formCommissie(AesArtikel $obj, $include_id = false)
	{
		$cies = CommissieVerzameling::huidige("ALLE");
		$box = HtmlSelectbox::cies($cies, 'AesArtikel[commissie]');
		$box->select(tryPar('AesArtikel[commissie]'));
		return $box;
	}
}
