<?
abstract class iDealKaartjeView
	extends iDealKaartjeView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/*
	 *  Geeft een tableRow met waarden van het kaartje terug. Dit is een
	 * extensie van de standaard ArtikelView::tableRow
	 *
	 * @param kaartje Het kaartje waarvan je de tablerow wilt
	 * @return De tableRow
	 */
	static public function tableRow($kaartje)
	{
		$tr = parent::tableRow($kaartje);
		$tr->addData(self::waardeActiviteit($kaartje));
		$tr->addData(self::waardeAutoInschrijven($kaartje));
		$tr->addData(self::waardeReturnUrl($kaartje));
		return $tr;
	}

	/*
	 *  Geeft een tableheader voor een tabel met kaartjes, extensie van
	 * ArtikelView::tableHeaderRow
	 *
	 * @return De tableHeader
	 */
	static public function tableHeaderRow()
	{
		$tr = parent::tableHeaderRow();
		$tr->addHeader(_('Activiteit'));
		$tr->addHeader(_('Automatisch inschrijven'));
		$tr->addHeader(_('Verkoopbaar via site'));
		$tr->addHeader(_('Returnurl'));
		return $tr;
	}

	/*
	 *  Geeft een wijzigtabel terug voor een kaartje, extensie van ArtikelView::wijzigTable
	 *
	 * @param obj Het kaartje
	 * @param nieuw Bool of het een nieuw kaartje is of niet
	 * @param show_error Of er errors zijn die moeten worden laten zien
	 * @return De table
	 */
	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$div = parent::wijzigTable($obj, $nieuw, $show_error);
		if($nieuw)
			$div->add(self::wijzigTR($obj, 'activiteit', $show_error));
		else
			$div->add(self::infoTR($obj, 'activiteit'));
		$div->add(self::wijzigTR($obj, 'autoInschrijven', $show_error));
		$div->add(self::wijzigTR($obj, 'returnUrl', $show_error));
		$div->add(self::wijzigTR($obj, 'externVerkrijgbaar', $show_error));
		$div->add(self::wijzigTR($obj, 'digitaalVerkrijgbaar', $show_error));
		return $div;
	}

	/*
	 *  Geeft het 'winkel'-scherm waarop mensen kunnen aangeven hoeveel
	 * kaartjes ze willen etc
	 *
	 * @param iDealkaartjeVerzameling De verzameling met iDealKaartjes waaruit de
	 * gebruiker kan kiezen
	 * @param activiteit De activiteit waarvoor er kaartjes gekocht worden
	 * @param return Een array met eventuele foutmeldingen
	 */
	static public function kaartjeKopen(iDealKaartjeVerzameling $iDealkaartjeVerzameling
		, Activiteit $activiteit
		, iDealAntwoordVerzameling $antwoorden)
	{
		global $session;

		$show_error = false;

		$cleanhtml = tryPar('cleanhtml', false);
		if($cleanhtml) {
			$page = Page::getInstance('cleanhtml');
			$page->addHeadCSS(Page::minifiedFile('a-eskwadraat.css', 'css'));
			$page->start('iDeal');
		} else {
			$page = Page::getInstance()->start(
				sprintf(_("Kaartje kopen voor %s"), $activiteit->getTitel()));
		}

		if($cleanhtml) {
			$page->add($div = new HtmlDiv());
			$div->setCssStyle("border: solid 4px #CC0066; padding: 10px");
			$div->add($img = new HtmlImage('/Layout/Images/ideal.gif', "IDEAL"));
			$img->setCssStyle("float: right");
			$div->add(new HtmlHeader(2, sprintf(_("Betalen voor %s"), $activiteit->getTitel())));
		} else {
			$page->add($div = new HtmlDiv());
		}

		if(tryPar('cleanhtml', false))
		{
			foreach($session->getFlashBag()->all() as $soort => $meldingen) {
				switch($soort)
				{
				case 'succes':
					$alertsoort = 'success';
					break;
				case 'fout':
					$alertsoort = 'danger';
					break;
				case 'waarschuwing':
				default:
				$alertsoort = 'warning';
				break;
				}

				$div->add(new HtmlDiv(new HtmlDiv(new HtmlDiv(implode('<br>', $meldingen), 'alert alert-'.$alertsoort), 'col-sm-10'), 'row'));
			}

			$session->getFlashBag()->clear();
		}

		$div->add($form = HtmlForm::named("IDEAL Transactie"));
		$form->add(HtmlInput::makeHidden("activiteit", $activiteit->geefID()));

		if($cleanhtml)
		{
			$form->add(new HtmlDiv($div = new HtmlDiv(null, 'col-sm-10'), 'row'));
			$form->add(HtmlInput::makeHidden('cleanhtml', 1));
		}
		else
			$form->add($div = new HtmlDiv());

		$selections = array();
		foreach($iDealkaartjeVerzameling as $iDealkaartje) {
			foreach($iDealkaartje->getVoorraden() as $v) {
				if(!$v->getVerkoopbaar())
					continue;
				if(!$v->getVerkoopPrijs()->getPrijs())
					continue;
				if($v->getAantal() == 0) {
					$selections[$v->geefID()] = iDealKaartjeView::waardeNaam($iDealkaartje)
						. " " . VerkoopPrijsView::waardePrijs($v->getVerkoopPrijs())
						. " (UITVERKOCHT)";
				} else {
					if($cleanhtml)
						$selections[$v->geefID()] = iDealKaartjeView::waardeNaam($iDealkaartje)
						. " " . VerkoopPrijsView::waardePrijs($v->getVerkoopPrijs());
					else
						$selections[$v->geefID()] = iDealKaartjeView::waardeNaam($iDealkaartje)
						. " " . VerkoopPrijsView::waardePrijs($v->getVerkoopPrijs())
						. " (" . $v->getAantal() . " beschikbaar)";
				}
			}
		}

		$div->add(self::makeFormEnumRow('kaartje', $selections, tryPar('kaartje')
			, _('Selecteer wat u wilt kopen'),
				sprintf(_('(+ €%s transactiekosten per transactie)'),
					iDeal::transactiekosten()->alsFloat())));

		$div->add(self::makeFormNumberRow('aantal', htmlspecialchars(tryPar('aantalKopen'))
			, null, null, _('Hoeveel wilt u er kopen?')));

		if(tryPar('email'))
			$mail = tryPar('email');
		elseif($persoon = Persoon::getIngelogd())
			$mail = $persoon->getEmail();
		else
			$mail = '';

		$div->add(self::makeFormStringRow('email', htmlspecialchars($mail)
			, _('Wat is uw emailadres?')));

		if(tryPar('emailVerificatie'))
			$mailVer = tryPar('emailVerificatie');
		elseif($persoon = Persoon::getIngelogd())
			$mailVer = $persoon->getEmail();
		else
			$mailVer = '';

		$div->add(self::makeFormStringRow('emailVerificatie', htmlspecialchars($mailVer)
			, _('Vult het emailadres nog een keer in')));

		if($antwoorden->aantal() > 0)
		{
			if($antwoorden->aantal() == 1)
				$div->add(new HtmlHeader(4, _('Geef antwoord op de volgende vraag')));
			else
				$div->add(new HtmlHeader(4, _('Geef antwoord op de volgende vragen')));

			foreach($antwoorden as $antwoord)
			{
				$div->add(new HtmlDiv(array(
					new HtmlLabel('vraag', _('Vraag'), 'col-sm-2 control-label'),
					new HtmlDiv(ActiviteitVraagView::waardeVraag($antwoord->getVraag()), 'actvraag col-sm-10')
				), 'form-group'));
				$div->add(iDealAntwoordView::wijzigTR($antwoord, "antwoord", $show_error, true));
			}
		}

		$div->add(HtmlInput::makeFormSubmitButton(_("Door naar iDeal")));

		$page->end();
	}

	/*
	 *  Maakt een selectbox met activiteiten om uit te kiezen
	 *
	 * @return De selectbox
	 */
	static public function formActiviteit(iDealKaartje $obj, $include_id = false)
	{
		$acts = ActiviteitVerzameling::getActiviteiten();
		$actarr = array();
		foreach($acts as $key => $act)
		{
			$actarr[$key] = ActiviteitView::titel($act);
		}
		$box = HtmlSelectbox::fromArray('iDealKaartje[activiteit]', $actarr, null, 1);
		$box->select(tryPar('iDealKaartje[activiteit]'));
		return $box;
	}

	/*
	 *  Verwerkt een form van kaartje wijzigen
	 *
	 * @param obj Het kaartje wat verwerkt moet worden
	 *
	 * @return Het kaartje
	 */
	static public function processNieuwForm($obj)
	{
		$act = Activiteit::geef(tryPar('iDealKaartje[activiteit]'));

		$obj = new iDealKaartje($act);
		parent::processNieuwForm($obj);
		return $obj;
	}

	/*
	 *  Geeft de returnUrl in anchor-vorm
	 *
	 * @param kaartje Het idealKaartje waarvan je de returnurl wilt
	 * @return De anchor
	 */
	static public function waardeReturnUrl(iDealKaartje $kaartje)
	{
		return new HtmlAnchor($kaartje->getReturnUrl(), parent::waardeReturnUrl($kaartje));
	}

	/*
	 *  Maakt een pagin voor het toevoegen van iDealkaartjes bij een activiteit
	 *
	 * @param kaartje Een idealkaartje-object
	 * @param activiteit De activiteit voor het kaartje
	 * @param show_error Bool of foutmeldingen moeten worden laten zien
	 */
	static public function toevoegen(iDealKaartje $kaartje, Activiteit $activiteit, $show_error)
	{
		$page = Page::getInstance()->start(_('Kaartje toevoegen'));

		$page->add($form = HtmlForm::named('iDealKaartjeToevoegen'));

		$form->add($div = new HtmlDiv());

		$div->add($tDiv = self::wijzigTable($kaartje, true, $show_error));

		$tDiv->add(self::makeFormMoneyRow('verkoopprijs', tryPar('verkoopprijs', null), null, null, _('Verkoopprijs')));
		$tDiv->add(self::makeFormNumberRow('aantal', tryPar('aantal', null), null, null, _('Aantal')));

		$form->add(HtmlInput::makeFormSubmitButton(_('Voeg toe')));

		$page->end();
	}

	static public function opmerkingAutoInschrijven()
	{
		return _("Dit geeft aan of de leden automatisch ingeschreven moeten worden "
			. "bij het kopen van een kaartje, indien dit mogelijk is. Als iemand bv niet "
			. "ingelogd is gebeurt er niets.");
	}

	static public function opmerkingReturnURL()
	{
		return _("Dit is de url waar de personen naar worden doorgestuurd als ze "
			. "succesvol een kaartje gekocht hebben");
	}

	static public function opmerkingExternVerkrijgbaar()
	{
		return _("Dit geeft aan of ook niet-leden een kaartje mogen kopen.");
	}

	static public function opmerkingDigitaalVerkrijgbaar()
	{
		return _("Dit geeft aan hoe het kaartje verkregen wordt. NAMENLIJST "
			. "betekent dat leden automatisch worden ingeschreven zodra het "
			. "kaartje gekocht wordt, PERSOONLIJK dat kaartjes per persoon "
			. "(ook voor externen) zijn en CODE betekent dat ze een qr-code "
			. "krijgen toegestuurd zodra ze het kaartje gekocht hebben, "
			. "die ze vrij mogen doorsturen.");
	}
}
