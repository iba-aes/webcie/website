<?
/**
 * $Id$
 */
abstract class DictaatView
	extends DictaatView_Generated
{
	static public function tableRow($dictaat)
	{
		$tr = parent::tableRow($dictaat);
		$tr->addData(self::waardeAuteur($dictaat));
		$tr->addData(self::waardeUitgaveJaar($dictaat));
		$tr->addData(self::waardeBeginGebruik($dictaat));
		$tr->addData(self::waardeEindeGebruik($dictaat));
		$tr->addData(self::waardeCopyrightOpmerking($dictaat));
		$tr->addData(self::downloadLink($dictaat));
		return $tr;
	}

	static public function tableHeaderRow()
	{
		$tr = parent::tableHeaderRow();
		$tr->addHeader(_('Auteur'));
		$tr->addHeader(_('Uitgavejaar'));
		$tr->addHeader(_('Begin gebruik'));
		$tr->addHeader(_('Eind gebruik'));
		$tr->addHeader(_('Copyrightopmerking'));
		$tr->addHeader(_('Downloaden'));
		return $tr;
	}

	static public function viewInfo($obj, $header = false)
	{
		$panel = parent::viewInfo($obj, $header);

		$table = $panel->getChild(1)->getChild(0);

		foreach($obj->getJaargangArtikelRelaties() as $vakartikel) {
			$table->add(self::infoTRData(_("Gebruikt voor vak"), VakView::makeLink($vakartikel->getJaargang()->getVak())));

			if(hasAuth('bestuur')) {
				$table->add(self::infoTRData(_("Schatting nodig"), JaargangArtikelView::waardeSchattingNodig($vakartikel)))
					  ->add(_("Gratis reserveringen"), JaargangArtikelView::waardeOpmerking($vakartikel));
			}
		}
	}

	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$tbody = parent::wijzigTable($obj, $nieuw, $show_error);

		$tbody->add(self::wijzigTR($obj, 'auteur', $show_error))
			  ->add(self::wijzigTR($obj, 'uitgaveJaar', $show_error))
			  ->add(self::wijzigTR($obj, 'digitaalVerspreiden', $show_error))
			  ->add(self::wijzigTR($obj, 'beginGebruik', $show_error))
			  ->add(self::wijzigTR($obj, 'eindeGebruik', $show_error));
		$tbody->add(self::wijzigTR($obj, 'copyrightOpmerking', $show_error));

		return $tbody;
	}

	static public function processNieuwForm($obj)
	{
		if($pers = Persoon::geef(tryPar('auteurContactID'))) {
			$obj->setAuteur($pers);
		}
		parent::processNieuwForm($obj);
	}

	static public function processWijzigForm($obj)
	{
		if($pers = Persoon::geef(tryPar('auteurContactID'))) {
			$obj->setAuteur($pers);
		} else {
			$id = tryPar('auteurContactID', null);
			if(is_null($id))
				$obj->setAuteur(0);
			else
				$obj->setAuteur($id);
		}
		parent::processWijzigForm($obj);
	}

	static public function uploaden()
	{
		global $DEPARTEMENTNAMEN;

		$page = Page::getInstance()->start(_("Dictaat uploaden"));

		$page->add(new HtmlDiv(sprintf(_('Voor meer informatie over de dictatenregeling, '
			. 'zie %s[VOC:een url]. Houdt er rekening mee dat wij het contact met '
			. 'stichting PRO regelen indien nodig.')
			, new HtmlAnchor('http://www.uu.nl/university/library/NL/informatie/auteursrechten/Pages/default.aspx'
				, 'http://www.uu.nl/university/library/NL/informatie/auteursrechten/Pages/default.aspx'))
			, 'bs-callout bs-callout-info'));
		$page->add(new HtmlDiv(sprintf(_('Voor vragen, stuur gerust een mailtje '
			. 'naar %s[VOC: mailadres]!')
			, new HtmlAnchor('mailto:boeken@a-eskwadraat.nl', "boeken@a-eskwadraat.nl"))
			, 'bs-callout bs-callout-info'));

		$page->add($form = HtmlForm::named('DictaatUploaden'));

		$form->add($div = new HtmlDiv());
		$form->setAttribute('enctype', 'multipart/form-data');

		$div->add(self::makeFormStringRow('auteurVoorletters', tryPar('auteurVoorletters')
			, _('Wat zijn uw voorletters?')));
		$div->add(self::makeFormStringRow('auteurNaam', tryPar('auteurNaam')
			, _('Wat is uw achternaam?')));
		$div->add(self::makeFormStringRow('auteurEmail', tryPar('auteurEmail')
			, _('Wat is uw emailadres?')));
		$div->add(self::makeFormStringRow('naam', tryPar('naam')
			, _('Wat is de naam van het dictaat?')));
		$div->add(self::makeFormStringRow('vakcode', tryPar('vakcode')
			, _('Voor welk van wordt het dictaat gebruikt? (vakcode)')));
		$div->add(self::makeFormEnumRow('departement', $DEPARTEMENTNAMEN
			, tryPar('departement'), _('Vanuit welk departement wordt dit vak uitgedragen?')));
		$div->add(self::makeFormNumberRow('schatting', tryPar('schatting')
			, null, null, _('Hoeveel dictaten schat u dat er nodig zijn? (optioneel)')));
		$div->add(self::makeFormNumberRow('jaar', tryPar('jaar')
			, null, null, _('Uit welk jaar stamt de originele druk?')));
		$div->add(self::makeFormBoolRow('digitaalVerspreiden', tryPar('digitaalVerspreiden')
			, _('Mogen we dit dictaat onder onze leden kostenloos digitaal verspreiden?')));
		$div->add(self::makeFormTextRow('copyrightOpmerking', tryPar('copyrightOpmerking')
			, _('Worden er stukken geciteerd in uw dictaat? Zo ja, op welke '
			. 'pagina\'s en vanuit welke bron? ')
			, _('Houd er rekening mee dat u zelf '
			. 'zorg draagt dat wij hierover goed geinformeerd zijn zodat wij het '
			. 'kunnen aangeven bij stichting PRO.')));
		$div->add(self::makeFormTextRow('weggeefOpmerking', tryPar('weggeefOpmerking')
			, _('Wilt u gratis exemplaren reserveren voor uw student-assistenten? '
			. 'Zo ja, hoeveel?')
			, sprintf('%s%s%s', _('Wij houden deze apart voor u en kunnen dit aantal persoonlijk '
			. 'afleveren.'), new HtmlBreak(), _('Let op: voor vakken van het '
			. 'departement Natuurkunde geldt dat er toestemming vanuit het '
			. 'departement nodig is om dictaten weg te mogen geven. Wij zullen '
			. 'contact met u opnemen als het aantal dat u wilt reserveren niet '
			. 'door het departement is goedgekeurd.'))));
		$div->add(self::makeFormFileRow('dictaat', null, false
			, _('Selecteer het dictaat dat u wilt uploaden (pdf)')));
		$div->add(self::makeFormTextRow('opmerking', tryPar('opmerking')
			, _('Heeft u verder nog vragen of opmerkingen?')));

		$form->add(HtmlInput::makeFormSubmitButton(_('Stuur deze gegevens op!')));

		$page->end();
	}

	static public function details (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("Details %s"), DictaatView::waardeNaam($dictaat)));

		$page->add(DictaatView::downloadLink($dictaat));

		if(hasAuth('boekcom')) {
			// de volgende links zijn nodig zolang dictaatbeheer en artikels niet netjes zijn samengevoegd
			$page->add(new HtmlParagraph(sprintf(_("Bekijk dit dictaat als artikel: %s"),
				new HtmlAnchor($dictaat->url(), DictaatView::waardeNaam($dictaat))
			)));
			$page->add(new HtmlParagraph(sprintf(_("Wijzig dit artikel: %s"),
				new HtmlAnchor($dictaat->wijzigURL(), _("Wijzig!"))
			)));
			$page->add(new HtmlParagraph(sprintf(_("Verwijder dit artikel: %s"),
				new HtmlAnchor($dictaat->verwijderURL(), _("Weg ermee!"))
			)));

			$page->add(new HtmlHeader(3, _("Boekcom-acties:")));
			$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
			$div->add($ul = new HtmlList());
			$ul->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Bestand", _("Verander bijbehorend bestand."))));
			$ul->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Orderen", _("Plaats order bij drukkerij."))));
			$ul->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Levering", _("Verwerk losse levering."))));
			$page->add(new HtmlHR());
		}

		$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
		$div->add($ul = new HtmlList());
		$ul->add(new HtmlListItem(sprintf(_("Oorspronkelijke uploader: %s"), PersoonView::makeLinkWithEmail($dictaat->getAuteur()))));
		$ul->add(new HtmlListItem(sprintf(_("Begin gebruik dictaat: %s"), DictaatView::waardeBeginGebruik($dictaat))));
		$ul->add(new HtmlListItem(sprintf(_("Einde gebruik dictaat: %s"), DictaatView::waardeEindeGebruik($dictaat))));

		if(hasAuth('bestuur')) {
			$page->add($row = new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
			$div->add(new HtmlHeader(3, _("Voorraad")));
			$div->add($ul = new HtmlList());
			$ul->add(new HtmlListItem(
				sprintf(_("In voorraad (verkoopbaar): %u (%s)")
					, $dictaat->getVerkoopbareVoorraadRelaties()->getAantal()
					, new HtmlAnchor($dictaat->url() . '/Dictaat/Voorraad', _("details")))));
			$ul->add(new HtmlListItem(
				sprintf(_("In voorraad (onverkoopbaar): %u (%s)")
					, $dictaat->getOnverkoopbareVoorraadRelaties()->getAantal()
					, new HtmlAnchor($dictaat->url() . 'Dictaat/Voorraad', _("details")))));
			$ul->add(new HtmlListItem(
				sprintf(_("Open orders: %u (%s)")
					, $dictaat->getOpenOrderRelaties()->getAantal()
					, new HtmlAnchor($dictaat->url() . 'Dictaat/Orders', _("details")))));
			$ul->add(new HtmlListItem(
				sprintf(_("Verkocht: %u (%s)")
					, $dictaat->getAantalVerkopen()
					, new HtmlAnchor($dictaat->url() . 'Dictaat/Verkopen', _("details")))));

			$row->add($div = new HtmlDiv(null, 'col-md-6'));
			$div->add(new HtmlHeader(3, _("Copyrightopmerking")));
			$div->add(DictaatView::waardeCopyrightOpmerking($dictaat));
		}

		$page->add(new HtmlHeader(3, _("Vakken waarbij dit dictaat wordt gebruikt:")));
		$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
		$div->add($ul = new HtmlList());
		foreach($dictaat->getJaargangArtikelRelaties() as $vakartikel) {
			$ul->add($li = new HtmlListItem(VakView::makeLink($vakartikel->getJaargang()->getVak())));
			if(hasAuth('bestuur')) {
				$li->add($p = new HtmlParagraph());
				$p->add(new HtmlSpan(sprintf(new HtmlStrong(_("Schatting nodig: %s")), JaargangArtikelView::waardeSchattingNodig($vakartikel))));
				$p->add(new HtmlParagraph(new HtmlSpan(sprintf(new HtmlStrong(_("Gratis reserveringen: %s")), JaargangArtikelView::waardeOpmerking($vakartikel)))));
			}
		}

		$page->end();
	}

	static public function bestandFout (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("%s downloaden"), DictaatView::waardeNaam($dictaat)));

		$page->add(new HtmlSpan(_("Het gevraagde dictaat is niet gevonden."), 'negatief'));

		$page->add(HtmlAnchor::button($dictaat->url(), _('Terug naar de dictaat-pagina')));

		if (DEBUG) {
			$page->add(new HtmlParagraph(_("Houd er rekening mee dat je op een debug omgeving zit, hier werkt bestanden uploaden niet.")));
		}
		$page->end();
	}

	static public function orders (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("Orders van %s"), DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));

		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar de dictaat-pagina')));

		if(hasAuth('boekcom'))
			$div->add(HtmlAnchor::button($dictaat->url() . '/Dictaat/Orderen', _('Nieuwe order plaatsen')));

		if($dictaat->getOpenOrderRelaties()->aantal() > 0) {
			$page->add(new HtmlHeader(3, _("Open orders")));
			$page->add($table = new HtmlTable(null, 'dictaten'));
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Geplaatst")))
				->add(new HtmlTableHeaderCell(_("Leverancier")))
				->add(new HtmlTableHeaderCell(_("Aantal")))
				->add(new HtmlTableHeaderCell(_("Waarde")));
			if(hasAuth('boekcom')) {
				$row->add(new HtmlTableHeaderCell());
			}

			foreach($dictaat->getOpenOrderRelaties() as $order) {
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(OrderView::waardeDatumGeplaatst($order)))
					->add(new HtmlTableDataCell(OrderView::waardeLeverancier($order)))
					->add(new HtmlTableDataCell(OrderView::waardeAantal($order)))
					->add(new HtmlTableDataCell(OrderView::waardeWaardePerStuk($order)));
				if(hasAuth('boekcom')) {
					$row->add(new HtmlTableDataCell(sprintf(("(%s, %s)")
						, new HtmlAnchor($dictaat->url() . '/Dictaat/Orders/' . $order->geefID() . '/Levering', _("levering"))
						, new HtmlAnchor($dictaat->url() . '/Dictaat/Orders/' . $order->geefID() . '/Annuleer', _("annuleren")))));
				}
			}
		}

		if ($dictaat->getGeslotenOrderRelaties()->aantal() > 0) {
			$page->add(new HtmlHeader(3, _("Gesloten orders")));
			$page->add($table = new HtmlTable());
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Geplaatst")))
				->add(new HtmlTableHeaderCell(_("Leverancier")))
				->add(new HtmlTableHeaderCell(_("Aantal")))
				->add(new HtmlTableHeaderCell(_("Waarde")));

			foreach($dictaat->getGeslotenOrderRelaties() as $order) {
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(OrderView::waardeDatumGeplaatst($order)))
					->add(new HtmlTableDataCell(OrderView::waardeLeverancier($order)))
					->add(new HtmlTableDataCell(OrderView::waardeAantal($order)))
					->add(new HtmlTableDataCell(OrderView::waardeWaardePerStuk($order)));
			}
		}

		if ($dictaat->getGeannuleerdOrderRelaties()->aantal() > 0) {
			$page->add(new HtmlHeader(3, _("Geannuleerde orders")));
			$page->add($table = new HtmlTable());
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Geplaatst")))
				->add(new HtmlTableHeaderCell(_("Leverancier")))
				->add(new HtmlTableHeaderCell(_("Aantal")))
				->add(new HtmlTableHeaderCell(_("Waarde")));
			if(hasAuth('boekcom')) {
				$row->add(new HtmlTableHeaderCell());
			}

			foreach($dictaat->getGeannuleerdOrderRelaties() as $order) {
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(OrderView::waardeDatumGeplaatst($order)))
					->add(new HtmlTableDataCell(OrderView::waardeLeverancier($order)))
					->add(new HtmlTableDataCell(OrderView::waardeAantal($order)))
					->add(new HtmlTableDataCell(OrderView::waardeWaardePerStuk($order)));
				if(hasAuth('boekcom')) {
					$row->add(new HtmlTableDataCell(sprintf(("(%s)")
						, new HtmlAnchor($dictaat->url() . '/Dictaat/Orders/' . $order->geefID() . '/Annuleer', _("ongedaan maken")))));
				}
			}
		}

		$page->end();
	}

	/**
	 *  Maak een div met inputs voor dictaatleveringen.
	 * Het idee is dat dit gemeenschappelijk kan werken voor orders en losse leveringen.
	 *
	 * @param order Het order dat we willen aanmaken.
	 * @param show_error Of we al een error willen tonen.
	 * @returns Een HtmlDiv met HtmlInputs voor de benodigde waarden.
	 */
	static private function dictaatOrderFormOnderdeel($order, $show_error)
	{
		$div = new HtmlDiv();
		$selects = array();
		foreach(LeverancierVerzameling::dictatenLeveranciers() as $leverancier) {
			$selects[$leverancier->geefID()] = LeverancierView::waardeNaam($leverancier);
		}
		$div->addImmutable(self::makeFormEnumRow('leverancier', $selects,
			tryPar('leverancier'), _('Bij welke leverancier?')));

		$div->addImmutable(self::makeFormNumberRow('aantal', tryPar('aantal'), 0, null, _('Om hoeveel stuks gaat het?')));
		$div->addImmutable(DictaatOrderView::losseLeveringOrder($order, $show_error));

		return $div;
	}

	/**
	 *  Verwerk de gegevens van het dictaatOrderFormOnderdeel.
	 * De returnwaarde is nog niet opgeslagen of gecontroleerd!
	 *
	 * @param dictaat Het dictaat dat we willen orderen.
	 * @returns Een Order-object met de gegevens van het formulier ingevuld.
	 */
	static public function dictaatOrderFormProcess($dictaat)
	{
		$leverancier = Leverancier::geef(tryPar('leverancier'));
		$order = new DictaatOrder($dictaat, $leverancier);
		$order = DictaatOrderView::processLosseLeveringOrder($order);

		$order->setAantal(tryPar('aantal'));

		return $order;
	}

	static public function plaatsOrder (Dictaat $dictaat, Order $order, $show_error)
	{
		$page = Page::getInstance()->start(sprintf(_('Plaats order voor %s')
			, DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));
		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar dictaat-pagina')))
			->add(HtmlAnchor::button($dictaat->url() . '/Dictaat/Orders/', _('Alle orders bekijken')));

		$page->add(new HtmlHeader(3, _("Plaats order")));

		$page->add($form = HtmlForm::named('PlaatsOrder'));
		// Bereken marges automagisch.
		$page->addFooterJs(Page::minifiedFile('Boekweb.js'));
		$order = new DictaatOrder();
		$form->add($div = self::dictaatOrderFormOnderdeel($order, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Plaats order!")));

		$page->end();
	}

	/**
	 *  Geef een pagina met formulier om een levering in te vullen.
	 * Als er geen order is, is dit een losse levering, anders een echte levering.
	 */
	static public function nieuweLevering (Dictaat $dictaat, Order $order = null)
	{
		$page = Page::getInstance();

		// Als er nog geen order klaarstaat, dan maken we een nieuwe.
		$losseLevering = is_null($order);
		if (is_null($order)) {
			$order = new DictaatOrder($dictaat);
		}

		if($losseLevering)
			$page->start(sprintf(_("Losse levering voor %s"), DictaatView::waardeNaam($dictaat)));
		else
			$page->start(sprintf(_("Levering order voor %s"), DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));
		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar dictaat-pagina')))
			->add(HtmlAnchor::button($dictaat->url() . '/Dictaat/Orders/', _('Alle orders bekijken')));

		if($losseLevering) {
			$page->add(new HtmlHeader(3, _('Verwerk losse levering')));
		} else {
			$page->add(new HtmlHeader(3, _('Verwerk levering')));

			$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
			$div->add($ul = new HtmlList());
			$ul->addChild(sprintf(_('Datum geplaatst: %s'), OrderView::waardeDatumGeplaatst($order)));
			$ul->addChild(sprintf(_('Leverancier: %s'), OrderView::waardeLeverancier($order)));
			$ul->addChild(sprintf(_('Aantal: %s'), OrderView::waardeAantal($order)));
		}

		$page->add($form = HtmlForm::named("nieuweLevering"));

		if($losseLevering)
		{
			// Bereken marges automagisch.
			$page->addFooterJs(Page::minifiedFile('Boekweb.js'));
			$form->add($div = self::dictaatOrderFormOnderdeel($order, false));
		} else {
			$form->add($div = new HtmlDiv());
		}

		$div->add(self::makeFormDateRow('binnen_datum', tryPar('binnen_datum')
			, _('Wanneer kwam de levering binnen?')));

		// Als er geen order is, moeten we dit al invullen bij de order.
		if (!$losseLevering) {
			$div->add(self::makeFormNumberRow('aantal', tryPar('aantal', $order->getAantal())
				, null, null, _('Hoeveel stuks zijn er geleverd?')));
		}

		$div->add(self::makeFormEnumRow('locatie', VoorraadView::labelenumLocatieArray()
			, tryPar('locatie'), _('Waar ligt de levering nu?')));

		$form->add(HtmlInput::makeFormSubmitButton(_('Verwerk levering')));

		$page->end();
	}

	static public function voorraden (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("Voorraden van %s"), DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));
		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar dictaat-pagina')))
			->add(HtmlAnchor::button($dictaat->url() . '/Voorraad/Mutaties'
			, _('Ga naar mutaties-overzicht-pagina')));

		if($dictaat->getVerkoopbareVoorraadRelaties()->aantal() > 0) {
			$page->add(new HtmlHeader(3, _("Verkoopbare voorraad")));

			$naam = null;
			$table = new HtmlTable();
			foreach($dictaat->getVerkoopbareVoorraadRelaties() as $voorraad) {
				if($naam != $voorraad->getLocatie()) {
					$naam = $voorraad->getLocatie();
					$page->add(new HtmlHeader(4, VoorraadView::waardeLocatie($voorraad)));
					$table->add($row = new HtmlTableRow());
					$row->add(new HtmlTableHeaderCell(_("Aantal")))
						->add(new HtmlTableHeaderCell(_("Eerste mutatie")))
						->add(new HtmlTableHeaderCell(_("Laatste mutatie")))
						->add(new HtmlTableHeaderCell(_("Waarde per stuk")))
						->add(new HtmlTableHeaderCell(_("Verkoopbaar")))
						->add(new HtmlTableHeaderCell(_("Verkoopprijs")))
						->add(new HtmlTableHeaderCell(_("Omschrijving")));
					if(hasAuth('boekcom'))
						$row->add(new HtmlTableHeaderCell());
				}
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(VoorraadView::waardeAantal($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeEersteMutatie($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeLaatsteMutatie($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeWaardePerStuk($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeVerkoopbaar($voorraad)))
					->add(new HtmlTableDataCell(VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs())))
					->add(new HtmlTableDataCell(VoorraadView::waardeOmschrijving($voorraad)));
				if(hasAuth('boekcom')) {
					$row->add(new HtmlTableDataCell(sprintf(_("(%s, %s)")
						, new HtmlAnchor($dictaat->url() . '/Voorraad/' . $voorraad->geefID(), _("geschiedenis"))
						, new HtmlAnchor($dictaat->url() . '/Voorraad/' . $voorraad->geefID() . '/Verplaatsen', _("verplaasten")))));
				}
			}
			$page->add($table);
		}

		if ($dictaat->getOnverkoopbareVoorraadRelaties()->aantal() > 0) {
			$page->add(new HtmlHeader(3, _("Onverkoopbare voorraad")));
			$naam = null;
			$table = new HtmlTable();
			foreach($dictaat->getOnverkoopbareVoorraadRelaties() as $voorraad) {
				if($naam != $voorraad->getLocatie()) {
					$naam = $voorraad->getLocatie();
					$page->add(new HtmlHeader(4, VoorraadView::waardeLocatie($voorraad)));
					$table->add($row = new HtmlTableRow());
					$row->add(new HtmlTableHeaderCell(_("Aantal")))
						->add(new HtmlTableHeaderCell(_("Eerste mutatie")))
						->add(new HtmlTableHeaderCell(_("Laatste mutatie")))
						->add(new HtmlTableHeaderCell(_("Waarde per stuk")))
						->add(new HtmlTableHeaderCell(_("Verkoopbaar")))
						->add(new HtmlTableHeaderCell(_("Verkoopprijs")))
						->add(new HtmlTableHeaderCell(_("Omschrijving")));
					if(hasAuth('boekcom'))
						$row->add(new HtmlTableHeaderCell());
				}
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(VoorraadView::waardeAantal($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeEersteMutatie($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeLaatsteMutatie($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeWaardePerStuk($voorraad)))
					->add(new HtmlTableDataCell(VoorraadView::waardeVerkoopbaar($voorraad)))
					->add(new HtmlTableDataCell(VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs())))
					->add(new HtmlTableDataCell(VoorraadView::waardeOmschrijving($voorraad)));
				if(hasAuth('boekcom')) {
					$row->add(new HtmlTableDataCell(sprintf(_("(%s, %s)")
						, new HtmlAnchor($dictaat->url() . '/Voorraad/' . $voorraad->geefID(), _("geschiedenis"))
						, new HtmlAnchor($dictaat->url() . '/Voorraad/' . $voorraad->geefID() . '/Verplaatsen', _("verplaasten")))));
				}
			}
			$page->add($table);
		}

		$page->end();
	}

	static public function veranderBestand (Dictaat $dictaat)
	{
		$page = Page::getInstance()->start(sprintf(_("Verander bestand %s"), DictaatView::waardeNaam($dictaat)));

		$page->add($div = new HtmlDiv(null, 'btn-group'));

		$div->add(HtmlAnchor::button($dictaat->url(), _('Terug naar de dictaat-pagina')));
		$div->add(HtmlAnchor::button($dictaat->downloadUrl(), _('Het oude dictaat downloaden')));

		$page->add(new HtmlHR());

		$page->add($form = HtmlForm::named('veranderBestand'));
		$form->setAttribute("enctype", "multipart/form-data");

		$form->add(self::makeFormFileRow('dictaat', null, false, _('Selecteer het nieuwe bestand (pdf)')));
		$form->add(HtmlInput::makeFormSubmitButton(_('Verander het bestand!')));

		$page->end();
	}

	static public function downloadLink (Dictaat $dictaat)
	{
		if ($dictaat->getDigitaalVerspreiden() || hasAuth('bestuur'))
			return new HtmlSpan(array(
				new HtmlAnchor($dictaat->downloadUrl(),
						_('Download dictaat')), (!$dictaat->getDigitaalVerspreiden()) ?
						new HtmlSpan(
							_("Dit dictaat mag niet digitaal verspreid worden onder de studenten!"),
							'negatief') : null));
		return new HtmlSpan(_("Dit dictaat is helaas niet downloadbaar."), 'negatief');
	}

	static public function boekcomActies($dictaat)
	{
		$list = parent::boekcomActies($dictaat);
		$list->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Bestand", _("Verander bijbehorend bestand."))));
		$list->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Orders", _("Bekijk alle orders"))));
		$list->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Xeroxmail", _("Stuur een ordermail naar Xerox."))));
		$list->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Orderen", _("Plaats order bij drukkerij."))));
		$list->add(new HtmlListItem(new HtmlAnchor($dictaat->url() . "/Dictaat/Levering", _("Verwerk losse levering."))));
		return $list;
	}

	static public function formAuteur(Dictaat $dictaat, $include_id = false)
	{
		return self::defaultFormPersoonVerzameling(null, 'auteurContactID', true);
	}

	static public function waardeNaam (Artikel $dictaat)
	{
		if (!$dictaat instanceof Dictaat)
			return parent::waardeNaam($dictaat);
		return parent::waardeNaam($dictaat) . ' (' . self::waardeUitgaveJaar($dictaat) . ')';
	}

	/**
	 *  Voeg een formulier toe aan de pagina om Xerox te mailen.
	 *
	 * Deze mail heeft als bedoeling om ze te vragen om een paar dictaten te drukken.
	 *
	 * @return Een formulier, klaar om te adden.
	 */
	public static function xeroxMailForm(Dictaat $dictaat) {
		$form = HtmlForm::named("stuurMail");
		$form->add(new HtmlHeader(3, _("Verstuur een mail aan de Xerox")));

		$form->addImmutable(new HtmlParagraph(sprintf(_(
			"Met een druk op de knop stuur je een mail naar de Xerox ".
			"met de vraag of ze het dictaat %s willen drukken. ".
			"Vul eerst wel de benodigde velden in."
		), self::detailsLink($dictaat))));

		// Toon een preview van de mail die gemaild gaat worden
		$form->addImmutable(new HtmlHeader(4, sprintf(
			_("Template voor de mail: %s[VOC: (wijzig-link)]"),
			new HtmlAnchor(Template::geefVanLabel(BW2_XEROXMAIL_TEMPLATE)->wijzigURL(), _("(wijzig)"))
		)));
		$form->addImmutable(new HtmlPre(Template::geefOngerenderd(BW2_XEROXMAIL_TEMPLATE)));

		// Benodigdheden: aantal en kleur kaft
		$form->addImmutable(HtmlInput::makeHidden("dictaat", $dictaat->geefId()));
		$form->addImmutable(View::makeFormNumberRow("aantal", tryPar("aantal", NULL), 0, NULL, _("Hoeveel stuks:")));
		$form->addImmutable(View::makeFormStringRow("kaftsoort", tryPar("kaftsoort", NULL), _("Kleur van de kaft:")));

		$form->add($regel = new HtmlParagraph(_("Ontvanger van de mail:")));
		$regel->add(new HtmlStrong(Register::getValue("bw2_dictaatOrderAdres")));
		$regel->add(new HtmlAnchor(REGISTER_BASE, _("(wijzig)")));

		$form->addImmutable(HtmlInput::makeSubmitButton(_("Mail Xerox!")));

		return $form;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
