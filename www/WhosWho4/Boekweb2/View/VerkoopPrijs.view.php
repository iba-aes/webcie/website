<?
abstract class VerkoopPrijsView
	extends VerkoopPrijsView_Generated
{
	static public function wijzigForm(VerkoopPrijs $vp, $show_error = false)
	{
		$page = Page::getInstance()->start(_("Verkoopprijs wijzigen"));

		$page->add($form = HtmlForm::named('verkoopprijsForm'));

		$form->add(self::wijzigTable($vp, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Voer in")));

		$page->end();
	}

	static public function wijzigTable(VerkoopPrijs $vp, $show_error)
	{
		$tbody = new HtmlDiv();
		$tbody->add(self::wijzigTR($vp, 'prijs', $show_error));
		return $tbody;
	}

	static public function processNieuwForm(VerkoopPrijs $vp)
	{
		$vpPar = tryPar('VerkoopPrijs', null);
		$vp = new VerkoopPrijs($vp->getVoorraad(), new DateTimeLocale(), $vpPar['Prijs']);
		return $vp;
	}

	static public function formPrijs(VerkoopPrijs $obj, $include_id = false)
	{
		return self::defaultFormMoney($obj, 'Prijs', null, null, $include_id = false);
	}
}
