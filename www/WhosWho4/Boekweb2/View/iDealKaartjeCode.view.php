<?
abstract class iDealKaartjeCodeView
	extends iDealKaartjeCodeView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Genereer de scanbare QR-code van het kaartje.
	 * @param code De code van het kaartje.
	 * @param actTitel Een string die aangeeft waar het kaartje voor is.
	 * @return Een string met het kaartje als png.
	 */
	public static function maakQR($code, $actTitel="") {
		$kaartjePNG = iDealKaartjeCode::makeQR($code);
		$black = imagecolorallocate($kaartjePNG, 0x00, 0x00, 0x00);

		imagefttext($kaartjePNG, 16, 0, 5, 		30, 	$black, LIBDIR . "/Consolas.ttf", '1 kaartje ' . $actTitel);
		imagefttext($kaartjePNG, 28, 0, 10, 	320,	$black, LIBDIR . "/Consolas.ttf", $code);
		imagefttext($kaartjePNG, 16, 0, 160, 	305, 	$black, LIBDIR . "/Consolas.ttf", date("y-m-d"));
		imagefttext($kaartjePNG, 16, 0, 160, 	325, 	$black, LIBDIR . "/Consolas.ttf", date("H:i:s"));
		imagefttext($kaartjePNG, 20, 0, 275, 	315, 	$black, LIBDIR . "/Consolas.ttf", 1 . "/" . 1);

		ob_start();
		imagepng($kaartjePNG);
		$stringdata = ob_get_contents();
		ob_end_clean();
		return $stringdata;
	}
}
