<?
/**
 * $Id$
 */
class LeverancierVerzameling
	extends LeverancierVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // LeverancierVerzameling_Generated
	}

	static public function colaproductenLeveranciers ()
	{
		return LeverancierQuery::table()
			->whereProp('colaproducten', true)
			->verzamel();
	}

	static public function dibsproductenLeveranciers ()
	{
		return LeverancierQuery::table()
			->whereProp('dibsproducten', true)
			->verzamel();
	}

	static public function dictatenLeveranciers ()
	{
		return LeverancierQuery::table()
			->whereProp('dictaten', true)
			->verzamel();
	}

	static public function geefAlleLeveranciers()
	{
		return LeverancierQuery::table()
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
