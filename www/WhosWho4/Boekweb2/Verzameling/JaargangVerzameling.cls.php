<?
/**
 * $Id$
 */
class JaargangVerzameling
	extends JaargangVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // JaargangVerzameling_Generated
	}

	static public function geefVanVak(Vak $vak)
	{
		return JaargangQuery::table()
			->whereProp('Vak', $vak)
			->verzamel();
	}

	/**
	 * Geef alle huidig relevante jaargangen:
	 * dit zijn de jaargangen die in het gegeven interval vallen.
	 *
	 * Default: het interval is van nu tot over 1 jaar.
	 *
	 * @param DateTimeLocale|null $begin Het begin van het interval, dus de jaargangbegindatum moet hierna zijn.
	 * Default: nu.
	 * @param DateTimeLocale|null $eind Het eind van het interval, dus de jaargangeinddatum moet hiervoor zijn.
	 * Default: over een jaar.
	 *
	 * @return JaargangVerzameling
	 */
	public static function verzamelHuidige($begin = null, $eind = null)
	{
		if (is_null($begin))
		{
			$begin = new DateTimeLocale();
		}
		if (is_null($eind))
		{
			$eind = new DateTimeLocale();
			$eind->add(new DateInterval('P1Y'));
		}

		return JaargangQuery::table()
			->whereProp('datumBegin', '>=', $begin)
			->whereProp('datumBegin', '<', $eind)
			->verzamel();
	}
}
