<?
class TellingVerzameling
	extends TellingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TellingVerzameling_Generated
	}

	static public function geefAlleTellingen()
	{
		return TellingQuery::table()
			->verzamel();
	}
}
