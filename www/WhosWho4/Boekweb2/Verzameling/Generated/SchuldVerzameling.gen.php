<?
abstract class SchuldVerzameling_Generated
	extends TransactieVerzameling
{
	/**
	 * @brief De constructor van de SchuldVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // TransactieVerzameling
	}
	/**
	 * @brief Maak van deze SchuldVerzameling een TransactieVerzameling.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze SchuldVerzameling.
	 */
	public function toTransactieVerzameling()
	{
		if($this->aantal() == 0)
			return new TransactieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getTransactieTransactieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getTransactieTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return TransactieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een SchuldVerzameling van Transactie.
	 *
	 * @return SchuldVerzameling
	 * Een SchuldVerzameling die elementen bevat die bij de Transactie hoort.
	 */
	static public function fromTransactie($transactie)
	{
		if(!isset($transactie))
			return new SchuldVerzameling();

		return SchuldQuery::table()
			->whereProp('Transactie', $transactie)
			->verzamel();
	}
}
