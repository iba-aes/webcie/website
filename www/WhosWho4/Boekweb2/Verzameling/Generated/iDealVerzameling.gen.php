<?
abstract class iDealVerzameling_Generated
	extends GiroVerzameling
{
	/**
	 * @brief De constructor van de iDealVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // GiroVerzameling
	}
	/**
	 * @brief Maak van deze iDealVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze iDealVerzameling.
	 */
	public function toVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een iDealVerzameling van Voorraad.
	 *
	 * @return iDealVerzameling
	 * Een iDealVerzameling die elementen bevat die bij de Voorraad hoort.
	 */
	static public function fromVoorraad($voorraad)
	{
		if(!isset($voorraad))
			return new iDealVerzameling();

		return iDealQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}
}
