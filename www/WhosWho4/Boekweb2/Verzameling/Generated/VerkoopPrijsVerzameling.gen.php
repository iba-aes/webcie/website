<?
abstract class VerkoopPrijsVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VerkoopPrijsVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VerkoopPrijsVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VerkoopPrijsVerzameling.
	 */
	public function toVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VerkoopPrijsVerzameling van Voorraad.
	 *
	 * @return VerkoopPrijsVerzameling
	 * Een VerkoopPrijsVerzameling die elementen bevat die bij de Voorraad hoort.
	 */
	static public function fromVoorraad($voorraad)
	{
		if(!isset($voorraad))
			return new VerkoopPrijsVerzameling();

		return VerkoopPrijsQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}
}
