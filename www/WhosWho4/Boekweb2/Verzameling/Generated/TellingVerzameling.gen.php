<?
abstract class TellingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TellingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TellingVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze TellingVerzameling.
	 */
	public function toTellerVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTellerContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TellingVerzameling van Teller.
	 *
	 * @return TellingVerzameling
	 * Een TellingVerzameling die elementen bevat die bij de Teller hoort.
	 */
	static public function fromTeller($teller)
	{
		if(!isset($teller))
			return new TellingVerzameling();

		return TellingQuery::table()
			->whereProp('Teller', $teller)
			->verzamel();
	}
}
