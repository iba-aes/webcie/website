<?
abstract class TellingItemVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TellingItemVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TellingItemVerzameling een TellingVerzameling.
	 *
	 * @return TellingVerzameling
	 * Een TellingVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TellingItemVerzameling.
	 */
	public function toTellingVerzameling()
	{
		if($this->aantal() == 0)
			return new TellingVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTellingTellingID()
			                      );
		}
		$this->positie = $origPositie;
		return TellingVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TellingItemVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TellingItemVerzameling.
	 */
	public function toVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TellingItemVerzameling van Telling.
	 *
	 * @return TellingItemVerzameling
	 * Een TellingItemVerzameling die elementen bevat die bij de Telling hoort.
	 */
	static public function fromTelling($telling)
	{
		if(!isset($telling))
			return new TellingItemVerzameling();

		return TellingItemQuery::table()
			->whereProp('Telling', $telling)
			->verzamel();
	}
	/**
	 * @brief Maak een TellingItemVerzameling van Voorraad.
	 *
	 * @return TellingItemVerzameling
	 * Een TellingItemVerzameling die elementen bevat die bij de Voorraad hoort.
	 */
	static public function fromVoorraad($voorraad)
	{
		if(!isset($voorraad))
			return new TellingItemVerzameling();

		return TellingItemQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}
}
