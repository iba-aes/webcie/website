<?
abstract class VerplaatsingVerzameling_Generated
	extends VoorraadMutatieVerzameling
{
	/**
	 * @brief De constructor van de VerplaatsingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieVerzameling
	}
	/**
	 * @brief Maak van deze VerplaatsingVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VerplaatsingVerzameling.
	 */
	public function toTegenVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTegenVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VerplaatsingVerzameling van TegenVoorraad.
	 *
	 * @return VerplaatsingVerzameling
	 * Een VerplaatsingVerzameling die elementen bevat die bij de TegenVoorraad hoort.
	 */
	static public function fromTegenVoorraad($tegenVoorraad)
	{
		if(!isset($tegenVoorraad))
			return new VerplaatsingVerzameling();

		return VerplaatsingQuery::table()
			->whereProp('TegenVoorraad', $tegenVoorraad)
			->verzamel();
	}
}
