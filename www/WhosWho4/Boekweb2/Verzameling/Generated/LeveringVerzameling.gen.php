<?
abstract class LeveringVerzameling_Generated
	extends VoorraadMutatieVerzameling
{
	/**
	 * @brief De constructor van de LeveringVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieVerzameling
	}
	/**
	 * @brief Maak van deze LeveringVerzameling een OrderVerzameling.
	 *
	 * @return OrderVerzameling
	 * Een OrderVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze LeveringVerzameling.
	 */
	public function toOrderVerzameling()
	{
		if($this->aantal() == 0)
			return new OrderVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getOrderOrderID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getOrderOrderID()
			                      );
		}
		$this->positie = $origPositie;
		return OrderVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze LeveringVerzameling een LeverancierVerzameling.
	 *
	 * @return LeverancierVerzameling
	 * Een LeverancierVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze LeveringVerzameling.
	 */
	public function toLeverancierVerzameling()
	{
		if($this->aantal() == 0)
			return new LeverancierVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLeverancierContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LeverancierVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een LeveringVerzameling van Order.
	 *
	 * @return LeveringVerzameling
	 * Een LeveringVerzameling die elementen bevat die bij de Order hoort.
	 */
	static public function fromOrder($order)
	{
		if(!isset($order))
			return new LeveringVerzameling();

		return LeveringQuery::table()
			->whereProp('Order', $order)
			->verzamel();
	}
	/**
	 * @brief Maak een LeveringVerzameling van Leverancier.
	 *
	 * @return LeveringVerzameling
	 * Een LeveringVerzameling die elementen bevat die bij de Leverancier hoort.
	 */
	static public function fromLeverancier($leverancier)
	{
		if(!isset($leverancier))
			return new LeveringVerzameling();

		return LeveringQuery::table()
			->whereProp('Leverancier', $leverancier)
			->verzamel();
	}
}
