<?
abstract class BestellingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BestellingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BestellingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BestellingVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BestellingVerzameling een ArtikelVerzameling.
	 *
	 * @return ArtikelVerzameling
	 * Een ArtikelVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BestellingVerzameling.
	 */
	public function toArtikelVerzameling()
	{
		if($this->aantal() == 0)
			return new ArtikelVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getArtikelArtikelID()
			                      );
		}
		$this->positie = $origPositie;
		return ArtikelVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BestellingVerzameling van Persoon.
	 *
	 * @return BestellingVerzameling
	 * Een BestellingVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new BestellingVerzameling();

		return BestellingQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een BestellingVerzameling van Artikel.
	 *
	 * @return BestellingVerzameling
	 * Een BestellingVerzameling die elementen bevat die bij de Artikel hoort.
	 */
	static public function fromArtikel($artikel)
	{
		if(!isset($artikel))
			return new BestellingVerzameling();

		return BestellingQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}
}
