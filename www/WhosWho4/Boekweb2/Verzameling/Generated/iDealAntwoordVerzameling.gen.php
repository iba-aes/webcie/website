<?
abstract class iDealAntwoordVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de iDealAntwoordVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze iDealAntwoordVerzameling een iDealVerzameling.
	 *
	 * @return iDealVerzameling
	 * Een iDealVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze iDealAntwoordVerzameling.
	 */
	public function toIdealVerzameling()
	{
		if($this->aantal() == 0)
			return new iDealVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getIdealTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return iDealVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze iDealAntwoordVerzameling een ActiviteitVraagVerzameling.
	 *
	 * @return ActiviteitVraagVerzameling
	 * Een ActiviteitVraagVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze iDealAntwoordVerzameling.
	 */
	public function toVraagVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVraagVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVraagactiviteitID()
			                      ,$obj->getVraagVraagID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVraagVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een iDealAntwoordVerzameling van Ideal.
	 *
	 * @return iDealAntwoordVerzameling
	 * Een iDealAntwoordVerzameling die elementen bevat die bij de Ideal hoort.
	 */
	static public function fromIdeal($ideal)
	{
		if(!isset($ideal))
			return new iDealAntwoordVerzameling();

		return iDealAntwoordQuery::table()
			->whereProp('Ideal', $ideal)
			->verzamel();
	}
	/**
	 * @brief Maak een iDealAntwoordVerzameling van Vraag.
	 *
	 * @return iDealAntwoordVerzameling
	 * Een iDealAntwoordVerzameling die elementen bevat die bij de Vraag hoort.
	 */
	static public function fromVraag($vraag)
	{
		if(!isset($vraag))
			return new iDealAntwoordVerzameling();

		return iDealAntwoordQuery::table()
			->whereProp('Vraag', $vraag)
			->verzamel();
	}
}
