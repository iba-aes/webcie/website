<?
abstract class VoorraadMutatieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VoorraadMutatieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VoorraadMutatieVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VoorraadMutatieVerzameling.
	 */
	public function toVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VoorraadMutatieVerzameling van Voorraad.
	 *
	 * @return VoorraadMutatieVerzameling
	 * Een VoorraadMutatieVerzameling die elementen bevat die bij de Voorraad hoort.
	 */
	static public function fromVoorraad($voorraad)
	{
		if(!isset($voorraad))
			return new VoorraadMutatieVerzameling();

		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}
}
