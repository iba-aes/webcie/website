<?
abstract class JaargangVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de JaargangVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze JaargangVerzameling een VakVerzameling.
	 *
	 * @return VakVerzameling
	 * Een VakVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze JaargangVerzameling.
	 */
	public function toVakVerzameling()
	{
		if($this->aantal() == 0)
			return new VakVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVakVakID()
			                      );
		}
		$this->positie = $origPositie;
		return VakVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze JaargangVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze JaargangVerzameling.
	 */
	public function toContactPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContactPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een JaargangVerzameling van Vak.
	 *
	 * @return JaargangVerzameling
	 * Een JaargangVerzameling die elementen bevat die bij de Vak hoort.
	 */
	static public function fromVak($vak)
	{
		if(!isset($vak))
			return new JaargangVerzameling();

		return JaargangQuery::table()
			->whereProp('Vak', $vak)
			->verzamel();
	}
	/**
	 * @brief Maak een JaargangVerzameling van ContactPersoon.
	 *
	 * @return JaargangVerzameling
	 * Een JaargangVerzameling die elementen bevat die bij de ContactPersoon hoort.
	 */
	static public function fromContactPersoon($contactPersoon)
	{
		if(!isset($contactPersoon))
			return new JaargangVerzameling();

		return JaargangQuery::table()
			->whereProp('ContactPersoon', $contactPersoon)
			->verzamel();
	}
}
