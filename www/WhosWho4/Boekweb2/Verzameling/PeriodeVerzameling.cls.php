<?
class PeriodeVerzameling
	extends PeriodeVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PeriodeVerzameling_Generated
	}

	/**
	 * @brief Geef de opeenvolgende periodes waar begin- en einddatum inzitten.
	 */
	static public function tussen($begindatum, $einddatum) {
		// Geef alle periodes die beginnen NA begindatum en voor einddatum.
		$query = PeriodeQuery::table()
			->whereProp("datumBegin", ">", $begindatum)
			->whereProp("datumBegin", "<", $einddatum)
			->orderByAsc("datumBegin");
		$resultaat = $query->verzamel();
		// Hier ontbreekt dus de periode die $begindatum bevat, voeg die toe.
		$resultaat->voegtoe(Periode::vanDatum($begindatum));
		return $resultaat;
	}
}
