<?
class iDealAntwoordVerzameling
	extends iDealAntwoordVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // iDealAntwoordVerzameling_Generated
	}

	static public function vanActiviteit(Activiteit $act)
	{
		return iDealAntwoordQuery::table()
			->join('ActiviteitVraag')
			->join('iDeal')
			->whereProp('Activiteit', $act)
			->whereProp('Status', 'SUCCESS')
			->verzamel();
	}
}
