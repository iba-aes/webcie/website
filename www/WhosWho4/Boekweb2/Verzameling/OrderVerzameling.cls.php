<?
/**
 * $Id$
 */
class OrderVerzameling
	extends OrderVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // OrderVerzameling_Generated
	}

	static public function vanArtikel (Artikel $artikel)
	{
		return OrderQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}

	static public function openVanArtikel (Artikel $artikel)
	{
		$lv = LeveringQuery::table()
			->join('Order')
			->whereProp('Order.artikel', $artikel)
			->whereProp('Order.geannuleerd', false)
			->verzamel();

		$orders = OrderQuery::table()
			->whereProp('Artikel', $artikel)
			->whereProp('geannuleerd', false)
			->verzamel();

		$aantal = array();
		foreach($orders as $o) {
			$aantal[$o->geefID()] = 0;
		}

		foreach($lv as $l) {
			$aantal[$l->getOrderOrderID()] = $l->getAantal();
		}

		$ids = new OrderVerzameling();
		foreach($aantal as $key => $a) {
			$order = Order::geef($key);
			if($a < $order->getAantal()) {
				$ids->voegtoe($order);
			}
		}

		return $ids;
	}

	static public function geslotenVanArtikel (Artikel $artikel)
	{
		$lv = LeveringQuery::table()
			->join('Order')
			->whereProp('Order.artikel', $artikel)
			->whereProp('Order.geannuleerd', false)
			->verzamel();

		$orders = OrderQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();

		$aantal = array();
		foreach($orders as $o) {
			$aantal[$o->geefID()] = 0;
		}

		foreach($lv as $l) {
			if(isset($aantal[$l->getOrderOrderID()]))
				$aantal[$l->getOrderOrderID()] += $l->getAantal();
			else
				$aantal[$l->getOrderOrderID()] = $l->getAantal();
		}

		$ids = new OrderVerzameling();
		foreach($aantal as $key => $a) {
			$order = Order::geef($key);
			if($a >= $order->getAantal()) {
				$ids->voegtoe($order);
			}
		}

		return $ids;
	}

	static public function geannuleerdVanArtikel (Artikel $artikel)
	{
		return OrderQuery::table()
			->whereProp('Artikel', $artikel)
			->whereProp('geannuleerd', true)
			->verzamel();
	}

	public function getAantal ()
	{
		$aantal = 0;
		foreach ($this as $that)
			$aantal += $that->getAantal();
		return $aantal;
	}

	static public function geefAlleOrders()
	{
		return OrderQuery::table()->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
