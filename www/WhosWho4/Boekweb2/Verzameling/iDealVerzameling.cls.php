<?
/**
 * $Id$
 */
class iDealVerzameling
	extends iDealVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // iDealVerzameling_Generated
	}

	public static function openAndExpired()
	{
		return iDealQuery::table()
			->whereString('Transactie.overerving', 'iDeal')
			->whereInProp('status', ["OPEN", "OPENPOGING2", "OPENPOGING3", "OPENPOGING4", "OPENPOGING5"])
			->whereProp('expire', '<=', new DateTimeLocale())
			->verzamel();
	}

	public static function allemaal($status = null, $artikel = null, $maand, $jaar)
	{
		global $WSW4DB;

		$where = '';
		switch($status)
		{
			case 'gelukt': $where = ' LEFT JOIN `Transactie` ON `Transactie`.`transactieID` = `iDeal`.`transactieID` WHERE `Transactie`.`status`="SUCCESS"'; break;
			case 'mislukt': $where = ' LEFT JOIN `Transactie` ON `Transactie`.`transactieID` = `iDeal`.`transactieID` WHERE `status` IN("CANCELLED", "EXPIRED", "FAILURE", "BANKFAILURE")'; break;
			case 'open': $where = ' LEFT JOIN `Transactie` ON `Transactie`.`transactieID` = `iDeal`.`transactieID` WHERE `status` IN("OPEN", "OPENPOGING2", "OPENPOGING3", "OPENPOGING4", "OPENPOGING5")'; break;
			case 'inactief': $where = ' LEFT JOIN `Transactie` ON `Transactie`.`transactieID` = `iDeal`.`transactieID` WHERE `status`="INACTIVE"'; break;
			default: $where = ' LEFT JOIN `Transactie` ON `Transactie`.`transactieID` = `iDeal`.`transactieID` WHERE 1=1'; break;
		}
		
		if($artikel)
		{
			$variantjoin = " LEFT JOIN `Voorraad` ON `Voorraad`.`voorraadID` = `iDeal`.`voorraad_voorraadID` LEFT JOIN `Artikel` ON `Artikel`.`artikelID` = `Voorraad`.`artikel_artikelID` ";
			$where .= " AND `Artikel`.`artikelID` = ". $artikel;
		}
		else
		{
			$variantjoin = "";
		}

		$where .= " AND MONTH(`Transactie`.`gewijzigdWanneer`) = $maand AND YEAR(`Transactie`.`gewijzigdWanneer`) = $jaar";
		$ids = $WSW4DB->q('COLUMN SELECT `iDeal`.`transactieID` FROM `iDeal`' . $variantjoin . $where);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
