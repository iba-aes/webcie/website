<?
/**
 * $Id$
 */
class DictaatVerzameling
	extends DictaatVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DictaatVerzameling_Generated
	}

	static public function alle ()
	{
		return DictaatQuery::table()
			->orderByDesc('artikelID')
			->verzamel();
	}

	static public function verkoopbaar ()
	{
		return VoorraadQuery::table()
			->join('Artikel')
			->whereProp('verkoopbaar', true)
			->whereString('Artikel.overerving', 'Dictaat')
			->verzamel('Dictaat');
	}

	static public function verkoopbaarInBoekenhok ()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Voorraad`.`artikel_artikelID` FROM ('
						. 'SELECT `Voorraad`.`artikel_artikelID`, SUM(`VoorraadMutatie`.`aantal`) AS `s` FROM `VoorraadMutatie`'
						. ' LEFT JOIN `Voorraad` ON `VoorraadMutatie`.`voorraad_voorraadID` = `Voorraad`.`voorraadID`'
						. ' WHERE `Voorraad`.`locatie` = \'boekenhok\''
						. ' GROUP BY `Voorraad`.`artikel_artikelID`) AS `t`'
						. ' LEFT JOIN `Artikel` ON `Voorraad`.`artikel_artikelID` = `Artikel`.`artikelID`'
						. ' WHERE `t`.`s` > 0 AND `Voorraad`.`verkoopbaar` = 1 AND `Artikel`.`overerving` = \'Dictaat\'');

		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
