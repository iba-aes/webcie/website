<?
/**
 * $Id$
 */
class TransactieVerzameling
	extends TransactieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TransactieVerzameling_Generated
	}

	// Vraag alle transacties van een bepaald persoon op
	static public function getPersoonTransacties (Persoon $persoon, $aantal = null)
	{
		$query = TransactieQuery::table()
			->whereProp('Contact', $persoon)
			->orderByDesc('wanneer');

		if (is_int($aantal) && $aantal > 0) {
			$query->limit($aantal);
		}

		return $query->verzamel();
	}

	static public function getTransactiesVanVerkoop(Boekverkoop $verkoop, $pin = 'pin1')
	{
		$opentijd = $verkoop->getOpen();
		$geslotentijd = (!is_null($verkoop->getGesloten())) ? $verkoop->getGesloten() : new DateTimeLocale();

		$apps = Pin::enumsApparaat();
		if($pin == 'pin1') {
			$app = $apps[0];
		} else {
			$app = $apps[1];
		}

		return PinQuery::table()
			->whereProp('Rubriek', 'BOEKWEB')
			->whereProp('wanneer', '>', $opentijd)
			->whereProp('wanneer', '<', $geslotentijd)
			->whereProp('apparaat', $app)
			->verzamel('Transactie');
	}

	public function totaal()
	{
		$totaal = 0;

		foreach($this as $transactie) {
			$totaal += $transactie->getBedrag();
		}

		return $totaal;
	}

	static public function geefAlleVanBeginEnEindEnRubriekEnSoort($begin, $eind, $rubriek, $soort)
	{
		$query = TransactieQuery::table()
			->whereProp('wanneer', '>=', $begin)
			->whereProp('wanneer', '<=', $eind);

		if($rubriek) {
			$query->whereProp('rubriek', $rubriek);
		}

		if($soort) {
			$query->whereProp('soort', $soort);
		}

		return $query->verzamel();
	}

	static public function verkopenVanVoorraad(Voorraad $voorraad)
	{
		return TransactieQuery::table()
			->join('Verkoop')
			->join('VoorraadMutatie')
			->whereProp('VoorraadMutatie.voorraad', $voorraad)
			->verzamel();
	}

	public function getVerkopen()
	{
		$verkopen = new VerkoopVerzameling();

		foreach($this as $transactie) {
			$verkopen->union($transactie->getVerkopen());
		}

		return $verkopen;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
