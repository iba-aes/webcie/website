<?
class BoekverkoopVerzameling
	extends BoekverkoopVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BoekverkoopVerzameling_Generated
	}

	static public function geefBoekverkopen()
	{
		return BoekverkoopQuery::table()
			->orderByDesc('verkoopdag')
			->limit(100)
			->verzamel();
	}

	static public function geefAlleVanDag($dag)
	{
		return BoekverkoopQuery::table()
			->whereProp('verkoopdag', new DateTimeLocale($dag))
			->verzamel();
	}
}
