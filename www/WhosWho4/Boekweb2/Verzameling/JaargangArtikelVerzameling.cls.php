<?
/**
 * $Id$
 */
class JaargangArtikelVerzameling
	extends JaargangArtikelVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // JaargangArtikelVerzameling_Generated
	}

	static public function vanArtikel (Artikel $artikel)
	{
		return JaargangArtikelQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
