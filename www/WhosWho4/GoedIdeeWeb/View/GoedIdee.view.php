<?
abstract class GoedIdeeView
	extends GoedIdeeView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeView.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdee(GoedIdee $obj)
	{
		return self::defaultWaarde($obj);

	}
	/**
	 * Maak een visuele representatie van de huidige toestand en het eerste 
	 * bericht van een GoedIdee.
	 *
	 * @param idee Een GoedIdee.
	 * @return HtmlDiv Een HtmlDiv die informatie over en de oorspronkelijke 
	 * tekst van idee bevat.
	 * @see GoedIdeeBerichtVerzamelingView::toonBerichten
	 */
	public static function toon (GoedIdee $idee)
	{
		$ingelogd = Persoon::getIngelogd();
		$reacties = GoedIdeeBerichtVerzameling::geefReacties($idee);

		$div = new HtmlDiv(NULL, 'goedidee');

		$div->add(new HtmlHeader(2, sprintf('%s: %s',
			self::waardeGoedIdeeID($idee),
			self::waardeTitel($idee))));

		if ($ingelogd)
		{
			$form = new HtmlForm('get', GOEDIDEEWEBBASE . '/InverteerGoedIdeeSortering');
			$form->add(HtmlInput::makeSubmitButton(_('Inverteer sortering')));
			$form->addClass('sorteringsbutton');
			$div->add($form);
		}

		if (hasAuth('bestuur') || hasAuth('experimencie'))
		{
			if (!$idee->getPubliek())
			{
				$div->add($form = new HtmlForm('get', 'Publiek'));
				$form->add(HtmlInput::makeSubmitButton(_('Maak dit goede idee openbaar'), ''));
			}
			else
			{
				$div->add($form = new HtmlForm('get', 'Verborgen'));
				$form->add(HtmlInput::makeSubmitButton(_('Verberg dit goede idee'), ''));
			}
		}

		$div->add(new HtmlDiv(new HtmlStrong(self::labelStatus($idee) . ':') . ' ' . self::waardeStatus($idee)));

		$div->add(GoedIdeeBerichtView::toonIdee($idee));

		return $div;
	}

	/**
	 * Maak een visuele representatie van de reacties op een GoedIdee.
	 * 
	 * @param idee Een GoedIdee.
	 * @param omgekeerd Indien True, worden de GoedIdeeBerichten in aflopende 
	 * volgorde weergegeven; anders worden ze in oplopende volgorde 
	 * weergegeven.
	 * @return HtmlDiv Een HtmlDiv die de reacties op GoedIdee bevat.
	 */
	public static function toonReacties (GoedIdee $idee, $omgekeerd = False)
	{
		$reacties = $idee->getReacties(); 

		$div = new HtmlDiv(NULL, 'goedidee');
		$div->add(GoedIdeeBerichtVerzamelingView::toonBerichten($reacties, $omgekeerd));

		return $div;
	}

	/**
	 * Maak een tabelrij van een GoedIdee.
	 *
	 * @param idee Een GoedIdee.
	 * @return HtmlTableRow Een HtmlTableRow die het nummer, de huidige status en de huidige titel van GoedIdee bevat.
	 */
	public static function toTableRow (GoedIdee $idee)
	{
		$url = $idee->url();
		$openIdeeJs = "document.location='$url'";

		$row = new HtmlTableRow(NULL, self::waardeStatus($idee));
		$td = $row->addData(self::makeLink($idee));
		$td = $row->addData(self::getGewijzigdTijd($idee));
		$td = $row->addData(self::waardeStatus($idee));

		$td = $row->addData(new HtmlAnchor($idee->url(), self::waardeTitel($idee)));

		$td = $row->addData(self::waardePubliek($idee->getPubliek()));

		$row->addClass(strtolower($idee->getStatus()));

		return $row;
	}

	/**
	 * Maak een formulier om een nieuw Goed Idee te melden.
	 *
	 * @param goedidee Het nieuwe Goed Idee.
	 * @param bericht Het bericht van het nieuwe Goed Idee. Dit is een aparte 
	 * parameter omdat GoedIdee geen veld bericht heeft (dat zit in GoedIdeeBericht).
	 * @param toon_fouten Indien True, toon een kolom met foutmeldingen.
	 * @return HtmlForm Een HtmlForm met velden om een nieuw GoedIdee te melden, 
	 * vooringevuld met de informatie in goedidee en bericht.
	 */
	public static function nieuwGoedIdee (GoedIdee $goedidee, $bericht, $toon_fouten)
	{
		$form = HtmlForm::named('NieuwGoedIdee');

		if (hasAuth('bestuur') || hasAuth('experimencie'))
		{
			$formdiv = new HTMLDiv(null, 'form-group');
			$melderdiv = new HTMLDiv(null, 'col-sm-10');
			$melderdiv->add(self::defaultFormPersoonVerzameling(null, 'Melder', true, false, true));
			$label = new HtmlLabel('Melder', 'Melder', 'col-sm-2 control-label');
			$formdiv->add($label);
			$formdiv->add($melderdiv);
			$form->add($formdiv);
		}
		$form->add(self::wijzigTR($goedidee, 'Titel', $toon_fouten));
		$form->add(self::wijzigTR($goedidee, 'Bericht', $toon_fouten));

		$form->add(HtmlInput::makeFormSubmitButton(_('Voer in')));

		return $form;
	}

	/**
	 * Geef de tijd waarop er voor het laatst iets aan het goedidee gewijzgd is
	 * of wanneer er voor het laatst iemand op het goedidee gereageerd heeft
	 *
	 * @param idee Een GoedIdee.
	 * @return DateTimeLocale De tijd waarop het goede idee voor het laatst gewijzigd is
	 */
	static public function getGewijzigdTijd(GoedIdee $idee)
	{
		$gewijzigdtijd = $idee->getMoment();

		$berichten = $idee->getBerichten();
		
		$laatstetijd = $berichten->last()->getMoment();

		if($gewijzigdtijd <= $laatstetijd)
			return $laatstetijd->format('Y-m-d H:i:s');
		else
			return $gewijzigdtijd->format('Y-m-d H:i:s');
	}

	/**
	 * Maak een link naar een GoedIdee.
	 *
	 * @param idee Een GoedIdee.
	 * @param titel (Optioneel) de tekst waar je op kan klikken. Per default het ideeID.
	 * @return HtmlAnchor Een HtmlAnchor naar de url van idee met gegeven titel.
	 */
	public static function makeLink (GoedIdee $idee, $titel = NULL)
	{
		if (!$titel) {
			$titel = self::waardeGoedIdeeID($idee);
		}
		return new HtmlAnchor(HTTPS_ROOT . $idee->url(), $titel);
	}
	
	public static function waardeMelder(GoedIdee $obj)
	{
		return PersoonView::makeLink($obj->getMelder());
	}

	public static function waardeMoment(GoedIdee $idee)
	{
		if (($moment = $idee->getMoment()) instanceof DateTimeLocale)
			return $moment->strftime('%Y-%m-%d %H:%M');
		return NULL;
	}

	public static function waardePubliek($value)
	{
		if($value){
			return _('Ja');
		}
		else{
			return _('Nee');
		}
	}

	public static function formBericht(GoedIdee $obj)
	{
		$bericht = tryPar('GoedIdee[Bericht]', tryPar('bericht', $obj->getOmschrijving()));
		$element = HtmlTextarea::withContent($bericht, 'GoedIdee[Bericht]', 80, 10);
		$element->addClass('monospace');
		return $element;
	}

	public static function melder(GoedIdee $idee)
	{
		$infoMelder = "Melder: " . PersoonView::naam($idee->getMelder());
		return $infoMelder;
	}

	public static function labelEnumStatus($value)
	{
		switch ($value)
		{
		case 'GEMELD':
			return _('Gemeld');
		case 'AFGEWEZEN':
			return _('Afgewezen');
		case 'GEKEURD':
			return _('Goedgekeurd');
		case 'BEZIG':
			return _('Wordt aan gewerkt');
		case 'UITGEVOERD':
			return _('Uitgevoerd');
		case 'VERJAARD':
			return _('Verjaard');
		}
	}

	public static function labelGoedIdeeID(GoedIdee $obj = NULL)
	{
		return '#';
	}
	public static function labelMelder(GoedIdee $obj = NULL)
	{
		return _('Melder');
	}
	public static function labelTitel(GoedIdee $obj = NULL)
	{
		return _('Titel');
	}
	public static function labelStatus(GoedIdee $obj = NULL)
	{
		return _('Status');
	}
	public static function labelMoment(GoedIdee $obj = NULL)
	{
		return _('Moment');
	}
	public static function labelBericht(GoedIdee $obj = NULL)
	{
		return _('Bericht') . (new HtmlSpan('*', 'verplicht'));
	}
	public static function labelPubliek(GoedIdee $obj = NULL)
	{
		return 'Publiek?';
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld Bericht
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bericht representeert.
	 */
	public static function opmerkingBericht()
	{
		return NULL;
	}

}
