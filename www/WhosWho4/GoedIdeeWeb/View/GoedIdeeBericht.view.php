<?
abstract class GoedIdeeBerichtView
	extends GoedIdeeBerichtView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeBerichtView.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdeeBericht(GoedIdeeBericht $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * De regex waarmee we verwijzingen naar goedideeen in berichten vinden.
	 */
	const GOEDIDEE_REGEX = '/\#(\d+)/';

	/**
	 * Maak een visuele representatie van een GoedIdeeBericht.
	 *
	 * @param ideeBericht Een GoedIdeeBericht.
	 * @param vorige Het GoedIdeeBericht dat voor GoedIdeeBericht kwam. Wordt gebruikt om 
	 * de verschillen tussen de twee te berekenen en tonen.
	 * @return HtmlDiv Een HtmlDiv die een visuele representatie van GoedIdeeBericht bevat.
	 */
	static public function toon (GoedIdeeBericht $ideeBericht, GoedIdeeBericht $vorige = NULL)
	{
		$melderDiv = new HtmlDiv(null, 'col-md-3');
		$melderDiv->add(new HtmlSpan(self::waardeMelder($ideeBericht), 'melder'));
		$melderDiv->add(new HtmlBreak());
		$melderDiv->add(new HtmlSpan(self::waardeMoment($ideeBericht), 'moment'));


		$diff = new HtmlParagraph();
		if ($vorige instanceof GoedIdeeBericht) {
			// Genereer de diff tussen dit GoedIdeeBericht en de vorige
			$veranderingen = GoedIdeeBericht::veranderingen($vorige, $ideeBericht);
	
			if ($dTitel = $veranderingen['titel']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Titel') . ':')) . vsprintf(' %s &#8594; %s', $dTitel));
				$diff->add(new HtmlBreak());
			}
			if ($dStatus = $veranderingen['status']) {
				$diff->add(new HtmlSpan(new HtmlStrong(_('Status') . ':')) . vsprintf(' %s &#8594; %s', $dStatus));
				$diff->add(new HtmlBreak());
			}			
		}

		if ($diff->numHtmlChildren() > 0) {
			$melderDiv->add($diff);
		}

		$berichtDiv = new HtmlDiv(self::waardeBericht($ideeBericht), 'col-md-9 bericht monospace');
		return new HtmlDiv(array($melderDiv, $berichtDiv), 'row');
	}

	public static function toonIdee (GoedIdee $idee)
	{
		$ideeBericht = $idee->getIdeeBericht();

		$berichtDiv = new HtmlDiv(NULL, 'row');
		$berichtDiv->add($melderDiv = new HtmlDiv(NULL, 'col-md-3'));
		$melderDiv->add(new HtmlSpan(self::waardeMelder($ideeBericht), 'melder'));
		$melderDiv->add(new HtmlBreak());
		$melderDiv->add(new HtmlSpan(self::waardeMoment($ideeBericht), 'moment'));

		$berichtDiv->add(new HtmlDiv(GoedIdeeBerichtView::waardeBericht($ideeBericht), 'col-md-9 eersteBericht bericht monospace'));
		return $berichtDiv;
	}

	/**
	 *  Maak een formulier om een nieuw GoedIdeeBericht bij een GoedIdee te plaatsen.
	 *
	 * @param idee Een GoedIdee.
	 * @param titel De titel die in het formulier komt te staan.
	 * @param status De status die in het formulier komt te staan.
	 * @param bericht Het Bericht dat in het formulier komt te staan.
	 * @param toon_fouten Indien True, toon een kolom met foutmeldingen.
	 * @return HtmlForm Een HtmlForm om een nieuw GoedIdeeBericht bij een GoedIdee te plaatsen. De 
	 * informatie in de parameters is hierin ingevuld.
	 */
	public static function nieuwBericht (GoedIdee $idee, $titel, $status, $bericht, $toon_fouten)
	{
		global $request;

		$titel = $titel ?: $idee->getTitel();
		$status = $status ?: $idee->getStatus();

		$form = HtmlForm::named('NieuwGoedIdeeBericht');
		$form->addClass('form-inline goedidee');


		// Deze post-var is om te controleren of er nog nieuwe GoedIdeeberichten zijn
		// toegevoegd aan het goede idee terwijl de user het bericht aan het schrijven was
		$now = new DateTimeLocale();
		$form->add(HtmlInput::makeHidden('nieuwBerichtTime', $now->format('Y-m-d H:i:s')));

		$form->add(HtmlInput::makeText('GoedIdeeBericht[Titel]', $titel, 50));

		$form->add(new HtmlBreak());
		
		if (hasAuth('bestuur') || hasAuth('experimencie')){
			$form->add(new HtmlLabel('GoedIdeeBericht[Status]', _('Status:')));
			$form->add(self::maakStatusForm('GoedIdeeBericht[Status]', $status));
		}
		$form->add($textarea = HtmlTextarea::withContent($bericht, 'GoedIdeeBericht[Bericht]', 80, 10));
		$textarea->addClass('monospace');

		$form->add(new HtmlBreak());
		$form->add(HtmlInput::makeSubmitButton(_('Voer in')));

		if($request->server->get('HTTP_REFERER', null) || tryPar('referer'))
		{
			$form->add(HtmlInput::makeHidden('referer', tryPar('referer', $request->server->get('HTTP_REFERER'))));
		}

		return $form;
	}

	/**
	 * Verstuur een mail na het aanmaken van het GoedIdeeBericht
	 *
	 * De mail wordt verstuurd naar de melder.
	 *
	 * @param obj Een GoedIdeeBericht.
	 * @param isNieuweIdee Indien True worden de teksten van de mails erop 
	 * aangepast dat het een nieuwe GoedIdee betreft; anders wordt er vanuit gegaan 
	 * dat het om een nieuw bericht bij een bestaand GoedIdee gaat.
	 */
	public static function verstuurMails (GoedIdeeBericht $obj, $isNieuwIdee)
	{
		$idee = $obj->getGoedIdee();
		$melder = $obj->getMelder();

		if ($vorige = $obj->vorige()) {
			$veranderingen = GoedIdeeBericht::veranderingen($vorige, $obj);
		}
		else {
			$veranderingen = array('titel' => False, 'status' => False);
		}

		$waardeGoedIdeeID = GoedIdeeView::waardeGoedIdeeID($idee);

		$subject = sprintf('Goed Idee #%s: %s', $waardeGoedIdeeID, $obj->getTitel());
		if ($isNieuwIdee) {
			$mailbody = "<p>" . _('Er is een Goed Idee aan het GoedIdeeWeb van A–Eskwadraat toegevoegd.') . "</p>";
		} else {
			$mailbody = "<p>" . sprintf(_('Er is een Goed Idee in het GoedIdeeWeb van A–Eskwadraat gewijzigd door %s.'), PersoonView::naam($melder)) . "</p>";	
		}

        $mailbody .= GoedIdee::makeLink($idee, _('Klik op deze link voor meer informatie'))->makeHtml();

		if ($dTitel = $veranderingen['titel']) {
			$infoTitel = _('Goed Idee') . vsprintf(" $waardeGoedIdeeID: %s -> %s", $dTitel);
		} else {
			$infoTitel = _('Goed Idee') . " $waardeGoedIdeeID: " . GoedIdeeBerichtView::waardeTitel($obj);
		}

        $infoMelder = GoedIdeeView::melder($idee);
        $infoWanneer = "Om: " . GoedIdeeBerichtView::waardeMoment($obj);

		if ($dStatus = $veranderingen['status']) {
			$infoStatus = _('Status') . vsprintf(': %s -> %s', $dStatus);
		} else {
			$infoStatus = _('Status') . ': ' . GoedIdeeBerichtView::waardeStatus($obj);
		}
		$mailbody .= sprintf("
			<p><strong>
			<hr />
			%s<br />
			%s<br />
			%s<br />
			%s<br />
			</strong>",
			$infoTitel,
			$infoMelder,
			$infoWanneer,
			$infoStatus);

		// stop de berichten van nieuw naar oud in de mail
		$huidige = $obj;
		do {
			$mailbody .= "</p><hr /><p>";
			if ($melder = $huidige->getMelder()) {
				$mailbody .= PersoonView::naam($melder);
			} else {
				$mailbody .= $huidige->getMelderEmail();
			}

			$mailbody .= " (".GoedIdeeBerichtView::waardeMoment($huidige)."):<br />";

			if($huidige->vorige())
			{
				$diff = GoedIdeeBericht::veranderingen($huidige->vorige(), $huidige);
				foreach($diff as $key => $d) {
					if(is_null($d)) {
						continue;
					}

					$func = 'label'.ucfirst($key);
					$mailbody .= "<b>".GoedIdeeView::{$func}()."</b>: ";
					$mailbody .= vsprintf("%s -> %s<br />", $d);
				}
			}

			$mailbody .= GoedIdeeBerichtView::waardeBericht($huidige);

		} while ($huidige = $huidige->vorige());
		$mailbody .= "</p>";


		$mail = new Email();
		$mail->setTo($melder);
		$mail->setFrom(Commissie::cieByLogin("webcie"));
		$mail->setSubject($subject);
		$mail->setBody($mailbody);

		if ($mail->valid()) {
			$mail->send();
		}	
	}




	public static function waardeMelder(GoedIdeeBericht $obj)
	{
		return PersoonView::makeLink($obj->getMelder());
	}

	public static function maakStatusForm ($naam, $status)
	{
		$statussen = array();
		foreach (GoedIdee::enumsStatus() as $st)
		{
			$statussen[$st] = self::labelEnumStatus($st);
		}

		$box = HtmlSelectbox::fromArray($naam, $statussen, $status);
		$box->setSize(1);
		return $box;
	}

	public static function labelEnumStatus($value)
	{
		return GoedIdeeView::labelEnumStatus($value);
	}

	public static function waardeBericht(GoedIdeeBericht $obj)
	{
		$bericht = htmlspecialchars($obj->getBericht());
		return self::goedIdeeBerichtLinks(nl2br($bericht));
	}
	public static function waardeMoment(GoedIdeeBericht $obj)
	{
		if (($moment = $obj->getMoment()) instanceof DateTimeLocale)
			return $moment->strftime('%Y-%m-%d %H:%M');
		return NULL;
	}

	protected static function goedIdeeBerichtLinks ($tekst)
	{
		// Regex voor externe URLs (syntax: http://blablalba OF https://blablabla)
		$tekst = preg_replace('/(^|\s)(https?:\/\/[A-Za-z0-9\-]+\.[A-Za-z0-9\.\-]+[A-Za-z0-9\-\.\_~\:\?\&\#\=\;\+\'\!\@\$\%\/\[\]]*)/',
				'${1}<a href="${2}">${2}</a>', $tekst);
		// Regex voor interne URLs (syntax: /blablabla/meermeukvanjouwlink)
		$tekst = preg_replace('/(^|\s)' .                                     '(\/[A-Za-z0-9\-\.\_~\:\?\&\#\=\;\+\'\!\@\$\%\/\[\]]+)/',
				'${1}<a href="'.HTTPS_ROOT.'${2}">${2}</a>', $tekst);

		// GoedIdeeen
		$tekst = preg_replace_callback(self::GOEDIDEE_REGEX,
			function($matches)
			{
				$b = GoedIdee::geef((int)$matches[1]);
				return ($b instanceof GoedIdee ? GoedIdeeView::makeLink($b, $matches[0]) : $matches[0]); },
			$tekst);

		// Lid-nrs
		$tekst = preg_replace('/(^|(?<=\s))\+(\d+)/', '<a href="'.HTTPS_ROOT.'/Leden/${2}">${0}</a>', $tekst);

		return $tekst;
	}

}
