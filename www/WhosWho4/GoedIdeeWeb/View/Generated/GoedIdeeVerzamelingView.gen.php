<?
abstract class GoedIdeeVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeVerzamelingView.
	 *
	 * @param GoedIdeeVerzameling $obj Het GoedIdeeVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdeeVerzameling(GoedIdeeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
