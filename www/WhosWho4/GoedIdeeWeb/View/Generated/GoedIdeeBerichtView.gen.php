<?
abstract class GoedIdeeBerichtView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeBerichtView.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdeeBericht(GoedIdeeBericht $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld goedideeBerichtID.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld goedideeBerichtID labelt.
	 */
	public static function labelGoedideeBerichtID(GoedIdeeBericht $obj)
	{
		return 'GoedideeBerichtID';
	}
	/**
	 * @brief Geef de waarde van het veld goedideeBerichtID.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld goedideeBerichtID van het
	 * object obj representeert.
	 */
	public static function waardeGoedideeBerichtID(GoedIdeeBericht $obj)
	{
		return static::defaultWaardeInt($obj, 'GoedideeBerichtID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * goedideeBerichtID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld goedideeBerichtID representeert.
	 */
	public static function opmerkingGoedideeBerichtID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld goedidee.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld goedidee labelt.
	 */
	public static function labelGoedidee(GoedIdeeBericht $obj)
	{
		return 'Goedidee';
	}
	/**
	 * @brief Geef de waarde van het veld goedidee.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld goedidee van het object obj
	 * representeert.
	 */
	public static function waardeGoedidee(GoedIdeeBericht $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getGoedidee())
			return NULL;
		return GoedIdeeView::defaultWaardeGoedIdee($obj->getGoedidee());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld goedidee.
	 *
	 * @see genericFormgoedidee
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld goedidee staat en kan
	 * worden bewerkt. Indien goedidee read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formGoedidee(GoedIdeeBericht $obj, $include_id = false)
	{
		return static::waardeGoedidee($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld goedidee. In
	 * tegenstelling tot formgoedidee moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formgoedidee
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld goedidee staat en kan
	 * worden bewerkt. Indien goedidee read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormGoedidee($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * goedidee bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld goedidee representeert.
	 */
	public static function opmerkingGoedidee()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melder.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld melder labelt.
	 */
	public static function labelMelder(GoedIdeeBericht $obj)
	{
		return 'Melder';
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melder van het object obj
	 * representeert.
	 */
	public static function waardeMelder(GoedIdeeBericht $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMelder())
			return NULL;
		return LidView::defaultWaardeLid($obj->getMelder());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melder.
	 *
	 * @see genericFormmelder
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is betreft het een statisch html-element.
	 */
	public static function formMelder(GoedIdeeBericht $obj, $include_id = false)
	{
		return LidView::defaultForm($obj->getMelder());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melder. In
	 * tegenstelling tot formmelder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmelder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMelder($name, $waarde=NULL)
	{
		return LidView::genericDefaultForm('Melder');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld melder
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melder representeert.
	 */
	public static function opmerkingMelder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(GoedIdeeBericht $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(GoedIdeeBericht $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(GoedIdeeBericht $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(GoedIdeeBericht $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(GoedIdeeBericht::enumsStatus() as $id)
			$soorten[$id] = GoedIdeeBerichtView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(GoedIdeeBericht $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(GoedIdeeBericht $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', GoedIdeeBericht::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bericht.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld bericht labelt.
	 */
	public static function labelBericht(GoedIdeeBericht $obj)
	{
		return 'Bericht';
	}
	/**
	 * @brief Geef de waarde van het veld bericht.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bericht van het object obj
	 * representeert.
	 */
	public static function waardeBericht(GoedIdeeBericht $obj)
	{
		return static::defaultWaardeText($obj, 'Bericht');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bericht.
	 *
	 * @see genericFormbericht
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bericht staat en kan
	 * worden bewerkt. Indien bericht read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBericht(GoedIdeeBericht $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Bericht', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bericht. In
	 * tegenstelling tot formbericht moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbericht
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bericht staat en kan
	 * worden bewerkt. Indien bericht read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBericht($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Bericht', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bericht
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bericht representeert.
	 */
	public static function opmerkingBericht()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld moment.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld moment labelt.
	 */
	public static function labelMoment(GoedIdeeBericht $obj)
	{
		return 'Moment';
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @param GoedIdeeBericht $obj Het GoedIdeeBericht-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld moment van het object obj
	 * representeert.
	 */
	public static function waardeMoment(GoedIdeeBericht $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Moment');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld moment.
	 *
	 * @see genericFormmoment
	 *
	 * @param GoedIdeeBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is betreft het een statisch html-element.
	 */
	public static function formMoment(GoedIdeeBericht $obj, $include_id = false)
	{
		return static::waardeMoment($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld moment. In
	 * tegenstelling tot formmoment moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmoment
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMoment($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld moment
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld moment representeert.
	 */
	public static function opmerkingMoment()
	{
		return NULL;
	}
}
