<?
abstract class GoedIdeeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeView.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdee(GoedIdee $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld goedideeID.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld goedideeID labelt.
	 */
	public static function labelGoedideeID(GoedIdee $obj)
	{
		return 'GoedideeID';
	}
	/**
	 * @brief Geef de waarde van het veld goedideeID.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld goedideeID van het object obj
	 * representeert.
	 */
	public static function waardeGoedideeID(GoedIdee $obj)
	{
		return static::defaultWaardeInt($obj, 'GoedideeID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * goedideeID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld goedideeID representeert.
	 */
	public static function opmerkingGoedideeID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melder.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld melder labelt.
	 */
	public static function labelMelder(GoedIdee $obj)
	{
		return 'Melder';
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melder van het object obj
	 * representeert.
	 */
	public static function waardeMelder(GoedIdee $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMelder())
			return NULL;
		return LidView::defaultWaardeLid($obj->getMelder());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melder.
	 *
	 * @see genericFormmelder
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is betreft het een statisch html-element.
	 */
	public static function formMelder(GoedIdee $obj, $include_id = false)
	{
		return LidView::defaultForm($obj->getMelder());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melder. In
	 * tegenstelling tot formmelder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmelder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMelder($name, $waarde=NULL)
	{
		return LidView::genericDefaultForm('Melder');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld melder
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melder representeert.
	 */
	public static function opmerkingMelder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(GoedIdee $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(GoedIdee $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(GoedIdee $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(GoedIdee $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(GoedIdee $obj)
	{
		return static::defaultWaardeString($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(GoedIdee $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(GoedIdee $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(GoedIdee::enumsStatus() as $id)
			$soorten[$id] = GoedIdeeView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(GoedIdee $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(GoedIdee $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', GoedIdee::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld publiek.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld publiek labelt.
	 */
	public static function labelPubliek(GoedIdee $obj)
	{
		return 'Publiek';
	}
	/**
	 * @brief Geef de waarde van het veld publiek.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld publiek van het object obj
	 * representeert.
	 */
	public static function waardePubliek(GoedIdee $obj)
	{
		return static::defaultWaardeBool($obj, 'Publiek');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld publiek.
	 *
	 * @see genericFormpubliek
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld publiek staat en kan
	 * worden bewerkt. Indien publiek read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPubliek(GoedIdee $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Publiek', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld publiek. In
	 * tegenstelling tot formpubliek moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpubliek
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld publiek staat en kan
	 * worden bewerkt. Indien publiek read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPubliek($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Publiek');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld publiek
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld publiek representeert.
	 */
	public static function opmerkingPubliek()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld moment.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld moment labelt.
	 */
	public static function labelMoment(GoedIdee $obj)
	{
		return 'Moment';
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @param GoedIdee $obj Het GoedIdee-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld moment van het object obj
	 * representeert.
	 */
	public static function waardeMoment(GoedIdee $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Moment');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld moment.
	 *
	 * @see genericFormmoment
	 *
	 * @param GoedIdee $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is betreft het een statisch html-element.
	 */
	public static function formMoment(GoedIdee $obj, $include_id = false)
	{
		return static::waardeMoment($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld moment. In
	 * tegenstelling tot formmoment moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmoment
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMoment($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld moment
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld moment representeert.
	 */
	public static function opmerkingMoment()
	{
		return NULL;
	}
}
