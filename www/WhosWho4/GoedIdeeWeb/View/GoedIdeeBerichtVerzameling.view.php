<?
abstract class GoedIdeeBerichtVerzamelingView
	extends GoedIdeeBerichtVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeBerichtVerzamelingView.
	 *
	 * @param GoedIdeeBerichtVerzameling $obj Het GoedIdeeBerichtVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdeeBerichtVerzameling(GoedIdeeBerichtVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * Maak een visuele representatie van een GoedIdeeBerichtVerzameling.
	 *
	 * @param berichten Een GoedIdeeBerichtVerzameling.
	 * @return HtmlDiv Een door HtmlDiv die het bericht in elk GoedIdeeBericht bevat, elk
	 * door een HtmlHR gescheiden.
	 * @see GoedIdeeBerichtView::toon
	 */
	public static function toonBerichten (GoedIdeeBerichtVerzameling $berichten, bool $omgekeerd)
	{
		if ($berichten->aantal() == 0)
		{
			return NULL;
		}

		$idee = $berichten->first()->getGoedIdee();

		$berichtTeksten = array();

		$vorige = $berichten->first()->vorige();

		foreach ($berichten as $bericht)
		{
			$berichtTeksten[] = GoedIdeeBerichtView::toon($bericht, $vorige);
			$vorige = $bericht;
		}


		$div = new HtmlDiv();
		foreach ($berichtTeksten as $berichtTekst)
		{
			$div->add(new HtmlHR());
			$div->add($berichtTekst);
		}

		if ($omgekeerd)
		{
			$berichtTeksten = array_reverse($berichtTeksten);
		}

		return $div;
	}

}
