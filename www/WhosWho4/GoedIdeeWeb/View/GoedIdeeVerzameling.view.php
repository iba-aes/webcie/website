<?
abstract class GoedIdeeVerzamelingView
	extends GoedIdeeVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GoedIdeeVerzamelingView.
	 *
	 * @param GoedIdeeVerzameling $obj Het GoedIdeeVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGoedIdeeVerzameling(GoedIdeeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	private static function addTab(&$navtabs, &$tabpanes, $id, $title, $body, $count = NULL, $isActive = false)
	{
		if ($count !== NULL) {
			$title = array($title, new HtmlSpan($count, 'badge'));
		}
		$a = new HtmlAnchor("#tab-$id", $title);
		$a->setAttributes(array(
			'role' => 'tab',
			'data-toggle' => 'tab'
		));
		$li = new HtmlListItem($a, $isActive ? 'active' : NULL, NULL, false);
		$li->setAttribute('role', 'presentation');
		$navtabs->add($li);

		$div = new HtmlDiv($body, 'tab-pane' . ($isActive ? ' active' : ''), "tab-$id");
		$div->setAttribute('role', 'tabpanel');
		$tabpanes->add($div);
	}

	/**
	 *  Maak een persoonlijke GoedIdeeweb-homepage.
	 *
	 * @param persoon De persoon voor wie pagina persoonlijk is.
	 * @return HtmlDiv Een HtmlDiv met GoedIdee-tabellen: een met laatst gemelde goede ideeen, een met laatst 
	 * gewijzigde ideeen en een met door persoon gemelde goede ideeen.
	 */
	static public function toonInfoPagina (Persoon $persoon = NULL)
	{
		$div = new HtmlDiv(array(
			$navtabs = new HtmlList(false, null, 'nav nav-tabs'),
			$tabpanes = new HtmlDiv(null, 'tab-content')
		));

		$navtabs->setAttribute('role', 'tablist');

		$closed = tryPar('closed', false);
		
		$laatstGemeldeIdeeen = GoedIdeeVerzameling::laatsteGemeld(GOEDIDEEWEB_AANTAL_GEMELD, !$closed);
		$isMeer = $laatstGemeldeIdeeen->aantal() == GOEDIDEEWEB_AANTAL_GEMELD;
		self::addTab($navtabs, $tabpanes, 'laatstgemeld', 'Laatst gemeld',
				self::tabel($laatstGemeldeIdeeen, "LaatstGemeld"),
				$laatstGemeldeIdeeen->aantal() . ($isMeer ? '+' : ''),true);

		$laatstGewijzigdeIdeeen = GoedIdeeVerzameling::laatsteGewijzigd(GOEDIDEEWEB_AANTAL_GEMELD, !$closed);
		$isMeer = $laatstGewijzigdeIdeeen->aantal() == GOEDIDEEWEB_AANTAL_GEMELD;
		self::addTab($navtabs, $tabpanes, 'laatstgewijzigd', 'Laatst gewijzigd',
				self::tabel($laatstGewijzigdeIdeeen, "LaatstGewijzigd"),
				$laatstGewijzigdeIdeeen->aantal() . ($isMeer ? '+' : ''), false);

		if ($persoon) {
			$gemeldeIdeeen = GoedIdeeVerzameling::geefGemeld($persoon, !$closed);
			if ($gemeldeIdeeen->aantal() > 0)
				self::addTab($navtabs, $tabpanes, 'gemeld', _('Door mij gemeld'),
						self::tabel($gemeldeIdeeen), $gemeldeIdeeen->aantal(), false);
        	}

		$actions = new HtmlDiv(array(
			new HtmlDiv(array(
				HtmlAnchor::button(GOEDIDEEWEBBASE . '/Melden', array(
					HtmlSpan::fa('plus', 'nieuw'),
					'&nbsp;' . _("Goed Idee melden")
				))
			), 'btn-group'),
			$zoekform = new HtmlForm('GET', GOEDIDEEWEBBASE . '/Zoeken'),
			$closedform = new HtmlForm('GET')
		), 'form-inline');

		$zoekform->addClass('input-group');
		$zoekform->add(new HtmlDiv(array(
			HtmlInput::makeText('zoekterm', null, null, 'form-control', null, _('Snel zoeken...')),
			new HtmlSpan(new HtmlButton('submit', HtmlSpan::fa('search', _('Snel zoeken...'))), 'input-group-btn')
		)));
		$closedform->addClass('btn-group');
		$closedform->setAttribute('style', 'margin-left: 1em;');
		$closedform->add(array(
			new HtmlButton('submit', 'Alleen open goede ideeën', 'closed', '', 'btn btn-default' . ($closed ? '' : ' active')),
			new HtmlButton('submit', 'Alle ideeën', 'closed', '1', 'btn btn-default' . ($closed ? ' active' : ''))
		));

		return array($actions, new HtmlHR(), $div);
	}

	/**
	 * Maak een formulier waarmee Goede Ideeën gezocht kunnen worden.
	 *
	 * @param zoekterm De ingevulde zoekterm.
	 * @param status De geselecteerde status.
	 * @return HtmlForm Een HtmlForm waarmee op goede ideeën gezocht kan worden en waar de 
	 * waarden van de parameters in ingevuld staan.
	 */
	public static function zoekForm ($zoekterm = '', $status = NULL)
	{
		$form = new HtmlForm('GET');

		$form->add(new HtmlDiv(array(
			new HtmlLabel('zoekterm', _('Zoekterm:'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText('zoekterm', $zoekterm, null, 'form-control', null, 'Zoek op titel of inhoud...'), 'col-sm-10')
		), 'form-group'));

		$form->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeSubmitButton(_('Zoeken')), 'col-sm-12 text-center'), 'form-group'));
		return $form;
	}

	/**
	 * Maak een resizable tabel van een GoedIdeeVerzameling.
	 *
	 * @param ideeen De GoedIdeeVerzameling waarvan een tabel gemaakt wordt.
	 * @return HtmlResizableDiv Een HtmlResizableDiv met HtmlTable met en rij voor elk Goed Idee in 
	 * ideeen, of een gewone HtmlDiv indien ideeen minder dan 6 elementen bevat.
	 */
	static public function resizableTabel (GoedIdeeVerzameling $ideeen, $gesorteerdOp = "Status")
	{
		$div = $ideeen->aantal() > 6 ? new HtmlResizableDiv("100%", "180px") : new HtmlDiv();
		$div->add(self::tabel($ideeen, false, $gesorteerdOp));
		return $div;
	}

	/**
	 * Maak een tabel van een GoedIdeeVerzameling.
	 *
	 * @param ideeen De GoedIdeeVerzameling waarvan een tabel gemaakt wordt.
	 * @param gesorteerdOp De kolom die verteld op welke kolom je moet sorteren.
	 * @return HtmlTable Een HtmlTable met en rij voor elk GoedIdee in ideeen.
	 */
	static public function tabel (GoedIdeeVerzameling $ideeen, $gesorteerdOp = "LaatstGemeld")
	{
		if ($ideeen->aantal() == 0)
		{
			return new HtmlEmphasis(_('Er zijn geen goede ideeën gevonden.'));
		}
		$ideeen->sorteer($gesorteerdOp);

		$table = new HtmlTable(NULL, 'goedidee sortable table-non-striped');
		$thead = $table->addHead();
		$row = $thead->addRow();
		$row->addData(GoedIdeeView::labelGoedIdeeID(), 'goedIdeeID');
		$row->addData('Gewijzigd');
		$row->addData(GoedIdeeView::labelStatus(), 'status');
		$row->addData(GoedIdeeView::labelTitel(), 'titel');
		$row->addData(GoedIdeeView::labelPubliek(), 'publiek');

		$tbody = $table->addBody();
		foreach ($ideeen as $idee)
		{
			$tbody->addImmutable(GoedIdeeView::toTableRow($idee, true));
		}

		return $table;
	}
}
