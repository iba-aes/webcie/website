<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
class GoedIdee
	extends GoedIdee_Generated
{

	/** Verzamelingen **/
	protected $goedIdeeBerichtVerzameling;
	/**
	 * @brief De constructor van de GoedIdee-klasse.
	 *
	 * @param DateTimeLocale|null moment Het moment waarop het idee is aangemaakt.
	 * @param Lid melder Degene die de bug heeft gemeld.
	 * @param string titel De titel van het idee.
	 */
	public function __construct($moment = NULL, $melder = NULL, $titel = '', $omschrijving = '', $publiek = false)
	{
		parent::__construct($moment); // GoedIdee_Generated

		$this->setMelder($melder);
		$this->setTitel($titel);
		$this->setOmschrijving($omschrijving);
	}

	/**
	 * @brief Returneert de GoedIdeeBerichtVerzameling die hoort bij dit object.
	 * @return GoedIdeeBerichtVerzameling een GoedIdeeBerichtVerzameling horende bij het object.
	 */
	public function getGoedIdeeBerichtVerzameling()
	{
		if(!$this->goedIdeeBerichtVerzameling instanceof GoedIdeeBerichtVerzameling)
		{
			$this->goedIdeeBerichtVerzameling = new GoedIdeeBerichtVerzameling();
		}
		return $this->goedIdeeBerichtVerzameling;
	}


	/**
	 * Bepaal of de huidig ingelogde gebruiker dit Goed Idee mag bekijken.
	 *
	 * @return bool Of dit Goed Idee bekeken kan worden.
	 */
	public function magBekijken ()
	{
		$isMelder = $this->getMelder() === Persoon::getIngelogd();
		$publiek = $this->getPubliek();

		return $isMelder || hasAuth('bestuur') || $publiek || hasAuth('experimencie');
	}


	/**
	 * Bepaal of de huidig ingelogde gebruiker dit Goed Idee mag wijzigen.
	 *
	 * @return bool Of dit Goed Idee gewijzigd kan worden.
	 */
	public function magWijzigen ()
	{
		return hasAuth('bestuur') || hasAuth('experimencie');
	}

	/**
	 *  Bepaal of de huidige ingelogde gebruiker op het Goed Idee mag reageren.
	 *
	 * @return bool True indien er gereageerd mag worden, false anders.
	 */
	public function magReageren ()
	{
		return $this->magBekijken();
	}

	/**
	 *  Bepaal de url van dit Goed Idee.
	 *
	 * @return string een string die de locale url naar dit Goed Idee representeert.
	 */
	public function url ()
	{
		return GOEDIDEEWEBBASE . '/' . $this->geefID();
	}

	/**
	 *  Zoek de oorspronkelijke tekst bij het ingevoerde idee.
	 *
	 * @return GoedIdeeBericht het eerste GoedIdeeBericht van dit Goed Idee.
	 */
	public function getIdeeBericht()
	{
		return GoedIdeeBericht::geefEersteVanGoedIdee($this);
	}

	/**
	 *  Zoek alle berichten die bij dit GoedIdee horen.
	 *
	 * @return GoedIdeeBerichtVerzameling een GoedIdeeBerichtVerzameling met alle GoedIdeeBerichten van dit GoedIdee.
	 */
	public function getBerichten ()
	{
		return GoedIdeeBerichtVerzameling::geefBerichten($this);
	}

	/**
	 *  Zoek alle reacties die bij dit GoedIdee horen.
	 *
	 * @return GoedIdeeBerichtVerzameling Een GoedIdeeBerichtVerzameling met alle GoedIdeeBerichten van dit GoedIdee
	 * behalve de eerste.
	 */
	public function getReacties ()
	{
		return GoedIdeeBerichtVerzameling::geefReacties($this);
	}

	/**
	 * Voeg een GoedIdeeBericht toe aan dit GoedIdee.
	 *
	 * Deze methode handelt de caching van de huidige titel en status van dit GoedIdee af.
	 *
	 * @param ideeBericht Een GoedIdeeBericht.
	 */
	public function voegGoedIdeeBerichtToe (GoedIdeeBericht $ideeBericht)
	{
		$titel = $ideeBericht->getTitel();
		$status = $ideeBericht->getStatus();

		// Update de cache-info van het GoedIdee
		$this->setTitel($titel);
		if (in_array($status, GoedIdee::enumsStatus()))
		{
			$this->setStatus($status);
		}

		GoedIdeeBerichtView::verstuurMails($ideeBericht, False);
	}

	/**
	 * Maak een link naar een GoedIdee.
	 *
	 * @param idee Een GoedIdee.
	 * @param titel (Optioneel) de tekst waar je op kan klikken. Per default het GoedIdeenummer.
	 * @return HtmlAnchor Een HtmlAnchor naar de url van GoedIdee met gegeven titel.
	 */
	public static function makeLink (GoedIdee $idee, $titel = NULL)
	{
		if (!$titel) {
			$titel = self::waardeGoedIdeeID($idee);
		}
		return new HtmlAnchor(HTTPS_ROOT . $idee->url(), $titel);
	}
	
	/**
	 * Geef de statussen die in een bepaalde statusset zitten.
	 *
	 * @param setnaam De naam van een verzameling van statussen.
	 * @return array Een array die met de bijbehorende statussen, die leeg is als setnaam nergens bij hoort.
	 */
		
	public static function geefStatusSet ($setnaam)
	{
		global $goedIdeeStatusSets;

		if (in_array($setnaam, self::enumsStatus()))
			return array($setnaam);

		if (array_key_exists($setnaam, $goedIdeeStatusSets))
			return $goedIdeeStatusSets[$setnaam];

		return array();
	}

}
