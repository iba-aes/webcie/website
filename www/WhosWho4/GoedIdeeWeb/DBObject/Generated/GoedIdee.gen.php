<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class GoedIdee_Generated
	extends Entiteit
{
	protected $goedideeID;				/**< \brief PRIMARY */
	protected $melder;
	protected $melder_contactID;		/**< \brief PRIMARY */
	protected $titel;
	protected $omschrijving;
	protected $status;					/**< \brief ENUM:GEMELD/AFGEWEZEN/GEKEURD/BEZIG/UITGEVOERD/VERJAARD */
	protected $publiek;
	protected $moment;
	/** Verzamelingen **/
	protected $goedIdeeBerichtVerzameling;
	/**
	/**
	 * @brief De constructor van de GoedIdee_Generated-klasse.
	 *
	 * @param mixed $a Moment (datetime)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(!$a instanceof DateTimeLocale)
		{
			if(is_null($a))
				$a = new DateTimeLocale();
			else
			{
				try {
					$a = new DateTimeLocale($a);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->moment = $a;

		$this->goedideeID = NULL;
		$this->melder = NULL;
		$this->melder_contactID = 0;
		$this->titel = '';
		$this->omschrijving = '';
		$this->status = 'GEMELD';
		$this->publiek = False;
		$this->goedIdeeBerichtVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Moment';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld goedideeID.
	 *
	 * @return int
	 * De waarde van het veld goedideeID.
	 */
	public function getGoedideeID()
	{
		return $this->goedideeID;
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @return Lid
	 * De waarde van het veld melder.
	 */
	public function getMelder()
	{
		if(!isset($this->melder)
		 && isset($this->melder_contactID)
		 ) {
			$this->melder = Lid::geef
					( $this->melder_contactID
					);
		}
		return $this->melder;
	}
	/**
	 * @brief Stel de waarde van het veld melder in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return GoedIdee
	 * Dit GoedIdee-object.
	 */
	public function setMelder($new_contactID)
	{
		unset($this->errors['Melder']);
		if($new_contactID instanceof Lid
		) {
			if($this->melder == $new_contactID
			&& $this->melder_contactID == $this->melder->getContactID())
				return $this;
			$this->melder = $new_contactID;
			$this->melder_contactID
					= $this->melder->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->melder == NULL 
				&& $this->melder_contactID == (int)$new_contactID)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melder geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melder geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMelder()
	{
		if (array_key_exists('Melder', $this->errors))
			return $this->errors['Melder'];
		$waarde1 = $this->getMelder();
		$waarde2 = $this->getMelderContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld melder_contactID.
	 *
	 * @return int
	 * De waarde van het veld melder_contactID.
	 */
	public function getMelderContactID()
	{
		if (is_null($this->melder_contactID) && isset($this->melder)) {
			$this->melder_contactID = $this->melder->getContactID();
		}
		return $this->melder_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return GoedIdee
	 * Dit GoedIdee-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return GoedIdee
	 * Dit GoedIdee-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('GEMELD','AFGEWEZEN','GEKEURD','BEZIG','UITGEVOERD','VERJAARD');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return GoedIdee
	 * Dit GoedIdee-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld publiek.
	 *
	 * @return bool
	 * De waarde van het veld publiek.
	 */
	public function getPubliek()
	{
		return $this->publiek;
	}
	/**
	 * @brief Stel de waarde van het veld publiek in.
	 *
	 * @param mixed $newPubliek De nieuwe waarde.
	 *
	 * @return GoedIdee
	 * Dit GoedIdee-object.
	 */
	public function setPubliek($newPubliek)
	{
		unset($this->errors['Publiek']);
		if(!is_null($newPubliek))
			$newPubliek = (bool)$newPubliek;
		if($this->publiek === $newPubliek)
			return $this;

		$this->publiek = $newPubliek;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld publiek geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld publiek geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPubliek()
	{
		if (array_key_exists('Publiek', $this->errors))
			return $this->errors['Publiek'];
		$waarde = $this->getPubliek();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld moment.
	 */
	public function getMoment()
	{
		return $this->moment;
	}
	/**
	 * @brief Returneert de GoedIdeeBerichtVerzameling die hoort bij dit object.
	 */
	public function getGoedIdeeBerichtVerzameling()
	{
		if(!$this->goedIdeeBerichtVerzameling instanceof GoedIdeeBerichtVerzameling)
			$this->goedIdeeBerichtVerzameling = GoedIdeeBerichtVerzameling::fromGoedidee($this);
		return $this->goedIdeeBerichtVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return GoedIdee::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van GoedIdee.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return GoedIdee::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getGoedideeID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return GoedIdee|false
	 * Een GoedIdee-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$goedideeID = (int)$a[0];
		}
		else if(isset($a))
		{
			$goedideeID = (int)$a;
		}

		if(is_null($goedideeID))
			throw new BadMethodCallException();

		static::cache(array( array($goedideeID) ));
		return Entiteit::geefCache(array($goedideeID), 'GoedIdee');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'GoedIdee');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('GoedIdee::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `GoedIdee`.`goedideeID`'
		                 .     ', `GoedIdee`.`melder_contactID`'
		                 .     ', `GoedIdee`.`titel`'
		                 .     ', `GoedIdee`.`omschrijving`'
		                 .     ', `GoedIdee`.`status`'
		                 .     ', `GoedIdee`.`publiek`'
		                 .     ', `GoedIdee`.`moment`'
		                 .     ', `GoedIdee`.`gewijzigdWanneer`'
		                 .     ', `GoedIdee`.`gewijzigdWie`'
		                 .' FROM `GoedIdee`'
		                 .' WHERE (`goedideeID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['goedideeID']);

			$obj = new GoedIdee($row['moment']);

			$obj->inDB = True;

			$obj->goedideeID  = (int) $row['goedideeID'];
			$obj->melder_contactID  = (int) $row['melder_contactID'];
			$obj->titel  = trim($row['titel']);
			$obj->omschrijving  = trim($row['omschrijving']);
			$obj->status  = strtoupper(trim($row['status']));
			$obj->publiek  = (bool) $row['publiek'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'GoedIdee')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getMelderContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->goedideeID =
			$WSW4DB->q('RETURNID INSERT INTO `GoedIdee`'
			          . ' (`melder_contactID`, `titel`, `omschrijving`, `status`, `publiek`, `moment`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %i, %s, %s, %i)'
			          , $this->melder_contactID
			          , $this->titel
			          , $this->omschrijving
			          , $this->status
			          , $this->publiek
			          , $this->moment->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'GoedIdee')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `GoedIdee`'
			          .' SET `melder_contactID` = %i'
			          .   ', `titel` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `status` = %s'
			          .   ', `publiek` = %i'
			          .   ', `moment` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `goedideeID` = %i'
			          , $this->melder_contactID
			          , $this->titel
			          , $this->omschrijving
			          , $this->status
			          , $this->publiek
			          , $this->moment->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->goedideeID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenGoedIdee
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenGoedIdee($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenGoedIdee($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Status';
			$velden[] = 'Publiek';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Status';
			$velden[] = 'Publiek';
			$velden[] = 'Moment';
			break;
		case 'get':
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Status';
			$velden[] = 'Publiek';
			$velden[] = 'Moment';
		case 'primary':
			$velden[] = 'melder_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'GoedIdeeBerichtVerzameling';
			break;
		default:
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Status';
			$velden[] = 'Publiek';
			$velden[] = 'Moment';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle GoedIdeeBericht-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$GoedIdeeBerichtFromGoedideeVerz = GoedIdeeBerichtVerzameling::fromGoedidee($this);
		$returnValue = $GoedIdeeBerichtFromGoedideeVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object GoedIdeeBericht met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $GoedIdeeBerichtFromGoedideeVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `GoedIdee`'
		          .' WHERE `goedideeID` = %i'
		          .' LIMIT 1'
		          , $this->goedideeID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				if($singlequery !== 1) {
					$queriesgelukt = false;
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->goedideeID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `GoedIdee`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `goedideeID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->goedideeID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van GoedIdee terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'goedideeid':
			return 'int';
		case 'melder':
			return 'foreign';
		case 'melder_contactid':
			return 'int';
		case 'titel':
			return 'string';
		case 'omschrijving':
			return 'string';
		case 'status':
			return 'enum';
		case 'publiek':
			return 'bool';
		case 'moment':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'goedideeID':
		case 'melder_contactID':
		case 'publiek':
			$type = '%i';
			break;
		case 'titel':
		case 'omschrijving':
		case 'status':
		case 'moment':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `GoedIdee`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `goedideeID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->goedideeID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `GoedIdee`'
		          .' WHERE `goedideeID` = %i'
		                 , $veld
		          , $this->goedideeID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'GoedIdee');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle GoedIdeeBericht-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$GoedIdeeBerichtFromGoedideeVerz = GoedIdeeBerichtVerzameling::fromGoedidee($this);
		$dependencies['GoedIdeeBericht'] = $GoedIdeeBerichtFromGoedideeVerz;

		return $dependencies;
	}
}
