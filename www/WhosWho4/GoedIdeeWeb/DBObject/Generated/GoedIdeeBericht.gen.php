<?
abstract class GoedIdeeBericht_Generated
	extends Entiteit
{
	protected $goedideeBerichtID;		/**< \brief PRIMARY */
	protected $goedidee;
	protected $goedidee_goedideeID;		/**< \brief PRIMARY */
	protected $melder;
	protected $melder_contactID;		/**< \brief PRIMARY */
	protected $titel;					/**< \brief NULL */
	protected $status;					/**< \brief ENUM:GEMELD/AFGEWEZEN/GEKEURD/BEZIG/UITGEVOERD/VERJAARD */
	protected $bericht;					/**< \brief NULL */
	protected $moment;
	/**
	/**
	 * @brief De constructor van de GoedIdeeBericht_Generated-klasse.
	 *
	 * @param mixed $a Goedidee (GoedIdee OR Array(goedIdee_goedideeID) OR
	 * goedIdee_goedideeID)
	 * @param mixed $b Moment (datetime)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof GoedIdee)
		{
			$this->goedidee = $a;
			$this->goedidee_goedideeID = $a->getGoedideeID();
		}
		else if(is_array($a))
		{
			$this->goedidee = NULL;
			$this->goedidee_goedideeID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->goedidee = NULL;
			$this->goedidee_goedideeID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(!$b instanceof DateTimeLocale)
		{
			if(is_null($b))
				$b = new DateTimeLocale();
			else
			{
				try {
					$b = new DateTimeLocale($b);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->moment = $b;

		$this->goedideeBerichtID = NULL;
		$this->melder = NULL;
		$this->melder_contactID = 0;
		$this->titel = NULL;
		$this->status = 'GEMELD';
		$this->bericht = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Goedidee';
		$volgorde[] = 'Moment';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld goedideeBerichtID.
	 *
	 * @return int
	 * De waarde van het veld goedideeBerichtID.
	 */
	public function getGoedideeBerichtID()
	{
		return $this->goedideeBerichtID;
	}
	/**
	 * @brief Geef de waarde van het veld goedidee.
	 *
	 * @return GoedIdee
	 * De waarde van het veld goedidee.
	 */
	public function getGoedidee()
	{
		if(!isset($this->goedidee)
		 && isset($this->goedidee_goedideeID)
		 ) {
			$this->goedidee = GoedIdee::geef
					( $this->goedidee_goedideeID
					);
		}
		return $this->goedidee;
	}
	/**
	 * @brief Geef de waarde van het veld goedidee_goedideeID.
	 *
	 * @return int
	 * De waarde van het veld goedidee_goedideeID.
	 */
	public function getGoedideeGoedideeID()
	{
		if (is_null($this->goedidee_goedideeID) && isset($this->goedidee)) {
			$this->goedidee_goedideeID = $this->goedidee->getGoedideeID();
		}
		return $this->goedidee_goedideeID;
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @return Lid
	 * De waarde van het veld melder.
	 */
	public function getMelder()
	{
		if(!isset($this->melder)
		 && isset($this->melder_contactID)
		 ) {
			$this->melder = Lid::geef
					( $this->melder_contactID
					);
		}
		return $this->melder;
	}
	/**
	 * @brief Stel de waarde van het veld melder in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return GoedIdeeBericht
	 * Dit GoedIdeeBericht-object.
	 */
	public function setMelder($new_contactID)
	{
		unset($this->errors['Melder']);
		if($new_contactID instanceof Lid
		) {
			if($this->melder == $new_contactID
			&& $this->melder_contactID == $this->melder->getContactID())
				return $this;
			$this->melder = $new_contactID;
			$this->melder_contactID
					= $this->melder->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->melder == NULL 
				&& $this->melder_contactID == (int)$new_contactID)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melder geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melder geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMelder()
	{
		if (array_key_exists('Melder', $this->errors))
			return $this->errors['Melder'];
		$waarde1 = $this->getMelder();
		$waarde2 = $this->getMelderContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld melder_contactID.
	 *
	 * @return int
	 * De waarde van het veld melder_contactID.
	 */
	public function getMelderContactID()
	{
		if (is_null($this->melder_contactID) && isset($this->melder)) {
			$this->melder_contactID = $this->melder->getContactID();
		}
		return $this->melder_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return GoedIdeeBericht
	 * Dit GoedIdeeBericht-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('GEMELD','AFGEWEZEN','GEKEURD','BEZIG','UITGEVOERD','VERJAARD');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return GoedIdeeBericht
	 * Dit GoedIdeeBericht-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bericht.
	 *
	 * @return string
	 * De waarde van het veld bericht.
	 */
	public function getBericht()
	{
		return $this->bericht;
	}
	/**
	 * @brief Stel de waarde van het veld bericht in.
	 *
	 * @param mixed $newBericht De nieuwe waarde.
	 *
	 * @return GoedIdeeBericht
	 * Dit GoedIdeeBericht-object.
	 */
	public function setBericht($newBericht)
	{
		unset($this->errors['Bericht']);
		if(!is_null($newBericht))
			$newBericht = trim($newBericht);
		if($newBericht === "")
			$newBericht = NULL;
		if($this->bericht === $newBericht)
			return $this;

		$this->bericht = $newBericht;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bericht geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bericht geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBericht()
	{
		if (array_key_exists('Bericht', $this->errors))
			return $this->errors['Bericht'];
		$waarde = $this->getBericht();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld moment.
	 */
	public function getMoment()
	{
		return $this->moment;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return GoedIdeeBericht::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van GoedIdeeBericht.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return GoedIdeeBericht::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getGoedideeBerichtID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return GoedIdeeBericht|false
	 * Een GoedIdeeBericht-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$goedideeBerichtID = (int)$a[0];
		}
		else if(isset($a))
		{
			$goedideeBerichtID = (int)$a;
		}

		if(is_null($goedideeBerichtID))
			throw new BadMethodCallException();

		static::cache(array( array($goedideeBerichtID) ));
		return Entiteit::geefCache(array($goedideeBerichtID), 'GoedIdeeBericht');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'GoedIdeeBericht');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('GoedIdeeBericht::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `GoedIdeeBericht`.`goedideeBerichtID`'
		                 .     ', `GoedIdeeBericht`.`goedidee_goedideeID`'
		                 .     ', `GoedIdeeBericht`.`melder_contactID`'
		                 .     ', `GoedIdeeBericht`.`titel`'
		                 .     ', `GoedIdeeBericht`.`status`'
		                 .     ', `GoedIdeeBericht`.`bericht`'
		                 .     ', `GoedIdeeBericht`.`moment`'
		                 .     ', `GoedIdeeBericht`.`gewijzigdWanneer`'
		                 .     ', `GoedIdeeBericht`.`gewijzigdWie`'
		                 .' FROM `GoedIdeeBericht`'
		                 .' WHERE (`goedideeBerichtID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['goedideeBerichtID']);

			$obj = new GoedIdeeBericht(array($row['goedidee_goedideeID']), $row['moment']);

			$obj->inDB = True;

			$obj->goedideeBerichtID  = (int) $row['goedideeBerichtID'];
			$obj->melder_contactID  = (int) $row['melder_contactID'];
			$obj->titel  = (is_null($row['titel'])) ? null : trim($row['titel']);
			$obj->status  = strtoupper(trim($row['status']));
			$obj->bericht  = (is_null($row['bericht'])) ? null : trim($row['bericht']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'GoedIdeeBericht')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getGoedideeGoedideeID();
		$this->getMelderContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->goedideeBerichtID =
			$WSW4DB->q('RETURNID INSERT INTO `GoedIdeeBericht`'
			          . ' (`goedidee_goedideeID`, `melder_contactID`, `titel`, `status`, `bericht`, `moment`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %s, %s, %s, %i)'
			          , $this->goedidee_goedideeID
			          , $this->melder_contactID
			          , $this->titel
			          , $this->status
			          , $this->bericht
			          , $this->moment->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'GoedIdeeBericht')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `GoedIdeeBericht`'
			          .' SET `goedidee_goedideeID` = %i'
			          .   ', `melder_contactID` = %i'
			          .   ', `titel` = %s'
			          .   ', `status` = %s'
			          .   ', `bericht` = %s'
			          .   ', `moment` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `goedideeBerichtID` = %i'
			          , $this->goedidee_goedideeID
			          , $this->melder_contactID
			          , $this->titel
			          , $this->status
			          , $this->bericht
			          , $this->moment->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->goedideeBerichtID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenGoedIdeeBericht
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenGoedIdeeBericht($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenGoedIdeeBericht($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Status';
			$velden[] = 'Bericht';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Melder';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Goedidee';
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Status';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
			break;
		case 'get':
			$velden[] = 'Goedidee';
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Status';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
		case 'primary':
			$velden[] = 'goedidee_goedideeID';
			$velden[] = 'melder_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Goedidee';
			$velden[] = 'Melder';
			$velden[] = 'Titel';
			$velden[] = 'Status';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `GoedIdeeBericht`'
		          .' WHERE `goedideeBerichtID` = %i'
		          .' LIMIT 1'
		          , $this->goedideeBerichtID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				if($singlequery !== 1) {
					$queriesgelukt = false;
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->goedideeBerichtID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `GoedIdeeBericht`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `goedideeBerichtID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->goedideeBerichtID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van GoedIdeeBericht terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'goedideeberichtid':
			return 'int';
		case 'goedidee':
			return 'foreign';
		case 'goedidee_goedideeid':
			return 'int';
		case 'melder':
			return 'foreign';
		case 'melder_contactid':
			return 'int';
		case 'titel':
			return 'string';
		case 'status':
			return 'enum';
		case 'bericht':
			return 'text';
		case 'moment':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'goedideeBerichtID':
		case 'goedidee_goedideeID':
		case 'melder_contactID':
			$type = '%i';
			break;
		case 'titel':
		case 'status':
		case 'bericht':
		case 'moment':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `GoedIdeeBericht`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `goedideeBerichtID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->goedideeBerichtID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `GoedIdeeBericht`'
		          .' WHERE `goedideeBerichtID` = %i'
		                 , $veld
		          , $this->goedideeBerichtID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'GoedIdeeBericht');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
