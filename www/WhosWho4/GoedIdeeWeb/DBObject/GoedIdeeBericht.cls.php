<?
class GoedIdeeBericht
	extends GoedIdeeBericht_Generated
{
	/**
	/**
	 * @brief De constructor van de GoedIdeeBericht_Generated-klasse.
	 *
	 * @param mixed $a Goedidee (GoedIdee OR Array(goedIdee_goedideeID) OR
	 * goedIdee_goedideeID)
	 * @param mixed $b Moment (datetime)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct($a, $b); // GoedIdeeBericht_Generated

		$this->goedideeBerichtID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Goedidee';
		$volgorde[] = 'Moment';
		return $volgorde;
	}

	/**
	 *  Zoek chronologisch het eerste GoedIdeeBericht bij een GoedIdee.
	 *
	 * @param idee een GoedIdee.
	 * @return GoedIdeeBericht het GoedIdeeBericht dat bij idee hoort met het kleinste moment.
	 */
	public static function geefEersteVanGoedIdee (GoedIdee $idee)
	{
		return GoedIdeeBerichtQuery::table()
			->whereProp('GoedIdee', $idee)
			->orderByAsc('moment')
			->limit(1)
			->geef();
	}

	/**
	 *  Zoek chronologisch het laatste GoedIdeeBericht bij een GoedIdee.
	 *
	 * @param idee een GoedIdee.
	 * @return GoedIdeeBericht het GoedIdeeBericht dat bij idee hoort met het grootste moment.
	 */
	public static function geefLaatsteVanGoedIdee (GoedIdee $idee)
	{
		return GoedIdeeBerichtQuery::table()
			->whereProp('GoedIdee', $idee)
			->orderByDesc('moment')
			->limit(1)
			->geef();
	}

	/**
	 *  Zoek het vorige GoedIdeeBericht in dit GoedIdee.
	 *
	 * @return GoedIdeeBericht het GoedIdeeBericht dat bij hetzelfde GoedIdee hoort als dit GoedIdeeBericht met 
	 * het grootste moment kleiner dan het moment van dit GoedIdeeBericht, of NULL 
	 * als deze niet bestaat.
	 */
	public function vorige ()
	{
		return GoedIdeeBerichtQuery::table()
			->whereProp('GoedIdee', $this->getGoedIdee())
			->whereProp('moment', '<', $this->getMoment())
			->orderByDesc('moment')
			->limit(1)
			->geef();
	}

	/**
	 * Maak het eerste bericht dat hoort bij een GoedIdee
	 * 
	 * @param idee Het GoedIdee waar dit bericht bij hoort
	 * @param moment Het moment van het melden van het GoedIdee
	 * @return GoedIdeeBericht Het eerste bericht dat hierbij hoort
	 */
	public static function eersteBericht (GoedIdee $idee, DateTimeLocale $moment)
	{
		$ideeBericht = new GoedIdeeBericht($idee, $moment);
		$ideeBericht->setMelder($idee->getMelder());
		$ideeBericht->setTitel($idee->getTitel());
		$ideeBericht->setBericht($idee->getOmschrijving());
		return $ideeBericht;
	}

	/**
	 * Bereken de veranderingen tussen twee instanties van GoedIdeeBericht.
	 *
	 * @param oud Het eerste GoedIdeeBericht, mits deze bestaat, anders is deze null.
	 * @param nieuw Het tweede GoedIdeeBericht.
	 * @return array Een associatieve array die als keys de strings 'titel' en status' heeft en als waarden NULL indien 
	 * de desbetreffende velden gelijk zijn voor de twee objecten, en een array
	 * met de ge-escapede waarden van het veld van oud respectievelijk nieuw.
	 */
	public static function veranderingen ($oud, $nieuw)
	{
		$diff = array('titel' => NULL, 'status' => NULL);

		if ($oud === null) {
			$diff['titel'] = array(null, GoedIdeeBerichtView::waardeTitel($nieuw));
			$diff['status'] = array(null, GoedIdeeBerichtView::waardeStatus($nieuw));
			return $diff;
		}

		if ($oud->getTitel() != $nieuw->getTitel()) {
			$diff['titel'] = array(GoedIdeeBerichtView::waardeTitel($oud), GoedIdeeBerichtView::waardeTitel($nieuw));
		}
		if ($oud->getStatus() != $nieuw->getStatus()) {
			$diff['status'] = array(GoedIdeeBerichtView::waardeStatus($oud), GoedIdeeBerichtView::waardeStatus($nieuw));
		}

		return $diff;
	}


	/**
	 * @brief Controleer of de waarde van het veld bericht geldig is. In het bijzonder moet er een bericht zijn als de titel
	 * en de status niet worden aangepast.
	 * @return bool|string
	 * False als de huidige waarde van het veld bericht geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBericht()
	{
		if (array_key_exists('Bericht', $this->errors))
			return $this->errors['Bericht'];

		$idee = $this->getGoedIdee();
		$laatste = GoedIdeeBericht::geefLaatsteVanGoedIdee($idee);

		$diff = GoedIdeeBericht::veranderingen($laatste, $this);

		if ($diff['titel'] === null && $diff['status'] === null && $this->getBericht() === null) {
			return _('Je moet wel iets veranderen als je iets invoert.');
		}

		return False;
	}
}
