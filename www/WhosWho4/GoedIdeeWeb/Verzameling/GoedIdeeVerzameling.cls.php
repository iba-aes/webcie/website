<?
class GoedIdeeVerzameling
	extends GoedIdeeVerzameling_Generated
{
	/**
	 * @brief De constructor van de GoedIdeeVerzameling-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // GoedIdeeVerzameling_Generated
	}

	public static function filterOpQuery($query) {
		$pers = Persoon::getIngelogd();
		if (!$pers) {
			return $query->whereProp('GoedIdee.publiek', true);
			}
		return $query->where(function($q) use ($pers) {
			$q->whereProp('GoedIdee.publiek', true);
			if (hasAuth('bestuur') || hasAuth('experimencie')) {
				$q->orWhereProp('GoedIdee.publiek', false);
			} else {
				$q->orWhereProp('GoedIdee.melder', $pers);
			}
		});
	}


	/**
	 *  Zoek GoedIdee-objecten die aan bepaalde criteria voldoen.
	 *
	 * In het algemeen geldt dat elke parameter die NULL of leeg is niet wordt meegenomen in de zoekopdracht.
	 *
	 * @param zoekterm Een string waarop gezocht wordt in GoedIdeeBerichten.
	 * @param status Een status of statusset. Indien niet NULL wordt alleen op 
	 * Goede ideeen gezocht die deze status of een status in deze statusset hebben.
	 * @param offset De offset voor welke bugs opgehaald moeten worden uit de db
	 * @param tellen Een bool of er geteld moet worden hoeveel goede ideeen het zijn
	 * ipv dat er een verzameling gereturnd wordt.
	 * @return GoedIdeeVerzameling Een GoedIdeeVerzameling.
	 * @see GoedIdee::geefStatusSet
	 */
	static public function zoeken ($zoekterm, $status = NULL, $offset = null, $tellen = false)
	{
		$statusSet = GoedIdee::geefStatusSet($status);

        	if($zoekterm && is_numeric($zoekterm))
        	    $zoektermid = (int)$zoekterm;
     		else
         	   $zoektermid = null;

		$q = GoedIdeeQuery::table()
			->join('GoedIdeeBericht', 'GoedIdeeBericht.goedidee_goedideeID', 'GoedIdee.goedideeID');
		$q = self::filterOpQuery($q);

		if ($zoekterm) {
			$zoekterm = '%' . str_replace('%', '\%', $zoekterm) . '%';
			$q->where(function ($q2) use ($zoekterm, $zoektermid) {
				$q2->whereLikeProp('GoedIdeeBericht.bericht', $zoekterm)
					->orWhereLikeProp('GoedIdee.titel', $zoekterm)
					->orWhereProp('GoedIdee.goedideeID', $zoektermid);
			});
		}

		if ($statusSet)
			$q->whereInProp('GoedIdee.status', $statusSet);

		if ($tellen) {
			return $q->aggregate('COUNT', 'DISTINCT GoedIdee.goedideeID');
		} 
		else{
			$q->orderByDesc('GoedIdeeBericht.goedidee_goedideeID');
			$q->limit(GOEDIDEEWEB_IDEE_BIJ_ZOEKEN);
			if ($offset) $q->offset($offset);
			return $q->verzamel();
		}
	}

	/**
	 *  Zoek GoedIdeeen die het laatst gemeld zijn.
	 *
	 * @param aantal Het maximale aantal GoedeIdeeen.
	 * @param alleenGekeurd Indien True, zoek alleen Goede Ideeen die de status GEKEURD hebben.
	 * @return GoedIdeeVerzameling Een GoedIdeeVerzameling die de laatste aantal gemelde (al dan niet 
	 * goedgekeurde) Goede Ideeen bevat.
	 */
	static public function laatsteGemeld ($aantal = 20, $alleenGekeurd = False)
	{

		$q = GoedIdeeQuery::table()
			->orderByDesc('GoedIdee.moment');
		$q = self::filterOpQuery($q);

		if ($alleenGekeurd)
		{	
			$statusSet=GoedIdee::geefStatusSet('GEKEURDEN');
		}
		else
		{
			$statusSet=GoedIdee::geefStatusSet('NIETKLAAR');
		}
		$q->whereInProp('GoedIdee.status', $statusSet);

		$q->limit($aantal);
		return $q->verzamel();

	}

	/**
	 *  Zoek GoedIdeeen die het laatst gewijzigd zijn.
	 *
	 * @param aantal Het maximale aantal GoedeIdeeen.
	 * @param alleenGekeurd Indien True, zoek alleen Goede Ideeen die de status GEKEURD hebben.
	 * @return GoedIdeeVerzameling Een GoedIdeeVerzameling die de laatste aantal gewijzigde (al dan niet 
	 * goedgekeurde) Goede Ideeen bevat.
	 */
	static public function laatsteGewijzigd ($aantal = 20, $alleenGekeurd = False)
	{
		$q = GoedIdeeQuery::table()
			->join('GoedIdeeBericht', 'GoedIdeeBericht.goedidee_goedideeID', 'GoedIdee.goedideeID')
			->orderByDesc('GoedIdeeBericht.moment');

		$q = self::filterOpQuery($q);

		if ($alleenGekeurd)
		{	
			$statusSet=GoedIdee::geefStatusSet('GEKEURDEN');
		}
		else
		{
			$statusSet=GoedIdee::geefStatusSet('NIETKLAAR');
		}
		$q->whereInProp('GoedIdee.status', $statusSet);

		$q->limit($aantal);
		return $q->verzamel();
	}


	/**
	 *  Zoek alle GoedIdeeen die door een bepaalde Persoon gemeld zijn.
	 *
	 * @param persoon Een Persoon.
	 * @param alleenOpen Indien True, zoek alleen goedgekeurde GoedIdeeen.
	 * @return GoedIdeeVerzameling Een GoedIdeeVerzameling die alle (al dan niet gekeurde) GoedIdeeen bevat die 
	 * door persoon gemeld zijn.
	 */
	static public function geefGemeld (Persoon $persoon, $alleenGekeurd = False)
	{

		$q = GoedIdeeQuery::table()
			->orderByDesc('GoedIdee.moment');
		$q = self::filterOpQuery($q);

		if (!$persoon){
			return new GoedIdeeVerzameling();
		}
		else{
			$q->whereProp('GoedIdee.melder', $persoon);
		}

		if ($alleenGekeurd) {
			$statusSet = GoedIdee::geefStatusSet('GEKEURD');
			$q->whereInProp('GoedIdee.status', $statusSet);
		}
		return $q->verzamel();

	}

	/**
	 *  Vergelijk twee GoedIdeeen, geef -1 als de eerste eerder gemeld is, 0 als gelijk, 1 als later
	 */
	static public function sorteerOpLaatstGemeld($a, $b) {
		$a = GoedIdee::geef($a)->getMoment();
		$b = GoedIdee::geef($b)->getMoment();
		if ($a > $b) return -1;
		if ($a < $b) return 1;
		return 0;
	}
	/**
	 *  Vergelijk twee GoedIDeeen, geef -1 als de eerste eerder gewijzigd is, 0 als gelijk, 1 als later
	 */
	static public function sorteerOpLaatstGewijzigd($a, $b) {
		$a = GoedIdee::geef($a)->getGewijzigdWanneer();
		$b = GoedIdee::geef($b)->getGewijzigdWanneer();
		if ($a > $b) return -1;
		if ($a < $b) return 1;
		return 0;
	}

}
