<?
abstract class GoedIdeeVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de GoedIdeeVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze GoedIdeeVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze GoedIdeeVerzameling.
	 */
	public function toMelderVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMelderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een GoedIdeeVerzameling van Melder.
	 *
	 * @return GoedIdeeVerzameling
	 * Een GoedIdeeVerzameling die elementen bevat die bij de Melder hoort.
	 */
	static public function fromMelder($melder)
	{
		if(!isset($melder))
			return new GoedIdeeVerzameling();

		return GoedIdeeQuery::table()
			->whereProp('Melder', $melder)
			->verzamel();
	}
}
