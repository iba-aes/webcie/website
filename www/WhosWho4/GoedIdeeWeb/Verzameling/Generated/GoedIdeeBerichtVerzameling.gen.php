<?
abstract class GoedIdeeBerichtVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de GoedIdeeBerichtVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze GoedIdeeBerichtVerzameling een GoedIdeeVerzameling.
	 *
	 * @return GoedIdeeVerzameling
	 * Een GoedIdeeVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze GoedIdeeBerichtVerzameling.
	 */
	public function toGoedideeVerzameling()
	{
		if($this->aantal() == 0)
			return new GoedIdeeVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getGoedideeGoedideeID()
			                      );
		}
		$this->positie = $origPositie;
		return GoedIdeeVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze GoedIdeeBerichtVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze GoedIdeeBerichtVerzameling.
	 */
	public function toMelderVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMelderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een GoedIdeeBerichtVerzameling van Goedidee.
	 *
	 * @return GoedIdeeBerichtVerzameling
	 * Een GoedIdeeBerichtVerzameling die elementen bevat die bij de Goedidee hoort.
	 */
	static public function fromGoedidee($goedidee)
	{
		if(!isset($goedidee))
			return new GoedIdeeBerichtVerzameling();

		return GoedIdeeBerichtQuery::table()
			->whereProp('Goedidee', $goedidee)
			->verzamel();
	}
	/**
	 * @brief Maak een GoedIdeeBerichtVerzameling van Melder.
	 *
	 * @return GoedIdeeBerichtVerzameling
	 * Een GoedIdeeBerichtVerzameling die elementen bevat die bij de Melder hoort.
	 */
	static public function fromMelder($melder)
	{
		if(!isset($melder))
			return new GoedIdeeBerichtVerzameling();

		return GoedIdeeBerichtQuery::table()
			->whereProp('Melder', $melder)
			->verzamel();
	}
}
