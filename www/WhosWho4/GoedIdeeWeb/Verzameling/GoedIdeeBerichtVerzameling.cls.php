<?
class GoedIdeeBerichtVerzameling
	extends GoedIdeeBerichtVerzameling_Generated
{
	/**
	 * @brief De constructor van de GoedIdeeBerichtVerzameling-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // GoedIdeeBerichtVerzameling_Generated
	}

	/**
	 *  Zoek alle berichten bij een Goed Idee, behalve de eerste.
	 *
	 * @param goedidee een Goed Idee.
	 * @return GoedIdeeBerichtVerzameling  een GoedIdeeBerichtVerzameling met alle GoedIdeeBerichten die bij bug 
	 * horen behalve chronologisch de eerste.
	 */
	static public function geefReacties (GoedIdee $goedidee)
	{
		return GoedIdeeBerichtQuery::table()
			->whereProp('GoedIdee', $goedidee)
			->orderByAsc('moment')
			->offset(1)
			->limit(100000)
			->verzamel();
	}

	/**
	 *  Zoek alle berichten bij een Goed Idee
	 *
	 * @param goedidee een Goed Idee.
	 * @return GoedIdeeBerichtVerzameling een GoedIdeeBerichtVerzameling met alle GoedIdeeBerichten die bij bug
	 * horen
	 */
	static public function geefBerichten (GoedIdee $goedidee)
	{
		return GoedIdeeBerichtQuery::table()
			->whereProp('GoedIdee', $goedidee)
			->orderByAsc('moment')
			->verzamel();
	}
}
