<?php

require_once('WhosWho4/constants.php');
require_once('WhosWho4/WSW4Hulpfuncties.php');
require_once('WhosWho4/Spookweb2/SW2Hulpfuncties.php');

function wsw4_nongenerated_autoload($class)
{
	switch($class)
	{
	case 'Kart':
		require_once('WhosWho4/Kart.cls.php');
		break;
	case 'KartView':
		require_once('WhosWho4/Kart.view.php');
		break;
	case 'MailParser':
		require_once('WhosWho4/Mailings/MailParser.cls.php');
		break;
	case 'MailMimePart':
		require_once('WhosWho4/Mailings/MailMimePart.cls.php');
		break;
	case 'IOUView':
		require_once('WhosWho4/IOU/IOUView.php');
		break;
	case 'BackgroundView':
		require_once('WhosWho4/View/Background.view.php');
		break;
	case 'PublisherView':
		require_once('WhosWho4/Publisher.view.php');
		break;
	case 'SysteemApi':
		require_once('WhosWho4/SysteemApi.cls.php');
		break;
	case 'HigherOrderVerzameling':
		require_once('WhosWho4/Basis/HigherOrderVerzameling.cls.php');
		break;
	case 'Dili_Controller':
		require_once('WhosWho4/Controllers/Dili.php');
		break;
	case 'DiliView':
		require_once('WhosWho4/View/Dili.view.php');
		break;
	case 'StatsView':
		require_once('WhosWho4/View/Stats.view.php');
		break;
	case 'Twig_LoaderDBObject':
		require_once('WhosWho4/DBObject/Template.cls.php');
		break;
	case 'Controller':
		require_once('WhosWho4/Basis/Controller.cls.php');
		break;
	}
}
spl_autoload_register('wsw4_nongenerated_autoload');
