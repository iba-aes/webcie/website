<?php

setZeurmodus();

/**
 * @brief Controller die hoort bij Template.
 */
class TemplateController
{
	/**
	 * @brief Toon alle geconfigureerde templates.
	 *
	 * @site{/Service/Intern/Template/index.html}
	 */
	public static function overzicht() {
		$page = new HTMLPage();
		$page->start();

		// Nieuwform voor templates voor goden.
		if (hasAuth('god'))
		{
			$template = new Template();
			$form = HtmlForm::named('nieuweTemplate', TEMPLATE_BASE . 'Nieuw');
			$form->add(TemplateView::createForm($template, false, 'set'));
			$form->add(HtmlInput::makeFormSubmitButton(_('Nieuwe template jonguh!')));
			$page->add($form);
		}

		$page->add(TemplateVerzamelingView::templateOverzicht());

		return new PageResponse($page);
	}

	/**
	 * @brief Handelt de variabele entry van een template af.
	 *
	 * @site{/Service/Intern/Template/ * /}
	 */
	static public function templateEntry($args)
	{
		$template = Template::geef($args[0]);
		if (!$template) return false;

		return array('name' => $args[0],
					'displayName' => TemplateView::waardeLabel($template),
					'access' => true);
	}

	/**
	 * @brief Maak een nieuwe template.
	 *
	 * @site{/Service/Intern/Template/Nieuw}
	 */
	static public function templateNieuw()
	{
		requireAuth('god');

		$template = new Template();
		$token = Token::processNamedForm();
		$gesubmit = false;
		if ($token == "nieuweTemplate") {
			$gesubmit = true;
			TemplateView::processForm($template, 'set');
			if ($template->valid()) {
				$template->opslaan();
				return Page::responseRedirectMelding(TEMPLATE_BASE, _("Nieuwe template toegevoegd!"));
			}
		}
		$page = new HTMLPage();
		$page->start();
		$form = HtmlForm::named('nieuweTemplate', TEMPLATE_BASE . 'Nieuw');
		$form->add(TemplateView::createForm($template, $gesubmit, 'set'));
		$form->add(HtmlInput::makeFormSubmitButton(_('Nieuwe template jonguh!')));
		$page->add($form);
		return new PageResponse($page);
	}

	/**
	 * @brief Wijzig de (inhoud van de) template met het variabele stukje ID.
	 *
	 * @site{/Service/Intern/Template/ * /Wijzig}
	 */
	static public function templateWijzig()
	{
		requireAuth('bestuur');

		$vfs = vfsVarEntryNames();

		if (!($template = Template::geef($vfs[0])))
		{
			return responseUitStatusCode(403);
		}

		$token = Token::processNamedForm();
		if ($token == "wijzigTemplate") {
			TemplateView::processForm($template);
			if ($template->valid()) {
				$template->opslaan();
				return Page::responseRedirectMelding(TEMPLATE_BASE, _("Template gewijzigd!"));
			}
		}

		$page = TemplateView::pageLinks($template);
		$page->start();
		$form = HtmlForm::named('wijzigTemplate');
		$form->add(TemplateView::createForm($template));
		$form->add(HtmlInput::makeFormSubmitButton(_('Dat is mijn template en daar moet u het mee doen!')));
		$page->add($form);
		return new PageResponse($page);
	}

	/**
	 * @brief Verwijder de template met het variabele stukje ID.
	 *
	 * @site{/Service/Intern/Template/ * /Verwijder}
	 */
	static public function templateVerwijder()
	{
		requireAuth('god');

		$vfs = vfsVarEntryNames();

		if (!($template = Template::geef($vfs[0])))
		{
			return responseUitStatusCode(403);
		}

		if (Token::processNamedForm('TemplateVerwijderen'))
		{
			$error = $template->verwijderen();

			if (!empty($error)) {
				Page::addMelding($error);
			} else {
				return Page::responseRedirectMelding(TEMPLATE_BASE, _("Template is verwijderd!"));
			}
		}

		$page = TemplateView::pageLinks($template);
		$page->start();
		$page->add(TemplateView::verwijderForm($template));
		return new PageResponse($page);
	}
}

?>
