<?php

abstract class Ajax_Controller
{
	// TODO SOEPMES: deze functie wordt nooit gebruikt
	static public function dili_zoek()
	{
		if(!Dili_Controller::magInschrijven())
			spaceHTTP(403);
			
		$leden = LidVerzameling::zoek(tryPar('str'), true);
		$page = Page::getInstance('innerhtml')
			 ->start()
			 ->add(PersoonVerzamelingView::lijst($leden, null, array(), '', null, true))
			 ->end();
	}
	
	/**
	 * Simpel zoekformulier afhandelen
	 *
	 * @site{/Ajax/Persoon/Zoek}
	 */
	static public function persoon_zoek ()
	{
		$leden = LidVerzameling::zoek(tryPar('str'));
		$page = new InnerHTMLPage();
		$page->start()
			->add(PersoonVerzamelingView::lijstToonEnkel($leden));
		$response = new PageResponse($page);
		return $response;
	}

	/**
	 * Shop een persoon in je kart.
	 *
	 * @site{/Ajax/Persoon/Shop}
	 */
	static public function persoon_shop ()
	{
		$lidid = tryPar('lid');
		$pers = Persoon::geef($lidid);
		if($pers && $pers->magBekijken())
		{
			Kart::geef()->voegPersoonToe($lidid);
			$content = HtmlSpan::fa_stacked(array(
				HtmlSpan::fa('shopping-cart', _('Unshop'), 'fa-stack-1x text-muted'),
				HtmlSpan::fa('ban', _('Unshop'), 'fa-stack-2x text-danger')
			));
			$body = new HtmlSpan($a = PersoonView::makeLink($pers, $content, null, 2));
			$a->setAttribute('onclick',"unShopLidAjax(" . $pers->geefID() . ", this); return false");
			$a->addClass('btn btn-primary');
		}
		else
		{
			$body = new HtmlSpan('Probleem!');
		}

		$page = Page::getInstance('innerhtml')
			 ->start()
			 ->add($body)
			 ->end();
	}

	/**
	 * Shop een persoon uit je kart.
	 *
	 * @site{/Ajax/Persoon/UnShop}
	 */
	static public function persoon_unshop ()
	{
		$lidid = tryPar('lid');
		$pers = Persoon::geef($lidid);
		if($pers && $pers->magBekijken())
		{
			Kart::geef()->verwijderPersoon($lidid);
			$content = HtmlSpan::fa('shopping-cart', _('Shop'), 'text-muted');
			$body = new HtmlSpan($a = PersoonView::makeLink($pers, $content, null, 1));
			$a->setAttribute('onclick',"shopLidAjax(" . $pers->geefID() . ", this); return false");
			$a->addClass('btn btn-primary');
		}
		else
		{
			$body = new HtmlSpan('Probleem!');
		}

		$page = Page::getInstance('innerhtml');
		$page->start()->add($body)->end();
	}

	/**
	 * Shop een commissie in je kart.
	 *
	 * @site{/Ajax/Cie/Shop}
	 */
	static public function cie_shop ()
	{
		$cieid = tryPar('cie');
		$cie = Commissie::geef($cieid);
		$icon = tryPar('icon');
		if ($cie && $cie->magBekijken() && hasAuth('ingelogd'))
		{
			Kart::geef()->voegCieToe($cieid);
			$body = CommissieView::makeShopLink($cie, false, $icon);
			$body->setAttribute('onclick', 'unShopCieAjax(' . $cie->geefID() . ", $icon, this); return false");
		}
		else
		{
			$body = new HtmlSpan('Probleem!');
		}

		$page = Page::getInstance('innerhtml')
			 ->start()
			 ->add($body)
			 ->end();
	}

	/**
	 * Shop een commissie uit je kart.
	 *
	 * @site{/Ajax/Cie/UnShop}
	 */
	static public function cie_unshop ()
	{
		$cieid = tryPar('cie');
		$cie = Commissie::geef($cieid);
		if ($cie && $cie->magBekijken() && hasAuth('ingelogd'))
		{
			$icon = tryPar('icon');
			Kart::geef()->verwijderCie($cieid);
			$body = CommissieView::makeShopLink($cie, true, $icon);
			$body->setAttribute('onclick', 'shopCieAjax(' . $cie->geefID() . ", $icon, this); return false");
		}
		else
		{
			$body = new HtmlSpan('Probleem!');
		}

		$page = Page::getInstance('innerhtml')
			 ->start()
			 ->add($body)
			 ->end();
	}

	/**
	 * Zoek naar leden in het formulier
	 * post-data: 'str': bevat de zoekterm
	 *            'naam': formveld om aan toe te voegen
	 *            'single': moeten het radio ipv checkbuttons zijn?
	 *
	 * @site{/Ajax/Form/defaultFormPersoonVerzameling/Zoek}
	 */
	static public function form_defaultFormPersoonVerzameling_zoek ()
	{
		$pers = PersoonVerzameling::zoek(tryPar('str')); //TODO
		$pers->sorteer("Naam");
		$pers->sorteer("MijEerst");

		$naam = tryPar('naam', 'PersoonVerzameling');
		$single = trypar('single', false);

		$page = new InnerHTMLPage();
		$page->start();
		if ($pers->aantal() > 0)
		{
			foreach ($pers as $figuur) {
				$page->add(PersoonView::selectieOptie($naam, $figuur, $single));
			}
		}
		else
		{
			$page->add(new HtmlSpan(_("Geen resultaten gevonden.")));
		}
		return new PageResponse($page);
	}

	/**
	 * Voeg de leden van de commissie toe aan het zoekformulier
	 * post-data: 'cie': id van de commissie
	 *            'naam': formveld om aan toe te voegen
	 *            'single': moeten het radio ipv checkbuttons zijn?
	 *
	 * @site{/Ajax/Form/defaultFormPersoonVerzameling/CieZoek}
	 */
	static public function form_defaultFormPersoonVerzameling_cieZoek ()
	{
		$cie = Commissie::geef(tryPar('cieId'));
		$naam = tryPar('naam', 'PersoonVerzameling');
		$single = trypar('single', false);

		$page = new InnerHTMLPage();
		$page->start();
		if ($cie) {
			$pers = $cie->leden();

			if ($pers->aantal() > 0)
			{
				foreach ($pers as $figuur) {
					$page->add(PersoonView::selectieOptie($naam, $figuur, $single));
				}
			} else
				$page->add(new HtmlSpan(_("Geen resultaten gevonden.")));
		} else {
			$page->add(new HtmlSpan(_("Die commissie bestaat niet.")));
		}
		return new PageResponse($page);
	}

	/**
	 * @site{/Ajax/Form/defaultFormPersoonVerzameling/LaadKart}
	 */
	static public function form_defaultFormPersoonVerzameling_laadKart()
	{
		$naam = tryPar('naam', 'PersoonVerzameling');
		$single = trypar('single', false);

		$page = Page::getInstance('innerhtml')->start();
		$pers = Kart::geef()->getAllePersonen();
		if ($pers->aantal() > 0)
		{
			foreach ($pers as $figuur) {
				$page->add(PersoonView::selectieOptie($naam, $figuur, $single));
			}
		} else
			$page->add(new HtmlSpan(_('Je kart is leeg.') . ' ' .
				new HtmlAnchor('/Service/Kart', _('Shop er eerst wat mensen in!'))));
		$page->end();
	}

	/**
	 * @site{/Ajax/Form/defaultFormPersoonVerzameling/FotoWebZoek}
	 */
	static public function form_fotowebPersoonVerzameling_zoek ()
	{
		$pers = PersoonVerzameling::zoek(tryPar('str')); //TODO
		$pers->sorteer('Naam');
		$selected = explode(',', tryPar('sel'));

		$page = Page::getInstance('innerhtml')->start();
		if($pers->aantal() > 0) {
			foreach($pers as $p)
			{
				$page->add("<option value=" . $p->getContactID() . ">" . PersoonView::naam($p) . "</option>");
			}
		}
		else
			$page->add("<option value>" . _("Geen resultaten") . "</option>");
		$page->end();
	}

	/**
	 * @site{/Ajax/VernieuwToken}
	 */
	public static function vernieuwToken()
	{
		$token = Token::reconstruct(); //pakt automatisch tryPar('token')
		if(!$token || $token->isUsed())
			die("0");
		$token->setStamp(time());
		die(($token->getTTL()-5) . ""); //Beetje marge om botsingen te voorkomen
	}

	/**
	 * De functie die wordt aangeroepen via ajax als je te lang naar de website kijkt
	 *
	 * @site{/Ajax/Achievements/Webcam}
	 */
	public static function webcamAchievement()
	{
		$pers = Persoon::getIngelogd();
		if($pers) {
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('creeper');
		}
		die();
	}

	/**
	 * De functie die wordt aangeroepen via ajax als je de hele a-es2historie gelezen hebt
	 *
	 * @site{/Ajax/Achievements/Historie}
	 */
	public static function historieAchievement()
	{
		$pers = Persoon::getIngelogd();
		if($pers) {
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('historie');
		}
		die();
	}

	/**
	 * De functie die wordt aangeroepen via ajax als je naar de ibablog gaat
	 *
	 * @site{/Ajax/Achievements/Ibablog}
	 */
	public static function ibablog()
	{
		$pers = Persoon::getIngelogd();
		if($pers) {
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('ibablog');
		}
		die();
	}

	/**
	 * Zoek het voorouderdeel van de mentorstamboom op
	 *
	 * @site{/Ajax/Lid/Stamboom}
	 */
	public static function stamboom()
	{
		$lidnr = tryPar('lidnr');
		$wortel = Lid::geef($lidnr);

		if (!$wortel) {
			die();
		}

		$div = LidView::lidStamboom($wortel, 0, false, true);
		$page = Page::getInstance('innerhtml')
			->start()
			->add($div)
			->end();

		die();
	}

	/**
	 * Zoek het nageslachtdeel van de mentorstamboom op
	 *
	 * @site{/Ajax/Lid/StamboomRev}
	 */
	public static function stamboomRev()
	{
		$lidnr = tryPar('lidnr');
		$wortel = Lid::geef($lidnr);

		if (!$wortel) {
			die();
		}

		$div = LidView::lidStamboomRev($wortel, 0, false, true);
		$page = Page::getInstance('innerhtml')
			->start()
			->add($div)
			->end();

		die();
	}

	/**
	 * @site{/Ajax/Lid/TourAfgemaakt}
	 */
	public static function tourAfgemaakt()
	{
		$persoon = Persoon::getIngelogd();
		if(!$persoon)
			spaceHttp(403);

		$voorkeur = $persoon->getVoorkeur();
		$voorkeur->setTourAfgemaakt(true);
		$voorkeur->opslaan();

		return;
	}

	/**
	 * Wordt gebruikt in de space/sendmail.php onder DEBUG
	 *
	 * Om deze mail te kunnen sturen moet iemand:
	 * 1. een systeemaccount hebben waarmee hij op de debug-pagina van iemand mag.
	 * 2. Ingelogd zijn
	 *
	 * @site{/Ajax/StuurDebugMail}
	 *
	 */
	public static function stuurDebugMail()
	{
		if (!DEBUG || !Persoon::getIngelogd())
			return responseUitStatusCode(403);

		require_once('space/mail_headers.php');

		$subject = 'DEBUGMAIL (to: ' . tryPar('to') . '): ' . trim(tryPar('subject'));
		$mailcontent = tryPar('mailcontent');

		$from = tryPar('from');
		$to = tryPar('address');
		$themail = "From: " . trim($from) . "\n" .
			"Sender: " . trim($from) . "\n" .
			"To: " . trim($to) . "\n" .
			"Subject: " . encodeHeader($subject) . "\n" .
			$mailcontent;
		$bounceto = ''; // hier doen we gewoon niet aan.

		if (!$from || !$to || !$themail) {
			return responseUitStatusCode(400);
		}

		forceSendMail($from, $to, $themail, $bounceto);
		return new JSONResponse(array("status" => "succesvol"));
	}
}
