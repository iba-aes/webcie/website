<?php

/**
 * /Vereniging
 *  /Nieuws					-> (overzicht) geeft een kort nieuwsoverzicht
 *   /Allemaal				-> (allemaal) geeft een overzicht van alle nieuwsberichten
 *   /Toevoegen				-> (toevoegen) voegt een nieuwe mededeling toe
 *   /mededeling_id			-> (details) details van een mededeling
 *    /Wijzig				-> (wijzig) wijzigt een bestaande mededeling
 */

abstract class Mededelingen_Controller
{	
	static public function overzicht()
	{
		$div = new HtmlDiv();
		$div->add(MededelingVerzamelingView::maakOverzicht(MededelingVerzameling::huidige(), true));
		$page = Page::getInstance()
			->addHeadCSS(Page::minifiedFile('datatables.css', 'css'))
			->addFooterJS(Page::minifiedFile('datatables.js', 'js'))
			->addFooter(HtmlScript::makeJavascript('$("#mededelingTable").show().DataTable({
	"order": [[ 4, "desc" ]]
	});'))
			->start(_("Mededelingen"), 'mededelingen')
			->add($div)
			->add(new HtmlTableDataCell(HtmlAnchor::button("/Vereniging/Nieuws/Allemaal", _("Ga naar het archief"))))
			->add(new HtmlParagraph(new HtmlAnchor('/Vereniging/Nieuws/RSS', HtmlSpan::fa('rss', 'RSS') . '&nbsp;' . _("RSS feed"))))
			->end();
	}

	static public function allemaal()
	{
		$perpagina = WSW4_MEDEDELINGEN_PER_PAGINA;

		$limit = tryPar('limit', '0_0');
		$offset = explode('_', $limit);
		if($offset[0] < 0)
			$offset[0] = 0;

		$mededelingen = MededelingVerzameling::allemaal();
		$div = new HtmlDiv(MededelingVerzamelingView::maakOverzicht($mededelingen, true));
		$page = Page::getInstance()
			->addHeadCSS(Page::minifiedFile('datatables.css', 'css'))
			->addFooterJS(Page::minifiedFile('datatables.js', 'js'))
			->addFooter(HtmlScript::makeJavascript('$("#mededelingTable").show().DataTable({
	"order": [[ 4, "desc" ]]
	});'))
			->start(_("Alle mededelingen"), 'mededelingen')
			->add($div)
			->end();
	}

	/**
	 * De RSS-feed van de laatste nieuwsberichten
	 */
	static public function rss ()
	{
		/** Laad alle activiteiten **/
		$nieuws = MededelingVerzameling::allemaal();

		/** Ga naar de view **/
		MededelingVerzamelingView::rss($nieuws);
	}

	static public function toevoegen()
	{
		$show_error = false;

		/** Verwerk formulier-data **/
		$newMed = new Mededeling();
		if (Token::processNamedForm() == 'MedNieuw')
		{
			MededelingView::processNieuwForm($newMed);
			if ($newMed->valid('nieuw'))
			{
				$newMed->opslaan();
				Page::redirectMelding($newMed->url(), _("De mededeling is nu aangemaakt!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/** Ga naar de view **/
		MededelingView::maakFormPagina($newMed, true, $show_error);
	}

	static public function nieuwsoverzichtEntry($args)
	{
		if ($args[0] < 1) // Waarom gebeurt hier geen degelijke check?
			return false;

		return array('name' => $args[0],
			'displayName' => 'overzicht',
			'access' => true);
	}

	static public function mededelingEntry ($args)
	{
		$mededeling = Mededeling::geef($args[0]);
		if (!$mededeling) return false;

		return array('name' => $mededeling->geefID(),
			'displayName' => $mededeling->getOmschrijving(),
			'access' => $mededeling->magBekijken());
	}

	static public function details ()
	{
		$medid = (int) vfsVarEntryName();
		$med = Mededeling::geef($medid);

		MededelingView::details($med);
	}

	static public function wijzigen()
	{
		global $auth;

		$show_error = false;

		/** Verwerk formulier-data **/
		$medID = (int)vfsVarEntryName();
		$newMed = Mededeling::geef($medID);

		// check of wijzigen eigenlijk wel mag
		if (!$auth->hasCieAuth($newMed->getCommissie()->geefID())) {
			spaceHTTP(403);
		}

		if (Token::processNamedForm() == 'MedNieuw')
		{
			MededelingView::processNieuwForm($newMed);
			if ($newMed->valid('nieuw'))
			{
				$newMed->opslaan();
				Page::redirectMelding($newMed->url(), _("De mededeling is nu gewijzigd!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/** Ga naar de view **/
		MededelingView::maakFormPagina($newMed, false, $show_error);
	}

	static function verwijderen()
	{
		global $auth;

		$medID = (int)vfsVarEntryName();
		$med = Mededeling::geef($medID);

		// check of dit eigenlijk wel mag
		if (!$auth->hasCieAuth($med->getCommissie()->geefID())) {
			spaceHTTP(403);
		}

		switch(Token::processNamedForm()) {
		case 'MededelingVerwijderen':
			$returnVal = $med->verwijderen();//weghalen die hap
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding("/Vereniging/Nieuws", '"' . $med->getOmschrijving() . _('" is verwijderd uit de Databaas.'));
			}
			return;
		case NULL:
		default:
			break;
		}

		$page = Page::getInstance();
		$page->start();
		$page->add(MededelingView::verwijderForm($med));
		$page->end();


	}
}
