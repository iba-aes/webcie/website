<?php
/**
 * /Onderwijs
 *  /Agenda				-> (agenda_content) Agenda
 */

abstract class Onderwijs_Controller
{
	/**
	 * Plaatst 5 aankomende activteiten in het menu
	 */
	static public function aankomendOnderwijs_menu()
	{
		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten(
				time(), (time()+(60*60*24*14)), 'PUBLIEK', 'J', null);
		$activiteiten->rewind();

		/** Maak de menu-items **/
		$children = array();
		for ($i = 0; $i < 5 && $i < $activiteiten->aantal(); $i++)
		{
			$act = $activiteiten->current();
			$children[] = array('url' => $act->url(), 'displayName' => $act->getTitel());
			$activiteiten->next();
		}

		/** Geef de items terug **/
		return array('url' => '/Onderwijs/Agenda', 'displayName' => _("Onderwijsagenda"), 'children' => $children);
	}

	/**
	 * Print de aankomende activiteiten op het scherm
	 **/
	static public function aankomendOnderwijs_content()
	{
		/** Stel vast of er ingelogd is **/
		$pers = Persoon::getIngelogd();

		/** Verwerk shortForm-data **/
		$stijl		= tryPar('stijl', 'lijst');
		$detail		= tryPar('detail', 'alle');
		$onderwijs	= tryPar('onderwijs', 'J');
		$cienr		= (int) tryPar('cienr');
		$heeljaar	= (bool) tryPar('heeljaar', false);

		if(is_null($pers))
			$detail = 'PUBLIEK';
		
		//Vanaf vandaag 0:00
		$van = new DateTimeLocale();
		$van->setTime(0,0);
		$tot = clone $van;
		if($heeljaar)
			$tot->modify("+1 year");
		else
			$tot->modify("+31 days");

		$cie = null;
		if(hasAuth('bestuur') || PersoonVerzameling::vanCommissie($cienr)->bevat($pers))
			$cie = Commissie::geef($cienr);

		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten(
				$van, $tot, $detail, $onderwijs, $cie)->filterBekijken();

		/** Ga naar de view **/
		ActiviteitVerzamelingView::aankomend($activiteiten, $stijl, $onderwijs, $detail, $pers, $cie, $heeljaar);
	}
}
