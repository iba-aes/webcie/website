<?php

setZeurmodus();

/**
 * @brief Exceptie die gegooid wordt als je probeert te testen op de livesite.
 *
 * @see TestController::isTestPage
 */
class TestOpLiveException extends ResponseException
{
	public function maakResponse()
	{
		$page = new HTMLPage();
		$page->setBrand('bugweb');
		$page->start(_("Tester niet testen!"));
		$page->add(new HtmlParagraph(_("Dit is niet je debug, vriend! Dus we gaan ook geen tests doen.")));
		$response = new PageResponse($page);
		$response->setStatusCode(403); // Zodat AJAX ook faalt.
		return $response;
	}
}

/**
 * @brief Controller om tests te runnen op je debuginstance.
 *
 * Het is een Extreem Slecht Idee™ om dit op live te runnen.
 */
abstract class TestController
{
	/**
	 * @brief Soort-van-decorator die testpages alleen voor goden op DEBUG toelaat.
	 *
	 * Indien de vereisten voor tests niet geslaagd zijn, gooit dit een ResponseException.
	 * Die zal door space worden afgevangen en omgezet in een response voor de gebruiker.
	 */
	private static function isTestPage()
	{
		requireAuth('god');
		if (!DEBUG)
		{
			throw new TestOpLiveException();
		}
	}

	/**
	 * @brief Laad het testsysteem in.
	 *
	 * Deze functie zorgt ervoor dat de TestRunner correct ingeladen wordt.
	 *
	 * @param naam NULL, of de naam van een losse test om uit te voeren.
	 * Moet een key zijn in $this->gevondenTests.
	 * Kan gebruikt worden door test discovery om minder tsjak in te hoeven laden.
	 *
	 * @return Een TestRunner met alle tests geregistreerd.
	 */
	private static function laadTests($naam = null)
	{
		// Voorkom ongelukjes...
		assert(DEBUG && hasAuth('god'));

		// Laad alle tests in.
		chdir('../../scripts');
		require_once('../scripts/script-init.php');
		$runner = new WhosWho4\TestHuis\TestRunner($naam);
		chdir('../www/space');

		return $runner;
	}

	/**
	 * @brief Welkomstpagina om je debug te testen.
	 *
	 * @site {/Service/Intern/Test/index.html}
	 */
	public static function home()
	{
		static::isTestPage();

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('test.css', 'css'));
		$page->addFooterJS(Page::minifiedFile('test.js'));
		$page->setBrand('bugweb');
		$page->start();

		// Uitleg
		$page->add($panel = new HtmlPanel(_("Test de site!")));
		$panel->add(new HtmlParagraph(sprintf(
			_("Op deze pagina zie je alle tests die op de site staan ingesteld."
			. " Zie voor meer info ook %s[VOC: link naar testhandleiding]."
			),
			new HtmlAnchor(
				"https://mediawiki.a-eskwadraat.nl/wiki/index.php/Tests",
				_("de testhandleiding")
			)
		)));
		// Resultaten
		$panel->add($resultaten = new HtmlParagraph());

		$resultaten->add(_("Gevonden tests") . ": ");
		$resultaten->add(new HtmlSpan("0", "test-nr", 'test-nr-gevonden'));

		$resultaten->add(_("Uitgevoerde tests") . ": ");
		$resultaten->add(new HtmlSpan("0", "test-nr", 'test-nr-uitgevoerd'));

		$resultaten->add(_("Geslaagde tests") . ": ");
		$resultaten->add(new HtmlSpan("0", "test-nr", 'test-nr-geslaagd'));
		// Run-knop
		$panel->add($runAllesButton = new HtmlButton('button', _("Run alle tests")));
		$runAllesButton->setId('test-runAlles');

		// Laad alle tests in.
		$runner = static::laadTests();

		// Toon ze in een mooi tabelletje.
		$page->add($tabel = new HtmlTable());
		$tabel->addClass("table table-white table-hover sortable");
		$headRow = $tabel->addHead()->addRow();
		$headRow->add(new HtmlTableHeaderCell(_("Naam / Info")));
		$headRow->add(new HtmlTableHeaderCell(_("Status")));
		$headRow->add(new HtmlTableHeaderCell()); // Run!
		$body = $tabel->addBody();
		foreach ($runner->getGevondenTests() as $testnaam => $testobj)
		{
			// Stop de naam van de test in de id, voor testdetectie in javascript.
			$body->add($row = new HtmlTableRow([], "test-item", $testnaam));

			// Naam van test
			$row->add($naamCell = new HtmlTableDataCell($testnaam, "test-item-naam"));
			// Informatie over testuitkomst.
			// Deze staan in dezelfde kolom als de naam want ziet er mooi uit.
			// Button en div worden door javascript getoond als er info is.
			$naamCell->add($infoButton = new HtmlButton('button', _("?")));
			$infoButton->addClass('btn btn-default test-item-infoToggle');
			$naamCell->add(new HtmlDiv($infoPre = new HtmlPre(_("Nog geen testresultaat bekend...")), "test-item-infoContainer"));
			$infoPre->addClass("test-item-info");

			// Status van test (door javascript geupdated)
			$row->add(new HtmlTableDataCell("?", "test-item-status"));

			// Run-knop (roept javascript aan)
			$row->add($runCell = new HtmlTableDataCell());
			$runCell->add($runButton = new HtmlButton('button', _("Run!")));
			$runButton->addClass('btn btn-default test-item-run');
			// Bonus: voer test in browser uit voor betere (meta-)debugging.
			// TODO: willen we dit behouden?
			$runCell->add(new HtmlAnchor(TEST_BASE . 'RunTest?test=' . $testnaam, new HtmlSmall(_("debug"))));
		}

		return new PageResponse($page);
	}

	/**
	 * @brief Voer de gegeven test uit en geef in JSON-vorm de resultaten.
	 *
	 * Neemt een GET-parameter: test, de naam van de test om te runnen.
	 *
	 * Output is dezelfde vorm als `TestRunner::runLosseTest`.
	 *
	 * @site {/Service/Intern/Test/RunTest}
	 *
	 * @see TestRunner::runLosseTest
	 */
	public static function runTestAJAX()
	{
		static::isTestPage();

		// We moeten wel een testnaam krijgen.
		// Verdere validatie op naam wordt door de TestRunner gedaan.
		$naam = tryPar('test', false);
		if (!$naam)
		{
			return responseUitStatusCode(400);
		}

		// Voer de gegeven test uit.
		$runner = static::laadTests($naam);
		$uitkomst = $runner->runLosseTest($naam);

		return new JSONResponse($uitkomst);
	}
}
