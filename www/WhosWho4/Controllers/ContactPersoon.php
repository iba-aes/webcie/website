<?php

abstract class ContactPersoon_Controller
{

	/**
	 * Returneert een array met het geselecteerd contactpersoon
	 *
	 * @site{/Leden/Organisatie/ * /ContactPersonen/ * /}
	 */
	static public function contactpersoonEntry ($args)
	{

		$cp = ContactPersoon::geef((int)$args[0], (int)$args[2], "NORMAAL");
		if (!$cp) return false;

		$pers = Persoon::geef($args[0]);

		return array( 'name' => $pers->geefID()
					, 'displayName' => PersoonView::naam($pers) . "; " . $cp->getFunctie()
					, 'access' => $cp->magBekijken());
	}

	/**
	 * Toon de detailspagina van een contactpersoon
	 *
	 * @site{/Leden/Organisatie/ * /ContactPersonen/ * /index.html}
	 */
	static public function details ()
	{
		/** Laad de gekozen contactpersoon **/
		$args = vfsVarEntryNames();
		$pers = Persoon::geef($args[0]);

		$cpers = ContactPersoon::geef($args[0], $args[1], "NORMAAL");

		if(!$cpers->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		return new PageResponse(ContactPersoonView::detailsPagina($cpers));
	}

	/**
	 * Maak een nieuwe contactpersoon aan
	 *
	 * @site{/Leden/Organisatie/ * /ContactPersonen/Nieuw}
	 */
	static public function nieuw ()
	{
		$entry = vfsVarEntryNames();

		ContactPersoon_Controller::wijzig(new ContactPersoon(new Persoon(), Organisatie::geef($entry[0]), "NORMAAL"));
	}

	/**
	 * Wijzig een contactpersoon
	 *
	 * @site{/Leden/Organisatie/ * /ContactPersonen/ * /Wijzig}
	 */
	static public function wijzig ($cpers  = null)
	{
		if(!$cpers){
			/** Laad de gekozen contactpersoon **/
			$args = vfsVarEntryNames();
			$pers = Persoon::geef($args[0]);

			$cpers = ContactPersoon::geef($args[0], $args[1], "NORMAAL");
		} else {
			$pers = $cpers->getPersoon();
		}

		if (!$cpers->magWijzigen())
			spaceHttp(403);

		$show_error = false;

		/** Verwerk formulier-data **/

		if (Token::processNamedForm() == 'ContactPersoonWijzig')
		{
			ContactPersoonView::processWijzigForm($cpers);
			if ($cpers->valid() && $pers->valid())
			{
				$pers->opslaan();
				$cpers->opslaan();
				Page::redirectMelding($cpers->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/** Ga naar de view **/
		ContactPersoonView::wijzig($cpers, $show_error);
	}

	/**
	 * @site{/Leden/Organisatie/ * /ContactPersonen/ * /Verwijder}
	 */
	static public function verwijder()
	{
		/** Laad de gekozen contactpersoon **/
		$args = vfsVarEntryNames();
		$pers = Persoon::geef($args[0]);

		$cpers = ContactPersoon::geef($args[0], $args[1], "NORMAAL");

		if (!$cpers->magWijzigen())
			spaceHttp(403);

		$page = Page::getInstance();
		$page->start();

		$header = _('Contactpersoon verwijderen');
		$page->add(new HtmlHeader(2, $header));

		switch(Token::processNamedForm()) {
		case 'ContactPersoonVerwijder':

			$cpersUrl = $cpers->getOrganisatie()->url(); //Weg is weg, dus alvast opslaan
			$page->add(_('Contactpersoon wordt nu verwijderd...even wachten...') . new HtmlBreak());
			$cpers->verwijderen();
			$pers->verwijderen(); //Ook het contact verwijderen
			$page->add(_('...en gelukt!'), new HtmlBreak(),
				sprintf(_('Terug naar de %s[VOC: hier komt \'organisiatie\']'),
				new HtmlAnchor($cpersUrl,_('organisatie'))));
			break;

		default:

			$page->add(sprintf(_('Weet je zeker dat je %s wilt verwijderen?'),
				PersoonView::naam($cpers->getPersoon())));

			$form = HtmlForm::named('ContactPersoonVerwijder');

			$form->add(HtmlInput::makeSubmitButton(_('Ja!')));
			$page->add($form);
			break;
		}

		$page->end();
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
