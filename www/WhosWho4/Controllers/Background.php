<?php

abstract class Background_Controller
{

	/**
	 * @site{/Background/ *}
	 */
	static public function backgroundFotoEntry ($args)
	{
		return array( 'name' => $args[0]
			, 'displayName' => $args[0]
			, 'access' => true);
	}

	/**
	 * @brief Geeft de locatie van een foto terug
	 *
	 * @param id Het nummer van de foto op het FS
	 * @return De locatie van de foto
	 */
	static private function geefLocatie ($id)
	{
		return self::geefMap() . $id . ".jpg";
	}

	/**
	 * @brief Geeft de map waar de foto's staan terug
	 *
	 * @return De map met foto's
	 */
	static private function geefMap ()
	{
		if(DEBUG) {
			return "fotos/Background-Debug/";
		} else {
			return "fotos/Background/";
		}
	}

	/**
	 * @brief Geeft de foto terug van het vfs
	 *
	 * @site{/Background/ * /index.html}
	 */
	static public function showBackgroundFoto ()
	{
		global $filesystem;

		$id = (int) vfsVarEntryName();

		$loc = self::geefLocatie($id);

		// als de foto niet bestaat, geef dan de eerste de beste terug
		// dit voorkomt allerlei errors die je krijgt door gecachte achtergronden
		// zolang het kan, geven we een foto ipv 404-error want dat is een nettere degradatie
		
		if (!$filesystem->has($loc)) {
			$dir = self::geefMap();
			$fotos = scandir(FILESYSTEM_PREFIX . $dir);

			// geef de eerste na '.' en '..'
			if (count($fotos) <= 2) {
				// maar als dat niet kan, geef een 404
				spaceHTTP("404");
			}
			$loc = $dir . "/" . $fotos[2];
		}
		
		$savefilename = $loc;

		return sendfilefromfs($loc, $savefilename);
	}

	/**
	 * @brief Verwijdert een foto van het vfs
	 *
	 * @site{/Background/ * /Delete}
	 */
	static public function deleteBackgroundFoto ()
	{
		if(!hasAuth('bestuur')){
			spaceHttp(403);
		}
		$id = (int) vfsvarEntryName();
		
		$loc = self::geefLocatie($id);

		unlink(FILESYSTEM_PREFIX . $loc);

		Page::redirectMelding('/Background/', _('Gelukt!'));
	}

	/**
	 * @brief Geeft een uniek ID aan de nieuw toe te voegen foto
	 *
	 * @return Een uniek ID voor een nieuwe foto of -1 als er al meer dan 1000 foto's zijn
	 */
	static private function geefUniekId () 
	{
		$dir = self::geefMap();
		$scanned_dir = scandir(FILESYSTEM_PREFIX . $dir);

		$i = 0;

		//1000 achtergrondfoto's is wel genoeg
		$limit = 1000;

		while($i < $limit)
		{
			if (!in_array($i . '.jpg', $scanned_dir)) 
				return $i;
			$i++;
		}
		return -1;

	}

	/**
	 * @brief Voegt een nieuwe achtergrondfoto toe
	 *
	 * @site{/Background/Toevoegen}
	 */
	static public function backgroundToevoegen ()
	{
		global $filesystem;

		if(!hasAuth('bestuur')) {
			spaceHttp(403);
		}

		$msg = null;

		if(Token::processNamedForm() == 'toevoegenbackground') {
			$fotoId = trypar('fotoId');
			if(!Media::geef($fotoId)) {
				$msg = new HtmlSpan(_('Er waren fouten.'),'text-danger strongtext');
				BackgroundView::toevoegen($msg);
			}
			else {
				$loc = Media::geefAbsLoc('groot', $fotoId);
				$id = self::geefUniekId();
				if($id == -1){
					$msg = new HtmlSpan(_('Er zijn te veel achtergrondfoto\'s'), 'text-danger strongtext');
					BackgroundView::toevoegen($msg);
				}
				else
				{
					$filesystem->copy($loc, self::geefMap() . $id . '.jpg');
					Page::redirectMelding('/Background/', _('Gelukt!'));
				}
			}
		}
		BackgroundView::toevoegen($msg);
	}

	/**
	 * @brief Geeft een overzicht van alle achtergrondfoto's
	 *
	 * @site{/Service/Intern/Background}
	 */
	static public function backgroundOverzicht()
	{
		if(!hasAuth('bestuur')){
			spaceHttp(403);
		}
		$page = Page::getInstance()->start();

		$page->add(new HtmlParagraph(_('Hieronder staan alle achtergrondfoto\'s. Klik op de link \'Verwijder foto\' 
			om een achtergrondfoto te verwijderen. Klik op \'Achtergrond toevoegen\' om een nieuwe achtergrondfoto toe te voegen.')));

		$directory = self::geefMap();
		$scanned_directory = scandir(FILESYSTEM_PREFIX . $directory);
		$scanned_directory = array_splice($scanned_directory, 2, count($scanned_directory)-2); 

		//Foto Overzichtje
		$length = count($scanned_directory);

		$x = 0;
		$rows = new HtmlDiv(null, 'container-fluid');
		foreach($scanned_directory as $foto) {

			if ($x % 4 == 0){
				$fotos = new HtmlDiv(null, 'row');
				$rows->add($fotos);
			}

			if ($x % 2 == 0) {
				$fotoCol = new HtmlDiv($fotos = new HtmlDiv(null, 'row'), 'col-l-4');
				$rows->add($fotoCol);
			}

			$dif = new HtmlDiv(null);
			$dif->add($link = new HtmlAnchor("/Background/".$foto));
			$link->add($img = new HtmlImage("/Background/".$foto, "Background"));
			$dif->add(new HtmlAnchor("/Background/".$foto."/Delete", _('Verwijder foto')));
			$img->setWidth(300);
			$fotos->add($dif->addClass('col-xs-4'));
			$x++;
		}

		$page->add($rows);
		$page->add(new HtmlHR());

		$page->add($list = new HtmlList());
		$list->add($li = new HtmlListItem());
		$li->add(new HtmlAnchor("/Background/Toevoegen", _('Achtergrond toevoegen')));

		$page->end();
	}
}

