<?php

abstract class Service_Controller
{
	public static function home()
	{
		global $auth;
		global $session;

		switch (Token::processNamedForm())
		{
			case 'override_datetime':
				$override = tryPar('override_datetime', null);

				if (is_null($override) && $session->has('override_datetime'))
				{
					$session->remove('override_datetime');
				}
				else
				{
					if (!is_null($override))
					{
						$session->set('override_datetime', $override);
					}
				}
				break;
			case 'toggle_god':
				if (!$session->get('use_god'))
				{
					$verify = tryPar('wachtwoord');
					$ww = Wachtwoord::geef($auth->getLidnr());
					if (!$ww->authenticate($verify))
					{
						Page::addMelding(_('Wachtwoord-verificatiefout'), 'fout');
					}
					else
					{
						$session->set('use_god', true);

						// Gooi meteen de cacheentries weg aangezien er verschillende
						// menu's zijn voor god en geen god
						gooiCacheEntrysWeg();

						// SOEPMES hack: omdat we beginnen met db-level ingelogd,
						// kun je niet zomaar de rest van de pagina aanmaken,
						// want die vereist een db-level van god.
						// Deze db-levels worden geset bij space-initialisatie.
						// Dus redirect meteen naar dezelfde pagina, die wel
						// met db-level god begint.
						Page::redirectMelding('/Service', _('Veel succes met je godrechten, maar gebruik ze wijselijk Petertje!'), 'succes');
					}
				}
				else
				{
					$auth->terugNaarJeEigenHuid();
					$session->set('use_god', false);

					// Gooi meteen de cacheentries weg aangezien er verschillende
					// menu's zijn voor god en geen god
					gooiCacheEntrysWeg();
				}
				break;
			case 'sloop':
				throw new Exception('RIP');
		}

		$body = new HtmlElementCollection();
		$body->add($div = new HtmlDiv(null, 'col-md-7'));
		$div->add(new HtmlParagraph(_("Welkom op de servicepagina's van A&ndash;Eskwadraat!")))
			->add(new HtmlParagraph(sprintf(_("Op dit gedeelte van de website kun je allerlei informatie vinden over service die A–Eskwadraat jou biedt. Er zijn verschillende %s te vinden die jouw commissiewerk makkelijker maken. Je kunt hier %s invoeren, als je een probleem bent tegengekomen op de website dat van enigszins technische aard is. Ook verzoeken tot aanpassingen kun je hier doen."), new HtmlEmphasis(_('handleidingen')), new HtmlEmphasis(_('bugs')))))
			->add(new HtmlParagraph(sprintf(_("Als je lid bent, kun je inloggen op deze site om van meer service gebruik te maken, zoals de %s. Als je in een of meerdere commissies zit, kun je hierdoor op deze pagina ook direct je commissiepagina's bewerken via de %s."), new HtmlEmphasis(_('webcam')), new HtmlEmphasis(_('Publisher')))));

		if (DEBUG && ($session->has('realuserid') || $auth->isGod($auth->getRealLidnr())))
		{
			$form = new HtmlForm('post', '/Service/Intern/Lidswitch/');
			$form->addClass('form-inline');
			$form->add(new HtmlParagraph(_("Kruip in de huid van: ")))
				->add($input_group = new HtmlDiv(null, 'input-group'));
			$input_group->add($lidInput = HtmlInput::makeText('overridelidnr', null, 5))
				->add(new HtmlDiv(HtmlInput::makeSubmitButton('Switch', 'Switch'), 'input-group-btn'));
			$lidInput->setAttribute('maxlength', 5);
			$div->add($form);

			$form = HtmlForm::named('override_datetime');
			$form->addClass('form-inline');
			$form->add(new HtmlParagraph(_("Override de tijd: ")))
				->add($input_group = new HtmlDiv(null, 'input-group'));
			$input_group->add($lidInput = HtmlInput::makeDateTime('override_datetime', null))
				->add(new HtmlDiv(HtmlInput::makeSubmitButton('Switch', 'Switch'), 'input-group-btn'));
			$div->add($form);
		}

		// Knop om een error te veroorzaken. Handig om Ĝi estas rompita te testen.
		if (DEBUG)
		{
			$form = HtmlForm::named('sloop');
			$form->addClass('form-inline');
			$input_group = new HtmlDiv(null, 'input-group');
			$input_group->setAttribute('style', 'margin-top: 1em;');
			$input_group->add(HtmlInput::makeSubmitButton(_('Veroorzaak een error'), 'Sloop'));
			$form->add($input_group);
			$div->add($form);
		}

		if ($auth->isGodAchtig($auth->getRealLidnr()))
		{
			$form = HtmlForm::named('toggle_god');
			$form->addClass('form-inline');

			$input_group = new HtmlDiv(null, 'input-group');
			$input_group->setAttribute('style', 'margin-top: 1em;');
			if ($session->get('use_god'))
			{
				$text = 'Zet godrechten UIT';
			}
			else
			{
				$form->add(new HtmlParagraph(new HtmlStrong(_("Om godrechten aan te zetten moet je je eigen wachtwoord (NIET het systeemaccount) invullen."))));
				$text = 'Zet godrechten AAN';
				// Als je al godrechten op live hebt, mag je ze wel zonder wachtwoord
				// weghalen, maar je hebt een ww nodig om ze te krijgen
				$input_group->add(HtmlInput::makePassword('wachtwoord', 14, null, 'wachtwoord', _('Wachtwoord')));
			}
			$form->add($input_group);
			$input_group->add(new HtmlDiv(HtmlInput::makeSubmitButton($text, $text), 'input-group-btn'));
			$div->add($form);
		}

		Page::getInstance()->start("Service")
			->add($body)
			->end();
	}

	public static function register()
	{
		global $request;

		$auths = Register::enumsAuth();
		$types = Register::enumsType();
		$showAuths = array_map(function ($a) {
			return ucfirst(strtolower($a));
		}, $auths);
		$showTypes = array_map(function ($a) {
			return ucfirst(strtolower($a));
		}, $types);

		// Nieuwe registerwaarde
		if ($request->request->get('submit') == "Nieuwe waarde toevoegen")
		{
			$label = $request->request->get('_nieuwLabel');
			$beschrijving = $request->request->get('_nieuweOpmerking');
			$type = $types[$request->request->get('_nieuweType')];
			$waarde = $request->request->get("_nieuweWaarde");
			if (empty($label))
			{
				Page::addMelding("Geef een naam voor de registerentry op!", 'fout');
			}
			elseif (empty($beschrijving))
			{
				Page::addMelding("Geef een beschrijving voor de registerentry op!", 'fout');
			}
			elseif (!Register::heeftType($waarde, $type))
			{
				Page::addMelding("De waarde had niet het goede type.", 'fout');
			}
			else
			{
				$entry = new Register();
				$entry->setLabel($label);
				$entry->setBeschrijving($beschrijving);
				$entry->setAuth($auths[$request->request->get('_nieuweAuth')]);
				$entry->setType($type);
				$entry->updateValue($waarde);
				$entry->opslaan();
				Page::addMelding("Een nieuwe registerentry '" . $label . "' is toegevoegd!", "succes");
			}
		}
		else
		{
			$entries = RegisterQuery::table()->verzamel();
			foreach ($entries as $id => $entry)
			{
				$label = $entry->getLabel();
				if ($request->request->has($id . "_edit"))
				{
					// Registerwaarde bewerken
					$type = $types[$request->request->get($id . "_type")];
					$waarde = $request->request->get($id . "_waarde");
					$beschrijving = $request->request->get($id . "_beschrijving");

					if (!Register::heeftType($waarde, $type))
					{
						Page::addMelding("Register entry '$label' moet een waarde van het goede type hebben.", 'fout');
					}
					elseif (empty($beschrijving))
					{
						Page::addMelding("Register entry '$label' moet een beschrijving hebben.", 'fout');
					}
					else
					{
						$entry->setAuth($auths[$request->request->get($id . "_auth")]);
						$entry->setBeschrijving($beschrijving);
						$entry->setType($type);
						$entry->updateValue($waarde);
						$entry->opslaan();
						Page::addMelding("Register entry '$label' is aangepast!", 'succes');
					}
				}
				elseif ($request->request->has($id . "_delete"))
				{
					// Registerwaarde verwijderen
					$entry->verwijderen();
					Page::addMelding("Registerentry '$label' is verwijderd!", 'succes');
				}
			}
		}

		$page = Page::getInstance()->start("Register");
		$page->add($form = new HtmlForm("post"));
		$form->setAttribute("onsubmit", "return confirm('Weet je zeker dat je deze actie wilt uitvoeren?');");

		$form->add($table = new HtmlTable());
		$table->add($row = new HtmlTableRow());

		$columns = array(
			array('name' => _("Label"), 'width' => 30),
			array('name' => _("Autorisatie"), 'width' => 10),
			array('name' => _("Type"), 'width' => 10),
			array('name' => _("Waarde"), 'width' => 30),
			array('name' => _("Actie"), 'width' => 20),
		);
		foreach ($columns as $col)
		{
			$row->add($th = new HtmlTableHeaderCell($col['name']));
			$th->setCssStyle('width: ' . $col['width'] . '%;');
		}

		$entries = RegisterQuery::table()->orderByAsc('label')->verzamel();
		foreach ($entries as $id => $entry)
		{
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableDataCell(array(
				new HtmlStrong($entry->getLabel()),
				HtmlTextarea::withContent($entry->getBeschrijving(), $id . "_beschrijving")
			)));
			$row->add(new HtmlTableDataCell(HtmlSelectbox::fromArray($id . "_auth", $showAuths, array_keys($auths, $entry->getAuth()))));
			$row->add(new HtmlTableDataCell(HtmlSelectbox::fromArray($id . "_type", $showTypes, array_keys($types, $entry->getType()))));
			$row->add(new HtmlTableDataCell(HtmlTextarea::withContent($entry->getWaarde(), $id . "_waarde")->setAttribute("placeholder", "Wijzig waarde")));

			$editButton = HtmlInput::makeSubmitButton("Wijzig", $id . "_edit");
			$deleteButton = HtmlInput::makeSubmitButton("Verwijderen", $id . "_delete");
			$deleteButton->addClass('btn btn-danger');

			$row->add(new HtmlTableDataCell(new HtmlDiv(array($editButton, $deleteButton), "btn-group")));
		}

		$table->add($row = new HtmlTableRow());

		$nieuweWaarde = new HtmlInput("_nieuwLabel");
		$nieuweWaarde->setAttribute("placeholder", "Nieuw label")->setAttribute("style", "width: 100%;");
		$opmerking = HtmlTextarea::withContent("", "_nieuweOpmerking")->setAttribute("placeholder", "Opmerking");
		$row->add(new HtmlTableDataCell($nieuweWaarde . $opmerking));

		$row->add($authOpties = new HtmlTableDataCell());
		$authOpties->add(HtmlSelectbox::fromArray("_nieuweAuth", $showAuths));

		$row->add($typeOpties = new HtmlTableDataCell());
		$typeOpties->add(HtmlSelectbox::fromArray("_nieuweType", $showTypes));

		$row->add(new HtmlTableDataCell(HtmlTextarea::withContent("", "_nieuweWaarde")->setAttribute("placeholder", "Nieuwe waarde")));
		$addButton = HtmlInput::makeSubmitButton("Nieuwe waarde toevoegen");
		$addButton->addClass('btn btn-success');
		$row->add(new HtmlTableDataCell($addButton));

		$page->end();
	}

	/**
	 * @brief Laat wat statistieken zien over viewcounts e.d.
	 *
	 * @site /Service/Intern/Stats
	 */
	public static function stats()
	{
		// Zodat we alles netjes doen :)
		setZeurmodus();

		$most = (int)tryPar('most', 10);
		$least = (int)tryPar('least', 30);

		$mostViewed = self::vfsViews('DESC', $most);
		$leastViewed = self::vfsViews('ASC', $least);

		$page = new HTMLPage();
		$page->start("Paginastatistieken");

		// We mieteren alles in dezelfde form, zodat wijzigingen aan de ene
		// parameter niet de waarde van de andere resetten.
		$page->add($form = new HtmlForm('get', null, true));

		$form->add(new HtmlLabel('most', _("Maximum aantal populairste")));
		$form->add(HtmlInput::makeNumber('most', $most, 0));
		$form->add(HtmlInput::makeSubmitButton(_("Gaan!")));
		$form->add(StatsView::viewCountTabel($mostViewed));

		$form->add(new HtmlLabel('least', _("Maximum aantal niet-populairste")));
		$form->add(HtmlInput::makeNumber('least', $least, 0));
		$form->add(HtmlInput::makeSubmitButton(_("Banaan!")));
		$form->add(StatsView::viewCountTabel($leastViewed));

		return new PageResponse($page);
	}

	/**
	 * @brief Vraag de views op van de minst/meest bekeken VFS-entries.
	 *
	 * @param $order Of we meest of minst bekeken entries willen zien.
	 *     Een van `'ASC'` of `'DESC'`.
	 * @param $limit Het maximaal aantal entries dat we willen zien.
	 *
	 * @returns Een array met de vorm `array('entry' => vfsEntry, 'views' => int)`.
	 */
	private static function vfsViews($order, $limit)
	{
		global $BMDB;

		// Zodat string interpolation niet kut is.
		$entryFile = ENTRYFILE;
		$entryHook = ENTRYHOOK;
		$entryInclude = ENTRYINCLUDE;

		// De query voor het aggegreren van views, die we parameteriseren op
		// de sortering en het aantal resultaten.
		$query = <<<SQL
SELECT benamite.id, COALESCE(viewcount, 0) as viewcount
FROM benamite LEFT JOIN
	( SELECT benamite_id, COUNT(benamite_id) as viewcount
	  FROM views
	  GROUP BY benamite_id
	) as viewcounts
ON benamite.id = benamite_id
-- We willen alleen entries laten zien de we ook echt loggen.
WHERE benamite.type IN ($entryFile, $entryHook, $entryInclude)
ORDER BY viewcount %s
LIMIT 0, %d;
SQL;
		// Ja, deze indentatie is super raar. Maar zo werkt heredoc syntax nou eenmaal...

		$results = $BMDB->q(sprintf($query, $order, $limit));
		return array_map(function ($result) {
			$id = $result['id'];
			$entry = vfs::get($id);

			if (!$entry)
			{
				user_error("Kan geen VFS entry vinden bij ID " . $id, E_USER_ERROR);
			}

			return array('entry' => $entry, 'views' => $result['viewcount']);
		}, $results);
	}

	public static function phpinfo() {
		phpinfo();
		exit;
	}
}
