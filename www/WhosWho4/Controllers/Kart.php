<?php
/**
 * /Kart			-> (overzicht) toon de inhoud van de kart + mogelijkheid tot uitschrijven
 *  /Toevoegen		-> (toevoegen) voeg meer mensen aan de kart toe
 */

abstract class Kart_Controller
{
	/**
	 * Toon een overzicht van de inhoud van de kart + mogelijkheid tot uitschrijven
	 */
	static public function overzicht ()
	{
		/** Perpareer de output **/
		$body = new HtmlDiv();

		/** Verwerk mogelijke formulier-data **/
		if (Token::processNamedForm() == 'Kart')
		{
			$data = tryPar('Kart');
			switch ($data['Actie'])
			{
				case 'personen_uitschrijven':
					$aantal = self::personen_uitschrijven($data);
					$melding = sprintf(_('%d %s uit de kart gehaald!'), $aantal, $aantal == 1 ? _('persoon') : _('personen'));
					Page::addMelding($melding);
					break;
				case 'personen_mailen':
					return self::personen_mailen($data);
				case 'cies_uitschrijven':
					$aantal = self::cies_uitschrijven($data);
					$melding = sprintf(_('%d %s uit de kart gehaald!'), $aantal, $aantal == 1 ? _('commissie') : _('commissies'));
					Page::addMelding($melding);
					break;
				case 'cies_mailen':
					return self::cies_mailen($data);
				default:
					$body->add(new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext'));
					break;
			}
		}
	
		/** Toon inhoud **/
		$body->add(KartView::overzicht('Kart', 'Kart'));

		/** Spuug alles uit **/
		$page = Page::getInstance()
			->start(_('Kart'))
			->add($body)
			->end();
	}

	/**
	 * Schrijf leden uit
	 * Submethode van overzicht()
	 *
	 * @return het aantal personen wat uitgeschreven is
	 */
	static public function personen_uitschrijven ($data)
	{
		/** Verwerk formulier-data **/
		$ids = array();
		foreach ($data['Geselecteerde'] as $id => $geselecteerd)
			if ($geselecteerd)
				$ids[] = $id;
		$personen = PersoonVerzameling::verzamel($ids);

		Kart::geef()->verwijderPersonen($ids);
		return $personen->aantal();
	}

	/**
	 * Mail leden
	 * Submethode van overzicht()
	 */
	static public function personen_mailen ($data)
	{
		$ids = array();
		foreach ($data['Geselecteerde'] as $id => $geselecteerd)
			if ($geselecteerd)
				$ids[] = $id;

		$personen = PersoonVerzameling::verzamel($ids);
		$personenclone = clone $personen; //We gaan de boel in de foreach aanpassen.
		$missend = new PersoonVerzameling();
		foreach ($personenclone as $persoon)
		{
			if (!$persoon->getEmail())
			{
				$personen->verwijder($persoon);
				$missend->voegtoe($persoon);
			}
		}

		self::mailen($data, $personen, $missend, 'Persoon');
	}

	/**
	 * Schrijf commissies uit
	 * Submethode van overzicht()
	 */
	static public function cies_uitschrijven ($data)
	{
		/** Verwerk formulier-data **/
		$ids = array();
		foreach ($data['GeselecteerdeCie'] as $id => $geselecteerd)
			if ($geselecteerd)
				$ids[] = $id;
		$cies = CommissieVerzameling::verzamel($ids);

		Kart::geef()->verwijderCies($ids);
		return $cies->aantal();
	}

	/**
	 * Mail leden
	 * Submethode van overzicht()
	 */
	static public function cies_mailen ($data)
	{
		$ids = array();
		foreach ($data['GeselecteerdeCie'] as $id => $geselecteerd)
			if ($geselecteerd)
				$ids[] = $id;

		$cies = CommissieVerzameling::verzamel($ids);
		$ciesclone = clone $cies; //We gaan de boel in de foreach aanpassen.
		$missend = new CommissieVerzameling();
		foreach ($ciesclone as $cie)
		{
			if (!$cie->getEmail())
			{
				$cies->verwijder($cie);
				$missend->voegtoe($cie);
			}
		}

		self::mailen($data, $cies, $missend, 'Commissie');
	}

	/**
	 * Mail geadresseerden. Submethode van personen_mailen en cies_mailen.
	 *
	 * @param data formulierdata.
	 * @param aan de geadresseerden, als PersoonVerzameling of CommissieVerzameling.
	 * @param missend de lijst van geadresseerden zonder emailadres.
	 * @param type het type objecten ('Persoon' of 'Commissie') dat gemaild wordt.
	 */
	static public function mailen ($data, Verzameling $aan, Verzameling $missend, $type)
	{
		global $BESTUUR;

		if ($type == 'Persoon' && $aan instanceof PersoonVerzameling)
		{
			$geadresseerdenLijst = implode('<br/>', PersoonVerzamelingView::naamMetLidNrArray($aan));
		}
		else if ($type == 'Commissie' && $aan instanceof CommissieVerzameling)
		{
			$geadresseerdenLijst = CommissieVerzamelingView::kommaLijst($aan, false);
		}
		else
		{
			user_error(sprintf("Ongeldig type van geadresseerdelijst %s, verwacht %s!", gettype($aan), $type));
		}

		/** Buffer voor berichten **/
		$form = true;
		$msg = null;
	
		/** Verwerk formulier-data **/
		$mail = new Email();

		if ($missend->aantal() > 0)
		{
			$ul = new HtmlList();
			foreach ($missend as $missende)
			{
				$ul->add(geefNaam($missende));
			}
			$msg .= new HtmlDiv(array(new HtmlSpan(_('Je hebt geadresseerden geslecteerd zonder emailadres, niet iedereen zal daarom je email ontvangen. Geadresseerden zonder adres waren:')), $ul), 'text-danger strongtext');
		}

		$mail->setTo($aan);

		if ($aan->aantal() == 0)
		{
			if($missend->aantal() == 0)
				$msg = _('Selecteer eerst geadresserden');
			Page::redirectMelding('/Service/Kart', $msg);
		}

		if (tryPar('KartMail'))
		{
			$from = tryPar('from');

			$afzender = NULL;
			if (hasAuth('bestuur')) {
				// Kijk of er een bestuursfunctie is gekozen
				$prefix = KartView::BESTUURFUNCTIE_PREFIX;
				if (substr($from, 0, strlen($prefix)) === $prefix) {
					$functie = substr($from, strlen($prefix));
					if (array_key_exists($functie, $BESTUUR)) {
						$afzender = $BESTUUR[$functie];
					}
				}
			}

			if (is_null($afzender)) {
				// Geen bestuursfunctie

				$cie = Commissie::cieByLogin($from);
				if (is_null($cie)) {
					// Val terug op het e-mailadres van de ingelogde persoon
					$afzender = Persoon::getIngelogd();
				} else if (hasAuth('bestuur') ||
						$cie->hasLid(Persoon::getIngelogd())) {
					// Het ingelogde lid mag vanaf deze commissie mailen
					$afzender = $cie;
				} else {
					// geen rechten om vanaf cie te mailen
					Page::addMelding("Je hebt geen rechten om vanaf deze cie te mailen!", "fout");
				}
			}

			$mail->setFrom($afzender);
			$mail->setReplyTo($data['replyto'])
				 ->setSubject($data['subject'])
				 ->setBody($data['body']);

			$file_error = false;

			// Kijken of er attachments zijn om te uploaden
			if(is_array($_FILES['Kart']['size']['attachments'])
				&& (count($_FILES['Kart']['size']['attachments']) > 0
					&& (count($_FILES['Kart']['size']['attachments']) != 1
						|| $_FILES['Kart']['size']['attachments'][0] != 0))
				)
			{
				for($i = 0; $i < count($_FILES['Kart']['size']['attachments']); ++$i)
				{
					if($_FILES['Kart']['error']['attachments'][$i])
					{
						$file_error .= new HtmlSpan(sprintf(_('Uploaden van %s niet gelukt'), $_FILES['Kart']['name']['attachments'][$i]));
						break;
					}
					$mail->setAttachmentFromFile($_FILES['Kart']['type']['attachments'][$i]
							, $_FILES['Kart']['tmp_name']['attachments'][$i]
							, $_FILES['Kart']['name']['attachments'][$i]
						);
				}
			}

			if ($mail->valid() && !$file_error)
			{
				//Genereer de forward voor de afzender
				$kopie = clone $mail;
				$fwdBody = _("Hoi") . ' ' . PersoonView::naam(Persoon::getIngelogd()) . ",<p>"
					. _("Er is door jou een mailtje verstuurd aan:") . "<br />"
					. $geadresseerdenLijst
					. "<p>---Mailbody:---<p>" . $kopie->getBody() . "<p>---Einde Mailbody---<p>" . _("Groetjes, de WebCie!");
				$kopie->setTo($afzender)
				      ->setSubject($data['subject'] . " (fwd)")
				      ->setBody($fwdBody);

				$mail->send();
				$kopie->send();
				Page::redirectMelding('/Service/Kart', _('Mail is succesvol verzonden!'));
			}
			else
			{
				$msg .= $file_error;
				$msg .= new HtmlSpan(_('Je mail is niet valide. Heb je een onderwerp en mailbody ingevoerd? Merk op dat je daarnaast zelf ook een valide emailadres moet hebben om te kunnen mailen.'), 'text-danger strongtext');
			}
		}

		KartView::mailForm($aan, $mail, $form, $msg, $type);
	}

	/**
	 * Voeg meer mensen aan je kart toe
	 */
	static public function toevoegen ()
	{
		/** Prepareerd output **/
		$body = new HtmlDiv();

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'KartToevoegen')
		{
			if (KartView::processNieuwForm()) {
				Page::redirectMelding('.', _("Je hebt nu mensen aan je kart toegevoegd."));
				return;
			} else {
				$body->add(new HtmlSpan(_("Niemand in de kart gestopt.")));
			}
		}

		$body->add(KartView::nieuwForm('KartToevoegen'));

		/** Spuug alles uit **/
		$div = new HtmlDiv();
		$div->add($header = new HtmlHeader(2, _("Mensen toevoegen aan kart")))
			->add($body);

		$page = Page::getInstance()
			->start(_("Mensen toevoegen"))
			->add($div)
			->end();
	}

	static public function kartOpslaan ()
	{
		global $auth;

		$body = new HtmlDiv();
		$msg = null;
		if(Token::processNamedForm() == 'KartOpslaan')
		{
			$naam = tryPar('naam',null);
			if(!$naam) {
				Page::addMelding(_("Geef een naam voor deze kart mee!"), 'fout');
			} else {
				if(PersoonKart::geef($auth->getLidnr(),$naam)) {
					Page::addMelding(_("Er bestaat al een kart met deze naam!"), 'fout');
				} else {
					$persoonkart = new PersoonKart(Persoon::getIngelogd(),$naam);
					$kart = Kart::geef();
					$personen = $kart->getPersonen();
					$cies = $kart->getCies();
					$media = $kart->getMedia();
					$kvverz = new KartVoorwerpVerzameling();
					foreach($personen as $p) {
						$kv = new KartVoorwerp($persoonkart);
						$kv->setPersoon($p);
						$kvverz->voegtoe($kv);
					}
					foreach($cies as $c) {
						$kv = new KartVoorwerp($persoonkart);
						$kv->setCommissie($c);
						$kvverz->voegtoe($kv);
					}
					foreach($media as $m) {
						$kv = new KartVoorwerp($persoonkart);
						$kv->setMedia($m);
						$kvverz->voegtoe($kv);
					}
					$persoonkart->opslaan();
					$kvverz->opslaan();
					Page::redirectMelding('/Service/Kart',_("Je kart is nu opgeslagen"));
				}
			}
		}
		
		$body->add(PersoonKartView::opslaanForm());

		$div = new HtmlDiv();
		$div->add($body);

		$page = Page::getInstance()
			->start(_("Kart opslaan"))
			->add($div)
			->end();
	}

	static public function kartLaden ()
	{
		global $auth;

		$naam = tryPar('naam');
		$toevoegen = (bool)tryPar('toevoegen', false);
		
		if(is_null($naam))
			return;

		$pk = PersoonKart::geef($auth->getLidnr(),$naam);
		if(is_null($pk))
			return;

		$pk->maakKart($toevoegen);

		Page::redirectMelding('/Service/Kart',sprintf(_('Kart %s geladen'),$naam));
	}

	static public function kartInfo ()
	{
		global $auth;

		$naam = tryPar('naam');
		if(!$naam)
			return false;

		$pk = PersoonKart::geef($auth->getLidnr(),$naam);
		if(!$pk)
			return;

		PersoonKartView::info($pk);
	}

	static public function kartVerwijderen()
	{
		global $auth;

		$naam = tryPar('naam');
		if(!$naam)
		{
			return responseUitStatusCode(403);
		}

		$pk = PersoonKart::geef($auth->getLidnr(),$naam);
		if(!$pk)
		{
			return responseUitStatusCode(403);
		}

		$returnVal = $pk->verwijderen();

		if(!is_null($returnVal)) {
			return Page::responseRedirectMelding('/Service/Kart', $returnVal);
		}

		return Page::responseRedirectMelding('/Service/Kart',sprintf(_('Kart %s verwijderd'),$naam));
	}
}
