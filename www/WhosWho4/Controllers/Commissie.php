<?php

abstract class CommissieController
{
	/**
	 * @site{/Vereniging/Commissies/Nieuw}
	 */
	static function nieuweCommissie()
	{
		requireAuth('bestuur');

		$cie = new Commissie();
		$cie->setDatumBegin(new dateTimeLocale());

		$show_error = false;

		if (Token::processNamedForm() == 'CieWijzig')
		{
			CommissieView::processForm($cie, 'nieuw');
			if($cie->valid())
			{
					$cie->opslaan();//Opslaan die hap
					Page::redirectMelding($cie->systeemUrl() . '/Info'
						, _("De gegevens zijn opgeslagen, profit!"));
			}
			else
			{
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
		}

		$page = Page::getInstance();
		$page->start(_("Nieuwe commissie"));

		$form = HtmlForm::named('CieWijzig');
		$form->add(CommissieView::createForm($cie, $show_error, 'nieuw')
				 , HtmlInput::makeFormSubmitButton(_('Aanmaken')));

		$page->add($form);
		$page->setLastMod($cie);
		$page->end();
	}

	/**
	 * Returneert een array met geselecteerde commissie
	 * $args = array(0 => 'cie_input')
	 *
	 * @site{/Vereniging/Commissies/ *}
	 */
	static public function commissieEntry ($args)
	{
		$cies = explode('_',$args[0]);
		$cielogin = '';
		$cienaam = '';
		$first = true;
		foreach($cies as $c) {
			$cie = Commissie::inputToCie($c);
			if (!$cie) return false;
			if($first) {
				$cielogin .= $cie->getLogin();
				$cienaam .= $cie->getNaam();
			} else {
				$cielogin .= '_'.$cie->getLogin();
				$cienaam .= ' & '.$cie->getNaam();
			}
			$first = false;
		}

		return array('name' => $cielogin,
					'displayName' => $cienaam,
					'access' => true);
	}

	/** Shop functies **/
	/**
	 * @site{/Vereniging/Commissies/ * /Shop}
	 */
	public static function shop()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		if(!$cie->magBekijken() || !hasAuth('ingelogd'))
		{
			spaceHttp(403);
		}

		Kart::geef()->voegCieToe($cie->geefID());
		spaceRedirect('/Service/Kart');
	}
	/**
	 * @site{/Vereniging/Commissies/ * /UnShop}
	 */
	public static function unShop()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		Kart::geef()->verwijderCie($cie->geefID());
		spaceRedirect('/Service/Kart');
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Info}
	 */
	static function infoPagina()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());

		$bestaatLogin = false;

		if(hasAuth('bestuur'))
		{
			try
			{
				$conn = new SysteemApi();

				if(!$bestaatLogin = $conn->bestaatLogin($cie->getLogin()))
				{
					Page::addMelding(_('De naam van het systeemaccount op de website bestaat niet of klopt niet, fix dit!'), 'fout');
				}
			}
			catch (IPALigtEruitException $e)
			{
				Page::addMelding(_('Er is geen connectie met ldap mogelijk! Schop de Sysop!'), 'fout');
			}
		}

		CommissieView::infoPagina($cie, $bestaatLogin);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Wijzig}
	 */
	static function wijzigPagina()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		if (!$cie->magWijzigen())
			spaceHttp(403);

		$show_error = false;

		//verwerk 't allemaal
		if (Token::processNamedForm() == 'CieWijzig')
		{
			CommissieView::processForm($cie, 'viewWijzig');
			if($cie->valid())
			{
					$cie->opslaan();//Opslaan die hap
					Page::redirectMelding($cie->systeemUrl() . '/Info', _("De gegevens zijn opgeslagen, profit!"));
			}
			else
			{
				$show_error = true;
				Page::addMelding(_('Er waren fouten.'), 'fout');
			}
		}

		$page = CommissieView::pageLinks($cie);
		$page->start(_('Commissie wijzigen'));

		$form = HtmlForm::named('CieWijzig');
		$form->add(CommissieView::createForm($cie, $show_error, 'viewWijzig')
				 , HtmlInput::makeFormSubmitButton(_('Wijzig')));

		$page->add(new HtmlParagraph($btn_group = new HtmlDiv(null, 'btn-group')));
		$btn_group->add(HtmlAnchor::button($cie->systeemUrl() . '/Info',_("Terug naar de commissie")));
		$btn_group->add(HtmlAnchor::button($cie->systeemUrl() . '/WijzigFoto',_("Wijzig de commissiefoto")));

		$ciefoto = CommissieFoto::zoekHuidig($cie);
		if($ciefoto && $cie->magWijzigen()) {
			$btn_group->add(HtmlAnchor::button($cie->systeemUrl() . '/VerwijderCommissieFoto', _('Commissiefoto verwijderen'), null, null,null, 'danger'));
		}

		$page->add($form);
		$page->setLastMod($cie);
		$page->end();
	}

	/**
	 * @site{/Vereniging/Commissies/ * /WijzigFoto}
	 */
	public static function wijzigFoto()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		if (!$cie->magWijzigen())
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'fotoForm')
		{
			$userfile = $_FILES["userfile"];

			$mediafile = Media::uploaden($userfile, true);

			if(is_array($mediafile) && count($mediafile) != 0)
			{
				Page::addMeldingArray($mediafile, 'fout');
			}
			else
			{
				$huidig = CommissieFoto::zoekHuidig($cie);
				if($huidig) {
					$huidig->setStatus('OUD');
					$huidig->opslaan();
				}

				$commissiefoto = new CommissieFoto($cie, $mediafile);
				$commissiefoto->setStatus('HUIDIG');
				$commissiefoto->opslaan();

				$tag = new Tag();
				$tag->setMedia($mediafile);
				$tag->setCommissie($cie);
				$tag->setTagger(Persoon::getIngelogd());
				$tag->opslaan();

				Page::redirectMelding($cie->systeemUrl() . '/Info', _('Gelukt!'));
			}
		}

		CommissieView::wijzigFoto($cie);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /verwijderFoto}
	 */
	public static function verwijderFoto()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		if (!$cie || !$cie->magWijzigen())
			spaceHttp(403);

		$foto = CommissieFoto::zoekHuidig($cie);
		if(!$cie)
			spaceHttp(403);

		if(Token::processNamedForm() == 'fotoVerwijderen')
		{
			$foto->verwijderen();

			Page::redirectMelding($cie->systeemUrl() . '/Info', _('Gelukt!'));
		}

		CommissieView::verwijderFoto($cie);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Verwijder}
	 */
	static function verwijderPagina()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());

		switch(Token::processNamedForm()) 
		{
			case 'CommissieVerwijderen':
				$returnVal = $cie->verwijderen();
				if(!is_null($returnVal)) {
					Page::addMeldingArray($returnVal, 'fout');
				} else {
					Page::redirectMelding("/Vereniging/Commissies", sprintf(_("%s[VOC: commissienaam] is verwijderd uit de Databaas."), CommissieView::waardeNaam($cie)));
					return;
				}
				break;
			case NULL:
			default:
				break;
		}

		$page = Page::getInstance();
		$page->start();
		$page->add(CommissieView::verwijderForm($cie));
		$page->end();
	}

	/**
	 * @site{/Vereniging/Commissies/ * /GeefSysteemAccount}
	 */
	static public function geefSysteemAccount()
	{
		requireAuth('bestuur');

		$cie = Commissie::inputToCie(vfsVarEntryName());

		if(Token::processNamedForm() == 'GeefSysteemAccount')
		{
			$login = tryPar('login');

			try
			{
				$conn = new SysteemApi();
				if($conn->aliasCheck($login))
				{
					if(!DEBUG && !DEMO)
					{
						$page = Page::getInstance();
						$page->addFooterJs(Page::minifiedFile('cieadd', 'js'));
						$page->start(_('Cieadd'));

						$page->add($div = new HtmlDiv($cie->geefID(), 'cie-id'));
						$div->setCssStyle('display: none;');
						$page->add($div = new HtmlDiv(tryPar('login'), 'login-name'));
						$div->setCssStyle('display: none;');
						$page->add($div = new HtmlDiv($cie->systeemUrl(), 'cieurl'));
						$div->setCssStyle('display: none;');

						$page->add(new HtmlParagraph(null, 'paragraph'));

						$page->add(new HtmlDiv(null, 'rotating-plane'));

						$page->end();
						return;
					}
					else
					{
						Page::setMelding(_('Omdat je niet op live zit is er niets gedaan!'), 'waarschuwing');
						Page::redirect($cie->systeemUrl(), 0);
					}
				}
				else
				{
					Page::addMelding(_('De accountnaam mag niet gebruikt worden'), 'fout');
				}
			}
			catch (IPALigtEruitException $e)
			{
				Page::addMelding(_("Er is geen verbinding met het systeem!"), 'fout');
			}
		}

		CommissieView::geefSysteemAccount($cie);
	}

	/**
	 * @site{/Ajax/Cie/CieaddUser}
	 */
	static public function cieaddUser()
	{
		requireAuth('bestuur');

		if(DEBUG || DEMO)
			spaceHttp(403);

		if(!$cie = Commissie::geef(tryPar('id')))
			spaceHttp(403);

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->cieaddUser($cie, $login);

		if(!$output)
		{
			$return = array('status' => 'OK', 'fout' => '');
		}
		else
		{
			// $output is een array van regels output
			$return = array('status' => 'FOUT', 'fout' => $output);
		}
		return new JSONResponse($return);
	}

	/**
	 * @site{/Ajax/Cie/CieaddSudo}
	 */
	static public function cieaddSudo()
	{
		requireAuth('bestuur');

		if(DEBUG || DEMO)
			spaceHttp(403);

		if(!$cie = Commissie::geef(tryPar('id')))
			spaceHttp(403);

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->cieaddSudo($cie, $login);

		if(!$output)
		{
			$return = array('status' => 'OK', 'fout' => '');
		}
		else
		{
			$return = array('status' => 'FOUT', 'fout' => $output);
		}
		return new JSONResponse($return);
	}

	/**
	 * @site{/Ajax/Cie/CieaddHomedir}
	 */
	static public function cieaddHomedir()
	{
		requireAuth('bestuur');

		if(DEBUG || DEMO)
			spaceHttp(403);

		if(!$cie = Commissie::geef(tryPar('id')))
			spaceHttp(403);

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->cieaddHomedir($cie, $login);

		if(!$output)
		{
			$return = array('status' => 'OK', 'fout' => '');
		}
		else
		{
			$return = array('status' => 'FOUT', 'fout' => $output);
		}
		return new JSONResponse($return);
	}

	/**
	 * @site{/Ajax/Cie/CieaddSendmail}
	 */
	static public function cieaddSendmail()
	{
		requireAuth('bestuur');

		if(DEBUG || DEMO)
			spaceHttp(403);

		if(!$cie = Commissie::geef(tryPar('id')))
			spaceHttp(403);

		$login = tryPar('login');

		sendmail('webcie@a-eskwadraat.nl', 'sysop@a-eskwadraat.nl', 'Nieuw account ' . $login . ' aangemaakt', 'Best Sysop,

Er is door ' . PersoonView::naam(Persoon::getIngelogd()) . ' een nieuw account ' . $login . ' aangemaakt voor de commissie ' . CommissieView::waardeNaam($cie) . '.

Groetjes
De website');
	}

	/**
	 * @site{/Ajax/Cie/getCieAnchor}
	 */
	static public function getCieAnchor()
	{
		if(!$cie = Commissie::cieByLogin(tryPar('cielogin')))
		{
			$result = array('cielogin' => tryPar('cielogin'), 'a' => 'Cielogin '.tryPar('cielogin').' niet gevonden!');
		}
		else
		{
			$a = CommissieView::makeLink($cie);
			$result = array('cielogin' => tryPar('cielogin'), 'a' => $a->makeHtml());
		}
		return new JSONResponse($result);
	}

	/**
	 * @site{/Vereniging/Commissies/Categorie/index.html}
	 */
	static public function categorieen()
	{
		requireAuth('bestuur');

		$categorieVerzameling = CommissieCategorieVerzameling::geefAlle();

		CommissieCategorieVerzamelingView::categorieen($categorieVerzameling);
	}

	/**
	 * @site{/Vereniging/Commissies/Categorie/ *}
	 */
	static public function categorieEntry($args)
	{
		requireAuth('bestuur');

		$cieCatId = $args[0];
		$cieCat = CommissieCategorie::geef($cieCatId);
		if(!$cieCat)
			spaceHttp(403);

		return array('name' => $cieCat->geefID(),
					'displayName' => $cieCat->geefID(),
					'access' => true);
	}

	/**
	 * @site{/Vereniging/Commissies/Categorie/ * /index.html}
	 */
	static public function categorie()
	{
		requireAuth('bestuur');

		$cieCatId = vfsVarEntryName();
		if(!($cieCat = CommissieCategorie::geef($cieCatId)))
			spaceHttp(403);

		CommissieCategorieView::categorie($cieCat);
	}

	/**
	 * @site{/Vereniging/Commissies/Categorie/Nieuw}
	 */
	static public function nieuweCategorie()
	{
		self::wijzigenCategorie(true);
	}
	/**
	 * @site{/Vereniging/Commissies/Categorie/ * /Wijzig}
	 */
	static public function wijzigCategorie()
	{
		self::wijzigenCategorie(false);
	}

	static public function wijzigenCategorie($nieuw = false)
	{
		requireAuth('bestuur');

		$cieCat = new CommissieCategorie();

		if(!$nieuw)
		{
			$cieCatId = vfsVarEntryName();
			if(!($cieCat = CommissieCategorie::geef($cieCatId)))
				spaceHttp(403);
		}

		$show_error = false;

		if(Token::processNamedForm() == 'Categorie')
		{
			CommissieCategorieView::processForm($cieCat);

			if($cieCat->valid())
			{
				$cieCat->opslaan();
				Page::redirectMelding('/Vereniging/Commissies/Categorie/', _('Gelukt!'));
			}
			else
			{
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
		}

		CommissieCategorieView::wijzig($cieCat, $show_error, $nieuw);
	}

	/**
	 * @site{/Vereniging/Commissies/Categorie/ * /Verwijder}
	 */
	static public function verwijderCategorie()
	{
		requireAuth('bestuur');

		$cieCatId = vfsVarEntryName();
		if(!($cieCat = CommissieCategorie::geef($cieCatId)))
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'CommissieCategorieVerwijderen') {
			$cieCat->verwijderen();

			Page::redirectMelding('/Vereniging/Commissies/Categorie', _('Gelukt!'));
		}

		$page = Page::getInstance()
			->start(sprintf(_('%s verwijderen'), CommissieCategorieView::waardeNaam($cieCat)))
			->add(CommissieCategorieView::verwijderForm($cieCat))
			->end();
	}
}
