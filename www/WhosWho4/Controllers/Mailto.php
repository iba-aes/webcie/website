<?php

abstract class Mailto_Controller
{
	static public function cieEntry ($args)
	{
		$cie = Commissie::cieByLogin($args[0]);
		if (!$cie instanceof Commissie) return false;
		return array('name' => $args[0], 'access' => true);
	}
	
	static public function mailto ()
	{
		global $request, $BMDB;

		$cie = Commissie::cieByLogin(vfsVarEntryName());

		$tekst = <<<HEREDOC
	Beste mensen,

	Er is een webformulier ingevuld voor jullie commissie. Hieronder staan de
	ingevulde waardes.

	De WebCie (www@A-Eskwadraat.nl)

	--- begin formulier ---

HEREDOC;

		if(!$request->server->get('REMOTE_HOST', null)) {
			$rhost = gethostbyaddr($request->server->get('REMOTE_ADDR'));
		} else {
			$rhost = $request->server->get('REMOTE_HOST');
		}

		if($request->server->get('HTTP_REFERER'))
		{
			$tekst .= "bronpagina = ".$request->server->get('HTTP_REFERER')."\n";
		}
		$tekst .= "ip-adres = $rhost (".$request->server->get('REMOTE_ADDR').")\n\n";

		foreach ($request->query->all() as $key => $value)
		{
			if($key == "nextpage") {
				$nextpage =  $value; //We eisen dat de nextpage is geset zodat www.a-es2.nl/$value werkt.
				continue;
			}
			if($key == "onderwerp") {
				$onderwerp = $value;
				continue;
			}
			if ($key == "reply") {
				$reply =$value;
				continue;
			}
			if($key == "email" && preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $value)) {
				$email = $value;
			}
			$tekst .= "$key = " . preg_replace('/\n/', "\n+ ", $value) . "\n";
		}

		$tekst .= "--- einde formulier ---\n";

		$errs = array();
		if (count($errs) > 0)
		{
			$url = $request->server->get('HTTP_REFERER');
			if (strpos($url, '?') > 0)
			{
				preg_match('/^(.*)\?.*$/', $url, $matches);
				$url = $matches[1];
			}
			$url = $url . '?' . implode('&', $errs);

			Page::redirect($url);
		}

		if(empty($onderwerp)) {
			$onderwerp = $cie->getEmail() .": Webform ingevuld!";
		}

		if (!isset($nextpage))
		{
			echo _("Oops, geen gegevens meegestuurd! Er ging iets mis, voornamelijk omdat de Cie die je wilt mailen geen nextpage heeft ingesteld!.") . "<br>";
			echo "Email " . $cie->getEmail() . " " . _("(of anders www@A-Eskwadraat)");
			exit;
		}

	/*	preg_match("/Vereniging\/Commissies\/(.*)\/(.*)\.html/", $reply, $matches);
		if (!$replymail = $BMDB->q("MAYBETUPLE SELECT `content`, `content_eng` FROM `benamite` WHERE
					`parent` = (SELECT `id` FROM `benamite` WHERE `name` = %s) AND
					`name` = %s", $matches[1], $matches[2] . '.html'))
		{
			echo "$reply <br />";
			echo _("Oops, geen reply-mail gevonden! Er ging iets mis.") . "<br>";
			echo "Email " . $cie->getEmail() . " " . _("(of anders www@A-Eskwadraat)");
			exit;
		}*/

		//Selecteerd verschilende vertalingen van de replymail
	/*	if (getLang() == 'nl')
			$replybody = $replymail['content'];
		elseif (!empty($replymail['content_eng']))
			$replybody = $replymail['content_eng'];
		else
			$replybody = "<em>No translation available, dutch content follows.</em><br />"
						. $replymail['content'];*/
		sendmail('Webform <www@A-Eskwadraat.nl>'
				, $cie->getEmail(), $onderwerp, $tekst);
		spaceRedirect(HTTPS_ROOT.$nextpage);

		/*sendmail('Webform <www@A-Eskwadraat.nl>'
				, $email, $onderwerp, $replybody, $cie->getEmail(), '', $cie->getEmail(), true);*/
		}
}
