<?php

abstract class PapierMolen_Controller
{
	static public function overzicht()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$studie = tryPar('studie', '');

		$pms = PapierMolenVerzameling::geefNietVerlopen($studie);

		PapierMolenVerzamelingView::overzicht($pms);
	}

	static public function papierMolenEntry($args)
	{
		$pm = PapierMolen::geef($args[0]);
		if(!$pm)
			return array('name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array('name' => $pm->geefID()
			, 'displayName' => _("Boek")
			, 'access' => true);
	}

	static public function papierMolen()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$id = vfsVarEntryName();
		$pm = PapierMolen::geef($id);

		if(!$pm)
			spaceHttp(403);

		PapierMolenView::papierMolen($pm);
	}

	static public function papierMolenContact()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$id = vfsVarEntryName();
		$pm = PapierMolen::geef($id);

		if(!$pm)
			spaceHttp(403);

		if(Token::processNamedForm() == "contactFormulier")
		{
			$mail = new Email();
			$mail->setTo($pm->getLid());
			$mail->setFrom(Persoon::getIngelogd());
			$mail->setSubject(_("Bericht via de PapierMolen"));
			$body = sprintf("%s heeft je een bericht gestuurd via de PapierMolen "
				. "op de A–Eskwadraatsite.\n\n<br><br>"
				. "Boek: %s / %s\n\n<br><br>", LidView::naam(Persoon::getIngelogd())
				, PapierMolenView::waardeEan($pm), PapierMolenView::waardeTitel($pm));
			$body .= tryPar('bericht');
			$mail->setBody($body);
			$mail->send();

			Page::redirectMelding($pm->url(), _("Bericht gestuurd!"));
		}

		PapierMolenView::contact($pm);
	}

	static public function nieuw()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$pm = new PapierMolen(new DateTimeLocale());

		$show_error = false;

		if(Token::processNamedForm() == "nieuwForm")
		{
			PapierMolenView::processWijzigForm($pm);
			$pm->setLid(Persoon::getIngelogd());

			if($pm->valid()) {
				$pm->opslaan();
				Page::redirectMelding($pm->url(), _("Boek aangeboden!"));
			} else {
				Page::addMelding(_("Er zijn fouten"), 'fout');
				$show_error = true;
			}
		}

		PapierMolenView::wijzigForm($pm, true, $show_error);
	}

	static public function papierMolenVerwijderen()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$id = vfsVarEntryName();
		$pm = PapierMolen::geef($id);

		if(!$pm)
			spaceHttp(403);

		if($pm->getLid() != Persoon::getIngelogd() && !hasAuth('bestuur'))
			spaceHttp(403);

		if(Token::processNamedForm("PapierMolen::Verwijderen"))
		{
			$ret = $pm->verwijderen();
			if(is_null($ret))
				Page::redirectMelding(PAPIERMOLENBASE, _("Aangeboden boek verwijderd"));
			else
				Page::addMelding($ret, 'fout');
		}

		PapierMolenView::verwijder($pm);
	}

	static public function papierMolenWijzigen()
	{
		if(!Persoon::getIngelogd())
			spaceHttp(403);

		$id = vfsVarEntryName();
		$pm = PapierMolen::geef($id);

		if(!$pm)
			spaceHttp(403);

		if($pm->getLid() != Persoon::getIngelogd() && !hasAuth('bestuur'))
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm("wijzigForm"))
		{
			PapierMolenView::processWijzigForm($pm);

			if($pm->valid()) {
				$pm->opslaan();
				Page::redirectMelding($pm->url(), _("Aangeboden boek gewijzigd!"));
			} else {
				Page::addMelding(_("Er zijn fouten"), 'fout');
				$show_error = true;
			}
		}

		PapierMolenView::wijzigForm($pm, false, $show_error);
	}
}
