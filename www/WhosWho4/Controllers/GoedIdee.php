<?php

declare(strict_types=1);
setZeurmodus();

/**
 * Class GoedIdee_Controller
 */
final class GoedIdee_Controller extends Controller
{
	/**
	 * Variabele entry voor een goed idee.
	 * @site{/Service/GoedIdeeWeb/ * /}
	 */
	static public function goedIdeeEntry($args)
	{
		$idee = GoedIdee::geef($args[0]);
		if (!($idee instanceof GoedIdee))
		{
			return false;
		}
		return array(
			'name' => $idee->geefID(),
			'displayName' => GoedIdeeView::waardeTitel($idee),
			'access' => $idee->magBekijken()
		);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/Melden}
	 */
	static public function nieuwGoedIdee()
	{

		$goedIdee = new GoedIdee(new DateTimeLocale());
		$msg = _('Voer hieronder een nieuw goed idee in. Controleer of jouw idee al niet door iemand anders is ingevoerd.');
		$toon_fouten = false;
		$bericht = '';

		if (Token::processNamedForm() === 'NieuwGoedIdee')
		{
			$ingelogd = Persoon::getIngelogd();

			GoedIdeeView::processForm($goedIdee);
			$moment = new DateTimeLocale();
			$bericht = tryPar('GoedIdee[Bericht]');
			$melder = tryPar('Melder');

			if (!$bericht)
			{
				Page::addMelding(_('Vul een omschrijving in.'), 'fout');
				$toon_fouten = true;
			}
			else
			{
				if ($melder === null)
				{
					$goedIdee->setMelder($ingelogd);
				}
				else
				{
					$goedIdee->setMelder($melder);
				}

				$goedIdee->setOmschrijving($bericht);

				if ($goedIdee->valid())
				{	
					$ideeBericht = GoedIdeeBericht::eersteBericht($goedIdee, $moment);
					$goedIdee->opslaan();
					$ideeBericht->opslaan();
					GoedIdeeBerichtView::verstuurMails($ideeBericht, true);
					return Page::responseRedirectMelding($goedIdee->url(), _('Het idee is ingevoerd!'));
				}
				else
				{
					Page::addMelding(_('Er waren fouten.'), 'fout');
					$toon_fouten = true;
				}
			}
		}

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('style_goedideeweb.css', 'css'))
			->addFooterJS(Page::minifiedFile('knockout.js', 'js'))
			->start(_('Nieuw Goed Idee melden'))
			->add(new HtmlDiv($msg, 'bs-callout bs-callout-info'))
			->add(GoedIdeeView::nieuwGoedIdee($goedIdee, $bericht, $toon_fouten));
			return new PageResponse($page);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/ * /index.html}
	 */
	static public function toonGoedIdee()
	{
		global $request;

		$goedIdeeID = vfsVarEntryName();
		$goedIdee = GoedIdee::geef($goedIdeeID);

		if (!$goedIdee->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		$ingelogd = Persoon::getIngelogd();
		$omgekeerd = false;
		if ($ingelogd)
		{
			$voorkeur = $ingelogd->getVoorkeur();
			$omgekeerd = $voorkeur->getGoedIdeeSortering() == 'DESC';
		}

		$toon_fouten = false;
		$msg = NULL;

		if (Token::processNamedForm() === 'NieuwGoedIdeeBericht')
		{

			$goedIdeeBericht = new GoedIdeeBericht($goedIdee);

			GoedIdeeBerichtView::processForm($goedIdeeBericht);
			$goedIdeeBericht->setMelder($ingelogd);

			if (!$goedIdeeBericht->valid())
			{
				$msg = new HtmlSpan(_('Er waren fouten.'), 'text-danger strongtext');
				$toon_fouten = true;
			}
			else
			{
				$goedIdeeBericht->opslaan();
				$goedIdee->voegGoedIdeeBerichtToe($goedIdeeBericht);
				$goedIdee->opslaan();

				Page::addMelding(_('De reactie is verstuurd.'), 'succes');
			}
		}

		if ($msg)
		{
			Page::addMelding($msg, 'fout');
		}

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('style_goedideeweb.css', 'css'))
			->addFooterJS(Page::minifiedFile('knockout.js', 'js'))
			->addFooterJS(Page::minifiedFile('goedideebericht.js', 'js'))
			->start(sprintf(_('Goed Idee %s'), GoedIdeeView::waardeGoedIdeeID($goedIdee)));

		if ($request->server->get('HTTP_REFERER', null) || tryPar('referer'))
		{
			$page->add(HtmlAnchor::button(tryPar('referer', $request->server->get('HTTP_REFERER')), _('Terug naar waar je vandaan kwam')));
		}


		$viewIdee = GoedIdeeView::toon($goedIdee);
		$viewReacties = GoedIdeeView::toonReacties($goedIdee, $omgekeerd);

		if ($goedIdee->magReageren())
		{
			$viewNieuwBericht = GoedIdeeBerichtView::nieuwBericht($goedIdee
				, $goedIdee->getTitel()
				, $goedIdee->getStatus()
				, ''
				, $toon_fouten
			);

			if ($omgekeerd)
			{
				$page->add($viewIdee);
				$page->add(new HtmlHR());
				$page->add($viewNieuwBericht);

				$page->add($viewReacties);
			}
			else
			{
				$page->add($viewIdee);
				$page->add($viewReacties);
				$page->add(new HtmlHR());
				$page->add($viewNieuwBericht);
			}
		}
		else
		{
			$page->add($viewIdee);
			$page->add($viewReacties);
		}
		return new PageResponse($page);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/ * /Publiek}
	 */
	static public function publiekIdee()
	{
		$idee = GoedIdee::geef(vfsVarEntryName());
		$idee->setPubliek(true);
		$idee->opslaan();

		$melding = _('Dit goede idee is nu openbaar.');

		return self::refererURL($melding);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/ * /Verborgen}
	 */
	static public function verbergIdee()
	{
		$idee = GoedIdee::geef(vfsVarEntryName());
		$idee->setPubliek(false);
		$idee->opslaan();

		$melding = _('Dit goede idee is nu verborgen.');

		return self::refererURL($melding);
	}
	/**
	 * @site{/Service/GoedIdeeWeb/InverteerGoedIdeeSortering}
	 */
	static public function inverteerGoedIdeeSortering()
	{
		$voorkeur = Persoon::getIngelogd()->getVoorkeur();
		$voorkeur->inverteerGoedIdeeSortering();
		$voorkeur->opslaan();

		$melding = sprintf(_('Goede Ideeën worden vanaf nu %s[VOC: sortering, cq aflopend etc] gesorteerd.'),
			PersoonVoorkeurView::waardeGoedIdeeSortering($voorkeur));

		return self::refererURL($melding);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/index.html}
	 */
	static public function home()
	{
		$ingelogd = Persoon::getIngelogd();

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('style_goedideeweb.css', 'css'));
		$page->start('Goede Ideeën')
			->add(GoedIdeeVerzamelingView::toonInfoPagina($ingelogd));
		return new PageResponse($page);
	}

	/**
	 * @site{/Service/GoedIdeeWeb/Zoeken}
	 */
	static public function zoeken()
	{
		global $request;

		$ideeAantal = 0;
		$tabel = '';
		$zoekterm = tryPar('zoekterm', '');

		$status = tryPar('status');
		$offset = (int)tryPar('limit', '0_0');

		if ($offset < 0)
		{
			$offset = 0;
		}

		if ($zoekterm || ($status && tryPar('submit')))
		{
			$ideeAantal = GoedIdeeVerzameling::zoeken($zoekterm, $status, null, true);
			$ideeen = GoedIdeeVerzameling::zoeken($zoekterm, $status, $offset);
			$tabel = GoedIdeeVerzamelingView::tabel($ideeen);
		}

		$zoekForm = GoedIdeeVerzamelingView::zoekForm($zoekterm, $status);

		$prevparams = array_merge($request->query->all(), array("offset" => max($offset - GOEDIDEEWEB_IDEE_BIJ_ZOEKEN, 0)));
		$nextparams = array_merge($request->query->all(), array("offset" => $offset + GOEDIDEEWEB_IDEE_BIJ_ZOEKEN));

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('style_goedideeweb.css', 'css'));
		$page->start(_('Zoeken naar goede ideeën'))
			->add(new HtmlDiv($zoekForm))
			->add($ideeAantal ? new HtmlParagraph(sprintf(_('%d[VOC: aantal ideeën] goede ideeën gevonden'), $ideeAantal)) : NULL)
			->add($ideeAantal ? limietLinks($ideeAantal, GOEDIDEEWEB_IDEE_BIJ_ZOEKEN) : NULL)
			->add($tabel);
		return new PageResponse($page);
	}

	static private function refererURL($melding = null)
	{
		global $request;


		if ($request->server->get('HTTP_REFERER', null))
		{	
			return Page::responseRedirectMelding($request->server->get('HTTP_REFERER', null), $melding);
		}
		else
		{
			return Page::responseRedirectMelding(GOEDIDEEWEBBASE, $melding);
		}
	}

}
