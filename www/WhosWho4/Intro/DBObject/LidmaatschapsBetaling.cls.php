<?
class LidmaatschapsBetaling
	extends LidmaatschapsBetaling_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	/**
	 * @param a Lid ( Lid OR Array(lid_contactID) OR lid_contactID )
	 * @param soort De waarde van het veld soort.
	 */
	public function __construct($a, $soort=NULL)
	{
		parent::__construct($a); // LidmaatschapsBetaling_Generated

		$this->setSoort($soort);
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		$volgorde[] = 'Soort';
		return $volgorde;
	}

	public function checkRekeningnummer()
	{
		$check = parent::checkRekeningnummer();

		if($check) {
			return $check;
		}

		$waarde = $this->getRekeningnummer();

		if($this->getSoort() == 'MACHTIGING' && empty($waarde)) {
			return _('Dit is een verplicht veld');
		}

		return False;
	}

	public function checkRekeninghouder()
	{
		$check = parent::checkRekeninghouder();

		if($check) {
			return $check;
		}

		$waarde = $this->getRekeninghouder();

		if($this->getSoort() == 'MACHTIGING' && empty($waarde)) {
			return _('Dit is een verplicht veld');
		}

		return False;
	}

	/**
	 * Geef de waarden die de gebruiker mag kiezen voor het veld soort.
	 *
	 * In tegenstelling tot enumsSoort zijn er bepaalde opties weggefilterd,
	 * afhankelijk van rechten en instellingen van het bestuur.
	 *
	 * @see enumsSoort
	 *
	 * @param string|null $waarde
	 * De huidig geselecteerde waarde van het veld soort.
	 *
	 * @return string[]
	 */
	public static function beschikbareEnumsSoort($waarde = null)
	{
		if (hasAuth('bestuur'))
		{
			// Bestuur mag alles uitkiezen.
			return static::enumsSoort();
		}

		// Anders moeten we in het register kijken welke mogen.
		$alleEnums = static::enumsSoort();
		$beschikbareEnums = [];
		foreach ($alleEnums as $soort)
		{
			$registerwaarde = Register::getValue('inschrijvingBetalen_' . $soort, false);
			$toegestaan = filter_var($registerwaarde, FILTER_VALIDATE_BOOLEAN);
			if ($waarde == $soort || $toegestaan)
			{
				$beschikbareEnums[] = $soort;
			}
		}
		return $beschikbareEnums;
	}
}
