<?

/***
 * $Id$
 */
class IntroDeelnemer
	extends IntroDeelnemer_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a)
	{
		parent::__construct($a);
	}

	/**
	 * @brief Geef de introdeelnemer met een gegeven magische code.
	 *
	 * @param code De magische code om op te geven.
	 *
	 * @return Een IntroDeelnemer-object, of null indien niet gevonden.
	 */
	public static function geefUitMagischeCode($code)
	{
		return IntroDeelnemerQuery::table()
			->whereProp('magischeCode', $code)
			->geef();
	}

	/**
	 * Stel een nieuwe willekeurige magische in voor de deelnemer.
	 *
	 * @return string
	 * De gegenereerde code.
	 */
	public function genereerMagischeCode()
	{
		$code = hash("sha256", Crypto::getRandomBytes(42));
		$this->setMagischeCode($code);
		return $code;
	}

	public function url()
	{
		return '/Leden/Intro/Deelnemer/' . $this->getLidContactID();
	}

	public function magBekijken()
	{
		return hasAuth('intro');
	}

	public function magWijzigen()
	{
		return hasAuth('intro');
	}

	public function magVerwijderen()
	{
		global $auth;
		return hasAuth('intro') || $auth->getLevel() == 'mollie';
	}

	public function getGroep()
	{
		if(!$this->getLid()) //Van niet leden weten wij geen studies, dus als je geen eens bijna-lid bent (een lidobject hebt) retourneren we null.
		{
			return null;
		}
		else
		{
			$studies = $this->getLid()->getStudies();
			foreach($studies as $studie)
			{
				if($studie->getDatumBegin()->format('Y') == colJaar())
				{
					return $studie->getGroep();
				}
			}
			return null;
		}
	}

	/**
	 * Tijdelijke methode voor de intro van 2020. Sinds Corona een ding is doet Sticky de inschrijvingen voor IC/IK, en
	 * speelt de inschrijvingen door aan ons. Helaas zijn we vergeten dit in de database te zetten, dus we moeten het
	 * maar even doen op basis van studie.
	 */
	public function isStickyInschrijving() {
		$studies = $this->getLid()->getStudies(true);
		$ids = [];
		foreach ($studies as $study) {
			$ids[] = $study->getStudie()->geefId();
		}
		$isICorIK = in_array('1', $ids) || in_array('2', $ids);
		return $this->getIntropakketje() === 'N' && $isICorIK;
	}

	public static function opReservelijst(StudieVerzameling $studies)
	{
		$kampgangers = IntroDeelnemerVerzameling::alleBijnaLeden("true");
		return $kampgangers->aantal() >= MAXAANTALKAMP;
	}

	public static function introMail($lid = null, $deel = null, $reservelijst = false)
	{
		global $WSW4DB;
		
		$MAIL_FROM = "Introductiecommissie <bachelor@intro-utrecht.nl>";
		$MAIL_CC = "Introductiecommissie Inschrijvingen <intro-inschrijvingen@A-Eskwadraat.nl>";
		$EXTRA_HEADERS = "Cc: $MAIL_CC\nX-Mailer: A-Eskwadraat Who's Who 4";//Dus ge-CC't naar intro-inschrijvingen

		$com = Commissie::geef(INTROCIE);//Pak meest recente intro, heeft eigenlijk alleen zin in het geval van debug vlak voor nieuwe intro opgezet is.
		$comID = $com->geefID();
		$introsecr = $WSW4DB->q('COLUMN SELECT persoon_contactID'
								. ' FROM CommissieLid'
								. ' WHERE commissie_commissieID = %i AND functie = "SECR"'
								, $comID); //Retouneert lidnr van de secretaris vd meest recente intro
		
		$sig = CommissieView::waardeNaam($com);
		if(count($introsecr) == 1) //Als er 1 secretaris is, onderteken met secretaris+commissienaam ipv alleen commissienaam
		{
				$sig = PersoonView::naam(Persoon::geef($introsecr[0])) . "\nSecretaris " . $sig;
		}
		
$verhaalnormaal = <<<HEREDOC
Leuk dat je meegaat op kamp! Alle informatie over het kamp zal je te zijner tijd nog ontvangen. 
Als je toch niet mee kunt op kamp moet je je uiterlijk zondag 1 september 2024 om 23:59 uitschrijven. 
Je kunt je uitschrijven door te mailen naar bachelor@intro-utrecht.nl.
HEREDOC;
$verhaalreserve = <<<HEREDOC
Je staat nu op de reservelijst voor het kamp. Mogelijk bellen we je vlak voor de intro met de vraag of je één van de 
vrijgekomen plekken wilt opvullen. Als je toch niet mee kunt op kamp en van de reservelijst af wilt moet je je uiterlijk 
zondag 1 september 2024 om 23:59 uitschrijven. Je kunt je uitschrijven door te mailen naar bachelor@intro-utrecht.nl.

Mocht er geen plek voor je vrijkomen dan storten we het inschrijfgeld natuurlijk terug.
HEREDOC;

		if($lid instanceof Lid && $deel instanceof IntroDeelnemer)
		{
			if($reservelijst){
				$verhaal = $verhaalreserve;
			} else {
				$verhaal = $verhaalnormaal;
			}
			$email = $lid->getEmail();
			$subject = "Inschrijving introductie";

			$kamp_verhaal = '';
			if ($deel->getKamp())
				$kamp_verhaal = $verhaal;
			
			// gegevens van inschrijving
			$naam = $lid->getNaam();
			$geboortedatum = $lid->getDatumGeboorte()->strftime("%d-%m-%Y");
			$studie = $lid->getStudies()->toStudieVerzameling()->implode("getNaam", ", ");
			$studentnummer = $lid->getStudentnr();

			$adres = $lid->getEersteAdres();

			$adresStraat = $adres->getStraat1() . " " . $adres->getHuisnummer() . $adres->getStraat2();
			$adresStad = $adres->getPostcode() . " " . $adres->getWoonplaats();

			$mobiel = $lid->getTelefoonNummers()->filter("getSoort", "MOBIEL")->first()->__toString();
			$noodnummer = $lid->getTelefoonNummers()->filter("getSoort", "OUDERS")->first()->__toString();

			$opKamp = ($deel->getKamp()) ? "Ja" : "Nee";
			$vega = ($deel->getVega()) ? "Ja" : "Nee";

			$allergieen = ($deel->getAllergie()) ? "Ja\n" . $deel->getAllergieen() : "Nee";
			$voedselBeperkingen = $deel->getVoedselBeperking();

			$opmerkingen = $deel->getIntroOpmerkingen();
		}
		else
		{
			$verhaal = "[Normale aanmelding]\n\n" .
					$verhaalnormaal
					. "\n\n[\Normale aanmelding]\n[Reservelijst aanmelding]\n\n" .
					$verhaalreserve
					. "\n\n[\Reservelijst aanmelding]";

			$email = "email@email.com";
			$subject = "Inschrijving introductie 1234";
			$kamp_verhaal = $verhaal. "\n[Dit verhaal staat er alleen als de deelnemer mee op kamp gaat]";

			$naam = "Voor Beeld";
			$geboortedatum = "1-1-1911";
			$studie = "Wiskunde, Natuurkunde";
			$studentnummer = "1234";

			$adresStraat = "Ergens op de aardbol 35";
			$adresStad = "3584 CC Utrecht";

			$mobiel = "0612345678";
			$noodnummer = "0622334455";

			$opKamp = "Ja of Nee";
			$vega = "Ja of Nee";

			$allergieen = "Ja\nLekke banden";
			$voedselBeperkingen = "Ik kan niet tegen eten";

			$opmerkingen = "Hier staat een opmerking";
		}

$message = <<<HEREDOC
Beste $naam,

Bedankt voor je inschrijving!

$kamp_verhaal

Onderaan deze mail staan de door jou ingevulde gegevens. Mocht er hier iets ontbreken, of verkeerd zijn ingevuld, reageer dan even op deze mail.

Tijdens de introductie gaan we onder andere gezamenlijk eten, er staat een leuke extra activiteit op het programma en we sluiten natuurlijk af 
met een eindfeest (helaas is dit wel 18+). Natuurlijk wil je hier bij zijn! Dan kun je een pakket kopen via https://www.intro-utrecht.nl/#pakketten.

Hieronder volgt een aantal belangrijke data:
- Maandag 2 september: De verplichte facultaire introductie, boekverkoop en eventueel uiteten met je mentorgroepje.
- Dinsdag 3 september: Verplichte introductiedag en eventueel uit eten met je mentorgroepje.
- Woensdag 4 t/m vrijdag 6 september: Introductiekamp met het thema ‘Mythical Madness’.
- Woensdag 11 (wiskunde en TWECO) of donderdag 12 september (natuurkunde, TWIN en TWINFO): Een leuke extra activiteit. 
  Voor wiskunde en TWECO is dit poolen. Voor natuurkunde, TWIN, en TWINFO is dit jeu de boule. Deze activiteiten overlappen niet met colleges.
- Vrijdag 13 september: Een gezellige borrel en het eindfeest.

Het introductiekatern met aanvullende informatie kan je vinden op
https://www.intro-utrecht.nl/#katern.

We hopen je hiermee voldoende op de hoogte te hebben gebracht. Bij vragen kan je altijd mailen naar bachelor@intro-utrecht.nl. 
Nog een fijne vakantie en tot ziens bij de introductie!

Met vriendelijke groet,
$sig

Studievereniging A–Eskwadraat
Universiteit Utrecht
tel: 030-253 4499
e-mail: bachelor@intro-utrecht.nl
www: intro-utrecht.nl

Personalia:
Naam: $naam
Geboortedatum: $geboortedatum
Studie: $studie
Studentnummer: $studentnummer

Adres:
$adresStraat
$adresStad

Telefoonnummers:
Mobiel: $mobiel
Noodnummer: $noodnummer

Mee op kamp? $opKamp
Vegan? $vega

Allergieën:
$allergieen

Overige voedsel beperking:
$voedselBeperkingen

Overige opmerkingeen:
$opmerkingen

HEREDOC;
		
		if($lid instanceof Lid && $deel instanceof IntroDeelnemer)
		{
			sendmail($MAIL_FROM, $email, $subject,nl2br($message), null, $EXTRA_HEADERS, 'intro@A-Eskwadraat.nl', true);
		}
		else
		{
			return "<pre>".$message."</pre>";
		}

	}

	public static function lidWordenMail(Lid $lid)
	{

		$com = Commissie::geef(INTROCIE);//Pak meest recente intro, heeft eigenlijk alleen zin in het geval van debug vlak voor nieuwe intro opgezet is.

		$MAIL_CC = "intro-inschrijvingen@A-Eskwadraat.nl";
		$email = new Email();
		$body = <<<HERE
Beste {$lid->getNaam()},

Bedankt voor je inschrijving!

Op maandag 6, dinsdag 7 en donderdag 16 september gaan we samen eten en er is donderdag 16 september (mits de corona-regels dit toestaan) een eindfeest. Op woensdagmiddag 8 en donderdag 9 september zijn er mysterie-activiteiten met je mentorgroepje. Als je bij deze activiteiten wilt zijn, kun je vanaf 21 juli intropakketten kopen via: <a href='http://www.intro-utrecht.nl/#pakketten'>http://www.intro-utrecht.nl/#pakketten</a>

Het programma staat op dit moment nog niet volledig vast, vanwege veranderende corona-regels. Kijk voor het programma tot nu toe op: <a href='http://www.intro-utrecht.nl/#programma'>http://www.intro-utrecht.nl/#programma</a>

Voor verdere informatie en overige activiteiten kun je het introductiekatern aan het eind van de vakantie op de site bekijken op: <a href='http://www.intro-utrecht.nl/#katern'>http://www.intro-utrecht.nl/#katern</a>

Nog een fijne vakantie gewenst en tot ziens tijdens de introductie!

Met vriendelijke groet,
Lizanne van der Laan
Secretaris van de Introductiecommissie 2021

<small>
A–Eskwadraat, de studievereniging voor wiskunde, natuurkunde, informatica en informatiekunde
Princetonplein 5 | Kamer 2.69 | 3584 CC Utrecht
(030) 253 4499 | bachelor@intro-utrecht.nl | intro-utrecht.nl
</small>
HERE;
		$body = nl2br($body);
		$email->setFrom($com)
			->setTo([$lid->getEmail(), $MAIL_CC])
			->setSubject("Inschrijving introductie 2021")
			->setBody($body, true);
		$email->send();
		return true;
	}

	public static function alfasMail(Lid $lid)
	{
		global $BESTUUR;
		$email = new Email();
		$activatieCode = ActivatieCode::fromLid($lid) ?? ActivatieCode::genereer($lid);
		$activatieCode->opslaan();
		$body = <<<HERE
Beste {$lid->getNaam()},

Vandaag is het Actieve Leden Festival A-Eskwadraat en Sticky (ALFAS).
Hier zal je kennismaken met de studieverenigingen A-Eskwadraat en Sticky en de kans krijgen om je in te schrijven bij A-Eskwadraat.
Je kunt je aanmelden met de volgende link:
<a href="https://a-eskwadraat.nl/Leden/Intro/Dili/Login">https://a-eskwadraat.nl/Leden/Intro/Dili/Login</a>
Met je lidnummer: {$lid->getContactID()}
En activatiecode: {$activatieCode->getCode()}
Aanmelden kost eenmalig 30 euro, maar dan ben je ook je hele studietijd lang lid. Je kunt je alvast inschrijven als je wilt, maar dat is niet verplicht. Je kunt ook wachten tot het ALFAS, dan krijg je sowieso meer informatie.
Tot straks op het ALFAS!

Met vriendelijke groet,

Clara Koster
Kandidaat-secretaris Studievereniging A–Eskwadraat
<small>
A–Eskwadraat, de studievereniging voor wiskunde, natuurkunde, informatica en informatiekunde
Princetonplein 5 | Kamer 2.69 | 3584 CC Utrecht
(030) 253 4499 | secretaris@a-eskwadraat.nl | www.a-eskwadraat.nl
</small>
HERE;
		$body = nl2br($body);
		$email->setFrom($BESTUUR['Secretaris'])
			->setTo([$lid->getEmail()])
			->setSubject("Lid worden bij A-Eskwadraat")
			->setBody($body, true);
		$email->send();
		return true;
	}


	/**
	 * @brief Zet bij een IntroDeelnemer dat die betaald heeft via iDEAL.
	 *
	 * Wordt aangeroepen vanuit iDeal::handelArtikelAf.
	 *
	 * @param email Het adres van de introdeelnemer.
     */
    public static function idealBetaald($email) {

        $lid = Contact::zoekOpEmail($email);
        if(!$lid) {
            sendmail('webcie@a-eskwadraat.nl'
                    , 'webcie@a-eskwadraat.nl'
                    , 'Introinschrijving misgegaan'
                    , "Yo WebCie!\r\nEr is net een iDealtransactie voor een introkamp succesvol betaald, "
                    . "maar de flippo van wie de betaling is hebben we niet kunnen vinden. "
                    . "Het gaat om iDealtransactie " . $this->geefID() . ". Fix dit moeilijk snel! "
                    . "\r\n\r\nGroetjes, de Website!");
            return;
        }
        $deelnemer = IntroDeelnemer::geef($lid->geefID());
        if(!$deelnemer || $deelnemer->getBetaald() != 'NEE') {
            sendmail('webcie@a-eskwadraat.nl'
                    , 'webcie@a-eskwadraat.nl'
                    , 'Introinschrijving misgegaan'
                    , "Yo WebCie!\r\nEr is net een iDealtransactie voor een introkamp succesvol betaald, "
                    . "maar er is geen deelnemer bij de flippo van wie de betaling is gevonden. "
                    . "Het gaat om iDealtransactie " . $this->geefID() . ". Fix dit moeilijk snel! "
                    . "\r\n\r\nGroetjes, de Website!");
            return;
        }

        //Verstuur de mail naar de deelnemer/intro om te laten weten dat de inschrijving succesvol was:
        $reservelijst = self::opReservelijst(LidStudieVerzameling::vanLid($lid)->toStudieVerzameling());
        self::introMail($lid, $deelnemer, $reservelijst);

        if (strlen($deelnemer->getIntroOpmerkingen()) > 1) {
            sendmail('www-data@a-eskwadraat.nl', 'bachelor@intro-utrecht.nl', 'Inschrijving [' . $lid->geefID() . '] heeft vragen/opmerkingen',
                    'Beste intro,' . "\n\n" .
                    'Er heeft zich iemand ingeschreven met nog een vraag/opmerking:' . "\n" .
                    $deelnemer->getIntroOpmerkingen() . "\n\n" .
                    'Handelen jullie het verder af?' . "\n\n" .
                    'Met vriendelijke groeten,' . "\n" .
                    'De introsmurf', $lid->getEmail());
        }

        //vanwege gekke voodoo magie moet opslaan gelijk na setBetaald.
        $deelnemer->setBetaald('IDEAL');
        $deelnemer->opslaan();
    }
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
