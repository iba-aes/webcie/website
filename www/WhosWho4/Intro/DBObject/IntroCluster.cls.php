<?

/***
 * $Id$
 * 
 * extends IntroGroepVerzameling ?
 */
class IntroCluster
	extends IntroCluster_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
		$this->jaar = date('Y');
	}
	
	public function url()
	{
		return '/Leden/Intro/Cluster/' . $this->getClusterID();
	}

	/** METHODEN **/
	public function groepjes()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `groepID`'
						.'FROM `IntroGroep`'
						.' WHERE `cluster_clusterID` = %i',$this->getClusterID());
		return IntroGroepVerzameling::verzamel($ids);
	}
	
	/**
	 * @brief Returneert of de gebruiker waarden in deze klasse mag zien.
	 */
	public static function magKlasseBekijken()
	{
		return hasAuth('lid');
	}

	public function magWijzigen()
	{
		return hasAuth('intro');
	}
	
	public function magVerwijderen()
	{
		return hasAuth('intro');
	}
	
	public function alleMentoren()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `lid_contactID`
		FROM `Mentor`
		RIGHT JOIN `IntroGroep` 
		ON `groepID` = `groep_groepID`
		WHERE `cluster_clusterID`= %i ', $this->getClusterID());

		return PersoonVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
