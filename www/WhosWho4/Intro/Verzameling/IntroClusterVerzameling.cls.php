<?

/**
 * $Id$
 */
class IntroClusterVerzameling
	extends IntroClusterVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // IntroClusterVerzameling_Generated
	}
	
	static public function getClusterDitJaar($jaar = null)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q("COLUMN SELECT `clusterID` FROM `IntroCluster` "
						. "WHERE `jaar` = %i",
						(is_null($jaar))?date("Y"):$jaar);
		return self::verzamel($ids);
	}
	
	public static function jarenMetCluster()
	{
		global $WSW4DB;
		$jaren = $WSW4DB->q('COLUMN SELECT DISTINCT `jaar`'
						.'FROM `IntroCluster`');
		$jaararray = array();
		foreach($jaren as $jaar)
		{
			$jaararray[$jaar] = $jaar;
		}
	    asort($jaararray);
		return $jaararray;
	}

	public static function alleClusters()
	{
		return IntroClusterQuery::table()
			->orderByDesc('jaar')
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
