<?
class LidmaatschapsBetalingVerzameling
	extends LidmaatschapsBetalingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // LidmaatschapsBetalingVerzameling_Generated
	}

	public static function alleBetalingen()
	{
		return LidmaatschapsBetalingQuery::table()->verzamel();
	}
}
