<?
class ActivatieCodeVerzameling
	extends ActivatieCodeVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ActivatieCodeVerzameling_Generated
	}

	public static function bijnaledenZonderCode() {
		$alleCodes = ActivatieCodeQuery::table()->verzamel()->keys();
		return LidQuery::bijnaleden()
			->whereNotInProp("Lid.contactID", $alleCodes)
			->verzamel();
	}
}
