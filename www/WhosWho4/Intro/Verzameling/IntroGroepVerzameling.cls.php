<?

/**
 * $Id$
 */
class IntroGroepVerzameling
	extends IntroGroepVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/**
		 Retourneert alle IntroGroepVerzameling objecten behorend bij de
		opgegeven mentor
	**/
	static public function groepenVanMentor(Contact $mentor)
	{
		global $WSW4DB;

		$contactid = $mentor->geefID();
		$ids = $WSW4DB->q("COLUMN SELECT groepID from Mentor WHERE contactID = %i", $contactid);
		return self::verzamel($ids);
	}

	static public function groepenVanJaar($jaar)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q("COLUMN SELECT groepID FROM IntroGroep JOIN IntroCluster
							ON IntroGroep.cluster_clusterID = IntroCluster.clusterID
							WHERE IntroCluster.jaar = %i", $jaar);
		return self::verzamel($ids);
	}

	/**
	 * Sorteer twee introgroepen op de naam.
	 * @returns -1 als a < b, 0 als a == b en 1 als a > b
	 */
	public static function sorteerOpNaam($aID, $bID) {
		$a = IntroGroep::geef($aID);
		$b = IntroGroep::geef($bID);

		$aNaam = $a->getNaam();
		$bNaam = $b->getNaam();

		if ($aNaam < $bNaam) return -1;
		if ($aNaam > $bNaam) return 1;
		return 0;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
