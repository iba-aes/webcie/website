<?
abstract class IntroGroepVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de IntroGroepVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze IntroGroepVerzameling een IntroClusterVerzameling.
	 *
	 * @return IntroClusterVerzameling
	 * Een IntroClusterVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze IntroGroepVerzameling.
	 */
	public function toClusterVerzameling()
	{
		if($this->aantal() == 0)
			return new IntroClusterVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getClusterClusterID()
			                      );
		}
		$this->positie = $origPositie;
		return IntroClusterVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een IntroGroepVerzameling van Cluster.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling die elementen bevat die bij de Cluster hoort.
	 */
	static public function fromCluster($cluster)
	{
		if(!isset($cluster))
			return new IntroGroepVerzameling();

		return IntroGroepQuery::table()
			->whereProp('Cluster', $cluster)
			->verzamel();
	}
}
