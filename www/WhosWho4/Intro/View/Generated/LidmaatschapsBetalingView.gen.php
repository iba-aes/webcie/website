<?
abstract class LidmaatschapsBetalingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LidmaatschapsBetalingView.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidmaatschapsBetaling(LidmaatschapsBetaling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(LidmaatschapsBetaling $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(LidmaatschapsBetaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(LidmaatschapsBetaling $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(LidmaatschapsBetaling::enumsSoort() as $id)
			$soorten[$id] = LidmaatschapsBetalingView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(LidmaatschapsBetaling $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param LidmaatschapsBetaling $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(LidmaatschapsBetaling $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', LidmaatschapsBetaling::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rekeningnummer.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rekeningnummer labelt.
	 */
	public static function labelRekeningnummer(LidmaatschapsBetaling $obj)
	{
		return 'Rekeningnummer';
	}
	/**
	 * @brief Geef de waarde van het veld rekeningnummer.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rekeningnummer van het object
	 * obj representeert.
	 */
	public static function waardeRekeningnummer(LidmaatschapsBetaling $obj)
	{
		return static::defaultWaardeString($obj, 'Rekeningnummer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rekeningnummer.
	 *
	 * @see genericFormrekeningnummer
	 *
	 * @param LidmaatschapsBetaling $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekeningnummer staat en
	 * kan worden bewerkt. Indien rekeningnummer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRekeningnummer(LidmaatschapsBetaling $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Rekeningnummer', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rekeningnummer. In
	 * tegenstelling tot formrekeningnummer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formrekeningnummer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekeningnummer staat en
	 * kan worden bewerkt. Indien rekeningnummer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRekeningnummer($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Rekeningnummer', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * rekeningnummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rekeningnummer representeert.
	 */
	public static function opmerkingRekeningnummer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rekeninghouder.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rekeninghouder labelt.
	 */
	public static function labelRekeninghouder(LidmaatschapsBetaling $obj)
	{
		return 'Rekeninghouder';
	}
	/**
	 * @brief Geef de waarde van het veld rekeninghouder.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rekeninghouder van het object
	 * obj representeert.
	 */
	public static function waardeRekeninghouder(LidmaatschapsBetaling $obj)
	{
		return static::defaultWaardeString($obj, 'Rekeninghouder');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rekeninghouder.
	 *
	 * @see genericFormrekeninghouder
	 *
	 * @param LidmaatschapsBetaling $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekeninghouder staat en
	 * kan worden bewerkt. Indien rekeninghouder read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRekeninghouder(LidmaatschapsBetaling $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Rekeninghouder', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rekeninghouder. In
	 * tegenstelling tot formrekeninghouder moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formrekeninghouder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rekeninghouder staat en
	 * kan worden bewerkt. Indien rekeninghouder read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRekeninghouder($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Rekeninghouder', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * rekeninghouder bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rekeninghouder representeert.
	 */
	public static function opmerkingRekeninghouder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ideal.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld ideal labelt.
	 */
	public static function labelIdeal(LidmaatschapsBetaling $obj)
	{
		return 'Ideal';
	}
	/**
	 * @brief Geef de waarde van het veld ideal.
	 *
	 * @param LidmaatschapsBetaling $obj Het LidmaatschapsBetaling-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ideal van het object obj
	 * representeert.
	 */
	public static function waardeIdeal(LidmaatschapsBetaling $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getIdeal())
			return NULL;
		return iDealView::defaultWaardeiDeal($obj->getIdeal());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ideal.
	 *
	 * @see genericFormideal
	 *
	 * @param LidmaatschapsBetaling $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ideal staat en kan worden
	 * bewerkt. Indien ideal read-only is betreft het een statisch html-element.
	 */
	public static function formIdeal(LidmaatschapsBetaling $obj, $include_id = false)
	{
		return iDealView::defaultForm($obj->getIdeal());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ideal. In
	 * tegenstelling tot formideal moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formideal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ideal staat en kan worden
	 * bewerkt. Indien ideal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormIdeal($name, $waarde=NULL)
	{
		return iDealView::genericDefaultForm('Ideal');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ideal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ideal representeert.
	 */
	public static function opmerkingIdeal()
	{
		return NULL;
	}
}
