<?
abstract class ActivatieCodeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActivatieCodeView.
	 *
	 * @param ActivatieCode $obj Het ActivatieCode-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActivatieCode(ActivatieCode $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param ActivatieCode $obj Het ActivatieCode-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(ActivatieCode $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param ActivatieCode $obj Het ActivatieCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(ActivatieCode $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld code.
	 *
	 * @param ActivatieCode $obj Het ActivatieCode-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld code labelt.
	 */
	public static function labelCode(ActivatieCode $obj)
	{
		return 'Code';
	}
	/**
	 * @brief Geef de waarde van het veld code.
	 *
	 * @param ActivatieCode $obj Het ActivatieCode-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld code van het object obj
	 * representeert.
	 */
	public static function waardeCode(ActivatieCode $obj)
	{
		return static::defaultWaardeString($obj, 'Code');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld code.
	 *
	 * @see genericFormcode
	 *
	 * @param ActivatieCode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is betreft het een statisch html-element.
	 */
	public static function formCode(ActivatieCode $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Code', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld code. In tegenstelling
	 * tot formcode moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formcode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormCode($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Code', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld code
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld code representeert.
	 */
	public static function opmerkingCode()
	{
		return NULL;
	}
}
