<?
abstract class ActivatieCodeVerzamelingView
	extends ActivatieCodeVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in ActivatieCodeVerzamelingView.
	 *
	 * @param obj Het ActivatieCodeVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActivatieCodeVerzameling(ActivatieCodeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Toon alle (gegeven) codes in een mooi tabelletje.
	 * @param codes Indien NULL, laad ze allemaal uit de databaas.
	 * @returns Een HtmlTable
	 */
	public static function tabel(ActivatieCodeVerzameling $codes = null) {
		if (is_null($codes)) {
			$codes = ActivatieCodeQuery::table()->verzamel();
		}
		$tabel = new HtmlTable(array(), "sortable");
		$tabel->add(new HtmlTableHead($row = new HtmlTableRow()));
		$row->add(new HtmlTableHeaderCell(_("Naam")));
		$row->add(new HtmlTableHeaderCell(_("Lidnummer")));
		$row->add(new HtmlTableHeaderCell(_("Code")));

		foreach ($codes as $code) {
			$lid = $code->getLid();

			// We willen alleen bijnaleden zien, echte leden zijn al geactiveerd.
			if ($lid->getLidToestand() != "BIJNALID") {
				continue;
			}

			$row = new HtmlTableRow();
			$row->add(new HtmlTableDataCell(IntroDeelnemerView::makeLink($lid)));
			$row->add(new HtmlTableDataCell(LidView::waardeContactID($lid)));
			$row->add(new HtmlTableDataCell(new HtmlCode(ActivatieCodeView::waardeCode($code))));

			$tabel->addImmutable($row);
		}

		return $tabel;
	}
}
