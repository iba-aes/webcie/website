<?

/**
 * $Id$
 */
abstract class IntroGroepVerzamelingView
	extends IntroGroepVerzamelingView_Generated
{
	/**
	 * Toon de introgroepen van een cluster op de manier van toonGroepen.
	 * @param cluster De cluster waarvan we de groepen willen zien.
	 * @param gesorteerd Of de groepen gesorteerd moeten worden.
	 */
	public static function toonClusterGroepen (IntroCluster $cluster, $gesorteerd)
	{
		$groepjes = $cluster->groepjes();
		if ($gesorteerd) {
			$groepjes->sorteer('naam');
		}
		return self::toonGroepen($groepjes, $cluster);
	}

	public static function toonGroepen (IntroGroepVerzameling $groepen = null, IntroCluster $cluster = NULL)
		{
			$div = new HtmlDiv();
			if($groepen->aantal()>0)
			{
				$div->add($a = new HtmlList());
				foreach($groepen as $groep)
				{
					$groeplink = IntroGroepView::maakLink($groep);
					$mentoren = $groep->mentoren();
					$b = "";
					foreach($mentoren as $mentor)
						{   
							if($b == "")
							{
								if($mentor->magBekijken())
									$b = new HtmlAnchor($mentor->url(),PersoonView::naam($mentor));
								else
									$b = PersoonView::naam($mentor);
							}
							else
							{	
								if($mentor->magBekijken())
									$b .= (" & ". new HtmlAnchor($mentor->url(),PersoonView::naam($mentor)));
								else
									$b .= (" & ". PersoonView::naam($mentor));
							}
						}
					$b = new HtmlSpan($b);
					$a->add(new HtmlListItem($groeplink. ' : '.$b));
				}
			}
			else
			{
				$div->add(new HtmlSpan(_('Er zijn nog geen mentorgroepen.')));
				if ($cluster && $clusterID = $cluster->geefID())
				{
					$div->add(new HtmlBreak())
						->add(IntroClusterView::maakNieuweGroepLink($cluster, _('Maak er een aan')));
				}
			}
			
			return $div;
		}

	static public function maakIntroGroepSelect($introgroepen, $multiple = false) {
		$igs = array();
		if($introgroepen instanceof IntroGroepVerzameling) {
			foreach($introgroepen as $introgroep)
			{
				$igs[$introgroep->geefID()] = IntroGroepView::waardeNaam($introgroep);
			}
		} else {
			foreach(array_filter($introgroepen) as $i) {
				if(array_key_exists($i,$igs))
					continue;
				$igs[$i] = IntroGroep::geef($i)->getNaam();
			}	
		}

		$selected = tryPar('introgroepen',array());

		$box = HtmlSelectbox::fromArray('introgroepen',$igs,$selected,5);
		if($multiple)
			$box->setMultiple();
		return $box;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1

