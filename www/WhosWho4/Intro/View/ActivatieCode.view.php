<?
abstract class ActivatieCodeView
	extends ActivatieCodeView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in ActivatieCodeView.
	 *
	 * @param obj Het ActivatieCode-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActivatieCode(ActivatieCode $obj)
	{
		return self::defaultWaarde($obj);
	}

}
