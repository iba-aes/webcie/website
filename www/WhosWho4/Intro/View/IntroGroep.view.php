<?

/**
 * $Id$
 */
abstract class IntroGroepView
	extends IntroGroepView_Generated
{
	static public function labelNaam(IntroGroep $obj)
	{
		return _('Naam mentorgroep');
	}

	static public function labelCluster(IntroGroep $obj)
	{
		return _('Kleur');
	}
	
	static public function maakLink($groep)
	{
		return new HtmlAnchor($groep->url(), IntroGroepView::waardeNaam($groep));
	}

	static public function maakLinkUitgebreid($groep)
	{
		$span = new HtmlSpan();
		$span->add(new HtmlAnchor('/Leden/Intro/?jaar=' . $groep->getJaar(),
				$groep->getJaar()))
			->add(" > ")
			->add(IntroClusterView::maakLink($groep->getCluster()))
			->add(" > ")
			->add(self::maakLink($groep));
		return $span;
	}

	static public function formCluster(IntroGroep $groep, $include_id = false)
	{   // TODO dit naar IntroClusterView (of IntroClusterVerzameling?) ...
		$div = new HtmlDiv();

		$jaren = IntroClusterVerzameling::jarenMetCluster();

		// We maken een knockout-model met de jaren zodat als het jaar gewijzigd
		// wordt, de zichtbare keuze-opties gewijzigd worden
		$div->add(HtmlScript::makeJavascript('$(function() {
	var ViewModel = function() {
		self.jaar = ko.observable(' . end($jaren) . ');
	}

	ko.applyBindings(new ViewModel());
});'));

		$div->add($sel = HtmlSelectbox::fromArray('test', $jaren));
		$sel->setAttribute('data-bind', 'value: jaar');

		foreach($jaren as $jaar) {
			$name = self::formveld($groep, 'Cluster');
			$clusters = IntroClusterVerzameling::getClusterDitJaar($jaar);
			$ret = array();
			foreach ($clusters as $cluster)
				$ret[] = HtmlInput::makeRadio($name
						, array($cluster->geefID(), IntroClusterView::waardeNaam($cluster))
						, $groep->getCluster() == $cluster
					);

			$div->add($d = new HtmlDiv($ret));
			$d->setAttribute('data-bind', 'visible: jaar() == ' . $jaar);
		}

		return $div;
	}
	
	static public function processNieuwForm(IntroGroep $groep)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($groep), NULL))
			return false;
			
		/** Vul alle statische informatie in **/
		return self::processForm($groep);
	}

	/**
	 * @brief Geef een HTML-pagina met info over deze groep.
	 *
	 * @return Een HTMLPage.
	 */
	static public function toonGroep(IntroGroep $grp)
	{
		$page = static::pageLinks($grp);
		$page
			->setBrand('intro')
			->start(_("Mentorgroep bekijken"))
			->add(self::bekijkGroep($grp));
		return $page;
	}
	
	static public function bekijkGroep(IntroGroep $grp)
	{
		$naamGro = IntroGroepView::waardeNaam($grp);

		$div = new HtmlDiv();
		$div->add($table = new HtmlTable($row = new HtmlTableRow()));
		$table->setAttribute('style','width: 100%');
		$row->add($cell = new HtmlTableDataCell());
		$cell->add(new HtmlSpan(_("Naam: "), 'strongtext'))
			->add(new HtmlSpan($naamGro))
		    ->add(new HtmlBreak());
		
		$clu = $grp->getCluster();
		if($clu)
		{
			$jaar = IntroClusterView::waardeJaar($clu);
			$naamClu = IntroClusterView::waardeNaam($clu);
			$cell->add(new HtmlSpan(_("Cluster: "), 'strongtext'))
				->add(IntroClusterView::maakLink($clu))
				->add(new HtmlBreak())
				->add(new HtmlSpan(_("Jaar: "), 'strongtext'))
				->add(new HtmlAnchor('../../?jaar='.$jaar, $jaar));
		}
		else
		{
			$cell->add(new HtmlSpan((_("Niet gekoppeld aan een introcluster"))))
				->add(new HtmlBreak());
		}
		
		if(Persoon::getIngelogd()) {
			$row->add($cell = new HtmlTableDataCell());
			$fotos = MediaVerzameling::getTopratedByMentorgroep($grp,3);
			$cell->add(MediaVerzamelingView::fotoLijstMentorgroep($fotos,$grp));
		}

		$div->add(new HtmlHeader(2, _("Mentoren")));
		$list = new HtmlList();
		$mentoren = $grp->mentoren();
		foreach($mentoren as $mentor)
			$list->add(new HtmlListItem(PersoonView::makeLink($mentor)));
		if($mentoren->aantal() == 0)
			$div->add(new HtmlParagraph(_("Deze mentorgroep heeft geen mentoren.")));
		else
			$div->add($list);
		
		$div->add(new HtmlHeader(2, _("Kindjes").' '.new HtmlSmall(new HtmlAnchor('Koppenblad', _('Koppenblad')))));
		$kindjes = $grp->kindjes();

		$list = new HtmlList();
		foreach($kindjes as $kindje) {
			if(!$kindje->isBijnaLid() || hasAuth('intro'))
				$list->add(new HtmlListItem(IntroDeelnemerView::makeLink($kindje)));
		}
		if($kindjes->aantal() == 0)
			$div->add(new HtmlParagraph(_("Deze mentorgroep heeft geen kindjes.")));
		else
			$div->add($list);

		return $div;
	}
	
	public static function processWijzigForm(IntroGroep $editGrp)
	{
			if(!$data = tryPar(self::formobj($editGrp), NULL))
			{
					return false;
			}
			self::processForm($editGrp);
	}
	
	/**
	 * Geef de wijzigpagina voor een introgroep.
	 *
	 * @return HTMLPage
	 */
	public static function wijzig($editGrp, $nieuw = false, $show_error)
	{
		$naam = IntroGroepView::waardeNaam($editGrp); 
		$page = static::pageLinks($editGrp);
		$page
			->setBrand('intro')
			->addFooterJS(Page::minifiedFile('knockout.js', 'js'));
		if($nieuw) {
			$page->start(_("Groepje toevoegen"), 'intro');
		} else {
			$page->start(_("Wijzig groepje") . ' ' .$naam, 'intro');
		}

		$page->add(IntroGroepView::wijzigForm('WijzigGrp', $editGrp, $nieuw, $show_error));
		return $page;
	}

	public static function wijzigForm($name = 'WijzigGrp', $editGrp = null, $nieuw = false, $show_error = true, $velden = 'viewWijzig')
	{
		if(is_null($editGrp))
		{
			$editGrp = new IntroGroep();
		}

		if($nieuw) {
			$mentorenLijst = new PersoonVerzameling();
			$jaar = date('Y');
		} else {
			$cluster = $editGrp->getCluster();
			$jaar = $cluster->getJaar();
			$mentorenLijst = $editGrp->mentoren();
		}
		$mentorenLijst->union(Kart::geef()->getLeden());

		$form = HtmlForm::named($name);
		$form->add($div = new HtmlDiv());

		$div->add(new HtmlHeader(3, _("Informatie")));

		$div->add(self::wijzigTR($editGrp, 'Cluster', $show_error))
			->add(self::wijzigTR($editGrp, 'Naam', $show_error));

		$div->add(new HtmlHeader(3, _("Mentoren: ")))
			->add(View::defaultFormPersoonVerzameling($editGrp->mentoren(), "Mentoren", false, true));

		$div->add(new HtmlHeader(3, _("Kindjes:")));
		$kindjeEnKart = $editGrp->kindjes();
		$kindjeEnKart->union(Kart::geef()->getLeden());
		$kindjeEnKartValid = new PersoonVerzameling();
		foreach($kindjeEnKart as $kindje)
		{
			$studies = $kindje->getStudies();
			foreach($studies as $studie)
			{
				if($studie->getDatumBegin()->format('Y') == $jaar)
				{
					$kindjeEnKartValid->voegtoe($kindje);
				}
			}
		}

		$div->add(new HtmlSpan(_("Let op: je kunt alleen kindjes toevoegen met een studie die begonnen is in het jaar ") . $jaar . ". "));
		$div->add(View::defaultFormPersoonVerzameling($kindjeEnKartValid, 'Kindjes', false, true));

		if($jaar == date('Y'))
		{
			$groeploos = IntroDeelnemerVerzameling::kindjesZonderGroep();

			$div->add(new HtmlSpan(sprintf(_("Er zijn ook %d mentorgroeploze kindjes gevonden."), $groeploos->aantal())));

			$div->add(View::defaultFormPersoonVerzameling($groeploos, 'Loners', false, false, false));
		}

		if($nieuw) {
			$form->add(HtmlInput::makeSubmitButton(_("Voeg toe")));
		} else {
			$form->add(HtmlInput::makeSubmitButton(_("Wijzig de introgroep")));
		}
		return $form;
	}
	
	public static function verwijderGroep($grp, $msg)
	{
			if($grp instanceof IntroGroep)
			{
				$page = Page::getInstance()
					 ->start(null, 'intro')
					 ->setBrand('intro')
					 ->add(new HtmlHeader(2, _("Verwijderen")));
	
				$page->add(new HtmlSpan("Weet je zeker dat je de volgende groep wilt verwijderen?"))
					 ->add(IntroGroepView::bekijkGroep($grp));

				$form = new HtmlForm('post');
				$form->add(new HtmlParagraph(HtmlInput::makeCheckbox('grpid')
							->setCaption(_("Ik weet het heel zeker!"))))
					 ->add(new HtmlDiv(HtmlInput::makeSubmitButton(_("Volgende"), 'check')));
				$page->add($form);
				$page->end();
			}
			else
			{   
				Page::redirectMelding('/Leden/Intro', 'De groep is verwijderd, hoera!');
			}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
