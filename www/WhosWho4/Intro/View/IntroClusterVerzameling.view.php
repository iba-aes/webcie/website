<?

/**
 * $Id$
 */
abstract class IntroClusterVerzamelingView
	extends IntroClusterVerzamelingView_Generated
{
	/**
	 * Maak een formulier om aan te geven welke clusters getoond moeten worden.
	 * @param defJaar Welk jaar al geselecteerd moet zijn.
	 * @param gesorteerd Of de sorteringscheckbox aan moet staan.
	 */
	public static function shortForm($defJaar, $gesorteerd)
	{
		$formTable = new HtmlTable();

		$row = $formTable->addRow();
		$row->addHeader(_("Jaren"));
		$row->addHeader(_("Gesorteerd"));

		$row = $formTable->addRow();
		$row->addData(HtmlSelectbox::fromArray('jaar',IntroClusterVerzameling::jarenMetCluster(), (string) $defJaar,1,true));
		$row->addData(HtmlInput::makeCheckbox('gesorteerd', $gesorteerd, null, null, null, true));
		return new HtmlDiv($formTable, null, 'shortFormAgenda');
	}

	/**
	 * Maak een div met tabel voor de kleur, supermentor en introgroepen per cluster.
	 * @param clusters Welke clusters getoond moeten worden. Zo niet, alleen de table header.
	 * @param gesorteerd Of de introgroepen gesorteerd moeten worden.
	 */
	public static function bekijkClusters(IntroClusterVerzameling $clusters = null, $gesorteerd = false)
	{		
		$div = new HtmlDiv();
		$div ->add($table = new HtmlTable(null, 'lijst'));

		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$a = $row->addHeader(_('Clusters'));
		$a->colspan(3);

		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_('Naam'));
		$row->addHeader(_('Supermentor'));
		$row->addHeader(_('Mentorgroepen'));

		if ($clusters)
		{
			foreach ($clusters as $cluster)
			{
				$naam = IntroClusterView::waardeNaam($cluster);
				$tbody->add($row = new HtmlTableRow());
				$id = $cluster->geefID();
				$row ->addData(new HtmlAnchor('cluster/'.$id,$naam));
				if($cluster->getPersoon())
					$row ->addData(new HtmlSpan(PersoonView::makeLink($cluster->getPersoon())));
				else
					$row ->addData(new HtmlSpan(_('Er is nog geen Supermentor')));
				$row ->addData(IntroGroepVerzamelingView::toonClusterGroepen($cluster, $gesorteerd));
			}
		}
		
		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
