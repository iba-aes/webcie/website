<?
abstract class Constraint_Generated
{
	protected $veld;					/**< \brief NO_CONSTRUCTOR */
	protected $waarde;					/**< \brief NO_CONSTRUCTOR */
	protected $operatie;				/**< \brief NO_CONSTRUCTOR */
	/**
	 * @brief De constructor van de Constraint_Generated-klasse.
	 */
	public function __construct()
	{
		$this->veld = '';
		$this->waarde = '';
		$this->operatie = '';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld veld.
	 *
	 * @return string
	 * De waarde van het veld veld.
	 */
	public function getVeld()
	{
		return $this->veld;
	}
	/**
	 * @brief Geef de waarde van het veld waarde.
	 *
	 * @return string
	 * De waarde van het veld waarde.
	 */
	public function getWaarde()
	{
		return $this->waarde;
	}
	/**
	 * @brief Geef de waarde van het veld operatie.
	 *
	 * @return string
	 * De waarde van het veld operatie.
	 */
	public function getOperatie()
	{
		return $this->operatie;
	}
}
