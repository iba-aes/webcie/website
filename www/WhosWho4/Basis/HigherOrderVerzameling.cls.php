<?php
/**
 *  Maakt het mogelijk om op een verzameling higher order messages en functies uit te voeren
 *
 * Overgenomen van de HigherOrderCollectionProxy van Laravel
 */
class HigherOrderVerzameling
{
	protected $verzameling;

	protected $functie;

	public function __construct(Verzameling $verzameling, $functie)
	{
		$this->functie = $functie;
		$this->verzameling = $verzameling;
	}

	public function __get($key)
	{
		return $this->verzameling->{$this->functie}(function($value) use ($key) {
			return is_array($value) ? $value[$key] : $value->{$key};
		});
	}

	public function __call($functie, $params)
	{
		return $this->verzameling->{$this->functie}(function($value) use ($functie, $params) {
			// De ...-operator werkt vanaf php 5.6
			return $value->{$functie}(...$params);
		});
	}
}
