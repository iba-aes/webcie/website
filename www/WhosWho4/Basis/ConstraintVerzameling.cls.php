<?
class ConstraintVerzameling
	extends ConstraintVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($input = null, $throw = true)
	{
		parent::__construct(); // ConstraintVerzameling_Generated

		if (is_array($input))
			foreach ($input as $rule)
				$this->voegtoe(new Constraint($rule, $throw));
	}

	public function sqlWhere ()
	{
		$where = array();

		foreach ($this as $con)
			$where[] = $con->sqlWhere();

		if (count($where) > 0)
			return 'WHERE ' . implode(' AND ', $where);

		return '';
	}

	public function sqlValues ()
	{
		$return = array();
		foreach ($this as $con)
			$return[] = $con->getWaarde();

		return $return;
	}
}
