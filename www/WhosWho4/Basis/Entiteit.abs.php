<?
/**
 * 
 */
abstract class Entiteit
	extends Entiteit_Generated
{

	/**
	 *  Default constructor voor Entiteit.
	 */
	public function __construct ()
	{
		parent::__construct();
	}

	/**
	 *  Bepaal of de gebruiker dit object mag verwijderen.
	 *
	 * @return True indien de huidig ingelogd gebruiker de juiste rechten heeft;
	 * False anders.
	 */
	public function magVerwijderen ()
	{
		return false;
	}

	/**
	 *  Bepaal of de gebruiker dit object mag wijzigen.
	 *
	 * Deze methode is abstract en vereist een implementatie in iedere 
	 * subklasse van Entiteit.
	 */
	public function magWijzigen ()
	{
		user_error('501 Please override', E_USER_ERROR);
	}

	/**
	 *  Geef een String terug met klassenaam + "View".
	 * In PHP mag kan de deze string als "object" gebruiken voor statische
	 * aanroepen.
	 *
	 * Bijvoorbeeld:
	 * * $view = $object->getView();
	 * * $view::functieUitDeView();
	 *
	 * @return Een string met de naam van de View-klasse die bij deze klasse 
	 * hoort.
	 */
	public function getView ()
	{
		return get_class($this) . "View";
	}

	/**
	 *  override DBObject::gewijzigd()
	 */
	public function gewijzigd ($updatedb = False)
	{
		global $auth;

		$this->gewijzigdWanneer = new DateTimeLocale();
		$this->gewijzigdWie     = $auth->getLidNr();

		parent::gewijzigd($updatedb);
	}

	/**
	 * @brief Haalt alle namen van de vars op die van dit object bekeken mogen worden
	 *
	 * @param vars Een array met de eventuele vars die we al uit een children-aanroep
	 *        gekregen hebben. Als vars NULL is op geen array, wordt het overschreven
	 *        met een lege array.
	 *
	 * @return Een array met als strings de namen van de vars die bezichticht mogen worden
	 */
	public function getBekijkbareVars($vars = NULL)
	{
		// We willen er zeker van zijn dat $vars een array is
		if(is_null($vars) || !is_array($vars))
			$vars = array();

		// Als we tot nu toe niets van het object mogen bekijken moeten we niet
		// gewijzigdWanner en/of gewijzigdWie teruggeven, hierdoor kan iemand namelijk
		// achterhalen dat het object wel bestaat, maar dat het gewoon niet bekeken
		// mag worden
		if(count($vars) != 0)
		{
			$vars[] = 'gewijzigdWanneer';

			$gewijzigdWie = Persoon::geef($this->getGewijzigdWie());
			if($gewijzigdWie && $gewijzigdWie->magBekijken())
				$vars[] = 'gewijzigdWie';
		}
		else
		{
			$vars = array();
		}

		return $vars;
	}

	public function getWijzigbareVars($vars = NULL)
	{
		if(is_null($vars) || !is_array($vars))
			$vars = array();

		return $vars;
	}

	/**
	 * @brief Geeft de uri van dit object in de api terug.
	 *
	 * @return De uri als string
	 */
	public function getApiUri()
	{
		return HTTPS_ROOT . API_BASE . get_class($this) . '/' . $this->geefID();
	}

	/**
	 * @brief Maakt een array van een object dat vervolgens met json_encode in
	 *        een json-format gestopt kan worden. De rede waarom we hier geen
	 *        json_decode doen is omdat deze methode recursief aangeroepen kan
	 *        worden en we pas op het einde alles willen encoden.
	 *
	 * @param wantedVars Een array met de variabele die van het object gevraagd
	 *        worden. In dat geval doet de rest van de vars er niet toe.
	 *
	 * @param expand NULL, of een array van variabelen die als volwaardig JSON
	 * moeten worden weergegeven (anders worden ze alleen als URL weergegeven).
	 *
	 * @return Een array klaar om ge-encode te worden als json. Als er niks van
	 *         het object bekeken mag worden geven we een lege array terug.
	 */
	public function __json($wantedVars = NULL, $expand = NULL)
	{
		// Als we het object niet mogen bekijken geven we meteen een lege array terug
		if(!$this->magBekijken())
			return array();

		$bekijkbareVars = $this->getBekijkbareVars();

		// Als we geen enkele var mogen bekijken, geef lege array terug
		if(count($bekijkbareVars['vars']) == 0 && count($bekijkbareVars['verzamelingen']) == 0)
			return array();

		// Stop wat standaardwaarden in het resultaat.
		// De href is de URL waar dit object te vinden is.
		$object = $this->jsonSamenvatting();
		$class = get_class($this);
		foreach($class::velden('primary') as $field)
		{
			$exp = explode('_', $field);
			$func = 'get';
			foreach($exp as $e)
				$func .= ucfirst($e);

			$object[$field] = $this->$func();
		}

		// Als we wantedVars hebben hoeven we alleen daar door te lopen
		$vars = (!is_null($wantedVars) && is_array($wantedVars)) ? $wantedVars : $bekijkbareVars['vars'];

		foreach($vars as $var)
		{
			// Als de methode niet bestaat komt ie ook niet in het json-object.
			// Hierdoor kan de user niet weten of die geen rechten heeft om
			// de var te bekijken, of dat de var gewoon niet bestaat
			$function = 'get' . ucwords($var);
			if(!method_exists($this, $function))
				continue;

			$toExpand = false;
			if($expand && (is_array($expand) && in_array(strtolower($var), $expand)))
				$toExpand = true;

			// Haal de waarde van de var op
			$value = $this->$function();

			// Als de value een object is en geen datum/tijd
			if($value && !($value instanceof DateTimeLocale) && is_object($value))
			{
				if($value instanceof Verzameling)
				{
					$valueJSON = $value->__json($toExpand, $wantedVars);
				}
				else
				{
					// Als we niets van het object mogen bekijken geef dan een lege
					// array terug
					$objVars = $value->getBekijkbareVars();
					if (count($objVars) == 0)
					{
						$valueJSON = [];
					}
					elseif ($toExpand)
					{
						$valueJSON = $value->__json();
					}
					else
					{
						$valueJSON = $value->jsonSamenvatting();
					}
				}

				$object[$var] = $valueJSON;
			}
			// Een datum/tijd krijgt altijd hetzelfde format
			else if($value instanceof DateTimeLocale)
			{
				$class = get_class($this);
				$type = null;

				while($class && is_null($type))
				{
					$type = $class::getVeldType(strtolower($var));
					$class = get_parent_class($class);
				}
				if($type === 'date')
					$value = $value->format('Y-m-d');
				else
					$value = $value->__toString();

				$object[$var] = $value;
			}
			else
			{
				$object[$var] = $value;
				//$function = 'waarde' . ucwords($var);
				//$viewClass = get_class($this) . 'View';
				//$object[$var] = $viewClass::$function($this);
			}
		}

		return $object;
	}

	/**
	 * @brief Geef een korte JSON-versie van dit object weer.
	 *
	 * Hierin staan alleen de velden die elke JSON bevat,
	 * dus elk object ziet eruit als samenvatting + eventuele velden.
	 *
	 * Met deze versie zou het object herkenbaar moeten zijn,
	 * en kun je via de API de volledige versie ophalen.
	 * Het idee hiervan is dat je geen onbeperkt grote stamboom van objecten geeft
	 * bij het bekijken van een los object in Entiteit::__json,
	 * maar op zekere diepte afkapt door Entiteit::jsonSamenvatting aan te roepen.
	 *
	 * @return Iets wat in json_encode gestopt kan worden,
	 * in de praktijk een associatieve array.
	 */
	protected function jsonSamenvatting()
	{
		// Als het object niets interessants bevat,
		// dan geven we niets terug.
		if (!$this->magBekijken() || count($this->getBekijkbareVars()) == 0)
		{
			return [];
		}

		// Als we wel wat bevatten, dan stoppen we de standaardvelden erin.
		// Voor een zinnige omschrijving moeten we
		// de methode {$class}View::defaultWaarde{$class} aanroepen.
		$viewClass = $this->getView();
		$defaultWaardeFunctie = "defaultWaarde" . get_class($this);
		return [
			'href' => $this->getApiUri(),
			'desc' => (string)$viewClass::$defaultWaardeFunctie($this),
		];
	}

	public function __jsonPut($data)
	{
		$wijzigbareVars = $this->getWijzigbareVars();

		foreach(get_object_vars($data) as $key => $var)
		{
			$res = preg_grep("/$key/i", $wijzigbareVars);
			if(count($res) == 0)
				return array($key . ' niet toegestaan om te wijzigen');
		}

		foreach(get_object_vars($data) as $key => $var)
		{
			$newValue = $var;
			$setFunction = 'set' . ucfirst($key);
			$this->$setFunction($newValue);
		}

		if($this->valid())
			$this->opslaan();
		else
			return array_filter($this->getErrors());

		return false;
	}

	static public function __jsonPost($data)
	{
		$class = get_called_class();

		$constructorVolgorde = $class::constructorVolgorde();

		$params = array();

		if(is_array($constructorVolgorde))
		{
			foreach(get_object_vars($data) as $key => $var)
			{
				$res = preg_grep("/$key/i", $constructorVolgorde);
				if(count($res) != 0)
				{
					$position = key($res);
					$params[$position] = $var;
					unset($data->$key);
				}
			}
		}

		if(count($params) != 0)
		{
			$curr_param_nr = 0;
			foreach($params as $key => $param)
			{
				if($key != $curr_param_nr)
					return array('Geef de parameter mee voor ' . $constructorVolgorde[$curr_param_nr]);
				$curr_param_nr++;
			}
		}

		$reflect = new ReflectionClass($class);
		try
		{
			$obj = $reflect->newInstanceArgs($params);
		}
		catch(Exception $e)
		{
			return array('Exception opgetreden: ' . $e->xdebug_message);
		}

		$wijzigbareVars = $obj->getWijzigbareVars();

		foreach(get_object_vars($data) as $key => $var)
		{
			$res = preg_grep("/$key/i", $wijzigbareVars);
			if(count($res) == 0)
				return array($key . ' niet toegestaan om te wijzigen');
		}

		foreach(get_object_vars($data) as $key => $var)
		{
			$newValue = $var;
			$setFunction = 'set' . ucfirst($key);
			$obj->$setFunction($newValue);
		}

		if(!$obj->magWijzigen())
			return false;

		if($obj->valid())
		{
			try
			{
				$obj->opslaan();
			}
			catch(Exception $e)
			{
				return array('Exception opgetreden: ' . $e->xdebug_message);
			}
		}
		else
			return array_filter($obj->getErrors());

		return $obj;
	}

	public function __jsonDelete()
	{
		$ret = $this->checkVerwijderen();
		if($ret)
			return array($ret);

		$ret = $this->verwijderen();
		if($ret)
			return array($ret);

		return false;
	}

	/**
	 * @brief Geef een json-editie van dit object en alles wat erop dependt.
	 *
	 * Indien het object niet bekeken mag worden, is het resultaat leeg.
	 * Indien een dependency / member van het object niet bekeken mag worden,
	 * is het resultaat een lege array.
	 */
	public function exportJSON()
	{
		if (!$this->magBekijken())
		{
			return ['dependencies' => [], 'object' => []];
		}

		$dependencies = [];
		foreach ($this->getDependencies() as $klassenaam => $dependencyVerzameling)
		{
			if ($dependencyVerzameling->aantal() == 0)
			{
				continue;
			}
			$jsonData = [];
			foreach ($dependencyVerzameling as $obj)
			{
				$jsonData[$obj->geefID()] = $obj->__json();
			}
			$dependencies[$klassenaam] = $jsonData;
		}
		return ['dependencies' => $dependencies, 'object' => $this->__json()];
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
