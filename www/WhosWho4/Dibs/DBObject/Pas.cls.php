<?
/**
 * $Id$
 */
class Pas
	extends Pas_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Pas_Generated

		$this->pasID = NULL;
	}

	public function url ($extension = '')
	{
		if (empty($extension))
			$extension = '';
		else
			$extension = "/$extension";
		return DIBSBASE . '/Passen/' . $this->getPasID() . $extension;
	}

	public function magVerwijderen ()
	{
		return hasAuth('bestuur');
	}

	public function isGebruikt ()
	{
		$transacties = DibsTransactieVerzameling::getPasTransacties($this);
		return $transacties->aantal();
	}

	public function checkRfidTag() {
		$super = parent::checkRfidTag();
		if ($super)
			return $super;

		$rfidTag = $this->getRfidTag();
		if (((int)$rfidTag == 0))
			return _('Voer een geldig pasnummer in!');

		$bestaande = self::geefRfidTag($rfidTag);
		if ($bestaande)
		{
			if ($bestaande->getPersoonContactID() == $this->getPersoonContactID())
				return _('Dit pasnummer hoort al bij deze persoon!');

			return _('ZOMG!!! SUPERERROR!! BELLEN RINKELEN! DEZE PAS HOORT AL BIJ ') . $bestaande->getPersoonContactID();
		}

		return false;
	}

	/**
	 * Geef een Pas met de gegeven rfidTag.
	 *
	 * @param string $tag De waarde van het veld rfidTag.
	 *
	 * @return Pas|false
	 */
	public static function geefRfidTag ($tag)
	{
		return PasQuery::table()
			->whereString('rfidTag', $tag)
			->geef();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
