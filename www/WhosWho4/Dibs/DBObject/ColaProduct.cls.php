<?
/**
 * $Id$
 */
class ColaProduct
	extends ColaProduct_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($volgorde = 0)
	{
		parent::__construct($volgorde); // ColaProduct_Generated

		$this->artikelID = NULL;
	}

	/**
	 * @brief Zoek de Voorraad met lege omschrijving.
	 *
	 * Deze zou voor dit Artikel uniek moeten zijn. Geeft een user error als
	 * de Voorraad niet gevonden kan worden.
	 *
	 * @return de Voorraad die bij dit Artikel hoort en een lege
	 * omschrijving heeft.
	 */
	public function getDefaultVoorraad()
	{
		foreach ($this->getVoorraden() as $voorraad)
			if (!$voorraad->getOmschrijving()) 
				return $voorraad;
		if ($this->getInDB())
			user_error(sprintf(_('ColaProduct \'%s\' (%d) heeft geen correcte Voorraad!'), $this->getNaam(), $this->getArtikelID()));
		else
			return NULL;
	}

	/** 
	 * @brief Zoek het ColaProduct met de gegeven volgorde.
	 *
	 * @param volgorde de volgorde waarnaar gezocht wordt.
	 * @return het ColaProduct met de gegeven volgorde, of NULL als deze niet
	 * bestaat.
	 */
	static public function geefVolgorde ($volgorde)
	{
		return ColaProductQuery::table()
			->whereProp('volgorde', $volgorde)
			->limit(1)
			->geef();
	}

	/** 
	 * @brief Zoek het ColaProduct met de gegeven naam.
	 *
	 * @param naam de naam waarnaar gezocht wordt.
	 * @return het ColaProduct met de gegeven naam, of NULL als deze niet
	 * bestaat.
	 */
	static public function geefDoorNaam ($naam)
	{
		return ArtikelQuery::table()
			->whereString('overerving', 'ColaProduct')
			->whereProp('naam', $naam)
			->geef();
	}

	/** 
	 * @brief Zoek het ColaProduct met de gegeven Barcode.
	 *
	 * @param code de Barcode waarnaar gezocht wordt.
	 * @return het ColaProduct met de gegeven Barcode, of NULL als deze niet
	 * bestaat.
	 */
	static public function geefDoorBarcode ($code)
	{
		return BarcodeQuery::table()
			->join('Voorraad', 'Barcode.voorraad_voorraadID', 'Voorraad.voorraadID')
			->join('Colaproduct', 'Voorraad.artikel_artikelID', 'ColaProduct.artikelID')
			->whereProp('barcode', $code)
			->geef();
	}

	/**
	 * @brief Zoek de verkoopbaarheid van dit ColaProduct uit.
	 *
	 * Geeft een user error indien dit ColaProduct geen geldige default
	 * Voorraad heeft.
	 *
	 * @return true als dit product verkoopbaar is, false anders.
	 */
	public function getVerkoopbaar ()
	{
		return $this->getDefaultVoorraad()->getVerkoopbaar();
	}

	/**
	 * @brief Stel in of dit ColaProduct verkoopbaar is.
	 *
	 * Geeft een user error indien dit ColaProduct geen geldige default
	 * Voorraad heeft.
	 *
	 * @param value of dit ColaProduct verkoopbaar moet zijn.
	 * @return dit ColaProduct.
	 */
	public function setVerkoopbaar ($value)
	{
		$voorraad = $this->getDefaultVoorraad();
		$voorraad->setVerkoopbaar($value);
		$voorraad->opslaan();
		return $this;
	}

	/**
	 * @brief Geef de huidige prijs van dit ColaProduct, in kudos.
	 *
	 * Geeft een user error indien dit ColaProduct geen geldige default
	 * Voorraad heeft.
	 *
	 * @return de huidige prijs van dit ColaProduct in kudos (centen).
	 */
	public function getKudos ()
	{
		return EURO_TO_KUDOS * $this->getDefaultVoorraad()->getVerkoopPrijs()->getPrijs();
	}

	/**
	 * @brief Geef de URL van dit ColaProduct.
	 *
	 * @param extension wordt aan de URL geplakt indien deze niet empty is.
	 * @return de URL van dit ColaProduct, met mogelijk de extension eraan
	 * vastgeplakt.
	 */
	public function url ($extension = '')
	{
		if (empty($extension))
			$extension = '';
		else
			$extension = "/$extension";
		return DIBSBASE . '/Colaproducten/' . $this->getArtikelID() . $extension;
	}

	/**
	 * @brief Controleer de geldigheid van de volgorde van dit ColaProduct.
	 *
	 * @return true indien de volgorde bestaat en groter dan 0 is, false anders.
	 */
	public function checkVolgorde ()
	{
		return $this->getVolgorde() !== NULL && $this->getVolgorde() >= 0;
	}

	/**
	 * @brief Sla dit ColaProduct op.
	 *
	 * Wordt geoverriden omdat de verschuiving in volgorde van dit ColaProduct 
	 * ook andere ColaProducten kan beïnvloeden. Indien de volgorde niet 
	 * veranderd wordt maar dit object geen geldige volgorde meer heeft, 
	 * default de volgorde naar de hoogste volgorde die in de database voorkomt 
	 * +1, een waarde die altijd beschikbaar zou moeten zijn.
	 *
	 * @param dvolgorde de verandering in volgorde van dit ColaProduct. Indien
	 * gelijk aan -1 of 1 wordt de volgorde met dat getal veranderd en wisselt
	 * deze van plaats met het andere object.
	 */
	public function opslaan ($dvolgorde = 0)
	{
		if ($dvolgorde == -1 || $dvolgorde == 1)
		{
			$volg = $this->getVolgorde() + $dvolgorde;
			$prod = static::geefVolgorde($volg);
			if (!$prod instanceof ColaProduct)
			{
				// Geen product met volgorde r gevonden
				parent::opslaan();
				return;
			}
			$prod->volgorde = $this->getVolgorde();
			$this->volgorde = $volg;
			$this->dirty = true;
			$prod->dirty = true;
			parent::opslaan();
			$prod->opslaan();
		}
		else
		{
			// Als de volgorde van dit object ongeldig is of al bestaat bij een ander object,
			// default naar: het maximum van alle volgordes + 1.
			$huidigevolgorde = $this->getVolgorde();
			$bestaandeobject = static::geefVolgorde($huidigevolgorde);
			if ($huidigevolgorde <= 0 || 
				($bestaandeobject && $bestaandeobject->getArtikelID() != $this->getArtikelID()))
			{
				$this->volgorde = ColaProductVerzameling::vindHoogsteVolgorde() + 1;
			}
			parent::opslaan();
		}
	}

	static public function getVeldType($field)
	{
		$type = parent::getVeldType($field);

		if(is_null($type))
		{
			if(strtolower($field) == 'verkoopbaar')
				return 'bool';
		}

		return $type;
	}

	/**
	 * @brief Is het toegestaan dat een voorraad van dit Artikel verkocht wordt?
	 *
	 * Dit wordt gebruikt in Voorraad::checkVerkoopbaar,
	 * als een artikel nog niet "af" is wil je verkopen voorkomen.
	 * Let op: dit slaat alleen op formulierverwerking,
	 * en wordt niet gebruikt bij beslissen of een voorraad verkoopbaar is!
	 *
	 * Defaultimplementatie is altijd FALSE geven.
	 *
	 * @returns Een menselijk leesbare foutmelding, of false.
	 */
	public function verkopenVerboden() {
		if (is_null(hrefToEntry(ColaProductView::plaatjeURL($this)))) {
			return _('zonder plaatje gaat de tablet stuk, dus niet verkoopbaar');
		}
		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
