<?
/**
 * $Id$
 */
abstract class ColaProductVerzamelingView
	extends ColaProductVerzamelingView_Generated
{
	/*functie die alle producten in een tabel zet, aangeroepen door self::overzicht*/
	static public function colaProductTabel (ColaProductVerzameling $colaproducten)
	{
		if ($colaproducten->aantal())
		{
			$colaproducten->sorteer('volgorde');
			$tablediv = new HtmlDiv();

			$form = new HtmlForm();
			$form->add($table = new HtmlTable());

			$table->add($thead = new HtmlTableHead())
				  ->add($tbody = new HtmlTableBody());
			$thead->add($row = new HtmlTableRow());
			$row->makeHeaderFromArray(array(
				ColaProductView::labelNaam(),
				ColaProductView::labelVerkoopbaar(),
				ColaProductView::labelPrijs()));

			$cell = $row->addHeader(array(
					HtmlSpan::fa('chevron-up', _('Up'), 'text-muted'),
					HtmlSpan::fa('chevron-down', _('Down'), 'text-muted')
				));
			$cell->setAttribute('colspan', 2);

			$index = 0;
			foreach ($colaproducten as $c)
			{
				$omitup = $index == 0;
				$omitdown = $index == $colaproducten->aantal() - 1;
				$tbody->add(ColaProductView::colaProductTable($c, $omitup, $omitdown));
				$index++;
			}
			$tablediv->add($form);
		}
		else
		{
			$tablediv = new HtmlDiv(new HtmlParagraph(_('Er zijn geen colaproducten gevonden.')));
		}

		$par = new HtmlDiv();
		$par->add(ColaProductView::nieuwColaproductLink(_('Maak nieuw colaproduct aan')))
			->add(new HtmlBreak())
			->add(new HtmlBreak());

		$par->add(HtmlAnchor::button(DIBSBASE . '/Colaproducten/Overzicht', 'Overzicht van verkochte producten'));

		$tablediv->add($par);

		return $tablediv;
	}

	/**
	 * Functie voor een overzichtpagina.
	 *
	 * @see Dibs_Controller::colaproductOverzicht()
	 * aangeroepen door Dibs_Controller::colaproductOverzicht
	 *
	 * @return HTMLPage
	 */
	static public function overzicht (ColaProductVerzameling $colaproducten) {
		$page = Page::getInstance()
			->start(_('Colaproductoverzicht'));
		$body = new HtmlDiv();
		$body->add(self::colaProductTabel($colaproducten));
		$page->add($body);

		return $page;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
