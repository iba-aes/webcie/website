<?
/**
 * $Id$
 */
abstract class DibsInfoVerzamelingView
	extends DibsInfoVerzamelingView_Generated
{
	static public function tabel (DibsInfoVerzameling $dibsers) {
		$tablediv = new HtmlDiv();
		$tablediv->add(new HtmlHeader(3, _('Alle DiBSers op een rijtje')));

		$aantal = $dibsers->aantal();
		$buttons = $aantal > 30;
		if ($buttons)
		{
			$script = HtmlScript::makeJavascript("var rowid = 0; var alles=0;
			function showRow() {
				var row = document.getElementById('displayRow' + rowid);
				if (row.style.display == 'none') row.style.display = '';
			}

			function hideRow() {
				var row = document.getElementById('displayRow' + rowid);
				if (row.style.display == '') row.style.display = 'none';
			}
		
			function nextRow() {
				hideRow();
				rowid++;
				if (!document.getElementById('displayRow' + rowid))
					rowid--;
				showRow();
				tmp1 = 30*rowid+1;
				tmp2 = Math.min(30*(rowid+1),$aantal);
				document.getElementById('textid').innerHTML = 'Je ziet ' + tmp1 + ' tot ' + tmp2 + ' van de $aantal';
			}
			
			function previousRow() {
				hideRow();
				rowid--;
				if (rowid<0)
					rowid++;
				showRow();
				tmp1 = 30*rowid+1;
				tmp2 = Math.min(30*(rowid+1),$aantal);
				document.getElementById('textid').innerHTML = 'Je ziet ' + tmp1 + ' tot ' + tmp2 + ' van de $aantal';
			}
		
			function firstRow() {
				hideRow();
				rowid=0;
				showRow();
				tmp1 = 1;
				tmp2 = Math.min(30,$aantal);
				document.getElementById('textid').innerHTML = 'Je ziet ' + tmp1 + ' tot ' + tmp2 + ' van de $aantal';
			}

			function lastRow() {
				hideRow();
				while(document.getElementById('displayRow' + rowid))
					rowid++;
				rowid--;
				showRow();
				tmp1 = 30*rowid+1;
				tmp2 = $aantal;
				document.getElementById('textid').innerHTML = 'Je ziet ' + tmp1 + ' tot ' + tmp2 + ' van de $aantal';
			}
		
			function showAll() {
				if(!alles) {
					hideRow();
					rowid=0;
					while(document.getElementById('displayRow' + rowid)) {
						showRow();
						rowid++;
					}
					document.getElementById('textid').innerHTML = 'Je ziet allesch!';
					document.getElementById('showall').value = 'Laat niet allesch zien';
					alles=1;
				} else {
					rowid=0;
					while(document.getElementById('displayRow' + rowid)) {
						hideRow();
						rowid++;
					}
					rowid=0;
					showRow();
					firstRow();
					document.getElementById('showall').value = 'Laat allesch zien';
					alles=0;
				}
			}");
			$tablediv->add($script);

			$tablediv->add($form = new HtmlForm());

			$form->add(HtmlInput::makeButton('<<', '<<', 'firstRow()'));
			$form->add(HtmlInput::makeButton('<', '<', 'previousRow()'));

			$form->add($strong = new HtmlStrong(sprintf(_('Je ziet %s tot %s van de %s'), 1, min(30,$aantal), $aantal)));
			$strong->setAttribute('id', 'textid');

			$form->add(HtmlInput::makeButton('>', '>', 'nextRow()'));
			$form->add(HtmlInput::makeButton('>>', '>>', 'lastRow()'));

			$form->add($but = HtmlInput::makeButton('showalli', 'Laat allesch zien', 'showAll()'));
			$but->setAttribute('id', 'showall');
		}

		$tablediv->add($table = new HtmlTable());

		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());
		$tbody->setAttribute('id','displayRow0');
		$thead->add($row = new HtmlTableRow());
		$row->makeHeaderFromArray(array(
			DibsInfoView::labelPersoon(),
			DibsInfoView::labelPersoonID(),
			DibsInfoView::labelPas(),
			DibsInfoView::labelKudos(),
			DibsInfoView::labelLaatste()));

		if (hasAuth('god'))
		{
			$row->addHeader(_('Gewijzigd wanneer'));
			$row->addHeader(_('Gewijzigd wie'));
		}

		$count = 0;
		$id = 1;
		foreach ($dibsers as $d)
		{
			if ($count == 30)
			{
				$count = 0;
				$table->add($tbody = new HtmlTableBody());
				$tbody->setAttribute('id', "displayRow$id");
				$tbody->setAttribute('style', 'display: none');
				$id++;
			}
			$count++;
			$row = DibsInfoView::toTable($d);
			$tbody->add($row);
		}

		return $tablediv;
	}

	static public function zoeken (DibsInfoVerzameling $dibsers = null, $str = null)
	{
		$container = null;
		if ($dibsers !== null) {
			$dibsers->sorteer('naam');
			$container = self::tabel($dibsers);
		}

		$page = Page::getInstance();

		$form = new HtmlForm();
		$form->addClass('form-inline');
		$form->setName('DibsInfoVerzameling')
			 ->setAttribute('onsubmit', 'DibsInfo_Zoeken_zoek(true); return false')
			 ->add(HtmlInput::makeSearch('DibsInfoVerzamelingZoeken', ($str) ? $str : '')
					->setAttribute('onkeyup', 'DibsInfo_Zoeken_zoek(false)'))
			 ->add(HtmlInput::makeSubmitButton('Zoeken'))
			 ->setToken(null); //geen token voor deze post-form

		$page->start(_('Zoeken'))
			 ->add(new HtmlParagraph(_('Hier kun je zoeken op naam, lidnummer of pasnummer.')))
			 ->add($form)
			 ->add(new HtmlBreak());

		if ($container)
			$page->add(new HtmlDiv($container, null, 'DibsInfo_zoekContainer'));

		return $page;
	}

	static public function quickform ($dibsers, $str, $id)
	{
		$container = null;
		if ($dibsers !== null) {
			$dibsers->sorteer('naam');
			$container = self::lijstQuickform($dibsers);
		}

		$page = Page::getInstance();

		$form = new HtmlForm();
		$form->addClass('form-inline');
		$form->setName('PersoonQuickform')
			 ->setAttribute('onsubmit', 'Persoon_ZoekenQuickform_zoek(true); return false')
			 ->add(HtmlInput::makeSearch('DibsInfoVerzamelingZoekenQuickform', ($str) ? $str : '')
				 ->setAttribute('onkeyup', 'Persoon_ZoekenQuickform_zoek(false)'))
			 ->add(HtmlInput::makeSubmitButton('Zoeken'))
				 ->setToken(null); //geen token voor deze post-form

		$page->start()
			 ->add(new HtmlParagraph(_('Hier kun je snel zoeken om iemand aan te melden voor het DiBS.')))
			 ->add($form)
			 ->add(new HtmlBreak())
			 ->add($container ? (new HtmlDiv($container, null, 'DibsInfo_zoekContainer')) : new HtmlDiv());
		if ($id) {
			$info = DibsInfo::geef($id);
			$page->add(new HtmlHeader(2, sprintf(_('Dibsinformatie over %s'),
				new HtmlAnchor($info->getPersoon()->url(), DibsInfoView::waardePersoon($info)))));
			$page->add(DibsInfoView::toonInfo($info));
		}

		return $page;
	}
	
	public static function lijstQuickform(PersoonVerzameling $dibsers)
	{
		$return = new HtmlElementCollection();
		if ($dibsers->aantal() == 0)
			$return->add(new HtmlSpan(_('Geen resultaten gevonden.')));
		if ($dibsers->aantal() == 1) {
			spaceRedirect(DIBSBASE . '/Dibsinfo/Nieuw?persoonID=' . $dibsers->first()->getContactID());
		} else {
			$body = new HtmlElementCollection();

			$body->add(new HtmlHeader(3, _('Het resultaat van leden die nog niet kunnen DiBSen')));
			$body->add($table = new HtmlTable(array($thead = new HtmlTableHead(), $tbody = new HtmlTableBody())));

			$thead->add($trow = new HtmlTableRow());
			$trow->makeHeaderFromArray(array(
				_('Naam'),
				_('Lidnr'),
				_('Lidtoestand')));

			foreach ($dibsers as $dibser) {
				if (DibsInfo::geef($dibser))
					continue;
				$tbody->add($trow = new HtmlTableRow());
				$trow->addData(new HtmlAnchor(
					DIBSBASE . '/Dibsinfo/Nieuw?persoonID=' . $dibser->getContactID()
					, PersoonView::naam($dibser)));
				$trow->addData(PersoonView::waardeContactID($dibser));
				$trow->addData(LidView::waardeLidToestand(Lid::geef($dibser->getContactID())));
			}
			$return->add($body);
		}
		return $return;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
