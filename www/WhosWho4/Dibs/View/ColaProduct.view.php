<?
/**
 * $Id$
 */
abstract class ColaProductView
	extends ColaProductView_Generated
{
	public function __construct()
	{
		parent::__construct(); // ColaProduct_Generated
	}

	static public function tableRow($cp)
	{
		$tr = parent::tableRow($cp);
		$tr->addData(self::waardeVolgorde($cp));
		return $tr;
	}

	static public function tableHeaderRow()
	{
		$tr = parent::tableHeaderRow();
		$tr->addHeader(_('Volgorde'));
		return $tr;
	}

	/**
	 * @brief Toon een enkel ColaProduct.
	 *
	 * @param obj het ColaProduct om te tonen.
	 */
	static public function toon (ColaProduct $obj)
	{
		$voorraad = $obj->getDefaultVoorraad();
		$prijs = $voorraad->getVerkoopPrijs();

		$body = new HtmlDiv();

		$body->add(new HtmlDiv(new HtmlDiv($list = new HtmlList(), 'col-md-6'), 'row'));

		$list->addChild(sprintf('%s: %s', static::labelNaam($obj), static::waardeNaam($obj)));
		$list->addChild(sprintf('%s: %s', static::labelVerkoopbaar($obj), VoorraadView::waardeVerkoopbaar($voorraad)));
		$list->addChild(sprintf('%s: %s', static::labelPrijs($obj), VerkoopPrijsView::waardePrijs($prijs)));
		$list->addChild(sprintf('%s: %s', static::labelVolgorde($obj), static::waardeVolgorde($obj)));

		$body->add(static::plaatje($obj));
		$body->add(new HtmlBreak())
			->add(new HtmlBreak());

		$barcodes = $obj->getDefaultVoorraad()->getBarcodes();
		$body->add(new HtmlStrong(_('Barcodes') . ':'));
		if($barcodes) {
			$body->add(new HtmlBreak());
			foreach($barcodes as $b) {
				$body->add(new HtmlSpan(BarcodeView::waardeBarcode($b)));
				$body->add(new HtmlBreak());
			}
		}

		$body->add($div = new HtmlDiv(null, 'btn-group'));
		$div->add(HtmlAnchor::button($obj->url('UploadPlaatje'), _('Upload een plaatje')))
			->add(HtmlAnchor::button($obj->url('VoegBarcodeToe'), _('Voeg een barcode toe')))
			->add(HtmlAnchor::button(DIBSBASE . '/Colaproducten', _('Terug naar de colaproducten')));

		$page = Page::getInstance()
			 ->setEditUrl($obj->url() . '/Wijzig')
			 ->start(_('Product') . ' ' . static::waardeNaam($obj))
			 ->add($body);

		return $page;
	}

	/**
	 * @brief Converteer een ColaProduct naar een HtmlTableRow.
	 *
	 * @param c het ColaProduct om te converteren.
	 * @param omitup of het pijltje naar boven moet worden weergegeven.
	 * @param omitdown of het pijltje naar beneden moet worden weergegeven.
	 *
	 * @return een HtmlTableRow met de naam, verkoopbaarheid en huidige prijs
	 * van het product en mogelijk een of twee pijltjes om de volgorde te
	 * veranderen.
	 */
	static public function colaProductTable (ColaProduct $c, $omitup = false, $omitdown = false)
	{
		$voorraad = $c->getDefaultVoorraad();
		$prijs = $voorraad->getVerkoopPrijs();

		$row = new HtmlTableRow();
		$row->addData(new HtmlAnchor($c->url(), static::waardeNaam($c)));
		$row->addData(new HtmlAnchor($c->url('WijzigVerkoopbaar'),
			HtmlSpan::maakBoolPicto($c->getVerkoopbaar())
		));
		$row->addData(VerkoopPrijsView::waardePrijs($prijs));
		$row->addData($omitup
			? NULL
			: new HtmlAnchor($c->url('WijzigVolgorde?r=-1'), HtmlSpan::fa('chevron-up', _('Up'), 'text-muted')));
		$row->addData($omitdown
			? NULL
			: new HtmlAnchor($c->url('WijzigVolgorde?r=1'), HtmlSpan::fa('chevron-down', _('Down'), 'text-muted')));
		return $row;
	}

	/**
	 * @brief Maak een link naar de pagina waarop een nieuw ColaProduct kan worden
	 * aangemaakt.
	 *
	 * @param titel de tekst van de link.
	 * @return een HtmlAnchor naar de pagina voor een nieuwe ColaProduct.
	 */
	static public function nieuwColaproductLink ($titel) {
		return HtmlAnchor::button(DIBSBASE . '/Colaproducten/Nieuw', $titel);
	}

	/**
	 * @brief Maak een pagina om een ColaProduct te maken of te wijzigen.
	 *
	 * @see wijzigForm
	 * @param ColaProduct $obj het ColaProduct dat gewijzigd moet worden, of, als het nog
	 * niet in de database staat, aangemaakt moet worden.
	 * @param bool $show_error Bool of de error moet worden laten zien.
	 *
	 * @return HTMLPage
	 */
	static public function toevoegen (ColaProduct $obj, $show_error)
	{
		if($obj->getInDB()) {
			$starth = _('Wijzig') . ' ' . static::waardeNaam($obj);
		} else {
			$starth = _('Maak colaproduct');
		}

		$page = Page::getInstance()
			->start($starth, 'colaproducten');
		$page->add(self::wijzigForm('Nieuw Colaproduct', $obj, $show_error));
		return $page;
	}

	/**
	 * @brief Maak een form om een ColaProduct aan te maken of te wijzigen.
	 *
	 * @param name de naam van het form.
	 * @param obj het Colaproduct dat gewijzigd moet worden. Indien nit NULL is
	 * wordt er een nieuw ColaProduct aangemaakt.
	 * @param show_error indien True worden errors in het form getoond.
	 *
	 * @return HtmlForm voor het maken of wijzigen van een ColaProduct.
	 */
	static public function wijzigForm ($name = 'Nieuw Colaproduct', $obj = null, $show_error = true) {
		if (is_null($obj))
		{
			$obj = new ColaProduct();
			$nieuw = true;
		}
		$voorraad = $obj->getDefaultVoorraad();

		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($obj, 'naam', $show_error));
		$div->add(self::wijzigTR($obj, 'verkoopbaar', $show_error));

		if(!$obj->getInDB()) {
			$div->add(self::wijzigTR($obj, 'prijs', $show_error));
		}

		$form->add(HtmlInput::makeFormSubmitbutton(_('Genereer!')));
		return $form;
	}

	/**
	 * @brief Verwerk een ColaProduct-form door velden uit de GET of POST te halen.
	 *
	 * @return een associatieve array die de naam, verkoopbaarheid en prijs (in
	 * kudos) bevat.
	 */
	static public function processNieuwColaProductForm ($obj)
	{
		self::processForm($obj);
		$arr = array();
		$arr['Verkoopbaar'] = tryPar('Verkoopbaar');
		$arr['Prijs'] = tryPar('Prijs')/ EURO_TO_KUDOS;
		return $arr;
	}

	/**
	 * @param Barcode $obj
	 * @param string $msg Een foutmelding die bovenaan de pagina wordt getoond.
	 *
	 * @return null|Page
	 */
	static public function toevoegenBarcode(Barcode $obj, $msg) {
		$page = Page::getInstance()
			->start('Toevoegen barcode')
			->add(new HtmlHeader(2, 'Toevoegen barcode'));
		if ($msg !== null)
			$page->add($msg);
		if ($msg == null)
			$page->add(self::nieuweBarcodeForm('Nieuwe Barcode', $obj, false));
		else
			$page->add(self::nieuweBarcodeForm('Nieuwe Barcode', $obj, true));

		return $page;
	}

	static public function nieuweBarcodeForm($naam = 'Nieuwe Barcode', $obj, $show_error = false) {
		if (is_null($obj))
			$obj = new Barcode();

		$form = HtmlForm::named($naam);

		$form->add($div = new HtmlDiv());
		$div->add($table = new HtmlTable());
		$div->add(HtmlInput::makeText('barcode'));
		$form->add(HtmlInput::makeSubmitbutton(_('Genereer!')));

		return $form;
	}

	/**
	 * @brief Geef de URL waarop het plaatje van een ColaProduct te zien is.
	 *
	 * @param product het ColaProduct waarvan een plaatje wordt gemaakt.
	 * @return Een string met de link naar het plaatje.
	 */
	static public function plaatjeUrl(ColaProduct $product) {
		return DIBSBASE . '/ColaProductPlaatjes/' . $product->getArtikelID() . '.jpg';
	}

	/**
	 * @brief Maak een HtmlImage van een ColaProduct.
	 *
	 * @param product het ColaProduct waarvan een plaatje wordt gemaakt.
	 * @return een HtmlImage die het ColaProduct representeert.
	 */
	static public function plaatje(ColaProduct $product)
	{
		$img = new HtmlImage(static::plaatjeUrl($product),
			static::waardeNaam($product));
		$img->setLazyLoad();
		$img->setAttribute('style', 'height: 100px');
		return $img;
	}

	/**
	 * @brief Maak een div waarin een plaatje voor een ColaProduct kan worden
	 * geüpload.
	 *
	 * @param product het ColaProduct waarvoor een nieuw plaatje kan worden
	 * geüpload.
	 * @return een HtmlDiv die een formulier bevat om een plaatje te uploaden.
	 */
	static public function uploadPlaatje (ColaProduct $product) {
		$id = static::waardeArtikelID($product);

		$div = new HtmlDiv();
		$div->add(HtmlAnchor::button($product->url(), _('Ga terug')));

		$form = HtmlForm::named('plaatjeForm');
		$form->add(HtmlInput::makeHidden('artikelID', $id));
		$form->add(HtmlInput::makeHidden("stage","confirm"));
		$form->add(HtmlInput::makeHidden("MAX_FILE_SIZE","102400"));
		$form->add(HtmlInput::makeFile("userfile"));
		$form->add(HtmlInput::makeSubmitButton(_("Upload file")));

		$div->add(new HtmlParagraph(sprintf(_('Het uploaden van een plaatje voor %s kan hier. Je plaatje moet voldoen aan de volgende eisen:'),
				static::waardeNaam($product))))
			->add(new HtmlList(false, array(
				sprintf(_('Het format van het plaatje moet jpeg zijn;'), $id),
				_('Het bestand mag niet groter zijn dan 100 KB.')
			)))
			->add(new HtmlParagraph(_('Als de test lukt, word je automatisch doorverwezen naar de publisher.')))
			->add($form)
			->add(new HtmlBreak());

		return $div;
	}

	static public function labelVerkoopbaar(ColaProduct $obj = NULL)
	{
		return _('Verkoopbaar');
	}
	static public function labelPrijs(ColaProduct $obj = NULL)
	{
		return _('Prijs');
	}
	static public function labelVolgorde(ColaProduct $obj = NULL)
	{
		return _('Volgorde');
	}

	static public function opmerkingNaam ()
	{
		return _('De naam van het product');
	}
	static public function opmerkingVerkoopbaar ()
	{
		return _('Is het product verkoopbaar?');
	}
	static public function opmerkingPrijs ()
	{
		return _('Hoeveel kudos (eurocent) het product kost');
	}

	static public function formVerkoopbaar($obj)
	{
		$voorraad = $obj->getDefaultVoorraad();
		return HtmlInput::makeCheckbox('Verkoopbaar',
			$voorraad ? VoorraadView::waardeVerkoopbaar($voorraad) : true);
	}

	static public function formPrijs($obj)
	{
		$voorraad = $obj->getDefaultVoorraad();
		$prijs = tryPar('Prijs', $voorraad ? $obj->getKudos() : 0);
		return HtmlInput::makeNumber('Prijs', $prijs, 0);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
