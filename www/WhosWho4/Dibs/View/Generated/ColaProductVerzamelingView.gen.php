<?
abstract class ColaProductVerzamelingView_Generated
	extends ArtikelVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ColaProductVerzamelingView.
	 *
	 * @param ColaProductVerzameling $obj Het ColaProductVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeColaProductVerzameling(ColaProductVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
