<?
abstract class DibsProductView_Generated
	extends ArtikelView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DibsProductView.
	 *
	 * @param DibsProduct $obj Het DibsProduct-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDibsProduct(DibsProduct $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld kudos.
	 *
	 * @param DibsProduct $obj Het DibsProduct-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld kudos labelt.
	 */
	public static function labelKudos(DibsProduct $obj)
	{
		return 'Kudos';
	}
	/**
	 * @brief Geef de waarde van het veld kudos.
	 *
	 * @param DibsProduct $obj Het DibsProduct-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kudos van het object obj
	 * representeert.
	 */
	public static function waardeKudos(DibsProduct $obj)
	{
		return static::defaultWaardeInt($obj, 'Kudos');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kudos.
	 *
	 * @see genericFormkudos
	 *
	 * @param DibsProduct $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kudos staat en kan worden
	 * bewerkt. Indien kudos read-only is betreft het een statisch html-element.
	 */
	public static function formKudos(DibsProduct $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Kudos', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kudos. In
	 * tegenstelling tot formkudos moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formkudos
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kudos staat en kan worden
	 * bewerkt. Indien kudos read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormKudos($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Kudos');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kudos
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kudos representeert.
	 */
	public static function opmerkingKudos()
	{
		return NULL;
	}
}
