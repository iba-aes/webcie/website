<?
abstract class DibsInfoVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DibsInfoVerzamelingView.
	 *
	 * @param DibsInfoVerzameling $obj Het DibsInfoVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDibsInfoVerzameling(DibsInfoVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
