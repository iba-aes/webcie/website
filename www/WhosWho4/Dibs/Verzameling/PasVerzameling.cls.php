<?
/**
 * $Id$
 */
class PasVerzameling
	extends PasVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PasVerzameling_Generated
	}

	static public function getPersoonPassen(Persoon $persoon)
	{
		global $WSW4DB;
		$query = 'COLUMN SELECT `Pas`.`pasID`'
			   . ' FROM  `Pas`'
			   . ' WHERE `persoon_contactID` = %i'
			   . ' ORDER BY `pasID` ASC';

		$id = $persoon->getContactID();

		$ids = $WSW4DB->q($query, $id);
		return self::verzamel($ids);
	}

	static public function getPassen() {
		global $WSW4DB;

		$result = $WSW4DB->q('COLUMN SELECT `Pas`.`pasID` FROM `Pas`');
		return self::verzamel($result);
	}

	static public function sorteerOpLidnr ($aID, $bID) {
		$a = (int)Pas::geef($aID)->getPersoonContactID();
		$b = (int)Pas::geef($bID)->getPersoonContactID();

		return ($a - $b);
	}
	
	static public function sorteerOpPasnr ($a, $b) {
		return ($a - $b);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
