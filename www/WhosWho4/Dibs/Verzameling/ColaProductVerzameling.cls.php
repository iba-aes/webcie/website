<?
/**
 * $Id$
 */
class ColaProductVerzameling
	extends ColaProductVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ColaProductVerzameling_Generated
	}

	static public function alle () { return self::getColaproducten(); }

	/**
	 * Haalt alle colaproducten op
	 *
	 * @return ColaProductVerzameling
	 */
	static public function getColaproducten () {
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `ColaProduct`.`artikelID` FROM `ColaProduct`
			ORDER BY `ColaProduct`.`Volgorde` ASC');
		return self::verzamel($ids);
	}

	static public function sorteerOpVolgorde ($aID, $bID) {
		$a = (int)ColaProduct::geef($aID)->getVolgorde();
		$b = (int)ColaProduct::geef($bID)->getVolgorde();

		return ($a - $b);
	}

	static public function getActieveColaProducten () {
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `ColaProduct`.`artikelID`'
			.' FROM `ColaProduct`'
			.' LEFT JOIN `Voorraad` ON `ColaProduct`.`artikelID` = `Voorraad`.`artikel_artikelID`'
			.' WHERE `Voorraad`.`verkoopbaar` = 1'
		);
		return self::verzamel($ids);
	}

	static public function vindHoogsteVolgorde () {
		global $WSW4DB;
		$volg = $WSW4DB->q('MAYBEVALUE SELECT MAX(`volgorde`) FROM `ColaProduct`');
		if ($volg)
			return $volg;

		return 0;
	}

	static public function geefMetVoorraad () {
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Voorraad`.`artikel_artikelID`'
			.' FROM `Voorraad`'
			.' LEFT JOIN `ColaProduct` ON `Voorraad`.`artikel_artikelID` = `ColaProduct`.`artikelID`'
		);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
