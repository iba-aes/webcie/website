<?
/**
 * $Id$
 */
class DibsProductVerzameling
	extends DibsProductVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DibsProductVerzameling_Generated
	}

	static public function alle () { return self::getDibsproducten(); }
	
	static public function getDibsproducten () {
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `DibsProduct`.`artikelID` FROM `DibsProduct`');
		return self::verzamel($ids);
	}

	static public function verkoopbaar()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `DibsProduct`.`artikelID`'
			.' FROM `DibsProduct`'
			.' LEFT JOIN `Voorraad` ON `Voorraad`.`artikel_artikelID` = `DibsProduct`.`artikelID`'
			.' WHERE `Voorraad`.`verkoopbaar` = 1'
		);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
