<?
/**
 * $Id$
 */
class DibsTransactieVerzameling
	extends DibsTransactieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DibsTransactieVerzameling_Generated
	}

	public static function getTotalen ($van, $tot)
	{
		global $WSW4DB;
		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);
		$ret = array();

		foreach (array('<', '>') as $symb)
		{
			$query = 'VALUE SELECT SUM(`Transactie`.`bedrag`) FROM `Transactie`
					WHERE `Transactie`.`wanneer` >= %s AND `Transactie`.`wanneer` < %s
					AND `Transactie`.`bedrag` ' . $symb . ' 0 
					AND `Transactie`.`overerving` = \'DibsTransactie\'';

			$ret[] = $WSW4DB->q($query, $van, $tot);
		}
		$ret[] = $ret[0] + $ret[1];

		foreach (array('BVK', 'IDEAL') as $bron)
		{
			$query = 'VALUE SELECT SUM(`Transactie`.`bedrag`) FROM
				`Transactie` JOIN `DibsTransactie`
				ON `Transactie`.`transactieID`=`DibsTransactie`.`transactieID`
				WHERE `Transactie`.`wanneer` >= %s AND `Transactie`.`wanneer` < %s';

			if (!is_null($bron))
				$query .= ' AND `DibsTransactie`.`bron` = %s';

			$ret[] = $WSW4DB->q($query, $van, $tot, $bron);
		}
		$ret[] = $ret[3] + $ret[4];

		return $ret;
	}

	// Vraag het bedrag in omloop op de gegeven datum op
	static public function getBedragBegin ($datum = 0)
	{
		global $WSW4DB;

		$datum = ($datum instanceof DateTimeLocale) ? $datum->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $datum);
		$value = $WSW4DB->q('VALUE SELECT SUM(`Transactie`.`bedrag`) FROM `Transactie`
			WHERE `Transactie`.`overerving` = \'DibsTransactie\' AND `Transactie`.`wanneer` < %s',
			$datum);

		return $value;
	}

	/**
	 * Vraag alle transacties op tussen twee momenten.
	 *
	 * @param DateTimeLocale|int $van Het begin van het interval. Indien int, is het een unix-timestamp.
	 * @param DateTimeLocale|int $tot Het einde van het interval. Indien int, is het een unix-timestamp.
	 *
	 * @return DibsTransactieVerzameling
	 */
	static public function getTransacties ($van = 0, $tot = 0)
	{
		global $WSW4DB;
		$query = 'COLUMN SELECT `Transactie`.`transactieID`'
			  . ' FROM `Transactie`'
			  . ' WHERE `overerving` = \'DibsTransactie\''
			  . ' AND `wanneer` >= %s'
			  . ' AND `wanneer` < %s'
			  . ' ORDER BY `wanneer` ASC';

		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);

		$ids = $WSW4DB->q($query, $van, $tot);
		return self::verzamel($ids);
	}

	/**
	 * Vraag het aantal transacties tussen twee momenten
	 */
	static public function telTransacties ($van = 0, $tot = 0)
	{
		global $WSW4DB;
		$query = 'VALUE SELECT COUNT(`Transactie`.`transactieID`)'
			  . ' FROM `Transactie`'
			  . ' WHERE `overerving` = \'DibsTransactie\''
			  . ' AND `wanneer` >= %s'
			  . ' AND `wanneer` < %s';

		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);

		return $WSW4DB->q($query, $van, $tot);
	}
	/**
	 * Vraag het aantal kudos gekocht door leden
	 * (dus in principe het aantal transacties met negatieve bedragen)
	 */
	static public function telKudosGekocht($van = 0, $tot = 0)
	{
		global $WSW4DB;
		$query = 'VALUE SELECT SUM(`Transactie`.`bedrag`)'
			  . ' FROM `Transactie`'
			  . ' WHERE `overerving` = \'DibsTransactie\''
			  . ' AND `wanneer` >= %s'
			  . ' AND `wanneer` < %s'
			  . ' AND `bedrag` < 0';

		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);

		$totaalBedrag = $WSW4DB->q($query, $van, $tot);
		// Vermenigvuldig met -1 want we tellen "uitgaven"
		return -DibsTransactie::euroToKudos($totaalBedrag);
	}
	/**
	 * Vraag het aantal kudos betaald door leden
	 * (dus in principe het aantal transacties met positieve bedragen)
	 */
	static public function telKudosBetaald($van = 0, $tot = 0)
	{
		global $WSW4DB;
		$query = 'VALUE SELECT SUM(`Transactie`.`bedrag`)'
			  . ' FROM `Transactie`'
			  . ' WHERE `overerving` = \'DibsTransactie\''
			  . ' AND `wanneer` >= %s'
			  . ' AND `wanneer` < %s'
			  . ' AND `bedrag` > 0';

		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);

		$totaalBedrag = $WSW4DB->q($query, $van, $tot);
		return DibsTransactie::euroToKudos($totaalBedrag);
	}

	// Vraag alle transacties van een bepaald persoon op
	static public function getPersoonTransacties (Persoon $persoon, $aantal = null)
	{
		global $WSW4DB;

		if (is_int($aantal) && $aantal > 0)
			$limit = ' LIMIT %i';
		else
			$limit = '';

		$query = 'COLUMN SELECT `Transactie`.`transactieID`'
			   . ' FROM  `Transactie`'
			   . ' WHERE `overerving` = \'DibsTransactie\' AND `contact_contactID` = %i'
			   . ' ORDER BY `wanneer` DESC'
			   . $limit;

		$id = $persoon->getContactID();

		$ids = $WSW4DB->q($query, $id, $aantal);
		return self::verzamel($ids);
	}

	static public function getPasTransacties (Pas $pas)
	{
		global $WSW4DB;
		$query = 'COLUMN SELECT `DibsTransactie`.`transactieID`'
			   . ' FROM  `DibsTransactie`'
			   . ' WHERE `DibsTransactie`.`pas_pasID` = %s';

		$id = $pas->getPasID();

		$ids = $WSW4DB->q($query, $id);
		return self::verzamel($ids);
	}

	static public function arrayOfTransacties($cum = true, $kudos = false, $kudosverkocht = false)
	{
		global $WSW4DB;

		//Haal de data op
		if ($kudos)
			if ($kudosverkocht)
				$data = $WSW4DB->q('TABLE SELECT (-100 * SUM(`bedrag`)) as id, YEAR(`wanneer`) as year, MONTH(`wanneer`) as month '
					. 'FROM `Transactie` '
					. 'WHERE `overerving` = \'DibsTransactie\' '
					. 'AND `bedrag` < 0 '
					. 'GROUP BY YEAR(`wanneer`), MONTH(`wanneer`)');
			else
				$data = $WSW4DB->q('TABLE SELECT (100 * SUM(`bedrag`)) as id, YEAR(`wanneer`) as year, MONTH(`wanneer`) as month '
					. 'FROM `Transactie` '
					. 'WHERE `overerving` = \'DibsTransactie\' '
					. 'AND `bedrag` > 0 '
					. 'GROUP BY YEAR(`wanneer`), MONTH(`wanneer`)');
		else
			if($kudosverkocht)
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as id, YEAR(`wanneer`) as year, MONTH(`wanneer`) as month '
					. 'FROM `Transactie` '
					. 'WHERE `overerving` = \'DibsTransactie\' '
					. 'AND `bedrag` < 0 '
					. 'GROUP BY YEAR(`wanneer`), MONTH(`wanneer`)');
			else
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as id, YEAR(`wanneer`) as year, MONTH(`wanneer`) as month '
					. 'FROM `Transactie` '
					. 'WHERE `overerving` = \'DibsTransactie\' '
					. 'AND `bedrag` > 0 '
					. 'GROUP BY YEAR(`wanneer`), MONTH(`wanneer`)');

		//Reverse de array zodat we de laatste twaalf elementen van $data kunnen laten zien
		$data = array_reverse($data);

		$tijd = new DateTimeLocale(date('Y') . '-' . date('m'));
		$res = array();
		$cumul = 0;

		//Bereken het cumulative aantal
		if ($cum) {
			foreach ($data as $d) {
				if (array_search($d, $data) == 12)
					break;
				$cumul += $d["id"];
			}
		}

		if ($data[0]["month"] != date('m'))
			$row = array("id" => 0, "year" => date('Y'), "month" => date('n'));
		else
			$row = array_shift($data);

		for ($i = 0; $i < 12; $i++) {
			//Als er geen data meer is, is het aantal nul
			if (is_null($row)) {
				$res[($tijd->format('Y-m'))] = 0;
				$tijd->sub(new DateInterval('P1M'));
				continue;
			}

			$temp = new DateTimeLocale("$row[year]-$row[month]");
			if ($tijd == $temp) {
				$res[($temp->format('Y-m'))] = ($cum) ? $cumul : $row["id"];
				$cumul -= $row["id"];
				$row = array_shift($data);
			} else {
				$res[($tijd->format('Y-m'))] = ($cum) ? $cumul : $row["id"];
			}

			$tijd->sub(new DateInterval('P1M'));
		}

		// Return de array weer in goede volgorde
		return array_reverse($res);
	}

	/**
	 * Zoek de leden die niet sinds $tot een transactie op DiBS hebben gedaan.
	 * Omdat je waarschijnlijk wil weten wanneer dit was, returnen we de laatste transactie per lid.
	 * @returns De verzameling van laatste transacties per lid.
	 */
	public static function getLaatsteTransacties($tot) {
		global $WSW4DB;

		$query = 'COLUMN SELECT t.transactieID, t.wanneer'
		       . ' FROM Transactie t'
		       . ' INNER JOIN'
		       . '  (SELECT contact_contactID, MAX(wanneer) AS maxWanneer'
		       . '  FROM Transactie'
		       . '  GROUP BY contact_contactID) groupedt'
		       . ' ON t.contact_contactID = groupedt.contact_contactID'
		       . ' AND t.wanneer = groupedt.maxWanneer'
		       . ' WHERE t.wanneer < %s'
		       . ' ORDER BY t.wanneer ASC'
		       ;

		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);
		$ids = $WSW4DB->q($query, $tot);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
