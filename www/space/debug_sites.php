<?php
global $DEBUG_SITES;

// Doe het in een functie om de globale namespace niet te polluten.
// TODO: willen we dit samenvoegen met _laad_bestuurdata van de specialconstants?
function _laad_webcieledendata()
{
	global $DEBUG_SITES;
	$DEBUG_SITES = [];

	$configdata = yaml_parse_file(FS_ROOT . '/../config.yaml');

	foreach ($configdata['webcie'] as $webcielid)
	{
		$contactid = $webcielid['contactid'];
		$debugsite = $webcielid['debugsite'];
		$DEBUG_SITES[$contactid] = $debugsite;
	}
}
_laad_webcieledendata();
