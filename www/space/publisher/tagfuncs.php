<?php

/* Remove form tags from the input and prepent all the 'name' fields
 * with a nonameclash_
 *
 * This function is used by publisher and (should) prevent users from
 * breaking the UI.
 */
require_once(LIBDIR . '/simple_html_dom.php');

function disable_forms($txt)
{
	$txt = preg_replace('%<[/]?form[^>]*>%', '', $txt);
	$txt = preg_replace('%(<(button|input|select|textarea)[^>]*name=")([^"]*"[^>]*>)%', '$1nonameclash_$3', $txt);
	return $txt;
}

/*
 */
function replace_publisher_tags($txt)
{
	$ptrn = 'act';
	$txt = replace_tag($txt, $ptrn, 'nr', 'publisher_activiteit_link');

	$ptrn = 'actfotos';
	$txt = replace_tag($txt, $ptrn, 'nr', 'publisher_fotos_act_link');

	$ptrn = 'cie';
	$txt = replace_tag($txt, $ptrn, 'login', 'publisher_cie_link');

	$ptrn = 'ciefotos';
	$txt = replace_tag($txt, $ptrn, 'login', 'publisher_fotos_cie_link');

	$ptrn = 'foto';
	$txt = replace_tag($txt, $ptrn, 'id', 'publisher_foto_link');

	$ptrn = 'lid';
	$txt = replace_tag($txt, $ptrn, 'nr', 'publisher_lid_link');

	$txt = replace_img_src($txt);

	return $txt;
}

function replace_img_src($txt)
{
	$html = str_get_html($txt);
	if($html)
	{
		foreach($html->find('img') as $i => $elem)
		{
			if(!isset($elem->attr['src']))
				$elem->{'src'} = '';

			$elem->{'data-original'} = $elem->attr['src'];
			if(isset($elem->attr['class']))
				$elem->attr['class'] = 'lazy ' . $elem->attr['class'];
			else
				$elem->{'class'} = 'lazy';
			$elem->attr['src'] .= '?lowres=1';
		}
		return $html->save();
	}
}

function replace_tag($txt, $ptrn, $attr, $linkfunc)
{
	$html = str_get_html($txt);
	if($html)
	{
		foreach($html->find($ptrn) as $i => $elem)
		{
			$elem->tag = 'a';
			$elem->href = $linkfunc($elem->attr[$attr]);
			unset($elem->attr[$attr]);
		}

		return $html->save();
	}
}

/**
 * produceer de url van een lid gegeven door lidnummer
 */
function publisher_lid_link($lidnr)
{
	if(!($lid = Lid::geef($lidnr)) || !$lid->magBekijken())
		return null;

	return $lid->url();
}

/**
 * produceer de url van een commissie gegeven door loginnaam
 */
function publisher_cie_link($cielogin)
{
	$cie = Commissie::cieByLogin($cielogin);
	if (($cie instanceof Commissie) && $cie->magBekijken())
		return $cie->url();

	return null;
}

/**
 * produceer de url van een activiteit gegeven door activiteitnummer
 */
function publisher_activiteit_link($actid)
{
	$act = Activiteit::geef($actid);
	if ($act instanceof Activiteit && $act->magBekijken())
		return $act->url();
	
	return null;
}

/**
 * produceer de url van een foto (of eigenlijk allerlei soorten media) gegeven door id
 */
function publisher_foto_link($fotoid)
{
	$fotoid = (int)$fotoid;

	$fotoinfo = Media::geef($fotoid);
	if ((!$fotoinfo instanceof Media) || (!$fotoinfo->magBekijken())) {
		return null;
	}

	// FIXME: verzin waar we heen moeten linken
	// return $fotoinfo->url();
	return null;
}

/**
 * Produceer de url van de foto's van een activiteit gegeven door activiteitnummer
 */
function publisher_fotos_act_link($actnr)
{
	$actnr = (int)$actnr;
	$act = Activiteit::geef($actnr);
	if ((!$act instanceof Activiteit) || (!$act->magBekijken()))
		return null;

	// check of er eigenlijk wel foto's horen bij de act
	$fotos = MediaVerzameling::getTopratedByAct($act,1);
	if (count($fotos) <= 0) {
		return null;
	}

	return $act->fotourl();
}

/**
 * Produceer een url naar de foto's van een commissie gegeven door loginnaam
 */
function publisher_fotos_cie_link($cielogin)
{
	$cie = Commissie::cieByLogin($cielogin);
	if ((!$cie instanceof Commissie) && (!$cie->magBekijken()))
		return null;

	// check of er eigenlijk wel foto's horen bij de cie
	$fotos = MediaVerzameling::getTopratedbyCie($cie, 1);
	if (count($fotos) <= 0) {
		return null;
	}

	return $cie->fotoUrl();
}

