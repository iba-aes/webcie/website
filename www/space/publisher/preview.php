<?php
// $Id$

// return array met errors, geen nieuws is goed nieuws
function preview_content($content)
{
	$errors = array();
	if(empty($content))
		return $errors;

	$nsgmls = proc_open('nsgmls -s -c '.LIBDIR.'/sgml/catalog'
						, array( 0 => array('pipe', 'r')	// stdin
						       , 2 => array('pipe', 'w'))	// stderr
						, $pipes);
	if(!$nsgmls)
		user_error("kan nsgmls niet executeren", E_USER_ERROR) ;

	fwrite($pipes[0]
		, '<!DOCTYPE HTML PUBLIC "-//AES//DTD HTML//EN//1.0"'
		.	' "'.LIBDIR.'/sgml/aes.dtd">'."\n"
		. "<html>\n"
		. '<head>'
		.  '<title>title</title>'
		.  '<META http-equiv="Content-Type" content="text/html; charset=utf-8">'
		. "</head>\n"
		. "<body>\n"
		. $content
		. "</body>\n</html>"
		);
	fclose($pipes[0]);

	while($line = fgets($pipes[2], 1024)) {
		list($p, $f, $line, $char, $l, $mesg) = explode(':', $line, 6);
		//Als het geen E(error) of W(warning is dan hebben we maar 5 argumenten.
		if(!$mesg)
			$mesg = $l;
		# alleen melding over stdin
		if($f != '<OSFD>0')
			continue;

		$errors[] = array($line - 4, $char, $mesg);
	}
	fclose($pipes[2]);
	proc_close($nsgmls);
	return $errors;
}

function replaceAndSign($content){
	$tmp_result='';
	$exploded=explode('&', $content);
	
	$tmp_result.= array_shift($exploded);
	
	while(count($exploded)>0){
		$item = array_shift($exploded);
		$pos= strpos($item, ";");
		
		if ($pos!== false){
			$word = substr($item, 0, $pos);
		}
		if( $pos === false){
			// de & heeft geen bijkomende ';' dus moet gereplaced worden
			$tmp_result.='&amp;'.$item;
		} else if( count(preg_grep("/.*(\s+).*/", array($word)))>0 ){
			// de entity heeft een whitespace zitten tussen '&' en ';'
			$tmp_result.='&amp;'.$item;
		} else {
			//anders is het een entity die gecheckt mag worden door preview.
			$tmp_result.='&'.$item;
		}
	}
	return $tmp_result;
}
