<?php

class Purifier {
	static $config;
	static $purifier;

	static function purify($dirty_html) {
		// maak statische instances aan indien nuttig
		if (!self::$config) {
			self::$config = HTMLPurifier_Config::createDefault();
			// Cache geen dingen want dingen in je www-map schrijfbaar voor php maken
			// is echt een heel erg slecht idee
			self::$config->set("Cache.DefinitionImpl", null);
		}
		if (!self::$purifier) {
			self::$purifier = new HTMLPurifier(self::$config);
		}

		// schoon op!
		$clean_html = self::$purifier->purify($dirty_html);
		return $clean_html;
	}
};

?>
