<?php

// $Id$

// remove any trailing slashes
function vfsSlashCheck($content)
{
    if(empty($content)) {
        return '';
    }
    while(substr($content, -1) == '/') {
        $content = substr($content, 0, -1);
    }
    return $content;
}
