<?php

/**
 * @brief Probeer aan de hand van een url, het een entry te vinden.
 *
 * Indien er geen entry is met deze URL, is het resultaat null.
 *
 * Vereist dat de URL precies overeenkomt met de entry,
 * dus als er een PHP-haak in het pad staat, en er dan nog wat URL komt,
 * is het resultaat null. (Bijvoorbeeld /Vereniging/Commissies/webcie/Info geeft null,
 * want /Info wordt pas door de hook op /Vereniging/Commissies/webcie afgehandeld.)
 * Variabele entries werken wel zoals je verwacht:
 * /Leden/8566 geeft LedenController::LidEntry.
 *
 * @see geefEntryMetURL
 *
 * @param href String met uri om te parsen
 * @param entry vanaf waar dieper zoeken (als $href met een '/' begint wordt er automatisch terug gevallen op $tree)
 * @param tree name van de tree
 *
 * @return Maybe de entry die overeenkomt met de gegeven URL.
 */
function hrefToEntry($href, $entry=null, $tree=null)
{
	$ref = hrefToReference($href, '', $entry, $tree);
	if(!empty($ref['remaining'])) {
		return NULL;
	}
	return $ref['entry'];
}

/**
 * @param href uri (als string) om te parsen
 * @param params parameters die we hadden (wordt mogelijk door getLink() uitgebreid)
 * @param entry vanaf waar dieper zoeken (als $href met een '/' begint wordt er automatisch terug gevallen op $tree)
 * @param tree name van de tree
 * @param rewrite zijn we een keer via een getLink() gegaan (zoja moet space straks rewriten)
 * @param show_file_path bool of het pad moet worden laten zien
 * @param request De URL die in de originele aanroep is meegegeven. Default: href.
 *
 * @return array met de volgende velden:
 *		entry		de diepst entry die we konden vinden
 *		remaining	het stuk $href wat niet meer matchde
 *		rewrite		zijn we via een getLink()
 *		access		mag je er wel bij?
 *		params		associatieve array met GET-parameters
 *		request		De URL die in de originele aanroep is meegegeven.
 */
function hrefToReference($href, $params=[], $entry=null, $tree=null, $rewrite=false, $show_file_path = false, $request = null)
{
	global $BMDB;

	// De recursieve aanroep geeft $request mee om de originele URL te bewaren,
	// dus als die niet geset is, zijn we bij de originele aanroep.
	if (is_null($request))
	{
		$request = $href;
	}

	$rootId = getTreeId($tree);

	if(!$entry)
		$entry = vfs::get($rootId);

	if(substr($href, 0, 1) == '/') {
		if($entry->getId() != $rootId) {
			$entry = vfs::get($rootId);
			$rewrite = true;
		}
		$href = substr($href, 1);
	}

	// We hebben al een resultaat als we niet in recursie gaan:
	// geef gewoon de entry waar we al zijn.
	$result = [
		'access' => true,
		'entry' => $entry,
		'remaining' => $href,
		'request' => $request,
		'rewrite' => $rewrite,
		'params' => $params
	];

	// Early exit: als de (root-)entry geen children heeft,
	// of dat we geen child meer hoeven, dan zijn we klaar.
	if(!$entry->isDir() || empty($href))
	{
		return $result;
	}

	// Split $href op eerstvolgende '/', zodat $name de naam van het kind is en $rest het pad is onder het kind.
	$pos = strpos($href, '/');
	if($pos === False) {
		$name = $href;
		$rest = '';
		$slash = False;
	} else {
		$name = substr($href, 0, $pos);
		$rest = substr($href, $pos + 1);
		$slash = True;
	}

	// Zie bug #7501 voor meer info
	// Zie E1525187739 over info over het gebruik van urldecode en mb_strtolower
	$name = urldecode($name);

	// Probeer een entry dieper te gaan in de recursie.
	// Dit kan op twee manieren: of er is een child met letterlijke naam,
	// of die is er niet en we proberen een variabele entry.
	$child = $entry->getChild($name);
	if(!$child || $child instanceof vfsEntryVariable)
	{
		// Probeer via variabele entry:
		$child = $entry->getChild('*');

		if ($child && !$child->checkUrl($name))
		{
			// Helaas, de variabele entry lust de URL niet.
			$result['access'] = false;
			return $result;
		}
	}

	if(!$child)
	{
		// We hebben geen child dat past op de URL,
		// dus we zijn klaar met de recursie.
		return $result;
	}

	// We kunnen een niveau dieper gaan, dus update het resultaat.
	$result['entry'] = $child;
	$result['remaining'] = $rest;

	// TODO SOEPMES: waarom wordt dit niet gecheckt als we bij de entry zelf zitten?
	if(!$child->mayAccess()) {
		$result['access'] = false;
		return $result;
	}

	if($child->getName() != $name)
		$rewrite = True;

	// Het kan zo zijn dat we een volledige rewrite tegenkomen of een link:
	// volledige rewrite gooit de rest weg en een link niet.
	$rewriteNaar = $child->getRewriteNaar();
	if (!is_null($rewriteNaar))
	{
		// Ja we zetten deze variabele op true en gooien het meteen in de tail call,
		// maar het is duidelijker dan een random `true` in je argumentenlijst.
		$rewrite = true;
		return hrefToReference($rewriteNaar, $params, $entry, $tree, $rewrite, $show_file_path, $request);
	}

	// check voor verweg links...
	if($child->isLink() && !$show_file_path) {
		$link = $child->getLink();
		$rewrite = True;
		if($rest)
			$link .= '/'.$rest;

		return hrefToReference($link, $params, $entry, $tree, $rewrite, $show_file_path, $request);
	}

	if($child->isDir())
		$rewrite |= !$slash;

	return hrefToReference($rest, $params, $child, $tree, $rewrite, $show_file_path, $request);
}

/**
 * @brief Zet een benamite-URI om in een vfs-reference.
 *
 * @param uri De string met URI om te vertalen.
 * Hier worden '*' niet in de variabele entry gestopt, maar geven de variabele entry zelf.
 *
 * @param from Vanaf waar dieper zoeken
 * (als $href met een '/' begint wordt er automatisch terug gevallen op $tree)
 *
 * @param show_file_path bool of het pad moet worden laten zien
 *
 * @return Een reference van de vfs-entry. Ziet er als array uit met volgende keys/waarden:
 * 		entry		de diepst entry die we konden vinden
 * 		remaining	het stuk $href wat niet meer matchde
 * 		rewrite		zijn we via een getLink()
 * 		access		mag je er wel bij?
 * 		params		de parameters
 *
 * @see hrefToReference
 */
function benamiteHrefToReference($uri, $from = NULL, $show_file_path = false)
{
	$ref = hrefToReference($uri, NULL, $from, NULL, false, $show_file_path);

	if(!$ref['entry'] || empty($ref['remaining']))
		return $ref;

	if(substr($ref['remaining'], 0, 2) == '*/' || $ref['remaining'] == '*')
	{
		$entry = $ref['entry']->getChild('*');
		if(!$entry)
			return $ref;

		$recur = benamiteHrefToReference(substr($ref['remaining'], 2), $entry);
		$recur['rewrite'] |= $ref['rewrite'];
		$recur['access'] &= $ref['access'];
		return $recur;
	}

	return $ref;
}

/*
 * Maak mbv de parameters array een string met de GET parameters.
 */
function vfsGenParams()
{
	global $request;

	if ($request->server->get('QUERY_STRING'))
		return '?'.$request->server->get('QUERY_STRING');
	return '';
}

/*
 */
function getTreeId($name = null)
{
	if($name == ROOT_ID) {
		return ROOT_ID;
	} elseif(empty($name)) {
		$name = 'site_live';
	}

	return vfs::get(ROOT_ID)->getChild($name)->getId();
}
