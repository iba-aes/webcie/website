<?php
// $Id$

use \Symfony\Component\HttpFoundation\Response;

class vfsEntryInclude extends vfsEntry
{
	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);
	}

	function display()
	{
		$this->logView();

		global $ALLOWED_FILE_TYPES;
		global $logger;

		$file = $this->getSpecialFile();

		// Haal bestandstype uit extensie.
		// $ext is de extensie (of '' indien geen)
		// en $mime is de mimetype (of 'text/html' bij default).
		$mime = 'text/html';
		$pos = strrpos($file, '.');
		$ext = '';
		if($pos !== False)
		{
			$ext = substr($file, $pos+1);
			if(isset($ALLOWED_FILE_TYPES[$ext]))
			{
				$mime = $ALLOWED_FILE_TYPES[$ext];
			}
		}

		if ($ext == 'php')
		{
			// Verkrijg de functie om aan te roepen.
			// (Let op: dit doet require_once op een file!
			// Onder andere wordt dus ook top-level html al geoutput hier.)
			$func = $this->getSpecialFunc();
			// getSpecialFunc() doet de require_once op $file
			$calledFunc = $this->getMethodFromFuncString($func);
			if (!$calledFunc)
			{
				return null;
			}

			return $calledFunc();
		}
		if ($mime == 'text/html')
		{
			if (getZeurmodus())
			{
				user_error("er wordt een html-bestand gerequired, doe dit in een controller!", E_USER_DEPRECATED);
			}

			$page = new HTMLPage();
			$page->start();

			// Include het bestand en stop stdout in een Page.
			ob_start();
			require($file);
			$page->add(ob_get_clean());

			return new PageResponse($page);
		}

		// Als het eruit ziet als tekstbestand,
		// doen we require, als een templatingsysteem (ongeveer).
		if(substr($mime, 0, 5) == 'text/') {
			if (getZeurmodus())
			{
				user_error("er wordt een tekst-bestand gerequired, doe dit in een controller!", E_USER_DEPRECATED);
			}

			$response = new Response();
			$response->headers->set("content-type", $mime);

			// De tekst wordt ge-echo'd, vang dit af en stop het in de response.
			ob_start();
			require($file);
			$response->setContent(ob_get_clean());
			return $response;
		}

		return sendfilefromfs($file, $this->getDisplayName(), '/');
	}

 /****************************************************************************/

	function getFile()
	{
		return $this->getSpecial();
	}
	function setFile($file)
	{
		// FIXME check if "ROOT_FS/$file" exists...
		$this->setSpecial($file);
	}
}
