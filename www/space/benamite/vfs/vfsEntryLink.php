<?php
// $Id$

class vfsEntryLink extends vfsEntry
{
	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);
	}

	function display()
	{
		global $uriRef;
		return responseRedirect($this->url() . $uriRef['remaining'] . vfsGenParams());
	}

	/**
	 * @brief Geef waar de link naartoe (default) of vandaan wijst.
	 *
	 * LET OP: dit is per default iets anders dan `vfsEntry::url()`,
	 * die geeft waar deze entry te vinden is.
	 *
	 * @sa gelinkteURL
	 * @sa origineleURL
	 *
	 * @param root Bij welke entry de URL begint (default: als `$entry->isRoot()`).
	 * @param show_file_path Indien `TRUE`, doe het gedrag van `origineleURL`.
	 * Indien `FALSE` (default), doe het gedrag van `gelinkteURL`.
	 *
	 * @return Een string die de URL weergeeft waar de link naartoe wijst.
	 */
	function url($root = null, $show_file_path = false)
	{
		if($show_file_path)
		{
			return $this->origineleURL($root);
		}
		else
		{
			return $this->gelinkteURL($root);
		}
	}

	/**
	 * @brief Geef waar de link naartoe wijst.
	 *
	 * @sa url
	 * @sa origineleURL
	 *
	 * @param root Bij welke entry de URL begint (default: als `$entry->isRoot()`).
	 *
	 * @return Een string die de URL weergeeft waar de link naartoe wijst.
	 */
	public function gelinkteURL($root = null)
	{
		$link = $this->getLink();
		if(substr($link, 0, 1) == '/') {
			$target = hrefToEntry($link);
			if($target)
				return $target->url($root);
			return $link;
		} elseif(preg_match('#^https?://#i', $link) === 1) {
			return $link;
		}

		$parent = $this->getParent();
		return $parent->url($root) . $link;
	}

	/**
	 * @brief Geef waar de link vandaan wijst.
	 *
	 * @sa url
	 * @sa gelinkteURL
	 *
	 * @param root Bij welke entry de URL begint (default: als `$entry->isRoot()`).
	 *
	 * @return Een string die de URL weergeeft waar de link vandaan wijst.
	 */
	public function origineleURL($root = null)
	{
		return parent::url($root);
	}

 /****************************************************************************/

	/**
	 * @brief Geef de URL waar deze entry heen linkt.
	 *
	 * @return String met URL waarheen gelinkt wordt.
	 */
	function getLink()
	{
		return $this->getSpecial();
	}
	function setLink($link)
	{
		$this->setSpecial(vfsSlashCheck($link));
	}
}
