<?php
/**
 *	$Id$
 */

function mijnActiviteiten_menu ()
{
	if (is_null($pers = Persoon::getIngelogd()))
		return;

	$acts = ActiviteitVerzameling::aankomendVanPersoon($pers, false, 5);

	$children = array();
	foreach ($acts as $act)
	{
		$cutname = str_cut(ActiviteitView::waardeKorteTitel($act), MENU_CUTLENGTH);
		$tooltip = ($cutname == ActiviteitView::waardeKorteTitel($act) ? NULL : ActiviteitView::waardeKorteTitel($act));
		$children[] = array('url' => $act->url(),
			'displayName' => $cutname,
			'tooltip' => $tooltip);
	}

	if (count($children) > 0)
		return array('displayName' => _("Mijn activiteiten"), 'children' => $children);
}

function mijnBestellingen_content($rest)
{
	switch ( count($rest) ) {
		case 0:
			$_REQUEST['page'] = 'bestelling';
			break;
		case 1:
			$_REQUEST['page'] = 'bestelstatus';
			$_REQUEST['bestelnr'] = $rest[0];
			break;
		case 2:
			if ( $rest[1] == 'Afbestellen' ) {
				$_REQUEST['page'] = 'afbestellen';
			} elseif ( $rest[1] == 'Vervalwijzig' ) {
				$_REQUEST['page'] = 'vervalwijzig';
			} else {
				return responseUitStatusCode(404);
			}
			$_REQUEST['bestelnr'] = $rest[0];
			break;
		default:
			return responseUitStatusCode(404);
	}

	require_once("bookweb/home.html");
}

function mijnBestellingen_menu()
{
	global $auth;

	if (!hasAuth('lid')) return;

	$bestellinghome = '/Onderwijs/Boeken/Bestellingen/';
	require_once('bookweb/booksql.php');
	require_once('bookweb/interface.php');

	$allebestellingen = sqlGetBestellingen(getSelLid());
	$children = array();
	foreach ($allebestellingen as $bestelnr => $bestelling) {
		if (!$bestelling['vervallen'] && !$bestelling['gekocht']) {
			$children[] = array('url' => $bestellinghome . $bestelnr,
				'displayName' => $bestelling['titel'],
				'tooltip' => $bestelling['auteur']);
		}
	}

	if (sizeOf($children) > 0) {
		if (getSelLid() == $auth->getLidnr()) {
			$mijn = _('Mijn Bestellingen');
		} else {
			$persoon = Persoon::geef(getSelLid());
			$mijn = sprintf(_("%s[VOC: bezittelijk voornaamwoord] Bestellingen"),
				$persoon->getVoornaamwoord()->vervoeg("jouw")
			);
		}
		return array('displayName' => $mijn,
			'url'  => '/Onderwijs/Boeken/Bestellingen/');

	}
}

// TODO SOEPMES: dit is hetzelfde als Persoon::mijnCiesGroepenDisputen, weg ermee?
function mijnCiesGroepenDisputen($soort)
{
	$namen = array('CIE' => _('Mijn Commissies')
		, 'GROEP' => _('Mijn Groepen')
		, 'DISPUUT' => _('Mijn Disputen'));

	$children = array();
	$mycies = Persoon::getIngelogd()->getCommissies();
	foreach ($mycies as $cie) {
		if ($cie->getSoort() != $soort) continue;
		$cutname = str_cut($cie->getNaam(), MENU_CUTLENGTH);
		$tooltip = ($cutname == $cie->getNaam() ? NULL : $cie->getNaam());
		$children[] = array('url' => $cie->getUrlBase(),
			'displayName' => $cutname,
			'tooltip' => $tooltip);
	}

	if ( hasAuth('actief') && count($children) > 0) {
		$menu = array('displayName' => $namen[$soort], 'children' => $children);
		return $menu;
	}
}

function mijnCommissies_menu()
{
	return mijnCiesGroepenDisputen('CIE');
}

function mijnDibs_menu()
{
	if (!($info = DibsInfo::geef(Persoon::getIngelogd())))
		return;

	$displayName = _('Mijn DiBS');
	$child = array('displayName' => sprintf(_('%s kudos'), $info->getKudos()),
		'url' => DIBSBASE . '/home');
	return array('displayName' => $displayName,
		'children' => array($child));
}

function mijnDisputen_menu()
{
	return mijnCiesGroepenDisputen('DISPUUT');
}

function mijnGroepen_menu()
{
	return mijnCiesGroepenDisputen('GROEP');
}

function mijnPlanners_menu()
{
	$planners = PlannerVerzameling::getOpenPlanners(Persoon::getIngelogd());
	if($planners->aantal() == 0)
		return;

	$childs = array();
	foreach($planners as $p) {
		//de functie ingelogd geeft 0 terug als de persoon niets van de planner heeft ingevuld, 1 als ie deels is ingebuld en 2 anders
		if($p->ingevuld(Persoon::getIngelogd()) == 2 || $p->getLastDate() < new DateTimeLocale("now"))
			continue;
		$childs[] = array('displayName' => PlannerView::waardeTitel($p),PlannerView::waardeTitel($p),'url' => $p->url());
	}

	if(sizeof($childs) == 0)
		return;

	$displayName = _('Mijn open Planners');
	return array('displayName' => $displayName,
		'children' => $childs);
}
