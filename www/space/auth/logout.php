<?php
// $Id$

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;


/**
 * haalt alles uit de sessie, zoals mylidnr en realuserid.
 *
 * 'mylidnr' wordt overal gebruikt om te zien of iemand is ingelogd.
 * Dit zorgt ervoor dat op de logoutpagina zelf al een nieuwe inlogbox staat.
 *
 * 'realuserid' wordt gebruikt voor goden om in de huid te kruipen van personen.
 */
function doeLogout()
{
	global $session;

	// zoek op waar we waren op de site toen we uitlogden,
	// zodat we dezelfde kleur kunnen behouden
	$url = HTTPS_ROOT . '/';
	if ($parent = tryPar('parent'))
	{
		$vfsEntry = vfs::get(tryPar('parent'));
		if ($vfsEntry)
		{
			$url = HTTPS_ROOT . $vfsEntry->url();
		}
	}

	$messages = $session->getFlashBag()->peekAll();
	$session->clear();
	$session->getFlashBag()->setAll($messages);

	$session->set('isUitgelogd', true);
	$session->save();

	// TODO: als dit in een controllerfunctie staat, return de response
	// ipv meteen sturen.
	return responseRedirect($url);
}
