<?php

namespace Space\Auth;

use Symfony\Component\HttpFoundation\Request;

/**
 * Auth-klasse die alles toestaat.
 */
class GodAuth extends Auth
{
	public function __construct(Request $request, $session, Constants $constants, array $debugSites)
	{
		parent::__construct($request, $session, $constants, 'god', $debugSites);
	}

	public function hasCieAuth($cienr)
	{
		return true;
	}
	public function hasCiesAuth(\CommissieVerzameling $cies)
	{
		return true;
	}
	public function isGod($contactID = null)
	{
		return true;
	}
	public function isGodAchtig($contactID = null)
	{
		return true;
	}
}