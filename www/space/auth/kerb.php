<?php

use \Symfony\Component\HttpFoundation\Response;

function doeKerb()
{
	global $session;

	$status = 1;
	// Als het enc_kerb_data-post-veld gezet is
	if(isset($_POST['enc_kerb_data']))
	{
		//Decrypt de data
		// (Merk op dat dit base64-encoded is, maar openssl neemt base64-encoded data).
		$encrypted = $_POST['enc_kerb_data'];
		// TODO SECURITY ISSUE: The use of [ECB] mode is not recommended.
		$string = trim(openssl_decrypt($encrypted, 'AES-256-ECB', KERB_AES_KEY));

		// Zit de substr lidnr= erin en lukt het exploden?
		$exp = explode('lidnr=', $string);
		if(isset($exp[1])) {
			$str = $exp[1];

			// Haal de timestamp eruit
			$exp = explode('timestamp=', $str);
			if(isset($exp[1])) {
				$id = (int)$exp[0];

				// Zit de salt in de string
				if(strpos($exp[1], KERB_SALT) !== False) {
					$timeexp = explode(KERB_SALT, $exp[1]);
					if(sizeof($timeexp) == 2 && empty($timeexp[1])) {
						$timestamp = $timeexp[0];
						$date = new DateTime();
						$timestamp2 = $date->getTimestamp();

						// Kijk of de timestamps minder dan 5 seconden van elkaar verschillen
						if($timestamp2 - $timestamp > 5) {
							return;
						}

						// Kijk of het lidnr bestaat
						$pers = Persoon::geef($id);
						if($pers) {
							// Als alles goed is zet het lidnr in de sessie en statuscode wordt 0
							$session->set('mylidnr', $id);
							$status = 0;
						}
					}
				}
			}
		}
	}
	return new Response($status);
}
