<?php

namespace Space\Auth;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class Auth
{
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var Session
	 */
	private $session;
	/**
	 * @var Constants
	 */
	private $constants;
	/**
	 * @var string
	 */
	private $level;
	/**
	 * @var string[]
	 */
	private $debugSites;

	// TODO: te veel constructor parameters

	/**
	 * Constructor.
	 *
	 * @param Request $request
	 * De request die de gebruikter maakt.
	 * @param Session? $session
	 * De sessie van de request (is null indien we op de command line draaien).
	 * @param Constants $constants
	 * De huidige waarden van de constanten, voor testbaarheid.
	 * @param string $level
	 * Het level van de huidige gebruiker.
	 * @param array $debugSites
	 * Mapt contactID's van goden naar hun debugwebsite. Je mag godrechten aanzetten desda je contactID een key is.
	 */
	public function __construct(Request $request, $session, Constants $constants, $level, $debugSites)
	{
		$this->request = $request;
		$this->session = $session;
		$this->constants = $constants;
		$this->level = $level;
		$this->debugSites = $debugSites;
	}

	/**
	 * Test of de gebruiker op de tablet zit. Is niet volledig safe en alleen
	 * te gebruiken in combinatie met een gewone hasAuth().
	 */
	public function hasTabletAgent()
	{
		// Dit is het ip-adres van de dibs-tablet
		return ($this->request->server->get('HTTP_X_FORWARDED_FOR') == '10.14.0.44')
			|| ($this->request->server->get('HTTP_USER_AGENT') == 'Dibs tablet');
	}

	/**
	 * Gegeven een commissie, retourneer of de user toegang heeft
	 * tot de commissie. Dat betekent dat de user in de cie zit
	 * of de user is bestuur.
	 *
	 * @param cienr Het commissieID om bij te checken
	 *
	 * @return bool Een boolean of de permissie klopt
	 */
	public function hasCieAuth($cienr)
	{
		if (hasAuth('bestuur'))
		{
			return true;
		}

		if (!$lidnr = $this->getLidnr())
		{
			return false;
		}

		$cies = \Persoon::geef($lidnr)->getCommissies(false, true)->keys();
		return in_array($cienr, $cies);
	}

	/**
	 * Gegeven een CommissieVerzameling, retourneer boolean
	 * of je toegang hebt tot 1 van de commissies.
	 * D.w.z.: je zit in de cie, of je bent >= bestuur.
	 *
	 * @param \CommissieVerzameling cies De CommissieVerzameling om bij te checken
	 *
	 * @return bool Een boolean of de persmissie klopt
	 */
	public function hasCiesAuth(\CommissieVerzameling $cies)
	{
		foreach ($cies as $cie)
		{
			if ($this->hasCieAuth($cie->getCommissieID()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Zorgt ervoor dat de user de cieRechten heeft, anders wordt
	 * er een 403 gegooid.
	 *
	 * @param cienr Het commissieID waarvan de user de rechten moet hebben
	 * @throws \HTTPStatusException
	 */
	public function requireCieAuth($cienr)
	{
		if (!$this->hasCieAuth($cienr))
		{
			requireAuth('ingelogd');
			spaceHTTP(403);
		}
	}

	/**
	 * Geeft terug of een contact god-machten mag gebruiken, en godmodus ook aan heeft staan.
	 * @param contactID het ID van het contact en anders wordt het ingelogde lid gebruikt als contactID null is
	 * @return bool
	 */
	public function isGod($contactID = null)
	{
		// scripts kunnen hun eigen level instellen
		if (defined('SCRIPT'))
		{
			return $this->level == 'god';
		}

		// We zouden ook kunnen redeneren dat op DEBUG, een godachtige altijd een god is.
		// return isGodAchtig && (DEBUG || $this->session->get('use_god'));
		return $this->isGodAchtig($contactID) && $this->session->get('use_god');
	}

	/**
	 * Geeft terug of een contact voorkomt in de godnummers, dus of het een god is als het zijn god-machten gebruikt.
	 * @param contactID het ID van het contact en anders wordt het ingelogde lid gebruikt als contactID null is
	 * @return bool
	 */
	public function isGodAchtig($contactID = null)
	{
		// scripts kunnen hun eigen level instellen
		if (defined('SCRIPT'))
		{
			return $this->level == 'god';
		}

		if (is_null($contactID))
		{
			$contactID = $this->getLidnr();
		}
		return !is_null($contactID) && isset($this->debugSites) && array_key_exists(intval($contactID), $this->debugSites);
	}

	/**
	 * Zegt of een contact een bestuursfunctie bekleed
	 * @param contactID het ID van het contact en anders wordt het ingelogde lid gebruikt als contactID null is
	 * @return bool
	 */
	public function isBestuur($contactID = null)
	{
		if ($contactID == null)
		{
			$contactID = $this->getLidnr();
		}
		return !is_null($contactID) && in_array(intval($contactID), $this->constants->getBestuurLidnrs());
	}

	/**
	 * Zegt of een contact de rechten mag hebben om hele serieuze dingen aan het
	 * boekenweb van de site mag aanpassen.
	 * De boekencommissaris van bestuur moet hier altijd onder vallen. Er mogen ook
	 * meerdere mensen onder vallen.
	 * @param contactID het ID van het contact en anders wordt het ingelogde lid gebruikt als contactID null is
	 * @return bool
	 */
	public function isBoekenCommissaris($contactID = null)
	{
		if (is_null($contactID))
		{
			$contactID = $this->getLidnr();
		}
		return !is_null($contactID) && in_array(intval($contactID), $this->constants->getBoekcomLidnrs());
	}

	/**
	 * Zegt of dit contact de DiBS-Tablet is.
	 * Deze mag enorm weinig op de site omdat het een 'beestje' is.
	 * @param contactID het ID van het contact en anders wordt het ingelogde lid gebruikt als contactID null is
	 * @return bool
	 */
	public function isDiBSTablet($contactID = null)
	{
		if (is_null($contactID))
		{
			$contactID = $this->getLidnr();
		}
		return !is_null($contactID) && $this->constants->getDibsTabletId() === intval($contactID);
	}

	/**
	 * Geeft het lidnr van de ingelogde user, of FALSE als er niemand is ingelogd.
	 * Als je in de huid kruipt van iemand anders, verander je ook 'mylidnr' in de
	 * sessie.
	 * @see space/auth/login.php:overrideuser
	 */
	public function getLidnr()
	{
		//Bij het gebruik van PHPUnit is geen sessie. Hierdoor is $session dus null.
		if (!$this->session)
		{
			return false;
		}

		if ($this->session->has('mylidnr'))
		{
			return $this->session->get('mylidnr');
		}

		// Als we gebruik maken van de nieuwe api moeten we kijken naar het Request-object
		$request = \Request::getInstance();
		return $request->getLoginID();
	}

	/**
	 * Geeft het lidnummer van de gebruiker die echt ingelogd is.
	 * Zelfs als je in de huid van iemand kruipt, blijft dit lidnummer hetzelfde, in
	 * tegenstelling tot getLidnr.
	 */
	public function getRealLidnr()
	{
		if ($this->session->has('realuserid'))
		{
			return $this->session->get('realuserid');
		}
		return $this->getLidnr();
	}

	/**
	 * Als een god in de huid van iemand anders is gebeurd,
	 * dan zorgt deze functie ervoor dat de god terug naar zijn eigen huid gaat.
	 *
	 * Als deze functie uitgevoerd is, geeft getRealLidnr() nog steeds hetzelfde
	 * resultaat, maar als iemand eerst in een huid van iemand zat, dan geeft
	 * getLidnr() iets anders.
	 * @see getRealLidnr
	 */
	public function terugNaarJeEigenHuid()
	{
		if ($this->session->has('realuserid'))
		{
			$this->session->set('mylidnr', $this->session->get('realuserid'));
			$this->session->remove('realuserid');
		}
	}

	/**
	 * Retourneert het (maximale) databaselevel dat de huidige user heeft.
	 *
	 * Hiermee gaan we de bijbehorende databaseuser waaronder deze connectie dan draait vinden.
	 * @see getDatabaseUsername
	 *
	 * @return string
	 * Een string die aangeeft welk level de gebruiker heeft.
	 */
	public function getLevel()
	{
		// command-line -> zelf level kiezen
		if (!$this->request->server->get('REMOTE_ADDR'))
		{
			return $GLOBALS['LEVEL'];
		}

		$contactID = $this->getLidnr();

		// check van hoog naar laag of we aan de voorwaarden voldoen
		if ($this->isGod($contactID))
		{
			return 'god';
		}
		if ($this->isBestuur($contactID))
		{
			return 'bestuur';
		}
		if ($this->isDiBSTablet($contactID))
		{
			return 'dibstablet';
		}
		if ($contactID == $this->constants->getGitWebhookId())
		{
			return 'gitwebhook';
		}
		if ($contactID == $this->constants->getMollieWebhookId())
		{
			return 'mollie';
		}
		if ($contactID)
		{
			return 'ingelogd';
		}

		// geen succes? dan het laagste level
		return 'gast';
	}
}
