<?php
/**
 * @brief Bepaalt of we fascistisch gaan doen over deprecated tsjak.
 *
 * AUB niet direct setten want globals zijn kwaadaardig.
 * Gebruik liever setZeurmodus.
 *
 * Dit is een array van de vorm `$zeuronderwerp => $zeurniveau`.
 * Het idee is dat je in `setZeurmodus` aangeeft op welke gebieden de huidige
 * code (bijvoorbeeld controllerfunctie) up to date is.
 * Als je iets doet wat deprecated is, wordt `getZeurmodus` gecheckt,
 * en als je op het relevante zeuronderwerp had aangegeven up to date te zijn,
 * krijg je een deprecation-notice.
 *
 * Op dit moment betekenen zeurniveau's nog niets, maar `FALSE` cast naar 0 en
 * `TRUE` naar 1, mochten we nog iets zinnigs ermee willen doen.
 *
 * Huidige zeuronderwerpen:
 *  * `'default'`: alle dingen die in de branch `feature-responses` deprecated zijn gemaakt.
 *  * `'set-class'`: zeurt over het aanpassen van de class zonder dat er
 *                   gebruik wordt gemaakt van de functies `addClass` of
 *                   `removeClass` in een HtmlElement
 *  * `'requirePar'`: aanroep op `requirePar` ipv `tryPar` + zelf valideren
 *
 * @sa getZeurmodus
 * @sa setZeurmodus
 */
$zeurmodus = array();

/**
 * @brief Bepaal of er gezeurd moet worden op het gegeven onderwerp.
 *
 * Het idee is dat je bij deprecated onderdelen van de code checkt
 * op `getZeurmodus('onderwerp')` en zo ja een `E_USER_DEPRECATED`-error gooit.
 *
 * @param zeuronderwerp Het onderwerp waar het onder valt.
 * Zie voor de uitleg van alle onderwerpen de documentatie bij `$zeurmodus`.
 *
 * @return Een bool of er gezeurd moet worden.
 *
 * @sa zeurmodus
 * @sa getZeurmodus
 */
function getZeurmodus($zeuronderwerp = 'default') {
	global $zeurmodus;
	if (!DEBUG) {
		// Zeurmodus helpt niet op live:
		// teveel mailtjes over deprecated tsjak.
		return false;
	}

	return array_key_exists($zeuronderwerp, $zeurmodus) && $zeurmodus[$zeuronderwerp];
}

/**
 * @brief Stel in of er gezeurd moet worden op het gegeven onderwerp.
 *
 * @param zeuronderwerp Het onderwerp waar het onder valt.
 * Zie voor de uitleg van alle onderwerpen de documentatie bij `$zeurmodus`.
 *
 * @param zeurniveau Bool om aan te geven of dit niveau aan of uit moet.
 * Default: aan. (Dit kan eventueel een int worden?)
 *
 * @sa zeurmodus
 * @sa getZeurmodus
 */
function setZeurmodus($zeuronderwerp = 'default', $zeurniveau = true) {
	global $zeurmodus;
	if (!DEBUG) {
		// Zeurmodus helpt niet op live:
		// teveel mailtjes over deprecated tsjak.
		return;
	}

	$zeurmodus[$zeuronderwerp] = $zeurniveau;
}

// De modi hieronder worden standaard aangezet. Als je het wilt uitzetten, kun
// je dit in je controller functie toevoegen.

// zeur altijd tenzij het wordt uitgezet op bepaalde plekken
setZeurmodus('set-class');
