<?php

// HTML space/classes/html/basis
function html_classes_autoload($class)
{
	switch($class)
	{
	case 'HtmlElement':
		require_once('space/classes/html/basis/HtmlElement.cls.php');
		break;
	case 'HtmlContainer':
		require_once('space/classes/html/basis/HtmlContainer.cls.php');
		break;
	case 'HtmlAbbreviation':
		require_once('space/classes/html/basis/HtmlAbbreviation.cls.php');
		break;
	case 'HtmlAnchor':
		require_once('space/classes/html/basis/HtmlAnchor.cls.php');
		break;
	case 'HtmlBreak':
		require_once('space/classes/html/basis/HtmlBreak.cls.php');
		break;
	case 'HtmlButton':
		require_once('space/classes/html/basis/HtmlButton.cls.php');
		break;
	case 'HtmlCode':
		require_once('space/classes/html/basis/HtmlCode.cls.php');
		break;
	case 'HtmlDiv':
		require_once('space/classes/html/basis/HtmlDiv.cls.php');
		break;
	case 'HtmlElementCollection':
		require_once('space/classes/html/basis/HtmlElementCollection.cls.php');
		break;
	case 'HtmlEmphasis':
		require_once('space/classes/html/basis/HtmlEmphasis.cls.php');
		break;
	case 'HtmlException':
	case 'HtmlElementImmutableException':
		require_once('space/classes/html/basis/HtmlException.cls.php');
		break;
	case 'HtmlForm':
		require_once('space/classes/html/basis/HtmlForm.cls.php');
		break;
	case 'HtmlHR':
		require_once('space/classes/html/basis/HtmlHR.cls.php');
		break;
	case 'HtmlHeader':
		require_once('space/classes/html/basis/HtmlHeader.cls.php');
		break;
	case 'HtmlIFrame':
		require_once('space/classes/html/basis/HtmlIFrame.cls.php');
		break;
	case 'HtmlImage':
		require_once('space/classes/html/basis/HtmlImage.cls.php');
		break;
	case 'HtmlInput':
		require_once('space/classes/html/basis/HtmlInput.cls.php');
		break;
	case 'HtmlLabel':
		require_once('space/classes/html/basis/HtmlLabel.cls.php');
		break;
	case 'HtmlLegend':
		require_once('space/classes/html/basis/HtmlLegend.cls.php');
		break;
	case 'HtmlList':
		require_once('space/classes/html/basis/HtmlList.cls.php');
		break;
	case 'HtmlListItem':
		require_once('space/classes/html/basis/HtmlListItem.cls.php');
		break;
	case 'HtmlNoScript':
		require_once('space/classes/html/basis/HtmlNoScript.cls.php');
		break;
	case 'HtmlOptGroup':
		require_once('space/classes/html/basis/HtmlOptGroup.cls.php');
		break;
	case 'HtmlPanel':
		require_once('space/classes/html/derived/HtmlPanel.cls.php');
		break;
	case 'HtmlParagraph':
		require_once('space/classes/html/basis/HtmlParagraph.cls.php');
		break;
	case 'HtmlPre':
		require_once('space/classes/html/basis/HtmlPre.cls.php');
		break;
	case 'HtmlScript':
		require_once('space/classes/html/basis/HtmlScript.cls.php');
		break;
	case 'HtmlSelectbox':
		require_once('space/classes/html/basis/HtmlSelectbox.cls.php');
		break;
	case 'HtmlSelectboxOption':
		require_once('space/classes/html/basis/HtmlSelectboxOption.cls.php');
		break;
	case 'HtmlSource':
		require_once('space/classes/html/basis/HtmlSource.cls.php');
		break;
	case 'HtmlSmall':
		require_once('space/classes/html/basis/HtmlSmall.cls.php');
		break;
	case 'HtmlSpan':
		require_once('space/classes/html/basis/HtmlSpan.cls.php');
		break;
	case 'HtmlStrong':
		require_once('space/classes/html/basis/HtmlStrong.cls.php');
		break;
	case 'HtmlTable':
		require_once('space/classes/html/basis/HtmlTable.cls.php');
		break;
	case 'HtmlTableCol':
		require_once('space/classes/html/basis/HtmlTableCol.cls.php');
		break;
	case 'HtmlTableDataCell':
		require_once('space/classes/html/basis/HtmlTableDataCell.cls.php');
		break;
	case 'HtmlTableHeaderCell':
		require_once('space/classes/html/basis/HtmlTableHeaderCell.cls.php');
		break;
	case 'HtmlTableHead':
		require_once('space/classes/html/basis/HtmlTableHead.cls.php');
		break;
	case 'HtmlTableFoot':
		require_once('space/classes/html/basis/HtmlTableFoot.cls.php');
		break;
	case 'HtmlTableBody':
		require_once('space/classes/html/basis/HtmlTableBody.cls.php');
		break;
	case 'HtmlTableRow':
		require_once('space/classes/html/basis/HtmlTableRow.cls.php');
		break;
	case 'HtmlTextarea':
		require_once('space/classes/html/basis/HtmlTextarea.cls.php');
		break;
	case 'HtmlVideo':
		require_once('space/classes/html/basis/HtmlVideo.cls.php');
		break;
	// Uitbreidingen van space/classes/html/basis
	case 'HtmlCommissieSelectbox':
		require_once('space/classes/html/forms/HtmlCommissieSelectbox.cls.php');
		break;
	case 'HtmlHideableDiv':
		require_once('space/classes/html/HtmlHideableDiv.cls.php');
		break;
	case 'HtmlResizableDiv':
		require_once('space/classes/html/HtmlResizableDiv.cls.php');
		break;
	}
}
spl_autoload_register('html_classes_autoload');
