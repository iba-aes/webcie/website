<?
// $Id$

/**
 *	@brief Class om een HTML div-element te produceren met class panel.
 *
 *	Dit kan niet zomaar in HtmlDiv gebeuren, omdat we twee divs in elkaar hebben
 *	zitten: een panel bevat een panelbody, waar je ->add op doet.
 *	Daarom een klasse die doet alsof het de panelbody is,
 *	maar die ook de panelhead genereert.
 */
class HtmlPanel
	extends HtmlDiv
{
	/**
	 * @brief De tekst (zou ook HtmlElement kunnen zijn) die in de heading komt.
	 */
	private $heading;

	/**
	 * @brief Maak een HtmlDiv die eruit ziet als een panel.
	 *
	 * Dat houdt dus in dat je een heading hebt en daaronder een div waar je
	 * al je favoriete HtmlElementen in kan stoppen.
	 *
	 * @param heading De tekst die bovenin het panel komt te staan.
	 * @param children Een HtmlElement of array daarvan, die dit element automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt. Dit geeft 
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($heading, $children = array(), $class = null, $id = null)
	{
		if (!$class) {
			$class = "panel-body";
		} else {
			$class .= " panel-body";
		}
		parent::__construct($children, $class, $id);
		$this->setHeading($heading);
	}

	/**
	 * @brief Stelt de heading van dit panel in.
	 *
	 * De heading is een stukje tekst dat bovenaan de panel wordt getoond.
	 *
	 * @param id De nieuwe heading.
	 * @return Dit HtmlElement.
	 */
	public function setHeading($heading)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->heading = $heading;
		return $this;
	}
	
	/**
	 * @brief Geeft de heading van dit panel.
	 *
	 * De heading is een stukje tekst dat bovenaan de panel wordt getoond.
	 *
	 * @return De inhoud van de heading van dit HtmlElement.
	 */
	public function getHeading()
	{
		return $this->heading;
	}

	public function makeHtml($indent=0) {
		// Stiekem zijn we helemaal geen simpele div,
		// maar bevatten we nog een div eromheen met de heading erin :O
		$panel = new HtmlDiv(null, 'panel panel-default');
		$panel->add(new HtmlDiv($this->getHeading(), 'panel-heading'));
		$panel->add(parent::makeHtml($indent+1));
		return $panel->makeHtml($indent);
	}
}
