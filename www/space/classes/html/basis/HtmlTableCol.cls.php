<?php
// $Id$

/**
 *  @brief Implementeert een HTML col (mag alleen in een table).
 */
class HtmlTableCol
	extends HtmlElement
{
	/**
	 * @brief Construeer een nieuw col-element.
	 *
	 * @param span Het aantal kolommen dat dit element breed is.
	 * @param style De CSS style die dit element meekrijgt.
	 */
	public function __construct($span = 1, $style = null)
	{
		parent::__construct('col');

		$this->_allowedAttributes[] = 'rowspan';

		if($span > 1)
			$this->setAttribute('rowspan', $span);
		if($style != null)
			$this->setCssStyle($style);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
