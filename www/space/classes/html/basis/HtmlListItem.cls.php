<?php
// $Id$

/**
 *	@brief Deze klasse implementeert een HTML list item: &lt;li&gt;
 */
class HtmlListItem
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw list item-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die dit HtmlListItem 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 * @param listGroupItem Een bool om aan te geven of het Item standaard css-klassen krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null, $listGroupItem = TRUE)
	{
		parent::__construct('li', true);

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		if (!$listGroupItem)
			$this->setNoDefaultClasses();

		$this->addChildren($children);
	}

	protected function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('list-group-item');

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
