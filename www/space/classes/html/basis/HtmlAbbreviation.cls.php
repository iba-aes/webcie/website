<?
// $Id$

/**
 *	@brief Class om een HTML abbr-element te produceren
 */
class HtmlAbbreviation
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw abbr-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlAbbreviation 
	 * automatisch als kind(eren) krijgt.
	 * @param title De title van de abbr
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $title = null, $class = null, $id = null)
	{
		parent::__construct('abbr', true);

		if ($title != null)
			$this->setAttribute('title', $title);
		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
