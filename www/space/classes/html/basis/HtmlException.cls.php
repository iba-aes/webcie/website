<?php
// $Id$

/**
 *	@brief Deze klasse beschrijft een Exception die gegooid kan worden bij
 *	problemen bij het bouwen van HTML-elementen en HTML-code
 */
class HtmlException
	extends \LogicException
{

}

/**
 *	@brief Exception als je een Immutable html-element toch wil aanpassen
 */
class HtmlElementImmutableException
	extends \HtmlException
{
	private $slachtoffer;

	public function __construct($slachtoffer)
	{
		$this->slachtoffer = $slachtoffer;
	}
}

// vim:sw=4:ts=4:tw=0:foldlevel=1
