<?php
// $Id$

/**
 *	@brief Deze klasse implementeert een HTML paragraph: &lt;p&gt;
 */
class HtmlParagraph
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw paragraph-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlParagraph 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('p', true);

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
