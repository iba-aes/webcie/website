<?php
// $Id: HtmlSelectbox.cls.php 9912 2010-04-10 21:33:01Z sjeik $

/**
	@brief Implementeert een HTML form (inclusief gebruik tokens)

	@see Token
*/
class HtmlForm extends HtmlContainer
{
	private $_tokenChild;

	/**
	 * @brief Simpele constructor
	 *	@param method POST of GET
	 *	@param action de URL om naar te POSTen/GETten
	 *	@param noToken Geef aan of je wel of geen token wilt, als het een get 
	 *	form is krijg je sowieso geen token.
	 *
	 *	Merk op dat deze functie automatisch een hidden form element met een
	 *	token toevoegt. Als je dit perse niet wil, of je eigen token in wil
	 *	stellen, dan kun je de functie setToken gebruiken om een ander Token in
	 *	te stellen (of null als je geen token wil).
	 *	Uiteraard is het ook mogelijk om het HtmlInputHidden element op te
	 *	zoeken d.m.v. de functie getChildren().
	 *
	 *	@see Token
	 *	@see setToken
	*/
	public function __construct($method = 'post', $action = NULL, $noToken = false)
	{
		parent::__construct('form', true);

		$this->_allowedAttributes[] = 'name';
		$this->_allowedAttributes[] = 'class';
		$this->_allowedAttributes[] = 'method';
		$this->_allowedAttributes[] = 'action';
		$this->_allowedAttributes[] = 'autocomplete';
		$this->_allowedAttributes[] = 'enctype';
		$this->_allowedAttributes[] = 'onsubmit';
		$this->_allowedAttributes[] = 'target';
		$this->setAttribute("method", $method);
		if($action) $this->setAttribute("action", $action);

		if (strtolower($method) != 'get' && !$noToken)
			$this->setToken(new Token($action));
	}

	/**
	 * 	@brief Formulier dat default naar z'n eigen URI post en intern een naam heeft.
	 *
	 * 	@param name De interne naam van het (token in het) formulier.
	 * 	@param action De actie die wordt uitgevoerd als het formulier gesubmit wordt.
	 */
	public static function named($name, $action = null)
	{
		$form = new HtmlForm('post', $action);

		$t = $form->_tokenChild;
		$t->setFormName($name);
		$t->setTargetURI($action ?: $t->getReqURI());


		return $form;
	}

	/**
	 * 	@brief Stelt het Token in dat met dit form meegestuurd wordt
	 *
	 * 	Het nieuwe Token vervangt een eventueel eerder ingesteld Token, maar
	 * 	enkel als dit gebeurd is d.m.v. de deze functie (of de constructor).
	 * 	Een zelf toegevoegd HtmlInputHidden element met daarin een Token
	 * 	identifier wordt natuurlijk niet herkend.
	 *
	 * 	@param token een Token object of null
	**/
	public function setToken($token)
	{
		if(!$token instanceof Token && $token !== NULL) {
			throw new HtmlException("Invalid Token-object ingesteld voor HtmlForm: $token");
		}

		$this->_tokenChild = $token;
	}

	/**
	 * @brief Stel de method van dit formulier in.
	 *
	 * @param method De nieuwe methode van het formulier.
	 * @return Dit HtmlForm.
	 */
	public function setMethod($method)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->setAttribute('method', $method);
		return $this;
	}
	
	/**
	 * @brief Vraag het Token van dit formulier op.
	 *
	 * @return het Token van dit HtmlForm.
	 */
	public function getToken()
	{
		return $this->_tokenChild;
	}

	private static function typeDFS($elem, $type, $recursive = false)
	{
		foreach ($elem->_children as $child) {
			if (!is_object($child)) continue;

			if ($child->attributeIsAllowed('type') && $child->getAttribute('type') == $type) {
				// we hebben onze waarde gevonden!
				return true;
			}
			if ($child instanceof HtmlContainer && $recursive && HtmlForm::typeDFS($child, $type, true)) {
				// kijk recursief of we hem toch hebben.
				return true;
			}
		}
		// niet gevonden
		return false;
	}

	/**
	 * @todo deze functie is nog niet gedocumenteerd.
	 */
	public function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('form-horizontal');

		// Als er een bestand in 't form kan worden gepost dan moeten we de encoding setten.
		if (HtmlForm::typeDFS($this, 'file', true)) {
			$this->setAttribute('enctype','multipart/form-data');
		}
		// Als het form geen submitknop heeft, zetten we die er automatisch bij voor mensen die geen JS hebben
		if (!HtmlForm::typeDFS($this, 'submit', false)) {
			$this->add(new HtmlNoScript(HtmlInput::makeSubmitButton(_("Versturen"))));
		}

		if(isset($this->_tokenChild)) 
		{
			$this->addChild(
				HtmlInput::makeHidden("token"
									, $this->_tokenChild->getIdentifier())
				);
			//zet een client-side timer neer die waarschuwt bij verlopen van het formulier
			//neem niet de volledige TTL om race-condities te voorkomen
			$this->setAttribute('data-tokenTTL', $this->_tokenChild->getTTL()-5);
		}

		return $this;
	}

	/**
	 * @brief Stel de naam van dit formulier in.
	 *
	 * @param name De nieuwe naam van het formulier.
	 * @return Dit HtmlForm.
	 */
	public function setName ($name)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->setAttribute('name', $name);
		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
