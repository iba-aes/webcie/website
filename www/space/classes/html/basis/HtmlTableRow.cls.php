<?php
// $Id$

/**
 *  @brief Implementeert een HTML table row.
 */
class HtmlTableRow
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw table row-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTableRow
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('tr', false);
		$this->_allowedChildren[] = 'HtmlTableHeaderCell';
		$this->_allowedChildren[] = 'HtmlTableDataCell';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}

	/**
	 * @brief Voeg een HtmlTableDataCell met een &nbsp; toe.
	 *
	 * @return de toegevoegde HtmlTableDataCell
	 * @see addData
	 */
	public function addEmptyCell ($class = null, $id = null)
	{
		return $this->addData('&nbsp;', $class, $id);
	}

	/**
	 * @brief Voeg een HtmlTableDataCell toe.
	 *
	 * @param data De inhoud van de nieuwe cel.
	 * @param class De class van het element
	 * @param id De id van het element
	 * @return De toegevoegde HtmlTableDataCell.
	 */
	public function addData ($data = null, $class = null, $id = null)
	{
		$this->addChild($td = new HtmlTableDataCell($data, $class, $id));
		return $td;
	}

	public function addImmutableData ($data = null, $class = null, $id = null)
	{
		$this->addImmutable($td = new HtmlTableDataCell($data, $class, $id));
		return $td;
	}

	/**
	 * @brief Voeg een HtmlTableHeaderCell toe.
	 *
	 * @param data De inhoud van de nieuwe cel.
	 * @param class De class van de nieuwe cel.
	 * @param id De id van de nieuwe cel.
	 * @return De toegevoegde HtmlTableHeaderCell.
	 */
	public function addHeader ($data = null, $class = null, $id = null)
	{
		$this->addChild($td = new HtmlTableHeaderCell($data, $class, $id));
		return $td;
	}

	/**
	 * @brief Voegt meerdere HtmlTableHeaderCell aan deze HtmlTableRow.
	 *
	 * @param headers De inhoud van de headers die worden toegevoegd.
	 * @param class De class van de nieuwe headers.
	 */
	public function makeHeaderFromArray ($headers, $class = null)
	{
		foreach ($headers as $header) {
			$this->addHeader($header, $class);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
