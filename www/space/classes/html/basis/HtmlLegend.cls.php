<?
// $Id$

/**
 *	@brief Class om een HTML legend-element te produceren
 */
class HtmlLegend
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw legend-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlLegend
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('legend', true);
		$this->_allowedAttributes[] = "dir";
		$this->_allowedAttributes[] = "onclick";
		$this->_allowedAttributes[] = "ondblclick";
		$this->_allowedAttributes[] = "onkeydown";
		$this->_allowedAttributes[] = "onkeypress";
		$this->_allowedAttributes[] = "onkeyup";
		$this->_allowedAttributes[] = "onmousedown";
		$this->_allowedAttributes[] = "onmousemove";
		$this->_allowedAttributes[] = "onmouseout";
		$this->_allowedAttributes[] = "onmouseover";
		$this->_allowedAttributes[] = "onmouseup";

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
