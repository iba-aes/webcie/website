<?
// $Id$

/**
	@brief Deze klasse vormt de basis voor alle HTML elementen

	Voor geheugenefficiëntie moet je de waarde ->immutable checken voordat je
	een aanpassing doet aan een HtmlElement-object!
**/
abstract class HtmlElement
{
	protected $_elementName;
	protected $_attributes = array();
	private $_knownAttributes = array(
			  'action' => 'string'
			, 'autoplay' => null
			, 'alt' => 'string'
			, 'align' => array('left','right','center')
			, 'autocomplete' => array('off')
			, 'border' => 'int'
			, 'cellpadding' => 'int'
			, 'checked' => null
			, 'cols' => 'int'
			, 'colspan' => 'int'
			, 'controls' => null
			, 'disabled' => null
			, 'draggable' => 'bool'
			, 'enctype' => 'string' //Voor bestanden
			, 'for' => 'string'
			, 'height' => 'int'
			, 'hidden' => 'bool'
			, 'href' => 'string'
			, 'id' => 'string'
			, 'label' => 'string'
			, 'maxlength' => 'int'
			, 'multiple' => null
			, 'method' => array('post','POST','get','GET')
			, 'name' => 'string' // naam van een HTML form element
			, 'min' => 'int'
			, 'max' => 'int'
			, 'onclick' => 'script'
			, 'onchange' => 'script'
			, 'ondrag' => 'script'
			, 'ondrop' => 'script'
			, 'onkeypress' => 'script'
			, 'onkeyup' => 'script'
			, 'onload' => 'script'
			, 'onmouseout' => 'script'
			, 'onmouseover' => 'script'
			, 'onresize' => 'script'
			, 'onsubmit' => 'script'
			, 'oninput' => 'script'
			, 'placeholder' => 'string'
			, 'readonly' => null
			, 'rel' => 'string'
			, 'required' => array('required')
			, 'role' => 'string'
			, 'rows' => 'int'
			, 'rowspan' => 'int'
			, 'selected' => null
			, 'size' => 'int'
			, 'sorttable_customkey' => 'string' // custom key waar op gesorteerd kan worden door JS
			, 'src' => 'string'
			, 'data-src' => 'string'
			, 'step' => 'int'
			, 'style' => 'string'
			, 'target' => 'string' // target van een link
			, 'title' => 'string'
			, 'type' => 'string' // type van een html input element, of 't type van script
			, 'valign' => array('top', 'middle', 'bottom') // verticale alignment
			, 'value' => 'string' // post/get value van een HTML form variable
			, 'width' => 'int'
		);
	
	// Default toegestande attributes (moeten keys zijn van _knownAttributes!)
	protected $_allowedAttributes = array("id", "style", "title", "role");

	protected $caption = null;

	// Mag dit html-element nog aangepast worden?
	// (Want dan is hij bijvoorbeeld al gerenderd)
	protected $immutable = false;

	/**
	 * Dit is een opzoek-tabel die alle CSS-classes bevat als key
	 * Bijvoorbeeld, als we de classes 'fa' en 'fa-icon' hebben, dan geldt:
	 *     $classes = array('fa' => true, 'fa-icon' => true)
	 */
	protected $classes = array();

	// Bool voor het niet-setten van de standaardclasses van html-objecten
	protected $noDefaultClasses = false;

	/**
		@brief Construeert een nieuw HTML element met naam $elementname

		@param elementName de HTML-naam van de tag van het element, bijvoorbeeld
		"div" voor een &lt;div&gt;-element
	**/
	protected function __construct($elementName){
		$this->_elementName = $elementName;
	}

	/**
		@brief Maakt een string-reprsentatie van alle attributen voor dit element
		@return Een string met daarin alle attributen die zijn ingesteld voor dit element
	**/
	protected function makeAttributesString()
	{
		$attStrings = array();

		// Op 1 april 2018 herschalen we alle elementen random.
		if(date('d-m') == '01-04') {
			// Bekom een float uniform op [0, 1]
			$uniformeFloat = (mt_rand() + 1) / mt_getrandmax();
			// Maak hier een of andere distributie voor die clustert op -0.1 ongeveer.
			$schaal = 1 - (log($uniformeFloat + 1) / 5);
			// Print het getal met punten voor decimalen en niets voor duizendtallen.
			$schaalCSS = number_format($schaal, 10, ".", "");
			$attStrings['style'] = 'style="transform: scale(' . $schaalCSS . ');"';
		}

		foreach($this->_attributes as $att => $value) {
			if ($value === null) $attStrings[] = $att;
			else $attStrings[] = $att . '="' . htmlspecialchars($value) .'"';
		}

		if (!empty($this->classes)) {
			// Voeg een concatenatie van alle gespecifieerde CSS klassen toe, als deze er zijn.
			// voorbeeld: class="fa fa-check"
			$attStrings[] = 'class="' . implode(' ', array_keys($this->classes)) . '"';
		}

		if (count($attStrings) == 0)
			return '';
		return ' ' . implode(' ', $attStrings);
	}


	/**
		@brief Geeft de waarde van een bepaald ingesteld attribuut
		@param att De naam van het attribuut, bijvoorbeeld 'style'
		@return De waarde van een ingesteld attribuut, of null als het attribuut niet ingesteld was
		@exception HtmlException als het opgevraagde attribuut ongeldig is in de context van het HTML element
		@see hasAttribute, attributeIsAllowed, setAttribute
	**/
	public function getAttribute($att)
	{
		//De html5 attributen data-* mogen altijd!
		if(substr($att, 0, 5) != 'data-')
		{
			if (!$this->attributeIsKnown($att)){
				throw new HtmlException("Attribuut '$att' is totaal onbekend?!");
			} elseif(!$this->attributeIsAllowed($att)){
				throw new HtmlException(
					"Attribuut \"$att\" niet toegestaan in HTML element \"$this->_elementName\""
				);
			}
		}
		
		return isset($this->_attributes[$att]) ? $this->_attributes[$att] : null;
	}

	/**
		@brief Stelt meerdere attributen in 1x in

		@param $attributes is een key-value array: key is attribute, value is de waarde van het attribute
	**/
	public function setAttributes($attributes){
		$this->assertMutable();
		foreach ($attributes as $attr => $val)
			$this->setAttribute($attr, $val);
	}

	/**
		@brief Bepaalt of een bepaald attribuut is ingesteld op dit HTML element
		@return true als het attribuut ingesteld was, false wanneer dit niet het geval was
		@param att Het attribuut in kwestie
	**/
	public function hasAttribute($att){
		return in_array($att, array_keys($this->_attributes));
	}

	/**
		@brief Bepaalt of een bepaald attribuut geldig is in de context van dit HTML element
		@return true als het attribuut geldig is, false wanneer dit niet het geval is
		@param att Het attribuut in kwestie
	**/
	public function attributeIsAllowed($att){
		return in_array($att, $this->_allowedAttributes);
	}

	/**
		@brief Bepaalt of een bepaald attribuut uberhaupt geldig is. 
		Deze functie bepaalt of een attribuut bekend is bij de HTML classes.
		Het resultaat van de functie zegt niets over de geldigheid van het
		attribuut op dit specifieke HTML element.

		@param att Het attribuut om de geldigheid van te bepalen.
		@see attributeIsAllowed
	**/
	private function attributeIsKnown($att){
		return in_array($att, array_keys($this->_knownAttributes));
	}

	/**
		@brief Stelt een attribuut in voor dit HTML element.
		In principe heeft bijna ieder attribuut een waarde, maar dit is niet
		altijd het geval. Denk aan het attribuut 'disabled' voor een
		HtmlSelectboxOption.
		@see getAttribute
		@param att Het attribuut om in te stellen, bijv. 'style'
		@param val De waarde van het attribuut, bijv. 'color: red'
	**/
	public function setAttribute($att, $val)
	{
		$this->assertMutable();
		//De html5 attributen data-* mogen altijd!
		if(substr($att, 0, 5) != 'data-')
		{
			if ($att === "class") {
				if (getZeurModus('set-class')) {
					user_error("Als je het class-attribuut wilt aanpassen, moet je
							addClass of removeClass aanroepen!", E_USER_DEPRECATED);
				}
				return $this->addClass($val);
			}

			if (!$this->attributeIsKnown($att)){
				throw new HtmlException("Attribuut '$att' is totaal onbekend?!");
			} elseif(!$this->attributeIsAllowed($att)){
				throw new HtmlException(
					"Attribuut \"$att\" niet toegestaan in HTML element \"$this->_elementName\""
				);
			}
			$type = $this->_knownAttributes[$att];
			if ($type === null && $val !== null){
				// Dit attribuut kan geen value hebben. Bijvoorbeeld het attribuut 'multiple' in een <selectbox>
				throw new HtmlException("Attribuut \"$att\" kan geen value hebben!");
			} else {
				// Controleren of de opgegeven waarde wel voldoet aan type restrictions
				if (is_array($type)){
					// Er is een beperkt aantal predefined values die ingesteld kunnen worden
					if (!in_array($val, $type)){
						throw new HtmlException('Waarde "' . $val . '" is niet ' .
							'geldig voor attribute "' . $att . '"'
						);
					}
				} else {
					// Simpele typecheck
					if(!$this->is_type($val, $type)) {
						throw new HtmlException(
							'Attribuut "'.$att.'" moet van type "'.$type.'" zijn"'
							.', maar "' . $val . '" is dat niet!'
						);
					}
				}
			}
		}

		$this->_attributes[$att] = $val;
		return $this;
	}

	/**
		@brief Haalt een bepaald attribuut uit dit HTML-element
		@param att het attribuut dat verwijderd moet worden
	**/
	public function unsetAttribute ($att)
	{
		$this->assertMutable();
		if (isset($this->_attributes[$att]))
			unset($this->_attributes[$att]);
		return $this;
	}

	/**
	 * @brief Stelt het attribuut 'id' in van dit HTML-element
	 *
	 * @param id De nieuwe id.
	 * @return Dit HtmlElement.
	 */
	public function setId ($id)
	{
		return $this->setAttribute('id', $id);
	}
	
	/**
	 * @brief Retourneert de waarde van het attribuut 'id' van dit HTML-element
	 *
	 * @return De waarde van het 'id'-attribuut van dit HtmlElement.
	 */
	public function getId ()
	{
		return $this->getAttribute('id');
	}
	
	/**
	 * @brief Voegt de gespecificeerde (CSS-)classes toe aan de lijst van classes die aan het HTML-element zullen worden toegewezen als HTML-attribuut.
	 * @param classes De nieuwe klassen die toegevoegd moeten worden. Dit mag een whitespace-gescheiden string zijn, of een array van strings die slechts één klasse mag bevatten.
	 */
	public function addClass($classes)
	{
		return $this->setClassStatus($classes, true);
	}

	/**
	 * @brief Verwijdert de gespecificeerde (CSS-)classes uit de lijst van classes die aan het HTML-element zullen worden toegewezen als HTML-attribuut.
	 * @param classes De klassen die verwijderd moeten worden. Dit mag een whitespace-gescheiden string zijn, of een array van strings die slechts één klasse mag bevatten.
	 */
	public function removeClass($classes)
	{
		return $this->setClassStatus($classes, false);
	}

	/**
	 * Voegt een/meerdere CSS-classes toe aan dit HtmlElement, of verwijdert deze juist.
	 * @param $classes: als string: whitespace-gescheiden klassenamen, als array: verschillende klassenamen (zonder whitespace)
	 */
	private function setClassStatus($classes, $used)
	{
		$this->assertMutable();

		// splits op ' '
		if (is_string($classes)) {
			// bv, dit is toegestaan: setClassStatus('fa fa-check')
			$classes = preg_split('/\s+/', $classes);
		}

		if (!is_array($classes)) {
			// Het was geen string en geen array, help!
			return $this;
		}

		foreach ($classes as $class) {
			if (empty($class) || preg_match('/\s/',$class)) {
				// We gaan geen lege klasse toevoegen
				// en ook geen klasse die spaties bevat
				continue;
			}
			// Escape de classes een beetje voor het geval een technisch persoon verkeerde tsjak hier neerzet.
			$class = htmlspecialchars($class);

			if ($used)
				$this->classes[$class] = true;
			else if (array_key_exists($class, $this->classes))
				unset($this->classes[$class]);
		}
		return $this;
	}

	/**
	 * @brief Stelt het attribuut 'style' in van dit HTML-element
	 *
	 * @param cssstyle De nieuwe style.
	 * @return Dit HtmlElement.
	 */
	public function setCssStyle ($cssstyle)
	{
		return $this->setAttribute('style', $cssstyle);
	}

	/**
	 * @brief Bepaalt of een bepaalde waarde van een bepaald type is
	 * @param val de waarde die getest dient te worden
	 * @param type het type dat verwacht wordt
	**/
	private function is_type($val, $type)
	{
		switch ($type) {
		case 'bool':
			return is_bool($val) || $val == "true" || $val == "false";
		case 'string':
			return is_string($val) || is_int($val);
		case 'script':
			return HtmlElement::is_javascript($val);
		case 'int':
			return is_numeric($val);
		case null:
			return is_null($val);
		default:
			throw new HtmlException(
				'Onbekend attribuut type "'.$type.'" voor waarde "'.$val.'"'
			);
		}
	}

	/**
	 * @brief Genereert een random attribute 'id' als dit HTML-element nog geen id had.
	 *
	 *	Deze functie genereert een random attribute 'id' als deze attribute nog
	 *	niet ingesteld was. Deze random waarde bestaat uit 10 random
	 *	cijfers/letters en wordt direct ingesteld.
	 *
	 *	@see getId
	 *	@return De gegenereerde random id, of het id dat reeds ingesteld was
	**/
	protected function ensureId(){
		$this->assertMutable();
		if (!$this->getAttribute('id')){
			// Generate random string van 10 letters/cijfers
			$randomid = chr(rand(48,57));
			for($i = 0; $i < 9; $i++){
				$digit = rand(0, 1);
				if ($digit > 0.5) $randomid .= chr(rand(48,57)); // random digit
				else $randomid .= chr(rand(65,90)); // random letter
			}
			$this->setAttribute('id', $randomid);
		}
		return $this->getAttribute('id');
	}

	private static function is_javascript($val)
	{
		//TODO: find a way to determine if something is javascript
		return is_string($val);
	}

	/**
	 * @brief Maakt een HTML-representatie van dit object en retourneert deze
	 *
	 *	Deze functie stelt een HTML-representatie op van dit object (inclusief
	 *	attributes) en retourneert deze als string.
	 *
	 *	@return Een string met HTML-representatie van dit object
	**/
	public function makeHtml() {
		if (!$this->immutable)
			$this->prepareHtml();

		$out = '<'.$this->_elementName.$this->makeAttributesString().'>';

		if (!is_null($this->caption))
			$out .= new HtmlSpan($this->caption,
				(isset($this->_attributes['class'])) ? $this->_attributes['class'] : null,
				(isset($this->_attributes['id'])) ? $this->_attributes['id'] . '_caption' : null
				);

		return $out;
	}

	/**
	 * 	@brief Placeholder die overridden kan worden in subclasses. Deze
	 * 	functie wordt door HtmlElement::makeHtml aangeroepen alvorens
	 * 	daadwerkelijk makeHtml() aan te roepen
	 *
	 * 	Sommige classes willen net voordat er HTML gegenereerd wordt nog wat
	 * 	doen. Een paar children toevoegen, een paar attributes wijzigen,
	 * 	etcetera. Het is overbodig om in dat geval makeHtml te overriden en
	 * 	vervolgens expliciet de parent class te callen. In zo'n geval kan de
	 * 	function 'prepareMakeHtml' overridden worden.
	**/
	protected function prepareHtml(){}

	/**
	 *	@brief The __toString method allows a class to decide how it will react
	 *	when it is converted to a string.
	 *
	 *	@see makeHtml
	 */
	public function __tostring ()
	{
		return $this->makeHtml();
	}

	/**
	 * @brief Stel de caption van dit HTML-element in.
	 *
	 * @param caption De nieuwe caption van dit HtmlElement.
	 * @return Dit HtmlElement.
	 */
	public function setCaption ($caption)
	{
		$this->assertMutable();
		$this->caption = $caption;
		return $this;
	}

	/**
	 * @brief Vind de caption van dit HTML-element.
	 *
	 * @return De caption van dit HtmlElement.
	 */
	public function getCaption ()
	{
		return $this->caption;
	}

	/**
	 * @brief Maak dit element nooit meer aanpasbaar.
	 *
	 * Nuttig als het al gerenderd is en je niet daaroverheen wilt fietsen.
	 */
	public function makeImmutable()
	{
		// prepareHtml kan in sommige gevallen een default class toevoegen.
		// Hiervoor moet het object nog bewerkbaar zijn!
		$this->prepareHtml();

		$this->immutable = true;
	}

	/**
	 * @brief Gooit een HtmlElementImmutableException als $this-immutable aan staat.
	 */
	public function assertMutable()
	{
		if ($this->immutable)
			throw new HtmlElementImmutableException($this);
	}

	/**
	 * @brief Zet de standaard-classes van het html-object uit
	 */
	public function setNoDefaultClasses()
	{
		$this->noDefaultClasses = true;
	}
}
