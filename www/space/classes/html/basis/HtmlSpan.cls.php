<?
// $Id$

/**
 *	@brief Class om een HTML span-element te produceren
 */
class HtmlSpan
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw span-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlSpan 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('span', true);
		$this->_allowedAttributes[] = "class";
		$this->_allowedAttributes[] = "dir";
		$this->_allowedAttributes[] = "id";
		$this->_allowedAttributes[] = "onclick";
		$this->_allowedAttributes[] = "ondblclick";
		$this->_allowedAttributes[] = "onkeydown";
		$this->_allowedAttributes[] = "onkeypress";
		$this->_allowedAttributes[] = "onkeyup";
		$this->_allowedAttributes[] = "onmousedown";
		$this->_allowedAttributes[] = "onmousemove";
		$this->_allowedAttributes[] = "onmouseout";
		$this->_allowedAttributes[] = "onmouseover";
		$this->_allowedAttributes[] = "onmouseup";
		$this->_allowedAttributes[] = "style";
		$this->_allowedAttributes[] = "title";

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}

	static public function fa($icon, $title = '', $extraClasses = null)
	{
		if(empty($title))
			user_error('Bij een font-awesome-icon hoort een title text!');

		$span = new HtmlSpan(null, "fa fa-$icon ".$extraClasses);
		$span->setAttribute('title', $title);

		return $span;
	}

	static public function fa_stacked($children = array())
	{
		return new HtmlSpan($children, 'fa-stack');
	}

	/**
	 * @brief Maak een vinkje of een kruisje.
	 *
	 * @param bool True om een vinkje te maken, false om een kruisje te maken.
	 * @return Een HtmlSpan die een groen vinkje of een rood kruisje weergeeft.
	 */
	public static function maakBoolPicto($bool)
	{
		if($bool)
			return HtmlSpan::fa('check', _('OK'), 'text-success');
		else
			return HtmlSpan::fa('times', _('Fout'), 'text-danger');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
