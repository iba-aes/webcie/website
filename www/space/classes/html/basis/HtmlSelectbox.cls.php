<?php
// $Id$

/**
 *	@brief Implementeert een HTML form selectbox
 */
class HtmlSelectbox
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw selectbox-element.
	 *
	 * @param selectboxname de naam van het html-element
	 * @param id het id van de HtmlSelectbox
	 */
	public function __construct($selectboxname, $id = NULL)
	{
		parent::__construct('select', false);
		$this->_allowedAttributes[] = "onchange";
		$this->_allowedAttributes[] = "multiple";
		$this->_allowedAttributes[] = "name";
		$this->_allowedAttributes[] = "size";
		$this->_allowedAttributes[] = "class";
		$this->_allowedAttributes[] = "id";
		$this->_allowedChildren[] = "HtmlSelectboxOption";
		$this->_allowedChildren[] = "HtmlOptGroup";

		$this->setAttribute("name", $selectboxname);
		if ($id !== NULL) $this->setId($id);
	}

	/**
	 *	@brief Stel in of deze HtmlSelectbox het formulier autosubmit.
	 *	
	 *	@param autosubmit True om het formulier te submitten zodra een waarde wordt geselecteerd, 
	 *	False om dit niet te doen.
	 *	@return Deze HtmlSelectbox.
	 */
	public function setAutoSubmit($autosubmit = true)
	{
		if ($autosubmit) $this->setAttribute("data-autosubmit", "true");
		else $this->unsetAttribute("data-autosubmit");
		return $this;
	}

	/**
	 * @brief Stel in of multiselect aanstaat.
	 *
	 * Stel in of in deze HtmlSelectbox meerdere waarden tegelijk kunnen zijn 
	 * geselecteerd.
	 *
	 * @param multiple True om multiselect aan te zetten, False om het uit te zetten.
	 * @return Deze HtmlSelectbox.
	 */
	public function setMultiple($multiple = true)
	{
		if($multiple)
		{
			$this->setAttribute("multiple", NULL)
				 ->setAttribute("name", $this->getAttribute("name") . "[]");
		}
		else
		{
			$this->unsetAttribute("multiple")
				 ->setCaption(null);
		}
		return $this;
	}

	/**
	 * @brief Stel de grootte in, of False om dit automatisch te bepalen.
	 *
	 * @param num De nieuwe grootte van deze HtmlSelectbox.
	 * @return Deze HtmlSelectbox.
	 */
	public function setSize ($num = false)
	{
		if ($num && $num != 0)
			$this->setAttribute('size', $num);
		elseif($num != 0)
		{
			if (count($this->getOptions()) > 10)
				$this->setAttribute('size', 10);
			else
				$this->setAttribute('size', count($this->getOptions()));
		}
		return $this;
	}

	/**
	 *	@brief Shortcut voor getAttribute("name");
	 *
	 *	@return De naam van dit element.
	 *	@see getAttribute
	 */
	public function getName()
	{
		return $this->getAttribute("name");
	}

	/**
	 *	@brief voeg een optie toe
	 *	@param value waarde die in de post data komt
	 *	@param label naam van de optie
	 *	@param selected of de optie meteen geselecteerd moet worden
	 *	@return de toegevoegde HtmlSelectboxOption
	 */
	public function addOption($value, $label, $selected = false)
	{
		$option = new HtmlSelectboxOption($value, $label);
		if ($selected)
			$option->setSelected();
		$this->add($option);
		return $option;
	}

	/**
	 *	@brief Voegt een item toe, vooraan de lijst.
	 *
	 *	Als er geen ander item expliciet geselecteerd staat, wordt dit
	 *	(standaard lege) item geselecteerd.
	 *
	 *	@param value value voor het lege item, default = ""
	 *	@param caption caption voor het lege item, default = ""
	 *	@return de toegevoegde HtmlSelectboxOption
	 */
	public function prependOption($value = "", $caption = "")
	{
		$option = new HtmlSelectboxOption($value, $caption);
		$this->prependChild($option);
		return $option;
	}

	/**
	 * Returneert alle options
	 *
	 * @return alle options
	 */
	public function getOptions ()
	{
		$ret = array();
		foreach ($this->getChildren() as $option)
			if ($option instanceof HtmlSelectboxOption)
				$ret[] = $option;
		return $ret;
	}

	/**
	 *	@brief Selecteert de gegeven optie in de lijst. Voor lijsten waarin
	 *	meerdere opties geselecteerd kunnen worden, kan deze functie meerdere
	 *	malen worden aangeroepen. Als een lijst niet meerdere selecties
	 *	toestaat, wordt de actieve selectie gewist alvorens de nieuwe optie te
	 *	selecteren.
	 *
	 *	@param selectoption Een HtmlSelectboxOption object, of een string met
	 *	       de value van de te selecteren option.
	 */
	public function select($selectoption)
	{
		$multiselect = $this->hasAttribute("multiple");
		self::selectFromSet($selectoption, $multiselect, $this->getChildren());
		return $this;
	}

	static protected function selectFromSet ($selectoption, $multiselect, $opts)
	{
		foreach ($opts as $option)
		{
			if ($option instanceof HtmlOptGroup)
			{
				self::selectFromSet ($selectoption, $multiselect, $option->getChildren());
				continue;
			}
			else if(($selectoption instanceof HtmlSelectboxOption
				&& $option === $selectoption)
			||($selectoption == $option->getValue())
			||(is_array($selectoption) &&
				in_array($option->getValue(), $selectoption))
			)
			{
				// Deze optie moet attribute "selected" krijgen
				$option->setSelected();

			} elseif (!$multiselect){
				// Deze <option> hoeft niet geselecteerd te zijn
				$option->setSelected(false);
			}
		}
	}

	/**
	 *  @brief Haal zo mogelijk een geposted geselecteerde waarde op voor het
	 *	selecteren van iets uit een lijst. Als er iets is meegegeven dan geven
	 *  we dit door aan select als fallback wanneer trypar niets terug geeft.
	 *
	 *  Let op dat dit (nog) niet werkt voor multiselectbox!
	 **/
	public function tryparSelect($selectoption = null)
	{
		if(trypar($this->getName())){
			$selectoption = trypar($this->getName());
		}
		return $this->select($selectoption);
	}

	/**
	 *	@brief Maak van een associatieve array een selectbox. Als de array arrays bevat
	 *	  die ieder een eigen option group moeten voorstellen dan wordt hier ook
	 *	  rekening mee gehouden.
	 *	@param name de naam van het html element
	 *	@param options de associatieve array
	 *	@param selected een array van elementen die geselecteerd moeten zijn
	 *	@param size de grootte van de HtmlSelectbox
	 *	@param auto True om autosubmit aan te zetten
	 *	@return de selectbox
	 */
	public static function fromArray ($name, $options, $selected = array(), $size = null, $auto = false)
	{
		$box = new HtmlSelectbox($name);
		if (!is_array($selected))
			$selected = array($selected);
		foreach ($options as $value => $label)
		{
			if(is_array($label)){
				$group = HtmlOptGroup::fromArray($value, $label, $selected);
				$box->add($group);
			} else {
				$opt = $box->addOption($value, $label);
				if (in_array($value, $selected))
					$opt->setSelected();
			}
		}
		$box->setSize($size)
			->setAutoSubmit($auto);
		return $box;
	}

	/**
	 * @brief Maak van een Single-Dimensional array een selectbox.
	 *    De key en value van een option worden dan gelijk aan
	 *    de waarde die in de array staan
	 *    Simpele wrapper om HtmlSelectBox::fromArray()
	 * @param name de naam van het html element
	 * @param options de single-dimensional array
	 * @param selected een array van elementen die geselecteerd moeten zijn
	 * @param size de grootte van de HtmlSelectbox
	 * @param auto True om autosubmit aan te zetten
	 * @return de selectbox
	 *
	 */
	public static function fromSingleDimensionArray($name, $options, $selected = array(), $size = null, $auto = false)
	{
		$options = array_combine($options, $options);
		return self::fromArray($name, $options, $selected, $size, $auto);
	}

	/**
	 * @brief Maak een SelectBox van commissies.
	 *
	 * @param verz Een CommissieVerzameling.
	 * @param name De naam van de nieuwe HtmlSelectbox.
	 * @param selectedCieId De id van de Commissie die geselecteerd is.
	 * @param required True om er een verplicht veld van te maken.
	 * @return Een HtmlSelectbox die de namen Commissies in verz bevat.
	 */
	static public function cies(CommissieVerzameling $verz, $name, $selectedCieId = null, $required = false)
	{
		$box = new HtmlSelectbox($name);

		if($required)
			$box->addOption('', '');

		foreach($verz as $cieid => $cie)
		{
			$box->addOption($cieid
					, CommissieView::waardeNaam($cie)
					, $cieid == $selectedCieId);
		}
		return $box;
	}

	protected function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('selectpicker form-control');

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
