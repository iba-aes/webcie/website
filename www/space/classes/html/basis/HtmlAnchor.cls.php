<?php
// $Id$

/**
 *	@brief Klasse die een HTML anchor (link) representeert.
 */
class HtmlAnchor
	extends HtmlContainer
{
	/**
	 *	@brief Construeer een nieuwe anchor tag met opgegeven URL en caption.
	 *
	 *	@param url de url waar de HtmlAnchor heen verwijst.
	 *	@param caption de tekst op de HtmlAnchor.
	 *	@param onclick de actie die uitgevoerd wordt als er op de HtmlAnchor 
	 *	wordt geklikt.
	 *	@param class de klassen van de anchor
	 *	@param id het id van de anchor
	 */
	function __construct ($url = null, $caption = null, $onclick = null, $class = null, $id = null)
	{
		parent::__construct('a', true);
		$this->_allowedAttributes[] = 'href';
		$this->_allowedAttributes[] = 'target';
		$this->_allowedAttributes[] = 'onclick';
		$this->_allowedAttributes[] = 'name';
		$this->_allowedAttributes[] = 'rel';
		$this->_issueNewlines = false;

		if ($url)
			$this->setAttribute('href', $url);
		if ($onclick)
			$this->setAttribute('onclick', $onclick);
		if (!is_null($caption))
			$this->addChildren($caption);
		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);
	}

	/**
	 * @brief Stel de href van deze anchor in.
	 *
	 * @param url de nieuwe url waar deze HtmlAnchor heen verwijst.
	 * @return deze HtmlAnchor.
	 */
	public function setHref ($url = null)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->setAttribute('href', $url);
		return $this;
	}

	/**
	 * @brief Stel de naam van deze anchor in.
	 *
	 * @param name de nieuwe naam van deze HtmlAnchor.
	 * @return deze HtmlAnchor.
	 */
	public function setName ($name = null)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->setAttribute('name', $name);
		return $this;
	}

	/**
	 */
	static public function button($url, $caption, $onclick = null, $class = null, $id = null, $type = 'primary')
	{
		$anchor = new HtmlAnchor($url, $caption, $onclick, $class, $id);
		$anchor->addClass('btn btn-' . $type . ((is_null($class)) ? "" : " $class"));
		return $anchor;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
