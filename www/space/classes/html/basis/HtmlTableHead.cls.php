<?php
/**
 * Implementeert een html-table-header
 */
class HtmlTableHead extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw table header-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTableHeader
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('thead', false);

		$this->_allowedChildren[] = 'HtmlTableRow';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}

	/**
	 * @brief Voeg een HtmlTableRow toe.
	 *
	 * @return de toegevoegde HtmlTableRow.
	 */
	public function addRow()
	{
		$row = new HtmlTableRow();
		$this->addChild($row);
		return $row;
	}
}
