<?php

/**
 *	@brief Implementeert een OptGroup voor in selectBoxes
 */

class HtmlOptGroup
	extends HtmlContainer
{
	/**
	 *	@brief Constructor
	 *	@param label is het label van de optgroup
	 */
	public function __construct($label)
	{
		parent::__construct('optgroup', false);
		$this->_allowedAttributes[] = "label";
		$this->_allowedAttributes[] = "disabled";
		$this->_allowedChildren[] = "HtmlSelectboxOption";

		$this->setAttribute("label", $label);
	}
	
	/**
	 *	@brief enable/disable the group
	 *	@param disabled turn this feature on and off (default is off)
	 */
	public function setDisabled($disabled = true)
	{
		if($disabled)
			$this->setAttribute("disabled", null);
		else
			$this->unsetAttribute("disabled");

		return $this;
	}	
	
	/**
	 *	@brief voeg een optie toe
	 *	@param value waarde die in de post data komt
	 *	@param label naam van de optie
	 *	@param selected True om deze optie te selecteren
	 *	@return de toegevoegde HtmlSelectboxOption
	 */
	public function addOption($value, $label, $selected = false)
	{
		$option = new HtmlSelectboxOption($value, $label);
		if ($selected)
			$option->setSelected();
		$this->add($option);
		return $option;
	}
	
	/**
	 *	@brief Voegt een item toe, vooraan de lijst.
	 *
	 *	Als er geen ander item expliciet geselecteerd staat, wordt dit
	 *	(standaard lege) item geselecteerd.
	 *
	 *	@param value value voor het lege item, default = ""
	 *	@param caption caption voor het lege item, default = ""
	 *	@return de toegevoegde HtmlSelectboxOption
	 */
	public function prependOption($value = "", $caption = "")
	{
		$option = new HtmlSelectboxOption($value, $caption);
		$this->prependChild($option);
		return $option;
	}

	/**
	 * @brief Returneert alle options
	 *
	 * @return de options in deze HtmlOptGroup.
	 */
	public function getOptions ()
	{
		$ret = array();
		foreach ($this->getChildren() as $option)
			if ($option instanceof HtmlSelectboxOption)
				$ret[] = $option;
		return $ret;
	}
	
	/**
	 *	@brief Maak van een associatieve array een HtmlOptGroup.
	 *	@param label het label van de groep
	 *	@param options de associatieve array
	 *	@param selected een array van elementen die geselecteerd moeten worden.
	 *	@return de selectbox
	 */
	public static function fromArray ($label, $options, $selected = array())
	{
		$box = new HtmlOptGroup($label);
		if (!is_array($selected))
			$selected = array($selected);
		foreach ($options as $value => $label)
		{
			$opt = $box->addOption($value, $label);
			if (in_array($value, $selected))
				$opt->setSelected();
		}
		return $box;
	}
}
