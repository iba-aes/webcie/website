<?php
// $Id$

/**
 *	@brief Implementeert een HTML form textarea
 */
class HtmlTextarea
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw textarea-element.
	 *
	 * @param name De naam van het nieuwe element.
	 * @param cols Het aantal kolommen van het nieuwe element.
	 * @param rows Het aantal rijen van het nieuwe element.
	 */
	public function __construct($name = "", $cols = null, $rows = null)
	{
		parent::__construct('textarea', true);
		$this->_allowedAttributes[] = "name";
		$this->_allowedAttributes[] = "cols";
		$this->_allowedAttributes[] = "rows";
		$this->_allowedAttributes[] = "readonly";
		$this->_allowedAttributes[] = "required";
		$this->_allowedAttributes[] = 'placeholder';

		$this->setAttribute("name", $name);

		if($cols)
			$this->setAttribute("cols", (int)$cols);
		if($rows)
			$this->setAttribute("rows", (int)$rows);
	}

	/**
	 * @brief Maak deze HtmlTextarea groot.
	 *
	 * Groot betekent 60 kolommen en 5 rijen.
	 */
	public function setLarge()
	{
		$this->setAttribute("cols", 60);
		$this->setAttribute("rows", 5);
	}

	/**
	 * @brief Maak een nieuwe HtmlTextarea met inhoud.
	 *
	 * @param content De inhoud van de nieuwe HtmlTextarea.
	 * @param name De naam van het nieuwe element.
	 * @param cols Het aantal kolommen van het nieuwe element.
	 * @param rows Het aantal rijen van het nieuwe element.
	 * @return De nieuwe HtmlTextarea.
	 */
	public static function withContent($content, $name = "", $cols = null, $rows = null)
	{
		$area = new HtmlTextarea($name, $cols, $rows);
		$area->add($content);
		return $area;
	}

	/**
	 * @brief Stel de placeholder van de textarea in.
	 *
	 * @param placeholder de nieuwe placeholder van de textarea.
	 * @return deze HtmlTextarea.
	 */
	public function setPlaceholder ($placeholder = null)
	{
		$this->setAttribute('placeholder', $placeholder);
		return $this;
	}

	protected function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('form-control');

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
