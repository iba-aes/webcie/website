<?
// $Id$

/**
 *	@brief Class om een HTML no script-element te produceren
 */
class HtmlNoScript
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw no script-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlNoScript 
	 * automatisch als kind(eren) krijgt.
	 */
	public function __construct($children = array())
	{
		parent::__construct('noscript', true);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
