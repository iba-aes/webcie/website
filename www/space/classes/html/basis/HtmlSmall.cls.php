<?
// $Id$

/**
 *	@brief Class om een HTML small-element te produceren
 */
class HtmlSmall
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw pre-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlSmall
	 * automatisch als kind(eren) krijgt.
	 */
	public function __construct($children = array())
	{
		parent::__construct('small', true);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
