<?php
// $Id$

/**
 *	@brief Implementeert een HTML table header cell.
 */
class HtmlTableHeaderCell
	extends HtmlTableDataCell
{
	/**
	 * @brief Construeer een nieuw table header-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTableHeader
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct();
		$this->_elementName = 'th';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
