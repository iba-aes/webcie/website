<?php
// $Id$

/**
 *	@brief Deze class implementeert een HTML Emphasis container: &lt;em&gt;
 */
class HtmlEmphasis
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw emphasis-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlEmphasis
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('em', true);

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
