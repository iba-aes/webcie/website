<?php

/**
 * @brief Implementeert een HTML button-element voor het gebruik in forms.
 *
 * Niet te verwarren met een HtmlInput van type button, reset of submit,
 * maar wel erg vergelijkbaar.
 *
 * @see HtmlInput
 */

class HtmlButton
	extends HtmlContainer
{
	public function __construct($type = "submit", $children = null, $name = null, $value = null, $class = 'btn btn-default', $id = null) {
		if ($type != "submit" && $type != "reset" && $type != "button" && $type != "menu") {
			throw new BadMethodCallException("Buttons moeten 'submit', 'reset', 'button' of 'menu' zijn");
		}
		parent::__construct('button', true);
		// TODO: hier mag nog een heleboel bij als we HTML5-features willen
		$this->_allowedAttributes[] = 'disabled';
		$this->_allowedAttributes[] = 'href';
		$this->_allowedAttributes[] = 'name';
		$this->_allowedAttributes[] = 'type';
		$this->_allowedAttributes[] = 'value';
		$this->_allowedAttributes[] = 'onclick';

		if ($class != null) {
			$this->addClass($class);
		}
		if ($id != null) {
			$this->setAttribute('id', $id);
		}
		if ($name != null) {
			$this->setAttribute('name', $name);
		}
		if ($value != null) {
			$this->setAttribute('value', $value);
		}
		$this->setAttribute('type', $type);
		if ($children != null) {
			$this->addChildren($children);
		}
	}
}
