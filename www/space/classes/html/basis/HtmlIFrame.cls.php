<?php
// $Id$

/**
 *	@brief Implementeert een IFrame
 */
class HtmlIFrame
	extends HtmlContainer
{

	/**
	 * @brief Construeer een nieuw IFrame-element.
	 *
	 * @param src De pagina die wordt weergegeven in het IFrame.
	 */
	public function __construct($src = null)
	{
		parent::__construct('iframe', true);
		$this->_allowedAttributes[] = 'src';
		$this->_allowedAttributes[] = 'onload';

		if($src) $this->setAttribute("src" ,$src);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
