<?php
// $Id: HtmlTable.cls.php 10042 2010-06-01 21:47:54Z sjeik $

/**
 *  @brief Implementeert een HTML lijst
 */
class HtmlList
	extends HtmlContainer
{

	/**
	 * @brief Construeer een nieuw list-element.
	 *
	 * @param ordered True om een geordende lijst te maken, False om een 
	 * ongeordende lijst te maken.
	 * @param children Een HtmlElement of array daarvan, die deze HtmlList 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct($ordered = false, $children = array(), $class = null, $id = null)
	{
		parent::__construct( $ordered ? 'ol' : 'ul', false);
		$this->_allowedChildren[] = 'HtmlListItem';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}

	/**
	 * @brief Override die een $child in een HtmlListItem stopt.
	 * 
	 * @param child Het kind dat toegevoegd wordt.
	 * @param class De klassen die de HtmlListItem krijgt.
	 * @param id Het id wat de HtmlListItem krijgt.
	 */
	public function addChild($child, $class = null, $id = null)
	{
		if(is_null($child))
			return;

		if(!$child instanceof HtmlListItem)
			$child = new HtmlListItem($child, $class, $id);

		parent::addChild($child);
	}

	/**
	 * Voeg deze lijst samen met een andere.
	 *
	 * @param list Een andere HtmlList.
	 * @return Deze lijst met alle kinderen van list eraan toegevoegd.
	 */
	public function merge (HtmlList $list)
	{
		$childs = $list->getChildren();
		foreach ($childs as $child)
			$this->add($child);
		return $this;
	}

	protected function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('list-group list-group-hover');

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
