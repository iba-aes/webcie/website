<?php
/**
 *	Deze klasse stelt een html element label voor.
 */
class HtmlLabel
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw label-element.
	 *
	 * @param for De naam van het formulieronderdeel waar dit label voor is.
	 * @param children Een HtmlElement of array daarvan, die deze HtmlLabel 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($for = '', $children = array(),  $class = null, $id = null)
	{
		parent::__construct('label', true);
		$this->_allowedAttributes[] = "class";
		$this->_allowedAttributes[] = "id";
		$this->_allowedAttributes[] = "for";

		if ($for != null)
			$this->setAttribute('for', $for);
		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}
}
