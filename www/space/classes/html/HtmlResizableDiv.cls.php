<?php
/**
	Klasse die een DIV-element kan maken dat een beperkte grootte heeft die
	ingesteld kan worden. Zo kan enkel de bovenkant van een lijst met items
	worden weergegeven, terwijl de onderkant verborgen wordt. Een druk op de
	knop toont ook de onderkant van de lijst.
*/
class HtmlResizableDiv extends HtmlDiv {
	private $_initialWidth = null;
	private $_initialHeight = null;

	public function __construct($initialWidth = "100%", $initialHeight = "100%", $class = null)
	{
		parent::__construct();
		$this->ensureId();
		$this->_initialWidth = $initialWidth;
		$this->_initialHeight = $initialHeight;

		if ($class != null)
			$this->addClass($class);
	}
	/**
		Maakt een "button" (icon) dat de state van de div representeert.
		Het icon is een link naar de tegenovergestelde state.
	*/
	function makeButton($iconExpand = '/Layout/Images/Icons/expand.png', $iconCollapse = '/Layout/Images/Icons/collapse.png'){
		// icon tonen met pijltjes omlaag
		$icon = $iconExpand;
		$alt = _("Meer informatie tonen");
		$icon = new HtmlImage($icon, $alt,  _("In/uitklappen"));
		$icon->ensureId();

		$id = $this->getid();
		$innerdiv_id = $id . "_innerdiv";
		$iconid = $icon->getID();
		$gradientid = $id . "_gradient";
		
		$initialHeight = $this->_initialHeight;
		$initialWidth = $this->_initialWidth;

		$jscript = "document.getElementById('$innerdiv_id').style.height = " .
			"(document.getElementById('$innerdiv_id').style.overflow == 'hidden' ? 'auto' : '$initialHeight'); ";
		$jscript .= "document.getElementById('$innerdiv_id').style.width = " .
			"(document.getElementById('$innerdiv_id').style.overflow == 'hidden' ? 'auto' : '$initialWidth'); ";
		$jscript .= "document.getElementById('$innerdiv_id').style.overflow = " .
			"(document.getElementById('$innerdiv_id').style.overflow == 'hidden' ? 'visible' : 'hidden'); ";
		$jscript .= "document.getElementById('$iconid').src = " .
			"(document.getElementById('$innerdiv_id').style.overflow == 'hidden' ? '$iconExpand' : '$iconCollapse'); ";
		$jscript .= "document.getElementById('$gradientid').style.visibility = " .
			"(document.getElementById('$innerdiv_id').style.overflow == 'hidden' ? 'visible' : 'hidden'); ";

		
		$anchor = new HtmlAnchor("javascript:void(0)", $icon);
		$anchor->setAttribute("onclick", $jscript);

		return $anchor;
	}

	protected function prepareHtml(){
		// Een extra DIV invoegen...
		$content = $this->_children;
		$this->_children = array();

		$this->add($innerdiv = new HtmlDiv());
		$innerdiv->_children = $content;
		$innerdiv->setId($this->getId() . "_innerdiv");
		$innerdiv->setCssStyle("height: " . $this->_initialHeight . "; width: " . $this->_initialWidth . "; overflow: hidden");

		// Zorgen voor fade-out effect onderkant lijst
		$this->add($img = new HtmlImage("/Layout/Images/gradient-verticaal.png", 'gradient'));
		$this->setCssStyle("position: relative;");
		$img->setCssStyle("width: 100%; height: 20px; position: relative; bottom: 20px; z-index: 1; pointer-events:none;");
		$img->setId($this->getId() . "_gradient");

		// Knop neerzetten
		$this->add($button = $this->makeButton());
		$button->setCssStyle("position: absolute; bottom: 0px; left: 0px; z-index: 2;");
		//$button->setCssStyle("z-index: 2;");
	}
}

