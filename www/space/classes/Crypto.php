<?php

/**
 * @brief Klasse om crypto-gerelateerde dingen te doen.
 *
 * @see BCryptHandler
**/

class Crypto
{
	/**
	 * @brief Geef cryptografisch veilige random bytes.
	 *
	 * @param count Het aantal bytes om te genereren.
	 *
	 * @return Een string met random bytes erin.
	 */
	public static function getRandomBytes($count)
	{
		$bytes = '';
		if(function_exists('openssl_random_pseudo_bytes'))
		{
			$bytes = openssl_random_pseudo_bytes($count, $allesGoed);
			if(!$allesGoed)
				user_error("OpenSSL is brak vandaag, kan geen hoogwaardige random data genereren", E_USER_ERROR);
		}
		else
		{
			user_error("openssl_random_pseudo_bytes is niet beschikbaar, ik kan niets doen!", E_USER_ERROR);
		}
		return $bytes;
	}

	/**
	 * @brief Encodeer bytes tot printbare tekst.
	 *
	 * @param input Een string met arbitraire bytes erin.
	 *
	 * @return Een string die printbaar is en dezelfde entropie heeft.
	 */
	public static function encodeBytes($input)
	{
		// The following is code from the PHP Password Hashing Framework
		$itoa64 = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

		$output = '';
		$i = 0;
		do {
			$c1 = ord($input[$i++]);
			$output .= $itoa64[$c1 >> 2];
			$c1 = ($c1 & 0x03) << 4;
			if ($i >= 16) 
			{
				$output .= $itoa64[$c1];
				break;
			}

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 4;
			$output .= $itoa64[$c1];
			$c1 = ($c2 & 0x0f) << 2;

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 6;
			$output .= $itoa64[$c1];
			$output .= $itoa64[$c2 & 0x3f];
		} while (1);

		return $output;
	}
}
