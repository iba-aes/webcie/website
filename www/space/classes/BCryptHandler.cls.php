<?php

/**
	@brief Deze klasse maakt nieuwe bcrypt hashes en kan een wachtwoord met
			een opgeslagen hash vergelijken

	bcrypt is een cpu-intensieve hashing functie. De rekentijd kan je
	beinvloeden door $rounds, het aantal iteraties is namelijk 2^$rounds
	Met rounds=12 duurt het ongeveer 0.4 sec (jan. 2013, op Max). 
	Dat is voorlopig wel even genoeg.
	
	De gegeneerde hash bevat zowel een salt als de daadwerkelijke hash, en
	ook nog eens het aantal rounds. Je checkt dus een wachtwoord door deze 
	te bcrypten met de hash uit de database. Dat zou dus diezelfde hash op 
	moeten leveren!
	
	Omdat bcrypt de key voor elke nieuwe ronde nodig heeft kun je niet het
	aantal rounds verhogen zonder dat je het plaintext wachtwoord hebt. Dat 
	moet dus gebeuren bij het inloggen van de gebruiker. Wel kun je een
	verschillend aantal rounds per gebruiker hebben, omdat dit in de hash 
	is opgeslagen en dus geen tussenkomst van de code nodig heeft.
	
	OpenSSL wordt gebruikt voor extra bonusentropie in de salts.
	
	Code komt grotendeels uit http://stackoverflow.com/questions/4795385/how-do-you-use-bcrypt-for-hashing-passwords-in-php/4795552
	
	Let op: we gebruiken nu de 2a-modus. Zie
	http://www.php.net/security/crypt_blowfish.php
	over waarom dit verkeerd is. Bij volgende debian-update overstappen
	naar 2y-modus
**/

class BCryptHandler 
{
	private $rounds;
	
	public function __construct($rounds = 12) 
	{
		if(!defined("CRYPT_BLOWFISH") || CRYPT_BLOWFISH != 1)
			user_error("BCrypt is niet beschikbaar, ik kan niets doen!", E_USER_ERROR);
		$this->rounds = $rounds;
	}
	
	/** @brief Eet een plaintext wachtwoord, genereer een salt en maak een hash **/
	public function nieuweHash($input) 
	{
		$hash = crypt($input, $this->makeSalt());

		if(strlen($hash) > 13)
			return $hash;

		user_error("BCrypt kan geen wachtwoord hashen, dit is zeer ernstig", E_USER_WARNING);
		return false;
	}

	/** @brief Neem een plaintext wachtwoord en een salt en check of ze overeenkomen **/
	public function verifieer($input, $existingHash) 
	{
		if(strlen($existingHash) != 60)
			user_error("Je hebt de parameters verwisseld, sjaak!", E_USER_ERROR);
		$hash = crypt($input, $existingHash);
		// gebruik hash_equals om altijd dezelfde timing te hebben (timing attack)
		// eerst werd hier === gebruikt om mogelijke casting te blokkeren
		return hash_equals($hash, $existingHash);
	}

	private function makeSalt()
	{
		$salt = sprintf('$2a$%02d$', $this->rounds);
		$bytes = Crypto::getRandomBytes(16);
		$salt .= Crypto::encodeBytes($bytes);
		return $salt;
	}
}
