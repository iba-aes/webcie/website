<?php

$clstm = new ProfilerTimerMark('classes/includes.php');
Profiler::getSingleton()->addTimerMark($clstm);

function classes_autoload($name)
{
	switch($name)
	{
	case 'BestuursLid':
		require_once('space/classes/BestuursLid.php');
		break;
	case 'BCryptHandler':
		require_once('space/classes/BCryptHandler.cls.php');
		break;
	case 'Crypto':
		require_once('space/classes/Crypto.php');
		break;
	case 'DateTimeLocale':
		require_once('space/classes/DateTimeLocale.cls.php');
		break;
	case 'Email':
		require_once('space/classes/Email.php');
		break;
	case 'Money':
		require_once('space/classes/Money.cls.php');
		break;
	case 'Token':
		require_once('space/classes/Token.cls.php');
		break;
	case 'Rss_Item':
 		require_once('space/classes/Rss/Item.php');
		break;
	case 'VCalendar_Item':
		require_once('space/classes/VCalendar/Item.php');
		break;
	case 'VCalendar_Event':
		require_once('space/classes/VCalendar/Event.php');
		break;
	}
}
spl_autoload_register('classes_autoload');

require_once('Page/init.php');
require_once('html/init.php');

$clstm->markEnd();
