<?php
/**
 * Stelt een VCalendar-event voor
 */
class VCalendar_Event extends VCalendar_Item
{
	public function __construct ()
	{
	}

	public function __toString ()
	{
		$output = 'BEGIN:VEVENT' . "\r\n"
				. parent::__toString()
				. 'END:VEVENT';
		return $output;
	}
}
