<?php
require_once('space/classes/html/init.php');

/**
 * @brief Een HTMLpagina zonder specifieke opmaak.
 *
 * Handig als je iets met iframes wilt doen,
 * of als je eenmalig speciale opmaak nodig hebt.
 */
class CleanHTMLPage extends Page
{
	private $started;	// ->start() should only be called once
	private $content;
	private $head;

	public function __construct()
	{
		$this->started = false;
		$this->content = new HtmlElementCollection();
		$this->head = array();
	}

	public function getContentDiv()
	{
		return $this->content;
	}

	public function addHead($line)
	{
		if ($this->started)
		{
			trigger_error("Er is al begonnen met output.", E_USER_ERROR);
		}
		$this->head[] = $line;
		return $this;
	}

	public function addHeadCSS($script, $media = 'screen')
	{
		$this->addHead('<link rel="stylesheet" href="'.$script.'" '
						.'type="text/css" media="'.$media.'">');
		return $this;
	}

	/**
	 * \brief Voeg CSS aan de head toe, maar dan inline met een style-tag.
	 */
	public function addHeadCSSInline($content)
	{
		$this->addHead('<style>' . $content . '</style>');
		return $this;
	}

	/**
	 * @brief Begin met deze pagina te genereren.
	 *
	 * @param maintitle Indien niet NULL, krijgt de pagina een `<title>`-tag met dit erin.
	 */
	public function start ($maintitle = null)
	{
		if($this->started) {
			user_error('LET OP: Page->start() werd voor een tweede keer'
			          . ' aangeroepen.  Dit moet gefixed worden!'
			          , E_USER_NOTICE);
			return $this;
		}

		// Indien nodig voegen we een title toe.
		if (!is_null($maintitle)) {
			$this->addHead('<title>' . htmlspecialchars($maintitle) . '</title>');
		}

		$this->started = true;

		// Begin met de pagina te bouwen.
		$this->add('<html>');
		if (count($this->head) > 0) {
			$this->add('<head>');
			foreach ($this->head as $headElement)
			{
				$this->add($headElement);
			}
			$this->add('</head>');
		}

		return $this;
	}

	public function add(...$children)
	{
		if(!$this->started) {
			user_error('LET OP: Page->start() nog niet aangeroepen.'
			          . ' Dit moet eerst gefixed!'
			          , E_USER_ERROR);
		}

		$this->content->addChildren(...$children);
		return $this;
	}

	/**
	 * @brief Rond de pagina af en schrijf het allemaal naar stdout.
	 *
	 * Gaat hilarisch stuk als je headers gebruikt enzo.
	 * @deprecated Gebruik de `respond`-methode.
	 *
	 * @sa respond
	 */
	public function end()
	{
		if (getZeurmodus())
		{
			user_error("Gebruik CleanHTMLPage::respond", E_USER_DEPRECATED);
		}

		echo $this->respond();
		exit;
	}

	/**
	* @brief Rond de pagina af en geef een mooie Html-string.
	*/
	public function respond()
	{
		return $this->content->makeHtml() . '</html>';
	}

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 */
	public function contentType() {
		return 'text/html; charset=utf-8';
	}
}

/**
 * @deprecated Gebruik CleanHTMLPage
 */
class CleanHtml_Page extends CleanHTMLPage { }
