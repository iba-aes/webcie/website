<?php
/**
 * Wordt gebruikt om txt-bestanden naar het scherm te sturen
 */
class TextPage extends Page
{
	private $started = false;

	private $content = [];

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 *
	 * @return Een string met de waarde van de content-type.
	 */
	public function contentType()
	{
		return 'text/plain; charset=utf-8';
	}

	/**
	 * @brief Doet niets maar lijkt op andere Page::start-methoden.
	 *
	 * @param title Wordt genegeerd.
	 */
	public function start($title = '')
	{
		return $this;
	}

	public function add(...$children)
	{
		foreach ($children as $child)
		{
			$this->content[] = (string)$child;
		}

		return $this;
	}

	public function end()
	{
		echo $this->respond();
		exit;
	}

	/**
	 * @brief Geef de inhoud van deze pagina.
	 *
	 * @return Een string met platte tekst.
	 */
	public function respond()
	{
		return implode("\n", $this->content);
	}
}

/**
 * @brief Hetzelfde als TextPage maar dan met een lelijke underscore.
 *
 * Nodig voor backwards compatibility.
 *
 * @deprecated Gebruik TextPage want die past in de conventies.
 */
class Text_Page extends TextPage
{
	public function __construct()
	{
		if (getZeurmodus())
		{
			user_error("Denk aan de conventies, gebruik Textpage ipv Text_Page!", E_USER_DEPRECATED);
		}
	}
}
