<?php

namespace Space;

use \Exception;

/**
 * Wordt gegooid als er geen chocola valt te maken van een string, bij het parsen van input.
 */
class GeenChocolaException extends Exception { }
