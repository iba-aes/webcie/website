<?php

/**
 * Representeert een bestuurslid van de vereniging, met alle nuttige functies
 * die erbij horen.
 *
 * Dit is geen Contact/Lid/Persoon DBObject. Hiervoor moet je in WhosWho4
 * zitten. Je kunt wel het bijbehoorende lid krijgen door
 * `Lid::geef($bestuurslid->getLidNummer())`.
 *
 * Deze class wordt heel vroeg ingeladen door space/init.php omdat dit gebruikt
 * wordt door space/specialconstants.php & space/auth/autorisatie.php
 */
class BestuursLid {
	/** Het bestuurslid is lid van de vereniging en heeft dit ID */
	private $lidNummer;
	/** Dit is de leesbare naam van de functie */
	private $functie = NULL;
	/** E-mailadres deel voor @a-eskwadraat.nl */
	private $emailPrefix;
	/** Of deze persoon boekencommissaris mogen zijn (mag >1 zijn) */
	private $isBoekCom;

	public function __construct($lidNummer, $emailPrefix, $isBoekCom = false) {
		$this->lidNummer = $lidNummer;
		$this->emailPrefix = $emailPrefix;
		$this->isBoekCom = $isBoekCom;
	}

	/**
	 * De functie moet hier pas worden ingevuld en niet in de constructor omdat
	 * we dit vanuit space/specialconstants.php aanroepen bij het aanmaken van de
	 * $BESTUUR array die als key de functie heeft. Hiermee voorkomen we een
	 * string die dubbelop is.
	 * Eenmaal een functie gekregen, kan een bestuurslid niet van functie
	 * veranderen.
	 * @param functie de functie van dit bestuurslid binnen het bestuur
	 */
	public function setFunctie($functie) {
		// Het is maar één keer te veranderen
		if (is_null($this->functie)) {
			$this->functie = $functie;
		}
	}

	/** Het bestuurslid is lid van de vereniging en heeft dit ID */
	public function getLidNummer() {
		return $this->lidNummer;
	}

	/** Dit is de leesbare naam van de functie */
	public function getFunctie() {
		return $this->functie;
	}

	/** E-mailadres deel voor @a-eskwadraat.nl */
	public function getEmailPrefix() {
		return $this->emailPrefix;
	}

	/** 
	 * @brief Geeft e-mailadres veld wat in een e-mail als afzender/ontvanger gebruikt kan worden.
	 * Dit bevat ook een toepasselijke naam van de persoon in tegenstelling tot
	 * getEmailAdres
	 * Dit wordt gebruikt in de Email-klasse
	 */
	public function getEmail() {
		// TODO: enige vorm van escaping indien nodig...
		return '"' . ucfirst($this->functie) . ' A-Eskwadraat" <' .
			$this->getEmailAdres() . '>';
	}

	/**
	 * @brief geeft het e-mailadres van het bestuurslid
	 * Dit kan gebruikt worden voor een mailto: e-mailadres want die eet geen
	 * getEmail() output!
	 */
	public function getEmailAdres() {
		return "$this->emailPrefix@A-Eskwadraat.nl";
	}

	/** Of deze persoon boekencommissaris mogen zijn (mag >1 zijn) */
	public function isBoekCom() {
		return $this->isBoekCom;
	}
};

