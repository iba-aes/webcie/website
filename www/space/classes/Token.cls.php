<?php
// $Id$

/**
 * @brief Representeert een (gebruikers)fout ontstaan in de `Token`-klasse.
 *
 * Deze exceptie wordt gegooid als een token niet klopt,
 * en als die niet wordt opgevangen geeft `maakResponse` aan de gebruiker een
 * melding dat de token stuk is en of ze niet kunnen proberen te refreshen.
 */
class TokenException extends ResponseException
{
	/**
	 * @brief De HTMLPage met uitleg over de exceptie voor de gebruiker.
	 */
	private $page;

	/**
	 * @brief Constructor.
	 *
	 * @param errorcode een const om de error te bepalen, bijv. Token::INVALIDREFERER
	 * @param token de token die de foutmelding genereerde, zodat extra details gegeven kunnen worden
	 * @param redirect als deze URL ingesteld is, krijgt de gebruiker een link "ga verder"
	 */
	public function __construct($errorcode, $token, $redirect)
	{
		global $logger;

		$this->page = new HTMLPage();
		$this->page->start(_("Fout bij verwerken formulier"));

		$logger->info("Token is fout: code $errorcode");
		if ($token)
		{
			$logger->info("Token is fout: inhoud " . print_r($token, true));
		}

		// Zet de melding erin.
		$div = new HtmlDiv();
		$div->addClass("waarschuwing");
		$div->addChildren(Token::strictErrorDescription($errorcode));

		// Maak een linkje terug.
		if (!$redirect)
		{
			$redirect = "javascript:history.back();";
		}
		$anchor = new HtmlAnchor($redirect, _("Ga terug"));
		$div->addChildren(HtmlBreak::multiple(2), $anchor);

		// Zet het in de pagina.
		$this->page->add($div);
	}

	public function maakResponse()
	{
		return new PageResponse($this->page);
	}
}

/**
 * @brief Deze klasse implementeert en beheert tokens t.b.v. authenticatie en
 * verificatie van gebruikersacties
 *
 * Tokens bieden bescherming tegen CSRF (Cross-Site Request Forgery) attacks.
 * Een standaard-form (of het nu POST of GET is) brengt vaak een actie teweeg.
 * Of het nu komt door het opvragen van een URL met parameters daarin, of het
 * versturen van POST/GET variabelen. In veel gevallen is het eenvoudig om een
 * form met zulke POST/GET variabelen te kopieren en een ingelogde gebruiker
 * (met rechten) te verleiden het vervalsde form te gebruiken om zo een
 * bepaalde state change te bewerkstelligen.
 *
 * Door iedere form of state changing link met een uniek token uit te rusten,
 * kan dit voorkomen worden. Zo'n token is uniek, slechts 1x bruikbaar en
 * verloopt na een bepaalde periode. Zonder dit token mee te sturen kan een
 * bepaalde actie niet uitgevoerd worden. Doordat een token slechts eenmaal
 * bruikbaar is, worden ook dubbele state changes (door F5) tegengegaan.
 *
 * Merk op dat Tokens geen 100% bescherming bieden tegen CSRF-attacks. Een
 * aanvaller zou immers een pagina kunnen construeren die een request doet
 * naar een pagina, vervolgens een token uit de pagina extracten en dat token
 * gebruiken in een forged request.
 *
 * Voor de details over de implementatie van het token, zie de constructor.
 *
 * @see __construct
 **/
class Token
{
	private $_ttl;
	private $_identifier;
	private $_requri;
	private $_stamp;
	private $_used;
	private $_targeturi;
	private $_form;

	/** @brief Const gebruikt voor een geldig token * */
	const VALID = 1;

	/** @brief Const gebruikt voor een expired token * */
	const EXPIRED = 2;

	/** @brief Const gebruikt voor een token dat totaal onbekend is * */
	const UNKNOWN = 3;

	/** @brief Const gebruikt voor een token dat reeds eerder gebruikt is * */
	const ALREADYUSED = 4;

	/** @brief Const gebruikt voor een token met een invalid referer * */
	const INVALIDREFERER = 5;

	/** @brief Cosnt gebruikt voor een niet-bestand token * */
	const NOTOKEN = 6;

	/** @brief Const gebruikt wanneer een token niet geldig is voor deze pagina * */
	const INVALIDURI = 7;


	/**
	 * @brief Construeert een token en registreert dit in de sessie van de
	 * huidige gebruiker
	 *
	 * Een token bevat de volgende informatie:
	 * - moment van genereren (timestamp, in seconden sinds 1970)
	 * - time to live (geldigheidsduur, in seconden)
	 * - de volledige URL van de pagina die op dit moment opgevraagd wordt
	 * - een random number
	 * - optioneel: de URL (enkel local part) waarvoor het token geldig is
	 * - optioneel: een naam om het bijbehorden form mee te identificeren
	 *
	 * Van deze informatie, plus de huidige sessionid (wanneer beschikbaar) en
	 * het lidnr van de ingelogde gebruiker (wanneer beschikbaar) wordt een
	 * SHA1-hash berekend, welke dient als unieke identifier voor het token.
	 * De bovenstaande details van de token worden server-side in de session
	 * van de gebruiker opgeslagen, onder de unieke identifier.  Op elk moment
	 * kan geverifieerd worden of een token dat gebruikt wordt in een page
	 * request geldig is, door de sessie-informatie van de gebruiker te
	 * bekijken.
	 *
	 * De naam van het formulier wordt NIET gebruikt in checks.
	 *
	 * Bij GET requests moeten we alles na de ? negeren, dit veranderd namelijk
	 * iedere keer als we een request doen en zal bijna nooit overeenkomen met
	 * nieuwe/oude urls.
	 **/
	public function __construct($form = NULL)
	{
		global $session, $request;

		$this->_stamp = time();
		$this->_ttl = 86400;
		$this->_identifier = rand() * rand();
		$this->_requri = strtok($request->server->get("REQUEST_URI"), "?");
		$this->_used = false;
		$this->_form = $form;

		// Voeg de token toe aan de huidige sessie
		$session->set("tokens/" . $this->_identifier, $this);
	}

	/**
	 * @brief Bekijkt de tokens die geregistreerd staan in de session, en ruimt
	 * verlopen tokens op
	 **/
	public static function cleanupExpiredTokens()
	{
		global $session;

		if (!$session->has("tokens"))
		{
			return;
		}

		$cleanup = array();

		// Over alle tokens heenlopen
		foreach ($session->get("tokens") as $identifier => $token)
		{
			// Token verlopen? Toevoegen aan array $cleanup
			if ($token->hasExpired())
			{
				$cleanup[] = $token;
			}
		}

		foreach ($cleanup as $token)
		{
			$token->removeFromSession();
		}
	}

	/**
	 * @brief Bepaalt of dit token verlopen is
	 **/
	public function hasExpired()
	{
		return $this->_stamp + $this->_ttl < time();
	}

	/**
	 * @brief Probeert een token te verwerken en geeft de gebruiker eventueel een foutmelding.
	 *
	 * Deze functie is zo'n beetje de package deal voor default token
	 * handling:
	 * - controleert token
	 * - als het token valid is: markeer als 'gebruikt'
	 * - als het token invalid is: gooi een TokenException (zodat de gebruiker de fout te zien krijgt)
	 *
	 * @see TokenException
	 *
	 * @param redirect de URL om de aan de gebruiker te presenteren onder een
	 * link 'ga verder'. Als de parameter null is, krijgt de gebruiker geen
	 * link.
	 * @param identifier De identifier van het te controleren Token. By
	 * default null, in dat geval wordt geprobeerd om de POST/GET var 'token'
	 * te gebruiken
	 * @param notoken Is het ok als we GEEN token identifier hebben?
	 *
	 * @return Het token dat verwerkt is.
	 **/
	public static function processToken($redirect = null, $identifier = null, $notoken = false)
	{
		$rescode = Token::useToken($identifier);

		if ($notoken && $rescode === Token::NOTOKEN)
		{
			return NULL;
		}

		$token = Token::reconstruct($identifier);
		if ($rescode !== Token::VALID)
		{
			throw new TokenException($rescode, $token, $redirect);
		}

		return $token;
	}

	/**
	 * @brief Gebruikt een token. Bij problemen retourneert deze functie een
	 * const die het probleem representeert.
	 *
	 * Deze functie controleert of de referer overeen komt met de pagina die
	 * oorspronkelijk het token heeft uitgegeven. Tevens wordt natuurlijk
	 * gecontroleerd of het token niet verlopen is, en of het token niet
	 * eerder gebruikt is.
	 *
	 * Als het token geldig is, wordt het direct als 'gebruikt' gemarkeerd
	 * zodat het niet nogmaals gebruikt kan worden.
	 *
	 * @param identifier De identifier van de token dat gebruikt moet worden.
	 * Als de identifier null is, dan wordt geprobeerd om de GET/POST
	 * variabele 'token' uit te lezen en te gebruiken.
	 *
	 * @see validateToken
	 **/
	public static function useToken($identifier = null)
	{
		$valres = Token::validateToken($identifier);

		if ($valres === Token::VALID)
		{
			// Alles in orde
			$token = Token::reconstruct($identifier);
			$token->markUsed();
		}
		return $valres;
	}

	/**
	 * @brief Valideert een token. Retourneert VALID als alles in orde is, of
	 * een int als er iets mis is met het token.
	 *
	 * Deze functie controleert of de referer overeen komt met de pagina die
	 * oorspronkelijk het token heeft uitgegeven. Tevens wordt natuurlijk
	 * gecontroleerd of het token niet verlopen is, of eerder gebruikt is.
	 *
	 * De functie requireToken doet ongeveer hetzelfde als deze functie, maar
	 * zal een Exception gooien wanneer er iets mis is.
	 *
	 * @param identifier De identifier van de token dat gebruikt moet worden.
	 *
	 * @see VALID
	 * @see EXPIRED
	 * @see UNKNOWN
	 * @see ALREADYUSED
	 * @see INVALIDREFERER
	 * @see INVALIDURI
	 * @see NOTOKEN
	 *
	 * @return een class constant met de status van het token
	 **/
	public static function validateToken($identifier = NULL)
	{
		global $logger;
		global $request;

		if (!isset($identifier))
		{
			$identifier = tryPar("token");
		}
		if (!$identifier)
		{
			return Token::NOTOKEN;
		}

		$token = Token::reconstruct($identifier);
		if (!$token)
		{
			return Token::UNKNOWN;
		}
		if ($token->isUsed())
		{
			return Token::ALREADYUSED;
		}
		if ($token->hasExpired())
		{
			return Token::EXPIRED;
		}

		// Referer controleren. Let op: de referer is een complete URL,
		// inclusief hostname, terwijl een Token enkel het lokale deel van de
		// URL opslaat.
		// Als $requri == false dan heeft het token geen requri, dan hoeft het
		// ook niet gecontroleerd te worden.
		// TODO: wat als gebruikers hun referer niet meesturen?
		// TODO: AJAX, zorgt er voor dat je requri nooit gelijk is aan http_referer. (bug #5244)
		if ($token->_requri && $request->server->get("HTTP_REFERER"))
		{
			$referer = @strtok($request->server->get("HTTP_REFERER"), "?");
			$hostname = $request->server->get("HTTP_HOST");
			$hostnamepos = strpos($referer, $hostname);
			if ($hostnamepos === false)
			{
				return Token::INVALIDREFERER;
			}

			$referer_localpart = substr($referer, $hostnamepos + strlen($hostname));
			if ($referer_localpart != $token->getReqURI())
			{
				return Token::INVALIDREFERER;
			}
		}

		// Wanneer ingesteld: target URI controleren. Wanneer deze waarde !=
		// null is, dan is het token alleen geldig voor een bepaalde pagina.
		if ($token->_targeturi)
		{
			if (strtok($request->server->get("REQUEST_URI"), "?") != $token->_targeturi)
			{
				return TOKEN::INVALIDURI;
			}
		}

		// OK
		return Token::VALID;
	}

	/**
	 * @brief Werkt soortgelijk aan processToken, maar ipv het verwerkte token
	 * krijg je naam van het form terug.  Als
	 *
	 * @return NULL (bij geen Token) of de naam van het form
	 *
	 * @see processToken
	 * @see getFormName
	 **/
	public static function processNamedForm()
	{
		$token = Token::processToken(NULL, NULL, True);
		if (!$token)
		{
			return NULL;
		}

		return $token->getFormName();
	}

	/**
	 * @brief Verwijdert dit token uit de sessie van de gebruiker, waardoor
	 * het effectief niet meer bruikbaar is
	 *
	 * @see markUsed
	 **/
	public function removeFromSession()
	{
		global $session;

		$identifier = $this->_identifier;

		$session->remove("tokens/$identifier");
	}

	/**
	 * @brief Stelt de request URI van dit token in. Dit is de pagina waarop
	 * het Token gegenereerd werd.
	 *
	 * Als er een waarde voor request URI ingesteld wordt (by default is het
	 * $_SERVER["REQUEST_URI"] op het moment van genereren van het token), dan
	 * wordt de URI gecontroleerd bij het verwerken van het Token. Als de URI
	 * niet overeenkomt met de page referer, dan is het token ongeldig. Als
	 * er geen waarde ingesteld is (bijv. NULL of false), dan wordt dit niet
	 * gecontroleerd bij het verwerken van het Token. Handig in geval van
	 * Ajax-links, die hebben namelijk vaak vreemde referers.
	 **/
	public function setReqURI($requri)
	{
		$this->_requri = $requri;
	}

	/**
	 * @brief Stelt de target URI in waarvoor dit token geldig is.
	 *
	 * Door een target URI in te stellen, is het token enkel geldig voor het
	 * opvragen (POST/GET) van een bepaalde pagina (URL). Standaard is deze
	 * waarde niet ingesteld, en wordt dit dus ook niet gecontroleerd.
	 **/
	public function setTargetURI($targeturi)
	{
		$this->_targeturi = $targeturi;
	}

	public function setFormName($form)
	{
		$this->_form = $form;
	}

	/** @brief Retourneert het moment waarop dit token gegenereerd is (in seconden sinds start Unix epoch) * */
	public function getStamp()
	{
		return $this->_stamp;
	}

	/** @brief Set het moment waarop dit token gegenereerd is (in seconden sinds start Unix epoch) * */
	public function setStamp($stamp)
	{
		$this->_stamp = $stamp;
	}

	/** @brief Retourneert de time-to-live van dit Token (in seconden) * */
	public function getTTL()
	{
		return $this->_ttl;
	}

	/** @brief Set de time-to-live van dit Token (in seconden) * */
	public function setTTL($ttl)
	{
		$this->_ttl = $ttl;
	}

	/** @brief Retourneert de URI van de pagina waarvoor dit token gegenereerd is * */
	public function getReqURI()
	{
		return $this->_requri;
	}

	/** @brief Retourneert de unieke identifier van dit Token * */
	public function getIdentifier()
	{
		return $this->_identifier;
	}

	/** @brief Retourneert de (optionele) form naam die bij dit Token hoord * */
	public function getFormName()
	{
		return $this->_form;
	}

	/** @brief Retourneert of dit Token reeds eerder gebruikt is * */
	public function isUsed()
	{
		return $this->_used;
	}

	/**
	 * @brief Markeert het token als 'gebruikt'
	 *
	 * Op deze manier kan een gebruiker gewaarschuwd worden als een token twee
	 * keer gebruikt wordt. In dat geval heeft die waarschijnlijk een form
	 * voor een tweede keer verstuurd.
	 **/
	public function markUsed()
	{
		$this->_used = true;
	}

	/**
	 * @brief Genereert een nieuw token en retourneert de identifier
	 *
	 * Deze functie retourneert een nieuw token en retourneert vervolgens de
	 * identifier. Handig om oneliners mee te schrijven, aangezien je verder
	 * toch niet veel met het Token object hoeft te doen.
	 *
	 * @param requri Optionele URI waarvandaan dit token geldig moet zijn. Als
	 * bij het valideren van het token blijkt dat de referer niet gelijk is aan
	 * deze URI, dan is het token niet geldig. Als er expliciet 'false' als waarde
	 * wordt opgegeven, dan is het token geldig vanaf alle referer URIs. De
	 * default is NULL, dan wordt de huidige $_SERVER["REQUEST_URI"] gebruikt.
	 *
	 * @return De identifier van het nieuw gegenereerde token
	 **/
	public static function generate($requri = null)
	{
		$token = new Token();
		if ($requri !== null)
		{
			$token->setReqURI($requri);
		}
		return $token->getIdentifier();
	}

	/**
	 * @brief Probeert een Token te reconstrueren uit de sessie a.h.v. de
	 * identifier
	 *
	 * @return Het Token behorend bij de identifier, of null als het token
	 * niet bestaat. Let op! Het geretourneerde Token zou reeds gebruikt
	 * kunnen zijn, of verlopen.
	 **/
	public static function reconstruct($identifier = NULL)
	{
		global $session;

		if (!isset($identifier))
		{
			$identifier = tryPar("token");
		}

		return $session->get("tokens/$identifier");
	}

	/**
	 * @brief kijkt of er een token is meegestuurd naar deze pagina, maar
	 * niet is gebruikt -> probleem?
	 **/
	public static function checkUnusedToken()
	{
		$tokenId = tryPar('token');
		if ($tokenId && !self::reconstruct($tokenId)->isUsed())
		{
			$errortoken = self::reconstruct($tokenId);
			trigger_error("Er is een token meegestuurd naar deze pagina, maar hij is niet gebruikt. Fix de formulierverwerking!\nExtra informatie, Request Uri:" . $errortoken->getReqURI() . " Form name: " . $errortoken->getFormName(), E_USER_WARNING);
		}
	}

	/**
	 * @brief Pakt een error (bijv. de const ALREADYUSED) en retourneert een
	 * omschrijving van de error voor de gebruiker.  VALID en NOTOKEN worden
	 * niet gezien als een error.
	 *
	 * @return Een string met daarin de omschrijving van het probleem
	 *
	 * @see strictErrorDescription
	 **/
	public static function errorDescription($status)
	{
		switch ($status)
		{
			case Token::VALID:
			case Token::NOTOKEN:
				return NULL;    // geen error melding, alles ok
			DEFAULT:
				return Token::strictErrorDescription($status);
		} // end switch
	}

	/**
	 * @brief Pakt een error (bijv. de const ALREADYUSED) en retourneert een
	 * omschrijving van de error voor de gebruiker.
	 *
	 * @return Een string met daarin de omschrijving van het probleem
	 *
	 * @see errorDescription
	 **/
	public static function strictErrorDescription($status)
	{
		switch ($status)
		{
			case Token::ALREADYUSED:
				return _("Je hebt geprobeerd om een actie meerdere malen uit " .
					"te voeren, terwijl het de bedoeling is dat je deze " .
					"slechts 1x uitvoert om onverwachte resultaten te " .
					"voorkomen. Heb je misschien op F5 gedrukt of een " .
					"formulier twee keer geprobeerd te versturen?"
				);
			case Token::UNKNOWN:
				return _("De A–Eskwadraatwebsite gebruikt een " .
					"beveiligingsmechanisme om hackers buiten de deur te " .
					"houden: tokens. Het token dat je gebruikte om deze " .
					"pagina op te vragen is ongeldig, er is dus iets goed " .
					"mis!"
				);
			case Token::EXPIRED:
				return _("Je hebt te lang gewacht met het klikken op een " .
					"of het versturen van een formulier. Om te voorkomen " .
					"actie een ongewenst resultaat oplevert, is deze niet " .
					"uitgevoerd. Je kunt het uiteraard nogmaals proberen!"
				);
			case Token::INVALIDREFERER:
				return _("De A–Eskwadraatwebsite gebruikt een " .
					"beveiligingsmechanisme om hackers buiten de deur te " .
					"houden: tokens. Het token dat je gebruikte om deze " .
					"pagina op te vragen staat geregistreerd, maar vanaf " .
					"een andere pagina. Er is dus iets goed mis! " .
					"(tip: controleer of je browser wel een referer " .
					"meestuurt bij het opvragen van een pagina)"
				);
			case Token::NOTOKEN:
				return _("De A–Eskwadraatwebsite gebruikt een " .
					"beveiligingsmechanisme om hackers buiten de deur te " .
					"houden: tokens. De actie die je probeerde uit te " .
					"vereist zo'n token, maar je hebt er geen " .
					"meegestuurd. Er is dus iets goed mis!"
				);
			case Token::INVALIDURI:
				return _("De A–Eskwadraatwebsite gebruikt een " .
					"beveiligingsmechanisme om hackers buiten de deur te " .
					"houden: tokens. De actie die je probeerde uit te " .
					"vereist zo'n token, het token dat je hebt " .
					"meegestuurd is niet geldig voor deze " .
					"actie/pagina. Er is dus iets goed mis!"
				);
			DEFAULT:
				return _("De A–Eskwadraatwebsite gebruikt een " .
					"beveiligingsmechanisme om hackers buiten de deur te " .
					"houden: tokens. Helaas is er iets mis gegaan bij het " .
					"controleren van jouw token, probeer het later nog " .
					"eens. (" . $status . ")"
				);
		} // end switch
	}
}

// vim:sw=4:ts=4:tw=0:foldlevel=1
