<?
require_once('space/init.php');

function incomingUrl_content($args)
{
	global $request;

	if (count($args) < 1) {
		return responseUitStatusCode(404);
	}

	if(!TinyUrl::tinyUrlByUrl($args[0])) {
		return responseUitStatusCode(404);
	}

	$tinyUrl = TinyUrl::geef(TinyUrl::tinyUrlByUrl($args[0]));
	$hit = new TinyUrlHit($tinyUrl);
	$hit->setHost($request->server->get('HTTP_X_FORWARDED_FOR'));
	$hit->setHitTime(new DateTimeLocale());
	$hit->opslaan();

	return responseRedirect($tinyUrl->getOutgoingUrl());
}
