<?php

namespace Space;

use vfsEntry;

// Parse de argumenten. Dit moet aan het begin om configuratie in te stellen voor bijvoorbeeld auth.
// Argumentenvorm: (--key $value)*
// TODO: is er een mooie library hiervoor?
$bestand = null;
$klasse = null;
$functie = null;
$level = null;
// Parse argumenten.
// We lezen steeds een key in en een optionele value.
// In de switch-statement bepalen we of we de value daadwerkelijk inlezen, door $i++ te doen.
for ($i = 1; $i < count($argv); $i++)
{
	$argKey = $argv[$i];
	$argValue = $argv[$i + 1];
	switch ($argKey)
	{
	case '--bestand':
		$bestand = $argValue;
		$i++;
		break;
	case '--klasse':
		$klasse = $argValue;
		$i++;
		break;
	case '--functie':
		$functie = $argValue;
		$i++;
		break;
	case '--level':
		$level = $argValue;
		$i++;
		break;
	default:
		throw new RuntimeException("Onbekende cli-argument " . $argKey);
	}
}

if (is_null($bestand))
{
	throw new RuntimeException("Ontbrekende cli-argument: --bestand <bestand>");
}
if (is_null($functie))
{
	throw new RuntimeException("Ontbrekende cli-argument: --functie <functie>");
}
if (is_null($level))
{
	throw new RuntimeException("Ontbrekende cli-argument: --level <level>");
}

// Stel globals in uit de commandlineparameters.
global $LEVEL;
$LEVEL = $level;

// auto_prepend moet voor alle andere php-files geincluded worden
require_once('space/auto_prepend.php');

// Laad ook alle andere dependencies in.
require_once('space/init.php');

// Initialiseer Space.
global $auth;
$space = new Space($auth);

// Lees argument in uit stdin.
$stdin = file_get_contents('php://stdin');

// Voer de benodigde functie uit.
// Deze functie moet een string nemen en een teruggeven.
require_once($bestand);
try
{
	if ($klasse)
	{
		$stdout = call_user_func([$klasse, $functie], $stdin);
	}
	else
	{
		$stdout = call_user_func($functie, $stdin);
	}
}
catch (\Throwable $e)
{
	// We geven normaal geen exitstatus terug, dus doe dat nu wel,
	// zodat de aanroeper weet wat er misging.
	// Bovendien echo'en we de foutmelding naar stdout omdat Python daar iets makkelijker mee omgaat.
	echo $e->getMessage();
	exit(1);
}

echo $stdout;

return;
