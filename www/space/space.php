<?php
declare(strict_types=1);

namespace Space;

// auto_prepend.php zou al geladen moeten zijn, maar expliciet is beter dan impliciet
require_once 'space/auto_prepend.php';
require_once 'space/init.php';

global $space;
$space = new Space();
$space->display();
die();
