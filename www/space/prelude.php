<?php
// Utilityfuncties die geen dependencies hebben op andere code.
// Deze worden als (bijna!) eerste ingeladen, dus Space en WhosWho4 zijn nog niet ingeladen.

/**
 * Wordt deze code uitgevoerd als script in plaats van website?
 *
 * @return bool
 */
function is_script()
{
	return php_sapi_name() == 'cli';
}
