<?php
/**
 * Dit object geeft wat willekeurige informatie terug over het huidige verzoek
 * dat wordt afgehandeld door het huidige request-object
 */
abstract class RequestAPI
{
	/**
	 * Geeft een array terug met wat willekeurige informatie over het huidige
	 * request-object
	 */
	static public function get ($id, $params)
	{
		// Het huidige request object waar alle info vandaan komt
		$request = Request::getInstance();

		// Prepareer de array met alles wat mogelijk opgevraagd kan worden
		$info = array(
			'login' => $request->getLoginID()
		);

		// Als er niks specifieks is gevraagd, returneer dan alles
		if (count($params['velden']) == 0)
			return $info;

		// Als er iets specifieks wordt gevraagd, doe dat dan teruggeven
		$info_flip = array_flip($info);
		$info_flip_and_asked = array_intersect($info_flip, $params['velden']);
		$info = array_flip($info_flip_and_asked);
		return $info;
	}
}
