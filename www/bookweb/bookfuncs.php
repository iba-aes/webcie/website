<?php
// $Id$

/*
	Hier staan alle algemene BookWeb calls
	calls? wat voor calls? wat zijn calls??
*/

// moet in lib ofzo
function hasPositive($ar)
{
	foreach($ar as $a)
	{
		if ($a>0)
			return true;
	}
	return false;
}

function go()
{
	return tryPar('action')=='go';
}

function sorteerOpDatum($a, $b) 
{
	//voor gebruik met usort	
	return $b['stamp_boeken'] - $a['stamp_boeken'];
}

function makeBookRef($function,$text,$vars='')
{
	$function = ucfirst($function);
	return makeRef(STARTPAGE."$function".
		($vars?"?$vars":""), $text);
}

// met boolean voor het uitschakelen van deze link
function makeCondBookRef($function,$text,$disabled = FALSE, $rood = FALSE,
	$vars = '')
{
	if($disabled || (!$rood && isActionDateTimeChanged())) {
		return '<span class="disabled">'.$text.'</span>';
	}

	$ref = makeBookRef($function,$text, $vars);
	if ($rood && isActionDateTimeChanged()) {
		$ref = '<span style="background-color: red">'.$ref.'</span>';
	}

	return $ref;
}

function makeBookHomeRef($text)
{
	return makeRef(STARTPAGE,$text);
}

function getLeveranciersSelect($defaultboeklevnr = null) {
	if (!is_numeric($defaultboeklevnr) || $defaultboeklevnr < 0) {
		$defaultboeklevnr = BOEKLEVNR;
	}

	$leveranciers = sqlLeveranciers();
	foreach($leveranciers as $leveranciernr => $leverancierdata)
		$levers[$leveranciernr] = $leverancierdata['naam'];

	return addSelect('leveranciernr', $levers, tryPar('leveranciernr',$defaultboeklevnr), TRUE);
}

/* Gegeven een zoekstring geeft deze functie een lijst met boeken die aan de
 * zoekstring voldoen.
 */
function getBoeken($searchstr) {
	global $request;

	/* is dit misschien een van onze interne dingen:
	 * Extern-123 of A-Es2-456 ? */
	if(preg_match("/Extern\-?(\d+)/i", $searchstr, $enum)) {
		$boekdata['EAN'] = ean_addcheck('22'.sprintf('%010s',$enum[1]));
	} elseif( preg_match("/A\-?Es.\-?(\d+)/i", $searchstr, $anum)) {
		$boekdata['EAN'] = ean_addcheck('21'.sprintf('%010s',$anum[1]));
	} else {
		$boekdata['EAN'] = $searchstr;
	}
	$boekdata['titel'] = $searchstr;
	$boekdata['auteur'] = $searchstr;
	$boeken = sqlAlikeBoeken($boekdata);

	// 1 hit: klaar
	if (sizeOf($boeken) == 1) {
		return $boeken[0]['EAN'];
	}

	// geen hits, maar toch een EAN dan return dat EAN
	if (sizeOf($boeken) == 0 && isValidEAN($boekdata['EAN'])) {
		return $searchstr;
	}

	// anders, geef gebruiker een herstelmogelijkheid
	$method = $request->server->get('REQUEST_METHOD');
	$urlvars = $method=='POST'?$_POST:$_GET;
	$searchkey = array_search($searchstr, $urlvars);

	$formstr = addForm('', $method);
	foreach ($urlvars as $key => $value) {
		if ($key != $searchkey) {
			$formstr .= addHidden($key, $value);
		}
	}

	// in geval 0, geef mogelijkheid tot editen van searchstring
	if( sizeOf($boeken) == 0) {
			printHTML('Er is geen artikel bij ons bekend met kenmerk \''
				.htmlspecialchars($boekdata['EAN']).'\' en dit is ook geen geldig EAN!');
			echo $formstr
				. "ISBN, Titel of auteur: "
				. addInput1('EAN', $boekdata['EAN'], FALSE, 30);
			normalEndForm('ik waag nog een gok');

			print_error('Probeer het opnieuw.');
	}

	// geval > 1, geef keuzelijst met clickable ISBNs
	usort($boeken, 'sorteerOpDatum');
	echo "\n<p>Er zijn meerdere artikelen gevonden die lijken op "
		."&quot;<em>".htmlspecialchars($searchstr)."</em>&quot;.<br />\n</p>";
	foreach ($boeken as $boek) {
		$buttonstr  = $formstr;
		$buttonstr .= addHidden($searchkey, $boek['EAN']);
		$buttonstr .= '<input type="submit" value="'.print_EAN($boek['EAN'])."\">\n</form>";

		$boek['EAN'] = $buttonstr;
		$newboeken[] = $boek;

	}
	echo arrayToHTMLTable($newboeken
		, array('EAN' => 'ISBN'
			,'titel'   => 'Titel'
			,'auteur'  => 'Auteur'
			,'druk'    => 'Druk'
			,'bestelbaar' 	=> 'Bestelbaar'
		),
		array('bestelbaar' => 'print_yn_red'),
		array('bestelbaar' => 'center'),
		null,
		"bw_datatable_medium_100"
	);

	print_error('Klik op het ISBN voor het juiste artikel.');
}


function print_yn_red($yn){
	if ($yn == "N") return "<font style=\"color: red\">Nee</font>";
	elseif ($yn == "Y") return "Ja";
	else return "(onbekend)";
}

// vertaalt Y=>Ja, N=>Nee
function print_yn($yn)
{
	if($yn == 'Y')
		return 'Ja';
	if($yn == 'N')
		return 'Nee';
	return '';
}

// cut_* verkort een veld voor grote tabellen

// cut_auteur poogt alleen de achternaam van de eerste auteur weer te geven
function cut_auteur($auteur, $ishtml = FALSE, $simplecut = false, $maxchars = 25)
{
	if ($simplecut){
		$newauteur = str_cut($auteur, $maxchars);
	} else {
		$t=explode('/', $auteur);
		$t=explode(',', array_shift($t));
		$newauteur = trim(array_shift($t));
	}

	if (strlen($newauteur) != strlen($auteur) && !$simplecut){
		// Blijkbaar is er intelligent geknipt, wel even aangeven
		$newauteur = $newauteur . " (...)";
	}

	return $ishtml?
		('<span title=\''.htmlspecialchars($auteur).'\'>'
		.htmlspecialchars($newauteur).'</span>'):
		$newauteur;
}

// cut_titel snijdt een titel af na $maxchars chars
function cut_titel($titel, $ishtml = FALSE, $maxchars = 25)
{
	$newtitel = strlen($titel) <= 27?$titel:str_cut($titel, $maxchars);
	return $ishtml?
		('<span title=\''.htmlspecialchars($titel).'\'>'
		.htmlspecialchars($newtitel).'</span>'):
		$newtitel;
}

// Retourneert een link naar Osiris. Als $caption een lege string is, dan
// wordt alleen de URL geretourneerd. Als $caption inhoud heeft, dan wordt
// er een complete a-href geretourneerd
function makeOsirisLink($vakcode, $collegejaar, $caption){
	$osirisLink = "https://www.osiris.universiteitutrecht.nl/osistu_ospr/OnderwijsKiesCursus.do?event=toonCursus";
	$osirisLink .= "&cursuscode=" . $vakcode;
	$osirisLink .= "&collegejaar=" . $collegejaar;

	if ($caption != ""){
		return makeExternLink($osirisLink, $caption, '_blank');
	}

	return $osirisLink;
}

// Retourneert een link naar de Informatica vakpagina. Voor verdere
// functionele omschrijving, zie hierboven de functie makeOsirisLink(...)
function makeInformaticaLink($vakcode, $collegejaar, $caption){
	$infLink = "http://www.cs.uu.nl/education/vak.php";
	$infLink .= "?jaar=" . $collegejaar;
	$infLink .= "&vak=" . $vakcode;

	return makeExternLink($infLink, $caption);
}

// Retourneert een link naar de Wiskunde vakpagina. Voor verdere
// functionele omschrijving, zie hierboven de functie makeOsirisLink(...)
function makeWiskundeLink($vakcode, $collegejaar, $caption){
	$cjaarkort1 = substr($collegejaar, 2, 2);
	$cjaarkort2 = $cjaarkort1 + 1;
	if ($cjaarkort2 < 10) $cjaarkort2 = "0" . $cjaarkort2;
	$cjaarkort = $cjaarkort1 . $cjaarkort2;

	$wisLink = "http://www.math.uu.nl/Onderwijs/vakken/$cjaarkort/Bachelor/";
	$wisLink .= strtoupper($vakcode) . "/";


	return makeExternLink($wisLink, $caption);
}

// Retourneert een link naar een externe site. Als $caption een lege string
// is (of iets vergelijkbaars, zoals null), dan wordt alleen weer de url
// geretourneerd. Als $caption niet leeg/null is, dan wordt er een a-href
// gemaakt met als target $target (Wanneer niet-null)
function makeExternLink($url, $caption, $target = null){
	if (!$caption) return $url;
	if ($target) $target = ' target="' . htmlspecialchars($target) . '"';

	return '<a href="' . htmlspecialchars($url) . "\"$target>" .
		htmlspecialchars($caption) . "</a>";
}


function makeBroeseLink($ean, $caption = ""){
	$isbn = ean_to_isbn($ean);
	$url = "http://www.selexyz.nl/product/$isbn";

	if ($caption){
		return "<a href=\"$url\">$caption</a>";
	}

	return $url;
}

function makeVergelijkLink($ean, $caption = "", $title = ""){
	if (is_isbn($ean)){
		$isbn = $ean;
	} else {
		$isbn = ean_to_isbn($ean);
	}

	$url = "http://www.vergelijk.nl/boek?pageNum=1&isbnid=$isbn";

	if ($caption){
		return "<a href=\"$url\"" . ($title?" title=\"" . $title ."\"":"") .
			">$caption</a>";
	}

	return $url;
}

/**
 * Nodig voor de advertentie in de footer
 */
function showadimage_content($rest)
{
      if (count($rest) != 1)
               spaceHTTP(404);
       putAdImage($rest[0]);
}
function putAdImage ($image)
{
       $realpath = SPOCIE_HOME . 'Logos/' . $image;
       if (substr($image, 0, 1) != '/' && substr($image, 0, 2) != '..'
               && file_exists($realpath))
       {
               $pos = strrpos($image, '.');
               $type = substr($image, $pos+1);
               global $ALLOWED_FILE_TYPES;
               if (!isset($ALLOWED_FILE_TYPES[$type])) {
                       user_error("Sponsorimage van onjuist filetype", E_USER_ERROR);
               }
               return sendfilefromfs($realpath, null, '/');
       } else {
               user_error("Invalid filename", E_USER_ERROR);
       }
}

/**
 * Functies die vroeger in aes2funcs.php stonden
 */

/* Neem van 2D array $ar[][] elke keer het $i element, oftwel, geef alle
 * $ar[][$i] terug. */
function unzip($ar, $i) {
	$ret = array();
	foreach ($ar as $key => $elem) {
		$ret[$key] = $elem[$i];
	}
	return $ret;
}

/* Maak een html table, met de inhoud van arr, en headers $header. Er wordt
 * vanuit gegaan dat alles geldige html is, GEEN escaping dus, tenzij de
 * laatste parameter true is!
 * De keys van headers worden gebruikt als de lookupkey van elke rij in $arr
 * De derde parameter is een array met "vertalingsfuncties", zodat je elke
 * cel bv een number format, een print_isbn, printdate oid kunt meegeven.
 * De vierde parameter is bedoeld om mee te kunnen geven hoe een cel uitgelijnd
 * dient te worden.
 */
function arrayToHTMLTable($arr, $headers, $dispfunc = NULL, $align = NULL,
	$escape = FALSE, $tableclass ="table table-striped" )
{

	if (!$tableclass){
		$ret = "<table>\n";
	} else {
		$ret = "<table class=\"$tableclass\">\n";
	}

	$ret .= '<tr>';
	foreach ($headers as $header) {
		$ret .= "<th>$header</th>";
	}
	$ret .= "</tr>\n";

	foreach ($arr as $row) {
		$ret .= '<tr>';
		foreach ($headers as $key => $dummy) {
			if(isset($row[$key])) {
				// als er een displayfunc is, bewerk dan de data ermee
				if(isset($dispfunc[$key])) {
					$callFunc = $dispfunc[$key];
					if (strstr($callFunc, "::") !== false) 
						$callFunc = explode('::', $callFunc);
						
					$cell = call_user_func($callFunc, $row[$key]);
				} else {
					$cell = $row[$key];
				}
				if($escape) {
					$cell = htmlspecialchars($cell);
				}
			} else {
				$cell = '&nbsp;';
			}
			$aligncell = isset($align[$key])?
				' align=\''.htmlspecialchars($align[$key]).'\'':
				'';
			$ret .= "<td$aligncell>$cell</td>";
		}
		$ret .= "</tr>\n";
	}

	$ret .= "</table>\n";
	return $ret;
}

/* Spreekt voor zich */
function printHTML($text, $warn = FALSE) {
	echo '<p class='.($warn?'waarschuwing':'none').">$text</p>\n";
}

/* Maak een link gegeven een URL en tekst. */
// LET OP: text wordt NIET gehtmlentitied
function makeRef($url, $text, $parameters = null, $extraattr = array()) {
	$elems = @parse_url($url);

	if (!$url || @!$elems) {
		// geen geldige url blijkbaar, dus geen link van maken
		return $text;
	}
	
	// check voor samenplakkende slashes
	if (@$elems['path']) {
		$temp = strlen($elems['path']);
		$elems['path'] = preg_replace(',/+,', '/', $elems['path']);
		$temp -= strlen($elems['path']);
		if ($temp) {
			user_error("URL heeft teveel slashes: $url", E_USER_NOTICE);
		}
	}

	// reconstrueer de url. Geen ondersteuning voor: user en pass
	$url  = @$elems['scheme']? "$elems[scheme]:":'';
	$url .= @$elems['host']? "//$elems[host]":'';
	$url .= @$elems['port']? ":$elems[port]":'';
	$url .= @$elems['path']? "$elems[path]":'';
	
	if (@$elems['query']) {
		$query_elems = explode('&', $elems['query']);
	} else {
		$query_elems = array();
	}

	if ($parameters) {
		foreach ($parameters as $k=>$v) {
			$query_elems[] = urlencode($k).'='.urlencode($v);
		}
	}

	$url .= $query_elems ? '?'.implode('&', $query_elems) : '';
	$url .= @$elems['fragment']? "#$elems[fragment]":'';
	// zorg voor html 4.0 correcte escaping voor attributes
	$url = htmlspecialchars($url);

	$attrs = '';
	foreach ($extraattr as $key => $value) {
		$attrs .= ' '.htmlspecialchars($key).'="'.htmlspecialchars($value).'"';
	}

	if ($text === ""){
		return $url;
	} else {
		return "<a href=\"$url\"$attrs>$text</a>";
	}
}

/*	Retourneert een dropdown-menu (SELECT in HTML) met daarin
	alle jaren die ook in de global $PERIODES zitten.
	De naam van het formelement is $formElemName
	-- Sjeik
*/
function addSelectCollegejaar($formElementName, $selectedJaar = null, $autoSubmit = false){
	global $PERIODES;
	$keyvalues = array();
	$keyvalues[""] = "";
	foreach ($PERIODES as $jaar => $perlist){
		$jaarString = "$jaar-" . ($jaar + 1);
		$keyvalues[$jaar] = $jaarString;
	}
	
	return addSelect($formElementName, $keyvalues, $selectedJaar, true, $autoSubmit);
}

/**	Retourneert een dropdown-menu (SELECT) met daarin 
	de vier periodes in een jaar. Als de variabele $useJaar
	is opgegeven, wordt gekeken van welk jaar we de periodes
	weten uit $PERIODES. Overigens wordt $useJaar op dezelfde
	manier geinterpreteerd als in $PERIODES, dus: 2004 = 2004-2005.
	Als de variabele $selectedPeriode is ingesteld (1, 2, 3 of 4) dan
	wordt die option geselecteerd in de selectbox
	-- Sjeik
*/
function addSelectPeriodes($formElementName, $useJaar = null, $selectedPeriode = null, $autoSubmit = false){
	global $PERIODES;
	
	$keyvalues = array();
	if (!$useJaar){
		$keyvalues = array("" => "", 1 => 1, 2 => 2, 3 => 3, 4 => 4);
	} else { 
		// Er is een jaar geselecteerd, uitzoeken wanneer de periodes
		// in dat jaar vallen en die als caption (value) geven
		
		$periodenummer = 1;
		while ($periodenummer <= 4){
			if (isset($PERIODES[$useJaar][$periodenummer])){
				$periodeStart = $PERIODES[$useJaar][$periodenummer];
				$periodeEinde = periodeEindDatum($useJaar, $periodenummer);
				if (!$periodeEinde) $periodeEinde = "?";
				
				$keyvalues[$periodenummer] = "$periodenummer ($periodeStart tot $periodeEinde)";
			} // else: deze periode is nog niet gedefinieerd voor het gegeven jaar
			$periodenummer++;
		}
	}

	return addSelect($formElementName, $keyvalues, $selectedPeriode, true, $autoSubmit);
}

/** 
 * Print een regel in een CSV file. Accepteert ofwel:
 * - 1 parameter: een array met de fields;
 * - n parameters: de afzonderlijke velden.
 *
 * Regelt de quoting, escaping en separation.
 */
function putCsvLine()
{
	if ( func_num_args() == 1 && is_array(func_get_arg(0)) ) {
		$args = func_get_arg(0);
	} else {
		$args = func_get_args();
	}

	$line = "";
	foreach($args as $arg) {
		$line .= "\"" .
			str_replace('"','\"',
				strtr($arg, "\r\n", "  ") )
			. "\"" . CSVSEP;
	}
	// trim off the last CSVSEP
	echo substr($line,0,-1) . "\n";
}

// reload-protectie
function addRedo($name = '') {
	global $session;
	$redo = randstr(10);
	echo addHidden('redoprotectie'.$name, $redo);
	$session->set('REDO'.$name, $redo);
}

function checkRedo($name = '') {
	global $session;

	if ($session->get('REDO'.$name) != requirePar('redoprotectie'.$name)) {
		print_error(_('Deze pagina kan niet herladen worden. Probeer het via het menu.') . (DEBUG? " ($name)":'') );
	}
	$session->set('REDO'.$name, 'invalid');
}

/**
 * \brief Check of de string lijkt op een rekeningnummer.
 * Dit kan old-style, bankrekeningnummer, of new-style, IBAN zijn.
 * We gokken dat het old-style is als het numeriek is.
 * \returns Een bool die aangeeft of het nummer het juiste format heeft.
 */
function checkRekeningnrFormat($str) {
	$iban = strtolower(str_replace(' ','',$str));
	if (is_numeric($iban)) {
		// we gokken dat het nog volgens het antieke formaat is
		// geen ws
		$str = trim($iban);
		// geen puntjes
		$str = str_replace('.','', $str);

		// te kort of te lang
		if ( strlen($str) < 5 || strlen($str) > 9 ) return false;

		// Postbanknummer, niets verder te checken
		if ( strlen($str) < 9 ) return true;

		// Bankrekeningnummer, doe de elfproef
		$nrs = str_split($str);
		$som = 0;
		for ( $i = 1; $i <= 9; $i++) {
			$som += $i * $nrs[9-$i];
		}
		return ($som % 11) == 0;
	} else {
		// We nemen eventjes aan dat het IBAN is, dus doe een IBAN-check
		// schaamteloos overgenomen van http://stackoverflow.com/a/20983340
		$Countries = array('al'=>28,'ad'=>24,'at'=>20,'az'=>28,'bh'=>22,'be'=>16,'ba'=>20,'br'=>29,'bg'=>22,'cr'=>21,'hr'=>21,'cy'=>28,'cz'=>24,'dk'=>18,'do'=>28,'ee'=>20,'fo'=>18,'fi'=>18,'fr'=>27,'ge'=>22,'de'=>22,'gi'=>23,'gr'=>27,'gl'=>18,'gt'=>28,'hu'=>28,'is'=>26,'ie'=>22,'il'=>23,'it'=>27,'jo'=>30,'kz'=>20,'kw'=>30,'lv'=>21,'lb'=>28,'li'=>21,'lt'=>20,'lu'=>20,'mk'=>19,'mt'=>31,'mr'=>27,'mu'=>30,'mc'=>27,'md'=>24,'me'=>22,'nl'=>18,'no'=>15,'pk'=>24,'ps'=>29,'pl'=>28,'pt'=>25,'qa'=>29,'ro'=>24,'sm'=>27,'sa'=>24,'rs'=>22,'sk'=>24,'si'=>19,'es'=>24,'se'=>24,'ch'=>21,'tn'=>24,'tr'=>26,'ae'=>23,'gb'=>22,'vg'=>24);
		$Chars = array('a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15,'g'=>16,'h'=>17,'i'=>18,'j'=>19,'k'=>20,'l'=>21,'m'=>22,'n'=>23,'o'=>24,'p'=>25,'q'=>26,'r'=>27,'s'=>28,'t'=>29,'u'=>30,'v'=>31,'w'=>32,'x'=>33,'y'=>34,'z'=>35);

		if(strlen($iban) == $Countries[substr($iban,0,2)]){

			$MovedChar = substr($iban, 4).substr($iban,0,4);
			$MovedCharArray = str_split($MovedChar);
			$NewString = "";

			foreach($MovedCharArray AS $key => $value){
				if(!is_numeric($MovedCharArray[$key])){
					$MovedCharArray[$key] = $Chars[$MovedCharArray[$key]];
				}
				$NewString .= $MovedCharArray[$key];
			}

			if(bcmod($NewString, '97') == 1)
			{
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
}
