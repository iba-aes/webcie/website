<?php

// $Id$

/* bevat functies die met vakken en de boekvakrelatie te maken hebben:
   - vak invoeren/bewerken
   - vakinfo
   - boeken koppelen aan vakken
   - etc
 */

require_once('bookweb/init.php');

// dit stuurt een mail aan studenten als boekcom een alterboekvak doet
function alterboekvakMailStudents($boekvaknr, $reden, $oudeEAN) {
	$studbestellingen = sqlBesteldeBoekvakken($boekvaknr);

	$oldboek = sqlBoekGegevens($oudeEAN);
	$boekvakinfo = sqlBoekVakGegevens(array($boekvaknr));
	$newboek = $boekvakinfo[$boekvaknr];

	$data = array( 'reden' => $reden,
		'vak' => $newboek['vak'],
		'oldboek' => '"' . $oldboek['titel'] . '" van ' . $oldboek['auteur'] .
			' ('.print_EAN($oldboek['EAN']).')',
		'newboek' => '"' . $newboek['titel'] . '" van ' . $newboek['auteur'] .
			' ('.print_EAN($newboek['EAN']).')',
		);

	foreach($studbestellingen as $sb) {
		bwmail($sb['lidnr'], BWMAIL_VERANDERD_BOEK_VAK, $data);
	}

	return sizeof($studbestellingen);
}

// verander het boek van een vak en stel studenten op de hoogte
function alterboekvak() {

	$boekvaknr = requirePar('boekvaknr');
	$t=sqlBoekVakGegevens(array($boekvaknr));
	$bvdata = array_shift($t);

	if(tryPar('action') == 'check') {
		$EAN = requireEAN();
		$boekinfo = sqlBoekGegevens($EAN);
		$reden = tryPar('reden');

		if(!$reden) {
			printHTML('Achtung: je hebt geen "reden" opgegeven. Als je de '
				.'studenten wilt mailen is dat wel een goed idee.', TRUE);
		}

		startForm();
		echo addHidden('action', 'go').
			addHidden('boekvaknr', $boekvaknr).
			"<tr><td>Vak:</td><td>". htmlspecialchars($bvdata['vak']) ."</td></tr>
			<tr><td>Boek:</td><td>". print_EAN_bi($bvdata['EAN']).': '
				.htmlspecialchars($bvdata['auteur'].', '
				.$bvdata['titel'].' ('.@$bvdata['druk']).')</td></tr>
			<tr><td>Nieuw boek:</td><td>'.  print_EAN_bi($EAN).': '
				.htmlspecialchars($boekinfo['auteur'].', '
				.$boekinfo['titel'].' ('.@$boekinfo['druk']).")</td></tr>
			<tr><td valign='top'>Reden:</td><td>". nl2br($reden)."</td></tr>
			<tr><td>&nbsp;</td><td>".
			addCheckBox('mailstudents', '1', TRUE).' <label for="mailstudents">Studenten mailen</label></td></tr>'.
			'<tr><td>&nbsp;</td><td>'.
			addCheckBox('maildocent', '1', TRUE).' <label for="maildocent">Docent mailen</label></td></tr>'.
			addHidden('EAN', $EAN).
			addHidden('reden', $reden);

		addRedo();
		endForm('Zeker weten?', FALSE);

		return;
	} elseif (tryPar('action') == 'go') {
		checkRedo();

		$EAN = requireEAN();
		$boekinfo = sqlBoekGegevens($EAN);
		$oudeEAN = $bvdata['EAN'];
		$reden = tryPar('reden');

		/* Verander het boek van alle studentenbestellingen */
		sqlChangeBoekVak($boekvaknr, $EAN);

		/* Alle gemailde studenten die hun boek nog niet hebben gekocht */
		$res = sqlNogNietGekochteSBs($EAN, TRUE);
		foreach($res as $row) {
			sqlOnlever($row['bestelnr']);
		}

		/* Probeer of er van de nieuwe EAN misschien alweer artikelen geleverd
		 * zijn */
		processTo4(array($EAN));

		echo "<p>Studentbestellingen gewijzigd.";

		if(tryPar('mailstudents')) {
			echo "<br>". alterboekvakMailStudents($boekvaknr, $reden, $oudeEAN).
				" studenten gemaild.";
		}
		if(tryPar('maildocent')) {
			$data = array
				('docent'     => $bvdata['docent']
				,'vak'        => $bvdata['vak']
				,'titel_oud'  => $bvdata['titel']
				,'auteur_oud' => $bvdata['auteur']
				,'druk_oud'   => $bvdata['druk']
				,'EAN_oud'    => $bvdata['EAN']
				,'titel'      => $boekinfo['titel']
				,'auteur'     => $boekinfo['auteur']
				,'druk'       => $boekinfo['druk']
				,'EAN'        => $boekinfo['EAN']
				,'reden'      => $reden
				);
			bwmail($bvdata['email'], BWMAIL_DOCENT_VERANDERD_BOEKVAK, $data);
			printHTML('Docent is gemaild.');
		}
		echo "</p>";

		return;
	}


	echo "<h2>Verander boek van een vak</h2>\n\n";
	echo "<p>Met deze functie kun je een ander boek aan een vak koppelen, als
		bijvoorbeeld een ander boek verplicht wordt dan verplicht was. Dit wijzigt
		alle studentbestellingen die dat boekvak hebben besteld, en het boekvak
		zelf. Optioneel kun je de studenten hierover mailen.</p>\n\n";

	startForm();
	echo addHidden('action', 'check').
		addHidden('boekvaknr', $boekvaknr).
		"<tr><td>Vak:</td><td>". htmlspecialchars($bvdata['vak']) ."</td></tr>
		<tr><td>Boek:</td><td>". print_EAN_bi($bvdata['EAN']).': '.htmlspecialchars($bvdata['auteur'].', '.
			$bvdata['titel'].' ('.$bvdata['druk']).')</td></tr>
		<tr><td>Nieuw boek:</td><td>'. addInput1('EAN') .
			" (ISBN, EAN, Titel of Auteur)</td></tr>
		<tr><td valign='top'>Reden:</td><td>" . addTextArea('reden') ."</td></tr>\n";

	endForm('Check...', FALSE);

}

function insertcsvvakken ()
{
	global $BWDB;

	Page::getInstance()->start();

	echo "<h2>CSV-bestand invoeren</h2>\n";
	echo "<p>Gebruik dit formulier om een csv-bestand met vakinformatie te uploaden.</p>\n";
	echo "<form action=\"/Onderwijs/Boeken/CSVVakken\" method=\"post\">\n";
	echo "<p>Upload hier je bestand:</p>\n";
	echo "<textarea name=\"csv\"></textarea>\n";
	echo "<p>Formaat: vakcode,vaknaam,docent,email,richting,bama,dictaat,begin,eind</p>\n";
	echo "<p>Formaat-formaat: richting: 'ik' of 'ic' of 'na' of 'wi' of 'gt' (met '+' ertussen voor meerdere), dictaat: 'ja' of 'nee', bama: 'bachelor' of 'master', begin+eind: YYYY-MM-DD</p>\n";
	echo "<p><input type=\"submit\" name=\"submit\" value=\"Go!\" /></p>\n";
	echo "</form>\n";

	if (!is_null(tryPar('submit')))
	{
		$input = tryPar('csv');
		$lines = explode("\r\n", $input);
		foreach ($lines as $line)
		{
			$csv = str_getcsv($line);
			if (count($csv) != 9)
			{
				echo "<p class='error'>Ongeldige regel gevonden: $line</p>\n";
				continue;
			}

			$data = array();

			$data['afkorting']	= $csv[0];
			$data['naam']		= $csv[1];
			$data['url']		= '';
			$data['docent']		= $csv[2];
			$data['email']		= $csv[3];

			$studiejaar	= array();
			$studies = explode('+', $csv[4]);
			foreach ($studies as $studie)
				$studiejaar[$studie] = array($csv[5]);

			$dictaat = strtolower($csv[6]);
			$ja = array('ja', 'yes', 'true', 'j', 'y', 't');
			$nee = array('nee', 'no', 'false', 'n', 'f');
			if (in_array($dictaat, $ja)) {
				$data['dictaat'] = "Y";
			} elseif (in_array($dictaat, $nee)) {
				$data['dictaat'] = "N";
			} else {
				echo "<p class='error'>Ik snap de waarde van het veld dictaat niet: kies '$ja[0]' of '$nee[0]', niet '$dictaat'. Ik gok dat je 'nee' bedoelt...</p>";
				$data['dictaat'] = "N";
			}

			$data['begindatum']	= $csv[7];
			$data['einddatum']	= $csv[8];

			$data['collegejaar']	= fromColJaar(substr($data['begindatum'], 5, 2), substr($data['begindatum'], 0, 4));

			$error = FALSE;

			// check email
			$data['email'] = trim($data['email']);
			if ( ! checkEmailFormat ( $data['email'] ) ) {
				print_warning ( "Het e-mailadres '". htmlspecialchars($data['email']) .
					"' heeft geen geldig formaat.");
				$error = true;
			}

			// check datums
			if ( $data['einddatum'] < $data['begindatum'] ) {
				print_warning ( "Einddatum (". htmlspecialchars($data['einddatum']) .
					") is eerder dan begindatum (" . htmlspecialchars($data['begindatum']) . ").");
				$error = true;
			}
			$dparts = explode('-', $data['begindatum']);
			if ( fromColJaar($dparts[1], $dparts[0]) != $data['collegejaar'] ) {
				print_warning ( "Begindatum (". htmlspecialchars($data['begindatum']) .
					") ligt niet in collegejaar " . htmlspecialchars($data['collegejaar']) .
					"-" . htmlspecialchars($data['collegejaar']+1) . " (wat heel raar is omdat we het collegejaar berekenen aan de hand van de begindatum).");
				$error = true;
			}
			$dparts = explode('-', $data['einddatum']);
			if ( fromColJaar($dparts[1], $dparts[0]) != $data['collegejaar'] ) {
				print_warning ( "Einddatum (". htmlspecialchars($data['einddatum']) .
					") ligt niet in collegejaar " . htmlspecialchars($data['collegejaar']) .
					"-" . htmlspecialchars($data['collegejaar']+1) . " (die wordt berekend aan de hand van de begindatum).");
				$error = true;
			}

			// check duplicaten op afkorting
			$res = $BWDB->q('TABLE SELECT * FROM vak WHERE collegejaar = %i AND afkorting = %s',
				$data['collegejaar'], $data['afkorting']);
			if (sizeof($res) > 0)
			{
				echo "<p class='error'>Waarschuwing: er bestaat al een vak met de vakcode ".$data['afkorting'].
					" voor dit collegejaar. Het vak wordt overgeslagen.";
				continue;
			}

			// geen error, dan invoeren
			if (!$error) {
					$data['vaknr'] = sqlInsVak($data, $studiejaar);
					echo "<p>Het vak <a href='/Onderwijs/Boeken/Vakinfo?vaknr=".$data['vaknr']."'>"
						.htmlspecialchars($data['naam'])."</a> is ingevoerd.</p>\r\n";
			}
		}
	}

	Page::getInstance()->end();
}

// een nieuw vak invoeren of een bestaand vak bewerken (kink)
function insertvak() {

	echo "<h2>Vak invoeren</h2>\n";

	global $STUDIENAMEN,$STUDIEJAREN,$COLJAREN,$BWDB;

	switch(tryPar('action')) {
		case 'go':
			$updvaknr		= tryPar('updvaknr');
			$data['naam']		= requirePar('naam');
			$data['afkorting']	= tryPar('afkorting');
			$data['url']		= tryPar('url');
			$data['docent']		= requirePar('docent');
			$data['email']		= requirePar('email');
			$data['begindatum']	= requirePar('begindatum');
			$data['einddatum']	= requirePar('einddatum');
			$data['collegejaar']	= requirePar('collegejaar');
			if (tryPar("dictaat", false)){
				$data['dictaat'] = "Y";
			} else {
				$data['dictaat'] = "N";
			}
			$studiejaar		= requirePar('studies');

			$error = FALSE;

			// check email
			$data['email'] = trim($data['email']);
			if ( ! checkEmailFormat ( $data['email'] ) ) {
				print_warning ( "Het e-mailadres '". htmlspecialchars($data['email']) .
					"' heeft geen geldig formaat.");
				$error = true;
			}

			// check datums
			if ( $data['einddatum'] < $data['begindatum'] ) {
				print_warning ( "Einddatum (". htmlspecialchars($data['einddatum']) .
					") is eerder dan begindatum (" . htmlspecialchars($data['begindatum']) . ").");
				$error = true;
			}
			$dparts = explode('-', $data['begindatum']);
			if ( fromColJaar($dparts[1], $dparts[0]) != $data['collegejaar'] ) {
				print_warning ( "Begindatum (". htmlspecialchars($data['begindatum']) .
					") ligt niet in collegejaar " . htmlspecialchars($data['collegejaar']) .
					"-" . htmlspecialchars($data['collegejaar']+1) . ".");
				$error = true;
			}
			$dparts = explode('-', $data['einddatum']);
			if ( fromColJaar($dparts[1], $dparts[0]) != $data['collegejaar'] ) {
				print_warning ( "Einddatum (". htmlspecialchars($data['einddatum']) .
					") ligt niet in collegejaar " . htmlspecialchars($data['collegejaar']) .
					"-" . htmlspecialchars($data['collegejaar']+1) . ".");
				$error = true;
			}

			// check duplicaten op afkorting
			if ( !$updvaknr && !empty($data['afkorting']) ) {
				$res = $BWDB->q('TABLE SELECT * FROM vak WHERE
					collegejaar = %i AND afkorting = %s',
					$data['collegejaar'], $data['afkorting']);
				if ( sizeof($res) > 0 ) {
					$row = $res[0];
					print_warning ( "Waarschuwing: er bestaat al een vak met de vakcode " .
						makeBookRef('vakinfo',$data['afkorting'],
						'vaknr='.$row['vaknr']) . " in collegejaar " .
						htmlspecialchars($data['collegejaar']) .
						"-" . htmlspecialchars($data['collegejaar']+1) .
						". Het vak wordt desalniettemin ingevoerd.");
				}
			}

			// geen error, dan invoeren
			if ( !$error ) {
				if($updvaknr) {
					sqlInsVak($data, $studiejaar, $updvaknr);
					$data['vaknr'] = $updvaknr;
				} else {
					$data['vaknr'] = sqlInsVak($data, $studiejaar);
				}
				printHTML('Het vak <b>'.htmlspecialchars($data['naam']).'</b> is ' .
					($updvaknr ? 'gewijzigd':'ingevoerd').'.');
				printHTML(
					makeBookRef('vakinfo','Vakinfo','vaknr='.$data['vaknr']) . "<br />\n".
					makeBookRef('insboekvak','Een boek aan dit vak koppelen', 'vaknr='.$data['vaknr']) ."<br />\n".
					makeBookRef('insvak','Nog een vak invoeren')
					);
				break;
			}
			$data['studies'] = $studiejaar;
			// anders: geef form weer weer.
		default:
			$verplicht = '<span class="waarschuwing">*</span>';
			printHTML('Voer hieronder de gegevens in van het vak. '.
				'Velden met '.$verplicht.' zijn verplicht.');
			simpleForm();

			if ( isset( $error ) && $updvaknr ) {
				echo addHidden('updvaknr', $updvaknr);
			} else {
				$vaknr = tryPar('vaknr');

				if($vaknr) {
					$data = sqlVakkenGegevens(array($vaknr));
					if(!$data) {
						print_error('Geen vak gevonden met vaknr '.$vaknr);
					}
					$data = $data[$vaknr];
					echo addHidden('updvaknr', $vaknr);
				}
			}

			inputText('Vaknaam',	'naam',		@$data['naam'], $verplicht);

			inputText('Afkorting', 	'afkorting',	@$data['afkorting'],
			"<a href='http://www.amazon.co.uk'
			onMouseOver='this.href =
			\"https://www.osiris.universiteitutrecht.nl/osistu_ospr/OnderwijsKiesCursus.do?event=toonCursus&cursuscode=\" + encodeURIComponent(insvak.afkorting.value) +  \"&collegejaar=\" + encodeURIComponent(insvak.collegejaar.value)'>Osiris</a>");

			inputText('Website', 	'url',		@$data['url'],
			"(W&amp;I-vakpagina's worden automatisch gelinkt)");

			inputText('Docent', 	'docent',	@$data['docent'], $verplicht);
			inputText('E-mail', 	'email',	@$data['email'], $verplicht);
			inputText('Begindatum',	'begindatum',	@$data['begindatum'],
					$verplicht.' ' . _('(jjjj-mm-dd)'));
			inputText('Einddatum',	'einddatum',	@$data['einddatum'],
					$verplicht.' ' . _('(jjjj-mm-dd)'));

			// hier staat een matrix waar je ba/ma per studie kunt aangeven
			echo '<tr><td valign="top" colspan=\"2">Ba/ma:</td><td>'.$verplicht."</td></tr>\n";

			foreach($STUDIENAMEN as $studie => $naam) {
				echo "\t<tr><td>&nbsp;&nbsp;&nbsp;$naam:</td><td>";
				foreach($STUDIEJAREN as $jaar=>$jaarlang) {
					echo addCheckBox('studies['.$studie.'][]',
						(isset($data['studies'][$studie]) ?
							in_array($jaar, $data['studies'][$studie]) : FALSE )
						,$jaar).' '.$jaarlang.' ';
				}
				echo "</td></tr>\n";
			}

			// voeg de huidige waarde van coljaar ook toe aan COLJAREN
			if(isset($data['collegejaar'])) {
				$COLJAREN[$data['collegejaar']] =
					$data['collegejaar'].'-'.($data['collegejaar']+1);
			}

			if (@isset($data["collegejaar"])){
				$selColJaar = $data["collegejaar"];
			} else {
				$selColJaar = colJaar();
				$maand = strftime("%m");
				if (5 <= $maand && $maand < COLJAARSWITCHMAAND){
					// later dan mei: volgend collegejaar selecteren, handig voor de boekcom
					$selColJaar++;
				}
			}

			echo '<tr><td>Collegejaar:</td><td>'.
				addSelect('collegejaar', $COLJAREN, $selColJaar, TRUE).
				"</td><td>$verplicht</td></tr>\n";
			echo '<tr><td>Gebruikt dictaat:</td><td>'.
				addCheckBox('dictaat', @($data['dictaat'] == "Y")).
				"</td></tr>";
			endForm('Voer in!', FALSE);
			break;
	}
}

// maak wijzigen boek/vak relatie mogelijk.
function insertboekvak() {

	echo "<h2>Boekvak invoeren</h2>\n\n";

	$EAN 	= tryPar('EAN');
	$vaknr 	= tryPar('vaknr');
	$action	= tryPar('action');
	if($EAN) {
		$EAN = requireEAN();
	}

	// geen action, geef formulier
	// afhankelijk van of EAN, vaknr of findvak zijn gegeven
	// is het formulier al ingevuld of niet
	if(!$action) {
		echo "<table>\n";
		echo addForm(STARTPAGE). addHidden('page','insboekvak');
		if(!$EAN) {
			// Inputbox voor EAN weergeven
			inputText('ISBN/Titel/Auteur', 'EAN');
		} else {
			// EAN is vast
			$boekdata = sqlBoekGegevens($EAN);
			if(!$boekdata) {
				print_error('Geen boek gevonden met ISBN '.$EAN);
			}
			echo addHidden('EAN', $EAN).
				'<tr><td valign="top">ISBN:</td>'.
				'<td>'.print_EAN_bi($EAN).': '.
					htmlspecialchars($boekdata['titel']) .' van '.
					htmlspecialchars($boekdata['auteur']);
				if ($boekdata["bestelbaar"] == "N"){
					echo "<br/>";
					print_warning("Let op! Dit boek staat in BookWeb op
					&quot;niet bestelbaar&quot;!");
				}
				echo "</td></tr>\n";
		}

		if($vaknr) {
			$vakdata = sqlVakkenGegevens(array($vaknr));
			$vakdata = $vakdata[$vaknr];
			if(!$vakdata) {
				print_error('Geen vak gevonden met vaknr '.$vaknr);
			}
			echo addHidden('vaknr', $vaknr) .
				'<tr><td>Vak:</td><td>['.$vakdata['collegejaar'].' '.
					implode(',',array_keys($vakdata['studies'])) . '] '.
					htmlspecialchars($vakdata['naam']).' / '.
					htmlspecialchars($vakdata['docent']) .
					"</td></tr>\n";
		} elseif(!tryPar('findvak')) {
			inputText('Vaknaam/docent', 'findvak');
		} else {
			$findvak = requirePar('findvak');
			$vakken = sqlFindVak($findvak);
			if(count($vakken) == 0) {
				echo "</table>";
				print_error('Geen vak gevonden voor '.$findvak);
			} elseif(count($vakken) == 1) {
				$vakdata = array_shift($vakken);
				$vaknr = $vakdata['vaknr'];
				echo addHidden('vaknr', $vaknr) .
					'<tr><td>Vak:</td><td>['.$vakdata['collegejaar'].' '.
						implode(',',array_keys($vakdata['studies'])) . '] '.
						htmlspecialchars($vakdata['naam']) . ' / '.
						htmlspecialchars($vakdata['docent']) .
						"</td></tr>\n";
			} else {
				foreach($vakken as $vakdata) {
					$vakselect[$vakdata['vaknr']] = '['.$vakdata['collegejaar'].' '.
						implode(',',array_keys($vakdata['studies'])) . '] '.
						htmlspecialchars($vakdata['naam']) . ' / '.
						htmlspecialchars($vakdata['docent']);
				}
				echo '<tr><td>Vak:</td><td>'.addSelect('vaknr', $vakselect, null, TRUE)."</td></tr>\n";
			}
		}

		if(!($vaknr && $EAN)) {
			endForm('Opzoeken',FALSE);
		} else {
			echo '<tr><td>Verplicht:</td><td>'.
				addRadioBox('verplicht', TRUE,  'j') .' <label for="verplichtj">Ja</label> '.
				addRadioBox('verplicht', FALSE, 'n') ." <label for=\"verplichtn\">Nee</label></td></tr>\n".
				addHidden('action', 'koppel');
			endForm('Koppel boek aan vak',FALSE);
		}

		return;
	}

	// action is set, koppelen maar
	if(!($EAN && $vaknr)) {
		user_error('Interne fout: EAN en/of vaknr mist bij koppelen.', E_USER_ERROR);
	}

	sqlInsBoekVak($EAN, $vaknr, (requirePar('verplicht') == 'j'));

	printHTML('Koppeling gemaakt.');
	printHTML(makeBookRef('boekinfo', 'BoekInfo', 'EAN='.$EAN).'<br>'.
			makeBookRef('vakinfo', 'VakInfo', 'vaknr='.$vaknr));

	return;
}

// wijzig een boekvak relatie (maw: wijzig verplicht of niet)
// hier zou je twee opties kunnen maken:
// - wijzigen van verplicht
// - wijzigen van het boek dat bij dit vak hoort, met de mogelijkheid
//   tot het mailen van studenten (nu: altervak)
// (kink)
function wijzigboekvak() {

	"<h2>Boekvak wijzigen</h2>\n";

	$boekvaknr = requirePar('boekvaknr');
	$bvdata = sqlBoekVakData($boekvaknr);
	if(!$bvdata) {
		print_error('Geen boekvak gevonden met nr '.$boekvaknr);
	}

	if(go()) {
		$data = array('verplicht' => requirePar('verplicht'));
		sqlUpdBoekVak($boekvaknr, $data);
		printHTML('Boekvakgegevens gewijzigd.');
		printHTML(makeBookRef('boekinfo',  'BoekInfo', 'EAN='.$bvdata['EAN']).'<br>'.
					makeBookRef('vakinfo', 'VakInfo', 'vaknr='.$bvdata['vaknr']));
		return;
	}

	$boekdata = sqlBoekGegevens($bvdata['EAN']);
	$vakdata = sqlVakkenGegevens(array($bvdata['vaknr']));
	$vakdata = $vakdata[$bvdata['vaknr']];

	printHTML('Wijzig de gegevens voor boekvak:');
	simpleForm();
	echo '<tr><td>Boek:</td><td>'.
		print_EAN_bi($boekdata['EAN']).': '.
		htmlspecialchars($boekdata['titel']).' van '.
		htmlspecialchars($boekdata['auteur'])."</td></tr>\n";
	echo '<tr><td>Vak:</td><td>['.$vakdata['collegejaar'].'] '.
		htmlspecialchars($vakdata['naam'])."</td></tr>\n";

	echo '<tr><td>Verplicht:</td><td>'.
		addRadioBox('verplicht', ($bvdata['verplicht']=='Y'), 'Y') .' <label for="verplichtY">Ja</label> '.
		addRadioBox('verplicht', ($bvdata['verplicht']=='N'), 'N') ." <label for=\"verplichtN\">Nee</label></td></tr>\n";
	echo addHidden('boekvaknr', $boekvaknr);

	endForm('opslaan',FALSE);

	return;


}

// maak het mogelijk boek/vak te ontkoppelen, checkt of een boekvaknr al
// in gebruik is, dan gaat het niet.
function delboekvak() {

	echo "<h2>Boekvak ontkoppelen</h2>\n";

	$boekvaknr = requirePar('boekvaknr');
	$bvdata = sqlBoekVakData($boekvaknr);
	if(!$bvdata) {
		print_error('Geen boekvak gevonden met nr '.$boekvaknr);
	}

	global $BWDB;
	// unset eerst evt boekvaknummers van irrelevante sb's (verlopen of al gekocht)
	$BWDB->q('UPDATE studentbestelling SET boekvaknr = NULL WHERE (vervaldatum < CURDATE() OR
		gekochtdatum IS NOT NULL) AND boekvaknr = %i', $boekvaknr);
	if($BWDB->q('VALUE SELECT count(bestelnr) FROM studentbestelling '.
					'WHERE boekvaknr = %i', $boekvaknr) > 0 ) {
		print_error('H&eacute;, dit boekvaknr is in bestelling bij studenten en kan '.
						'daarom niet verwijderd worden.');
	}

	if(go()) {
		sqlDelBoekVak($boekvaknr);
		printHTML('Boek '.print_EAN_bi($bvdata['EAN']) .' en vak '
			.$bvdata['vaknr'].' ontkoppeld.');
		return;
	}

	simpleForm();
	echo addHidden('boekvaknr',$boekvaknr);
	printHTML('Je wilt boekvaknr '.$boekvaknr
		.' verwijderen, dwz de koppeling tussen boek '
		.print_EAN_bi($bvdata['EAN']) .' en vak '.$bvdata['vaknr']
		.' loslaten. Zeker?');
	endForm('Jawel',FALSE);

}

/* Maak het mogelijk boek te verwijderen. Checkt of er geen boekvaknrs bestaan,
 * dan gaat het niet. */
function delvak() {

	echo "<h2>Vak verwijderen</h2>\n";

	$vaknr = requirePar('vaknr');
	$t=sqlVakkenGegevens(array($vaknr));
	$vakdata = array_shift($t);
	$boekvakdata = sqlGetVakBoeken($vaknr);

	if(!$vakdata) {
		print_error('Geen vak gevonden met nr '.$vaknr);
	}

	if(sizeof($boekvakdata) > 0) {
		print_error('H&eacute;, aan dit vak zijn boeken gekoppeld en kan '
			.'daarom niet verwijderd worden.');
	}

	if(go()) {
		sqlDelVak($vaknr);
		printHTML('Het vak \''.$vakdata['naam'].'\' door \''
			.$vakdata['docent'].'\' ('.$vakdata['afkorting'].') gegeven in '
			.$vakdata['collegejaar'].' is verwijderd.');
		return;
	}

	simpleForm();
	echo addHidden('vaknr',$vaknr);
	printHTML('Weet je zeker dat je het vak \''.$vakdata['naam'].'\' door \''
		.$vakdata['docent'].'\' ('.$vakdata['afkorting'].') gegeven in '
		.$vakdata['collegejaar'].' wilt verwijderen?');
	endForm('Jawel',FALSE);

}

// sjeik: retourneert of een vak een dictaat gebruikt of niet
function vakGebruiktDictaat($vaknr){
	global $BWDB;
	$dictaat = $BWDB->q("
		VALUE SELECT dictaat
		FROM vak
		WHERE vaknr=%i",
		$vaknr);

	return ($dictaat == "Y");
}

// kink: als boekinfo maar dan vakcentrale
function vakInfo()
{
	$vakzoek = tryPar('vakzoek');
	$vaknr   = tryPar('vaknr');
	echo 'Vaknaam/docent: '.
		addForm('', 'GET').
		addHidden('page', 'vakinfo').
		addInput1('vakzoek', htmlspecialchars(@$vakzoek), FALSE, 30);
	normalEndForm('opzoeken');
	if(!$vakzoek && !$vaknr) {
		return;
	}

	// zoeken, als vaknr niet gegeven is, of info opvragen, als we vaknr wel weten
	if(!$vaknr) {
		$vakken = sqlFindVak($vakzoek);
		if(count($vakken) == 0) {
			printHTML('Helaas, niets gevonden dat iets met "'.$vakzoek.'" te maken heeft...!');
			return;
		} elseif (count($vakken) > 1) {
			printHTML('Welk vak bedoel je precies?');
			echo "<table class=\"bw_datatable_medium_100\">\n";
			echo "<tr><th>Vak</th><th>Afkorting</th><th>Docent</th><th>Jaar/studie</th></tr>\n";
			foreach($vakken as $vak) {
				echo '<tr><td>'.makeBookRef('vakinfo',
						htmlspecialchars($vak['naam']),
						'vaknr='.$vak['vaknr']);

				// Vakafkorting
				echo '</td><td>';
				if ($vak["afkorting"] && $vak["collegejaar"]){
					echo makeOsirisLink($vak["afkorting"],
					$vak["collegejaar"], $vak["afkorting"]);
				} elseif ($vak["afkorting"]){
					echo htmlspecialchars($vak["afkorting"]);
				} else {
					echo "onbekend";
				}
				echo '</td><td>'.htmlspecialchars($vak['docent']).
					'</td><td>'.$vak['collegejaar'].' '.implode(',',array_keys($vak['studies'])).
					"</td></tr>\n";
			}
			echo "</table>\n\n";
			return;
		}

		$vakinfo = array_shift($vakken);
		$vaknr = $vakinfo['vaknr'];
	} else {
		$vakken = sqlVakkenGegevens(array($vaknr));
		$vakinfo = $vakken[$vaknr];
	}

	// op dit moment zijn we klaar met zoeken en staat in vakinfo de info over dit vak
	global $STUDIENAMEN;

	echo "<h2>Vakinfo</h2>\n\n<h2>".htmlspecialchars($vakinfo['naam'])."</h2>\n\n";

	echo "<table>\n".
		'<tr><td>Afkorting:</td><td>';

	echo makeBookRef("Vakinfo?vakzoek=".$vakinfo["afkorting"], $vakinfo["afkorting"]);

	if ($vakinfo["afkorting"] && $vakinfo["collegejaar"]) {
		echo " (" . makeOsirisLink($vakinfo["afkorting"], $vakinfo["collegejaar"],
		"Osiris") . ")";
	}


	echo "</td></tr>\n".
		'<tr><td>Docent:</td><td>'.htmlspecialchars($vakinfo['docent']).
			($vakinfo['email'] ?
				' (<a href="mailto:'.$vakinfo['email'].'">'.$vakinfo['email'].'</a>)':'').
			"</td></tr>\n".
		'<tr><td>Periode:</td><td>'.$vakinfo['collegejaar'].'-'.($vakinfo['collegejaar']+1).
			': '.print_date($vakinfo['begindatum']).' t/m '.print_date($vakinfo['einddatum']).
			"</td></tr>\n".
		"<tr><td colspan=\"2\">Ba/ma:</td></tr>\n";
		foreach($vakinfo['studies'] as $studie => $jaren) {
			echo "<tr><td>&nbsp;&nbsp;&nbsp;".$STUDIENAMEN[$studie].":</td><td>".
				implode(', ', $jaren) . "</td></tr>\n";
		}
		echo '<tr><td>Web:</td><td>';
		if (!$vakinfo["url"]){
			// Geen URL
			if ($vakinfo["collegejaar"] &&
					substr($vakinfo["afkorting"], 0, 4) == "INFO"){
				// Informatica-vak
				echo makeInformaticaLink(
					$vakinfo["afkorting"], $vakinfo["collegejaar"],
					"(Informatica vakpagina)");
			} elseif ($vakinfo["collegejaar"] &&
					substr($vakinfo["afkorting"], 0, 3) == "WIS"){
				// Wiskunde-vak.
				echo makeWiskundeLink(
					$vakinfo["afkorting"], $vakinfo["collegejaar"],
					"(Wiskunde vakpagina)");
			} else {
				echo "-";
			}
		} else {
			echo makeExternLink($vakinfo["url"], $vakinfo["url"]);
		}
		echo "</td></tr>\n";


		echo '<tr><td>Dictaat:</td><td>'.
			print_yn($vakinfo['dictaat']) .
			"</td></tr>\n";

		echo "</table><br>\n\n";
	if (hasAuth('boekcom')) {
		echo '('.makeBookRef('insvak', 'wijzig', 'vaknr='.$vaknr).' / '
			.makeBookRef('delvak', 'verwijder', 'vaknr='.$vaknr).")<br />\n";
		echo "<p>\n<form method=POST>\n";
		echo addHidden('page', 'docentenboekenmail');
		echo addHidden('vaknr', $vaknr);
		normalEndForm('Mail docent de boeken van dit vak');
		echo "</p>\n";
	}

	echo "<span class=\"header3\">Boeken</span><br/>\n";
	$boeken = sqlGetVakBoeken($vaknr);

	echo "<table class=\"bw_datatable_medium_100\">\n".
		"<tr><th>Verpl.</th><th>Bestelb.</th><th>ISBN</th><th>Titel</th><th>Auteur</th></tr>\n";


	foreach($boeken as $boek) {
		$statusBestelbaar = "Ja";
		if ($boek["bestelbaar"] != "Y"){
			$statusBestelbaar = "<font color=\"red\">Nee</font>";
		}
		echo '<tr>';

		// Verplichting
		echo '<td align="center" width=\"1\">';
		echo print_yn($boek['verplicht']);
		echo '</td>';

		// Bestelbaar
		echo '<td align="center" width="1">'.
			$statusBestelbaar .
			'</td>';

		echo '<td width="1">'.print_EAN_bi($boek['EAN']).
			'</td><td>'.htmlspecialchars($boek['titel']).
			'</td><td>'.htmlspecialchars($boek['auteur']).
			'</td>';
		if (hasAuth('boekcom')) {
			echo '<td width="200">(wijzig '.makeBookRef('updboekvak','verplichting',
				'boekvaknr='.$boek['boekvaknr']).'/'.
				makeBookRef('alterboekvak','boek',
				'boekvaknr='.$boek['boekvaknr']).', '.
				makeBookRef('delboekvak',
				'wis', 'boekvaknr='.$boek['boekvaknr']).")</td>";
		}
		echo "</tr>\n";
	}

	if (vakGebruiktDictaat($vaknr)){
		echo "<tr>";
		if (sizeof($boeken) > 0){
			echo "<td colspan=\"3\">&nbsp;</td>";
			echo "<td colspan=\"3\">";
			echo "<em>Bij dit vak wordt naast de hierboven genoemde boeken
			ook een dictaat gebruikt</em>";
			echo "</td>";
		} else {
			echo "<td colspan=\"6\" align=\"center\">";
			echo "<em>Bij dit vak wordt alleen een dictaat gebruikt</em>";
			echo "</td>";
		}
		echo "</tr>";
	} elseif (sizeof($boeken) == 0){
		echo "<tr><td colspan=\"6\" align=\"center\"><em>Er zijn geen
		boeken of dictaat aan dit vak gekoppeld.</em></td></tr>";
	}
	echo "</table>\n\n";

	if (hasAuth('boekcom')) {
		echo makeBookRef('insboekvak','(toevoegen)','vaknr='.$vaknr);
	}

	return;
}
