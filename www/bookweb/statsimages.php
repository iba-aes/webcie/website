<?php
/**
	LET OP!! Deze file mag nooit zomaar ge-include worden, dat is namelijk
	totaal niet nuttig. Hier worden (buiten dispatch, space, etc) plaatjes
	gegenereerd en de enige output is dus binary! Hieronder worden zelf de
	nodige includes gedaan om met de database te kunnen communiceren.
*/

global $request;

if (strpos($request->server->get("REQUEST_URI"), "/Onderwijs/Boeken/statsimages.php") !== 0) {
	user_error("De file statsimages.php kan niet gebruikt worden vanuit een vorm van dispatch!");
} else {
	require_once(realpath(dirname(__FILE__) . "/init.php"));
	if (!hasAuth("verkoper")){
		print_error("Helaas! Je bent geen boekverkoper! Volgend jaar misschien?");
	}
	require_once("classes/BookwebStatistieken.cls.php");
	require_once("classes/BookwebStatistiekenPlaatjes.cls.php");
	require_once("../../lib/jpgraph-3.5.0b1/src/jpgraph.php");
	require_once("../../lib/jpgraph-3.5.0b1/src/jpgraph_line.php");
	require_once("../../lib/jpgraph-3.5.0b1/src/jpgraph_bar.php");

	BookwebStatistiekenPlaatjes::dispatch();
}

