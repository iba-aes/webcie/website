-- $Id$

-- MySQL dump 8.21
--
-- Host: localhost    Database: boeken
---------------------------------------------------------
-- Server version	3.23.49-log

--
-- Table structure for table 'bestelling'
--

/*

Een 'bestelling' is een bestelling bij de leverancier. Het bestaat uit de
volgende elementen:
- EAN: Primary key van het boek waar het om gaat
- aantal: Aantal boeken dat in bestelling bij de leverancier is. Dit hoeft
  niet het oorsponkelijke aantal te zijn, het kan ook later aangepast zijn
  (via alterbestelling)
- datum: Datum van de oorspronkelijke bestelling (dus niet van de laatste
  aanpassing)
- levbestelnr: primary key van een bestelling
- leveranciernr: bij welke leverancier deze bestelling gedaan is
- aantalgeleverd: Aantal boeken dat reeds geleverd zijn van deze bestelling.
- offerteitemnr: op basis van welke offerte deze bestelling gedaan is: WORDT
  (NOG?) NIET GEBRUIKT

Constraints: 0 <= aantalgeleverd <= aantal

*/

CREATE TABLE bestelling (
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  aantal int(11) NOT NULL default '0',
  datum date NOT NULL default '0000-00-00',
  levbestelnr int(11) NOT NULL auto_increment,
  leveranciernr int(11) NOT NULL default '0',
  stamp_bestelling timestamp(14) NOT NULL,
  aantalgeleverd int(11) NOT NULL default '0',
  offerteitemnr int(11) default NULL,
  wie int(11) default NULL,
  PRIMARY KEY  (levbestelnr),
  KEY datum (datum),
  KEY EAN (EAN)
) TYPE=MyISAM PACK_KEYS=1;


--
-- Table structure for table 'boeken'
--

/*

In 'boeken' staan alle boeken die ooit bestelbaar zijn geweest bij ons. Uit
deze tabel mag nooit iets verwijderd worden.

- EAN: Primary key van het boek waar het om gaat. Let op: EAN's worden af en
  toe veranderd, vanwege nieuwe uitgever, nieuwe editie, whatever, dan
  dient "hetzelfde" boek dus nog een keer ingevoerd te worden.
- Titel: De officiele titel van het boek. Geen latex-codes gebruiken!
- Auteur: De auteur/auteurs van het boek. In de vorm: 
  "Kleerekoper, J. P./Kinkhorst, T./Wolffelaar, J. van/Eggen S."
- Druk: (optioneel) de druk van het boek waar het om gaat. 
- Uitgever: de uitgever van het boek.
- Cienr: als het een A-Eskwadraatartikel betreft, de verantw. cie
- Bestelbaar: indien Y ziet een lid het boek in de lijst met bestelbare boeken,
  anders kan het niet besteld worden en is het alleen aanwezig voor de koppeling
  met leveringen, tellingen, verkopen, ...
- Telbaar: indien Y ziet een verkoper het boek in de lijst met te tellen boeken,
  anders hoeft het niet geteld te worden.
*/

CREATE TABLE boeken (
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  titel tinytext NOT NULL,
  auteur tinytext NOT NULL,
  druk tinytext,
  uitgever tinytext,
  cienr int(11) default NULL,
  bestelbaar enum('Y','N') NOT NULL default 'Y',
  telbaar enum('Y','N') NOT NULL default 'Y',
  stamp_boeken timestamp(14) NOT NULL,
  PRIMARY KEY  (EAN),
  KEY auteur (auteur(10)),
  KEY titel (titel(10)),
  KEY cienr (cienr)
) TYPE=MyISAM PACK_KEYS=1 COMMENT='Moet eigenlijk artikelen heten';

--
-- Table structure for table 'boekvak'
--

/*

Een 'boekvak' is de koppeling (many-to-many) van een boek met een vak. Hiermee
kan een student een boek bestellen dat bij een bepaald vak hoort. Sterker nog,
het is verplicht evenals 'studievak' om het boek in het bestelgedeelte voor
studenten te krijgen.

- Verplicht: Is voor dit vak het boek verplicht (of optioneel/wenselijk)?
- EAN: Het boek dat gekoppeld wordt. Foreign key van 'boeken'.
- Vaknr: Het vak dat gekoppeld wordt. Foreign key van 'vakken'.
- Boekvaknr: Primary key van een 'boekvak'.

*/

CREATE TABLE boekvak (
  verplicht enum('Y','N') NOT NULL default 'Y',
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  vaknr int(11) NOT NULL default '0',
  boekvaknr int(11) NOT NULL auto_increment,
  stamp_boekvak timestamp(14) NOT NULL,
  PRIMARY KEY  (boekvaknr),
  KEY EAN (EAN)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'leverancier'
--

/*

Tabel met alle informatie over de boekenleveranciers van A-Eskwadraat.

'Email', 'naam' en 'leveranciernr' worden in Bookweb gebruikt.

Boekenmarge is een percentage van de leverprijs dat standaard bij de leverprijs
opgeteld wordt om de studentenprijs te bepalen.

De velden spreken voor zich. 'Naam' en boekenmarge is als enige verplicht.

*/

CREATE TABLE leverancier (
  naam tinytext NOT NULL,
  leveranciernr int(11) NOT NULL auto_increment,
  adres tinytext,
  email tinytext,
  telefoon tinytext,
  contactpersoon tinytext,
  boekenmarge decimal(6,4) NOT NULL default '0.0000',
  stamp_leverancier timestamp(14) NOT NULL,
  PRIMARY KEY  (leveranciernr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'levering'
--

/*

Een 'levering' is een zending met boeken van een titel (EAN) van de leverancier
aan A-Eskwadraat. Uit deze tabel wordt de voorraad boeken die bij A-Eskwadraat
aanwezig is, gehaald.

- voorraadnr: hieruit kan afgeleid worden welk boek het betreft.
- Aantal: Aantal boeken dat in deze levering zat.
- Factuur: Het factuurnummer van de levering. (optioneel)
- levbestelnr: Koppeling met de bestelling die gedaan is, waardoor deze levering
  gearriveerd is. Foreign key van 'bestelling'.
- wanneer: Datum dat de levering is gearriveerd.
- invoerdatum: datum dat de leverings is ingevoerd (vanwaar dit onderscheid??)
- wie (lidnr): wie heeft deze levering ingevoerd.

*/

CREATE TABLE levering (
  voorraadnr int(11) NOT NULL default '0',
  aantal int(11) NOT NULL default '0',
  factuur tinytext,
  levbestelnr int(11) default NULL,
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  wie int(11) default NULL,
  invoerdatum datetime NOT NULL default '0000-00-00 00:00:00',
  KEY wanneer (wanneer),
  KEY voorraadnr (voorraadnr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'nieuws'
--

/*

Nieuws dat bekend is over een bepaald boek van een bepaalde bestellign.
Bijvoorbeeld dat het boek tijdelijk niet leverbaar is, of drie weken te laat
zal komen.

- Nieuws: De feitelijke informatie (ook wel: het boekennieuws) over de
  bestelling.
- EAN: Het EAN van het boek waarover nieuws is. Foreign key van 'boeken'.

*/

CREATE TABLE nieuws (
  nieuwsid int(10) unsigned NOT NULL auto_increment,
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  nieuws text NOT NULL,
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  wie int(11) default NULL,
  PRIMARY KEY  (nieuwsid),
  KEY EAN (EAN)
) TYPE=MyISAM;

--
-- Table structure for table 'offerte'
--

/*

Een offerte is een overzicht met de prijs van een boek
voordat de bestelling gedaan wordt door de BoekCom. Een entry in deze tabel is
meta-informatie van een offerte. Zie ook 'offerteitem'.

- Offertenr: Primary key van 'offerte'
- Leveranciernr: De leverancier die deze offerte offreert. Foreign key van
  'leverancier'.
- Leveranciersoffertenr: Offertenummer dat de leverancier hanteert,
  puur ter referentie, wordt in de code niks mee gedaan.
- Offertedatum: Datum dat deze offerte ingevoerd is.
- Verloopdatum: Datum vanaf wanneer deze offerte niet meer geldig is.

*/

CREATE TABLE offerte (
  offertenr int(11) NOT NULL auto_increment,
  leveranciernr int(11) NOT NULL default '0',
  leveranciersoffertenr char(11) NOT NULL default '',
  offertedatum date NOT NULL default '0000-00-00',
  verloopdatum date NOT NULL default '0000-00-00',
  wanneer datetime default NULL,
  wie int(11) default NULL,
  PRIMARY KEY  (offertenr)
) TYPE=MyISAM;

--
-- Table structure for table 'offerteitem'
--

/*

Een offerte voor een bepaald EAN. Is een onderdeel van 'offerte'. Een volledige offerte bestaat uit
een 'offerte' met een of meerdere 'offerteitem's.

- Offerteitemnr: Primary key van deze tabel.
- Offertenr: 'offerte' waar dit item bij hoort.
- EAN: Boek waarover de offerte wordt uitgebracht. Foreign key van 'boeken'.
- Levprijs: De stuksprijs die de leverancier voor dit EAN offreert.

NB: de prijs die een student betaalt is dus in de regel de levprijs
plus de boekenbuffer. In de studenteninterface wordt dus steeds de buffer
opgeteld bij de levprijs voor de weergave.

*/

CREATE TABLE offerteitem (
  offerteitemnr int(11) NOT NULL auto_increment,
  offertenr int(11) NOT NULL default '0',
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  levprijs decimal(4,2) NOT NULL default '0.00',
  PRIMARY KEY  (offerteitemnr),
  KEY offertenr (offertenr),
  KEY EAN (EAN)
) TYPE=MyISAM;

--
-- Table structure for table 'papiermolen'
--

/*
  Deze tabel is voor de tweedehands boekenmarkt PapierMolen.
  Zie aldaar.
*/

--
-- Table structure for table 'retour'
--

/*

De boeken die terug zijn gestuurd naar de leverancier. Dit gaat per levering
van een boek.

- Voorraadnr: te terug te sturen boeken komen uit deze voorraad (geeft
  aan welk boek het is en voor welke prijs het geleverd was.
- Aantal: Hoeveelheid die wordt teruggestuurd.
- Wanneer: Datum en tijd van invoer van deze retourzending.
- Waarde: de totale waarde van deze retourzending (waarvoor nodig??)
- Factuurnr: Factuurnummer van leverancier. (optioneel)

*/

CREATE TABLE retour (
  voorraadnr int(11) NOT NULL default '0',
  aantal int(11) NOT NULL default '0',
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  waarde decimal(13,2) default NULL,
  stamp_retour timestamp(14) NOT NULL,
  factuurnr tinytext,
  wie int(11) default NULL,
  KEY wanneer (wanneer),
  KEY voorraadnr (voorraadnr)
) TYPE=MyISAM PACK_KEYS=1;


--
-- Table structure for table 'studentbestelling'
--

/* 

Iedere bestelling van een student wordt hier bewaard.
* Wanneer een bestelling geannuleerd wordt (bijv door de student), blijft de
  bestelling bewaard.  'Vervaldatum' wordt dan op de vervaldag gezet en
  'vervalreden' krijgt uitleg (bijv "Geannuleerd door student").
* Wanneer een boek toegewezen kan worden aan de student, wordt de student
  gemaild ('gemailddatum' wordt ingevuld), wordt 'vervaldatum' twee weken later
  gezet en 'vervalreden' op "Verlopen".
* Wanneer een boek gekocht wordt, moeten vervaldatum en -reden genegeerd
  worden
* Vervaldatum is de dag dat een bestelling vervalt (verlopen, geannuleerd,
  ...), dus op de vervaldatum kun je een boek NIET meer kopen. Vervaldatum &
  -reden dienen genegeerd te worden als gekochtdatum not null is.
* Een bestelling kan ook een hogere prioriteit krijgen waardoor deze student
  het eerste het boek toegewezen krijgt, wanneer er boeken geleverd worden. Bij
  een lagere prioriteit krijgt de student het boek uiteraard als laatste.

Kortom:
  gekochtdatum gemailddatum vervaldatum (en -reden)
  null         null         null                     -> niet geleverd
  null         null         not null                 -> geannuleerd (afbesteld)
  null         not null     null                     -> inconsistentie
  null         not null     > NOW()                  -> geleverd
  null         not null     <= NOW()                 -> vervallen
  not null     not null     NEGEER                   -> gekocht
  not null     null         NEGEER                   -> inconsistentie

  gekochtdatum en gemailddatum zijn indien niet null altijd <= NOW()

- Gemailddatum: Datum waarop de student geinformeerd is dat de bestelling
  opgehaald kan worden.
- Gekochtdatum: Datum waarop het boek gekocht is,
- Lidnr: De student die het boek besteld heeft. Foreign key van WHOSWHO.leden
- Boekvaknr: Vak waarvoor dit boek besteld is. Hiermee is het EAN ook te
  achterhalen. Foreign key van boekvak
- Bestelnr: Primary key van deze tabel
- Bestelddatum: Datum waarop het boek door de student besteld is
- Prioriteit: Prioriteit van de bestelling. (definitie in contstants.php, def. 0)
- EAN: EAN van het boek dat besteld is. Beter dan 'boekvaknr' omdat sommige
  boeken niet in de tabel 'boekvak' voorkomen. Foreign key van 'boeken'
- Vervaldatum: Datum waarop de bestelling vervallen is. De reden van verval
  wordt in 'vervalreden' opgegeven.
- Vervalreden: De reden dat de bestelling vervallen is. (bijv "Verlopen",
  "Geannuleerd door student")
- Offerteitemnr: Offerte(item) waar deze bestelling aan gekoppeld is. Hiermee
  is de prijs te achterhalen. Foreign key van 'offerteitem'

*/

CREATE TABLE studentbestelling (
  gemailddatum date default NULL,
  gekochtdatum date default NULL,
  lidnr int(11) NOT NULL default '0',
  boekvaknr int(10) unsigned default NULL,
  bestelnr int(11) NOT NULL auto_increment,
  bestelddatum date NOT NULL default '0000-00-00',
  prioriteit tinyint(4) NOT NULL default '0',
  stamp_studentbestelling timestamp(14) NOT NULL,
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  vervaldatum date default NULL,
  vervalreden tinytext,
  offerteitemnr int(11) default NULL,
  PRIMARY KEY  (bestelnr),
  KEY lidnr (lidnr),
  KEY boekvaknr (boekvaknr),
  KEY EAN (EAN,prioriteit,bestelddatum)
) TYPE=MyISAM PACK_KEYS=1;



--
-- Table structure for table 'studievak'
--

/*

Een 'studievak' is de koppeling (many-to-many) van een studie met een vak.
Hiermee kan een student een boek bestellen dat bij een bepaalde studie en vak
hoort.

- Vaknr: Het vak dat gekoppeld wordt. Foreign key van 'vakken'.
- Studie: De studie die gekoppeld wordt. Enum.
- Jaar: het jaar van de studie dat dit betreft.

*/

CREATE TABLE studievak (
  vaknr int(11) NOT NULL default '0',
  studie enum('ic','ik','na','wi') NOT NULL default 'ic',
  jaar enum('1','2','3','master') NOT NULL default '1',
  stamp_studievak timestamp(14) NOT NULL,
  PRIMARY KEY  (vaknr,studie,jaar)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'telling'
--

/* 

Een telling, bestaande uit meerdere tellingitems. 
- tellingnr: de primary key
- wanneer: wanneer deze telling uitgevoerd is
- lidnr: de persoon die de boeken geteld heeft. Als dit veld null is, betekent
  dat, dat er nog niet geteld is. Als je namelijk kiest om een voorraadlijst
  uit te printen van bookweb, wordt er een telling en een aantal tellingitems
  in de db gezet, met lidnr op NULL. Die telling kun je dan in bookweb
  opvragen, corrigeren, en dan opsturen. Op dat moment wordt dan de telling
  daadwerkelijk ingevoerd, en lidnr ingevuld.

*/

CREATE TABLE telling (
  tellingnr int(11) NOT NULL auto_increment,
  telling_start datetime default NULL,
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  lidnr int(11) default NULL,
  opmerkingen text,
  PRIMARY KEY  (tellingnr)
) TYPE=MyISAM;

--
-- Table structure for table 'tellingitem'
--

/*

Tellingitems is waar een telling uit bestaat, per EAN een aantal.

Alle velden spreken voor zich, tellingnr is de foreign key bij telling

*/

CREATE TABLE tellingitem (
  tellingnr int(11) NOT NULL default '0',
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  aantal_voorraad int(11) NOT NULL default '0',
  aantal_buitenvk int(11) NOT NULL default '0',
  PRIMARY KEY  (tellingnr,EAN)
) TYPE=MyISAM;

--
-- Table structure for table 'terugkoop'
--

/*

Wanneer een boek teruggekocht wordt van een student, wordt hier een entry
aangemaakt.

- Wanneer: De datum en tijd dat het boek teruggekocht is. 
- Voorraadnr: welk voorraaditem wordt teruggekocht? M.a.w. welke EAN/prijs combi.
- Lidnr: Het lid van wie het boek teruggekocht wordt. Foreign key van
  'WHOSWHO_DB.leden'.
- Aantal: Het aantal exemplaren dat teruggekocht is. Standaard is dit 1.
- Studprijs: de prijs waarvoor dit boek teruggekocht (dus ook ooit verkocht!) is
- Verkoopnr: Nummer van de verkooptransactie waarmee dit boek ooit verkocht is.
  Foreign key van 'verkoop'.

*/

CREATE TABLE terugkoop (
  terugkoopnr int(11) NOT NULL auto_increment,
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  voorraadnr int(11) NOT NULL default '0',
  stamp_terugkoop timestamp(14) NOT NULL,
  lidnr int(11) default NULL,
  aantal int(11) NOT NULL default '1',
  verkoopnr int(11) NOT NULL default '0',
  studprijs decimal(13,2) NOT NULL default '0.00',
  wie int(11) default NULL,
  PRIMARY KEY  (terugkoopnr),
  KEY wanneer (wanneer),
  KEY voorraadnr (voorraadnr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'vak'
--

/*

Informatie over een bepaald vak.

- Naam: Naam vak het vak
- Afkorting: Afkorting van het vak (liefst Osiris-afkorting gebruiken).
  (optioneel)
- Collegejaar: Collegejaar waarin het vak gegeven wordt (bijv 2005 voor '05/'06)
- Docent: Naam van de docent. (optioneel)
- Email: E-mailadres van de docent. (optioneel)
- Begindatum: Eerste dag dat het vak gegeven wordt (bijv: 2003-09-01)
- Einddatum: Laatste dag dat het vak gegeven wordt (bijv: 2004-01-30)
- URL: Website van het vak. (optioneel)
- Vaknr: Primary key van 'vak'.

*/

CREATE TABLE vak (
  naam tinytext NOT NULL,
  afkorting varchar(8) default NULL,
  collegejaar year(4) NOT NULL default '0000',
  docent tinytext,
  email tinytext,
  beginweek int(11) NOT NULL default '0',
  eindweek int(11) NOT NULL default '0',
  begindatum date NOT NULL default '0000-00-00',
  einddatum date NOT NULL default '0000-00-00',
  url tinytext,
  vaknr int(11) NOT NULL auto_increment,
  stamp_vak timestamp(14) NOT NULL,
  PRIMARY KEY  (vaknr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'verkoop'
--

/*

Iedere verkooptransactie wordt in deze tabel bewaard.

- Wanneer: De datum en tijd waarop verkocht is.
- Voorraadnr: de voorraad waar dit boek toe hoort, maw EAN/prijs/leverancier.
- Lidnr: De persoon aan wie verkocht is. Foreign key van WHOSWHO.leden
- Studprijs: de prijs waarvoor dit boek verkocht is
- Verkoopnr: Primary key van deze tabel
- Aantal: Aantal verkochte items.

*/

CREATE TABLE verkoop (
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  voorraadnr int(11) NOT NULL default '0',
  lidnr int(11) default NULL,
  aantal int(11) NOT NULL default '1',
  studprijs decimal(13,2) NOT NULL default '0.00',
  verkoopnr int(11) NOT NULL auto_increment,
  wie int(11) default NULL,
  PRIMARY KEY  (verkoopnr),
  KEY lidnr (lidnr),
  KEY wanneer (wanneer),
  KEY voorraadnr (voorraadnr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'verkoopgeld'
--

/*

Iedere dag dat er een boekverkoop gedraaid wordt, wordt hier een entity
gemaakt met daarin de kasstanden/financien voor en na de boekverkoop. De
termen die eindigen op -in zijn de kasstanden bij (eerste) opening van de
boekverkoop en -uit zijn de eindstanden na de (laatste) sluiting van de
verkoop.
Helaas is de primary key 'verkoopdagnr' een integer. Misschien een idee om
hier een datum van te maken??

- Verkoper: Lidnr van de boekverkoper. Foreign key van WHOSWHO.leden
- Pin: Totaalbedrag dat via het pinapparaat is binnengekomen tijdens de
  verkoop.
- (Facturen: Totaalbedrag aan facturen dat binnen is gekomen.) => DEPRICATED
- Debiteuren: Alle debiteuren. Dit zijn bijvoorbeeld verkopen aan medewerkers
  die via hun instituut het boek kopen en dus nog moeten betalen.
  Wanneer dit bedrag negatief is, betekent dit dat een debiteur betaald heeft.
- Crediteuren: Alle crediteuren. Dit zijn bijvoorbeeld mensen die die een boek
  terug hebben laten kopen en een bewijsje hebben gehad van 'ik krijg nog
  geld'.
  Wanneer dit bedrag negatief is, betekent dit dat een crediteur betaald is.
- cash: Totaal ingekomen contante financien. Kastellingen, legingen e.d.
  dienen buiten bookweb om te gebeuren, is namelijk niet bijster interessant,
  en vooral weegt de complexere code niet echt op tegen het voordeel van niet
  met een rekenmachine en kasboekje een kas te controleren. En het is ook veel
  duidelijker dat het essentieel is dat een kasboekje klopt als het een echt
  fysiek kasboekje is, dit om herhaling incidenten uit het verleden te
  voorkomen.
- Kasverschil: Totaal verschil tussen ingekomen financien en alle mutaties. In
  feite is dit altijd opnieuw uit te rekenen
- Verkoopdagnr: Primary key van deze tabel
- mutaties: Alle mutaties die plaatsvinden bij de boekverkoop, maar niet als
  verkoop/terugkoop in bookweb zijn geregistreerd. Op termijn te verdwijnen,
  maar dan moeten wel eerst alles a-eskwadraatgoodies goed verkoopbaar zijn
- Sluitstamp: De timestamp van de laatste boekverkoopsluiting die dag.
- Openstamp: De timestamp van de eerste boekverkoopopening die dag.

*/

CREATE TABLE verkoopgeld (
  verkoopdag date NOT NULL default '0000-00-00',
  volgnr tinyint(3) unsigned NOT NULL auto_increment,
  verkoper int(11) NOT NULL default '0',
  pin decimal(13,2) default NULL,
  facturen decimal(13,2) default NULL,
  debiteuren decimal(13,2) default NULL,
  crediteuren decimal(13,2) default NULL,
  cash decimal(13,2) default NULL,
  donaties decimal(13,2) default NULL,
  kasverschil decimal(13,2) default NULL,
  mutaties decimal(13,2) default NULL,
  sluitstamp datetime default NULL,
  openstamp datetime default NULL,
  kv_reden text,
  geldigtot datetime default NULL,
  PRIMARY KEY  (verkoopdag,volgnr)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'verplaatsing'
--

/*

Wanneer een of meerdere boeken (van hetzelfde EAN) verplaatst worden van
voorraad naar buitenvk of andersom (uitleg => 'levering') wordt dat in deze
tabel geregistreerd. Tevens kan er een reden worden opgegeven. Dit moet
uiteraard dan ook in de tabel 'levering' worden aangepast.


- Wanneer: Datum en tijd van de verplaatsing
- Voorraadnr: EAN/prijs/leverancier die verplaatst wordt.
- Van: Locatie waar het boek vandaan komt (zie 'levering')
- Naar: Locatie waar het boek naartoe gaat (zie 'levering')
- Aantal: Aantal boeken van dit EAN dat verplaatst wordt
- Reden: Verklaring van de verplaatsing

*/

CREATE TABLE verplaatsing (
  wanneer datetime NOT NULL default '0000-00-00 00:00:00',
  voorraadnr int(11) NOT NULL default '0',
  van enum('voorraad','buitenvk') NOT NULL default 'buitenvk',
  naar enum('voorraad','buitenvk') NOT NULL default 'buitenvk',
  aantal int(11) NOT NULL default '0',
  stamp_verplaatsing timestamp(14) NOT NULL,
  reden tinytext,
  wie int(11) default NULL
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table 'voorraad'
--

/*

Voor iedere EAN wordt (gekoppeld met leveranciersprijs en leverancier)
bijgehouden hoeveel er op de verschillende voorraadlocaties (voorraad,
buitenverkoop) zijn.
Dit is dus een caching tabel van levering - verkoop + terugkoop.

- voorraadnr: Unieke (primary) key die aangeeft om welke
  (EAN,levprijs,leverancier) het gaat
- EAN: Geeft aan om welk artikel het gaat
- levprijs: De stuksprijs die de leverancier voor dit EAN vraagt
- adviesprijs: De stuksprijs die de boekencommissaris voor dit boek wil vragen
  voor dit EAN vraagt
- voorraad: Het aantal van dit EAN dat momenteel op voorraad ligt
- buitenvk: Het aantal van dit EAN dat momenteel buiten verkoop ligt
- leveranciernr: De leverancier van deze EAN voor deze prijs

*/

CREATE TABLE voorraad (
  voorraadnr int(11) NOT NULL auto_increment,
  EAN bigint(13) unsigned zerofill NOT NULL default '0000000000000',
  levprijs decimal(13,2) default NULL,
  adviesprijs decimal(13,2) default NULL,
  voorraad int(11) NOT NULL default '0',
  buitenvk int(11) NOT NULL default '0',
  leveranciernr int(11) NOT NULL default '0',
  PRIMARY KEY  (voorraadnr),
  UNIQUE KEY Wolfje (EAN,levprijs,leveranciernr)
) TYPE=MyISAM PACK_KEYS=1;

