<?php
/**
	Deze klasse bevat functies voor het weergeven van geschiedenis van
	BookWeb. Hij is in staat om een overzicht te tonen van boekverkopen in
	het verleden, maar ook om informatie te tonen over 1 specifieke
	boekverkoop
*/
class Geschiedenis {

	static function printGeschiedenis(){
		$maxResultaten = tryPar("maxResultaten", 100);
		$datumVanaf = tryPar("datumVanaf", null);
		$datumTot = tryPar("datumTot", null);

		// Form voor parameters van printLijst
		echo addForm("", "GET");
		echo addHidden("page", "geschiedenis");
		echo "<br/><strong>Selectiecriteria:</strong>\n";
		echo "<table width=\"50%\">\n";
		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			Maximaal aantal resultaten:";
		echo "		</td>\n";
		echo "		<td>";
		echo addInput1("maxResultaten", $maxResultaten);
		echo "</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			Datum vanaf: ";
		echo "		</td>\n";
		echo "		<td>";
		echo addInput1("datumVanaf", $datumVanaf);
		echo "</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			Datum tot: ";
		echo "		</td>\n";
		echo "		<td>";
		echo addInput1("datumTot", $datumTot);
		echo "</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td>&nbsp;</td>\n";
		echo "		<td>";
		echo "<input type=\"submit\" value=\"Vernieuwen\">";
		echo "</td>\n";
		echo "	</tr>\n";

		echo "</table>\n";
		echo "</form>\n";

		echo "<br/><br/>\n";
		Geschiedenis::printLijst($datumVanaf, $datumTot, $maxResultaten);
	}

	/**
		STATIC
		Print een lijst met informatie over alle boekverkopen in het
		verleden, met eventueel een selectie op datum (niet noodzakelijk)
	*/
	static function printLijst($datumStart = null, $datumEind = null, $maxAantalVerkopen = 100){
		global $BWDB;


		if ($datumStart == null){
			$datumStart = "0000-00-00";
		} else {
			$datumStart = parseDate($datumStart);
			if (!$datumStart){
				BookwebBase::addError(new BookwebError("De opgegeven
				parameter datumStart is ongeldig"));
				BookwebBase::printErrors();
				$datumStart = "0000-00-00";
			}
		}

		if ($datumEind == null){
			$datumEind = "NOW()";
		} else {
			$datumEind = parseDate($datumEind);
			if (!$datumEind){
				BookwebBase::addError(new BookwebError("De opgegeven
				parameter datumEind is ongeldig"));
				BookwebBase::printErrors();
				$datumEind = "NOW()";
			}
		}

		$verkoopgelden = $BWDB->q("
			TABLE SELECT *
			FROM verkoopgeld
			WHERE 	openstamp >= %s
			AND		sluitstamp <= %s
			ORDER BY verkoopdag DESC, volgnr DESC
			LIMIT %i",
			$datumStart, $datumEind, $maxAantalVerkopen);

		echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
		echo "	<tr style=\"text-align: left\">\n";
		echo "		<th>Verkoopdag</th>\n";
		echo "		<th>Open</th>\n";
		echo "		<th>Dicht</th>\n";
		echo "		<th>Totaal verkocht*</th>\n";
		echo "		<th>Kasverschil</th>\n";
		echo "	</tr>\n";
		$lastVerkoopdag = "";
		foreach ($verkoopgelden as $verkoopgeld){
			$verkoopdag = $verkoopgeld["verkoopdag"];
			$open = $verkoopgeld["openstamp"];
			$dicht = $verkoopgeld["sluitstamp"];
			$kasverschil = $verkoopgeld["kasverschil"];
			$geldigtot = $verkoopgeld["geldigtot"];

			$totaalverkocht =
				$verkoopgeld["pin"] + $verkoopgeld["pinII"] +
				$verkoopgeld["pin1"] + $verkoopgeld["pin2"] +
				$verkoopgeld["chip"] + $verkoopgeld["cash"] +
				$verkoopgeld["debiteuren"];

			if (!$dicht){
				$dicht = "nog geopend";
			}

			if ($geldigtot == null){
				echo "	<tr>\n";
			} else {
				// ongeldige rij
				echo "	<tr class=\"doorgestreept\" style=\"color: lightgray;\">\n";
			}

			if ($lastVerkoopdag == $verkoopdag){
				// Weglaten voor de duidelijkheid
				echo "		<td></td>\n";
			} else {
				echo "		<td>";
				echo "<a href=\"?page=toonverkopen&datum=$verkoopdag\" target=\"_self\">";
				echo "$verkoopdag";
				echo "</a>";
				echo "		</td>\n";
			}

			echo " 		<td>$open</td>\n";

			echo "		<td>$dicht</td>\n";
			echo "		<td>";
			echo BookwebBase::bedrag($totaalverkocht);
			echo "</td>\n";
			echo "		<td>";
			if ($kasverschil != 0){
				echo "<font color=\"red\">";
				echo BookwebBase::bedrag($kasverschil);
				echo "</font>";
			} else {
				echo BookwebBase::bedrag($kasverschil);
			}

			echo "</td>\n";

			echo "	</tr>\n";

			$lastVerkoopdag = $verkoopdag;
		}
		echo "</table><br/>\n";
		echo "<strong>Verklaring tabel:</strong><br/>\n";
		echo "Totaal verkocht: som van pin, chip, cash en debiteuren<br/>";

	}
}

