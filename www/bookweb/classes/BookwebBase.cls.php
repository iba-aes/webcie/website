<?php
/**
	Basisklasse voor (bijna) alle financiele klassen
*/

class BookwebBase
{
	static function addMessage($message)
	{
		$messages = BookwebBase::getMessages();

		if ($message instanceof BookwebMessage ||
			$message instanceof BookwebError ||
			$message instanceof BookwebWarning) {

			$messages[] = $message;
			return true;
		}
		return false;
	}

	static function hasNewMessages()
	{
		$messages = BookwebBase::getMessages();

		foreach ($messages as $message) {
			if (!$message->isProcessed()) {
				return true;
			}
		}
		return false;
	}

	static function hasNewErrors()
	{
		$messages = BookwebBase::getMessages();

		foreach ($messages as $message) {
			if ($message instanceof BookwebError) {
				if (!$message->isProcessed()) {
					return true;
				}
			}
		}
		return false;
	}

	static function addError($error)
	{
		BookwebBase::addMessage($error);
		return true;
	}

	static function printErrors($printOnlyNewErrors = true, $printBorder= true)
	{
		BookwebBase::printMessages($printOnlyNewErrors, $printBorder);
	}

	static function printMessagesAndErrors($printOnlyNewMessages = true, $printBorder = true)
	{
		BookwebBase::printMessages($printOnlyNewMessages, $printBorder);
	}

	static function printMessages($printOnlyNewMessages = true, $printBorder =	true)
	{
		$messages = BookwebBase::getMessages($printOnlyNewMessages);
		$errors = array();
		$warnings = array();
		$normalMessages = array();

		foreach ($messages as $message)
		{
			if (!$printOnlyNewMessages || !$message->isProcessed()) {
				$message->setProcessed();
				if ($message instanceof BookwebError) {
					$errors[] = $message;
				} elseif ($message instanceof BookwebWarning) {
					$warnings[] = $message;
				} else {
					$normalMessages[] = $message;
				}
			}
		}

		if (sizeof($errors) > 0) {
			if ($printBorder) {
				echo "<div style=\"border: 2px solid red\">\n";
			}

			$counter = 0;
			foreach ($errors as $error) {
				if ($counter != 0) {
					echo "<br/>\n";
				}
				echo "<span class=\"waarschuwing\">\n";
				echo $error->getMessage();
				echo "</span>\n";
				$counter++;
			}

			if ($printBorder) {
				echo "</div>\n";
			}
		}

		if (sizeof($warnings) > 0) {
			if ($printBorder) {
				echo "<div style=\"border: 2px solid orange\">\n";
			}

			$counter = 0;
			foreach ($warnings as $warning) {
				if ($counter != 0) {
					echo "<br/>\n";
				}
				echo "<span class=\"waarschuwing\">\n";
				echo $warning->getMessage();
				echo "</span>\n";
				$counter++;
			}

			if ($printBorder) {
				echo "</div>\n";
			}
		}

		if (sizeof($normalMessages) > 0) {
			if ($printBorder) {
				echo "<div style=\"border: 2px solid green\">\n";
			}

			$counter = 0;
			foreach ($normalMessages as $message) {
				if ($counter != 0) echo "<br/>\n";
				echo $message->getMessage();
				$counter++;
			}

			if ($printBorder) {
				echo "</div>\n";
			}
		}
	}

	static function getMessages()
	{
		static $messages;

		if (!isset($messages)) {
			$messages = array();
		}

		return $messages;
	}

	/** Laat het bedrag mooi opgemaakt zien (met een eurotekentje ervoor)
	*/
	static function bedrag($bedragstring)
	{
		//return "&euro; " . money_format('%i',$bedragstring);
		return Money::addPrice($bedragstring);
	}

	/*
	Maakt een url als:
	http://www.a-eskwadraat.nl/Onderwijs/Boeken/?page=bla&bla=bla&etc=etc

	Op basis van een gegeven array (key = variabelenaam, value =
	variabelewaarde)
	*/
	static function makeUrl($array_info)
	{
		$res = STARTPAGE;

		if (sizeof($array_info) > 0) {
			$res .= "?";

			foreach ($array_info as $varname=>$varvalue) {
				$varname = htmlspecialchars($varname);
				$varvalue = htmlspecialchars($varvalue);
				$res .= "$varname=$varvalue&";
			}
		}

		return $res;
	}
}

class BookwebError   extends BookwebMessage {}
class BookwebWarning extends BookwebMessage {}

class BookwebMessage
{
	var $_message = "";
	var $_processed = false;

	function __construct($message){
		$this->_message = $message;
	}

	function setMessage($message){
		$this->_message = $message;
	}

	function getMessage(){
		return $this->_message;
	}

	function setProcessed($processed = true){
		$this->_processed = $processed;
	}

	function isProcessed(){
		return $this->_processed;
	}
}

