<?php

class VoorraadSet implements Iterator, Countable {
	private $_voorraden;

	private function __construct(){
		$this->_voorraden = array();
	}

	/**
		Zoekt alle voorraden van het opgegeven EAN. By default worden alleen
		voorraden geretourneerd waar nog iets van op voorraad is. Voorraden van
		jaren terug zijn dus irrelevant.
	**/
	public static function createByEAN($EAN, $enkel_met_voorraad = true){
		$obj = new VoorraadSet();
		$obj->_voorraden = Voorraad::searchByEAN($EAN, $enkel_met_voorraad);
		return $obj;
	}

	/**
		Zoekt alle voorraden op basis van de opgegeven voorraadlocatie
	**/
	public static function createByVoorraadLocatie($locatie = "voorraad"){
		global $BWDB;
		Voorraad::checkVoorraadLocatie($locatie);

		$set = new VoorraadSet();
		$qres = $BWDB->q("COLUMN SELECT voorraadnr FROM voorraad WHERE $locatie > 0");
		foreach($qres as $voorraadnr){
			$set->_voorraden[] = new Voorraad($voorraadnr);
		}

	 	return $set;
	}

	public function size(){ return sizeof($this->_voorraden); }

	/**
		Retourneert de totaal beschikbare voorraad op de opgegeven
		voorraadlocatie. Dit is dus een sommatie van alle voorraden in deze
		VoorraadSet.
	**/
	public function getVoorraad($locatie = "voorraad"){
		$sum = 0;
		foreach ($this->_voorraden as $voorraad){
			$sum += $voorraad->getVoorraad($locatie);
		}
		return $sum;
	}




	/** Iterator-zaken **/
	public function rewind(){ reset($this->_voorraden); }
	public function current(){ return current($this->_voorraden); }
	public function key(){ return key($this->_voorraden); }
	public function next(){ return next($this->_voorraden); }
	public function valid(){ return $this->current() !== false; }

	/**
	 * Count elements of an object
	 * @link https://php.net/manual/en/countable.count.php
	 * @return int The custom count as an integer.
	 * </p>
	 * <p>
	 * The return value is cast to an integer.
	 * @since 5.1.0
	 */
	public function count()
	{
		return $this->size();
	}
}
