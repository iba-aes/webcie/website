<?php
/**
	Klasse waarin alles gebeurt m.b.t. het genereren van plaatjes bij
	statistieken uit bookweb. De parent class bevat alle methodes die de
	data op een zinnige manie ruit de database kunnen halen.
*/
class BookwebStatistiekenPlaatjes extends BookwebStatistieken {

	/**
		Zoekt a.h.v. parameters uit welk plaatje er precies moet komen
	*/
	static function dispatch(){
		$imagetype = tryPar("imagetype");
		$boekjaren = tryPar("boekjaren", null);
		$leveranciers = tryPar("leveranciers", null);
		$eans = tryPar("eans", null);

		if ($boekjaren != null){
			$boekjaren = explode(",", $boekjaren);
		} else {
			$boekjaren = array(date("Y"));
		}

		if ($leveranciers != null){
			// Een array maken
			$leveranciers = explode(",", $leveranciers);
		} else {
			$leveranciers = array(BOEKLEVNR);
		}

		if ($eans != null){
			$eans = explode(",", $eans);
		} else {
			$eans = array();
		}

		if ($imagetype == "boekjaarvergelijkingcuml"){
			BookwebStatistiekenPlaatjes::imgBoekjaarvergelijking(
				$boekjaren,null,$leveranciers, true, 1);
		} elseif ($imagetype == "boekjaarvergelijking"){
			BookwebStatistiekenPlaatjes::imgBoekjaarvergelijking(
				$boekjaren,null,$leveranciers, false, 1);
		} elseif ($imagetype == "periodevergelijking") {
			BookwebStatistiekenPlaatjes::imgPeriodeVergelijking(
				$boekjaren,null,$leveranciers, false);
		} elseif ($imagetype == "boekjaarvergelijkingcumlboeken"){
			BookwebStatistiekenPlaatjes::imgBoekjaarvergelijking(
				$boekjaren,null,$leveranciers, true, 2);
		} elseif ($imagetype == "boekjaarvergelijkingboeken"){
			BookwebStatistiekenPlaatjes::imgBoekjaarvergelijking(
				$boekjaren,null,$leveranciers, false, 2);
		} elseif ($imagetype == "periodevergelijkingboeken") {
			BookwebStatistiekenPlaatjes::imgPeriodeVergelijking(
				$boekjaren,null,$leveranciers, true);
		} elseif ($imagetype == "gemiddeldeprijs"){
			BookwebStatistiekenPlaatjes::imgBoekjaarvergelijking(
				$boekjaren,null,$leveranciers, true, 3);
		} elseif ($imagetype == "artikelenoffertevergelijking"){
			BookwebStatistiekenPlaatjes::imgArtikelenOfferteVergelijking($eans);
		} elseif ($imagetype == "artikelenverkoopgeschiedenis"){
			BookwebStatistiekenPlaatjes::imgArtikelenVerkoopGeschiedenis($eans);
		}
	}


	/**
		Retourneert een array:
			$array["r"] = rood
			$array["g"] = groen
			$array["b"] = blauw

		De parameter $hue wordt geacht tussen 0-360 te liggen (in graden dus!), de andere twee tussen 0-100.
	*/
	static function hsb2rgb($hue, $saturation, $brightness){
		$r = 0;
		$g = 0;
		$b = 0;
		if ($saturation != 0 && $hue >= 0 && $hue <= 360 && $saturation >= 0 && $saturation <= 100 && $brightness >= 0 && $brightness <= 100){
			//$hue = $hue * (360/255);
			// Saturation en brightness corrigeren zodat deze tussen 0 en 255 ligt
			$saturation = $saturation * (255/100);
			$brightness = $brightness * (255/100);

			$max = $brightness;
			$diff = $brightness * $saturation / 255;
			$min = $brightness - $diff;

			if ($hue < 60){
				$r = $max; $b = $min; $g = $hue * $diff / 60 + $min;
			} elseif ($hue < 120){
				$g = $max; $b = $min; $r = -($hue - 120) * $diff / 60 + $min;
			} elseif ($hue < 180){
				$g = $max; $r = $min; $b = ($hue - 120) * $diff / 60 + $min;
			} elseif ($hue < 240){
				$b = $max; $r = $min; $g = -($hue - 240) * $diff / 60 + $min;
			} elseif ($hue < 300){
				$b = $max; $g = $min; $r = ($hue - 240) * $diff / 60 + $min;
			} elseif ($hue <= 360){
				$r = $max; $g = $min; $b = -($hue - 360) * $diff / 60 + $min;
			}
		}

		return array(
			"r"=>$r,
			"g"=>$g,
			"b"=>$b);
	}

	/**
		Genereert $number kleuren op basis van een HSB-kleurenschema, waardoor de kleuren
		altijd verschillend en opvallend zijn. Resultaat:
		array(
			0	=> "#001122",
			1	=> "#558833", etc);

		(maar dan met mooie, gegenereerde kleuren)
	*/
	static function randomHexColors($number = 1){
		$res = array();
		for ($counter = 1; $counter <= $number; $counter++){
			// Berekenen hoeveel graden Hue (0-360) moet zijn
			$hue = 360 / ($number / $counter);
			$rgb = BookwebStatistiekenPlaatjes::hsb2rgb($hue, 100,100);
			$res[] = "#" . sprintf("%02X%02X%02X", $rgb["r"], $rgb["g"], $rgb["b"]);
		}

		return $res;
		//return "#" . sprintf("%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
	}

	/**
		Print een plaatje met staven voor verschillende periodes
	*/
	static function imgPeriodeVergelijking($jaren, $kleuren, $leveranciers, $grafiektype){
		/**
			Periodeomzetten ophalen:
			$periodeomzetten[2005][2] = omzet in tweede periode van
			boekjaar 2005
		*/
		$dbData = BookwebStatistieken::computeOmzet(
			"periode", $jaren, $leveranciers,$grafiektype);

		// Plaatje sorteren per periode. Dus eerst alle eerste periodes
		// (jaar1, jaar2, etc), dan alle tweede periodes, etc.
		/*for ($periodeCounter = 1; $periodeCounter <= 4; $periodeCounter++){
			foreach ($periodeOmzetten as $jaar=>$jaarPeriodes){
				if (!isset($jaarPeriodes[$periodeCounter])){
					$periodeOmzet = 0;
				} else {
					$periodeOmzet = $jaarPeriodes[$periodeCounter];
				}

				$periodeData[$periodeCounter][$jaar] = $periodeOmzet;
			}
		}*/
		global $PERIODES;
		foreach ($jaren as $jaar){
			for ($periodeCounter = 0; $periodeCounter <= 3; $periodeCounter++){
				if (isset($dbData[$jaar][$periodeCounter + 1])){
					$thisOmzet = $dbData[$jaar][$periodeCounter + 1];
				} else{
					$thisOmzet = 0;
				}

				$graphData[$jaar][$periodeCounter] = $thisOmzet;
			}
		}

		// Klaar maken om in het graph object te stoppen
		/*foreach ($graphData as $jaar=>$jaarInfo){
			foreach ($jaarInfo as $jaar=>$omzet){
				$newData[$periodeNr][] = $omzet;
			}
		}
		$periodeData = $newData;*/

		/*echo "<pre>";
		print_r($graphData);
		echo "</pre>";*/

		if ($kleuren == null || sizeof($kleuren) != sizeof($jaren)){
			$genKleuren = BookwebStatistiekenPlaatjes::randomHexColors(sizeof($jaren));


			$colorIndex = 0;

			foreach ($jaren as $jaar){
				$kleuren["$jaar"] = $genKleuren[$colorIndex];
				$colorIndex++;
			}
		}

		$bars = array();
		foreach ($graphData as $jaar=>$jaarInfo){
			$bplot = new BarPlot($jaarInfo);
			$bplot->setFillColor($kleuren["$jaar"]);
			$bplot->setLegend("Boekjaar " . $jaar . "-" . ($jaar + 1));
			$bars[] = $bplot;
		}
/*		echo "<pre>";
		print_r($periodeData);
		echo "</pre>\n";*/
		$gbarplot = new GroupBarPlot($bars);

		$graph = new Graph(750,500,"auto");
		$graph->SetScale("textlin");
		$graph->SetMargin(50,200,50,50);
		$graph->Add($gbarplot);
		$graph->title->Set("Vergelijking van periodes");
		$graph->title->SetFont(FF_VERA,FS_BOLD);
		$graph->xaxis->title->Set("Periodes en jaren");
		$graph->yaxis->title->SetFont(FF_VERA,FS_BOLD);
		$graph->xaxis->title->SetFont(FF_VERA,FS_BOLD);
		$graph->xaxis->SetTickLabels(array("Periode 1", "Periode 2",
		"Periode 3", "Periode 4"));
		// True = aantal units, False = waarde
		if ($grafiektype) {
			$graph->yaxis->title->Set("Aantal artikelen");
		}
		else {
			$graph->yaxis->title->Set("Omzet");
		}
		$graph->Stroke();
	}


	/**
		Print een plaatje naar de stdout met daarin een vergelijking van
		verschillende boekjaren, op basis van de omzet van gegeven
		leveranciers. Parameters:

		$jaren = array(2004,2005);
		$kleuren = array(2004=>"#FFFF44", 2005=>"#448822");

		als $kleuren null is, dan worden kleuren random gegenereerd

		$leveranciers[] = levnr

		$grafiektype 	= 1  => vergelijk omzet
				= 2  => vergelijk aantal boeken
	*/
	static function imgBoekjaarvergelijking($jaren, $kleuren = null, $leveranciers = null, $cumulatief = true, $grafiektype = 1 ){
		if ($kleuren == null || sizeof($kleuren) != sizeof($jaren)){
			$genKleuren = BookwebStatistiekenPlaatjes::randomHexColors(sizeof($jaren));

			$colorCounter = 0;
			foreach ($jaren as $jaar){
				$kleuren[$jaar] = $genKleuren[$colorCounter];
				$colorCounter++;
			}
		}

		/**
			Jaaromzetten ophalen:
			$jaaromzetten[2005][10-31] = omzet op 31 oktober in boekjaar 05-06
		*/
		$jaaromzetten = BookwebStatistieken::computeOmzet(
			"dag", $jaren, $leveranciers, $grafiektype );
		$jaaromzetten2 = BookwebStatistieken::computeOmzet(
			"dag", $jaren, $leveranciers, 2 );


		// Alle mogelijke datums uitzoeken die in een jaar voorkomen, om zo
		// de lijnen in de grafiek goed naast elkaar te kunnen zetten.
		$datums = array();
		/*foreach ($jaaromzetten as $boekjaar=>$boekjaaromzet){
			foreach ($boekjaaromzet as $datum=>$dagomzet){
				$datums[] = $datum;
			}
		}*/
		for ($monthcounter = 1; $monthcounter <= 12; $monthcounter++){
			for ($daycounter = 1; $daycounter <= 31; $daycounter++){
				if ($monthcounter < 10){
					$monthstring = "0$monthcounter";
				} else {
					$monthstring = "$monthcounter";
				}

				if ($daycounter < 10){
					$daystring = "0$daycounter";
				} else {
					$daystring = $daycounter;
				}
				$datums[] = $monthstring . "-" . $daystring;
			}
		}

		// Datums uniek maken en sorteren.
		array_unique($datums);
		sort($datums);

		// Door het sorteren komt 4 januari voor 5 oktober, terwijl we in
		// boekjaren denken. Dus 5 oktober (2005) komt voor 4 januari
		// (2006)! Dit moet gecorrigeerd worden, het makkelijkste is om een
		// boekjaar in 2en te delen: (1) voor jaarwisseling (2) na
		// jaarwisseling
		$newdata = array();
		$newdata2 = array();
		foreach ($jaaromzetten as $boekjaar=>$boekjaarinfo){
			foreach ($datums as $datum){
				if (substr($datum,0,2) < 8){
					// Maandnummer (1-12) is kleiner dan 8, dus de maand is
					// januari-juli van het tweede deel van het collegejaar
					$part = 2;
				} else {
					// Maandnummer is groter of gelijk aan 8, dus de maand is
					// augustus-december van het eerste deel van
					// het collegejaar
					$part = 1;
				}

				if (!isset($jaaromzetten[$boekjaar][$datum])){
					$newdata[$boekjaar][$part][$datum] = 0;
					$newdata2[$boekjaar][$part][$datum] = 0;
				} else{
					$newdata[$boekjaar][$part][$datum] = $jaaromzetten[$boekjaar][$datum];
					$newdata2[$boekjaar][$part][$datum] = $jaaromzetten2[$boekjaar][$datum];
				}
			}
		}
		$jaaromzetten = $newdata;
		$jaaromzetten2 = $newdata2;

		// Nu heeft $jaaromzetten de volgende vorm:
		// $jaaromzetten[$boekjaar][$boekjaardeel][$datum] = omzet
		//
		// Nu de data gereed maken om in een grafiek te stoppen
		foreach ($jaaromzetten as $boekjaar=>$boekjaarinfo){
			$cuml = 0;
			$cuml2 = 0;

			// Eerst deel 1 van het jaar, dan deel 2
			for ($i = 1; $i <= 2; $i++){
				$boekjaardeelinfo = $boekjaarinfo[$i];

				$lastMonth = "";
				foreach ($boekjaardeelinfo as $datum=>$omzet){
					$currentMonth = substr($datum, 0, 2);

					$cuml = $cuml + $omzet;
					$cuml2 = $cuml2 + $jaaromzetten2[$boekjaar][$i][$datum];
					if ($grafiektype == 3) {
						$graphdata[$boekjaar][] = $cuml2 !=0 ? $cuml / $cuml2 : 50;
					} elseif ($cumulatief){
						$graphdata[$boekjaar][] = $cuml;
					} else {
						$graphdata[$boekjaar][] = $omzet;
					}

					if ($lastMonth != $currentMonth){
						// Ticklabel maken
						$ticklabels[$boekjaar][] = "$currentMonth";
					} else {
						$ticklabels[$boekjaar][] = "";
					}

					$lastMonth = $currentMonth;
				}
			}
		}

		foreach ($graphdata as $boekjaar=>$boekjaardata){
			$boekjaar2 = $boekjaar + 1;
			$tempLine = new LinePlot($boekjaardata);
			$tempLine->SetLegend("Boekjaar $boekjaar-$boekjaar2");
			$tempLine->SetColor($kleuren[$boekjaar]);
			$lines[$boekjaar] = $tempLine;
		}

		//$basegraph = getBaseGraph(1000,500, 100000);
		$graph = new Graph(750,500,"auto");
		$graph->SetScale("textlin");

		foreach($lines as $boekjaar=>$line){
			$graph->Add($lines[$boekjaar]);
		}

		$graph->xaxis->title->Set("Maanden");
		// $graph->yaxis->title->Set("Omzet"); //Past niet!

		$graph->title->SetFont(FF_VERA,FS_BOLD);
		$graph->yaxis->title->SetFont(FF_VERA,FS_BOLD);
		$graph->xaxis->title->SetFont(FF_VERA,FS_BOLD);

		$graph->img->SetMargin(50,50,50,50);
		$graph->xaxis->SetTextTickInterval(31);
		$graph->xaxis->SetTickLabels($ticklabels[$jaren[0]]);
		$graph->legend->Pos(0.05,0.6,"right","center");


		if ($cumulatief){
			$title = "Vergelijking (cumulatief)";
		} else {
			$title = "Vergelijking";
		}

		if ($grafiektype == 1){
			$title .= " omzet boekjaren";
		} else {
			$title .= " aantal verkochte artikelen";
		}

		$graph->title->set($title);

		// Display the graph
		$graph->Stroke();
	}

	static function imgArtikelenVerkoopGeschiedenis($EANs){
		global $BWDB;
		$kleuren = BookwebStatistiekenPlaatjes::randomHexColors(sizeof($EANs));

		$colorCounter = 0;
		$titels = array();
		$verkopen = array();
		foreach ($EANs as $EAN){
			$kleuren["$EAN"] = $kleuren[$colorCounter];
			$colorCounter++;

			$titel = $BWDB->q("VALUE
				SELECT titel
				FROM boeken
				WHERE EAN=%s", $EAN);

			$titel = str_cut($titel, 25);
			$titels["$EAN"] = $titel;

		}

		$verkopen = BookwebStatistieken::getVerkopen($EANs);
		$verkopenCuml = array();
		$alleDatums = array();
		foreach ($verkopen as $EAN => $verkoopinfo){
			$lastAantal = 0;
			foreach ($verkoopinfo as $datum => $aantal){
				$lastAantal = $lastAantal + $aantal;
				$verkopenCuml["$EAN"]["$datum"] = $lastAantal;
				$alleDatums[] = $datum;
			}
		}

		$alleDatums = array_unique($alleDatums);
		if (sizeof($alleDatums) == 1){
			$alleDatums[] = "0000-01-01";
		}
		sort($alleDatums);
		foreach ($verkopenCuml as $EAN => $verkoopinfo){
			$laatsteWaarde = 0;
			foreach ($alleDatums as $datum){
				if (isset($verkopenCuml["$EAN"]["$datum"])){
					$laatsteWaarde = $verkopenCuml["$EAN"]["$datum"];
				}

				$graphData["$EAN"][] = $laatsteWaarde;
			}
		}


		foreach ($graphData as $EAN => $verkoopdata){
			$tempLine = new LinePlot($verkoopdata);
			$tempLine->SetLegend($titels["$EAN"]);
			$tempLine->SetColor($kleuren["$EAN"]);
			$lines[$EAN] = $tempLine;
		}

		//$basegraph = getBaseGraph(1000,500, 100000);
		$graph = new Graph(800,300,"auto");
		$graph->SetScale("textlin");

		foreach($lines as $EAN=>$line){
			$graph->Add($lines[$EAN]);
		}

	//	$graph->xaxis->title->Set("Maanden");
		// $graph->yaxis->title->Set("Omzet"); //Past niet!

		$graph->title->SetFont(FF_VERA,FS_BOLD);
		$graph->yaxis->title->SetFont(FF_VERA,FS_BOLD);
		$graph->xaxis->title->SetFont(FF_VERA,FS_BOLD);

		$graph->img->SetMargin(50,50,50,50);
		if (sizeof($alleDatums) > 20){
			$graph->xaxis->SetTextTickInterval(2);
		} elseif (sizeof($alleDatums) > 40){
			$graph->xaxis->SetTextTickInterval(4);
		}

		$graph->xaxis->SetFont(FF_VERA, FS_NORMAL, 6);
		$graph->xaxis->SetLabelAngle(50);
		$graph->xaxis->SetTickLabels($alleDatums);
		$graph->legend->Pos(0.05,0.6,"right","center");

		$graph->title->set("Verkoopgeschiedenis per artikel");

		// Display the graph
		$graph->Stroke();

	}

	static function imgArtikelenOfferteVergelijking($EANs){
		global $BWDB;

		// Kleuren genereren
		$kleuren = BookwebStatistiekenPlaatjes::randomHexColors(sizeof($EANs));
		$colorCounter = 0;
		$titels = array();
		foreach ($EANs as $EAN){
			$kleuren[$EAN] = $kleuren[$colorCounter];
			$colorCounter++;

			$titel = $BWDB->q("VALUE
				SELECT titel
				FROM boeken
				WHERE EAN=%s", $EAN);

			$titel = str_cut($titel, 25);
			$titels["$EAN"] = $titel;
		}

		// Array:
		// $prijzen[$EAN][$offertedatum] = $prijs
		$prijzen = BookwebStatistieken::artikelenOffertes($EANs);

		$lines = array();
		$alleDatums = array();
		// Alle datums opzoeken
		foreach ($prijzen as $EAN => $artikelinfo){
			foreach ($artikelinfo as $datum => $levprijs){
				$alleDatums[] = $datum;
			}
		}

		// Data preformatten zodat er voor iedere datum bij elk ean een
		// offerte is
		$alleDatums = array_unique($alleDatums);
		if (sizeof($alleDatums) == 1){
			// Extra datum ervoor invoegen, een grafiek moet namelijk
			// meerdere punten hebben
			$alleDatums[] = "0000-01-01";
		}

		sort($alleDatums);
		$graphData = array();
		foreach ($EANs as $EAN){
			$laatstePrijs = 0;
			foreach ($alleDatums as $datum){
				if (isset($prijzen["$EAN"]["$datum"])){
					$laatstePrijs = $prijzen["$EAN"]["$datum"];
				}
				//$laatstePrijs = $laatstePrijs + 1;
				$graphData["$EAN"]["$datum"] = $laatstePrijs;
				$graphData["$EAN"]["alleprijzen"][] = $laatstePrijs;
			}
		}
		foreach ($graphData as $EAN => $info){
			$tempLine = new LinePlot($info["alleprijzen"]);
			$tempLine->SetLegend($titels["$EAN"]);
			$tempLine->SetColor($kleuren[$EAN]);


			$lines[$EAN] = $tempLine;
		}

		$graph = new Graph(500,300,"auto");
		$graph->SetScale("textlin");
		foreach ($lines as $EAN => $line){
			$graph->Add($lines[$EAN]);
		}

		// $graph->yaxis->title->Set("Omzet"); //Past niet!

		$graph->title->SetFont(FF_VERA, FS_BOLD, 15);
		$graph->yaxis->SetFont(FF_VERA, FS_NORMAL, 9);
		//$graph->yaxis->title->set("Offerteprijs");

		$graph->xaxis->SetFont(FF_VERA, FS_NORMAL, 6);

		$graph->img->SetMargin(50,50,50,50);
		$graph->xaxis->SetTickLabels($alleDatums);
		$graph->xaxis->SetLabelAngle(50);
		$graph->legend->Pos(0.05,0.6,"right","center");

		$graph->title->set("Offertegeschiedenis");

		// Display the graph
		$graph->Stroke();
	}
}

