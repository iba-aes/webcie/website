<?php

/*
	Deze klasse representeert een voorraad: een bepaald item (EAN) van
	een bepaalde leverancier voor een bepaalde prijs.
	Iedere nieuwe levering voor dezelfde prijs vanhetzelfde item van
	dezelfde leverancier krijgt het zelfde voorraadnr. Nieuwe
	prijs/EAN/leverancier => nieuw voorraadnr
*/
class Voorraad extends BookwebBase {
	private $_voorraadnr = -1;
	private $_EAN;
	private $_levprijs;
	private $_adviesprijs;
	private $_voorraad = array ();
	private $_leveranciernr;
	private static $_voorraadlocaties = array(
		"voorraad",
		"buitenvk",
		"ejbv_voorraad"
	);


	function __construct($voorraadnr){
		// Alle aantallen op 0 initialiseren
		foreach (Voorraad::$_voorraadlocaties as $locatie){
			$this->_voorraad[$locatie] = 0;
		}

		if (is_numeric($voorraadnr) && $voorraadnr >= 0){
			// Uit database halen
			$this->_voorraadnr = $voorraadnr;
			$this->update();
		} // else: nieuwe voorraad
	}

	function isValid(){
		if ($this->getNr() >= 0){
			return $this->existsInDatabase();
		}

		return false;
	}

	/**
		Deze functie bekijkt van het gegeven voorraadnr of het
		bestaat in de database. Als voorraadnr null is, dan wordt
		"this" gebruikt. Deze functie is dus zowel statis als
		objectoriented
	*/
	function existsInDatabase($voorraadnr = null){
		global $BWDB;

		if ($voorraadnr == null) $voorraadnr = $this->getNr();

		$count = $BWDB->q("VALUE SELECT COUNT(*) FROM voorraad WHERE
		voorraadnr=%i", $voorraadnr);

		return ($count > 0);
	}

	function update(){
		if (!$this->isValid()){
			throw new BookwebException("Dit Voorraad-object bestaat niet in de database en kan derhalve niet bijgewerkt worden uit de database");
		} else {
			// Lekker updaten
			global $BWDB;

			$values = $BWDB->q("TUPLE SELECT *
				FROM voorraad WHERE
				voorraadnr=%i", $this->getNr()
			);

			return $this->setValuesByArray($values);
		}
	}


	function setValuesByArray($valuesarray){
		// Alle set-functies gooien een exception bij ongeldige waarde
		$this->setEAN($valuesarray["EAN"]);
		$this->setLeverancierNr($valuesarray["leveranciernr"]);
		$this->setLevPrijs($valuesarray["levprijs"]);
		$this->setAdviesPrijs($valuesarray["adviesprijs"]);
		$this->setVoorraad($valuesarray["voorraad"], "voorraad");
		$this->setVoorraad($valuesarray["buitenvk"], "buitenvk");
		$this->setVoorraad($valuesarray["ejbv_voorraad"], "ejbv_voorraad");

		return true;
	}

	private function _asArray(){
		$res = array(
			"EAN" => $this->_EAN,
			"levprijs" => $this->_levprijs,
			"adviesprijs" => $this->_adviesprijs,
			"voorraad" => $this->_voorraad["voorraad"],
			"buitenvk" => $this->_voorraad["buitenvk"],
			"ejbv_voorraad" => $this->_voorraad["ejbv_voorraad"],
			"leveranciernr" => $this->_leveranciernr
		);

		// Als dit object in de database zit ook het voorraadnr meesturen.
		if ($this->existsInDatabase()) $res["voorraadnr"] = $this->_voorraadnr;

		return $res;
	}

	function store(){
		global $BWDB;

		// NOTE: onderstaande functionaliteit kan niet worden geimplementeerd
		// m.b.v. "REPLACE INTO", omdat BookWeb vol zit met foreign key
		// constraints. Een REPLACE INTO doet eerst een delete, maar die zal
		// falen vanwege constraints.

		if ($this->existsInDatabase()){
			// UPDATE
			$data = $this->_asArray();
			unset($data["voorraadnr"]);
			$aff = $BWDB->q("RETURNAFFECTED UPDATE voorraad SET %S WHERE voorraadnr = %i",
				$data, $this->getNr()
			);

			if (!($aff === 1)){
				throw new Exception("Update van voorraadnr " . $this->getNr() .
					"niet gelukt om onbekende reden!"
				);
			}
		} else {
			// INSERT
			$voorraadnr = $BWDB->q("RETURNID INSERT INTO voorraad SET %S", $this->_asArray());
			$this->_voorraadnr = $voorraadnr;
		}
	}


	function setEAN($EAN){
		if (!is_EAN($EAN)) throw new BookwebException("Het opgegeven EAN ($EAN) is ongeldig");

		$this->_EAN = $EAN;
	}

	function getEAN(){
		return $this->_EAN;
	}


	function setLeverancierNr($leveranciernr){
		if (is_numeric($leveranciernr) && $leveranciernr >= 0){
			$this->_leveranciernr = $leveranciernr;
		} else {
			throw new BookwebException("Het opgegeven
			leveranciernr ($leveranciernr) is ongeldig: het
			moet numeriek zijn en groter of gelijk aan nul");
		}
	}

	function getLeverancierNr(){
		return $this->_leveranciernr;
	}

	function setLevPrijs($levprijs){
		if (is_numeric($levprijs) && $levprijs >= 0){
			$this->_levprijs = $levprijs;
		} else {
			throw new BookwebException("De opgegeven
			levprijs ($levprijs) is ongeldig: de prijs
			moet numeriek zijn en groter of gelijk aan nul");
		}

		return true;
	}

	function berekenVerkoopPrijs(){
		return berekenVerkoopPrijs($this->getLevPrijs(), $this->getBoekenMarge(), $this->getAdviesPrijs());
	}


	function getLevPrijs(){
		return $this->_levprijs;
	}

	function setAdviesPrijs($adviesprijs){
		if ($adviesprijs == null){
			$this->_adviesprijs = null;
		} elseif (is_numeric($adviesprijs) && $adviesprijs >= 0){
			$this->_adviesprijs = $adviesprijs;
		} else {
			throw new BookwebException("De opgegeven
			adviesprijs ($adviesprijs) is ongeldig: de prijs
			moet numeriek zijn en groter of gelijk aan nul");
		}

		return true;
	}

	function getAdviesPrijs(){
		return $this->_adviesprijs;
	}

	function getBuitenVk(){
		return $this->_buitenvk;
	}

	/**
		Verhoogt de voorraad op locatie '$locatie' met $aantal
	**/
	function incrVoorraad($aantal, $locatie = "voorraad"){
		// setVoorraad en getVoorraad doen voldoende checks op parameters
		$this->setVoorraad($this->getVoorraad($locatie) + $aantal, $locatie);
	}

	/**
		Verlaagt de voorraad op locatie '$locatie' met $aantal
	**/
	function decrVoorraad($aantal, $locatie = "voorraad"){
		// setVoorraad en getVoorraad doen voldoende checks op parameters
		$this->setVoorraad($this->getVoorraad($locatie) - $aantal, $locatie);
	}

	function setVoorraad($aantal, $locatie = "voorraad"){
		Voorraad::checkVoorraadLocatie($locatie); // doet exception
		$this->_checkAantal($aantal); // doet exception

		$this->_voorraad[$locatie] = $aantal;
		return true;
	}

	/**
		Controleert of het opgegeven aantal
		geldig is en geeft eventueel een error af. Gooit
		een exception bij ongeldige locatie, retourneert
		true bij geldige locatie
	**/
	private function _checkAantal($aantal){
		if (!is_numeric($aantal) || $aantal < 0){
			throw new BookwebException("Het opgegeven
			voorraadaantal ($aantal) is ongeldig: het moet
			numeriek zijn en groter of gelijk aan nul");
		}
		return true;
	}

	/**
		Retourneert het voorraadaantal op de opgegeven
		voorraadlocatie. Voorraadlocatie kan vanalles zijn,
		maar bij voorkeur "voorraad", "buitenvk" of "ejbv_voorraad"
		(default: "voorraad")
	**/
	function getVoorraad($locatie = "voorraad"){
		Voorraad::checkVoorraadLocatie($locatie); // doet exception

		return $this->_voorraad[$locatie];
	}

	/**
		Retourneert de boekenmarge die van toepassing is (is afhankelijk van leverancier)
	**/
	function getBoekenMarge(){
		$data = $this->getLeverancierData();
		return $data[0]["boekenmarge"];
	}

	/**
		Retourneert een key->value array met leverancierinfo
	**/
	function getLeverancierData(){
		global $BWDB;
		return $BWDB->q("TABLE SELECT *
			FROM leverancier
			WHERE leveranciernr = %i", $this->_leveranciernr
		);
	}

	/**
		Retourneert de naam van de leverancier van deze voorraad

		// TODO: eigenlijk moet er een Leverancier-object komen dat opgevraagd kan worden,
		// waarna in dat object een functie 'getNaam' aangeroepen wordt. Dit is niet de
		// goede plaats voor deze functie...
	**/
	function getLeverancierNaam(){
		$data = $this->getLeverancierData();
		return $data[0]["naam"];
	}

	/*	STATIC
		Deze functie retourneert een array van Voorraad-objecten
		die aan de gegeven zoekeisen voldoen. Als een zoekeis niet
		mee mag tellen, dan moet die null zijn.
		Als er iets fout gaat, retourneert deze functie false, als
		er geen resultaten zijn, dan retourneert deze functie een
		lege array
	*/
	static function find($EAN = null, $levprijs = null, $adviesprijs = null, $leveranciernr = null){
		global $BWDB;
		$res = array();


		if ($EAN == null && $levprijs == null && $adviesprijs == null && $leveranciernr == null){
			throw new BookwebException("Er moet minstens 1 zoekcriterium opgegeven worden!");
		}

		$voorraadnrs = array();

		// Voor de veiligheid worden hier meerdere, losse queries
		// uitgevoerd, waarbij het uiteindelijke resultaat bestaat
		// uit de intersectie van de verschillende resultaten
		$voorraadnrs["ean"] = array();
		$voorraadnrs["levprijs"] = array();
		$voorraadnrs["adviesprijs"] = array();
		$voorraadnrs["leveranciernr"] = array();

		if ($EAN != null){
			$voorraadnrs["ean"] = $BWDB->q("COLUMN SELECT voorraadnr
			FROM voorraad WHERE EAN=%s", $EAN);
		}
		if ($levprijs != null){
			$voorraadnrs["levprijs"] = $BWDB->q("COLUMN SELECT voorraadnr
			FROM voorraad WHERE levprijs=%s", $levprijs);
		}
		if ($adviesprijs != null){
			$voorraadnrs["adviesprijs"] = $BWDB->q("COLUMN SELECT voorraadnr
			FROM voorraad WHERE adviesprijs=%s", $adviesprijs);
		}
		if ($leveranciernr != null){
			$voorraadnrs["leveranciernr"] = $BWDB->q("COLUMN SELECT voorraadnr
			FROM voorraad WHERE leveranciernr=%s",
			$leveranciernr);
		}

		$voorraadnrs = array_intersect(
			$voorraadnrs["ean"], $voorraadnrs["levprijs"],
			$voorraadnrs["adviesprijs"],
			$voorraadnrs["leveranciernr"]);

		foreach ($voorraadnrs as $voorraadnr){
			$res[] = new Voorraad($voorraadnr);
		}

		return $res;
	}


	/**
		Deze functie retourneert meerdere soorten belangrijke
		informatie over de voorraad in de geselecteerd periode:
		- Leveringen in de gegeven periode
		- Retouren in de gegeven periode
		- Verkopen in de gegeven periode
		- Terugkopen in de gegeven periode

		Overigens is het mogelijk om 1 (of 2) van de 2 datums weg
		te laten. Als datum_tot weggelaten wordt, dan wordt NOW()
		gebruikt als datum_tot. Als datum_van weggelaten wordt, dan
		wordt alle geschiedenis tot datum_tot geretourneerd.

		Formaat:
		array(
		  "leveringen" => array(
		    "2004-10-10"=>22,
		    "2005-09-28"=>45),
		  "retouren" => array(
		    "2004-12-23"=>3
		    "2005-04-23"=>5);
		  etc
		)

		TODO: in de toekomst objecten (Levering, Retour, Verkoop,
		Terugkoop) in de array stoppen, i.p.v. aatallen
	*/
	function getVoorraadInformatie($datum_van = null, $datum_tot = null){
		global $BWDB;

		$res["leveringen"] = $this->getLeveringen($datum_van, $datum_tot);
		$res["retouren"] = $this->getRetouren($datum_van, $datum_tot);
		$res["verkopen"] = $this->getVerkopen($datum_van, $datum_tot);
		$res["terugkopen"] = $this->getTerugkopen($datum_van, $datum_tot);

		return $res;
	}

	function getLeveringen($datum_van = null, $datum_tot = null){
		global $BWDB;

		if (!$datum_van) $datum_van = "0000-00-00";
		if (!$datum_tot) $datum_tot = "NOW()";

		$res = array();
		$leveringen = $BWDB->q("TABLE SELECT wanneer, aantal FROM
		levering WHERE voorraadnr=%i AND wanneer>=%s AND wanneer<=%s",
		$this->getNr(), $datum_van, $datum_tot);
		foreach ($leveringen as $levering){
			$wanneer = $levering["wanneer"];
			$aantal = $levering["aantal"];

			if ( array_key_exists("$wanneer", $res) )
				$res["$wanneer"] += $aantal;
			else
				$res["$wanneer"] = $aantal;
		}

		return res;
	}

	function getRetouren($datum_van = null, $datum_tot = null){
		global $BWDB;

		if (!$datum_van) $datum_van = "0000-00-00";
		if (!$datum_tot) $datum_tot = "NOW()";

		$res = array();
		$retouren = $BWDB->q("TABLE SELECT wanneer, aantal FROM
		retour WHERE voorraadnr=%i AND wanneer>=%s AND wanneer<=%s",
		$this->getNr(), $datum_van, $datum_tot);
		foreach ($retouren as $retour){
			$wanneer = $retour["wanneer"];
			$aantal = $retour["aantal"];

			if ( array_key_exists("$wanneer", $res) )
				$res["$wanneer"] += $aantal;
			else
				$res["$wanneer"] = $aantal;
		}

		return $res;
	}

	/**
		Retourneert een array in formaat
			$array[datum] = aantal

		van de verkopen tussen $datum_van en $datum_tot. Eventueel kunnen
		die parameters weggelaten worden.
	**/
	function getVerkopen($datum_van = null, $datum_tot = null){
		global $BWDB;
		if (!$datum_van) $datum_van = "0000-00-00";
		if (!$datum_tot) $datum_tot = "NOW()";

		$res = array();
		$verkopen = $BWDB->q("TABLE SELECT wanneer, aantal FROM
		verkoop WHERE voorraadnr=%i AND wanneer>=%s AND wanneer<=%s",
		$this->getNr(), $datum_van, $datum_tot);

		foreach ($verkopen as $verkoop){
			$wanneer = $verkoop["wanneer"];
			$aantal = $verkoop["aantal"];

			if ( array_key_exists("$wanneer", $res) )
				$res["$wanneer"] += $aantal;
			else
				$res["$wanneer"] = $aantal;
		}

		return $res;
	}

	/**
		Retourneert het AANTAL netto verkopen
	*/
	function getNettoVerkopen($datum_van = null, $datum_tot = null){
		$verkopen = $this->getVerkopen($datum_van, $datum_tot);
		$terugkopen = $this->getTerugkopen($datum_van, $datum_tot);

		$verkopen = array_sum($verkopen);
		$terugkopen = array_sum($terugkopen);

		return ($verkopen - $terugkopen);
	}

	function getTerugkopen($datum_van = null, $datum_tot = null){
		global $BWDB;

		if (!$datum_van) $datum_van = "0000-00-00";
		if (!$datum_tot) $datum_tot = "NOW()";

		$res = array();
		$terugkopen = $BWDB->q("TABLE SELECT wanneer, aantal FROM
		terugkoop WHERE voorraadnr=%i AND wanneer>=%s AND wanneer<=%s",
		$this->getNr(), $datum_van, $datum_tot);
		foreach ($terugkopen as $terugkoop){
			$wanneer = $terugkoop["wanneer"];
			$aantal = $terugkoop["aantal"];

			if ( array_key_exists("$wanneer", $res) )
				$res["$wanneer"] += $aantal;
			else
				$res["$wanneer"] = $aantal;
		}

		return $res;
	}

	function getNr(){
		return $this->_voorraadnr;
	}

	/**
		Verplaatst voorraad van de ene locatie naar de andere locatie. Dit
		omhelst zowel een wijziging in de tabel 'voorraad' als een nieuwe entry
		in de tabel 'verplaatsing'
	**/
	function verplaats($van, $naar, $aantal, $reden){
		Voorraad::checkVoorraadLocatie($van); // doet exception
		Voorraad::checkVoorraadLocatie($naar); // doet exception
		$this->_checkAantal($aantal);

		$this->update();
		if ($this->getVoorraad($van) < $aantal){
			throw new Exception("Er is onvoldoende voorraad van artikel " .
				$this->getEAN() . " (voorraadnr " . $this->getNr() . ") aanwezig
				op voorraadlocatie '$van': " . $this->getVoorraad($van) .
				" stuks. Er kunnen geen $aantal items verplaatst worden naar
				locatie '$naar'."
			);
		}

		global $BWDB;
		global $auth;
		// Verplaatsing registreren in 'verplaatsing'-database
		$BWDB->q("INSERT INTO verplaatsing SET %S",
			array(
				'wanneer' 		=> getActionDateTime(),
				'voorraadnr' 	=> $this->getNr(),
				'van'     		=> $van,
				'naar'			=> $naar,
				'aantal'		=> $aantal,
				'reden'			=> $reden,
				'wie'			=> $auth->getLidnr()
			)
		);

		// Nu waarden van dit object bijwerken en opslaan
		$van_oudaantal = $this->getVoorraad($van);
		$naar_oudaantal = $this->getVoorraad($naar);
		$this->setVoorraad($van_oudaantal - $aantal, $van);
		$this->setVoorraad($naar_oudaantal + $aantal, $naar);
		$this->store();
	}


	/**
		Retourneert een array met informatie over deze voorraad en
		de facturen die ervan binnen gekomen zijn

		array(
			"nettoverkocht"=>342,
			"factuurregels"=>array(FactuurRegel-objecten)
		);

		Standaard worden alleen factuurregels die deel uitmaken van
		een definitieve factuur geretourneerd, maar dat gedag kan
		aangepast worden d.m.v. de functieparameters
	*/
	function getFactuurInformatie($definitieveFacturen = true,
	$conceptFacturen = false, $ongeldigeFacturen = false){

		$factuurRegels =
			LeverancierFactuurRegel::searchByVoorraad($this,
			$definitieveFacturen, $conceptFacturen,
			$ongeldigeFacturen);


		$aantalverkopen = $BWDB->q("VALUE SELECT SUM(aantal) FROM verkoop WHERE
		voorraadnr=%i", $this->getNr());

		$aantalterugkopen = $BWDB->q("VALUE SELECT SUM(aantal) FROM terugkoop WHERE
		voorraadnr=%i", $this->getNr());

		$nettoverkocht = $aantalverkopen - $aantalterugkopen;

		return array(
			"nettoverkocht"=>$nettoverkocht,
			"factuurregels"=>$factuurregels
		);
	}

	/**	STATIC
		Retourneert Voorraad-objecten die gerelateerd zijn aan het
		gegeven EAN (in een array). De voorraadnrs zijn de keys
		van de geretourneerde array.

		Als de parameter $alleen_met_voorraad true is, dan worden enkel
		objecten geretourneerd waarvoor nog een voorraad beschikbaar is
		(maakt niet uit of het reguliere voorraad, buiten-verkoop of
		EJBV-voorraad is)
	*/
	static function searchByEAN($EAN, $alleen_met_voorraad = true){
		if (!is_EAN($EAN)){
			throw new BookwebException("Het opgegeven EAN ($EAN) is ongeldig!");
		}

		global $BWDB;

		if (!$alleen_met_voorraad){
			// Ook objecten retourneren waarvan we helemaal geen
			// voorraad meer hebben
			$voorraadnrs = $BWDB->q("COLUMN SELECT voorraadnr
				FROM voorraad
				WHERE EAN=%s", $EAN
			);
		} else {
			// Enkel objecten waarvan nog spullen op voorraad zijn
			$voorraadnrs = $BWDB->q("COLUMN SELECT voorraadnr
				FROM voorraad
				WHERE EAN=%s
				AND (voorraad > 0
					OR buitenvk > 0
					OR ejbv_voorraad > 0
				)", $EAN
			);
		}

		$res = array();
		foreach ($voorraadnrs as $voorraadnr){
			$voorraad = new Voorraad($voorraadnr);
			$res[$voorraadnr] = $voorraad;
		}

		return $res;
	}

	/*
		Zoekt voorraad die verkocht is in de gegeven periode
	*/
	static function searchByVerkoopdatum($begin, $eind){
		if (!$begin = parseDate($begin)){
			return false;
		} elseif (!$eind = parseDate($eind)){
			return false;
		}

		global $BWDB;
		$voorraadnrs = $BWDB->q("COLUMN SELECT voorraadnr FROM verkoop
		WHERE wanneer>=%s AND wanneer <=%s GROUP BY voorraadnr", $begin, $eind);

		$res = array();
		foreach ($voorraadnrs as $voorraadnr){
			$voorraad = new Voorraad($voorraadnr);
			$res[] = $voorraad;
		}

		return $res;
	}

	static function isGeldigeVoorraadlocatie($locatie){
		return in_array($locatie, Voorraad::$_voorraadlocaties);
	}

	// Zie isGeldigeVoorraadLocatie. Deze functie gooit een exception bij ongeldige locatie
	static function checkVoorraadLocatie($locatie){
		if (!Voorraad::isGeldigeVoorraadLocatie($locatie)){
			throw new BookwebException("De opgegeven voorraadlocatie '$locatie' is ongeldig!");
		}
		return true;
	}
}

