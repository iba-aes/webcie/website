<?php

class LeverancierFactuurRegel extends BookwebBase
{
	var $_levfactuurregelnr = -1;
	var $_levfactuurnr;
	var $_voorraadnr;
	var $_EAN;
	var $_aantal = 0;
	var $_opmerkingen;
	var $_bedrag = 0;
	var $_bezwaren;
	var $_forceGeldigheid = "N";
	var $_factuurperiode_start = null;
	var $_factuurperiode_eind = null;

	function __construct($levfactuurregelnr = -1)
	{
		if (is_numeric($levfactuurregelnr) && $levfactuurregelnr >= 0){
			$this->_levfactuurregelnr = $levfactuurregelnr;
			$this->update();
		}
	}

	static function get($factuurregelnrs)
	{
		global $BWDB;
		static $cache;
		if (!$cache) $cache = array();
		if (sizeof($factuurregelnrs) == 0) return array();

		$nietincache = array();
		$res = array();
		foreach ($factuurregelnrs as $regelnr){
			if (!isset($cache[$regelnr])){
				$nietincache[] = $regelnr;
			} else {
				$res[$regelnr] = $cache[$regelnr];
			}
		}

		if (sizeof($nietincache) > 0){
			$data = $BWDB->q("
				TABLE SELECT *
				FROM leverancierfactuurregel
				WHERE levfactuurregelnr IN (%Ai)",
				$nietincache);

			foreach ($data as $row){
				$regelnr = $row["levfactuurregelnr"];
				$obj = new LeverancierFactuurRegel();
				$obj->setValuesByArray($row);
				$res[$regelnr] = $obj;
				$cache[$regelnr] = $obj;
			}
		}

		$sortedRes = array();
		foreach ($factuurregelnrs as $regelnr){
			if (isset($res[$regelnr])){
				$sortedRes[$regelnr] = $res[$regelnr];
			}
		}

		return $sortedRes;
	}

	function update()
	{
		global $BWDB;

		if ($this->getNr() >= 0){
			$values = $BWDB->q("TUPLE SELECT * FROM leverancierfactuurregel
			WHERE levfactuurregelnr=%i", $this->getNr());

			return $this->setValuesByArray($values);
		} else {
			parent::addError(new BookwebError("Dit object is nog niet
			opgeslagen in de database, er kan dus geen SQL-query mee
			uitgevoerd worden!"));
			return false;
		}
	}

	/**
		Deze functie dupliceert dit LeverancierFactuurRegel-object zodanig
		dat er onder een ander ID (nr) hetzelfde object opgeslagen wordt.
		Het resultaat is dat "this" verandert in het nieuwe object, dus met
		het nieuwe id
	*/
	function duplicateAndStore()
	{
		$this->_levfactuurregelnr = -1;
		$this->store();
	}

	function store()
	{
		global $BWDB;

		$voorraadnr = $this->getVoorraadnr();
		if ($voorraadnr < 0) $voorraadnr = null;

		$forceGeldigheid = $this->getForceGeldigheid();
		if ($forceGeldigheid != "Y" && $forceGeldigheid != "y" && $forceGeldigheid != true){
			$forceGeldigheid = "N";
		} else {
			$forceGeldigheid = "Y";
		}

		if ($this->getNr() >= 0 && is_numeric($this->getNr())){
			// SQL UPDATE


			$BWDB->q("UPDATE leverancierfactuurregel SET
				levfactuurnr=%i,
				EAN=%s,
				voorraadnr=%i,
				aantal=%s,
				bedrag=%s,
				opmerkingen=%s,
				bezwaren=%s,
				forceGeldigheid=%s,
				factuurperiode_start=%s,
				factuurperiode_eind=%s
				WHERE levfactuurregelnr=%i",
				$this->getLeveringFactuurNr(),
				$this->getEAN(),
				$voorraadnr,
				$this->getAantal(),
				$this->getBedrag(),
				$this->getOpmerkingen(),
				$this->getBezwaren(),
				$forceGeldigheid,
				$this->getFactuurperiodeStart(),
				$this->getFactuurperiodeEind(),
				$this->getNr()
			);
		} else {
			// SQL INSERT
			$this->_levfactuurregelnr = $BWDB->q("RETURNID INSERT INTO
				leverancierfactuurregel SET
				levfactuurnr=%i,
				EAN=%s,
				voorraadnr=%i,
				aantal=%s,
				bedrag=%s,
				opmerkingen=%s,
				bezwaren=%s,
				forceGeldigheid='N',
				factuurperiode_start=%s,
				factuurperiode_eind=%s",
				$this->getLeverancierFactuurNr(),
				$this->getEAN(),
				$voorraadnr,
				$this->getAantal(),
				$this->getBedrag(),
				$this->getOpmerkingen(),
				$this->getBezwaren(),
				$this->getFactuurperiodeStart(),
				$this->getFactuurperiodeEind()
			);
		}

		return true;
	}

	function setFactuurperiode($start, $eind)
	{
		if ($start){
			// Begin parsen
			$parsed = parseDate($start);
			if (!$parsed){
				parent::addError(new BookwebError("De opgegeven startdatum
				voor de factuurperiode ($start) is ongeldig"));
				return false;
			}
			$this->_factuurperiode_start = $parsed;
		} elseif ($start === null){
			$this->_factuurperiode_start = null;
		}

		if ($eind){
			// Eind parsen
			$parsed = parseDate($eind);
			if (!$parsed){
				parent::addError(new BookwebError("De opgegeven einddatum
				voor de factuurperiode ($start) is ongeldig"));
				return false;
			}

			$this->_factuurperiode_eind = $parsed;
		} elseif ($eind === null){
			$this->_factuurperiode_eind = null;
		}

		return true;
	}

	function getFactuurperiodeStart()
	{
		return $this->_factuurperiode_start;
	}

	function getFactuurperiodeEind()
	{
		return $this->_factuurperiode_eind;
	}

	function isValid()
	{
		$nr = $this->getNr();

		if ($nr >= 0){
			return LeverancierFactuurRegel::exists($nr);
		}
		return false;
	}

	/**
		Kijkt of het gegeven levfactuurregelnr in de database bestaat
		(static functie dus!)
	*/
	static function exists($levfactuurregelnr)
	{
		global $BWDB;

		$qres = $BWDB->q("VALUE SELECT COUNT(*) FROM leverancierfactuurregel
		WHERE levfactuurregelnr=%i", $levfactuurregelnr);

		return ($qres > 0);
	}


	function setValuesByArray($valuearray)
	{
		if (!$this->setLeverancierFactuurNr($valuearray["levfactuurnr"]))
			return false;

		if (!$this->setEAN($valuearray["EAN"]))
			return false;
		if (!$this->setVoorraadNr($valuearray["voorraadnr"]))
			return false;
		if (!$this->setAantal($valuearray["aantal"]))
			return false;
		if (!$this->setBedrag($valuearray["bedrag"]))
			return false;
		if (!$this->setOpmerkingen($valuearray["opmerkingen"]))
			return false;
		if (!$this->setBezwaren($valuearray["bezwaren"]))
			return false;
		if (!$this->setForceGeldigheid($valuearray["forceGeldigheid"]))
			return false;
		if (!$this->setFactuurperiode(
			$valuearray["factuurperiode_start"],
			$valuearray["factuurperiode_eind"]
		)) return false;

		if (isset($valuearray["levfactuurregelnr"]))
			$this->_levfactuurregelnr = $valuearray["levfactuurregelnr"];

		return true;
	}

	function setForceGeldigheid($force)
	{
		if ($force === true)	$force = "Y";
		if ($force === false)	$force = "N";
		if ($force === "on")	$force = "Y"; //checkbox

		if ($force === "Y" || $force === "N"){
			$this->_forceGeldigheid = $force;
			return true;
		} else {
			BookwebBase::addMessage(new BookwebError("De opgegeven
			parameter ($force) is niet geldig voor de functie
			setForceGeldigheid(bool)"));
			return false;
		}
	}

	function setLeverancierFactuur($leverancierfactuur)
	{
		if (!($leverancierfactuur instanceof LeverancierFactuur)){
			parent::addError(new BookwebError("Het opgegeven object is geen
			LeverancierFactuur-object"));
			return false;
		}
		return $this->setLeverancierFactuurNr($leverancierfactuur->getNr());
	}

	function setLeverancierFactuurNr($nr)
	{
		if (!is_numeric($nr) || $nr < 0){
			user_error("Je hebt een ongeldig nummer opgegeven: $nr", E_USER_ERROR);
		}
		$this->_levfactuurnr = $nr;
		return true;
	}

	function setISBN($isbn)
	{
		$isbn = trim($isbn);
		$isbn = str_replace("-", "", $isbn);
		$isbn = str_replace(" ", "", $isbn);

		if (!is_isbn($isbn)){
			parent::addError(new BookwebError("Het opgegeven ISBN ($isbn)
			is niet geldig!"));
			return false;
		}

		$EAN = isbn_to_EAN($isbn);

		return $this->setEAN($EAN);
	}

	function setEAN($EAN)
	{
		if (!is_EAN($EAN)) {
			parent::addError(new BookwebError("Het opgegeven
			EAN ($EAN) is niet geldig!"));
			return false;
		}
		$this->_EAN = $EAN;
		return true;
	}

	function getEAN()
	{
		return $this->_EAN;
	}

	function setVoorraadnr($nr)
	{
		if (!is_numeric($nr))
			$nr = -1;
		$this->_voorraadnr = $nr;
		return true;
	}

	function setAantal($aantal)
	{
		if (!is_numeric($aantal)) {
			parent::addError(new BookwebError("Het opgegeven aantal ($aantal)
			is ongeldig!"));
			return false;
		}

		$this->_aantal = $aantal;
		return true;
	}

	function setBedrag($bedrag)
	{
		if (!is_numeric($bedrag)) {
			BookwebBase::addError(new BookwebError("Het opgegeven bedrag
			($bedrag) is ongeldig!"));
			return false;
		}
		$this->_bedrag = $bedrag;
		return true;
	}

	function getForceGeldigheid()
	{
		$value = $this->_forceGeldigheid;
		if ($value === "Y")		return true;
		if ($value === true)	return true;
		if ($value === "N")		return false;
		if ($value === false)	return true;
		return false;
	}

	function getBedrag()
	{
		return $this->_bedrag;
	}

	function getTotaal()
	{
		return $this->getBedrag() * $this->getAantal();
	}

	/**
		Deze functie retourneert het verschil tussen het gefactureerde
		bedrag en de nettoverkopen van de gekoppelde voorraad.
		Uiteindelijk wordt geretourneert:
		verkochtbedrag - factuurbedrag

		Een negatief resultaat betekent dus dat er meer gefactureerd is dan
		verkocht en een positief resultaat betekent dat er te veel
		gefactureerd is.
	*/
	function getTotaalVerschil()
	{
		$voorraadnr = $this->getVoorraadnr();
		$factuur = new LeverancierFactuur($this->getFactuurnr());

		if (is_numeric($voorraadnr) && $voorraadnr >= 0) {
			$voorraad = new Voorraad($voorraadnr);
			$vNettoVerkopen = $this->getVoorraadNettoVerkopen();

			// Prijs van de gekoppelde voorraad opsnorren
			$vPrijs = $voorraad->getLevPrijs();

			// Totaal = prijs * hoeveelheid
			$vTotaalPrijs = ($vPrijs * $vNettoVerkopen);

			// Wat is er in totaal gefactureerd door de leverancier?
			$factuurBedrag = $this->getTotaal();

			// Verschil berekenen
			return ($vTotaalPrijs - $factuurBedrag);
		}
		return false;
	}

	function setOpmerkingen($opm)
	{
		if (!$opm)
			$opm = "";
		$this->_opmerkingen = $opm;
		return true;
	}

	function setBezwaren($bezw)
	{
		if (!$bezw)
			$bezw = "";
		$this->_bezwaren = $bezw;
		return true;
	}

	function getLeveringFactuurNr()
	{
		return $this->_levfactuurnr;
	}

	function getFactuurRegelNr()
	{
		return $this->_levfactuurregelnr;
	}

	function getFactuurNr()
	{
		return $this->_levfactuurnr;
	}

	function getFactuur()
	{
		$factuurnr = $this->getFactuurNr();
		return new LeverancierFactuur($factuurnr);
	}

	function getVoorraadNr()
	{
		return $this->_voorraadnr;
	}

	function getVoorraad()
	{
		return new Voorraad($this->getVoorraadNr());
	}

	function getAantal()
	{
		return $this->_aantal;
	}

	function getOpmerkingen()
	{
		return $this->_opmerkingen;
	}

	function getBezwaren()
	{
		return $this->_bezwaren;
	}

	function getNr()
	{
		return $this->_levfactuurregelnr;
	}

	function getLeverancierFactuurNr()
	{
		return $this->_levfactuurnr;
	}

	function getLeverancierFactuur()
	{
		$factuurnr = $this->getLeverancierFactuurNr();
		if ($factuurnr) {
			return LeverancierFactuur::get($factuurnr);
		}
		return null;
	}

	static function htmlTableHeader()
	{
			// Table header retourneren
			$res = "<tr>\n";
			$res .= "	<td><strong>Artikelnr</strong</td>\n";
			$res .= "	<td><strong>Titel</strong></td>\n";
			$res .= "	<td><strong>Auteur(s)</strong></td>\n";
			$res .= "	<td><strong>Druk</strong></td>\n";
			$res .= "	<td><strong>Aantal</strong></td>\n";
			$res .= "	<td><strong>Stukprijs</strong></td>\n";
			$res .= "	<td><strong>Totaal</strong></td>\n";
			$res .= "	<td><strong>Verschil&nbsp;*</strong></td>\n";
			$res .= "	<td width=\"16\">&nbsp;</td>\n"; // edit-knopje
			$res .= "	<td width=\"16\">&nbsp;</td>\n"; // delete-knopje
			$res .= "</tr>\n";
			return $res;
	}

	/**
		Retourneert een string met daarin een table row (incl <tr>-dingen)
		van deze factuurregel. Optionele parameters zijn:
		$editable is de regel bewerkbaar?
		$row_background wat moet de achtergrondkleur zijn van de rij?
		$alleenEANinfo moet alleen EAN info getoond worden? Anders ook
		gefactureerde aantallen, gefactureerde prijs en totaal
	*/
	function toHtmlTableRow($editable = false, $row_background = "white", $alleenEANinfo = false)
	{
		global $BWDB;

		$factuur = $this->getLeverancierFactuur();
		if ($factuur != null) {
			if ($factuur->isDefinitief() && $editable !== false) {
				parent::addError(new BookwebError("Deze factuurregel kan niet
				bewerkt worden, omdat de factuur de status
				&quot;definitief&quot; heeft!"));
				parent::printErrors();
				$editable = false;
			}
		}

		$voorraadnr = $this->getVoorraadNr();
		$EAN = $this->getEAN();
		$factuurregelnr = $this->getNr();

		if (is_EAN($EAN)) {
			$boekinfo = $BWDB->q("MAYBETUPLE SELECT * FROM boeken WHERE
			EAN=%s", $EAN);
		} else {
			$boekinfo["titel"] = "onbekend";
			$boekinfo["auteur"] = "onbekend";
			$boekinfo["druk"] = "onbekend";
		}

		$htmlbg = "style=\"background: $row_background\"";

		// Default values uit een parameter halen, als die er niet is, dan
		// de values uit het object gebruiken. Op deze manier krijgt de
		// gebruiker altijd zijn fouten weer terug te zien.
		$bedrag = tryPar("bedrag_$factuurregelnr", $this->getBedrag());
		$aantal = tryPar("aantal_$factuurregelnr", $this->getAantal());

		$res = "<tr>\n";

		$res .= "	<td $htmlbg valign=\"top\">";
		if (is_EAN($EAN)) {
			$res .= print_EAN_bi($EAN);
		} else {
			$res .= "$EAN";
		}
		$res .= "</td>\n";

		$res .= "	<td $htmlbg valign=\"top\">" . $boekinfo["titel"] . "</td>\n";
		$res .= "	<td $htmlbg valign=\"top\">" . $boekinfo["auteur"] . "</td>\n";
		$res .= "	<td $htmlbg valign=\"top\">" . $boekinfo["druk"] . "</td>\n";

		if ($alleenEANinfo) {
			$res .= "	<td $htmlbg colspan=\"3\">&nbsp;</td>\n";
			$res .= "</tr>\n";
			return $res;
		}

		// Gefactureerd aantal
		$res .= "	<td $htmlbg valign=\"top\">";
		if (!$editable) {
			$res .= $this->getAantal();
		} else {
			$res .=  addInput1("aantal_$factuurregelnr", $aantal, false, 2);
		}
		$res .= "	</td>\n";


		// Gefactureerd prijs per exemplaar
		$res .= "	<td $htmlbg valign=\"top\" align=\"right\">";
		if (!$editable) {
			$res .= BookwebBase::bedrag($this->getBedrag());
		} else {
			$res .= addInput1("bedrag_$factuurregelnr", $bedrag, false, 6);
		}
		$res .= "</td>\n";

		// Totaalbedrag van deze factuurregel (aantal * stukprijs)
		$res .= "	<td $htmlbg valign=\"top\" align=\"right\">";
		$res .= BookwebBase::bedrag($this->getTotaal());
		$res .= "	</td>\n";

		// Verschil tussen factuur en koppeling tonen
		$verschil = $this->getTotaalVerschil();
		if ($verschil === false) {
			// Verschil kon niet berekend worden. Misschien nog geen
			// koppeling?
			$verschil = "&nbsp;";
		} else {
			if (round($verschil,2) == 0.00) {
				$verschil = "&euro;&nbsp;0.00";
			} else {
				$verschil = BookwebBase::bedrag($verschil);
				$verschil = "<span style=\"color: red\">$verschil</span>";
			}
		}
		$res .= "	<td $htmlbg valign=\"top\" align=\"right\">$verschil</td>";
		$res .= "	<td valign=\"top\">";
		$levfact = LeverancierFactuur::get($this->getFactuurNr());
		if (!$levfact->isDefinitief() && !$editable) {
			$res .= "		<a href=\"" . $this->makeLinkEditMe() . "\" target=\"_self\">\n";
			$res .= "		<img src=\"/Layout/Images/Buttons/edit.png\" border=\"0\" />\n";
			$res .= "		</a>\n";
		} elseif (!$editable) {
			$res .= "		<img src=\"/Layout/Images/Buttons/edit_disabled.png\" border=\"0\" />\n";
		} else {
			$res .= "&nbsp;";
		}

		$res .= "	</td>\n";
		$res .= "	<td valign=\"top\">";

		if (!$levfact->isDefinitief() && !$editable) {
			$res .= "		<a href=\"" . $this->makeLinkDeleteMe() . "\" target=\"_self\">\n";
			$res .= "		<img src=\"/Layout/Images/Buttons/delete.png\" border=\"0\" />\n";
			$res .= "		</a>\n";
		} elseif (!$editable) {
			$res .= "		<img src=\"/Layout/Images/Buttons/delete_disabled.png\" border=\"0\" />\n";
		} else {
			$res .= "&nbsp;";
		}

		$res .= "	</td>\n";
		$res .= "</tr>\n";

		if ($this->getOpmerkingen()) {
			$opmerkingen = $this->getOpmerkingen();
			$opmerkingen = nl2br($opmerkingen);
			$res .= "<tr>\n";
			$res .= "	<td valign=\"top\">&nbsp;</td>\n";
			$res .= "	<td valign=\"top\" colspan=\"7\">\n";
			$res .= "		<div style=\"font-size: 8pt;\"><i>\n";
			$res .= "		" . $opmerkingen . "\n";
			$res .= "		</i></div>\n";
			$res .= "	</td>\n";
			$res .= "</tr>\n";
		}

		if (!$this->getForceGeldigheid()) {
			$geldigheidWarnings = $this->checkGeldigheid(true);
			if (sizeof($geldigheidWarnings) != 0) {
				if ($this->getBezwaren() != "") {
					$geldigheidWarnings[] = "Bezwaar: " . $this->getBezwaren();
				}
				foreach ($geldigheidWarnings as $warning) {
					$res .= "<tr>\n";
					$res .= "	<td bgcolor=\"$row_background\">&nbsp;</td>\n";
					$res .= "	<td colspan=\"7\" bgcolor=\"$row_background\" style=\"border:
					1px solid red\">\n";
					$res .= "		<div style=\"font-size: 8pt;\"><i>\n";
					$res .= "		$warning\n";
					$res .= "		</i></div>\n";
					$res .= "	</td>\n";
					$res .= "</tr>\n";
				}
			}
		} else {
			// Geforceerd geldig, waarschuwing tonen
			$res .= "<tr>\n";
			$res .= "	<td bgcolor=\"$row_background\">&nbsp;</td>\n";
			$res .= "	<td colspan=\"7\" bgcolor=\"$row_background\" style=\"border:
			1px solid red\">\n";
			$res .= "		<div style=\"font-size: 8pt;\"><i>\n";
			$res .= "		Deze factuurregel bevat fouten, maar is
			geforceerd geldig gemarkeerd\n";
			$res .= "		</i></div>\n";
			$res .= "	</td>\n";
			$res .= "</tr>\n";
		}

		return $res;
	}

	function deleteFromDatabase()
	{
		global $BWDB;

		if (!$this->isValid()) {
			parent::addError(new BookwebError("De opgegeven factuurregel
			staat niet in de database!"));
			return false;
		}

		$BWDB->q("DELETE FROM leverancierfactuurregel WHERE
		levfactuurregelnr=%i", $this->getNr());
		return true;
	}

	/**
		Deze functie controleert de geldigheid van deze FactuurRegel en
		zorgt ervoor dat alle problemen worden opgeslagen als
		BookwebWarnings in BookwebBase.
		Als de parameter (tenzij dat niet gewenst is, zie
		enige parameter). Als de parameter $returnwarnings true is, dan
		worden de warnings niet in BookwebBase opgeslagen, maar worden ze
		als array van strings geretourneerd. Als er dan geen warnings zijn,
		dan is de array leeg.
	**/
	function checkGeldigheid($returnwarnings = false)
	{
		$factuur = $this->getFactuur();
		$geldig = true;
		$warnings = array();
		if (trim($this->getBezwaren() != "")) {
			// Er zijn bezwaren ingevoerd!
			$msg = "Er zijn bezwaren tegen goedkeuring van deze
			factuurregel ingevoerd";
			if (!$returnwarnings) {
				BookwebBase::addMessage(new BookwebWarning($msg));
			} else {
				$warnings[] = $msg;
			}
			$geldig = false;
		}

		if (!$this->getForceGeldigheid()) {
			// deze factuurregel is niet geforceerd geldig gemaakt
			if ($this->getVoorraadnr() >= 0) {
				// Er is een voorraad gekoppeld!
				$voorraad = new Voorraad($this->getVoorraadnr());
				if (BookwebBase::hasNewErrors())
					BookwebBase::printErrors();

				$periodeStart = $this->getFactuurperiodeStart();
				$periodeEind = $this->getFactuurperiodeEind();
				if ($periodeStart == null || $periodeEind == null) {
					$periodeStart = $factuur->getFactuurperiodeStart();
					$periodeEind = $factuur->getFactuurperiodeEind();
				}

				// Zijn er meer boeken gefactureerd dan er verkocht zijn?
				$nettoVerkopen = $voorraad->getNettoVerkopen(
					$periodeStart, $periodeEind);

				if ($this->getAantal() > $nettoVerkopen) {
					$msg = "Het aantal gefactureerde boeken (" .
					$this->getAantal() . ") is hoger dan het
					aantal dat in BookWeb staat ($nettoVerkopen)!";

					if (!$returnwarnings) {
						BookwebBase::addMessage(new BookwebWarning($msg));
					} else {
						$warnings[] = $msg;
					}
					$geldig = false;
				}

				// Klopt de prijs van de koppeling?
				if ($this->getBedrag() != $voorraad->getLevPrijs()) {
					$msg = "Er is een verschil tussen de prijs in bookweb (" .
					BookwebBase::bedrag($voorraad->getLevPrijs()) . ") en
					het gefactureerde bedrag (" .
					BookwebBase::bedrag($this->getBedrag()) . ")!";

					if (!$returnwarnings) {
						BookwebBase::addMessage(new BookwebWarning($msg));
					} else {
						$warnings[] = $msg;
					}

					$geldig = false;
				}
			} else {
				// Geen koppeling
				$msg = "Deze factuurregel is niet aan een voorraad gekoppeld!";

				if (!$returnwarnings){
					BookwebBase::addMessage(new BookwebWarning($msg));
				} else {
					$warnings[] = $msg;
				}

				$geldig = false;
			}
		} else {
			/*
			$msg = "Deze factuurregel is geforceerd geldig verklaard!";
			if (!$returnwarnings){
				BookwebBase::addMessage(new BookwebWarning($msg));
			} else {
				$warnings[] = $msg;
			}*/
			$geldig = true;
		}

		if (!$returnwarnings) {
			return $geldig;
		}
		return $warnings;
	}

	/**
		Deze functie print een form waarin de gebruiker deze factuurregel
		kan wijzigen. Dit gaat om geavanceerde wijzigingen, waarbij
		bijvoorbeeld ook voorraadnrs gekoppeld kunnen worden en opmerkingen
		toegevoegd kunnen worden
	**/
	function printFormEdit()
	{
		global $BWDB;

		$geldig = $this->checkGeldigheid();
		if (!$geldig) {
			if (!$this->getForceGeldigheid()) {
				BookwebBase::addMessage(new BookwebError("Deze factuurregel
				kan niet als &quot;geldig&quot; bestempeld worden, omdat er
				inconsistenties zijn (zie hieronder)"));
			} else {
				BookwebBase::addMessage(new BookwebWarning("Deze
				factuurregel is geforceerd &quot;geldig&quot;"));
			}
		}

		parent::printMessages();

		echo addForm(parent::makeUrl(array(
			"page"=>"financieel",
			"fin_action"=>"facturen",
			"fin_subaction"=>"processeditfactuurregel",
			"levfactuurregelnr"=>$this->getNr())), "POST");

		$ean = tryPar("ean", $this->getEAN());
		$voorraadnr = tryPar("voorraadnr", $this->getVoorraadNr());
		$aantal = tryPar("aantal", $this->getAantal());
		$bedrag = tryPar("bedrag", $this->getBedrag());
		$opmerkingen = tryPar("opmerkingen", $this->getOpmerkingen());
		$bezwaren = tryPar("bezwaren", $this->getBezwaren());
		$forceGeldigheid = tryPar("forceGeldigheid",$this->getForceGeldigheid());
		$factuurperiode_start = tryPar("factuurperiode_start",
		$this->getFactuurperiodeStart());
		$factuurperiode_eind = tryPar("factuurperiode_eind",
		$this->getFactuurperiodeEind());

		echo "<br/><table>\n";
		echo "	<tr>\n";
		echo "		<td colspan=\"2\" align=\"center\"
		style=\"border-bottom: 1px solid black\">\n";
		echo "			Algemene informatie\n";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td><strong>Artikel:</strong></td>\n";
		echo "		<td>";
		echo addInput1("ean", print_ean($ean));
		echo "		</td>";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td><strong>Aantal:</strong></td>\n";
		echo "		<td>" . addInput1("aantal", $aantal, false, 3) . "</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td><strong>Bedrag:</strong></td>\n";
		echo "		<td>" . addInput1("bedrag", $bedrag, false, 6) . "</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td valign=\"top\"><strong>Opmerkingen:</strong></td>\n";
		echo "		<td>" . addTextArea("opmerkingen", $opmerkingen, 60, 5) . "</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td valign=\"top\"><strong>Bezwaren:</strong></td>\n";
		echo "		<td>" . addTextArea("bezwaren", $bezwaren, 60, 5) . "</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td valign=\"top\"><strong>Forceer geldigheid:</strong></td>\n";
		echo "		<td valign=\"top\">\n";
		echo "			" . addCheckBox("forceGeldigheid", $forceGeldigheid);
		echo "			(Let op! Inconsistenties worden dan genegeerd!)";
		echo "		</td>\n";
		echo "	</tr>\n";

		// Factuurperiode?
		$factuur = $this->getFactuur();
		echo "	<tr>\n";
		echo "		<td valign=\"top\"><strong>Factuurperiode:</strong></td>\n";
		echo "		<td>" .
			addInput1("factuurperiode_start", $factuurperiode_start) .
			" tot " .
			addInput1("factuurperiode_eind", $factuurperiode_eind) .
			"<br/>\n" .
			"Als je geen factuurperiode invult, wordt die van de factuur
			overgenomen (" . $factuur->getFactuurperiodeStart() . " tot " .
			$factuur->getFactuurperiodeEind() . ")" .
			"</td>\n";

		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td colspan=\"2\" style=\"border-bottom: 1px solid
		black\">\n";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "</table>\n";
		echo "<br/><br/>\n";

		echo "<h3>Voorraadkoppeling:</h3>\n";
		echo "Hieronder zie je een lijst met titels, prijzen en
		leveranciers. Iedere combinatie EAN/prijs/leverancier wordt
		aangeduid als een aparte voorraad. Aan een factuurregel kan een
		voorraad gekoppeld worden.<br/>\n";
		echo "De lijst hieronder bevat voorraden die mogelijk gerelateerd
		zijn aan deze factuurregel, in volgorde van waarschijnlijkheid.
		Selecteer een voorraad om die te koppelen aan deze
		factuurregel.<br/>\n";
		$voorraden = $this->zoekKandidaatVoorraden();

		echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
		echo "	<tr>\n";
		echo "		<th>&nbsp;</td>\n";
		echo "		<th>Artikelnr.</th>\n";
		echo "		<th>Titel en auteur</th>\n";
		echo "		<th>Leverancier</th>\n";
		echo "		<th>Leverprijs (adviesprijs)</th>\n";
		echo "		<th>Verkocht in fact. periode</th>\n";
		echo "	</tr>\n";

		// Array met voorraadnrs: gaat later in hidden field
		$voorraadnrs = array();
		foreach ($voorraden as $voorraad)
		{
			$EAN = $voorraad->getEAN();
			$voorraadnr = $voorraad->getNr();
			$artikelinfo = $BWDB->q("MAYBETUPLE SELECT * FROM boeken WHERE
			EAN=%s",$EAN);

			echo "<tr>\n";

			// Radiobox
			echo "	<td>\n";
			$checked = ($voorraad->getNr() === $this->getVoorraadNr());
			echo "		" . addRadioBox("voorraadnr", $checked,	$voorraadnr) . "\n";
			echo "	</td>\n";

			// ISBN of artikelcode met link
			echo "	<td>";
			echo print_EAN_bi($EAN);
			echo "</td>\n";

			// Titel en auteur in 1 TD
			echo "	<td>";
			echo "		" . $artikelinfo["titel"] . ",<br/>";
			echo "		" . $artikelinfo["auteur"];
			echo "	</td>\n";

			// Leverancier van deze voorraad
			echo "	<td>";
			echo getLeverancierNaam($voorraad->getLeverancierNr());
			echo "	</td>\n";

			// Leverprijs met adviesprijs tussen haakjes
			$vLevPrijs = $voorraad->getLevPrijs();
			echo "	<td>";
			if ($vLevPrijs != $this->getBedrag()) {
				echo "<span style=\"color: red\">$vLevPrijs</span>";
			} else {
				echo $vLevPrijs;
			}
			echo " (" . $voorraad->getAdviesPrijs() . ")";
			echo "	</td>\n";


			$fact_start = $this->getFactuurperiodeStart();
			$fact_eind = $this->getFactuurperiodeEind();
			if (!$fact_start || !$fact_eind) {
				$fact_start = $factuur->getFactuurperiodeStart();
				$fact_eind = $factuur->getFactuurperiodeEind();
			}

			$aantalverkopen = $voorraad->getVerkopen($fact_start, $fact_eind);
			if ($aantalverkopen) $aantalverkopen = array_sum($aantalverkopen);
			if (!$aantalverkopen) $aantalverkopen = 0;

			$aantalterugkopen = $voorraad->getTerugkopen($fact_start, $fact_eind);
			if ($aantalterugkopen)
				$aantalterugkopen = array_sum($aantalterugkopen);
			if (!$aantalterugkopen)
				$aantalterugkopen = 0;

			$netto = $aantalverkopen - $aantalterugkopen;

			// Berekening van het aantal verkopen in de factuurperiode
			echo "	<td>\n";
			echo "		$aantalverkopen - $aantalterugkopen = ";
			if ($netto != $this->getAantal()) {
				echo "<span style=\"color: red\">$netto</span>";
			} else {
				echo $netto;
			}
			echo "	</td>\n";
			echo "</tr>\n";


			// Een array samenstellen van alle voorraadnrs, zodat die later
			// in een hidden field neergezet kunnen worden
			$voorraadnrs[] = $voorraadnr;
		}
		echo "<tr>\n";
		echo "	<td>\n";
		echo "		" . addRadioBox("voorraadnr", ($this->getVoorraadnr() <
		0), '') .	"\n";
		echo "	</td>\n";
		echo "	<td colspan=\"3\">(geen koppeling)</td>\n";
		echo "</tr>\n";
		echo "</table><br/>\n";

		$voorraadnrs = implode(",", $voorraadnrs);
		echo addHidden("voorraadnrs", $voorraadnrs);
		echo addFormEnd("Wijzigingen opslaan", false);

		echo "<br/>\n";

		$factuur = $this->getFactuur();
		echo "(klik ";
		echo "<a href=\"" . parent::makeUrl(array(
			"page"=>"financieel",
			"fin_action"=>"facturen",
			"fin_subaction"=>"show",
			"levfactuurnr"=>$factuur->getNr())) . "\" target=\"_self\">";
		echo "hier";
		echo "</a>";
		echo " om terug te gaan naar het factuuroverzicht zonder de
		wijzigingen op te slaan)";
	}

	/**
		Deze functie retourneert het aantal verkopen geregistreerd in
		BookWeb van de aangekoppelde voorraad.
		Let op! Op het moment dat deze LeverancierFactuurRegel een eigen
		factuurdatum_start en factuurdatum_eind heeft, dan wordt die
		gebruikt. Als die velden niet gevuld zijn (in de meeste gevallen
		zo...), dan wordt die informatie van de LeverancierFactuur
		gebruikt.
	*/
	function getVoorraadAantalVerkopen()
	{
			$begin = $this->getFactuurperiodeStart();
			$eind = $this->getFactuurperiodeEind();

			if ($begin == null || $eind == null) {
				$factuur = $this->getFactuur();
				$begin = $factuur->getFactuurperiodeStart();
				$eind = $factuur->getFactuurperiodeEind();
			}
			$voorraad = $this->getVoorraad();

			$verkopen =	$voorraad->getVerkopen($begin, $eind);

			if ($verkopen)
				return array_sum($verkopen);

			return 0;
	}

	// Zie getVoorraadAantalVerkopen
	function getVoorraadAantalTerugkopen()
	{
			$begin = $this->getFactuurperiodeStart();
			$eind = $this->getFactuurperiodeEind();

			if ($begin == null || $eind == null) {
				$factuur = $this->getFactuur();
				$begin = $factuur->getFactuurperiodeStart();
				$eind = $factuur->getFactuurperiodeEind();
			}
			$voorraad = $this->getVoorraad();

			$terugkopen = $voorraad->getTerugkopen($begin, $eind);

			if ($terugkopen)
				return array_sum($terugkopen);

			return 0;
	}

	// Zie getVoorraadAantalVerkopen en getVoorraadAantalTerugkopen
	function getVoorraadNettoVerkopen()
	{
		return $this->getVoorraadAantalVerkopen()
					- $this->getVoorraadAantalTerugkopen();
	}
	/**
		Deze functie zoekt Voorraad-objecten die mogelijk gekoppeld
		kunnen worden aan deze factuurregel. Als er al een
		voorraadnr aan deze FactuurRegel is gekoppeld, dan wordt
		die sowieso geretourneerd.
		Er wordt een array van mogelijke Voorraad-objecten
		geretourneerd, die slim gesorteerd wordt (zie commentaar in de
		functie)
	*/
	function zoekKandidaatVoorraden()
	{
		// TODO uitgebreide zoekmogelijkheden maken (zie
		// functieomschrijving)

		// Alle voorraden opzoeken met dit EAN ('false' betekent dat
		// ook voorraden die op dit moment leeg zijn worden
		// geretourneerd)
		$voorraden = Voorraad::searchByEAN($this->getEAN(),false);

		// Nuttig sorteren: op basis van een slim puntensysteem. De array
		// krijgt een extra key met daarin de "score". Hoe hoger de score,
		// hoe hoger in de lijst. Hoe lager de score, hoe lager in de
		// lijst.
		// - > 0 verkocht: +1
		// - 0 verkocht -1
		// - Juiste leverancier +1
		// - Onjuiste leverancier -2
		// - Juiste prijs + 1
		// - Onjuiste prijs -2
		$scoreVoorraden = array();
		$factuur = $this->getFactuur();

		// Begin en eind factuurperiode vaststellen. Deze kan vastgesteld
		// zijn in deze factuurregel, of in de factuur.
		$fact_start = $this->getFactuurperiodeStart();
		$fact_eind = $this->getFactuurperiodeEind();
		if (!$fact_start || !$fact_eind) {
			$fact_start = $factuur->getFactuurperiodeStart();
			$fact_eind = $factuur->getFactuurperiodeEind();
		}

		foreach ($voorraden as $voorraad)
		{
			$score = 0;

			$verkopen =	$voorraad->getVerkopen($fact_start, $fact_eind);
			if ($verkopen) $verkopen = array_sum($verkopen);
			if (!$verkopen) $verkopen = 0;

			$terugkopen = $voorraad->getTerugkopen($fact_start, $fact_eind);
			if ($terugkopen) $terugkopen = array_sum($terugkopen);
			if (!$terugkopen) $terugkopen = 0;

			$netto = ($verkopen - $terugkopen);

			if ($netto > 0){
				$score = $score + 1;
			} else {
				$score = $score - 1;
			}

			if ($voorraad->getLeverancierNr() == $factuur->getLeverancierNr()){
				$score = $score + 1;
			} else {
				$score = $score - 2;
			}

			$voorraadPrijs = $voorraad->getLevPrijs() +	$voorraad->getAdviesPrijs();

			if ($voorraadPrijs == $this->getBedrag()){
				$score = $score + 1;
			} else {
				$score = $score - 2;
			}

			$scoreVoorraden[$score][] = $voorraad;
		}

		// Array omgekeerd sorteren (hoge score eerst, lage laatst)
		krsort($scoreVoorraden);

		// Even omzetten naar simpele array
		$sortedVoorraden = array();
		foreach ($scoreVoorraden as $score=>$voorraden){
			foreach ($voorraden as $voorraad){
				$sortedVoorraden[] = $voorraad;
			}
		}

		return $sortedVoorraden;

	}

	/**	STATIC
		Retourneert een array met LeverancierFactuurRegel-objecten
		die gerelateerd zijn aan het opgegeven Voorraad-object: dat
		wil zeggen dat de gebruiker een LeverancierFactuurRegel
		daaraan gekoppeld heeft.
		Standaard worden er alleen FactuurRegels geretourneerd die
		onderdeel uitmaken van een definieve factuur, maar evt. kan
		anders opgegeven worden.
	*/
	static function searchByVoorraad($voorraad, $definitieveFacturen = true
						, $conceptFacturen = false, $ongeldigeFacturen = false
						, $begindatum = "", $einddatum = "")
	{
		global $BWDB;

		if (!$voorraad instanceof Voorraad) {
			parent::addError(new BookwebError("Het opgegeven
			object is geen geldig Voorraad-object"));
			return false;
		}

		$res = array();
		if (!$begindatum && !$einddatum){
			$factuurregelnrs = $BWDB->q("
				COLUMN SELECT leverancierfactuurregel.levfactuurregelnr
				FROM leverancierfactuurregel
				WHERE voorraadnr=%i",
				$voorraad->getNr()
			);
		} else {
			// datumselectie actief
			// TODO QUERY FIXEN, werkt nu niet goed?
			$factuurregelnrs = $BWDB->q("
				COLUMN SELECT leverancierfactuurregel.levfactuurregelnr
				FROM leverancierfactuurregel
				LEFT JOIN leverancierfactuur USING(levfactuurnr)
				WHERE voorraadnr=%i",
/*				AND (
					leverancierfactuur.factuurperiode_start >= %s OR (
						leverancierfactuurregel.factuurperiode_start IS NOT NULL
						AND leverancierfactuurregel.factuurperiode_start >= %s
					)
				)
				AND (
					leverancierfactuur.factuurperiode_eind <= %s OR (
						leverancierfactuurregel.factuurperiode_eind IS NOT NULL
						AND leverancierfactuurregel.factuurperiode_eind <= %s
					)
				)",*/
				$voorraad->getNr(), $begindatum, $begindatum, $einddatum, $einddatum
			);
		}

		foreach ($factuurregelnrs as $factuurregelnr)
		{
			$factuurregel = new LeverancierFactuurRegel($factuurregelnr);
			$factuur = $factuurregel->getFactuur();

			if ($factuur->getStatus() == "definitief" &&
			$definitieveFacturen) $res[] = $factuurregel;

			if ($factuur->getStatus() == "concept" &&
			$conceptFacturen) $res[] = $factuurregel;

			if ($factuur->getStatus() == "ongeldig" &&
			$ongeldigeFacturen) $res[] = $factuurregel;
		}

		return $res;
	}

	function processEditFactuurRegel(){
		$ean = requireEAN("ean");
		$aantal = tryPar("aantal");
		$bedrag = tryPar("bedrag");
		$opmerkingen = tryPar("opmerkingen");
		$bezwaren = tryPar("bezwaren");
		$forceGeldigheid = tryPar("forceGeldigheid");
		$voorraadnr = tryPar("voorraadnr");
		$factuurperiode_start = tryPar("factuurperiode_start");
		$factuurperiode_eind = tryPar("factuurperiode_eind");

		if (!is_numeric($voorraadnr)) $voorraadnr = null;
		if (!$forceGeldigheid) $forceGeldigheid = "N";

		if ($this->setValuesByArray(
			array(
				"levfactuurnr"=>$this->getFactuurNr(),
				"EAN"=>$ean,
				"voorraadnr"=>$voorraadnr,
				"aantal"=>$aantal,
				"bedrag"=>$bedrag,
				"opmerkingen"=>$opmerkingen,
				"bezwaren"=>$bezwaren,
				"forceGeldigheid"=>$forceGeldigheid,
				"factuurperiode_start"=>$factuurperiode_start,
				"factuurperiode_eind"=>$factuurperiode_eind
			))){

			// Succesvol bijgewerkt
			$this->store();
			return true;
		}
		return false;

	}

	/*
		Retourneert een link waarmee deze factuurregel bewerkt kan worden
	*/
	function makeLinkEditMe($caption = "")
	{
		$url = BookwebBase::makeUrl(array(
			"page"=>"financieel",
			"fin_action"=>"facturen",
			"fin_subaction"=>"editfactuurregel",
			"levfactuurregelnr"=>$this->getNr()));

		if ($caption) return "<a href=\"$url\">$caption</a>";
		return $url;
	}

	function makeLinkDeleteMe($caption = '')
	{
		$url = BookwebBase::makeUrl(array(
			"page"=>"financieel",
			"fin_action"=>"facturen",
			"fin_subaction"=>"deletefactuurregel",
			"levfactuurregelnr"=>$this->getNr()));

		if ($caption) return "<a href=\"$url\">$caption</a>";
		return $url;
	}

	static function getFactuurRegelsByDate($van, $tot
											, $alleenBetaaldeFacturen = true)
	{
		global $BWDB;

		$nrs = $BWDB->q("
			COLUMN SELECT leverancierfactuurregel.levfactuurregelnr
			FROM leverancierfactuurregel
			LEFT JOIN leverancierfactuur USING (levfactuurnr)
			WHERE IFNULL(leverancierfactuurregel.factuurperiode_start, leverancierfactuur.factuurperiode_start) >= %s
			AND IFNULL(leverancierfactuurregel.factuurperiode_eind, leverancierfactuur.factuurperiode_eind) < %s" .
			($alleenBetaaldeFacturen ? " AND leverancierfactuur.betaaldatum IS NOT NULL" : ""),
			$van, $tot);

		return LeverancierFactuurRegel::get($nrs);
	}
}

