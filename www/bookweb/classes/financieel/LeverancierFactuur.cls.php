<?php

class LeverancierFactuur extends BookwebBase {
	var $_levfactuurnr;
	var $_factuurdatum;
	var $_betaaldatum;
	var $_factuurperiode_start;
	var $_factuurperiode_eind;
	var $_leveranciernr;
	var $_opmerkingen;
	var $_status;

	function __construct($levfactuurnr = null){
		if (is_numeric($levfactuurnr)){
			$this->_levfactuurnr = $levfactuurnr;
			$this->update();
		} // else: nieuwe factuur
	}

	static function get($levfactuurnr){
		static $cache;
		if (!$cache) $cache = array();

		if ( !isset($cache[$levfactuurnr]) ) {
			$cache[$levfactuurnr] = new LeverancierFactuur($levfactuurnr);
		}
		return $cache[$levfactuurnr];
	}

	/**
		Verwijdert dit object uit de database
	*/
	function deleteFromDatabase(){
		$id = $this->getNr();
		if (is_numeric($id) && $id >= 0){
			global $BWDB;

			$BWDB->q("DELETE FROM leverancierfactuurregel WHERE
			levfactuurnr=%i", $id);
			$BWDB->q("DELETE FROM leverancierfactuur WHERE
			levfactuurnr=%i", $id);

			return true;
		} else {
			BookwebBase::addError(new BookwebError("De primary key van het
			LeverancierFactuur-object dat verwijderd moet worden ($id) is
			ongeldig."));
		}

		return false;
	}

	/**
		Het object bijwerken met informatie uit de database
	**/
	function update(){
		global $BWDB;

		if (!is_numeric($this->getNr())) return false;


		$info = $BWDB->q("MAYBETUPLE SELECT * FROM leverancierfactuur WHERE
		levfactuurnr=%i", $this->getNr());

		if (!$info){
			BookwebBase::addError(new BookwebError("De opgegeven primary
			key voor LeverancierFactuur-objecten (" . $this->getNr() . ")
			bestaat niet in de database. De functie
			LeverancierFactuur->update() kan niet uitgevoerd worden."));
			return false;
		} else {
			return $this->setValuesByArray($info);
		}
	}

	/**
		Deze functie dupliceert deze LeverancierFactuur en alle gekoppelde
		LeverancierFactuurRegels en slaat alles opnieuw op in de database.
		Dus ook alle LeverancierFactuurRegels worden gedupliceerd en
		opgeslagen!
		Deze functie retourneert niets, maar modificeert "this" op zo'n
		manier dat het de nieuwe leverancierfactuurnr krijgt
	*/
	function duplicateAndStore(){
		// Eerst alle gekoppelde factuurregels opvragen, voordat het id
		// van dit object verandert.
		$factuurregels = $this->getFactuurregels();

		// Doen alsof ie niet opgeslagen is!
		$this->_levfactuurnr = -1;
		$this->store();

		// Nu is er een nieuwe ID (nr) toegewezen, alle factuurregels
		// dupliceren en veranderen
		foreach ($factuurregels as $factuurregel){
			// dupliceren
			$factuurregel->duplicateAndStore();

			// Nieuwe "parent" factuur instellen
			$factuurregel->setLeverancierFactuurnr($this->getNr());

			// en weer opslaan
			$factuurregel->store();
		}

		// Klaar!
	}

	function store(){
		global $BWDB;

		if (is_numeric($this->getNr()) && $this->getNr() >= 0){
			// SQL UPDATE
			$BWDB->q("UPDATE leverancierfactuur SET
				factuurdatum=%s,
				betaaldatum=%s,
				factuurperiode_start=%s,
				factuurperiode_eind=%s,
				leveranciernr=%i,
				opmerkingen=%s,
				status=%s
				WHERE levfactuurnr=%i",
				$this->getFactuurdatum(),
				$this->getBetaaldatum(),
				$this->getFactuurperiodeStart(),
				$this->getFactuurperiodeEind(),
				$this->getLeverancierNr(),
				$this->getOpmerkingen(),
				$this->getStatus(),
				$this->getNr()
			);

		} else {
			// SQL INSERT
			$this->_levfactuurnr = $BWDB->q("RETURNID INSERT INTO leverancierfactuur SET
				factuurdatum=%s,
				betaaldatum=%s,
				factuurperiode_start=%s,
				factuurperiode_eind=%s,
				leveranciernr=%i,
				opmerkingen=%s,
				status=%s",
				$this->getFactuurdatum(),
				$this->getBetaaldatum(),
				$this->getFactuurperiodeStart(),
				$this->getFactuurperiodeEind(),
				$this->getLeverancierNr(),
				$this->getOpmerkingen(),
				$this->getStatus()
			);
		}

		return true;
	}
	/*
		Deze functie wil graag een array met de keys

		levfactuurnr, factuurdatum, factuurperiode_start,
		factuurperiode_eind, leveranciernr, opmerkingen, status

		en stelt de private variabelen in.
	*/
	function setValuesByArray($infoarray){
		if (!$this->setFactuurdatum($infoarray["factuurdatum"])) return false;

		if (!$this->setBetaaldatum($infoarray["betaaldatum"])) return false;

		if (!$this->setFactuurPeriode($infoarray["factuurperiode_start"],
			$infoarray["factuurperiode_eind"])) return false;

		if (!$this->setLeverancierNr($infoarray["leveranciernr"])) return false;

		if (!$this->setOpmerkingen($infoarray["opmerkingen"])) return false;

		if (!$this->setStatus($infoarray["status"])) return false;

		if (isset($infoarray["levfactuurnr"])) $this->_levfactuurnr = $infoarray["levfactuurnr"];

		return true;
	}

	function setFactuurdatum($facdatum){
		// Factuurdatum van 0000-00-00 mag ook wel...
		if ($facdatum != "0000-00-00"){
			$parsed = parseDate($facdatum);
		} else {
			$parsed = $facdatum;
		}

		if (!$parsed){
			BookwebBase::addError(new BookwebError("De opgegeven factuurdatum
			($facdatum) is ongeldig!"));

			return false;
		} else {
			$this->_factuurdatum = $parsed;
		}

		return true;
	}

	function setBetaaldatum($datum){
		if ($datum == null){
			$this->_betaaldatum = null;
		} else {
			$parsed = parseDate($datum);
			if (!$parsed){
				BookwebBase::addError(new BookwebError("De opgegeven
				betaaldatum ($datum) is ongeldig!"));
				return false;
			} else {
				$this->_betaaldatum = $parsed;
			}
		}

		return true;
	}

	function setFactuurPeriode($start, $eind){
		if ($start){
			// Begin parsen
			$parsed = parseDate($start);
			if (!$parsed){
				BookwebBase::addError(new BookwebError("De opgegeven startdatum
				voor de factuurperiode ($start) is ongeldig"));
				return false;
			}
			$this->_factuurperiode_start = $parsed;
		}

		if ($eind){
			// Eind parsen
			$parsed = parseDate($eind);
			if (!$parsed){
				BookwebBase::addError(new BookwebError("De opgegeven einddatum
				voor de factuurperiode ($start) is ongeldig"));
				return false;
			}

			$this->_factuurperiode_eind = $parsed;
		}

		return true;
	}

	function setLeverancierNr($nr){
		if (!is_numeric($nr)) $nr = null;

		$this->_leveranciernr = $nr;

		return true;
	}

	function setOpmerkingen($opm){
		if (!$opm) $opm = "";

		$this->_opmerkingen = $opm;

		return true;
	}

	function setStatus($status){
		$status = trim($status);
		if (!$status) return false;

		if ($status != "concept" && $status != "definitief" && $status !=
			"ongeldig"){
			BookwebBase::addError(new BookwebError("De opgegeven status van de
			factuur ($status) is ongeldig!"));
			return false;
		} else {
			$this->_status = $status;
		}

		return true;
	}

	function getNr(){
		return $this->_levfactuurnr;
	}

	function getFactuurdatum(){
		return $this->_factuurdatum;
	}

	function getBetaaldatum(){
		return $this->_betaaldatum;
	}

	function getFactuurperiodeStart(){
		return $this->_factuurperiode_start;
	}

	function getFactuurperiodeEind(){
		return $this->_factuurperiode_eind;
	}

	function getLeverancierNr(){
		return $this->_leveranciernr;
	}

	function getOpmerkingen(){
		return $this->_opmerkingen;
	}

	function getStatus(){
		return $this->_status;
	}

	/**
		Geeft een mooie HTML-representatie van dit Factuur-object. Gekozen
		kan worden of er factuurregels (specificatie) getoond moet worden
		en of de informatie bewerkt moet kunnen worden
	**/
	function toHtmlString($toonFactuurRegels = true, $editFactuurInfo = false, $editFactuurRegels = false){
		global $BWDB;
		$this->update();

		echo "<h3>";
		if ($editFactuurInfo){
			if ($this->getNr() < 0){
				echo "Nieuwe factuur invoeren";
			} else {
				echo "Factuur bewerken";
			}
		} else {
			echo "Factuurinformatie";
		}
		echo "</h3>\n";

		if ($editFactuurInfo){
			$factuurDatum = parseDate(tryPar("factuurdatum", $this->getFactuurDatum()));
			$factuurLeverancierNr = tryPar("leveranciernr", $this->getLeverancierNr());
			$factuurPeriodeStart = parseDate(tryPar("factuurperiode_start", $this->getFactuurPeriodeStart()));
			$factuurPeriodeEind = parseDate(tryPar("factuurperiode_eind", $this->getFactuurPeriodeEind()));
			$factuurStatus = tryPar("status", $this->getStatus());
			$factuurOpmerkingen = tryPar("opmerkingen", $this->getOpmerkingen());
			$factuurBetaaldatum = tryPar("betaaldatum",	$this->getBetaaldatum());

			echo addForm("?page=financieel&fin_action=facturen&fin_subaction=processedit", "POST");
			echo addHidden("levfactuurnr", $this->getNr());
		}

		echo "<table>\n";
		echo "	<tr>\n";
		echo "		<td width=\"150\">\n";
		echo "			<strong>Factuurdatum:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			echo addInput1("factuurdatum", $factuurDatum);
		} else {
			echo $this->getFactuurdatum();
		}

		echo "		</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			<strong>Datum betaling:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			echo addInput1("betaaldatum", $factuurBetaaldatum);
		} else {
			$datum = $this->getBetaaldatum();
			if ($datum == null){
				echo "(nog niet betaald)";
			} else {
				echo $datum;
			}
		}

		echo "		</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td valign=\"top\">\n";
		echo "			<strong>Factuurperiode:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			echo addInput1("factuurperiode_start", $factuurPeriodeStart);
		} else {
			echo $this->getFactuurperiodeStart();
		}
		echo " tot ";
		if ($editFactuurInfo){
			echo addInput1("factuurperiode_eind", $factuurPeriodeEind);
		} else {
			echo $this->getFactuurperiodeEind();
		}
		echo "<br/><font size=\"1\">Let op! De datum &quot;tot&quot; is
		<em>niet &quot;tot en met&quot;</em>!</font>";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			<strong>Leverancier:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			echo getLeveranciersSelect($factuurLeverancierNr);
		} else {
			echo getLeverancierNaam($this->getLeverancierNr());
		}

		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			<strong>Status:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			$this->printStatusSelector("status");
		} else {
			echo $this->getStatus();
		}

		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td valign=\"top\">\n";
		echo "			<strong>Opmerkingen:</strong>\n";
		echo "		</td>\n";
		echo "		<td>\n";

		if ($editFactuurInfo){
			echo addTextArea("opmerkingen", $factuurOpmerkingen, 60, 5);
		} else {
			echo nl2br($this->getOpmerkingen());
		}

		echo "		</td>\n";
		echo "	</tr>\n";
		echo "</table>\n";

		if (!$editFactuurInfo){
			if (!$this->isDefinitief()){
				echo "<a href=\"" . $this->makeLink("edit") .
				"&levfactuurnr=" . $this->getNr() . "\"
				target=\"_self\">(wijzigen)</a><br/>\n";

				echo "<a href=\"" . $this->makeLink("delete") .
				"&levfactuurnr=" . $this->getNr() . "\"
				target=\"_self\">(verwijderen)</a><br/>\n";

				echo "<a href=\"" . $this->makeLink("invalidateduplicate") .
				"&levfactuurnr=" . $this->getNr() . "\"
				target=\"_self\">(ongeldig verklaren en dupliceren)</a><br/>\n";

				echo "<a href=\"" . $this->makeLink("trivialiteiten") .
				"&levfactuurnr=" . $this->getNr() . "\"
				target=\"_self\">(trivialiteiten invullen)</a>\n";
			}
		} else {
			if ($this->getNr() >= 0){
				echo "<input type=\"submit\" value=\"Wijzigingen opslaan\" />\n";
			} else {
				echo "<input type=\"submit\" value=\"Factuur opslaan\" />\n";
			}
			echo "</form>\n";
		}


		if (!$toonFactuurRegels) return;
		if ($this->getNr() < 0) return;

		echo "<hr/>\n";

		echo "<h3>Specificatie:</h3>\n";

		echo "<div style=\"font-size: 10pt;\">\n";
		$factuurregels = $this->getFactuurRegels();

		if (sizeof($factuurregels) == 0){
			echo "Deze factuur heeft geen factuurregels.<br/><br/>";
		} else {
			if (!$editFactuurRegels && !$this->isDefinitief()) {
				echo "Klik ";
				echo "<a href=\"" . $this->makeLink("editfactuurregels") .
				"&levfactuurnr=" . $this->getNr() . "\"target=\"_self\">\n";
				echo "hier</a>";
				echo " om onderstaande factuurregels te bewerken";
			}
			if ($editFactuurRegels){
				echo addForm($this->makeLink("processeditfactuurregels") .
				"&levfactuurnr=" . $this->getNr(),
				"POST");
			}

			echo "<table width=\"100%\">\n";

			// Eerst de headers doen.
			echo LeverancierFactuurRegel::htmlTableHeader();

			$factuurregelnrs = array();
			$counter = 0;
			$factuurTotaal = 0;
			$factuurVerschillenTotaal = 0;
			foreach ($factuurregels as $factuurregel){
				$rowbg = "white";
				if ($counter % 2 == 0){
					$rowbg = "#DCDCDC";
				}
				echo $factuurregel->toHtmlTableRow($editFactuurRegels,
				$rowbg);
				$factuurregelnrs[] = $factuurregel->getNr();
				$counter++;
				$factuurTotaal = $factuurTotaal + $factuurregel->getTotaal();
				$factuurVerschillenTotaal = $factuurVerschillenTotaal + $factuurregel->getTotaalVerschil();
			}

			// Even een hidden field maken met daarin alle factuurregelnummers
			$factuurregelnrs = implode(",", $factuurregelnrs);
			echo addHidden("factuurregelnrs", $factuurregelnrs);

			if ($counter != 0){
				echo "	<tr>\n";
				echo "		<td colspan=\"6\" align=\"right\" valign=\"bottom\">\n";
				echo "		Totalen:";
				echo "		</td>\n";

				// Totaal van de factuur
				echo "		<td align=\"right\" style=\"border-top: double
				black\" valign=\"bottom\">\n";
				echo Money::addPrice($factuurTotaal);
				echo "		</td>\n";

				// Totaal van de verschillen
				echo "		<td align=\"right\" style=\"border-top: double
				black\" valign=\"bottom\">\n";
				echo Money::addPrice($factuurVerschillenTotaal);
				echo "		</td>\n";

				echo "</tr>\n";
			}
			echo "</table>";
			echo "*) Factuurverschil = totaal verkocht - factuurbedrag<br/>\n";
			echo "Een positief verschil betekent dus dat er te
			weinig gefactureerd is, een negatief verschil betekent dat er
			te veel gefactureerd is.";

			echo "<br/>\n";
		}

		if (!$this->isDefinitief() && !$editFactuurRegels){
			echo "<a href=\"" . $this->makeLink("nieuwefactuurregels") .
			"&levfactuurnr=" . $this->getNr() . "\"target=\"_self\">\n";
			echo "(klik hier om nieuwe factuurregels toe te voegen aan deze
			factuur)";
			echo "</a>\n";
		} elseif ($editFactuurRegels){
			echo addFormEnd("Wijzigingen opslaan", false);
		}

		echo "</div><hr/>\n";

		echo "<h3>Verkochte artikelen die niet gefactureerd zijn</h3>\n";
		echo "<p>Het kan zijn dat een factuur niet volledig is en dat er dus
		volgens BookWeb wel artikelen verkocht zijn van de gegeven
		leverancier in de gegeven periode, maar dat die artikelen niet
		gefactureerd zijn.</p>\n\n";


		echo "<p>";
		$voorraden = $this->findVergetenProducten();

		$counter = 0;
		foreach ($voorraden as $voorraadnr=>$voorraadinfo){
			if (!isset($voorraadinfo["gefactureerd"])){
				$gefactureerd = 0;
			} else {
				$gefactureerd = $voorraadinfo["gefactureerd"];
			}

			if (!isset($voorraadinfo["verkocht"])){
				$verkocht = 0;
			} else {
				$verkocht = $voorraadinfo["verkocht"];
			}
			$EAN = $voorraadinfo["EAN"];

			// Alleen een waarschuwing tonen als er minder artikelen
			// gefactureerd zijn dan verkocht. Als er namelijk meer
			// gefactureerd is dan verkocht, dan wordt er een waarschuwing
			// getoond in de lijst met factuurregels
			if ($gefactureerd < $verkocht){
				$boekinfo = $BWDB->q("MAYBETUPLE
					SELECT * FROM boeken
					WHERE EAN=%s", $EAN);
				echo print_EAN_bi($EAN);
				echo " &quot;" . $boekinfo["titel"] . "&quot; door ";
				echo "&quot;" . $boekinfo["auteur"] . "&quot ";
				echo " -- $verkocht verkocht, $gefactureerd gefactureerd.
				(" . Money::addPrice($voorraadinfo["levprijs"]) . ")<br/>\n";
				$counter++;
			}

		}
		echo "</p>\n\n<p>";

		if ($counter == 0){
			echo "Alle verkochte artikelen zijn gefactureerd!\n";
		} else {
			echo "<a href=\"" . $this->makeLink("addvergetenproducten") .
				"&levfactuurnr=" . $this->getNr() .
				"\"	target=\"_self\">(bovenstaande producten en aantallen invullen als factuurregels)</a>\n";
		}
		echo "</p>";
	}

	/*	Retourneert een array met informatie over producten waarvoor minder gefactureerd is dan daadwerkelijk verkocht

		Formaat:
		$voorraden[voorraadnr]["verkocht"] = aantalverkocht
							  ["gefactureerd"] = aantal gefactureerd
							  ["EAN"] = ean
							  ["levprijs"] = leverancierprijs
	*/
	function findVergetenProducten(){
		$voorraden = array();

		$verkocht =	Voorraad::searchByVerkoopdatum(
			$this->getFactuurperiodeStart(),
			$this->getFactuurperiodeEind()
		);

		// Alle verkochte voorraad (volgens bookweb) langslopen
		foreach ($verkocht as $voorraad){
			$voorraadnr = $voorraad->getNr();

			if ($voorraad->getLeverancierNr() == $this->getLeverancierNr()){
				// Hoeveel verkopen zijn er geweest in die periode?
				$aantalVerkocht = $voorraad->getNettoVerkopen(
					$this->getFactuurperiodeStart(),
					$this->getFactuurperiodeEind()
				);

				$voorraden[$voorraad->getNr()]["EAN"] = $voorraad->getEAN();
				$voorraden[$voorraad->getNr()]["levprijs"] = $voorraad->getLevPrijs();
				$voorraden[$voorraad->getNr()]["verkocht"] = $aantalVerkocht;
				$voorraden[$voorraad->getNr()]["gefactureerd"] = 0; // initial value

				if ($aantalVerkocht != 0){
					// Blijkbaar zijn er (netto) verkopen geweest in de gegeven
					// periode, dus even kijken of het EAN dat verkocht is ook
					// wel gefactureerd is

					// Alle gefactureerde producten langslopen en de array $voorraden
					// opbouwen.
					$gefactureerd = $this->getFactuurRegels();

					foreach ($gefactureerd as $factuurregel){

						if ($factuurregel->getVoorraadnr() == $voorraad->getNr()){
							if (isset($voorraden[$voorraad->getNr()]["gefactureerd"])){
								$alGefactureerd = $voorraden[$voorraad->getNr()]["gefactureerd"];

								$voorraden[$voorraad->getNr()]["gefactureerd"] = $alGefactureerd + $factuurregel->getAantal();
							} else {
								$voorraden[$voorraad->getNr()]["gefactureerd"] = $factuurregel->getAantal();
							}

						}
					}
				} // else: geen nettoverkopen
			} // else: andere leverancier
		} // end foreach

		// Kloppende aantallen uit array verwijderen
		$goedeVoorraadnrs = array();
		foreach ($voorraden as $voorraadnr=>$voorraadinfo){
				if ($voorraadinfo["verkocht"] === $voorraadinfo["gefactureerd"]){
					$goedeVoorraadnrs[] = $voorraadnr;
				}
		}
		foreach ($goedeVoorraadnrs as $voorraadnr){
			unset($voorraden[$voorraadnr]);
		}

		return $voorraden;
	}

	/**
		Print een form waarop ISBNs kunnen worden ingevuld voor nieuwe
		factuurregels.
	*/
	function printFormNieuweFactuurRegels(){
		echo addForm(BookwebBase::makeUrl(array(
			"page"=>"financieel",
			"fin_action"=>"facturen",
			"fin_subaction"=>"processnieuwefactuurregels",
			"levfactuurnr"=>$this->getNr())), "POST");

		echo "<br/>Vul hieronder de ISBNs/artikelnummers in die je als nieuwe factuurregels
		wilt invoeren:<br/>\n";
		echo addTextArea("isbns", tryPar("isbns", ""), 40, 10);
		echo "<br/>\n";
		echo addFormEnd("ISBNs toevoegen aan factuur", false);
	}

	/**
		Retourneert of deze factuur definitief is (dus geen concept of
		ongeldig). Dat betekent namelijk dat er niets meer aan bewerkt mag
		worden.
	**/
	function isDefinitief(){
		return ($this->getStatus() == "definitief");
	}

	/*
		Print een form-element "select" voor het selecteren van de juiste
		status van deze factuur. De huidige status is de default value
	*/
	// BLAH... zowel static als non-static afhankelijk van $fieldname FIXME!
	function printStatusSelector($fieldname = "status"){
		$thisStatus = tryPar($fieldname, false);
		if ($thisStatus === false){
			if (isset($this)){
				$thisStatus = $this->getStatus();
			}
		}

		if (!$thisStatus) $thisStatus = "concept";

		echo addSelect($fieldname, array("concept", "ongeldig", "definitief"), $thisStatus);
	}

	function getFactuurRegels(){
		global $BWDB;

		$regelnrs = $BWDB->q("
			COLUMN SELECT levfactuurregelnr 
			FROM leverancierfactuurregel
			NATURAL LEFT JOIN boeken
			WHERE levfactuurnr=%i 
			ORDER BY auteur", $this->getNr());

		$res = LeverancierFactuurRegel::get($regelnrs);
		return $res;
	}

	function getTotaalBedrag() {
		global $BWDB;

		$totaalbedrag = $BWDB->q("VALUE SELECT SUM(aantal * bedrag) FROM
		leverancierfactuurregel WHERE levfactuurnr=%i", $this->getNr());

		if ( $totaalbedrag == "" )
			return 0;
		else
			return $totaalbedrag;
	}

	static function getAlleFacturen($laatsteEerst = false)
	{
		global $BWDB;

		$factuurnrs = $BWDB->q("
			COLUMN SELECT levfactuurnr
			FROM leverancierfactuur
			ORDER BY factuurdatum " . ($laatsteEerst ? "DESC" : "ASC") . ", status");

		$res = array();
		foreach ($factuurnrs as $factuurnr) {
			$res[] = new LeverancierFactuur($factuurnr);
		}

		return $res;
	}

	/**
		Print een lijst met de informatie uit de gegeven facturen. Let op!
		Het gaat hier om LeverancierFactuur-objecten!
		Als je een lijst wilt met alle facturen, dan laat je $facturen
		gewoon null
	*/
	static function printFactuurLijst($facturen = null)
	{
		echo "<br/>\n";
		if ($facturen == null) $facturen = LeverancierFactuur::getAlleFacturen(true);

		if (sizeof($facturen) <= 0){
			echo "Helaas, er zijn geen facturen gevonden!";
		} else {
			echo "Hieronder zie je een lijst met facturen. Klik op de
			factuurdatum om de factuurdetails te zien:<br/>\n";

			echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
			echo "	<tr>\n";
			echo "		<th>Factuurdatum</th>\n";
			echo "		<th>Leverancier</th>\n";
			echo "		<th>Factuurperiode</th>\n";
			echo "		<th>Status</th>\n";
			echo "		<th>Totaalbedrag</th>\n";
			echo "	</tr>\n";

			foreach ($facturen as $factuur){
				$levfactuurnr = $factuur->getNr();
				$factuurdatum = $factuur->getFactuurdatum();
				$leveranciernr = $factuur->getLeverancierNr();
				$periodeStart = $factuur->getFactuurperiodeStart();
				$periodeEind = $factuur->getFactuurperiodeEind();
				$status = $factuur->getStatus();
				$totaal = BookwebBase::bedrag($factuur->getTotaalBedrag());

				$tdstyle = ($status == "ongeldig" ? "style=\"color: grey\"" : "");

				echo "	<tr>\n";
				echo "		<td $tdstyle>\n";
				echo "			<a href=\"" . LeverancierFactuur::makeLink("show") .
				"&levfactuurnr=$levfactuurnr\" target=\"_self\">";
				echo "$factuurdatum";
				echo "			</a></td>\n";
				echo "		<td $tdstyle>\n";
				echo "			" . getLeverancierNaam($leveranciernr);
				echo "		</td>\n";
				echo "		<td $tdstyle>$periodeStart tot $periodeEind</td>\n";
				echo "		<td $tdstyle>$status</td>\n";
				echo "		<td $tdstyle>$totaal</td>\n";
				echo "	</tr>\n";
			}

			echo "</table>\n";
		}
	}


	static function dispatch(){
		requireAuth("boekcom");
		$fin_action = tryPar("fin_action");
		if ($fin_action != "facturen") return false;

		$fin_subaction = tryPar("fin_subaction");
		$levfactuurnr = tryPar("levfactuurnr", null);
		$levfactuurregelnr = tryPar("levfactuurregelnr", -1);

		$next_subaction	= tryPar("next_fin_subaction");
		if (is_numeric($levfactuurnr)){
			$factuur = new LeverancierFactuur($levfactuurnr);
			if (BookwebBase::hasNewErrors()){
				BookwebBase::printErrors();
				return;
			}
		} else {
			$factuur = null;
		}

		switch ($fin_subaction){
			case "trivialiteiten":
				if (!$factuur){
					print_error("Kan geen trivialiteiten invullen als er geen factuur is geselecteerd!");
				} else {
					$factuur->trivialeKoppelingen();
					echo $factuur->toHTmlString();
				}
				break;
			case "addvergetenproducten":
				// "Vergeten" (= ongefactureerde) producten invullen
				if (!$factuur){
					print_error("Kan geen vergeten producten invullen als er geen factuur is geselecteerd!");
				} else {
					$factuur->addVergetenProducten();
					echo $factuur->toHtmlString();
				}
				break;
			case "list":
				LeverancierFactuur::printFactuurLijst();
				break;
			case "edit":
				if ($factuur != null){
					// Er wordt een bestaande factuur bewerkt
					if ($factuur->isDefinitief()){
						echo "De opgegeven factuur mag niet bewerkt worden,
						omdat deze de status &quot;definitief&quot; heeft";
						return false;
					}
				} else {
					// nieuwe factuur aanmaken
					$factuur = new LeverancierFactuur();
				}

				$factuur->toHtmlString(false, true, false);
				break;
			case "delete":
				if ($factuur != null){
					if ($factuur->isDefinitief()){
						BookwebBase::addError(new BookwebError("Deze
						factuur kan niet verwijderd worden, omdat deze de
						status &quot;definitief&quot; heeft gekregen!"));
						BookwebBase::printErrors();
						return false;
					}

					$reallyDelete = tryPar("reallydelete", "");
					if ($reallyDelete != "ja"){
						// Waarschuwing tonen
						echo "<br/>Weet je zeker dat je deze factuur wilt
						verwijderen?<br>\n";
						echo "<a href=\"" . LeverancierFactuur::makeLink("delete") .
						"&reallydelete=ja&levfactuurnr=$levfactuurnr" . "\"
						target=\"_self\">Ja, gewoon doen!</a><br/>";

						echo "<a href=\"" . LeverancierFactuur::makeLink("show") .
						"&levfactuurnr=$levfactuurnr" . "\"
						target=\"_self\">Nee, help, niet doen!</a>";
					} else {
						// Echt verwijderen
						if ($factuur->deleteFromDatabase()){
							echo "<br>De factuur is succesvol verwijderd uit de
							database. Klik ";
							echo "<a href=\"" . LeverancierFactuur::makeLink("list") . "\"
							target=\"_self\">";
							echo "hier";
							echo "</a>";
							echo " om terug te gaan naar de lijst met
							facturen.";
						}
					}
				} else {
					// Geen factuur-object
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurnr ($levfactuurnr) is ongeldig"));
					BookwebBase::printErrors();

					return false;
				}
				break;
			case "editfactuurregels":
				if (is_numeric($levfactuurnr) && $factuur instanceof LeverancierFactuur){
					// Er wordt een bestaande factuur bewerkt
					if ($factuur->isDefinitief()){
						echo "De opgegeven factuur mag niet bewerkt worden,
						omdat deze de status &quot;definitief&quot; heeft";
						return false;
					}
				} else {
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurnr is ongeldig!"));
					return false;
				}

				$factuur->toHtmlString(true, false, true);
				break;
			case "processedit":
				if (is_numeric($levfactuurnr) && $factuur instanceof LeverancierFactuur){
					if ($factuur->isDefinitief()){
						echo "De opgegeven factuur mag niet bewerkt worden,
						omdat deze de status &quot;defintief&quot; heeft";
						return false;
					}
				} // else: nieuwe factuur ingevoerd
				$edited_id = LeverancierFactuur::processEditForm();

				if ($edited_id === false){
					// Fout bij het opslaan

					BookwebBase::printErrors();

					if ($levfactuurnr >= 0){
						// Blijkbaar is er iets fout gegaan bij het
						// bewerken van een bestaande factuur.
						$factuur = new LeverancierFactuur($levfactuurnr);

						// Opnieuw bewerken
						echo $factuur->toHtmlString(false,true,false);
					} else {
						// Er ging iets fout bij het invoeren van een
						// nieuwe factuur
						$factuur = new LeverancierFactuur(null);
						echo $factuur->toHtmlString(false,true,false);
					}
				} else {
					// Wijzigingen waren succesvol,
					// factuur tonen te verificatie
					BookwebBase::printMessagesAndErrors();
					$factuur = new LeverancierFactuur($edited_id);

					echo $factuur->toHtmlString();
				}

				break;
			case "show":
				if ($factuur != null){
					echo $factuur->toHtmlString();
				} else {
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurnr is ongeldig!"));
					BookwebBase::printErrors();
				}
				break;

			case "nieuwefactuurregels":
				if ($factuur != null){
					// OK
					$factuur->printFormNieuweFactuurRegels();
				} else {
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurnr is ongeldig!"));
					BookwebBase::printErrors();
				}
				break;
			case "processnieuwefactuurregels":
				// Hierna wordt automatisch de factuurinformatie en de
				// factuurspecificatie (bewerkbaar) getoond
				$factuur->processNieuweFactuurRegels(tryPar("isbns"));
				break;
			case "processeditfactuurregels":
				if ($factuur->processEditFactuurRegels() === false){
					BookwebBase::printErrors();

					// Gebruiker opnieuw het formulier
					// tonen
					echo $factuur->toHtmlString(true, false, true);
				} else {
					// In orde, geen fouten
					BookwebBase::addMessage(new
					BookwebMessage("De
					factuurregels zijn succesvol
					bijgewerkt in de database"));
					BookwebBase::printMessagesAndErrors();

					echo $factuur->toHtmlString(true,false,false);
				}

				break;
			case "deletefactuurregel":
				if (!LeverancierFactuurRegel::exists($levfactuurregelnr)){
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurregelnr ($levfactuurregelnr) bestaat
					niet!"));
				} else {
					$factuurregel = new
					LeverancierFactuurRegel($levfactuurregelnr);

					$factuur = $factuurregel->getFactuur();

					if (BookwebBase::hasNewErrors()){
						BookwebBase::printErrors();
					} else {
						$factuurregel->deleteFromDatabase();

						BookwebBase::addMessage(new BookwebMessage("De factuurregel
						is succesvol verwijderd uit de database"));
					}

				}

				echo "<br/>\n";
				BookwebBase::printMessagesAndErrors();

				if (isset($factuur)){
					echo $factuur->toHtmlString(true, false, false);
				}

				break;
			case "editfactuurregel":
				if (!LeverancierFactuurRegel::exists($levfactuurregelnr)){
					BookwebBase::addError(new BookwebError("Het opgegeven
					levfactuurregelnr ($levfactuurregelnr) bestaat
					niet!"));
				} else {
					$factuurregel = new LeverancierFactuurRegel($levfactuurregelnr);

					$factuurregel->printFormEdit();
				}

				break;
			case "processeditfactuurregel":
				if (!LeverancierFactuurRegel::exists($levfactuurregelnr)){
					BookwebBase::addError(new
					BookwebError("Het opgegeven
					levfactuurregelnr
					($levfactuurregelnr) bestaat
					niet!"));

					echo "<br/>\n";
					BookwebBase::printMessagesAndErrors();
				} else {
					$factuurregel = new LeverancierFactuurRegel($levfactuurregelnr);

					if ($factuurregel->processEditFactuurRegel()){
						// Succesvol bijgewerkt
						$factuur = $factuurregel->getFactuur();
						echo $factuur->toHtmlString();
					} else {
						// Fouten: form opnieuw tonen
						echo "<br/>\n";
						BookwebBase::printMessagesAndErrors();

						$factuurregel->printFormEdit();
					}
				}


				break;
			case "invalidateduplicate":
				// "this" markeren als ongeldig en een exacte kopie van
				// LeverancierFactuur en LeverancierFactuurRegel maken

				// "oude" factuur ongeldig markeren en opslaan
				$factuur->setStatus("ongeldig");
				$oudeOpmerkingen = $factuur->getOpmerkingen();

				if (trim($oudeOpmerkingen) != ""){
					$oudeOpmerkingen = $oudeOpmerkingen . "\n\n";
				}
				$factuur->setOpmerkingen($oudeOpmerkingen . "Ongeldig verklaard en gedupliceerd");
				$factuur->store();

				// "nieuwe" factuur maken en status resetten
				$factuur->duplicateAndStore();
				$factuur->setOpmerkingen(trim($oudeOpmerkingen));
				$factuur->setStatus("concept");
				$factuur->setFactuurdatum("0000-00-00");
				$factuur->store();

				echo "<br/>De oude factuur is gedupliceerd en vervolgens
				ongeldig gemarkeerd. Hieronder zie je de gekopieerde factuur, vul
				hier dus de nieuwe factuurdatum op in.<br/><br/>";
				$factuur->toHtmlString(false, true, false);
				break;

			case "factuuroverzicht":
				$van = "2005-07-01";
				$tot = "2006-08-01";

				LeverancierFactuur::printFactuurOverzicht(array(11), $van, $tot);
		}


	}

	/**
		Verwerkt een hele lijst bewerkte factuurregels
	*/
	function processEditFactuurRegels(){
		$factuurregelnrs = tryPar("factuurregelnrs", false);

		if (!$factuurregelnrs){
			// Er is geen lijst met bewerkte factuurnrs?
			BookwebBase::addError(new BookwebError("De gewijzigde factuurregels
			konden niet verwerkt worden, omdat er geen lijst is met
			gewijzigde factuurregelnrs"));
			return false;
		}

		$factuurregelnrs = explode(",", $factuurregelnrs);
		foreach ($factuurregelnrs as $factuurregelnr){
			$bedrag = tryPar("bedrag_$factuurregelnr");
			$aantal = tryPar("aantal_$factuurregelnr");

			$factuurregel = new LeverancierFactuurRegel($factuurregelnr);
			if ($factuurregel->isValid()){
				$factuurregel->setBedrag($bedrag);
				$factuurregel->setAantal($aantal);
				$factuurregel->store();
			} else {
				BookwebBase::addError(new BookwebError("Het factuurregelnr $factuurregelnr is
				niet geldig"));
			}
		}

		if (BookwebBase::hasNewErrors()) return false;
	}

	/*
		Verwerkt een form waarin deze factuur is bewerkt
	*/
	static function processEditForm()
	{
		$levfactuurnr = tryPar("levfactuurnr", null);
		$fin_subaction = tryPar("fin_subaction");

		if (!$fin_subaction == "processedit") return false;
		global $BWDB;

		$factuurDatum = parseDate(tryPar("factuurdatum"));
		if ($factuurDatum === false){
			BookwebBase::addError(new BookwebError("De opgegeven factuurdatum is
			ongeldig"));
			return false;
		}
		$factuurLeverancierNr = tryPar("leveranciernr");
		$betaalDatum = parseDate(tryPar("betaaldatum"));
		$factuurPeriodeStart = parseDate(tryPar("factuurperiode_start"));
		$factuurPeriodeEind = parseDate(tryPar("factuurperiode_eind"));
		if ($factuurPeriodeStart === false){
			BookwebBase::addError(new BookwebError("De opgegeven datum voor het
			begin van de factuurperiode is ongeldig"));
			return false;
		}
		if ($factuurPeriodeEind === false){
			BookwebBase::addError(new BookwebError("De opgegeven datum voor het
			einde van de factuurperiode is ongeldig"));
			return false;
		}

		$factuurStatus = tryPar("status");
		$factuurOpmerkingen = tryPar("opmerkingen");

		if (!is_numeric($levfactuurnr) || $levfactuurnr < 0){
			// Nieuwe factuur maken
			$factuur = new LeverancierFactuur();
		} else {
			$factuur = new LeverancierFactuur($levfactuurnr);
		}

		$factuur->setValuesByArray(array(
			"factuurdatum" => $factuurDatum,
			"betaaldatum" => $betaalDatum,
			"factuurperiode_start" => $factuurPeriodeStart,
			"factuurperiode_eind" => $factuurPeriodeEind,
			"leveranciernr" => $factuurLeverancierNr,
			"opmerkingen" => $factuurOpmerkingen,
			"status" => $factuurStatus));
		$factuur->store();

		echo "<br/>\n";
		if (!is_numeric($levfactuurnr) || $levfactuurnr < 0){
			$newid = $factuur->getNr();
			BookwebBase::addMessage(new BookwebMessage("De nieuwe
			factuur (ID $newid) is succesvol opgeslagen in de database"));
			return $newid;
		}
		BookwebBase::addMessage(new BookwebMessage("De
		wijzigingen zijn succesvol opgeslagen in de
		database"));
		return $levfactuurnr;
	}

	/**
		Parsed de invoer uit een textfield met allemaal
		artikelnummers (ISBN, EAN, etc) om ze
		vervolgens aan deze LeverancierFactuur te koppelen (zonder prijs en
		aantal, alleen het artikelnr.
		De gebruiker wordt hier al gewaarschuwd als een nummer niet
		geparsed kan worden. Die foute nummers worden genegeerd.
		Let op! Onbekende ISBNs of EANs worden wel verwerkt!
	*/
	function processNieuweFactuurRegels($input)
	{
		$factuurregels = array();

		$artikelnrs = explode("\n", trim($input));

		foreach ($artikelnrs as $artikelnr){
			$artikelnr = trim($artikelnr);
			$artikelnr = str_replace(" ", "", $artikelnr);
			$artikelnr = str_replace("-", "", $artikelnr);

			$newFactuurregel = new LeverancierFactuurRegel();
			$store = false;

			if (is_isbn($artikelnr)){
				$store = $newFactuurregel->setISBN($artikelnr);
			} elseif (is_ean($artikelnr)){
				$store = $newFactuurregel->setEAN($artikelnr);
			} else {
				BookwebBase::addError(new BookwebError("Het artikelnr
				&quot;$artikelnr&quot; kon niet verwerkt
				worden!"));
			}

			if ($store){
				if ($this->addFactuurRegel($newFactuurregel) === true){
					$newFactuurregel->store();
				}
			}

			if (BookwebBase::hasNewErrors()){
				BookwebBase::printErrors();
			}
		}

		echo $this->toHtmlString();
	}

	function addFactuurRegel($factuurregel){
		if (!($factuurregel instanceof LeverancierFactuurRegel)){
			BookwebBase::addError(new BookwebError("Het opgegeven object is geen
			geldig LeverancierFactuurRegel-object!"));
			return false;
		}

		return $factuurregel->setLeverancierFactuur($this);
	}

	static function makeLink($fin_subaction)
	{
		return STARTPAGE .
		"?page=financieel&fin_action=facturen&fin_subaction=$fin_subaction";
	}

	/**
		Voegt vergeten (= ongefactureerde) producten toe als factuurregels
		aan de factuur.
	*/
	function addVergetenProducten()
	{
		$vergetenProducten = $this->findVergetenProducten();

		foreach ($vergetenProducten as $voorraadnr=>$productinfo){
			$EAN = $productinfo["EAN"];
			$gefactureerd = $productinfo["gefactureerd"];
			$verkocht = $productinfo["verkocht"];
			$levprijs = $productinfo["levprijs"];

			$verschil = $verkocht - $gefactureerd;

			// Factuurregel in elkaar klussen
			$nieuweRegel = new LeverancierFactuurRegel();
			$nieuweRegel->setVoorraadnr($voorraadnr);
			$nieuweRegel->setLeverancierFactuur($this);
			$nieuweRegel->setEAN($EAN);
			$nieuweRegel->setAantal($verschil);
			$nieuweRegel->setBedrag($levprijs);
			$nieuweRegel->store();
		}
	}

	/**
		Maakt triviale koppelingen van LeverancierFactuurRegel-objecten in
		deze factuur die nog niet aan een voorraadnr gekoppeld zijn.
		Deze koppeling wordt alleen gemaakt als de leverprijs en het aantal
		verkochte boeken van de te koppelen voorraad overeenkomt
	*/
	function trivialeKoppelingen(){
		$factuurregels = $this->getFactuurRegels();
		$leveranciernr = $this->getLeverancierNr();

		$aantalKoppelingen = 0;
		foreach ($factuurregels as $factuurregel){
			$voorraadnr = $factuurregel->getVoorraadnr();
			$EAN = $factuurregel->getEAN();
			$aantal = $factuurregel->getAantal();
			$bedrag = $factuurregel->getBedrag();

			//echo "EAN: $EAN, aantal: $aantal, bedrag: $bedrag, voorraadnr: $voorraadnr";
			$factuurper_start = $factuurregel->getFactuurPeriodeStart();
			$factuurper_eind = $factuurregel->getFactuurPeriodeEind();
			if (!$factuurper_start) $factuurper_start = $this->getFactuurPeriodeStart();
			if (!$factuurper_eind) $factuurper_eind = $this->getFactuurPeriodeEind();

			if ( (!is_numeric($voorraadnr) || $voorraadnr < 0) && (is_numeric($aantal) && is_numeric($bedrag) && $bedrag > 0)){
				/*echo "$EAN is niet aan voorraad gekoppeld; ";
				echo ($aantal == 1) ? "$aantal exemplaar" : "$aantal exemplaren";
				echo " gefactureerd voor " .$bedrag . " per stuk.<br/>";*/

				$kandidaatvoorraden = $factuurregel->zoekKandidaatVoorraden();

				foreach ($kandidaatvoorraden as $kvoorraad){
					$nettoverkocht = $kvoorraad->getNettoVerkopen($factuurper_start, $factuurper_eind);
					$voorraadprijs = $kvoorraad->getLevPrijs();

					//echo "Kandidaatsvoorraad voor $EAN: nettoverkopen=$nettoverkocht, levprijs=$voorraadprijs<br/>";
					if (
							$aantal == $kvoorraad->getNettoVerkopen($factuurper_start, $factuurper_eind) &&
							$bedrag == $kvoorraad->getLevPrijs() &&
							$leveranciernr = $kvoorraad->getLeverancierNr()){

						// koppelen!
						$factuurregel->setVoorraadnr($kvoorraad->getNr());
						$factuurregel->store();

						$aantalKoppelingen++;

						break 1; // foreach niet verder uitvoeren
					}
				}
			}
		}

		$msg = ($aantalKoppelingen == 1) ? "Er is 1 triviale koppeling ingevuld" : "Er zijn $aantalKoppelingen triviale koppelingen ingevuld";

		BookwebBase::addMessage(new	BookwebMessage("$msg"));
		BookwebBase::printMessagesAndErrors();
	}

	/**
		Deze functie controleert over meerdere facturen: zo kun je van een
		jaar bekijken of alle verkochte boeken gefactureerd zijn (= alle
		voorraadnrs aan een factuurregel gekoppeld zijn) en of het totaal
		aantal gefactureerde boeken niet te groot is.

		1) alle verkopen (voorraadnrs) opzoeken
		2) gekoppelde leverancierfactuurregelnrs erbij zoeken
		3) controleren of het totaal aantal gefactureerde boeken klopt
		4) tonen per voorraadnr:
		  	- splitsen per leverancierfactuurregelnr
			- verschil tonen (aantal verkopen, totaalbedrag)
		5) optelling van verschillen tonen
	*/
	static function printFactuurOverzicht()
	{
		$color2 = "blue";
		$tr2 = "<tr style=\"color: $color2; font-size: 8pt; font-weight: 100\">"; //rij met factuurinfo
		$tr2_bg = "<tr style=\"background-color: lightblue; font-size: 8pt; font-weight: 100; font-style: italic;\">"; //rij met factuurinfo
		$leveranciernr = tryPar("leveranciernr");
		if (is_array($leveranciernr)) $leveranciernr = $leveranciernr[0];
		$begindatum = tryPar("begindatum");
		$einddatum = tryPar("einddatum");
		$noformatting = tryPar("noformatting", false);
		if (!$begindatum){
			$thismaand = date("n");
			$thisjaar = date("Y");
			$prevjaar = $thisjaar - 1;
			$nextjaar = $thisjaar + 1;
			if ($thismaand >= 8){
				$begindatum = "$thisjaar-08-01";
				$einddatum = "$nextjaar-08-01";
			} else {
				// eerder dan augustus
				$begindatum = "$prevjaar-08-01";
				$einddatum = "$thisjaar-08-01";
			}
		}

		echo addForm("", "GET") .
			addHidden("page", "financieel") . addHidden("fin_action", "facturen") . addHidden("fin_subaction", "factuuroverzicht") .
			"<table align=\"center\" class=\"bw_datatable_medium_100\" width=\"60\">\n" .
			"<tr>" .
				"<th colspan=\"2\">Opties factuuroverzicht</th>" .
			"</tr>\n" .
			"<tr>" .
				"<td>Leverancier:</td>" .
				"<td>" .
					bwfAddSelectLeveranciers("leveranciernr", false) .
				"</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td width=\"1\">Begindatum&nbsp;selectie:</td>" .
				"<td>" . addInput1("begindatum", $begindatum) . "</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td>Einddatum&nbsp;selectie:</td>" .
				"<td>" . addInput1("einddatum", $einddatum) . "</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td>&nbsp;</td><td>" .
					addCheckbox("conceptfacturen", tryPar("conceptfacturen", false)) .
					"<label for=\"conceptfacturen\">Conceptfacturen meenemen bij factuurcontrole</label>" .
				"</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td>&nbsp;</td><td>" .
					addCheckbox("noformatting", tryPar("noformatting", false)) . "<label for=\"noformatting\">Tonen zonder opmaak</label>" .
				"</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td colspan=\"2\" style=\"font-style: italic\">" .
					"Let op! Alle verkopen vanaf de begindatum <u>tot aan</u> de einddatum worden meegerekend in onderstaand overzicht. Dit in tegenstelling tot de facturen, " .
					"waarbij een factuur (en factuurregels) alleen worden meegenomen als de volledige factuurperiode binnen het gegeven interval valt." .
				"</td>" .
			"</tr>\n" .
			"<tr>" .
				"<td align=\"center\" colspan=\"2\">" .
					"<input type=\"submit\" value=\"Hop!\">" .
				"</td>" .
			"</tr>\n" .
			"</table><br/><br/>" .
			"</form>";

		echo "<table class=\"bw_datatable_medium_100\" style=\"font-size: 8pt\" cellspacing=\"0\">\n" .
			"<tr>" . // header verkoopinformatie
				"<th width=\"1\">Voorraadnr</th>" .
				"<th>EAN/ISBN</th>" .
				"<th>Titel/auteur</th>" .
				"<th width=\"1\">Verkocht</th>" .
				"<th width=\"1\">Teruggekocht</th>" .
				"<th width=\"1\">Netto</th>" .
				"<th width=\"1\">Leverancierprijs</th>" .
				"<th>Totaal</th>" .
				"<th width=\"1\">Verschil</th>" .
			"</tr>\n" .
			"$tr2" . // begin header factuurinformatie
				"<th>&nbsp;</th>" .
				"<th>&nbsp;</th>" .
				"<th colspan=\"3\">Factuurinformatie</th>" .
				"<th>Aantal</th>" .
				"<th>Factuurbedrag</th>" .
				"<th>Totaal</th>" .
				"<th>Verschil</th>" .
			"</tr>\n";

		$statistieken = array(); // stats zijn vet :)
		$statistieken["facttotaal"] = 0; // totaalbedrag factuur
		$statistieken["verktotaal"] = 0; // totaal verkocht
		$statistieken["facttotaalnietnl"] = 0; // totaalbedrag factuur niet-NL titels (voor Bruna sponsoring!)
		$statistieken["factaantal"] = 0;
		$statistieken["verkaantal"] = 0; // totaalbedrag factuur niet-NL titels (voor Bruna sponsoring!)

		$verterugkopen = sqlGetVerTerugkopen($begindatum, $einddatum, array($leveranciernr), "EAN, voorraadnr");
		foreach ($verterugkopen as $voorraadnr => $inforow){
			echo "<tr>";

			$verkopen = $inforow["verkopen"];
			$terugkopen = $inforow["terugkopen"];
			$verkochtNetto = $verkopen - $terugkopen;
			$EAN = $inforow["EAN"];
			$artikelgegevens = sqlBoekGegevens($EAN);
			$voorraadgeschiedenis = "/Onderwijs/Boeken/Voorraadmutaties?EAN=$EAN";
			$levprijs = $inforow["levprijs"];
			$verkochtBedrag = $levprijs * $verkochtNetto;

			echo "<td>" .
					($noformatting ? $voorraadnr : "<a href=\"$voorraadgeschiedenis\">$voorraadnr</a>") .
				"</td>" .
				"<td>" . ($noformatting ? print_EAN($EAN) : print_EAN_bi($EAN)) . "</td>" .
				"<td>" .
					"&quot;" . cut_titel($artikelgegevens["titel"]) . "&quot; door &quot;" . cut_auteur($artikelgegevens["auteur"]) . "&quot;" .
				"</td>" .
				"<td>$verkopen</td>" .
				"<td>$terugkopen</td>" .
				"<td>$verkochtNetto</td>" .
				"<td>" . ($noformatting ? $levprijs : Money::addPrice($levprijs)) . "</td>" .
				"<td>" . ($noformatting ? $levprijs : Money::addPrice($verkochtBedrag)) . "</td>";

			echo "</tr>\n";

			$voorraad = new Voorraad($voorraadnr);
			$factuurregels = LeverancierFactuurRegel::searchByVoorraad($voorraad, true, tryPar("conceptfacturen", false), false, $begindatum, $einddatum);

			$voorrFactStuks = 0; // totaal gefactureerd aantal van deze voorraad
			$voorrFactBedrag = 0; // totaal gefactureerd bedrag van deze voorraad
			foreach ($factuurregels as $factuurregel){
				$factuur = $factuurregel->getFactuur();
				$factPrijs = $factuurregel->getBedrag();
				$factAantal = $factuurregel->getAantal();
				$factOpm = $factuurregel->getOpmerkingen();
				$factTotaal = $factAantal * $factPrijs;

				echo "$tr2_bg" . // begin factuurinfo
					"<td></td><td></td>" .
					"<td colspan=\"3\" title=\"$factOpm\">" .
						$factAantal . ($factAantal == 1 ? " exemplaar" : " exemplaren") . " gefactureerd op " .
							($noformatting ? $factuur->getFactuurDatum() : $factuurregel->makeLinkEditMe($factuur->getFactuurDatum())) .
							" voor " . ($noformatting ? $factPrijs : Money::addPrice($factPrijs)) .
					"</td>" .
					"<td>" . $factAantal . "</td>" .
					($factPrijs != $levprijs ?
						"<td style=\"color: red\" title=\"$factOpm\">" . ($noformatting ? $factPrijs : Money::addPrice($factPrijs)) :
						"<td>" . ($noformatting ? $factPrijs : Money::addPrice($factPrijs))
					) . "</td>" .
					"<td>" . ($noformatting ? ($factAantal * $factPrijs) : Money::addPrice($factAantal * $factPrijs)) . "</td>" .
					"<td></td>" .
					"</tr>";

				$voorrFactStuks += $factAantal;
				$voorrFactBedrag += $factAantal * $factPrijs;

				$isbn = EAN_to_ISBN($EAN);
				if (substr($isbn, 0, 2) != "90"){
					// Niet-NL titel
					$statistieken["facttotaalnietnl"] += $factAantal * $factPrijs;
				}
			}
			$statistieken["verkaantal"] += $verkochtNetto; // totaal aantal verkocht
			$statistieken["factaantal"] += $voorrFactStuks; // totaal aantal gefactureerd
			$statistieken["verktotaal"] += $verkochtBedrag; // totaalbedrag verkocht van deze voorraad
			$statistieken["facttotaal"] += $voorrFactBedrag; // totaalbedrag gefactureerd van deze voorraad

			$isVerschilStuks = ($voorrFactStuks != $verkochtNetto);
			$verschilBedrag = round($verkochtBedrag - $voorrFactBedrag, 2);
			$isVerschilBedrag = ($verschilBedrag != 0);

			echo "$tr2_bg" .
				"<td colspan=\"5\">&nbsp;</td>" .
				"<td style=\"border-top: double $color2\">" .
					($isVerschilStuks && !$noformatting ? "<span style=\"color: red; font-weight: bold\">$voorrFactStuks</span>" : $voorrFactStuks) .
				"</td>" .
				"<td>&nbsp;</td>" .
				"<td style=\"border-top: double $color2\">" .
					($isVerschilBedrag && !$noformatting ?
						"<font style=\"color: red; font-weight: bold\">" . Money::addPrice($voorrFactBedrag) . "</span>" :
						($noformatting ? $voorrFactBedrag : Money::addPrice($voorrFactBedrag))
					) .
				"</td>" .
				"<td>" . ($noformatting ? $verschilBedrag : Money::addPrice($verschilBedrag)) . "</td>" .
				"</tr>";

			echo "<tr><td>&nbsp;</td></tr>";

		}

		echo "</table>";

		echo "<table class=\"bw_datatable_medium\">\n" .
			"<tr><th colspan=\"2\">Statistieken</th></tr>\n" .
			"<tr><td>Totaal gefactureerd</td><td align=\"right\">" . Money::addPrice( $statistieken["facttotaal"] ) . "</td></tr>\n" .
			"<tr><td>Totaal verkocht</td><td align=\"right\">" . Money::addPrice( $statistieken["verktotaal"] ) . "</td></tr>\n" .
			"<tr><td>Totaal gefactureerd aan niet-NL titels</td><td align=\"right\">" . Money::addPrice( $statistieken["facttotaalnietnl"] ) . "</td></tr>\n" .
			"<tr><td>Totaal aantal gefactureerd</td><td align=\"right\">" . $statistieken["factaantal"] . "</td></tr>\n" .
			"<tr><td>Totaal aantal verkocht</td><td align=\"right\">" . $statistieken["verkaantal"] . "</td></tr>\n" .
			"</table>";
	}
}

