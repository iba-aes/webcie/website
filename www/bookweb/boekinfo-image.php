<?php
require_once("bookweb/constants.php");
require_once("space/specialconstants.php");
require_once(realpath(dirname(__FILE__) . "/init.php"));
require_once("artikelcodes.php");
require_once("money.php");
require_once("booksql.php");

global $ttf, $ttf_bold;
$ttf = "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf";
$ttf_bold = "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans-Bold.ttf";
$fontsize = 9;

// Maakt een string korter, eventueel door (...) toe te voegen
function shortStr($str, $maxlength){
	if (strlen($str) > $maxlength){
		$str = substr($str,0,$maxlength - 5);
		$str .= "...";
	}
	return $str;
}

function strTextWidth($str, $ttf, $fontsize){
	global $ttf, $ttf_bold;
	$box = imagettfbbox($fontsize, 0, $ttf, $str);
	return $box[2] - $box[0];
}


header("Content-type: image/jpg");

$font = 2;
$font_bold = 3;

$wigglewidth = 300;
$wiggleheight = 183;
$width = "400";
$height = 150;

$ean = tryPar("EAN", tryPar("ean"));
$ean = str_replace("-", "", $ean);

$boekinfo = array();
if (!$sqlboekinfo = @sqlBoekGegevens($ean)){
	// Onbekend EAN?
	$boekinfo["titel"] = "onbekend";
	$boekinfo["auteur"] = "onbekend";
	$boekinfo["offerte"] = null;
	$boekinfo["bestelmoment"] = null;
	$boekinfo["leverdatum"] = null;
	$boekinfo["vrijevoorraad"] = 0;
} else {
	// Voorraad
	$faseninfo = sqlAlleFasenInfo(array($ean));
	$faseninfo = $faseninfo[$ean];
	$voorraden = sqlGetEANVoorraad($ean);

	$max_vkprijs = 0;
	foreach ($voorraden as $voorraad){
		$voorraadinfo = sqlGetVoorraadInfo($voorraad["voorraadnr"]);
		$vkprijs = berekenVerkoopPrijs(
			$voorraadinfo["levprijs"], $voorraadinfo["boekenmarge"], @$voorraadinfo["adviesprijs"]
		);
		if ($voorraad > 0 && $vkprijs > $max_vkprijs){
			$max_vkprijs = $vkprijs;
		}
	}

	$boekinfo["titel"] = shortstr($sqlboekinfo["titel"],50);
	$boekinfo["auteur"] = shortstr($sqlboekinfo["auteur"],50);
	$boekinfo["vrijevoorraad"] = $faseninfo["vr_34"];

	if ($boekinfo["vrijevoorraad"] == 0){
		// Geen vrije voorraad -> bestelinfo tonen
		$eerstvolgend_bestelmoment = null;
		global $BESTELDATA;
		foreach ($BESTELDATA as $bestelmoment => $leverdatum){
			if (strtotime($bestelmoment) > strtotime("now")){
				$eerstvolgend_bestelmoment = $bestelmoment;
				break;
			}
		}

		if ($eerstvolgend_bestelmoment == null){
			$boekinfo["bestelmoment"] = null;
		} else {
			$boekinfo["bestelmoment"] = $eerstvolgend_bestelmoment;
			$boekinfo["leverdatum"] = $BESTELDATA["$eerstvolgend_bestelmoment"];
		}

		// Offertes
		$offertes = sqlGetValidOffertesInfo(array($ean));
		$offertes = @$offertes["$ean"];

		if (sizeof($offertes) > 0){
			$boekinfo["offerte"] = berekenVerkoopPrijs(
				$voorraadinfo["levprijs"], $voorraadinfo["boekenmarge"], @$voorraadinfo["adviesprijs"]
			);
		} else {
			$boekinfo["offerte"] = null;
		}
	}
	$boekinfo["vkprijs"] = $max_vkprijs;
}

$bgcolor = tryPar("bgcolor", "#FFFFFF");
$bgcolor_rgb = sscanf($bgcolor,'#%2x%2x%2x');

$lines = array(3, 23, 38, 53, 68, 83, 98, 118, 132);

// Nieuw plaatje maken en Wiggle erin kopieren
$im = ImageCreate($width,$height);
$wiggle = ImageCreateFromPNG("/srv/http/www/www/bookweb/wiggle.png");
$wigglefactor = $height / $wiggleheight;
imagecopyresized($im, $wiggle, ($width - $wigglewidth * $wigglefactor) / 2, ($height - $wiggleheight * $wigglefactor) / 2, 0, 0, $wigglewidth * $wigglefactor, $wiggleheight * $wigglefactor, $wigglewidth, $wiggleheight);

$color_bg = ImageColorAllocate($im,$bgcolor_rgb[0],$bgcolor_rgb[1],$bgcolor_rgb[2]);
$color_black = ImageColorAllocate($im,0,0,0);
$color_white = ImageColorAllocate($im,255,255,255);
$color_aesred = ImageColorAllocate($im,151,0,30);

// Titel
$title = "A-Eskwadraat boekeninformatie"; // voor EAN $ean";
$titlewidth = strTextWidth($title, $ttf_bold, 12); //imagefontwidth($font_bold) * strlen($title);
$title_x = ($width - $titlewidth) / 2;
imagettftext($im, 12, 0, $title_x, $lines[0] + 12, $color_black, $ttf_bold, $title);
ImageLine($im, 0, 17, $width, 17, $color_aesred);

//ImageString($im, $font_bold, 5, $lines[1], "EAN:", $color_black);
ImageTTFText($im, $fontsize, 0, 5,	$lines[1] + $fontsize, $color_black, $ttf_bold, "EAN:");
ImageTTFText($im, $fontsize, 0, 100,$lines[1] + $fontsize, $color_black, $ttf, print_EAN($ean));
ImageTTFText($im, $fontsize, 0, 5,	$lines[2] + $fontsize, $color_black, $ttf_bold, "Titel:");
ImageTTFText($im, $fontsize, 0, 100,$lines[2] + $fontsize, $color_black, $ttf, $boekinfo["titel"]);
ImageTTFText($im, $fontsize, 0, 5, 	$lines[3] + $fontsize, $color_black, $ttf_bold, "Auteur(s):");
ImageTTFText($im, $fontsize, 0, 100,$lines[3] + $fontsize, $color_black, $ttf, $boekinfo["auteur"]);
ImageTTFText($im, $fontsize, 0, 5,	$lines[4] + $fontsize, $color_black, $ttf_bold, "Op voorraad:");
ImageTTFText($im, $fontsize, 0, 100,$lines[4] + $fontsize, $color_black, $ttf, $boekinfo["vrijevoorraad"] . " ex.");

if ($boekinfo["vrijevoorraad"] == 0){
	// Geen voorraad
	// Bestelinfo tonen
	ImageTTFText($im, $fontsize, 0, 5, $lines[5] + $fontsize, $color_black, $ttf_bold, "Bestellen:");

	if (!isset($boekinfo["bestelmoment"]) || $boekinfo["bestelmoment"] == null){
		$bestellen = "onbekend";
	} else {
		$bestellen = "voor " . date("d-m-Y", strtotime($boekinfo["bestelmoment"])) . " besteld, dan " . date("d-m-Y", strtotime($boekinfo["leverdatum"])) . " binnen";
	}
	ImageTTFText($im, $fontsize, 0, 100, $lines[5] + $fontsize, $color_black, $ttf, $bestellen);

	// Offerte tonen
	if ($boekinfo["offerte"] === null){
		$offertestr = "onbekend";
	} else {
		$offertestr = Money::addPrice($boekinfo["offerte"], false) . " euro";
	}
	ImageTTFText($im, $fontsize, 0, 5, $lines[6] + $fontsize, $color_black, $ttf_bold, "Verw. prijs:");
	ImageTTFText($im, $fontsize, 0, 100, $lines[6] + $fontsize, $color_black, $ttf, $offertestr);
} else {
	// Nog voorraad
	ImageTTFText($im, $fontsize, 0, 5, $lines[5] + $fontsize, $color_black, $ttf_bold, "Prijs:");
	ImageTTFText($im, $fontsize, 0, 100, $lines[5] + $fontsize, $color_black, $ttf, Money::addPrice($boekinfo["vkprijs"], false) . " euro");
}

$voorbehoud = "(wijzigingen voorbehouden)";
$voorbehoud_width = strTextWidth($voorbehoud, $font, $fontsize);
ImageTTFText($im, $fontsize, 0, ($width - $voorbehoud_width) / 2, $lines[7] + $fontsize, $color_black, $ttf, $voorbehoud);

$meerinfo = "Meer informatie: www.A-Eskwadraat.nl/Boeken";
$meerinfo_width = strTextWidth($meerinfo, $font, $fontsize);
ImageTTFText($im, $fontsize, 0, ($width - $meerinfo_width) / 2, $lines[8] + $fontsize, $color_black, $ttf_bold, $meerinfo);

imagepng($im);
