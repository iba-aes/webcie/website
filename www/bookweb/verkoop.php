<?php
// $Id$

/*
 *  Hier staan ongeveer alle verkoopfuncties
 */


/* kink: retourneert boolean die aangeeft of de verkoop open is */
function tryVerkoopOpen($nocache = FALSE) {

	static $isopen;
	if(!$nocache && isset($isopen)) return $isopen;

	global $BWDB;

	$res = $BWDB->q("COLUMN SELECT `verkoopdag`"
		." FROM `verkoopgeld`"
		." WHERE `geldigtot` IS NULL"
		." AND `sluitstamp` IS NULL"
		." AND `verkoopdag` = %s"
		, getActionDate());

	return $isopen = (sizeof($res) == 1);
}

/* zelfde als tryVerkoopOpen maar geeft foutmelding & exit als niet open */
function requireVerkoopOpen() {
	if(!tryVerkoopOpen())
		print_error(_('De verkoop is niet open!'));
}

/* (her)open een boekverkoop, met telling en kastelling.
 * kink */
function openverkoop()
{
    global $auth;

	if (tryVerkoopOpen()) {
		printHTML(_('De verkoop is al open!'));
		return;
	}

	$vkgeld = sqlGetVerkoopGeld(getActionDate());	// eventuele eerdere vk vandaag
	$last = sqlLastVerkoop();	// absoluut de laatste vk
	if ($vkgeld && !(tryPar('action') == 'heropen')) {
		printHTML(_('Er is al een verkoop geopend vandaag. Wil je die heropenen?'));
		simpleForm('heropen');
		endForm(_('ja joh!'), FALSE);
		return;
	}

	if(tryPar('action') != 'heropen' && !$last['sluitstamp']) {
		printHTML(sprintf('De verkoop is de vorige keer niet gesloten!
			(%s op %s)', PersoonView::naam(Lid::geef($last['verkoper'])),
			print_date($last['verkoopdag'])));
		printHTML(_('Je moet die eerst sluiten (met de juiste
			getallen) voor je verder kunt gaan.'));
		sluitverkoop( $last['verkoopdag'] );
		return;
	}

	if(tryPar('action') != 'heropen') {
		// sqlForceTelling checkt of dit de eerste telling van de maand is
		if (!telling(sqlForceTelling())) return;

		sqlNieuweVerkoop($auth->getLidnr());
		printHTML(sprintf(_('De vorige verkoop was op %s en werd gedaan door %s.'),
			print_date(substr($last['openstamp'],0,10)) ,
			PersoonView::naam(Lid::geef($last['verkoper']))));


	} else {
		sqlWisKasTelling($vkgeld['verkoopdag']);
	}

	bw_initKart();

	if (tryPar('action') == 'heropen') {
		printHTML(_('De verkoop is nu heropend. Vergeet niet om \'m af te sluiten!'));
	} else {
		printHTML(_('De verkoop is nu geopend. Vergeet niet om \'m af te sluiten!'));
	}


	// forced update van de gecachede tryverkoopopen
	tryVerkoopOpen(TRUE);

	verkoopmenu();

}

/* verzorgt een boekentelling, bijvoorbeeld als je de verkoop opent.
 * return true en false is voor het openen van de boekvk: true geeft
 * aan dat er verder gegaan kan worden met openen en false dat de
 * telling nog niet afgerond is.
 * Een telling met $force TRUE geeft geen getallen weer en kan niet
 * overgeslagen worden.
 * kink */
function telling($force = FALSE)
{
	global $logger;
	global $auth;

	// toch maar niet tellen?
	if(!$force && tryPar('action') == 'skiptelling') {
		// dat verklikken we wel.
		$data = array (
			'wie' => PersoonView::naam(Persoon::getIngelogd()),
			'wanneer' => getActionDateTime(),
			'probleem' => _('De telling is overgeslagen.') );
		bwmail(0, BWMAIL_TELLINGMELDING, $data);
		return TRUE;
	}

	if(tryPar('addrow') || tryPar('action') != 'dotelling') {
		// als er nog geen data is ingevoerd: haal de gegevens uit de db.
		// anders: haal de gegevens uit de postdata. dat gebeurt als we
		// bv een extra row toevoegen maar wel de ingevulde gegevens willen
		// behouden.
		$teldata = tryPar('teldata');
		if(!$teldata) {
			$data = sqlVoorraadLijst(TRUE, FALSE);
		} else {
			$data = array();
			$boekdata = sqlBoekenGegevens(array_keys($teldata));
			foreach($teldata as $EAN => $d) {
				$data[] = array_merge (
					array(
						'voorraad' => $d['voorraad'],
						'buitenvk' => $d['buitenvk'],
						'ejbv_voorraad' => $d['ejbv_voorraad']
					),
					$boekdata[$EAN]
				);
			}
		}

		$logger->info('Boekentelling op data ' . print_r($data));

		// doe een boekentelling, of niet
		if($force) {
			printHTML('<em>'._('Dit is een verplichte telling.').'</em>');
		} else {
			simpleForm('skiptelling');
			endForm(_('telling overslaan'), FALSE);
		}

		simpleForm('dotelling', 'class="bw_datatable_medium"');

		echo '<tr><th rowspan="2">auteur</th><th rowspan="2">titel</th>'.
			'<th rowspan="2">druk</th><th rowspan="2">ISBN</th><th colspan="3">aantal</th></tr>'.
			'<tr><th>voorraad</th><th>buiten vk</th><th>EJBV voorraad</th></tr>';

		foreach($data as $r) {
			echo '<tr><td>'.$r['auteur'].'</td><td>'.$r['titel'].'</td>' .
				'<td>' . (isset($r['druk'])?$r['druk']:'&nbsp;') . '</td>' .
				'<td>' . print_EAN($r['EAN']) . '</td>' .
				'<td>' . '<input type="text" name="teldata['.$r['EAN'].'][voorraad]" size="5"'.
					' value="'.($force?'':$r['voorraad']).'"></td>'.
				'<td><input type="text" name="teldata['.$r['EAN'].'][buitenvk]" size="5"'.
					' value="'.($force?'':$r['buitenvk']).'"></td>'.
				'<td><input type="text" name="teldata['.$r['EAN'].'][ejbv_voorraad]" size="5"'.
					' value="'.($force?'':$r['ejbv_voorraad']).'"></td>'.
				"</tr>\n";
		}

		// een rijtje toevoegen?
		if(tryPar('addrow')) {
			$EAN = requirePar('addEAN');

			// erg omslachtig. staat de EAN al in de tabel?
			$found = FALSE;
			foreach($data as $values) {
				if($values['EAN'] == $EAN) {
					printHTML('<tr><td colspan="5"><b>Dat ISBN staat al in de lijst!</b></td></tr>');
					$found = TRUE;
				}
			}

			// voeg een lege rij toe
			if(!$found) {
				$r = sqlBoekGegevens(bookweb_get_ean($EAN, true));
				if($r) {
					echo '<tr><td>'.$r['auteur'].'</td><td>'.$r['titel'].'</td><td>'.
						(isset($r['druk'])?$r['druk']:'&nbsp;') . '</td><td>' .
						print_EAN($r['EAN']) . '</td>' .
						'<td><input type="text" name="teldata['.$r['EAN'].'][voorraad]" size="5"></td>'.
						'<td><input type="text" name="teldata['.$r['EAN'].'][buitenvk]" size="5"></td>'.
						'<td><input type="text" name="teldata['.$r['EAN'].'][ejbv_voorraad]" size="5"></td>'.
						"</tr>\n" ;
				} else {
					printHTML('<tr><td colspan="5"><b>Geen boek gevonden adhv ISBN <i>'.
						print_EAN($EAN).'</i>!</b></td></tr>');
				}
			}
		}


		echo '</table><p><b>Nog meer artikelen?</b> Voeg een extra rij toe voor ISBN: '.
			'<input type="text" name="addEAN" size="11" /> '.
			'<input type="submit" name="addrow" value="rij erbij" /></p>';

		$telling_start = tryPar('telling_start');
		if(!$telling_start) $telling_start = date('Y-m-d H:i:s');
		echo '<p><b>Opmerkingen?</b><br><textarea name="telopm" rows="3" cols="80"></textarea></p>'.
			addHidden('telling_start', $telling_start);

		normalEndForm(' Telling invoeren ');

		return FALSE;
	}

	$telnr = sqlCreateTelling($auth->getLidnr(), tryPar('telopm'), tryPar('telling_start'));

	$teldata = requirePar('teldata');

	telling_diff($teldata, tryPar('telopm'));
	sqlInsertTellingItems($telnr, $teldata);

	printHTML('Telling ingevoerd, bedankt.');

	return TRUE;
}


// bereken het verschil tussen de voorraad en een ingevoerde
// telling. Als er verschil tussen zit mailen we dat naar de boekcom.
function telling_diff($teldata, $telopm = '') {

	$voorraad = sqlVoorraadLijst(TRUE, FALSE);
	$diff = array();
	$telopm = trim($telopm);

	// ga alles af, controleer elk EAN of die hetzelfde is als op voorraad
	foreach($voorraad as $row) {
		$EAN = $row['EAN'];
		foreach (array('voorraad', 'buitenvk', 'ejbv_voorraad') as $locatie) {
			if (!isset($teldata[$EAN][$locatie])
				|| trim($teldata[$EAN][$locatie]) == '') {
				$teldata[$EAN][$locatie] = 0;
			}
		}

		if( $row['voorraad'] != $teldata[$EAN]['voorraad'] ||
			$row['buitenvk'] != $teldata[$EAN]['buitenvk'] ||
			$row['ejbv_voorraad'] != $teldata[$EAN]['ejbv_voorraad']) {

			$diff[$EAN] = array (
				'tel' => $teldata[$EAN],
				'real' => $row );
		}
		// unset dit ean, aan het eind houden we dan de boeken over die
		// wel geteld zijn maar niet op voorraad waren
		unset($teldata[$EAN]);
	}

	// als er verschillen of extra boeken zijn, mailen
	if(count($diff) > 0 || count($teldata) > 0) {
		$mail = '';
		foreach($diff as $EAN => $data) {
			$mail .= print_EAN($EAN).":\n".
				"   Geteld:   ".$data['tel']['voorraad'].'/'.$data['tel']['buitenvk'].'/'.$data['tel']['ejbv_voorraad']."\n".
				"   Voorraad: ".$data['real']['voorraad'].'/'.$data['real']['buitenvk'].'/'.$data['real']['ejbv_voorraad']."\n\n";
		}
		foreach($teldata as $EAN => $data) {
			$mail .= "Extra boek ".print_EAN($EAN).": ".$data['voorraad'].'/'.$data['buitenvk'].'/'.$data['ejbv_voorraad']."\n\n";
		}
		$mail .= "\n(Tellingen zijn weergegeven als voorraad/buitenvk/ejbv_voorraad)\n";
		$mail .= "\nOpmerkingen van de teller:\n\"".$telopm.'"';

		$maildata = array (
			'wie' =>  PersoonView::naam(Persoon::getIngelogd()),
			'wanneer' => getActionDateTime(),
			'probleem' => 'Er zijn verschillen tussen de telling en de voorraad volgens BookWeb:'.
				"\n\n".$mail);
		bwmail(0, BWMAIL_TELLINGMELDING, $maildata);
	} elseif (@$telopm) {
		// alleen opmerkingen geplaatst, verder alles ok.
		$maildata = array (
			'wie' =>  PersoonView::naam(Persoon::getIngelogd()),
			'wanneer' => getActionDateTime(),
			'probleem' => 'De volgende opmerking is geplaatst:'.
				"\n\n\"".$telopm.'"');
		bwmail(0, BWMAIL_TELLINGMELDING, $maildata);

	}

	return;
}

/* sluit een boekverkoop OF corrigeer een kastelling */
function sluitverkoop($verkoopdag = null)
{
	if(!$verkoopdag)
		$verkoopdag = getActionDate();

	$verkoopgeld = sqlGetVerkoopGeld($verkoopdag);

	if (!$verkoopgeld) {
		print_error("Er is op $verkoopdag geen boekverkoop geopend, er valt
			dus niks te sluiten of corrigeren");
	}
	/* Controleer of er deze dag nog niet geteld is */
	if(!go() && tryPar('action') != 'skiptelling' && count(sqlGetTellingen($verkoopdag)) < 1) {
		printHTML('Er is vandaag nog geen telling geweest. Dit is je kans.');
		if(!telling()) {
			return;
		}
	}

	$alingevuld = (bool)@$verkoopgeld['sluitstamp'];

	if (tryPar('action') == 'go') {
		$contant   = Money::parseMoney(tryPar('contant'), true, 'contant');
		$pin1      = Money::parseMoney(tryPar('pin1'), true, 'pin1');
		$pin2      = Money::parseMoney(tryPar('pin2'), true, 'pin2');
		$debiteur  = Money::parseMoney(tryPar('debiteuren'), true, 'debiteuren');
		$crediteur = Money::parseMoney(tryPar('crediteuren'), true, 'crediteuren');
		$mutatie   = Money::parseMoney(tryPar('mutatie'), true, 'mutatie');
		$donaties  = Money::parseMoney(tryPar('donaties'), true, 'donaties');
	} else {
		$contant = $alingevuld?$verkoopgeld['cash']:0;
		$pin1 = $alingevuld?$verkoopgeld['pin1']:0;
		$pin2 = $alingevuld?$verkoopgeld['pin2']:0;
		$debiteur = $alingevuld?$verkoopgeld['debiteuren']:0;
		$crediteur = $alingevuld?$verkoopgeld['crediteuren']:0;
		$mutatie = $alingevuld?$verkoopgeld['mutaties']:0;
		$donaties = $alingevuld?$verkoopgeld['donaties']:0;
	}

	echo "<h2> ".
		($alingevuld ? "Corrigeer einddedagtellingen":"Sluit boekverkoop" ) .
		"</h2>\n";

	if(tryPar('action') == 'go') {
		$verterug  = sqlVerTeruggekocht($verkoopdag);
		$betaalmethoden = sqlGetBetaalmethodenByDate($verkoopdag);
		$kasverschillen = sqlGetVerklaardeKasverschillen($verkoopdag, TRUE);
		$verkocht  = $verterug['verkoop'];
		$terugkoop = $verterug['terugkoop'];
		$verkochtamount = array_sum(unzip($verkocht,'totaal')) -
			array_sum(unzip($terugkoop,'totaal'));
		$totaal = $contant + $pin1 + $pin2 + $debiteur - $crediteur;
		$extrapassiva = $mutatie + $donaties + $kasverschillen;
		$kasverschil = $totaal - $verkochtamount - $extrapassiva;

		global $BETAALMETHODEN;

		$methodeverschil = array();

		foreach (array_merge($BETAALMETHODEN['+'], $BETAALMETHODEN['-'])
			as $methode => $dummy)
		{
			$eindedagwaarde = ${$methode};
			$verkooptotaal  = @$betaalmethoden[$methode];

			if (!isset($eindedagwaarde)) {
				$eindedagwaarde = 0;
			}
			if (!isset($verkooptotaal)) {
				$verkooptotaal = 0;
			}
			if (round($eindedagwaarde-$verkooptotaal, 2) != 0.0) {
				$methodeverschil[$methode] = $eindedagwaarde - $verkooptotaal;
			}
		}

		$nettomethodeverschil = array_sum($methodeverschil);

		if (round($kasverschil*1000) == 0.0) {
			$kasverschil = 0;
		}
		$a = intval($nettomethodeverschil) - intval($extrapassiva);
		if (tryPar('accepteer_verschil') && tryPar('kv_reden') != '') {
			/* Het kasverschil is geaccepteerd */
			sqlInsKastelling($verkoopdag, $contant, $pin1, $pin2,
				$debiteur, $crediteur, $mutatie, $donaties, $kasverschil,
				tryPar('kv_reden'));

			printHTML($alingevuld ? ('De eindedagtellingen van '
				.print_date($verkoopdag).' zijn gecorrigeerd')
				:('De verkoop van '.print_date($verkoopdag).' is afgesloten.'));

			toonverkopen($verkoopdag);
			if ($kasverschil != 0) {
				printHTML('Er is nog onverklaard kasverschil. Je kunt nu een '
				.makeBookRef('kasverschillen', 'verklaring').' invoeren.');
			}
			return;
		} elseif ( sizeOf($methodeverschil) > 0 && ($a != 0 ||
				$nettomethodeverschil == 0 ) )
		{
			/* Er zijn andere bedragen voor de betalingsmethoden ingevoerd dan
			 * volgens Bookweb berekend is EN dit verschil is niet toevallig de
			 * extra passiva */
			printHTML('Jouw invoerde bedragen van de volgende betaalmethoden '
				.'kloppen niet met het totaal van de in Bookweb geregistreerde '
				.'transacties voor deze betaalmethode:', TRUE);
			echo "<ul>\n";
			foreach ($methodeverschil as $methode => $verschil) {
				if ($verschil < 0) {
					$verschil = Money::addPrice(-$verschil).' te weinig';
				} else {
					$verschil = Money::addPrice($verschil).' te veel';
				}
				echo '<li class=waarschuwing>'.ucfirst($methode)
					.': Hier heb je '.$verschil." ingevoerd\n";
			}
			echo "</ul>\n";
			printHTML('Wanneer je er van overtuigd bent dat de toch de juiste '
				.'bedragen hebt ingevoerd, voer dan een '
				.makeBookRef('kasverschillen','kasverschilverklaring')
				.' of een donatiebedrag in.', TRUE);
		} elseif ($kasverschil != 0) {
			/* Er is nog een kasverschil */
			printHTML('<font size="+2">Er is een onverklaard kasverschil, '
				.'verklaar je nader.</font>', TRUE);
		} elseif ($kasverschil == 0 ||
			 $a == 0)
		{
			/* Nu kan de verkoop veilig gesloten worden */
			sqlInsKastelling($verkoopdag, $contant, $pin1, $pin2,
				$debiteur, $crediteur, $mutatie, $donaties, $kasverschil,
				tryPar('kv_reden'));

			printHTML($alingevuld ? ('De eindedagtellingen van '
				.print_date($verkoopdag).' zijn gecorrigeerd')
				:('De verkoop van '.print_date($verkoopdag).' is afgesloten.'));

			toonverkopen($verkoopdag);
			if ($kasverschil != 0) {
				printHTML('Er is nog onverklaard kasverschil. Je kunt nu een '
				.makeBookRef('kasverschillen', 'verklaring').' invoeren.');
			}
			return;
		}

		$verkoopgeld['mutaties'] = $mutatie;
		$verkoopgeld['donaties'] = $donaties;
		$verkoopgeld['pin1']    = $pin1;
		$verkoopgeld['pin2']    = $pin2;
		$verkoopgeld['cash']     = $contant;
		$verkoopgeld['debiteuren'] = $debiteur;
		$verkoopgeld['crediteuren'] = $crediteur;

		toonverkopen($verkoopdag, $verkoopgeld);
	}

	simpleForm();
	if (tryPar('action') == 'go') {
		echo '<tr><td colspan=2><em>Kun je (een deel van) het '
			.makeBookRef('kasverschillen', 'kasverschil verklaren')
			.'? Doe dit dan eerst!</em><br>&nbsp;</td></tr>';
		echo addHidden('accepteer_verschil', TRUE);
	}
	echo Money::moneyInput('Contanten','contant',$contant)
	   . Money::moneyInput('Pin1','pin1',$pin1)
	   . Money::moneyInput('Pin2','pin2',$pin2) 
	   . Money::moneyInput('Debiteuren (bijv. facturen)','debiteuren',$debiteur)
	   . Money::moneyInput('Crediteuren (bijv. terugkopen)','crediteuren',$crediteur)
	   . Money::moneyInput('Mutaties','mutatie',$mutatie)
	   . Money::moneyInput('Donaties','donaties',$donaties);

	if (tryPar('action') == 'go') {
		echo '<tr><td valign=top><strong>Toelichting onverklaarbaar kasverschil:'
			.'</strong></td><td>'.addTextArea('kv_reden', NULL, 40, 4)
			.'</td></tr>';
		endForm('Accepteer bovenstaande gegevens');
	} else {
		endForm('Voer in');
	}
}


function verkoopbesteld($embedded = FALSE)
{
	requireVerkoopOpen();

	if(!$embedded && tryPar('action') == 'doshop') {

		checkRedo('besteldnaarkart');
		$bestellevs = tryPar('bestellevs');
		if (!$bestellevs) {
			print_error('Je moet natuurlijk wel een boek aanvinken');
		}
		$geselecteerd = array();
		foreach ($bestellevs as $bestellev) {
			list($bestelnr,$voorraadnr) = explode('-', $bestellev);
			if (isset($geselecteerd[$bestelnr])) {
				print_error("Je mag per bestelling maar 1 alternatief aanvinken!");
			}
			$geselecteerd[$bestelnr] = $voorraadnr;
		}

		$boekres = sqlBestelBoekInfoRes($geselecteerd);

		foreach($boekres as $data) {
			bw_addToKart(
				$geselecteerd[$data['bestelnr']], // voorraadnr
				cut_auteur($data['auteur']).': '.$data['titel'], // omschrijving
				1, // aantal
				$data['prijs'], // verkoopprijs
				$data['btwtarief'], //btwtarief
				$data['btw'], //btw
				array($data['bestelnr']), // studentbestelnummer
				'voorraad' // voorraadlocatie waarvandaan verkocht wordt (in dit geval altijd 'voorraad')
			);
		}

		verkoopMenu();

		return;
	} // action doshop


	/* Geleverde bestellingen die niet meer op voorraad zijn. */
	$lastigebestellingen = $lopendebest = array();
	$bestellingen = sqlGetBestellingen(getSelLid());

	$voorraadnrs = sqlBestelnrs2Voorraadnrs(array_keys($bestellingen));

	/* Controleer voor alle 'geleverde' EANs of er nog echt boeken op
	 * voorraad zijn. */
	foreach ($bestellingen as $bestelnr => $bestelling) {
		if ($bestelling['geleverd'] &&
				!($bestelling['vervallen'] || $bestelling['gekocht']) &&
				!@($voorraadnrs[$bestelnr])) {
			// er is geen voorraad bij dit bestelnr (hetzij de key met
			// dit bestelnr bestaat niet, of het is een lege array)
			$lastigebestellingen[$bestelnr] = $bestelling;
		}
		if(!$bestelling['vervallen'] && !$bestelling['gekocht']) {
			$lopendebest[$bestelnr] = $bestelling;
		}
	}

	if (sizeOf($lastigebestellingen) > 0) {
		echo '<P>Helaas zijn er een of meerdere van de geleverde '
			.'bestellingen uitverkocht, doordat er bijvoorbeeld '
			.'een beschadigd boek is omgeruild. Om deze reden is '
			.'er nu een nieuw boek voor deze student besteld bij '
			.'de leverancier. De student heeft hier '
			.'zojuist ook een mailtje over ontvangen.<BR>';

		foreach ($lastigebestellingen as $bestelnr => $bestelling) {
			$afbesteller = 'Bookweb';
			$reden = 'Voorraadproblemen met dit ISBN';
			$lidnr = $bestelling['lidnr'];

			sqlOnlever($bestelnr, SB_PRIO_VOORRAADPROB);
			bwmail($lidnr
				,BWMAIL_SB_VERTRAAGD_DOOR_VOORRAADPROBLEEM
				,$bestelling);
		}
		echo arrayToHTMLTable($lastigebestellingen, array
			('EAN'   => 'ISBN'
			,'titel'  => 'Titel'
			,'auteur' => 'Auteur'
			,'druk'   => 'Druk'
			,'vak'    => 'Vak'));
	}

	if (sizeof($lopendebest) == 0) {
		if ($embedded) {
			echo '(geen lopende bestellingen)';
		} else {
			echo '<p>Het geselecteerde lid heeft geen lopende '
				.'bestellingen. Wellicht is er echt niet besteld, '
				.'zijn ze al verkocht, of juist verlopen? Zie: '
				.makeBookRef('bestelling', 'bestelstatus controleren').'.</p>';
			verkoopMeer();
		}
		return;
	}

	/* Wat preprocessing voor de geleverde boeken */
	$disabledCheckbox = '<input type="checkbox" disabled="1">';
	$tabel = array();

	$sbnrs_in_kart = array();
	foreach(bw_getKart() as $voorr => $arr) {
		$sbnrs_in_kart = array_merge($sbnrs_in_kart, $arr['sbnrs']);
	}

	foreach ($lopendebest as $bestelnr => $besteldata) {
		if ($besteldata['url']) {
			$besteldata['vak'] = '<a href="'.$besteldata['url'].'">'
				.$besteldata['vak'].'</a>';
		}

		// is deze bestelling er al?
		if (!$besteldata['geleverd']) {
			$besteldata['kruisje'] = $disabledCheckbox;
			$besteldata['levstatus'] = 'Nog niet geleverd';
			$tabel[] = $besteldata;
			continue;
		}

		// hebben we nog wel voorraad?
		if (!@$voorraadnrs[$bestelnr]) {
			$besteldata['kruisje'] = $disabledCheckbox;
			$besteldata['levstatus'] = 'Niet meer aanwezig';
			$tabel[] = $besteldata;
			continue;
		}

		// zit dit item al in de kart?
		if(in_array($bestelnr, $sbnrs_in_kart)) {
			$besteldata['kruisje'] = $disabledCheckbox;
			$besteldata['levstatus'] = 'Al in kart';
			$tabel[] = $besteldata;
			continue;
		}

		$n = count($voorraadnrs[$bestelnr]);
		$i = 1;
		$first = TRUE;
		foreach ($voorraadnrs[$bestelnr] as $voorraadnr => $voorraad) {
			// we vinken steeds de goedkoopste voorraad voor een boek,
			// de verkoper kan dat eventueel overrijden maar het is
			// de meest zinnige default.
			$besteldata['kruisje'] = addCheckBox('bestellevs[]', $first
				, "$bestelnr-$voorraadnr");
			if ($n != 1) {
				$besteldata['kruisje'] .= " ($i/$n)";
				$i++;
				$first = FALSE;
			}
			$besteldata['levstatus'] = "Nog	$voorraad[voorraad] over";
			$besteldata['prijs'] = berekenVerkoopPrijs(
				$voorraad['levprijs'], $voorraad['boekenmarge'],
				@$voorraad['adviesprijs']);
			$tabel[] = $besteldata;
		}
	}

	if (!$embedded) {
		printHTML('De volgende boeken zijn besteld door het '
			.'geselecteerde lid:');
	}
	if (sizeof($tabel) > 0) {
		startForm('verkoopbesteld',null,null,null,'doshop');
		addRedo('besteldnaarkart');
		echo '<tr><td>';
		echo arrayToHTMLTable($tabel, array
				('kruisje' => ''
				,'prijs'   => 'Prijs'
				,'levstatus'   => 'Voorraad'
				,'titel'   => 'Titel'
				,'auteur' => 'Auteur'
				,'EAN' => 'ISBN'
				,'vak' => 'Vak'
				),
				array('prijs' => 'Money::addPrice', 'EAN' => 'print_EAN_bi'),
				array('prijs' => 'right'));
		echo '</td></tr>';
	}

	if(isset($n)) {
	// $n wordt alleen geset als er daadwerkelijk iets geleverd is
		if (!$embedded) {
			echo '<TR><TD colspan=\'7\'>Klik de te verkopen boeken '
				.'aan.</TD></TR>';
		}
		endForm('Inladen in de Winkelwagen',FALSE);
	} else {
		echo '<tr><td colspan=\'7\'><em>Geen te verkopen '
			.'boeken.</em>'
			.'</td></tr></table></form>';
	}
}

function verkoopMeer() {
	echo makeBookRef('verkoop', 'Verkoopmenu');
}

function verkooppakket()
{
	requireVerkoopOpen();

	$pakketnr = requirePar('pakketnr');
	$pakketdata = sqlGetPakketData($pakketnr);
	$force = tryPar('force', FALSE);

	$alinkart = $nietopvoorraad = array();
	/* Loop alle artikelen uit het pakket 1 maal langs */
	foreach ($pakketdata as $voorraadnr => $voorraaddata) {
		// Voorraadlocatie waarvandaan het artikel verkocht dient te worden:
		$voorraadlocatie = $voorraaddata['voorraadlocatie'];

		/* Check of er wel een voorraadnummer bestaat. Zo niet, dan zijn er
		 * geen voorraadnummers die bij dit pakketnr horen */
		if ($voorraadnr === '' && sizeof($pakketdata) == 1) {
			printHTML('Let op: Je probeert een leeg pakket te verkopen. Vraag '
				.'aan de Boekencommissaris waarom dit pakket (#'.$pakketnr.': '
				.$voorraaddata['naam'].') leeg is.', TRUE);
			return;
		}

		/* Check of het artikel toevallig al in de kart zit */
		$inkart = bw_checkKart($voorraadnr);
		if ($inkart > 0) {
			$alinkart[$voorraadnr] = $inkart;
		}

		/* Check of het artikel nog op voorraad is */
		if ($voorraaddata[$voorraadlocatie] - $inkart <= 0) {
			$nietopvoorraad[$voorraadnr] = TRUE;
		}
	}

	if (sizeof($alinkart) > 0 && !$force) {
		printHTML('Deze artikelen zitten al in je kart:', TRUE);
		echo "<ul>\n";
		foreach ($alinkart as $voorraadnr => $aantal) {
			echo '<li>'.$pakketdata[$voorraadnr]['titel'].' van '
				.$pakketdata[$voorraadnr]['auteur']
				.' ('.$pakketdata[$voorraadnr]['aantal']." maal)\n";
		}
		echo "</ul>\n";
		printHTML('Kies \'toch doorgaan\' als je ze toch in je kart erbij '
			.'wilt stoppen.');
	}

	if (sizeof($nietopvoorraad) > 0 && !$force) {
		printHTML('Deze artikelen zijn niet meer op voorraad:', TRUE);
		echo "<ul>\n";
		foreach ($nietopvoorraad as $voorraadnr => $dummy) {
			echo '<li>'.$pakketdata[$voorraadnr]['titel'].' van '
				.$pakketdata[$voorraadnr]['auteur']." (van voorraadlocatie '$voorraadlocatie')\n";
		}
		echo "</ul>\n";
		printHTML('Kies \'toch doorgaan\' als je toch de rest van het pakket '
			.'in je kart wilt stoppen.');
	}

	/* Er is extra verificatie nodig */
	if ((sizeOf($nietopvoorraad) != 0 || sizeOf($alinkart) != 0) && !$force) {
		simpleForm();
		echo addHidden('force', TRUE);
		addRedo();
		endForm('Toch doorgaan', FALSE);
		return;
	}

	/* Pop artikelen van het pakket in kart (let op aantal per EAN) */
	$leveranciersdata = sqlLeveranciers();
	foreach ($pakketdata as $voorraadnr => $voorraaddata) {
		if (isset($nietopvoorraad[$voorraadnr])){
			// Artikel is niet op voorraad, maar gebruiker wil toch doorgaan
			continue;
		}

		$prijs = berekenVerkoopPrijs($voorraaddata['levprijs'],
			$leveranciersdata[$voorraaddata['leveranciernr']]['boekenmarge'],
			@$voorraaddata['adviesprijs']
		);

		bw_addToKart($voorraadnr,
			cut_auteur($voorraaddata['auteur']).': '.$voorraaddata['titel'], // omschrijving
			$voorraaddata['aantal'], $prijs, getBtwtarief($voorraadnr), getBtw($voorraadnr),// aantal en prijs en btwtarief en btw
			null, // geen nummer van studentbestelling
			$voorraaddata['voorraadlocatie'] // voorraadlocatie waarvandaan het artikel moet komen
		);
	}

	verkoopMenu();
}

function verkooponbesteld() {
	global $BOEKENPLAATSEN;
	requireVerkoopOpen();

	if(tryPar('action') == 'verkoop') {
		// Product daadwerkelijk toevoegen aan kart

		$EAN = requireEAN();
		checkRedo($EAN);

		$voorraadnr_locatie = explode("-", requirePar('voorraadnr_voorraadlocatie'));
		$voorraadnr = $voorraadnr_locatie[0];
		$voorraadlocatie = $voorraadnr_locatie[1];
		Voorraad::checkVoorraadLocatie($voorraadlocatie);

		$boekinfo = sqlAlleFasenInfo(array($EAN));
		$inkart = bw_checkKart($voorraadnr);
		$totaalvr = $boekinfo[$EAN]['fase_3'] - $inkart;

		$aantal = requirePar('aantal');

		if ($aantal > $totaalvr) {
			print_error("H&eacute;, je probeert meer artikelen ($aantal) te verkopen ".
				"dan er zijn ($totaalvr)! Dat kan helaas (nog) niet.");
		}

		$vdata = sqlGetVoorraadInfo($voorraadnr);

		$prijs = berekenVerkoopPrijs($vdata['levprijs'],
				$vdata['boekenmarge'], @$vdata['adviesprijs']);

		bw_addToKart(
			$voorraadnr, cut_auteur($vdata['auteur']).': '.$vdata['titel'],
			$aantal, $prijs, getBtwtarief($voorraadnr), getBtw($voorraadnr), null, // aantal, prijs, btwtarief, btw en nummer van studentbestelling
			$voorraadlocatie // voorraadlocatie waarvandaan het artikel verkocht wordt
		);

		verkoopMenu();
		return;
	}

	// Gebruiker de beschikbare opties presenteren, na gecontroleerd te hebben
	// of het artikel nog wel voorradig is
	$EAN = requireEAN();

	$faseinfo = sqlFaseInfo('3', array($EAN));
	$faseinfo = $faseinfo[$EAN];

	printHTML("Losse verkoop van <i>$faseinfo[titel]</i> van <i>$faseinfo[auteur]</i> ".
		(@$faseinfo['druk'] ? ('druk '.$faseinfo['druk'].' '): '').'...');

	$voorraden = VoorraadSet::createByEAN($EAN);
	if ($voorraden->getVoorraad("voorraad") + $voorraden->getVoorraad("ejbv_voorraad") == 0){
		// Helemaal geen voorraad beschikbaar (ook niet artikelen die
		// toegewezen zijn aan een student)

		if ($voorraden->getVoorraad("buitenvk") > 0){
			printHTML('Van dit artikel liggen slechts nog exemplaren '
				.'buiten verkoop. Misschien wil de boekencommissaris '
				.'deze artikelen voor je verplaatsen naar de reguliere '
				.'verkoop. Voordat dit gebeurd is, kun je dit artikel '
				.'helaas niet verkopen.');
		} else {
			printHTML('Van dit artikel is niets meer op voorraad, dus '
				.'je kunt het ook niet verkopen.');
		}
		printHTML(makeBookRef('boekinfo','BoekInfo','EAN='.$EAN));
		printHTML(verkoopMeer());
		return;
	}

	if ($faseinfo['aantal'] <= 0) {
		// Totaal aantal boeken in fase 3 is 0 (of minder). Dat wil zeggen dat
		// er geen boeken meer voor de vrije verkoop beschikbaar zijn, maar
		// eerder is gecontroleerd of er uberhaupt nog boeken waren. Ergo: er
		// zijn nog wel boeken, maar niet in vrije verkoop!

		printHTML('<div class="waarschuwing"><b>LET OP: Van dit boek '
			.'zijn geen exemplaren vrij voor de losse '
			.'verkoop.</b></div> Je kunt ervoor kiezen toch het boek '
			.'te verkopen, maar de kans is aanwezig dat er binnenkort '
			.'een student langskomt die een mail gehad heeft dat \'ie '
			.'dit boek kan komen ophalen, terwijl dat boek er dus niet '
			.'meer is, omdat jij nu zijn boek hebt verkocht.');
	}

	$tabel = array();
	$num_voorraden = $voorraden->size(); // aantal voorraden
	$i = 1;
	$first = TRUE;
	$mogelijke_locaties = array("voorraad", "ejbv_voorraad");

	foreach ($voorraden as $voorraadnr => $voorraad) {
		foreach ($mogelijke_locaties as $voorraadlocatie){
			$aantal_beschikbaar = $voorraad->getVoorraad($voorraadlocatie) - bw_checkKart($voorraadnr, $voorraadlocatie);
			if ($aantal_beschikbaar == 0){
				// geen voorraad meer op deze locatie
				continue;
			}


			$row = array();

			$row['voorraadlocatie'] = $BOEKENPLAATSEN[$voorraadlocatie];
			$row['status'] = "Nog $aantal_beschikbaar beschikbaar voor verkoop";

			if ($voorraadlocatie == 'ejbv_voorraad'){
				$row['status'] .= "<br/><em>Let op! Deze boeken zijn speciaal gereserveerd voor de EJBV! Doe dit alleen als je zeker weet waar je mee bezig bent!";
			}

			// we vinken steeds de goedkoopste voorraad voor een boek,
			// de verkoper kan dat eventueel aanpassen maar het is
			// de meest logische default.
			$row['kruisje'] = addRadioBox(
				'voorraadnr_voorraadlocatie',
				$first && $voorraadlocatie != 'ejbv_voorraad',
				$voorraadnr . "-$voorraadlocatie"
			);

			$row['prijs'] = $voorraad->berekenVerkoopPrijs();

			$tabel[] = $row;

			$faseinfo['aantal'] -= bw_checkKart($voorraadnr, $voorraadlocatie);
			if ($first) $first = false;
		}
	}

	printHTML("In totaal ".($faseinfo['aantal']==1?"is":"zijn") .
		" er <b>". max(0, $faseinfo['aantal']) ."</b> exempla".($faseinfo['aantal']==1?"ar":"ren").
		" van dit artikel vrij voor in de losse verkoop. Uit welke voorraad had je dit artikel willen verkopen?<br />\n"
	);
	simpleForm('verkoop');

	echo arrayToHTMLTable($tabel, array
			('kruisje' => ''
			,'prijs'   => 'Prijs'
			,'voorraadlocatie' => 'Voorraadlocatie'
			,'status' => 'Status'
			),
			array('prijs' => 'Money::addPrice'),
			array('prijs' => 'right'))
		."</TR>\n";

	echo '<tr><td colspan="2">Aantal: '. addInput1('aantal','1',FALSE,3)."</td></tr>\n";

	echo addHidden('EAN', $EAN);
	echo addHidden('page', 'verkooponbesteld');
	echo addHidden('fase3', $faseinfo['aantal']);

	addRedo($EAN);

	endForm('Naar Kart');
}

/* Laat het menu zien dat hoort bij de functie toonVerkopenPerProduct */
function menuToonVerkopenPerProduct(){
	echo "<h2>Verkopen per product</h2>\n";
	echo "<p>Geef een zoekterm op in onderstaand invulformulier om de verkopen
	van het gezochte product te zien. Als je een nieuwe term invult, dan
	wordt het resultaat daarvan toegevoegd aan het resultaat. Je kunt dus
	van meerdere producten tegelijk de verkopen zien.</p>\n";
	echo "<hr />\n";

	// kommagescheiden lijst met geselecteerde eans
	$selectedEANs = tryPar("selectedEANs", "");
	$datumvan = tryPar("datumvan", "");
	$datumtot = tryPar("datumtot", "");
	// Array van maken
	if ($selectedEANs != ""){
		@$selectedEANs = explode(",", $selectedEANs);
	}

	if (!$selectedEANs) $selectedEANs = array();

	if(tryPar('EAN')) {
		$EAN = requireEAN();

		$selectedEANs[] = $EAN;
	}
	$selectedEANs = array_unique($selectedEANs);
	$selectedEANsStr = implode(",", $selectedEANs);

	echo addForm('', 'GET').
	addHidden('page', 'toonverkopenperproduct').
	addHidden('selectedEANs', htmlspecialchars(@$selectedEANsStr));

	echo "<table widht=\"500\">".
		"<tr>" .
			"<td>".
				"EAN/ISBN/A123/E123/auteur/titel: ".
			"</td><td>".
				addInput1('EAN', htmlspecialchars(@$EAN), FALSE, 30).
			"</td>" .
		"</tr>\n" .
		"<tr>" .
			"<td>" .
				"Datumselectie: " .
			"</td><td>" .
				addInput1("datumvan", htmlspecialchars($datumvan), FALSE, 10) .
				" tot " .
				addInput1("datumtot", htmlspecialchars($datumtot), FALSE, 10) .
			"</td>" .
		"</tr>";

	/*if (hasAuth("god")){
		// Mogelijkheid tonen om oneindig ver in het verleden te zoeken
		echo "<tr><td>&nbsp;</td><td>".
			addCheckbox("ignorePrivacy", tryPar("ignorePrivacy", false)) .
			" Onbeperkt zoeken in het verleden (alleen voor Gods)" .
			"</td></tr>";
	}*/

	echo "<tr><td>&nbsp;</td><td>";
	normalEndForm('opzoeken');
	echo "</td></tr></table>";

	echo "<hr />\n";
	if (sizeof($selectedEANs) > 0) {
		toonVerkopenPerProduct($selectedEANs, true, true, $datumvan, $datumtot);
	}
}

/**
	Toont transactiegroepen van de gegeven datum, gegroepeerd op
	transactie. Dus de verkoop van 2 collegeblokken en 2 boeken door 1
	student op hetzelfde moment wordt gegroepeerd getoond.
	Op deze manier kun je pinbonnen/kasboekjes checken.
*/
function toonTransactieGroepen($datum, $verkopers = null){
	$verkopen = sqlGetVerkopenPerTransactieGroep($datum, $verkopers);

	// geen detail (!$detail) ==> geen transacties tonen, maar aggregeren op lid, timestamp
	$geenDetail = tryPar("geendetail", false);
	$detail = !$geenDetail;

	echo "<h3>Verkopen gegroepeerd per transactie</h3>";
	echo '<table class="bw_datatable_medium_100">' .
		'<tr>' .
			'<th>Moment</th>' .
			'<th>Lid</th>' .
			'<th>Product(en)</th>' .
			'<th>Aantal</th>' .
			'<th>Geslaagd</th>'.
			'<th>Stuksprijs</th>' .
			'<th>BTW</th>' .
			'<th>Totaal</th>' .
		"</tr>\n";

	$firstrow = true;
	foreach ($verkopen as $datum=>$lidverkoop){
		if (!$firstrow && $detail) {
			echo "<tr><td>&nbsp;</td></tr>\n";
		}
		$firstrow = false;
		foreach ($lidverkoop as $lidnr=>$verkopen){
			$totaal = 0;
			$lastwanneer = null;
			$lastlidnr = null;
			$lastverkoper = null;
			$methodes = array();

			if ($detail){
				// Alle transacties tonen
				foreach ($verkopen as $verkoopinfo){
					$wanneer = $verkoopinfo["wanneer"];
					$lidnr = $verkoopinfo["lidnr"];
					$verkoper = $verkoopinfo["wie"];

					echo "<tr>";

					echo '<td valign="top">';
					if ($wanneer != $lastwanneer){
						$lastwanneer = $wanneer;
						echo $wanneer;
					} else {
						echo "&nbsp;";
					}
					echo "</td>";

					echo '<td valign="top">';
					if ($lidnr != $lastlidnr){
						$lastlidnr = $lidnr;
                        $persoon = Persoon::geef($verkoopinfo["lidnr"]);
                        if($persoon !== null)
                        {
						    echo $verkoopinfo["lidnr"] . ": " . PersoonView::makeLink($persoon);
                        }
                        else
                        {
                            echo $verkoopinfo["lidnr"] . ": Persoon onbekend!";
                        }
					} else {
						echo "&nbsp;";
					}
					echo "</td>";

					$subtotaal = $verkoopinfo["aantal"] * $verkoopinfo["studprijs"];

					echo '<td valign="top">' . cut_titel($verkoopinfo["titel"]) . "</td>";
					echo '<td valign="top" align="center">' . $verkoopinfo["aantal"] . "</td>";
					echo "<td valign ='top' align='right'> ".HtmlSpan::maakBoolPicto($verkoopinfo['Geslaagd']). "</td> ";
					echo '<td valign="top" align="right">' . Money::addPrice($verkoopinfo["studprijs"]) . "</td>";
					echo '<td valign="top" align="right">' . Money::addPrice($verkoopinfo["btw"]) . '</td>';
					echo '<td valign="top" align="right">' . Money::addPrice($subtotaal) . "</td>";
					echo "</tr>";

					$totaal = $totaal + $subtotaal;
					$methodes[] = $verkoopinfo["betaalmethode"];
					$firstrow = false;
				}

				// Optelling
				$methodes = array_unique($methodes);
				$methodes = implode(", ", $methodes);
				echo '<tr><td colspan="5">&nbsp;</td>';
				echo '<td style="border-top: double black" align="right">'.
					"$methodes: <strong>" . Money::addPrice($totaal) . '</strong>' .
					"</td></tr>";
			} else {
				// Geen detail: dus niet alle transacties tonen maar alleen
				// het totaalbedrag
				$aggrTotaal = 0;

				foreach ($verkopen as $verkoopinfo){
					$wanneer = $verkoopinfo["wanneer"];
					$lidnr = $verkoopinfo["lidnr"];
					$verkoper = $verkoopinfo["wie"];

					$subtotaal = $verkoopinfo["aantal"] * $verkoopinfo["studprijs"];

					$aggrTotaal += $subtotaal;
				}

				echo "<tr>" .
					"<td>$wanneer</td>" .
					"<td>" .
						PersoonView::makeLink(Persoon::geef($verkoopinfo["lidnr"])) .
					"</td>" .
					"<td>Diverse producten</td>" .
					"<td>" . sizeof($verkopen) . "</td>" .
					"<td>n.v.t.</td>" .
					"<td>" . Money::addPrice($aggrTotaal) . "</td>" .
					"</tr>\n";
			}
		}
	}
	if (sizeof($verkopen) == 0){
		echo '<tr><td colspan="6" align="center">' .
			'Er zijn geen verkopen om weer te geven' .
			'</td><tr>';
	}
	echo "</table>";
}

/* Laat alle verkopen zien in de geschiedenis van een bepaald EAN!*/
function toonVerkopenPerProduct($eans, $laatsteVerkoopEerst = true,
	$ignorePrivacy = false, $datumvan = null, $datumtot = null)
{

	$verkopen = sqlGetVerkopenPerProduct(
		$eans, $laatsteVerkoopEerst, $ignorePrivacy, $datumvan, $datumtot
	);

	echo "Verkopen voor de producten:<br/>\n";
	$artikelgegevens = array();
	$kopers = array();
	foreach ($eans as $ean){
		$productInfo = sqlBoekGegevens($ean);
		echo "\"" . $productInfo["titel"] . "\" (" . print_EAN_bi($ean) . ")<br/>\n";
		$artikelgegevens[$ean] = $productInfo;
	}
	if (sizeof($eans) > 0){
		echo "<a href=\"?page=toonverkopenperproduct\" target=\"_self\">";
		echo "(nieuwe zoekopdracht)";
		echo "</a><br/><br/>\n";
	}

	if (sizeof($verkopen) > 0){
		echo "<div style=\"font-size: 8pt\">\n";
		echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
		echo "	<tr>\n";
		echo "		<th>Datum</th>\n";
		echo "		<th>Product</th>\n";
		echo "		<th>Lid</th>\n";
		echo "		<th>Aantal</th>\n";
		echo "		<th><font size=\"1\">leveranciers</font><br/>Prijs</th>\n";
		echo "		<th>Totaal</th>\n";
		echo "	</tr>\n";

		$totaalAantal = 0;
		foreach($verkopen as $verkoop){
			$productInfo = $artikelgegevens[$verkoop["EAN"]];

			$totaalAantal = $totaalAantal + $verkoop["aantal"];
			$totaalPrijs = $verkoop["aantal"] * $verkoop["levprijs"];
			if(isset($verkoop["lidnr"])) {
				$persoon = Persoon::geef($verkoop["lidnr"]);
                if($persoon !== null)
                {   
                    $koper = PersoonView::makeLink($persoon);
                }
                else
                {
                    $koper = $verkoop["lidnr"].":Onbekend";
                }
				$kopers[] = $verkoop["lidnr"];
			} else {
				$koper = "Onbekend";
			}
			echo "	<tr>\n";
			echo "		<td>" . $verkoop["wanneer"] . "</td>\n";
			echo "		<td>" . $productInfo["titel"] . "</td>\n";
			echo "		<td>" . $koper . "</td>\n";
			echo "		<td align=\"right\">" . $verkoop["aantal"] . "</td>\n";
			echo "		<td align=\"right\">" . Money::addPrice($verkoop["levprijs"]) . "</td>\n";
			echo "		<td align=\"right\">" . Money::addPrice($totaalPrijs) . "</td>\n";
			echo "	</tr>\n";
		}
		echo "	<tr>\n";
		echo "		<td>&nbsp;</td>\n";
		echo "		<td>&nbsp;</td>\n";
		echo "		<td valign=\"bottom\" align=\"right\">Totaal aantal:</td>\n";
		echo "		<td valign=\"bottom\" style=\"border-top: black double\">$totaalAantal</td>\n";
		echo "	</tr>\n";
		echo "</table>\n";
		echo "</div>\n";

		if (sizeof($verkopen) > 0){
			// Leden in kart shoppen?
			$strlidnrs = implode(",", $kopers);
			
			$div = new HtmlDiv();
			$form = HtmlForm::named('KartToevoegen');
			$form->setAttribute('action','/Service/Kart/Toevoegen');	
			$form->getToken()->setTargetURI("/Service/Kart/Toevoegen");		
			foreach($kopers as $koper) 
			{
				$form->add(HtmlInput::makeHidden("LidVerzameling[$koper]", 'on'));
			}
			$form->add(array(new HtmlSpan(_("Klik ")), $button = HtmlInput::makeSubmitButton(_('hier')), new HtmlSpan(_(" om bovenstaande mensen in je kart te shoppen. Zo kun je ze lekker makkelijk inschrijven voor een
			activiteit in de Who's Who!"))));
			$div->add($form);
			$button->setAttribute('style', 'display:inline;clear:none');

			
			echo $div->makeHtml();
		}
	} else {
		echo "Er zijn geen verkopen gevonden die voldoen aan de gegeven
		eisen!";
	}
	/*echo "<br/><br/>";
	echo "Let op! Uit privacyoverwegingen worden alleen verkopen van de
	laatste 31 dagen getoond!";*/

}

/* Laat alle verkopen zien van de gevraagde datum. $verkoopgeld wordt
 * meegegeven indien beschikbaar, zodat dat niet nog een keer gequeried hoeft
 * te worden
 * verkopers is een array met daarin lidnrs van verkopers. Alleen
 * informatie van die verkopers zal worden getoond. Overigens mag het ook 1
 * lidnr zijn (dus geen array) */

function toonverkopen($verkoopdag = null, $verkoopgeld = null, $selectedVerkopers = null)
{
	/* embedded: of de output gebruikt wordt in een andere functie (true), of
	 * zelfstandig (false) */
	$embedded = (bool)$verkoopdag;

	// checken of $selectedVerkopers wel een array is.
	if ($selectedVerkopers != null && !is_array($selectedVerkopers)){
		$selectedVerkopers = array($selectedVerkopers);
	}

	if (!$verkoopdag) $verkoopdag = getActionDate();
	$alleVerkopers	= sqlGetDagVerkopers($verkoopdag);
	$verterug       = sqlVerTeruggekocht($verkoopdag, $selectedVerkopers);
	$verkocht       = $verterug['verkoop'];
	$teruggekocht   = $verterug['terugkoop'];
	// als verkoopgeld meegegeven wordt, neem die. Wordt nu iig gebruikt bij
	// het afsluiten/corrigeren van een eindedagtelling, zodat deze functie
	// alvast de nieuw in te vullen waarden weergeeft
	if (!$verkoopgeld) $verkoopgeld = sqlGetVerkoopGeld($verkoopdag);
	if (!$verkoopgeld) {
		print_error("Er is niet eens deze dag een verkoop geopend, kan dus ook
			verkopen niet laten zien.");
	}

	echo "<h2> Boekverkoop van ".print_date($verkoopdag). "</h2>\n";

	echo "Geopend: ".  print_datetime($verkoopgeld['openstamp']) .
		' (door ' . PersoonView::naam(Lid::geef($verkoopgeld['verkoper'])) . ')' .
		'<br/>' .
		'Gesloten: ' .
		(isset($verkoopgeld['sluitstamp'])?print_datetime($verkoopgeld['sluitstamp']):'<i>nog open</i>') .
		'<br/>' .
		'Verkopers: ';
	if (sizeof($alleVerkopers) == 0){
		echo '(geen)';
	} elseif (sizeof($alleVerkopers) == 1){
		echo PersoonView::naam(Persoon::geef($alleVerkopers[0]));
	} else {
		echo addForm("","GET");
		echo addHidden("page","toonverkopen");
		echo addHidden("datum", tryPar("datum", ""));
		echo "<div style=\"margin-left: 20px\">";
		foreach ($alleVerkopers as $verklidnr){
			$inArray = is_array($selectedVerkopers) && in_array($verklidnr, $selectedVerkopers);
			$legeArray = is_array($selectedVerkopers) && sizeof($selectedVerkopers) == 0;
			$selected = $selectedVerkopers === null || $inArray || $legeArray;

			echo addCheckBox("verkopers[]", $selected, $verklidnr);
			echo PersoonView::naam(Lid::geef($verklidnr)) . "<br/>";
		}
		echo "</div>";
		echo addCheckBox("geendetail", tryPar("geendetail"));
		echo "Transacties aggregeren per lid, datum/tijd<br/>";
		echo "<input type=\"submit\" value=\"Show me!\" class=\"bw_button\" />";
		echo "</form>";
	}

	$totaalverkocht = $totaalteruggekocht = 0;
	if ($verkocht) {
		printHTML('<h3>Verkochte artikelen</h3>');
		echo arrayToHTMLTable($verkocht,
			array('EAN'			=>	'ISBN'
				 ,'titel'		=>	'Titel'
				 ,'auteur'		=>	'Auteur'
				 ,'aantal'		=>	'Aantal'
				 ,'studprijs'	=>	'Prijs'
				 ,'btw'			=>	'BTW'
				 ,'totaal'		=>	'Subtotaal'),
			array('EAN' => 'print_EAN_bi', 'studprijs' => 'Money::addPrice', 'btw' => 'Money::addPrice',
				'totaal' => 'Money::addPrice'),
			array('aantal' => 'center', 'studprijs' => 'right', 'btw' => 'right',
				'totaal' => 'right'), null, "bw_datatable_medium_100");
		$totaal = 0.0;
		foreach ($verkocht as $verkoop) {
			$totaalverkocht += $verkoop['totaal'];
		}
		echo '<div align="right" width="100%">' .
			'<strong>'.
			'Totaal verkocht: '. Money::addPrice($totaalverkocht).
			'</strong>'.
			'</div>';
	}
	if ($teruggekocht) {
		printHTML('<h3>Teruggekochte artikelen</h3> ');
		echo arrayToHTMLTable($teruggekocht,
			array('EAN'=>'ISBN'
				 ,'titel'		=>	'Titel'
				 ,'auteur'		=>	'Auteur'
				 ,'aantal'		=>	'Aantal'
				 ,'studprijs'	=>	'Prijs'
				 ,'totaal'		=>	'Subtotaal'),
			array('EAN' => 'print_EAN_bi', 'studprijs' => 'Money::addPrice',
				'totaal' => 'Money::addPrice'),
			array('aantal' => 'center', 'studprijs' => 'right',
				'totaal' => 'right'), null, "bw_datatable_medium_100");
		$totaal = 0.0;
		foreach ($teruggekocht as $terugkoop) {
			$totaalteruggekocht += $terugkoop['totaal'];
		}
		echo '<div align="right" width="100%">' .
			'<strong>'.
			'Totaal teruggekocht: '. Money::addPrice($totaalteruggekocht) . '<br/>' .
			'Netto verkocht: ' . Money::addPrice($totaalverkocht - $totaalteruggekocht) .
			'</strong>'.
			'</div>';

	}

	if ( !($verkocht || $teruggekocht) ) {
		// Er zijn geen boeken/artikelen 'over de toonbank' gegaan.
		printHTML('Er zijn deze dag geen transacties geweest');
	} else {
		toonTransactieGroepen($verkoopdag, $selectedVerkopers);
	}

	if ($selectedVerkopers == $alleVerkopers || $selectedVerkopers === null) {
		if ($embedded || @$verkoopgeld['sluitstamp']) {
			printBalans($verkoopgeld, $totaalverkocht - $totaalteruggekocht);

			if (!$embedded) {
				echo "<center>\n";
				echo makeBookRef('sluitverkoop', 'Herzie de ingevulde
					eindedagwaardes').'<br />';
				echo makeBookRef('kasverschillen',
					'Bekijk en herzie de verklaring van de kasverschillen');
				echo "</center>\n";
			}
		} else {
			echo '<h2> De verkoop van deze dag is niet afgesloten, dus is er geen '
				.'eindedagtelling gedaan. </h2>';
			echo makeBookRef('sluitverkoop', 'Sluit deze boekverkoop alsnog');
		}
	} else {
		echo "<br/><strong>Let op!</strong> Je hebt een selectie gemaakt van een subset van de verkopers
		van deze dag, dat wil zeggen dat niet alle verkopen/terugkopen
		hierboven getoond worden. Als gevolg daarvan kan er ook geen
		correcte balans getoond worden.";
	}
}

/**
	Dit is een soort pretty-printer voor een rij in verkoopgeld. Er wordt
	een strak "balans"-tabelletje voor geprint. Verwacht wordt dus een
	array met velden uit de tabel "verkoopgeld" en het totaalbedrag van
	transacties (verrekend met terugkopen)
*/
function printBalans($verkoopgeld, $transacties){
	$verkoopdag = $verkoopgeld["verkoopdag"];

	echo "<center>\n<h2>Balans</h2>\n";

	echo "<table frame=box rules=groups cellpadding=3>\n"
		."<colgroup span=2>\n<thead>\n";
	echo '<tr><th colspan=2>Activa</th>'
		.'<th colspan=2>Passiva</th></tr></thead>';
	echo '<tr><td>Pin1 inkomsten:</td><td align=right>'
		.Money::addPrice($pin1 = $verkoopgeld['pin1']).'</td>'
		.'<td>Totaal transacties:</td><td align=right>'
		.Money::addPrice($transacties)
		.'</td></tr>';
	echo '<tr><td>Pin2 inkomsten:</td><td align=right>'
		.Money::addPrice($pin2 = $verkoopgeld['pin2']).'</td>';
	echo '<td>Mutaties:</td><td align=right>'
		.Money::addPrice($mutaties = $verkoopgeld['mutaties']).'</td></tr>';
	echo '<tr><td>Kas inkomsten:</td><td align=right>'
		.Money::addPrice($contant = $verkoopgeld['cash']).'</td>'
		.'<td>Donaties:</td><td align=right>'
		.Money::addPrice($donaties = $verkoopgeld['donaties']).'</td></tr>';
	echo '<tr><td>Debiteuren:</td><td align=right>'
		.Money::addPrice($debiteur = $verkoopgeld['debiteuren'] +
			@$verkoopgeld['facturen']).'</td>'
		.'<td>Crediteuren:</td><td align=right>'
		.Money::addPrice($crediteur = $verkoopgeld['crediteuren']).'</td></tr>';

	//Legacy verkoopmanieren, maar moeten wel in de administratie blijven.
	if($verkoopgeld['pin'] != 0) {
		echo '<tr><td><i>Pin-oud inkomsten:</i></td><td align=right>'
			.Money::addPrice($pin = $verkoopgeld['pin']).'</td>'
			.'<td></td><td align=right>'
			.'</td></tr>';
	}
	if($verkoopgeld['pinII'] != 0) {
		echo '<tr><td><i>Pin I&I inkomsten:</i></td><td align=right>'
			.Money::addPrice($pinII = $verkoopgeld['pinII']).'</td>'
			.'<td></td><td align=right>'
			.'</td></tr>';
	}

	$activa = @$pin + @$pinII + $pin1 + @$pin2 + $contant + $debiteur;
	$passiva = $transacties + $mutaties + $donaties + $crediteur;
	$kasverschillen = sqlGetVerklaardeKasverschillen($verkoopdag, TRUE);
	if ($kasverschillen != 0) {
		echo '<tr>';
		if ($kasverschillen < 0) {
			$activa += -$kasverschillen;
			echo '<td><a href="./Kasverschillen">Verklaarde kasverschillen:</a></td><td align=right>'
				.Money::addPrice(-$kasverschillen).'</td>'
				.'<td></td><td align=right></td></tr>';
		} elseif ($kasverschillen > 0) {
			$passiva += $kasverschillen;
			echo '<td></td><td align=right></td>'
				.'<td><a href="./Kasverschillen">Verklaarde kasverschillen:</a></td><td align=right>'
				.Money::addPrice($kasverschillen).'</td></tr>';
		}
	}
	echo '<tfoot><tr><td><b>Totaal:</b></td><td align=right><b>'
		.Money::addPrice($activa).'</b></td>'
		.'<td><b>Totaal:</b></td><td align=right><b>'
		.Money::addPrice($passiva).'</b></td></tr></tfoot>';
	echo '</colgroup></table>';

	$verschil = $activa - $passiva;
	if (abs($verschil) < 1e-8) {
		$verschil = 0.0;
	}
	echo '<h3> '.($verschil != 0?'<font color=red>':'')
		.'Onverklaard kasverschil: '.Money::addPrice($verschil)
		.($verschil != 0?'</font>':'').'</h3>';

	echo "</center>\n";

}

/* Laat overzicht zien van alle verklaarde kasverschillen op een bepaalde
 * verkoopdag.
 */
function verklaardeKasverschillen($verkoopdag, $data = NULL)
{
    global $auth;

	// mag verklaren als verkoop open is
	$verkoopgeld = sqlGetVerkoopGeld($verkoopdag);
	$mayedit = empty($verkoopgeld['sluitstamp']) && !empty($verkoopgeld);

	if ($mayedit && tryPar('action') == 'updateverschil') {
		checkRedo();

		$new = tryPar('new');
		$wis = tryPar('wis');

		/* Zou er iets toegevoegd zijn? */
		if (isset($new['kasverschil']) && $new['kasverschil']) {
			$new['kasverschil'] = Money::parseMoney($new['kasverschil']);
			if ($new['reden']) {
				$new['wie'] = $auth->getLidnr();
				sqlInsertKasverschil($verkoopdag, $new);
				$nieuwverschil = finGetVerkoopdagGelden($verkoopdag);
				printHTML('De wijzigingen zijn doorgevoerd.<br />'
					.'<strong>Let op: je moet de boekverkoop van deze dag '
					.makeBookRef('sluitverkoop', 'opnieuw sluiten')
					.' met het nieuwe kasverschil (<i>'
					.Money::addPrice($nieuwverschil['kasverschil_onverklaard'])
					.'</i>) wanneer je klaar bent met de wijzigingen '
					.'alhier.</strong>');
			} else {
				printHTML('<font color=red>Je moet wel een reden opgeven '
					.'voor het kasverschil! Hoe kun je het anders '
					.'\'verklaard\' noemen?</font>');
			}
		}

		/* Moeten er verklaarde kasverschillen gewist worden? */
		if (isset($wis)) {
			sqlDeleteKasverschillen($verkoopdag, array_keys($wis));
		}
		/* Geen break, zodat er meer ingevoerd kan worden */
	}

	if (!isset($data)) {
		$data        = sqlGetVerklaardeKasverschillen($verkoopdag);
		$verkoopbedragen = finGetVerkoopdagGelden($verkoopdag, $verkoopgeld);
		if (abs($verkoopbedragen['kasverschil_onverklaard']) > 1e-8) {
			echo '<h2><font color=red>Nog onverklaard kasverschil: '
				.Money::addPrice($verkoopbedragen['kasverschil_onverklaard'])
				.'</font></h2>';
		} else {
			echo '<h2>Geen <i>onverklaarde</i> kasverschillen</h2>';
		}
	}

	echo '<h2>Verklaarde kasverschillen</h2>';

	simpleForm('updateverschil');
	echo addHidden('datum', $verkoopdag);
	addRedo();

	$totaal = 0.0;
	if (sizeOf($data) > 0) {
		echo '<tr><th>Bedrag</th><th>Reden</th><th>Wis verklaring</th></tr>';
		foreach ($data as $volgnr => $row) {
			echo '<tr valign=top><td>'.Money::addPrice($row['kasverschil']).'</td>'
				.'<td><cite>'.htmlspecialchars($row['reden']).'</cite>' .
				'<br />&nbsp;--&nbsp;' . PersoonView::naam(Persoon::geef($row['wie'])) . '</td>'
				.'<td align=center>'.addCheckBox("wis[$volgnr]", false, '', $mayedit).'</td>'
				."</tr>\n";
			$totaal += $row['kasverschil'];
		}
	}
	echo '<tr><td colspan=3><br><b>Voeg verklaring toe</b></td></tr>';
	echo '<tr valign=top><td>&euro;&nbsp;'
		.addInput1('new[kasverschil]',
			(!@$new['reden']?@$new['kasverschil']:''), FALSE, 4,0,$mayedit)
		.'</td>'.'<td>'.addTextArea('new[reden]', '', 80, 4, null, $mayedit).'</td>'
		.'<td>&nbsp;</td></tr>';
	endForm('Voer wijzigingen door', FALSE);

	if ( !$mayedit ) {
		print_warning('Je kunt alleen verklaringen toevoegen of bewerken als de verkoop geopend is.');
	}
}

function toonTeVerkopenBesteldeBoeken($datum) {

	if (!$datum) {
		$datum = time();
	}
	$bestellingen = sqlTeVerkopenBoeken(array());

	if (sizeOf($bestellingen) == 0) {
		printHTML('Er zijn geen bestellingen die momenteel ('
			.date('d-m-Y',$datum).') kunnen worden opgehaald.');
		return;
	} else {
		printHTML('Voor de volgende studenten liggen op '
			.date('d-m-Y', $datum).' de volgende boeken klaar om verkocht '
			.'te worden');
	}

	$namen = getLedenNames( unzip($bestellingen,'lidnr') );
	foreach ($bestellingen as $bestelling) {
		$naam = htmlspecialchars($namen[$bestelling['lidnr']]);
		$bestelling['naam'] = "$naam ($bestelling[lidnr])";

		if (!$bestelling["euro"]){
			$bestelling["euro"] = $bestelling["levprijs"];
		}

		$newbestellingen[] = $bestelling;
	}
	echo arrayToHTMLTable($newbestellingen, array
		('naam'        => 'Naam (lidnr)'
		,'EAN'         => 'ISBN'
		,'titel'       => 'Boek'
		,'auteur'      => 'Auteur'
		,'druk'        => 'Druk'
		,'vervaldatum' => 'Vervaldatum'
		,'euro'        => 'Prijs'),
		array('EAN' => 'print_EAN_bi', 'vervaldatum' => 'print_date',
			'euro' => 'Money::addPrice', 'titel' => 'cut_titel', 'auteur' =>
			'cut_auteur'),
		array('vervaldatum' => 'right', 'euro' =>
		'right'), null, "bw_datatable_medium_100");
}

/**
 * Het menu voor boekverkopers
 */
function verkoopmenu() {

requireVerkoopOpen();

?>
<div class="row">
<div class="col-md-6">
<h3 class="page-header">Verkoop</h3>
<ul>
<li>Verkoop van bestelde artikelen:<br>
<?	verkoopbesteld(TRUE);
	?><p>

<li><?
if(isActionDateTimeChanged())
	echo '<span style="background-color: red">Losse verkoop:</span>';
else
	echo 'Losse verkoop:';
?>
<form action="" method="get">
<input type="hidden" name="page" value="verkooponbesteld">
<input type="text" name="EAN" value="" size="15">
<input type="submit" value=" verder &raquo; ">
</form><p>

<?
$pakketten = sqlGetPakketten();
if (sizeOf($pakketten) > 0) {
	echo '<li>';

	if(isActionDateTimeChanged()) {
		echo '<span style="background-color: red">Pakketten:</span>';
	} else {
		echo 'Pakketten:';
	}

	echo "<table>\n";

	foreach($pakketten as $pakketnr => $pakket) {
		echo "<tr><td>".makeBookRef('verkooppakket',
			htmlspecialchars($pakket['naam']), 'pakketnr='.$pakketnr)
			."</td></tr>\n";
	}
	echo "</table><p>\n</div>";
}
?>

<? $arts = sqlGetArtikelen();
$dictaten = array();
$boeken = array();
$rest = array();
foreach($arts as $art) {
	if($art['btw'] > 0)
	{$dictaten[] = $art['EAN'];}	
	elseif($art['leveranciernr'] == "1" || $art['leveranciernr'] == "3")
	{$boeken[] = $art['EAN'];}
	elseif(true)
	{$rest[] = $art['EAN'];}
}

echo '<div class="col-md-6">';
if(isActionDateTimeChanged())
	echo '<span style="background-color: red">Dictaten:</span>';
else
	echo '<h3 class="page-header">Dictaten:</h3>';
	if(sizeof($dictaten) !== 0)    
	losseItemsVerkoopForm($dictaten);
	echo '</div>';	

echo '<div class="col-md-6">';
if(isActionDateTimeChanged())
	echo '<span style="background-color: red">Overigen:</span>';
else
	echo '<h3 class="page-header">Overigen:</h3>';
	if(sizeof($rest) !== 0)
	losseItemsVerkoopForm($rest);
	echo '</div>';	


echo '<div class="col-md-6">';
if(isActionDateTimeChanged())
	echo '<span style="background-color: red">Boeken:</span>';
else
	echo '<h3 class="page-header">Boeken:</h3>';
	if(sizeof($boeken) !== 0)
	losseItemsVerkoopForm($boeken);
	echo '</div></div>';//Sluit ook de row div	

?>
<p>

<form action="" method="get">
<li>Omruilen:
<input type="hidden" name="page" value="omruil">
<input type="text" name="EAN" value="" size="20">
<input type="submit" value=" verder &raquo; ">
</form><br />
<i><font color="red"><b>LET OP:</b></font> van ruilen komt huilen!</i><p>

<li><a href="/Onderwijs/Boeken/">Hoofdmenu</a>
</ul>
<?php

}

/**
 * Omruilen
 */
function omruilen() {

	$EAN = requireEAN();
	$boekinfo = sqlBoekGegevens($EAN);

	$faseinfo = sqlAlleFasenInfo(array($EAN));
	$fi = $faseinfo[$EAN];

	$vk = sqlFindVerkoopForTerugkoop($EAN, getSelLid());
	if(count($vk) == 0) {
		printHTML("Dit artikel is niet aan dit lid verkocht, of is reeds eerder ".
			"teruggekocht.");
			verkoopMeer();
			return;
	}

	switch($act = tryPar('action')) {
		case 'tokart':
			checkRedo('terugkoopnaarkart');
			$verkoopnrs = requirePar('verkoopnr');
			$plaats = requirePar('plaats');

			foreach($verkoopnrs as $vknr => $tkaantal) {
				$num_in_kart = -bw_checkKart('tk'.$vknr);

				$tkaantal = (int)$tkaantal;
				if($tkaantal == 0) continue;

				if($tkaantal > ($vk[$vknr]['aantal'] - $num_in_kart)) {
					print_error("Je wilt meer exemplaren terugkopen ($tkaantal) dan ".
						"er toen verkocht zijn (".($vk[$vknr]['aantal']-$num_in_kart).
						"). Dat kan niet.");
				}
				$vkdata = sqlGetVerkoopData($vknr);
				
				bw_addToKart('tk'.$vknr, cut_auteur($boekinfo['auteur']).': '.$boekinfo['titel'],
					-$tkaantal, $vkdata['studprijs'], getBtwtarief($vkdata['voorraadnr']), getBtw($vkdata['voorraadnr']), null, $plaats);
			}

			// direct omruilen:
			// in dit geval moeten we ook een nieuw exemplaar in de kart bokken
			if(tryPar('prevaction') == 'interactiveswap') {
				verkooponbesteld();
			} else {
				verkoopMenu();
			}
			break;

		case 'interactiveswap':
		case 'terugkoop':

			echo "<h2>Omruilen: ";
			echo htmlspecialchars($boekinfo['titel'])."</h2>\n";

			printHTML("Van dit artikel zijn de volgende verkopen aan dit lid bekend.
				Geef aan hoeveel exemplaren je terug wilt kopen.");
			startForm('omruil');
			addRedo('terugkoopnaarkart');
			echo addHidden('action', 'tokart').
				addHidden('prevaction', $act);
			echo "<table>\n";
			echo "<tr><th>aantal<br>terugkopen</th><th>datum</th><th>aantal<br>verkocht</th><th>prijs</th></tr>\n";
			foreach($vk as $verkoopnr => $vkdata) {
				$num_in_kart = -bw_checkKart('tk'.$verkoopnr);
				if($vkdata['aantal'] - $num_in_kart == 0) continue;

				echo "<tr><td align=\"center\">".
					addInput1('verkoopnr['.$verkoopnr.']',
						$vkdata['aantal']-$num_in_kart,null,'3').
					"</td><td>".print_date($vkdata['wanneer']).
					"</td><td align=\"center\">".($vkdata['aantal']-$num_in_kart).
					"</td><td>".Money::addPrice($vkdata['studprijs']).
					"</td></tr>\n";
			}

			global $BOEKENPLAATSEN;
			echo "</table>\n\n<table><tr><td>Waar ga je het artikel terugleggen?</td><td>".
				addSelect('plaats', $BOEKENPLAATSEN, TRUE,'buitenvk')."</td></tr>\n";

			endForm('Naar kart',FALSE);
			break;
		default:

			echo "<h2>Omruilen: ";
			echo htmlspecialchars($boekinfo['titel'])."</h2>\n";

			echo "<p>Er zijn <b>$fi[vr_34]</b> artikelen op voorraad,
				waarvan <b>$fi[fase_3]</b> vrij.<br>Zeg het maar:</p>";

			$stock_avail = $fi['vr_34'] > 0;
			$stock_free  = $fi['fase_3'] > 0;

			echo "<ul>\n".
			"<li>". makeCondBookRef('omruil',
					'Direct omruilen', !$stock_avail, false, 'action=interactiveswap&EAN='.$EAN).
					($stock_avail && !$stock_free ?
						" <span class=\"waarschuwing\">(stelt iemand teleur)</span>": '').
					"<p>".
			"<li>Nieuw exemplaar bestellen: ".
				($fi['boek_bestelbaar'] == 'Y' ?
					makeBookRef('bestellen', 'normale prioriteit',
						'action=confirmbestel&EANs['.$EAN.']=1&prioriteit=' .
						SB_PRIO_REGULIER ) . ' | '.
					makeBooKRef('bestellen', 'hoge prioriteit',
						'action=confirmbestel&EANs['.$EAN.']=1&prioriteit=' .
						SB_PRIO_OMRUIL )
					: '<span class="disabled">artikel is niet bestelbaar</span>' );

			echo "<p>".
			"<li>".makeBookRef('omruil', 'Geld teruggeven',
				'action=terugkoop&EAN='.$EAN) .
			"</ul>";
	}

}


/**
 * Neem de inhoud van het wagentje en ga die afrekenen:
 * registreer de verkopen, update de studentbestellingen.
 */

function checkout()
{
	global $session;

	checkRedo('kart');

	global $BETAALMETHODEN;
	$methoden = array_merge($BETAALMETHODEN['+'], $BETAALMETHODEN['-']);

	$t=array_keys(requirePar('betaal'));
	$betaalmethode = array_shift($t);

	echo '<h2>Checkout: '.$methoden[$betaalmethode]."</h2>\n\n";

	$kart = bw_getKart();

	$sbnrs = $verkoopnrs = $displaytable_vk = $displaytable_tk = $terugk = array();
	$totaalbedrag = 0;
	foreach($kart as $verkoopnr => $vdata) {
		// filter de terugkopen eruit
		if(substr($verkoopnr,0,2) == 'tk') {
			$terugk[substr($verkoopnr,2)] = $vdata;
			continue;
		}

		// hier nog een soort extra check ofzo? of is de controle in sqlInsVerkoop genoeg?
		if(!$session->has('wagentjeuseBTW') || $session->get('wagentjeuseBTW')) {
			$verkoopnrs[$verkoopnr] = array(
				'aantal' => $vdata['aantal'],
				'vkprijs' => $vdata['prijs'],
				'btw' => $vdata['btw'],
				'voorraadlocatie' => $vdata['plaats']
			);

			$sbnrs = array_merge($sbnrs, $vdata['sbnrs']);

			$displaytable_vk[] = array(
				'aantal' => $vdata['aantal'],
				'omschrijving' => $vdata['omschrijving'],
				'prijs'	=> $vdata['prijs'],
				'totaal' => $vdata['prijs']*$vdata['aantal'],
				'btw' => $vdata['btw']*$vdata['aantal'] );
			$totaalbedrag += $vdata['prijs']*$vdata['aantal'];
		} else {
			$verkoopnrs[$verkoopnr] = array(
				'aantal' => $vdata['aantal'],
				'vkprijs' => $vdata['prijs']-$vdata['btw'],
				'btw' => 0,
				'voorraadlocatie' => $vdata['plaats']
			);

			$sbnrs = array_merge($sbnrs, $vdata['sbnrs']);

			$displaytable_vk[] = array(
				'aantal' => $vdata['aantal'],
				'omschrijving' => $vdata['omschrijving'],
				'prijs'	=> $vdata['prijs']-$vdata['btw'],
				'totaal' => ($vdata['prijs']-$vdata['btw'])*$vdata['aantal'],
				'btw' => 0 );
			$totaalbedrag += ($vdata['prijs']-$vdata['btw'])*$vdata['aantal'];
		}
	}

	$isNieuwLid = false;

	// invoeren die hap
	if(count($verkoopnrs) > 0) {
		//Hier gaan we kijken of er kudos verkocht worden en of het betreffende lid/persoon wel kan dibsen
		$kudosVoorraden = sqlGetEANVoorraad('2100000004713');
		$kudos = false;
		foreach($verkoopnrs as $verkoopnr => $verkoopinfo) {
			if(array_key_exists($verkoopnr, $kudosVoorraden)) {
				$kudos = true;
				$kudosVoorraad = $verkoopnr;
				break;
			}
		}

		if($kudos) {
			$pers = Persoon::geef(getSelLid());
			if(!DibsInfo::geef($pers)) {
				print_warning("De persoon waaraan je kudos probeert te verkopen kan nog niet DiBSen. Fix dit eerst even jôh!");
				exit;
			}
		}

		//Als er kudos worden verkocht schrijf die dan automatisch bij bij de persoon!
		if($kudos) {
			$pers = Persoon::geef(getSelLid());
			$info = DibsInfo::geef($pers);
			$aantal = -1*$verkoopnrs[$kudosVoorraad]['aantal'];
			$transactie = new DibsTransactie('BOEKWEB', 'VERKOOP', $info->getPersoon(), $aantal, 'Kudos gekocht bij de bvk', new DateTimeLocale(), 'BVK');
			if(!($error = $info->nieuweTransactie($transactie))) {
				$info->opslaan();
				sqlInsVerkoop($verkoopnrs, getSelLid(), $betaalmethode, $sbnrs);
				print_warning(_('Kudos verkocht aan persoon! De kudos zijn al meteen bij de persoon bijgeschreven!'));
			} else {
				print_warning(_('Er is iets mis gegaan met het verkopen van de kudos aan het persoon. Waarschijnlijk komt de persoon boven'));
			}
		} else {
			sqlInsVerkoop($verkoopnrs, getSelLid(), $betaalmethode, $sbnrs);
		}

	$eans = sqlVoorraadnrs2EAN(array_keys($kart));

    	//Als je een lidmaatschap hebt gekocht maak het lid dan ook lid.
		$liderror = false;
		foreach ($eans as $ean) {
		if (($ean == '2100000000159') || ($ean == '2100000000678')) {//Als lidmaatschap || Masterlidmaatschap, dan verder.
			if($lid = Lid::geef(getSelLid())) {
				//$lid = Lid::geef(getSelLid());
				if($lid->getLidToestand() != "LID") {
					$lid->setLidToestand("LID");
					$lid->defaultLidInstelling();
				        if($ean == '2100000000678') //Masterlidmaatschap start niet perse op EJVB
				        {
				            $lid->setLidVan(date('Y-m-d'));
					}
				        $lid->opslaan();
					$isNieuwLid = true;
				} else {
					if(!$liderror){
						$liderror = true;
						print_warning("Er is waarschijnlijk een extra lidmaatschap aan dit lid verkocht!");
						user_error(sprintf("Er is waarschijnlijk een extra lidmaatschap aan dit lid verkocht! 
							(ean: %s, contactID: %s)", $ean, $lid->getContactID()), E_USER_WARNING);
					}
				}
			} else if($persoon = Persoon::geef(getSelLid())) {
				$lid = Lid::persoonToLid($persoon);
				$lid->defaultLidInstelling();
				$isNieuwLid = true;
			}
		}
		}
	}

	$eans = sqlVoorraadnrs2EAN(array_keys($kart));
	$boekeninfo = sqlBoekenGegevens($eans);

	if(!is_null(tryPar('stuurbevestiging')))
	{
		$data['producten'] = '';
		$data['products'] = '';
		$totaal = 0;
		$totaalbtw = 0;
		$aantalproducten = 0;
		foreach($kart as $verkoopnr => $vdata) {
			if($vdata['aantal'] < 1)
				continue;
			
			if(!$session->has('wagentjeuseBTW') || $session->get('wagentjeuseBTW')) {
				$info = $boekeninfo[$eans[$verkoopnr]];
				$data['producten'] .= $vdata['aantal'] . 'x ' . 
				$info['titel'] . ' van `' . 
				$info['auteur'] . 
				'` (' . print_EAN($eans[$verkoopnr]) . ')' .
				' a ' . Money::addPrice($vdata['prijs'], false) . ' euro per stuk (waarvan ' . (($vdata['btw']) ? Money::addPrice($vdata['btw'], false) : '0') . ' euro aan btw) = ' . 
				Money::addPrice($vdata['prijs']*$vdata['aantal'], false) . ' euro' . 
				"\n";
				$data['products'] .= $vdata['aantal'] . 'x ' . 
				$info['titel'] . ' from `' . 
				$info['auteur'] . 
				'` (' . print_EAN($eans[$verkoopnr]) . ')' .
				' a ' . Money::addPrice($vdata['prijs'], false) . ' euro each (which ' . (($vdata['btw']) ? Money::addPrice($vdata['btw'],false) : '0') . ' euro to VAT) = ' . 
				Money::addPrice($vdata['prijs']*$vdata['aantal'], false) . ' euro' . 
				"\n";
				$totaal += $vdata['prijs']*$vdata['aantal'];
				$totaalbtw += $vdata['btw']*$vdata['aantal'];
				$aantalproducten += $vdata['aantal'];
			} else {
				$info = $boekeninfo[$eans[$verkoopnr]];
				$data['producten'] .= $vdata['aantal'] . 'x ' . 
				$info['titel'] . ' van `' . 
				$info['auteur'] . 
				'` (' . print_EAN($eans[$verkoopnr]) . ')' .
				' a ' . Money::addPrice($vdata['prijs']-$vdata['btw'], false) . ' euro per stuk (waarvan ' . Money::addPrice(0, false) . ' euro aan btw) = ' . 
				Money::addPrice(($vdata['prijs']-$vdata['btw'])*$vdata['aantal'], false) . ' euro' . 
				"\n";
				$data['products'] .= $vdata['aantal'] . 'x ' . 
				$info['titel'] . ' from `' . 
				$info['auteur'] . 
				'` (' . print_EAN($eans[$verkoopnr]) . ')' .
				' a ' . Money::addPrice($vdata['prijs']-$vdata['btw'], false) . ' euro each (which ' . Money::addPrice(0,false) . ' euro to VAT) = ' . 
				Money::addPrice(($vdata['prijs']-$vdata['btw'])*$vdata['aantal'], false) . ' euro' . 
				"\n";
				$totaal += ($vdata['prijs']-$vdata['btw'])*$vdata['aantal'];
				$totaalbtw += $vdata['btw']*$vdata['aantal'];
				$aantalproducten += $vdata['aantal'];
			}
		}
		
		$data['betaalwijze'] = $methoden[$betaalmethode];
		$data['producten'] .= "\n" . 
					'Totaal ' . $aantalproducten . 
					' product' . ($aantalproducten>1 ? 'en' : '') .
					' met een totaal van ' . Money::addPrice($totaal, false) . ' euro (waarvan ' . Money::addPrice($totaalbtw,false) . ' euro aan btw)'. 
					"\n"; 

		$data['products'] .= "\n" . 
					'Total ' . $aantalproducten . 
					' product' . ($aantalproducten>1 ? 's' : '') .
					' with a total of ' . Money::addPrice($totaal, false) . ' euro (which' . Money::addPrice($totaalbtw,false) . ' euro to VAT)'.
					"\n"; 
		


		$data['aantal'] = $aantalproducten;

		bwmail(getSelLid(), BWMAIL_AANKOOP_BEVESTIGING, $data);
	}

	$tk_voorraadnrs = array();
	// voer de terugkopen in
	foreach($terugk as $vknr => $tkdata) {
		$vkdata = sqlGetVerkoopData($vknr);
		$tk_voorraadnrs[] = $vkdata['voorraadnr'];
		sqlInsTerugkoop($vkdata['voorraadnr'],-$tkdata['aantal'],$tkdata['plaats'],getSelLid(),
			$vknr, $tkdata['prijs'], $betaalmethode);

		$displaytable_tk[] = array(
			'aantal' => -$tkdata['aantal'],
			'omschrijving' => $tkdata['omschrijving'],
			'prijs' => $tkdata['prijs'],
			'totaal' => -$tkdata['prijs']*$tkdata['aantal'] );
		$totaalbedrag += $tkdata['prijs']*$tkdata['aantal'];
	}

	//ProcessTo4 al de teruggekochte EANs
	processTo4(sqlVoorraadnrs2EAN($tk_voorraadnrs));

	// wagentje legen.
	bw_initKart();
	$session->remove('wagentjeuseBTW');

	if(count($displaytable_vk) > 0) {
		echo "<p>Het volgende is verkocht:".
			arrayToHTMLTable($displaytable_vk, array('aantal'=>'#','omschrijving'=>'Omschrijving',
				'prijs'=>'Prijs','totaal'=>'Totaal','btw'=>'BTW'),
				array('prijs'=>'Money::addPrice','omschrijving'=>'htmlspecialchars','totaal'=>'Money::addPrice','btw'=>'Money::addPrice'),
				array('aantal'=>'right','prijs'=>'right','totaal'=>'right','btw'=>'right')) ."</p>";
	}
	if(count($displaytable_tk) > 0) {
		echo "<p>Het volgende is teruggekocht:".
			arrayToHTMLTable($displaytable_tk,  array('aantal'=>'#','omschrijving'=>'Omschrijving',
				'prijs'=>'Prijs','totaal'=>'Totaal'),
				array('prijs'=>'Money::addPrice','omschrijving'=>'htmlspecialchars','totaal'=>'Money::addPrice'),
				array('aantal'=>'right','prijs'=>'right','totaal'=>'right')) ."</p>";
	}

	echo "<p>Betaald per $betaalmethode: ".Money::addPrice($totaalbedrag)."</p>";

	if($isNieuwLid) {
		echo('<p>Lidnummer ' . getSelLid() . ' is nu lid.</p>');
	}

	verkoopMeer();

	return;
}

function printVerkoopOverzicht(){
	global $BWDB;
	$van = tryPar('van');
	$tot = tryPar('tot');
	$crediteurenlijst = tryPar('crediteurenlijst', false);
	$zonderopmaak = tryPar('zonderopmaak', false);
	$groupbyBetaalmethode = tryPar("groupbyBetaalmethode", false);
	if ($crediteurenlijst){
		$zonderopmaak = true;
	}

	// Welke leveranciernrs zijn geselecteerd (default BOEKLEVNR)
	$leveranciernrs = tryPar('leveranciernrs');
	if ($leveranciernrs == null){
		$leveranciernrs = array(BOEKLEVNR);
	}

	if ($van == null){
		// Eerste van deze maand
		$van = date("Y-m-01");
	}
	if ($tot == null){
		// morgen (het is TOT niet tot en met!)
		$morgen = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
		$tot = date("Y-m-d", $morgen);
	}

	echo addForm("", "GET");
	echo addHidden("page", "verkoopoverzicht");
	echo "<br/>\n";
	echo "<span class=\"header3\">Verkoopoverzicht</span>\n";
	echo "<table width=\"100%\">\n";
	echo "	<tr>\n";
	echo "		<td valign=\"top\" width=\"50%\">\n";

	// Datums
	echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
	echo "	<tr><th colspan=\"2\">\n";
	echo "		Datumselectie:";
	echo "	</th></tr>\n";
	echo "	<tr>\n";
	echo "		<td width=\"100\">Vanaf:</td>\n";
	echo "		<td>" . addInput1("van", $van) . "</td>\n";
	echo "	</tr>\n";
	echo "	<tr>\n";
	echo "		<td>Tot:</td>\n";
	echo "		<td>" . addInput1("tot", $tot) . "</td>\n";
	echo "	</tr>\n";
	echo "</table><br/>\n";

	// Opties
	echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
	echo "	<tr><th>Opties</th></tr>\n";
	echo "	<tr><td>";
	echo "		" . addCheckBox("crediteurenlijst", $crediteurenlijst);
	echo " tonen als crediteurenlijst<br/>";
	echo "		" . addCheckBox("zonderopmaak", $zonderopmaak);
	echo " tonen zonder opmaak (eurotekens e.d.)<br/>";
	echo "		" . addCheckBox("groupbyBetaalmethode", $groupbyBetaalmethode);
	echo " splitsen op betaalmethode (PIN, etc.)<br/><br/>";
	echo "Sorteren op: ";
	$sortmogelijkheden = array("EAN" => "ISBN/EAN", "auteur" => "Auteur");
	echo addSelect("order", $sortmogelijkheden, "EAN", true);
	echo "	</td></tr>";
	echo "</table>";
	echo "		</td>\n";

	// Leveranciers
	echo "		<td width=\"50%\">\n";

	// Fancy tabelletje met selectbox met leveranciers
	echo bwfLeveranciers();

	echo "		</td>\n";
	echo "	</tr>\n";

	echo "	<tr><td colspan=\"2\" align=\"center\">\n";
	echo "		<input type=\"submit\" value=\"Zoeken!\"/></td>\n";
	echo "	</td></tr>";

	echo "</table>\n";
	echo "</form><br/></br>\n";

	$order = tryPar("order");
	$alleinfo = sqlVerkoopOverzicht($van, $tot, $leveranciernrs, $order, $groupbyBetaalmethode);

	// Wat simpele postprocessing: groeperen op leverancier, zodat
	// terugkopen als verkopen bij elkaar in de lijst staan
	$newinfo = array();
	foreach ($alleinfo as $voorraadnr => $info){
		$newinfo[$info["leveranciernr"]][$voorraadnr] = $info;
	}

	echo "<table width=\"100%\" class=\"bw_datatable fullwidth smalltext\">\n";
	echo "	<tr>\n";

	if (!$crediteurenlijst){
		echo "		<th>ISBN</strong></th>\n";
		echo "		<th>Titel</strong></th>\n";
		echo "		<th>Auteur</strong></th>\n";
		echo "		<th>Editie</strong></th>\n";
		echo "		<th>Leverprijs</strong></th>\n";
		echo "		<th>Netto verkocht</strong></th>\n";
		echo "		<th>Totaal</strong></th>\n";
		if ($groupbyBetaalmethode){
			echo "		<th>Methode</strong></th>\n";
		}
	} else {
		// Crediteurenlijst
		echo "		<th width=\"1\">Leverancier</th>\n";
		echo "		<th>ISBN</strong></th>\n";
		echo "		<th>Artikel</strong></th>\n";
		echo "		<th>Leverprijs</strong></th>\n";
		echo "		<th>Netto verkocht</strong></th>\n";
		echo "		<th>Totaal</strong></th>\n";
		if ($groupbyBetaalmethode){
			echo "		<th>Methode</strong></th>\n";
		}
	}
	echo "	</tr>\n";

	$grandtotaal = 0;

	$lastLeveranciernr = null;
	foreach ($newinfo as $leveranciernr=>$leverancierinfo){
		foreach ($leverancierinfo as $voorraadnr=>$voorraadinfo){
			$EAN = $voorraadinfo["EAN"];
			$levprijs = $voorraadinfo["levprijs"];
			$leveranciernr = $voorraadinfo["leveranciernr"];

			$verterugkopen = $voorraadinfo["mutaties"];
			foreach ($verterugkopen as $betaalmethode=>$mutatie){
				$verkocht = $mutatie["aantalverkocht"];
				$teruggekocht = $mutatie["aantalteruggekocht"];
				$netto = $verkocht - $teruggekocht;

				if ($EAN != null){
					$boekinfo = sqlBoekGegevens($EAN);

					$isbn = ean_to_isbn($EAN);

					$titel = cut_titel($boekinfo["titel"], true, 40);
					$auteur = cut_auteur($boekinfo["auteur"], true, false, 40);
					$editie = $boekinfo["druk"];
				} else {
					$isbn = "n.v.t.";
					$titel = "n.v.t.";
					$auteur = "n.v.t.";
					$editie = "n.v.t.";
				}
				$totaal = $netto * $levprijs;

				$grandtotaal = $grandtotaal + $totaal;

				echo "	<tr>\n";
				if (!$crediteurenlijst){
					if (!$zonderopmaak){
						echo "		<td>" . print_EAN_bi($EAN) . "</td>\n";
					} else {
						echo "		<td>" . print_EAN($EAN) . "</td>\n";
					}
					echo "		<td>$titel</td>\n";
					echo "		<td>$auteur</td>\n";
					echo "		<td>$editie</td>\n";
					echo "		<td>";
					if ($zonderopmaak){
						echo $levprijs;
					} else {
						echo Money::addPrice($levprijs);
					}
					echo "</td>\n";
					echo "		<td>$netto ($verkocht - $teruggekocht)</td>\n";
					echo "		<td>";
					if ($zonderopmaak){
						echo $totaal;
					} else {
						echo Money::addPrice($totaal);
					}
					echo "</td>\n";

					if ($groupbyBetaalmethode){
						echo "		<td>$betaalmethode</td>\n";
					}
				} else { // Tonen als crediteurenlijst voor afrekening
					if ($leveranciernr != $lastLeveranciernr && $lastLeveranciernr != null){
						// Extra rij invoegen voor de scheiding
						echo "	<tr><td>&nbsp;</td></tr>\n";
					}

					echo "		<td width=\"1\" valign=\"top\">";
					if ($leveranciernr != $lastLeveranciernr){
						echo getLeverancierNaam($leveranciernr);
						$lastLeveranciernr = $leveranciernr;
					} else {
						echo "&nbsp;";
					}
					echo "</td>\n";

					if (trim($isbn) != ""){
						echo "		<td valign=\"top\">'" . print_EAN($EAN) . "</td>\n";
					} else {
						echo "		<td valign=\"top\">n.v.t</td>\n";
					}
					echo "		<td valign=\"top\">&quot;$titel&quot; door &quot;$auteur&quot;</td>\n";
					echo "		<td valign=\"top\">" . (-$levprijs) . "</td>\n";
					echo "		<td valign=\"top\">$netto</td>\n";
					echo "		<td valign=\"top\">" . (-$totaal) . "</td>\n";

					if ($groupbyBetaalmethode){
						echo "		<td>$betaalmethode</td>\n";
					}
				}
			}
			echo "	</tr>\n";
		} // end foreach over voorraadnrs
	} // end foreach over leveranciernrs

	if (!$crediteurenlijst){
		echo "	<tr>\n";
		echo "		<td valign=\"bottom\" colspan=\"6\" align=\"right\">\n";
		echo "			Totaal:\n";
		echo "		</td>\n";
		echo "		<td style=\"border-top: double black\" valign=\"bottom\">";
		echo Money::addPrice($grandtotaal);
		echo "</td>\n";
		echo "	</tr>\n";
	}
	echo "</table>\n";
}
