<?php
// $Id$

/*

Functies die de interfaceomgeving voor bookweb creeeren. Vooralsnog komen hier
de functies voor het laten zien van de geselecteerde-klant balk.

*/

function getSelLid()
{
	global $session;
	global $auth;

	// je moet tenminste ingelogd zijn
	requireAuth('bijnalid');

	if (!hasAuth('verkoper')) return $auth->getLidnr();

	$sel = @$session->get('bookweb/selected_lid');
	if ($sel) return $sel;

	return $auth->getLidnr();
}

// om het geselecteerde lid te wijzigen
function changeselectedlid($data)
{
	global $session, $request;

	if (!isset($data)) {
		print_error('Geen data ingevoerd! Om een lid te selecteren scan je de collegekaart of voer je lidnr, studentnummer of een deel van de naam in.');
	}

	//Zorg dat we zoek nog een beetje kunnen gebruiken als getLidnrFromAnything
	//TODO: echte getLidnrFromAnything maken
	if(is_numeric($data) && strlen($data) <= 5)
	{
		$leden = PersoonVerzameling::zoek('id:' . $data);
	} else {
		$leden = PersoonVerzameling::zoek($data);
	}
	$leden->sorteer('naam');

	if($leden->aantal() > 1) {
		if ($leden->aantal() >= 50) {
			echo '<div class=waarschuwing>FUUUU! Zoekopdracht erg dubbelzinnig, '
				. 'dus dit zijn maar de eerste 50 resultaten.</div>';
		} else {
			echo '<div class=waarschuwing>Zoekopdracht dubbelzinnig, maak een '
				.'keuze:</div>';
		}

		$urlvars = $request->request->all();
		$newurl = 'changeselectedlid';
		$first = true;
		foreach ($urlvars as $key => $value) {
			if ($key == 'page' || $key == 'data') {
				continue;
			}
			$newurl .= ($first?'?':'&').urlencode($key).'='.urlencode($value);
			$first = false;
		}
		$newurl .= ($first?'?':'&').'data=id'.urlencode(':');

		echo "\n<ul>\n";
		foreach ($leden as $lidnr => $lid) {
			echo "\n<li>".makeBookRef("$newurl$lidnr"
							, PersoonView::naam($lid) . " ($lidnr)");
		}
		echo "\n</ul>\n";
		return;
	}

	if(!$leden->valid())
	{
		if(strlen($data) == 7) {
			// niets gevonden, maar het is mogelijk wel een studentnummer!
			$boekenkoper = "<a href=\"/Leden/Nieuw?formtype=quick"
						. "&studentnr=$data\">nieuwe boekenkoper aanmaken</a>";

			printHTML('Helaas, a.d.h.v. de zoekstring "'.$data.'" kon niemand '
			. 'geselecteerd worden. Het lijkt erop dat je een '
			. "studentnummer hebt ingevoerd, misschien wil je een $boekenkoper."
			, TRUE);

			return;
		}
		printHtml('Helaas, a.d.h.v. de zoekstring "'.$data.'" kon niemand ' .
			'geselecteerd worden.', TRUE);

		return;
	}
	
	$session->set('bookweb/selected_lid', $leden->current()->getContactID());

	// reset kart
	bw_initKart();
}

// Datum van actie, als 'YYYY-MM-DD HH:MM:SS' string
function getActionDateTime()
{
	global $session;
	$TODAY = strftime('%Y-%m-%d %H:%M:%S');
	if (!hasAuth('boekcom')) return $TODAY;

	$date = @$session->get('bookweb/action_date');
	if ($date) return $date;
	return $TODAY;
}

// Datum van actie, als 'YYYY-MM-DD' string
function getActionDate()
{
	return substr(getActionDateTime(),0,10);
}

// Of de datum van actie gewijzigd is...
function isActionDateTimeChanged()
{
	global $session;
	return $session->has('bookweb/action_date');
}

// wordt aangeroepen vannuit dispatch als je de datum/tijd wilt wijzigen
function changeActionDateTime()
{
	// terug naar nu:
	if (tryPar('nu')) {
		setActionDateTime();
		return;
	}

	setActionDateTime(requirePar('datetime'));
}

// Wijzig datum/tijd van actie, indien geen argument, of 'NULL' als argument:
// terug naar 'nu'
function setActionDateTime($newvalue = NULL)
{
	global $session;

	if (NULL === $newvalue) {
		$bookweb = $session->get('bookweb');
		unset($bookweb['action_date']);
		$session->set('bookweb', $bookweb);
		return;
	}

	if (!preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{2}):([0-9]{2})$/',
		$newvalue, $regs)) {

		print_error("Geef een tijdstip op als JJJJ-MM-DD UU:MM:SS");
	}
	list(, $year, $month, $day, $hour, $minute, $second) = $regs;

	$stamp = mktime($hour, $minute, $second, $month, $day, $year);
	$datetime = strftime('%Y-%m-%d %H:%M:%S', $stamp);

	if ($datetime != $newvalue) {
		print_error("Helaas, dat lijkt geen geldig tijdstip. Als ik $newvalue
			ontleed, krijg ik er $datetime uit. Controleer of het wel een
			bestaande datum is!");
	}
	if($stamp > time()) {
		print_error('Dude, die datum ligt in de toekomst. Dat kan niet goed zijn.');
	}

	$session->set('bookweb/action_date', $datetime);
}

// Laat als je voldoende rechten hebt een balk zien met daarin het
// geselecteerde lid (en mogelijkheid dat te veranderen), en de huidige datum
function showClientBar()
{
	if (!hasAuth('verkoper')) return;

	global $logger, $session, $BOEKENPLAATSEN;
	global $auth;

	$selected = getSelLid();

	$selectedLid = Persoon::geef($selected);

	// maak persoonswitching alleen mogelijk bij lege kart
	$haskart = (count(bw_getKart()) > 0 ) ;

	?>

<!-- BEGIN ClientBar -->

<table  width="100%" class="bw_clientbar">
<tbody>
<tr>
<td valign="top" rowspan="1" colspan="2" width="50%" id="selectedlid">
<strong>Geselecteerd lid:</strong><br>
<form action="" method="post" name="selectlid">
	<?php if ( $haskart ) { ?>
    Wijzig: <input type="text" size="7" disabled="disabled" />
    <input type="submit" value="OK" disabled="disabled" class="bw_button"/><br />
    <small>(er zit wat in je kart)</small>
	<?php } else { ?>
    <input type="hidden" name="page" value="changeselectedlid" />
    Wijzig: <input type="text" size="7" name="data" />
    <input type="submit" value="OK" class="bw_button"/><br />
    <small>(lidnr/studnr/naam)</small>
	<?php } ?>
</form>
<script language="JavaScript" type="text/javascript">
<!--
	document.selectlid.data.focus();
// -->
</script>
</td>
<td width="50%" id="actiondatetime" valign="top" align="center" <?=isActionDateTimeChanged()?'bgcolor="red"':''?>>
    <font size="+2"><strong><?=getActionDateTime()?></strong></font>
<? if (hasAuth('boekcom')) { ?>
    <form action="">
    <input type="hidden" name="page"     value="changeactiondatetime" />
    <input type="text"   name="datetime" value="<?=getActionDatetime()?>" />
    <input type="submit" value="Verander" class="bw_button"/>
    <? if (isActionDateTimeChanged()) { ?>
       <input type="submit" name="nu" value="Naar NU" class="bw_button" />
    <? } ?>
    </form>
<? } ?>
</td>

</tr>

<?php
// expand bar als er iemand anders geselecteerd is dan ikzelf,
// of als de verkoop geopend is
if(getSelLid() != $auth->getLidnr() || tryVerkoopOpen()):
?>
<tr>

<td valign="top" rowspan="1" align="center" colspan="1" id="lidfoto">
    <?=PersoonView::kop(Persoon::geef($selected))?>
</td>

<td valign="top" id="lidinfo">
    <a target="_blank" href="/Leden/<?=$selected?>">
	<?
		$naam    = PersoonView::naam(Persoon::geef($selected));
		if($selectedLid && $selectedLid instanceof Lid) 
		{
			$studies = sqlLidStudies($selected);

			if ($selectedLid->getLidToestand() == 'bijna lid' ) {
				$lidvan = '"bijna lid"';
			} 	
			$lidvanparts = explode('-', $selectedLid->getLidVan()->format('Y-m-d'));
			$lidvan = fromColJaar($lidvanparts[1], $lidvanparts[0]);
		
		}

		echo htmlspecialchars($naam) .'</a>';

		//Expres even laten staan zodat duidelijk waar nog naar moet worden gekeken TODO
		//if ($selectedLid->getLidToestand() != "boekenkoper"){
		if ($selectedLid){
			if($selectedLid instanceof Lid)
				echo '<br/> ('. implode(', ', $studies).' '.$lidvan.')';
			// deze opmerkingen zijn hier wel op hun plaats
			$pmdd = PersoonView::mededelingen($selectedLid);
			if ( count($pmdd) > 0) {
				echo "<table border=1 cellpadding=5 cellspacing=0>\n";
				foreach($pmdd as $md)
				{
					$mymdd="<p>".str_replace("\n\n","</p>\n<p>",
					$md['mededeling'])."</p>";
					echo "<tr><th>".$md['title']."</th></tr>\n";
					echo "<tr><td>".$mymdd."</td></tr>\n";
				}
				echo "</table>\n<p>\n";
				}
				if ($selectedLid instanceof Lid && $selectedLid->getLidTot()->hasTime() &&
					$selectedLid->getLidTot()->getTimestamp()<time()){
				echo "<br/><br/> Deze persoon is <strong>oud-lid</strong>";
			}
		} else {
			echo " <br/><br/>" .
				"Deze persoon is <strong>geen lid</strong> en heeft " .
				"in totaal al voor " . Money::addPrice(sqlGetVerkopenBedrag($selected)) .
				" aan producten gekocht. Gooi al je charmes in de " .
				"strijd en verkoop deze persoon een lidmaatschap!";
		}

		if ($selectedLid instanceof Lid && $selectedLid->isBijnaLid()) {
			echo "<br/><br/><span class='waarschuwing'> Deze persoon is <strong>bijna-lid</strong>."
				."<br/>Verkoop deze persoon daarom niks "
				."<br/>in de boekverkoop of maak "
				. $selectedLid->getVoornaamwoord()->vervoeg('jou')
				. " boekenlid "
				."<br/>of weet "
				. $selectedLid->getVoornaamwoord()->vervoeg('jou')
				. " een lidmaatschap aan te smeren!</span>";
		}

		// en dit zijn de opmerkingen die in de database staan
		if ( $selectedLid->getOpmerkingen() ){
			echo '<form method="POST" action="/Leden/'.$selected.'/Wijzig#Opmerkingen" target="_blank">';
?>
    		<strong><div class="waarschuwing">Opmerkingen:</div><blockquote>
           <?=htmlspecialchars($selectedLid->getOpmerkingen())?></blockquote></strong>

<input type="submit" name="action" value="Wijzig" />
<input type="submit" name="action" value="Wis" />
</form>
		<?php } ?>
</td>
<td valign="top" rowspan="2" colspan="1" id="transactie">
<?php
if(tryVerkoopOpen()):
?>
<strong>Transactie:</strong>
<form action="" method="POST">
<input type="hidden" name="page" value="checkout">
<?php addRedo('kart'); ?>
<table width="100%" id="producten">
<?php
	$totaal = 0;
	$totaalbtw = 0;
	$kart = bw_getKart();
	$logger->info('Boekverkoopkart: ' . print_r($kart, true));

	// display de verschillende items
	foreach($kart as $itemnr => $item) {
		if(!$session->has('wagentjeuseBTW') || $session->get('wagentjeuseBTW')) {
			// trim de omschrijving waar nodig
			$omschr = str_cut($item['omschrijving'], 60);
			echo "<tr class=\"productrij\">";
			echo "<td class=\"aantal\">".$item['aantal'].
				"</td><td title=\"".htmlspecialchars($item['omschrijving'])."\" class=\"product\">"
					.htmlspecialchars($omschr). '<br>' .
					'<font size="1">vanuit voorraadlocatie: ' . $BOEKENPLAATSEN[$item['plaats']] . '</font>' .
				"</td><td align=\"right\" class=\"bedrag\">". Money::addPrice($item['aantal'] * $item['prijs']).
				"</td><td align=\"right\">".$item['btwtarief']."%".
				"</td><td align=\"right\">".Money::addPrice($item['aantal'] * $item['btw']).
				"</td><td align=\"right\">[".makeBookRef('kartdrop', 'x','nr='.$itemnr)."]".
				"</td></tr>";
			$totaal += ($item['aantal']*$item['prijs']);
			$totaalbtw += ($item['aantal']*$item['btw']);
		} else {
			// trim de omschrijving waar nodig
			$omschr = str_cut($item['omschrijving'], 60);
			echo "<tr class=\"productrij\">";
			echo "<td class=\"aantal\">".$item['aantal'].
				"</td><td title=\"".htmlspecialchars($item['omschrijving'])."\" class=\"product\">"
					.htmlspecialchars($omschr). '<br>' .
					'<font size="1">vanuit voorraadlocatie: ' . $BOEKENPLAATSEN[$item['plaats']] . '</font>' .
				"</td><td align=\"right\" class=\"bedrag\">". Money::addPrice($item['aantal'] * ($item['prijs'] - $item['btw'])).
				"</td><td align=\"right\">".$item['btwtarief']."%".
				"</td><td align=\"right\"><p><strike>".Money::addPrice($item['aantal'] * $item['btw']). "</strike></p>" .
				"</td><td align=\"right\">[".makeBookRef('kartdrop', 'x','nr='.$itemnr)."]".
				"</td></tr>";
			$totaal += ($item['aantal']*($item['prijs']-$item['btw']));
			$totaalbtw += ($item['aantal']*$item['btw']);
		}
	}

	if (sizeof($kart) == 0){
		echo "<tr><td colspan=\"4\">(er zitten op dit moment geen producten
		in de kart)</td></tr>";
	}
	?>
			<tr>
				<td>&nbsp;</td>
				<td align="right">Totaal:</td>
				<td align="right" class="totaalbedrag"><strong><?=Money::addPrice($totaal)?></strong></td>
				<td align="right">BTW:</td>
				<?php
					if(!$session->has('wagentjeuseBTW') || $session->get('wagentjeuseBTW'))
						echo '<td align="right">' . Money::addPrice($totaalbtw) . '</td>';
					else
						echo '<td align="right"><p><strike>' . Money::addPrice($totaalbtw) . '</strike></p></td>';
				?>
			</tr>
			</table><br/>

			<div>
			<?php
				echo "<span>Deze transactie ";
				if(!$session->has('wagentjeuseBTW') || $session->get('wagentjeuseBTW'))
					echo "<a href='/Onderwijs/Boeken/ToggleBTW'>zonder</a>";
				else
					echo "<a href='/Onderwijs/Boeken/ToggleBTW'>met</a>";
				echo " BTW verrichten.</span>";
			?>
			</div><br/>

			<div style="font-size: 10pt">
		   <?php
			if(count($kart) > 0 && $totaal == 0) {
				echo "<input type=\"submit\" name=\"betaal[null]\" value=\" go \"> ";
			} else {
				global $BETAALMETHODEN;
				$paymethA = ($totaal >= 0 ? $BETAALMETHODEN['+'] : $BETAALMETHODEN['-']);
				$paymethB = ($totaal >= 0 ? $BETAALMETHODEN['-'] : $BETAALMETHODEN['+']);
				echo "Bedrag is betaald per:<br/>";
				foreach($paymethA as $methkey => $method) {
					echo "<input type=\"submit\" name=\"betaal[".$methkey."]\" value=\"".
						$method."\"".(count($kart) == 0 || $methkey == "chip"?' disabled="1"':'')." ".
						"style=\"margin: 0px; padding: 0px\"
						class=\"bw_button\" /> ";
				}
				echo "<br/>\n";
				echo "<label for=\"stuurAankoopBevestiging\">Verstuur aankoopbevestiging:". 
				"<input type=\"checkbox\" id=\"stuurAankoopBevestiging\" name=\"stuurbevestiging\" " .
				($totaal > 15 ? " checked=\"checked\"" : "") .
				"/>\n";
				echo "<br/><br />\n";
				echo "Bedrag direct terugkopen:<br/>";
				foreach($paymethB as $methkey => $method) {
					echo "<input type=\"submit\" name=\"betaal[".$methkey."]\" value=\"(".
						$method.")\"".(count($kart) == 0 || $methkey == "chip"?' disabled="1"':'')." " .
						"style=\"font-size: xx-small; margin: 0px; padding:
						0px\" class=\"bw_button\" /> ";
				}
			}
			?>
			</div>
			</form>
<?php
else :
	echo "<em>Verkoop is niet open</em>\n";
endif;
?>
       </td>
</tr>
<?php endif; ?>

</tbody>
</table>

<!-- EIND ClientBar -->

<?
// einde showClientBar functie
}

function losseItemsVerkoopForm($EANS)
{
	// DEBUGMSG uit, omdat het er niet leesbaarder op wordt, aan het eind van de
	// functie gaat ie weer op de oude waarde.
	$SQLMSG_TEMP = SQLMSG;
	$SQLMSG = false;

	$faseinfos = sqlFaseInfo('3', $EANS);

	echo "<table class='table table-striped table-condensed'>\n";

	foreach($faseinfos as $EAN => $faseinfo) {
		$voorraden = Voorraad::searchByEAN($EAN);
		echo '<tr>';

		if (!is_array($voorraden) || count($voorraden) === 0){
			// Niets in de losse verkoop
			echo "<td>Er is niets om te verkopen van EAN $EAN</td>";
		} elseif(count($voorraden) === 1) {
			// Exact 1 voorraad
			$voorraad = array_shift($voorraden);
			$voorraadnr = $voorraad->getNr();

			$inKart = bw_checkKart($voorraadnr);

			$selAantal = null;
			$faseinfo['aantal'] -= $inKart;

			if($faseinfo['aantal'] == 0)
				continue;

			for ($i = 1; $i <= $faseinfo['aantal']; $i++) {
				$selAantal[] = $i;
			}

			echo addForm('', 'post', NULL, 'verkooponbesteld');
			echo '<td align=left>' . addHidden('EAN', $EAN)
				. addHidden('page', 'verkooponbesteld')
				. addHidden('action', 'verkoop')
				. addHidden('fase3', $faseinfo['aantal'])
				. addHidden('voorraadnr_voorraadlocatie', $voorraad->getNr()
							. '-voorraad');

			addRedo($EAN);
			echo addSelect('aantal', $selAantal,'1')
				. "</td><td>" . addFormEnd('Naar Kart',false)
				. '</td><td>' . htmlspecialchars($faseinfo['titel'])
				. ' (&agrave; '
				. Money::addPrice($voorraad->berekenVerkoopPrijs())
				. ')</td>';
		} else {
			/* Als er meerdere voorraadnummers zijn, mag de verkoper de juiste zelf
			 * kiezen: extra tussenstap
			 */
			echo '<td>'
				. makeBookRef('verkooponbesteld', print_EAN($EAN), 'EAN='.$EAN)
				.  "</td><td></td><td>" . htmlspecialchars($faseinfo['titel']) . '</td>';
		}
		echo "</tr>\n";
	}
	echo "</table>\n";

	$SQLMSG = $SQLMSG_TEMP;
}

