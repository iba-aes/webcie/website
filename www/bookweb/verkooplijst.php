<?php

function verkooplijst() {
    require_once('bookweb/init.php');
    $page = Page::getInstance('cleanhtml')->start();

    $lijst = inputLijstExport(sqlVoorraadLijst(FALSE));
    $page->add($div = new HtmlDiv());
    $date = new DateTimeLocale('now');
    $div->add($div2 = new HtmlDiv());
    $div2->add(new HtmlParagraph("Datum: " . $date->format('Y-m-d')));
    $div2->add(new HtmlParagraph(_("Verkoper:")));
    $div2->setAttribute('style','font-size: 55%;');
    $div->add($table = new HtmlTable());
    $table->setAttribute('border', '1');
    $table->setAttribute('style','font-size: 55%;');
    $table->add($row = new HtmlTableRow());
    $row->add(new HtmlTableHeaderCell(_("ISBN")));
    $row->add(new HtmlTableHeaderCell(_("Auteur")));
    $row->add(new HtmlTableHeaderCell(_("Titel")));
    $row->add(new HtmlTableHeaderCell(_("Prijs")));
    $row->add(new HtmlTableHeaderCell(_("#vrrd")));
    $row->add(new HtmlTableHeaderCell(_("#btnvrkp")));
    $row->add(new HtmlTableHeaderCell(_("#verk")));
    $row->add(new HtmlTableHeaderCell(_("€tot")));
    foreach($lijst as $l) {
        $table->add($r = new HtmlTableRow());
        foreach($l as $td) {
            if(strpos($td, '\hfill') !== false) {
                foreach(explode(' \hfill ', $td) as $t)
                    $r->add(new HtmlTableDataCell($t));
            } elseif(strpos($td, '\euro \,') !== false) {
                $td = str_replace('\euro \,', '€', $td);
                $r->add(new HtmlTableDataCell($td));
            } else {
                $r->add(new HtmlTableDataCell($td));
            }
        }
    }

    $div->add($table = new HtmlTable());
    $table->setAttribute('border', '1');
    $table->setAttribute('style','font-size: 55%; width: 25%;');

    $table->add($row = new HtmlTableRow());
    $row->add($cell = new HtmlTableDataCell(_("Tellingen")));
    $cell->setAttribute('colspan', '2');
    $row->add($cell = new HtmlTableDataCell(_("Boekingen")));
    $cell->setAttribute('colspan', '2');

    $table->add($row = new HtmlTableRow());
    $row->add($cell = new HtmlTableDataCell(_("Pin:")));
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell());
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell(_("Totaal €tot:")));
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell());
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');

    $table->add($row = new HtmlTableRow());

    $table->add($row = new HtmlTableRow());
    $row->add($cell = new HtmlTableDataCell(_("Totaal tellingen:")));
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell());
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell(_("Totaal boekingen:")));
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');
    $row->add($cell = new HtmlTableDataCell());
    $cell->setAttribute('style', 'width: 25%');
    $cell->setAttribute('rowspan', '2');

    $page->end('cleanhtml');
}
