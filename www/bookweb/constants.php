<?php
// $Id$

/**
 * constanten.
 */

define('STARTPAGE', '/Onderwijs/Boeken/');

/* Het leveranciersnummer van A-Eskwadraat */
define('LEVNR_AES2', 2);
/* Het leveranciersnummer van de Boekencommissie */
define('LEVNR_BOEKCOM', 8);

/* dit bepaalt welke databaselevels gebruikt worden */
global $SQLDB;
$SQLDB = BOEKEN_DB;

/* Minimum aantal werkdagen waarbinnen een student de bestelling kan komen
 * afhalen.*/
define('SB_VERLOOPDAGEN', 10);

/* Aantal dagen voor een bestelmoment dat een student een herinnering van de
 * bestelling krijgt */
define('SB_HERINNERDAGEN', 3);

/* Verschillende prioriteiten die aan een studentbestelling gegeven
 * kunnen worden. */

// 'gewone' bestelling
define('SB_PRIO_REGULIER', 0);
// bij herbestellen na boek beloofd maar toch niet aanwezig
define('SB_PRIO_VOORRAADPROB', 5);
// bij herbestellen bij beschadigd boek en boekcom kiest voor hoge prio
define('SB_PRIO_OMRUIL', 8);
// bij het uitgeven van vouchers
// define('SB_PRIO_VOUCHER', 5)


global $BOEKENPLAATSEN;
// Definitie van voorraadlocaties. Let op! Moeten overeenkomen met kolomnamen in tabel 'voorraad'!
$BOEKENPLAATSEN = array(
	'voorraad' => 'reguliere voorraad',
	'buitenvk' => 'buiten verkoop',
	'ejbv_voorraad' => 'EJBV-voorraad'
);

global $BETAALMETHODEN;
/* De diverse betaalmethoden die Bookweb rijk is. */
$BETAALMETHODEN = array(
	'+' => array('pin1' => 'Pinautomaat 1', 'pin2' => 'Pinautomaat 2',
		'contant' => 'Contant', 'debiteur' => 'Betaal later'),
	'-' => array('contant' => 'Contant',
		'crediteur' => 'Wordt later terugbetaald'));

/* Uitsluitend TIJDENS de EJBV de knop weergeven om bij Informatica te kunnen
 * pinnen. */ /*
require_once('bookweb/interface.php');
if (strtotime(getActionDate()) == strtotime(EJBV)) {
	$BETAALMETHODEN['+']['pinII'] = 'Pinnen bij Informatica en Informatiekunde';
}
*/

global $STUDIENAMEN_en, $STUDIEJAREN;
// voor het bestellen van boeken
// TODO: gebruik normale gettext studienamen
$STUDIENAMEN_en = array
       ( 'ic' => 'Computer science'
       , 'ik' => 'Information science'
       , 'na' => 'Physics & Astronomy'
       , 'wi' => 'Mathematics'
       );

$STUDIEJAREN = array('bachelor' => _('Bachelorfase')
		    ,'master'   => _('Masterfase')
		    );

// ProcessTo4 enabled?
define('PT4_ENABLED', 1);

global $ISBN_COUNTRY_MAP;
// Om ISBNs te kunnen prettyprinten
$ISBN_COUNTRY_MAP = array('0','80','950','9960','99900');

global $ISBN_PUBL_MAP;
$ISBN_PUBL_MAP    = array(
    0 => array(array('00',200,7000,85000,900000,9500000),
                     "English group 0:AU:CA:GI:IE:NZ:PR:ZA:SZ:GB:US:ZW"),
    1 => array(array('00000000',55000,869800,9999900),
                     "English group 1:AU:CA:GI:IE:NZ:PR:ZA:SZ:GB:US:ZW"),
    2 => array(array('00',200,40000000,500,7000,84000,900000,9500000),
                     "French group:FR:BE:CA:LU:CH"),
    3 => array(array('00',200,7000,85000,900000,9500000),
                     "German group:AT:DE:CH"),
    4 => array(array('00',200,7000,85000,900000,9500000), "JP"),
    5 => array(array('00',200,7000,85000,900000,9500000),
         "Former USSR group:RU:AM:AZ:BY:EE:GE:KZ:KG:LV:LT:MD:TJ:TM:UA:UZ"),
    7 => array(array('00',100,5000,80000,900000), "CN"),
    80 => array(array('00',200,7000,85000,900000), "Czech/Slovak:CZ:SK"),
    81 => array(array('00',200,7000,85000,900000), "IN"),
    82 => array(array('00',200,7000,90000,990000), "NO"),
    83 => array(array('00',200,7000,85000,900000), "PL"),
    84 => array(array('00',200,7000,85000,900000,95000,9700), "ES"),
    85 => array(array('00',200,7000,85000,900000), "BR"),
    86 => array(array('00',300,7000,80000,900000), "Balkans:YU:BA:HR:MK:SI"),
    87 => array(array('00',400,7000,85000,970000), "DK"),
    88 => array(array('00',200,7000,85000,900000), "Italian group:IT:CH"),
    89 => array(array('00',300,7000,85000,950000), "Korean group:KP:KR"),
    90 => array(array('00',200,5000,70000,800000,9000000), "Dutch group 0:NL:BE"),
    91 => array(array('0',20,500,6500000,7000,8000000,85000,9500000,970000), "SE"),
    92 => array(array('0',60,800,9000), "INT"), // International organizations
    93 => array(array('0000000'), "IN"),
    94 => array(array('00',200,5000,70000,800000,9000000), "Dutch group 1:NL:BE"),
    950 => array(array('00',500,9000,99000), "AR"),
    951 => array(array('0',20,550,8900,95000), "FI"),
    952 => array(array('00',200,5000,89,9500,99000), "FI"),
    953 => array(array('0',10,150,6000,96000), "HR"),
    954 => array(array('00',400,8000,90000), "BG"),
    955 => array(array('0',20,550,800000,9000,95000), "LK"),
    956 => array(array('00',200,7000), "CL"),
    957 => array(array('00',440,8500,97000), "TW"),
    958 => array(array('0',600,9000,95000), "CO"),
    959 => array(array('00',200,7000), "CU"),
    960 => array(array('00',200,7000,85000), "GR"),
    961 => array(array('00',200,6000,90000), "SI"),
    962 => array(array('00',200,7000,85000), "HK"),
    963 => array(array('00',200,7000,85000), "HU"),
    964 => array(array('00',300,5500,90000), "IR"),
    965 => array(array('00',200,7000,90000), "IL"),
    966 => array(array('00',500,7000,90000), "UA"),
    967 => array(array('0',60,900,9900,99900), "MY"),
    968 => array(array('000000',10,400,500000,6000,800,900000), "MX"),
    969 => array(array('0',20,400,8000), "PK"),
    970 => array(array('00',600,9000,91000), "MX"),
    971 => array(array('00',500,8500,91000), "PH"),
    972 => array(array('0',20,550,8000,95000), "PT"),
    973 => array(array('0',20,550,9000,95000), "RO"),
    974 => array(array('00',200,7000,85000,900000), "TH"),
    975 => array(array('00',300,6000,92000,980000), "TR"),
    976 => array(array('0',40,600,8000,95000),
         "Caribbean Community:AG:BS:BB:BZ:KY:DM:GD:GY:JM:MS:KN:LC:VC:TT:VG"),
    977 => array(array('00',200,5000,70000), "EG"),
    978 => array(array('000',2000,30000), "NG"),
    979 => array(array('0',20,300000,400,700000,8000,95000), "ID"),
    980 => array(array('00',200,6000), "VE"),
    981 => array(array('00',200,3000), "SG"),
    982 => array(array('00',100,500000),
                 "South Pacific:CK:FJ:KI:MH:NR:NU:SB:TK:TO:TV:VU:WS"),
    983 => array(array('000',2000,300000,50,800,9000,99000), "MY"),
    984 => array(array('00',400,8000,90000), "BD"),
    985 => array(array('00',400,6000,90000), "BY"),
    986 => array(array('000000'), "TW"),
    987 => array(array('00',500,9000,99000), "AR"),
    9952 => array(array('00000'), "AZ"),
    9953 => array(array('0',20,9000), "LB"),
    9954 => array(array('00',8000), "MA"),
    9955 => array(array('00',400), "LT"),
    9956 => array(array('00000'), "CM"),
    9957 => array(array('00',8000), "JO"),
    9958 => array(array('0',10,500,7000,9000), "BA"),
    9959 => array(array('00'), "Libya"),
    9960 => array(array('00',600,9000), "SA"),
    9961 => array(array('0',50,800,9500), "DZ"),
    9962 => array(array('00000'), "PA"),
    9963 => array(array('0',30,550,7500), "CY"),
    9964 => array(array('0',70,950), "GH"),
    9965 => array(array('00',400,9000), "KZ"),
    9966 => array(array('00',70000,800,9600), "KE"),
    9967 => array(array('00000'), "KG"),
    9968 => array(array('0',10,700,9700), "CR"),
    9970 => array(array('00',400,9000), "UG"),
    9971 => array(array('0',60,900,9900), "SG"),
    9972 => array(array('0',40,600,9000), "PE"),
    9973 => array(array('0',10,700,9700), "TN"),
    9974 => array(array('0',30,550,7500), "UY"),
    9975 => array(array('0',50,900,9500), "MD"),
    9976 => array(array('0',60,900,99000,9990), "TZ"),
    9977 => array(array('00',900,9900), "CR"),
    9978 => array(array('00',950,9900), "EC"),
    9979 => array(array('0',50,800,9000), "IS"),
    9980 => array(array('0',40,900,9900), "PG"),
    9981 => array(array('0',20,800,9500), "MA"),
    9982 => array(array('00',40000,800,9900), "ZM"),
    9983 => array(array('00',500,80,950,9900), "GM"),
    9984 => array(array('00',500,9000), "LV"),
    9985 => array(array('0',50,800,9000), "EE"),
    9986 => array(array('00',400,9000), "LT"),
    9987 => array(array('00',400,8800), "TZ"),
    9988 => array(array('0',30,550,7500), "GH"),
    9989 => array(array('0',30,600,9700), "MK"),
    99901 => array(array('00'), "BH"),
    99903 => array(array('0',20,900), "MU"),
    99904 => array(array('0',60,900), "AN"),
    99905 => array(array('0',60,900), "BO"),
    99906 => array(array('0',60,900), "KW"),
    99908 => array(array('0',10,900), "MW"),
    99909 => array(array('0',40,950), "MT"),
    99910 => array(array('0000'), "SL"),
    99911 => array(array('00',600), "LS"),
    99912 => array(array('0',60,900), "BW"),
    99913 => array(array('0',30,600), "AD"),
    99914 => array(array('0',50,900), "SR"),
    99915 => array(array('0',50,800), "FK"),
    99916 => array(array('0',30,700), "NA"),
    99917 => array(array('0',30), "BN"),
    99918 => array(array('0',40,900), "FO"),
    99919 => array(array('0',40,900), "BJ"),
    99920 => array(array('0',50,900), "AD"),
    99921 => array(array('0',20,700), "QA"),
    99922 => array(array('0',50), "GT"),
    99923 => array(array('0',20,800), "SV"),
    99924 => array(array('0',30), "NI"),
    99925 => array(array('0',40,800), "PY"),
    99926 => array(array('0000',600), "HN"),
    99927 => array(array('0',30,600), "AL"),
    99928 => array(array('0',50,800), "GE"),
    99929 => array(array('0000'), "MN"),
    99930 => array(array('0',50,800), "AM"),
    99931 => array(array('0000'), "SC"),
    99932 => array(array('0',10), "MT"),
    99933 => array(array('00',300), "NP"),
    99934 => array(array('0'), "DO"),
    99935 => array(array('0000'), "HT"),
    99936 => array(array('0000'), "BT"),
    99937 => array(array('0',20), "MO")
  );

// Rubrieken zoals beschikbaar in de grootboeken
// (en dus gebruikt in de tabel "leverancier")
global $RUBRIEKEN;
$RUBRIEKEN = array(
	"Boekenfonds", "A-Eskwadraat", "Inventaris",
	"Extern", "Boekverkoop", "Dictaatverkoop"
);

