<?php
// $Id$

/*

Alleen nog maar BC functies

*/

function isValidEAN($EAN)
{
	return is_EAN($EAN);
}

function requireEAN($parname = 'EAN')
{
	return bookweb_get_EAN(requirePar($parname));
}

function tryEAN()
{
	$var = tryPar('EAN');
	if (!$var) return FALSE;

	return bookweb_get_EAN($var);
}

