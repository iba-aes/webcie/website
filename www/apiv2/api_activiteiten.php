<?php
require_once('space/init.php');
require_once('apiconst.php');

function activiteitToJsonSummary($act)
{
	$info = $act->getInformatie();
	$res = array(
		'id' => $act->getActiviteitID(),
		'titel' => $act->getTitel(),
		'beschrijving' => $info->getWerftekst(),
		'momentBegin' => $act->getMomentBegin()->format(DATE_ATOM),
		'momentEind' => $act->getMomentEind()->format(DATE_ATOM),
		'posterPad' => $act->posterPath() ? HTTPS_ROOT . $act->url() . '/ActPoster' : null
	);
	$kaartjes = iDealKaartjeVerzameling::geefScanbareKaartjes($act);
	if($kaartjes->aantal() != 0)
		$res['scanbaar'] = true;
	else
		$res['scanbaar'] = false;
	//het is handig om constante ids door te geven,
	//zodat wijzigingen slechts server-sided aanpassingen
	//behoeven
	switch($info->getActsoort()) 
	{
		case 'UU':		$res['soort'] = ACTIVITEIT_SOORT_UU; break;
		case 'ZUS':		$res['soort'] = ACTIVITEIT_SOORT_ZUS; break;
		case 'LAND':	$res['soort'] = ACTIVITEIT_SOORT_LAND; break;
		case '.COM':	$res['soort'] = ACTIVITEIT_SOORT_COM; break;
		case 'EXT':		$res['soort'] = ACTIVITEIT_SOORT_EXT; break;
		case 'AES2':
		default:		$res['soort'] = ACTIVITEIT_SOORT_AES; break;
	}
	return $res;
}

function activiteitToJsonDetail($act)
{
	$res = activiteitToJsonSummary($act);
	$info = $act->getInformatie();
	$res['locatie'] = $info->getLocatie();
	$res['prijs'] = $info->getPrijs();
	$res['homepage'] = $info->getHomepage();
	$res['inschrijfbaar'] = $info->getInschrijfbaar();
	if($res['scanbaar'])
	{
		$res['scanUrl'] = HTTPS_ROOT . $act->url() . '/ScanControle?code=';
		$res['magScannen'] = isset($_SESSION['magScannen'.$act->geefID()]) && $_SESSION['magScannen'.$act->geefID()];
	}
	//filterbekijken voor de zekerheid, je weet maar nooit
	//of er bijzondere commissies aan een activiteit verbonden
	//zijn...
	$commissies = $info->getCommissies()->filterBekijken();
	$commissiesArray = array();
	foreach($commissies as $com)
	{
		$commissiesArray[] = array(
			'id' => $com->getCommissieID(),
			'naam' => $com->getNaam());
	}
	$res['commissies'] = $commissiesArray;
	return $res;
}

if(!array_key_exists(1, $rest))
{
	if($_SERVER['REQUEST_METHOD'] === 'GET')
	{
		//activiteitenoverzicht
		$van = new DateTimeLocale();
		$van->setTime(0,0);
		if(isset($_GET['van']))
		{
			$van_tmp = new DateTimeLocale($_GET['van']);
			if($van_tmp != false)
				$van = $van_tmp;
		}
		$tot = clone $van;
		$tot->modify("+1 year");
		if(isset($_GET['tot']))
		{
			$tot_tmp = new DateTimeLocale($_GET['tot']);
			if($tot_tmp != false && $van < $tot_tmp)
				$tot = $tot_tmp;
		}
		$activiteiten = ActiviteitVerzameling::/*getActiviteiten*/tijdsSpan($van, $tot, 'PUBLIEK')->filterBekijken()->sorteer('BeginMoment');
		$activiteitenArray = array();
		foreach($activiteiten as $a)
			$activiteitenArray[] = activiteitToJsonSummary($a);
		$msgbody = array('activiteiten' => $activiteitenArray);
	}
	else
	{
		//error op dit moment
	}
}
else if(array_key_exists(2, $rest))
{
	if($_SERVER['REQUEST_METHOD'] === 'GET')
	{
		if($rest[2] == 'fotos')
		{
			$actid = intval($rest[1]);
			if($actid != 0)
			{
				$activiteiten = ActiviteitVerzameling::verzamel(array($actid))->filter('getToegang', 'PUBLIEK')->filterBekijken();
				if($activiteiten->bevat($actid))
				{
					$medias = MediaVerzameling::fromActiviteit($activiteiten->first())->filterBekijken();
					$fotoArray = array();
					foreach($medias as $m)
					{
						$foto = array(
							'id' => $m->getMediaID()
						);
						if($m->getSoort() == 'FOTO')
							$foto['mediumUrl'] = HTTPS_ROOT . $m->url() . '/Medium';
						else//films hebben alleen een thumbnail
							$foto['mediumUrl'] = HTTPS_ROOT . $m->url() . '/Thumbnail';
						$fotoArray[] = $foto;
					}
					$msgbody = array('fotos' => $fotoArray);
				}
			}
			else
			{
				//error
			}
		}
		else
		{
			//error
		}
	}
	else
	{
		//error op dit moment
	}
}
else
{
	if($_SERVER['REQUEST_METHOD'] === 'GET')
	{
		if($rest[1] == 'metFoto')
		{
			if(isset($_GET['jaar']) && intval($_GET['jaar']) > 0)
				$acts = ActiviteitVerzameling::geefVanJaarMetFoto(intval($_GET['jaar']))->filter('getToegang', 'PUBLIEK')->filterBekijken()->sorteer('BeginMoment',true);
			else
			{
				$count = 8;
				if(isset($_GET['laatste']) && intval($_GET['laatste']) > 0)
					$count = intval($_GET['laatste']);
				if($count > 50)
					$count = 50;
				$acts = ActiviteitVerzameling::geefLaatsteMetFoto($count)->filter('getToegang', 'PUBLIEK')->filterBekijken()->sorteer('BeginMoment',true);
			}
			$activiteitenArray = array();
			foreach($acts as $a)
			{
				$summ = activiteitToJsonSummary($a);
				$cover = MediaVerzameling::fromActiviteit($a)->filterBekijken()->first();
				if($cover->getSoort() == 'FOTO')
					$summ['coverUrl'] = HTTPS_ROOT . $cover->url() . '/Medium';
				else//films hebben alleen een thumbnail
					$summ['coverUrl'] = HTTPS_ROOT . $cover->url() . '/Thumbnail';
				$activiteitenArray[] = $summ;
			}
			$msgbody = array('activiteiten' => $activiteitenArray);
		}
		else
		{
			//activiteit details
			$actid = intval($rest[1]);
			if($actid != 0)
			{
				$activiteiten = ActiviteitVerzameling::verzamel(array($actid))->filter('getToegang', 'PUBLIEK')->filterBekijken();
				if($activiteiten->bevat($actid))
					$msgbody = array('activiteit' => activiteitToJsonDetail($activiteiten->first()));
			}
			else
			{
				//error
			}
		}
	}
	else
	{
		//error op dit moment
	}
}
?>
