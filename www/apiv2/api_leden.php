<?php

require_once('space/init.php');
require_once('WhosWho4/constants.php');
require_once('apiconst.php');

function lidToJsonSummary($lid)
{
	$foto = ProfielFoto::zoekHuidig($lid);
	$fotourl = null;
	if($foto != null && $foto->getMedia()->magBekijken())
		$fotourl = HTTPS_ROOT . $foto->getMedia()->url() . '/Thumbnail';
	$res = array(
		'id' => $lid->getContactID(),
		'naam' => $lid->getNaam(WSW_NAME_DEFAULT),
		'foto' => $fotourl
	);
	return $res;
}

function lidToJsonDetail($lid)
{
	$fotoid = -1;
	$foto = ProfielFoto::zoekHuidig($lid);
	if($foto != null && $foto->getMedia()->magBekijken())
		$fotoid = $foto->getMedia()->getMediaID();
	$res = array(
		'id' => $lid->getContactID(),
		'voornaam' => $lid->getVoornaam(),
		'tussenvoegsels' => $lid->getTussenvoegsels(),
		'achternaam' => $lid->getAchternaam(),
		'geboortenamen' => $lid->getGeboortenamen(),
		'voorletters' => $lid->getVoorletters(),
		'bijnaam' => $lid->getBijnaam(),
		'titelsPrefix' => $lid->getTitelsPrefix(),
		'titelsPostfix' => $lid->getTitelsPostfix(),
		'email' => $lid->getEmail(),
		'geboorteDatum' => $lid->getDatumGeboorte()->format(DATE_ATOM),
		'homepage' => $lid->getHomepage(),
		'fotoMediaId' => $fotoid,
		'lidVan' => $lid->getLidVan()->format(DATE_ATOM)
	);
	if($lid->getLidToestand() == 'OUDLID')
		$res['lidTot'] = $lid->getLidTot()->format(DATE_ATOM);
	switch($lid->getLidToestand())
	{
		case 'BIJNALID': $res['toestand'] = LID_TOESTAND_BIJNALID; break;
		case 'LID': $res['toestand'] = LID_TOESTAND_LID; break;
		case 'OUDLID': $res['toestand'] = LID_TOESTAND_OUDLID; break;
		case 'BAL': $res['toestand'] = LID_TOESTAND_BAL; break;
		case 'LIDVANVERDIENSTE': $res['toestand'] = LID_TOESTAND_LIDVANVERDIENSTE; break;
		case 'ERELID': $res['toestand'] = LID_TOESTAND_ERELID; break;
		case 'GESCHORST': $res['toestand'] = LID_TOESTAND_GESCHORST; break;
		case 'BEESTJE': $res['toestand'] = LID_TOESTAND_BEESTJE; break;
	}
	$telnrs = $lid->getTelefoonNummers()->filterBekijken();
	$telarray = array();
	foreach ($telnrs as $telnr) 
	{
		$telnrtmp = array(
			'id' => $telnr->getTelnrID(),
			'nummer' => $telnr->getTelefoonnummer()
		);
		switch($telnr->getSoort())
		{
			case 'THUIS': $telnrtmp['soort'] = TELNR_SOORT_THUIS; break;
			case 'MOBIEL': $telnrtmp['soort'] = TELNR_SOORT_MOBIEL; break;
			case 'WERK': $telnrtmp['soort'] = TELNR_SOORT_WERK; break;
			case 'OUDERS': $telnrtmp['soort'] = TELNR_SOORT_OUDERS; break;
			case 'RECEPTIE': $telnrtmp['soort'] = TELNR_SOORT_RECEPTIE; break;
			case 'FAX': $telnrtmp['soort'] = TELNR_SOORT_FAX; break;
		}
		$telarray[] = $telnrtmp;
	}
	$res['telefoonNummers'] = $telarray;
	$adressen = $lid->getAdressen()->filterBekijken();
	$addrarray = array();
	foreach ($adressen as $adres) 
	{
		$addrtmp = array(
			'id' => $adres->getAdresID(),
			'straat1' => $adres->getStraat1(),
			'straat2' => $adres->getStraat2(),
			'huisnummer' => $adres->getHuisnummer(),
			'postcode' => $adres->getPostcode(),
			'woonplaats' => $adres->getWoonplaats(),
			'land' => $adres->getLand()
		);
		switch($adres->getSoort())
		{
			case 'THUIS': $addrtmp['soort'] = ADRES_SOORT_THUIS; break;
			case 'OUDERS': $addrtmp['soort'] = ADRES_SOORT_OUDERS; break;
			case 'WERK': $addrtmp['soort'] = ADRES_SOORT_WERK; break;
			case 'POST': $addrtmp['soort'] = ADRES_SOORT_POST; break;
			case 'BEZOEK': $addrtmp['soort'] = ADRES_SOORT_BEZOEK; break;
			case 'FACTUUR': $addrtmp['soort'] = ADRES_SOORT_FACTUUR; break;
		}
		$addrarray[] = $addrtmp;
	}
	$res['adressen'] = $addrarray;
	//filterbekijken voor de zekerheid, je weet maar nooit
	//of er bijzondere commissies aan een lid verbonden
	//zijn...
	$commissies = $lid->getCommissies()->filterBekijken();
	$commissiesArray = array();
	foreach($commissies as $com)
	{
		$commissietmp = array(
			'id' => $com->getCommissieID(),
			'naam' => $com->getNaam()
		);
		switch($com->getSoort())
		{
			case 'CIE': $commissietmp['soort'] = COMMISSIE_SOORT_CIE; break;
			case 'GROEP': $commissietmp['soort'] = COMMISSIE_SOORT_GROEP; break;
			case 'DISPUUT': $commissietmp['soort'] = COMMISSIE_SOORT_DISPUUT; break;
		}
		$commissiesArray[] = $commissietmp;
	}
	$res['commissies'] = $commissiesArray;
	$lidstudies = $lid->getStudies()->filterBekijken();
	$lidstudiesArray = array();
	foreach($lidstudies as $lidstudie)
	{
		$studie = $lidstudie->getStudie();
		$groep = $lidstudie->getGroep();
		$lidstudietmp = array(
			'id' => $lidstudie->getLidStudieID(),
			'datumBegin' => $lidstudie->getDatumBegin()->format(DATE_ATOM),
			'datumEind' => ($lidstudie->getDatumEind() == null ? null : $lidstudie->getDatumEind()->format(DATE_ATOM)),
			'studie' => array(
				'id' => $studie->getStudieID(),
				'naam' => $studie->getNaam(),
				'fase' => ($studie->getFase() == 'BA' ? LID_STUDIE_FASE_BACHELOR : LID_STUDIE_FASE_MASTER)
			)
		);
		if($groep)
		{
			$lidstudietmp['groep'] = array(
				'id' => $groep->getGroepID(),
				'naam' => $groep->getNaam()
			);
		}
		switch($lidstudie->getStatus())
		{
			case 'STUDEREND': $lidstudietmp['status'] = LID_STUDIE_STATUS_STUDEREND; break;
			case 'ALUMNUS': $lidstudietmp['status'] = LID_STUDIE_STATUS_ALUMNUS; break;
			case 'GESTOPT': $lidstudietmp['status'] = LID_STUDIE_STATUS_GESTOPT; break;
		}
		$lidstudiesArray[] = $lidstudietmp;
	}
	$res['studies'] = $lidstudiesArray;
	//extra informatie als je het zelf bent
	if(Lid::getIngelogdLid() === $lid)
	{
		$res['vakidioot'] = $lid->getVakidOpsturen();
		$res['aes2roots'] = $lid->getAes2rootsOpsturen();
	}
	return $res;
}

function iouBetalingToJson($betaling)
{
	return array(
		'id' => $betaling->getBetalingID(),
		'datum' => $betaling->getGewijzigdWanneer()->format(DATE_ATOM),
		'omschrijving' => $betaling->getOmschrijving(),
		'bonOmschrijving' => $betaling->getBon()->getOmschrijving(),
		'bedrag' => $betaling->getBedrag(),
		'bonId' => $betaling->getBonBonID(),
		'naar' => $betaling->getNaar()->getNaam(WSW_NAME_DEFAULT),
		'naarId' => $betaling->getNaarContactID(),
		'van' => $betaling->getVan()->getNaam(WSW_NAME_DEFAULT),
		'vanId' => $betaling->getVanContactID()
	);
}

function getAchievements($persoon)
{
	global $BADGES;
	
	$persbadges = $persoon->getBadges();
	$resultaatarray = array();
	foreach($BADGES['onetime'] as $badge => $badgeArray)
	{
		// verberg schaduwachievements als je die nog niet hebt
		if (!Persoon::getIngelogd() || (!Persoon::getIngelogd()->hasBadge($badge) && $badgeArray['schaduw'])) {
			//if ($pers->hasBadge($badge)) {
				//$schaduwcount++;
			//}
			continue;
		}

		//if($i % 6 == 0) {
		//	$row = $ottable->addRow();
		//}

		//$row->add(PersoonBadgeView::toTableCell($badge, $badgeArray, $pers));
		//$i++;
		$resultaatarray[] = array(
			'id' => $badge,
			'titel' => $badgeArray['titel'],
			'omschrijving' => $badgeArray['omschrijving'],
			'heeftAchievement' => $persoon->hasBadge($badge)
		);
	}
	return array('oneTimeAchievements' => $resultaatarray);
}

if(Lid::getIngelogdLid())
{
	if(!array_key_exists(1, $rest))
	{
		if($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			//leden zoeken
			if(!isset($_GET['q']) || strlen($_GET['q']) < 3)
			{
				//error: geen query of te kort
			}
			else
			{
				$kart = Kart::geef()->getPersonen();
				$resultaten = LidVerzameling::zoek($_GET['q'])->leden()->filter('getLidToestand', 'LID')->filterBekijken();
				$resultaatarray = array();
				foreach($resultaten as $res)
				{
					$tmplidsum = lidToJsonSummary($res);
					$tmplidsum['inKart'] = $kart->bevat($res);
					$resultaatarray[] = $tmplidsum;
				}
				$msgbody = array('resultaten' => $resultaatarray);
			}
		}
	}
	else
	{
		if($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			if($rest[1] == 'random')
			{
				$msgbody = array('lid' => lidToJsonDetail(Lid::geefWillekeurig()));
			}
			else if($rest[1] == 'iou')
			{
				if(!array_key_exists(2, $rest))
				{
					$balans = IOUBetalingVerzameling::getBalans(Persoon::getIngelogd());
					$resultaatarray = array();
					foreach($balans as $pid => $bedrag)
					{
						if(round($bedrag, 2) == 0)
							continue;
						$resultaatarray[] = array(
							'persoonId' => $pid,
							'persoonNaam' => PersoonView::naam(Persoon::geef($pid)),
							'bedrag' => $bedrag
						);
					}
					$msgbody = array('balans' => $resultaatarray);
				}
				else
				{
					//openstaande bonnen met een persoon
					$lidid = intval($rest[2]);
					if($lidid > 0)
					{
						$betalingen = IOUBetalingVerzameling::alleVanTweePersonen(Persoon::getIngelogd(), Persoon::geef($lidid));
						$resultaatarray = array();
						foreach($betalingen as $bet)
						{
							$resultaatarray[] = iouBetalingToJson($bet);
						}
						$msgbody = array('betalingen' => $resultaatarray);
					}
					else
					{
						//error
					}
				}
			}
			else
			{
				if(array_key_exists(2, $rest))
				{
					if($rest[2] == 'achievements')
					{
						$lidid = intval($rest[1]);
						if($lidid > 0)
						{
							$persoon = Persoon::geef($lidid);
							if($persoon->magBekijken())
							{
								$msgbody = getAchievements($persoon);
							}
							else
							{
								//error	
							}
						}
						else
						{
							//error
						}
					}
					else
					{
						//error
					}
				}
				else
				{
					//lid details
					$lidid = intval($rest[1]);
					if($lidid > 0)
					{
						$leden = LidVerzameling::verzamel(array($lidid))->filter('getLidToestand', 'LID')->filterBekijken();
						if($leden->bevat($lidid))
							$msgbody = array('lid' => lidToJsonDetail($leden->first()));	
					}
					else
					{
						//error
					}
				}
			}
		}
		else
		{
			//error op dit moment
		}
	}
}
else
{
	//error
	//niet ingelogde mensen kunnen geen leden zien!	
}
?>