$(document).ready(function () {
    // Wat documentatie https://developer.chrome.com/articles/nfc/
    let button = $("#nfcStartButton");
    let field = $("#rfidTagField").find("input");
    let error = $("#nfcError");
    let errorP = error.find("p");


    if ("NDEFReader" in window) {
        error.addClass("alert-danger");
        error.hide();

        button.on("click", () => {
            const ndef = new NDEFReader();

            ndef.scan().then(() => {
                button.text("SCANNING...");
                ndef.onreading = event => {
                    let x = event.serialNumber.split(":")
                    x.reverse()
                    if (x.length === 7) {
                        x.splice(0, 4)
                        x.push("88")
                    }
                    field.val(parseInt(x.join(""), 16).toString().padStart(10, "0"));
                    error.hide();
                }

                ndef.onreadingerror = () => {
                    error.show();
                    errorP.text("ERROR: Kon deze tag niet goed lezen. Probeer het opnieuw of probeer een andere tag.")
                }

            }).catch(() => {
                error.show();
                errorP.text("ERROR: Kon niet starten met scannen.");
            })
        })
    } else {
        errorP.text("Jouw browser support geen NFC scanning. Probeer het opnieuw in een andere browser of vul handmatig een serienummer in.")
        button.prop('disabled', true);
    }
})
