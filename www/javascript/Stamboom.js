var toimpl = false;

$(document).ready(function() {
	makeExpandable($(document));

	$('.stamboom-expand-all').click(function() {
		$('.stamboom-expand').each(function(i, e) {
			toggleParents($(e), toimpl);
		});
		toimpl = !toimpl;
		if(toimpl) {
			$(this).text('Klap alles in');
		} else {
			$(this).text('Klap alles uit');
		}
	});
});

function makeExpandable(parent) {
	parent.find('.stamboom-expand').click(function() {
		var sib = $(this).parent().siblings().eq(0);
		if(sib.is(':visible')) {
			toggleParents($(this), true);
		} else {
			toggleParents($(this), false);
		}
		return false;
	});

	parent.find('.stamboom-fetch').click(function() {
		var lidnr = $(this).attr("id");
		
		fetchStamboom($(this), lidnr, $(this).parent().parent());
		$(this).text('...');
		return false;
	});

	parent.find('.stamboom-fetchRev').click(function() {
		var lidnr = $(this).attr("id");
		
		fetchStamboomRev($(this), lidnr, $(this).parent().parent());
		$(this).text('...');
		return false;
	});
};

function fetchStamboomRev(anch, lidnr, target) {
	$.ajax({ type: "POST", url: '/Ajax/Lid/StamboomRev', data: {lidnr: lidnr} }).done(
		function (html)
		{
			console.log(html);
			target.html(html);
			makeExpandable(target);
		})
}

function fetchStamboom(anch, lidnr, target) {
	$.ajax({ type: "POST", url: '/Ajax/Lid/Stamboom', data: {lidnr: lidnr} }).done(
		function (html)
		{
			console.log(html);
			target.html(html);
			makeExpandable(target);
		})
}

function toggleParents(anch, imp)
{
	var sib = anch.parent().siblings().eq(0);
	if(imp) {
		sib.hide();
		anch.text('Klap uit');
	} else {
		sib.show();
		anch.text('Klap in');
	}
}
