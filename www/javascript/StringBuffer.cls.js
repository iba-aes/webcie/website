function StringBuffer() {
	this.buffer = [];
}

function StringBuffer(string) {
	this.buffer = [ string ];
}

StringBuffer.prototype.append = function append(string)
{
	this.buffer.push(string);
	return this;
};

StringBuffer.prototype.toString = function toString()
{
	return this.buffer.join("");
};
