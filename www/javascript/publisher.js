$(function() {
	$('.renameButton').click(function() {
		$('.rename'+$(this).attr('id')).css('display', 'inline');
		$('.renameTitel'+$(this).attr('id')).hide();
	});

	$('.deleteButton').click(function() {
		var ret = confirm("Je staat op het punt om het bestand te verwijderen, weet je zeker dat je door wil gaan?");
		if(ret) {
			window.location = $(this).attr('href');
		}
		return false;
	});

	$('#previewButton').click(function() {
		var content = CKEDITOR.instances['editarea'].getData();
		console.log(content);
		BootstrapDialog.show({
			size: BootstrapDialog.SIZE_WIDE,
			title: 'Preview',
			message: $('<div></div>').append(content)
		});
	});
});
