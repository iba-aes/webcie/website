$(function() {
    var eventHandler = function(event) {
        let el = $(this);
        if (el.val() == 'true') {
            $('#emailFromSenderDiv').hide();
        }
        else {
            $('#emailFromSenderDiv').show();
            // Met naam
        }
    }
    $('#form_anoniem_false').on('change', eventHandler);
    $('#form_anoniem_true').on('change', eventHandler);
});