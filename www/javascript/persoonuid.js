$(function() {
	$('.uid').each(function() {
		var $lidid = $(this).attr('id').replace('plane-', '');
		$.ajax({
			url: '/Ajax/Persoon/UserCheck?lidid='+$lidid,
			async: true,
			success: function(data)
			{
				if(data.status == 'OK')
				{
					$('#plane-'+data.lidid).parent().append(data.uid);
					$('#plane-'+data.lidid).parent().append(' <a href="/Leden/' + data.lidid + '/ResetWachtwoord">Reset wachtwoord</a>');
					$('#plane-'+data.lidid).remove();
				}
				else
				{
					$('#plane-'+data.lidid).parent().append('<b>Geen</b>');
					$('#plane-'+data.lidid).remove();
				}
			}
		});
	});
});
