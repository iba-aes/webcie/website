(function($){
	var searchRequest = null;
	var changeTimer = false;

	$.persoonZoeken = function() {
		if(changeTimer !== false)
			clearTimeout(changeTimer);

		var $container = $('#Leden_zoekContainer');
		$container.html('Laden... / Loading...');

		var str = $('input[name="LidVerzamelingZoeken"]').val();
		// closes: #7830
		str = str.replace(/^\s+|\s+$/g,"");

		if (str.length < 3)
		{
			$container.html('Vul een zoekterm in / Enter a search term');
			return false;
		}

		changeTimer = setTimeout(function()
		{

			if(searchRequest != null)
				searchRequest.abort();

			searchRequest = $.ajax({ type: "POST"
				, url: $('input[name="LidVerzamelingZoeken"]').attr('data-searchurl')
				, data: {str: str}
			}).done(function (html)
			{
				var $container = $('#Leden_zoekContainer');
				$container.html(html);
				sorttable.init(true);
				// Deze function is gedefiniëerd in util.js, maar omdat het zoeken
				// pas gebeurd nadat de hele pagina is geladen, bestaat deze functie
				fotoPreview();

				// Deze functie komt uit profielJS.js en zorgt voor het vergroten
				// van de profielfoto als je erop klikt
				initProfielFoto();

				$container.find('img.lazy').lazyload().removeClass('lazy');
			});
		}, 300);

		return false;
	};
})(jQuery);

$(function() {
	var t = location.hash.substr(1);
	if(t.length) {
		$('input[name="LidVerzamelingZoeken"]').val(t);
		$.persoonZoeken();
	}
	$('input[name="LidVerzamelingZoeken"]').focus();
	/*$(window).unload(function(){
		location.hash = $('input[name="LidVerzamelingZoeken"]').val();
	});*/

	$('input[name="LidVerzamelingZoeken"]').keyup(function() {
		$.persoonZoeken();
		return false;
	});

	$('form[name="PersoonVerzameling"]').submit(function() {
		$.persoonZoeken();
		return false;
	});
});
