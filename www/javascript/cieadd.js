function cieaddSudo(cieid, login)
{
	$.ajax({
		url: '/Ajax/Cie/CieaddSudo?id=' + cieid + '&login=' + login,
		success: function(data) {
			if(data.status == 'OK') {
				$('.paragraph').append('Sudo-regels aangemaakt!<br/>Overgaan op homedir...<br/><br/>');
				cieaddHomedir(cieid, login);
			} else {
				$('.paragraph').append([
					'Sudo-regels aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
				$('.rotating-plane').remove();
			}
		}
	});
}

function cieaddHomedir(cieid, login)
{
	$.ajax({
		url: '/Ajax/Cie/CieaddHomedir?id=' + cieid + '&login=' + login,
		success: function(data) {
			$('.rotating-plane').remove();
			if(data.status == 'OK') {
				$('.paragraph').append('Homedir aangemaakt!<br/>De commissie heeft nu een systeemaccount!<br/>Gefeliciteerd!<br/>');
				$('.paragraph').append('<a class="btn btn-primary" href="' + $('.cieurl').html() + '">Terug naar de commissie</a>');
				cieaddSendmail(cieid, login);
			} else {
				$('.paragraph').append([
					'Homedir aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
			}
		}
	});
}

function cieaddSendmail(cieid, login)
{
	$.ajax({
		url: '/Ajax/Cie/CieaddSendmail?id=' + cieid + '&login=' + login
	});
}

$(function() {
	var cieid = $('.cie-id').html();
	var login = $('.login-name').html();

	$.ajax({
		url: '/Ajax/Cie/CieaddUser?id=' + cieid + '&login=' + login,
		success: function(data) {
			if(data.status == 'OK') {
				$('.paragraph').append('Account aangemaakt!<br/>Overgaan op sudo-regels...<br/><br/>');
				cieaddSudo(cieid, login);
			} else {
				$('.paragraph').append([
					'Account aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
				$('.rotating-plane').remove();
			}
		}
	});
});
