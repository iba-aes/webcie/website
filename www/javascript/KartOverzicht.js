function Kart_Overzicht_Inverteer_Personen ()
{
	if ($("#InverteerPersonen")[0].checked)
	{
		Kart_Overzicht_Selecteer_Personen();
	}
	else
	{
		Kart_Overzicht_Deselecteer_Personen();
	}
}

function Kart_Overzicht_Selecteer_Personen ()
{
	var boxes = $("#kart_personenlijst :checkbox").not("#InverteerPersonen");
	for (var i = 0; i < boxes.length; i++)
		boxes[i].checked = true;
}

function Kart_Overzicht_Deselecteer_Personen ()
{
	var boxes = $("#kart_personenlijst :checkbox").not("#InverteerPersonen");
	for (var i = 0; i < boxes.length; i++)
		boxes[i].checked = false;
}

function Kart_Overzicht_Personen_Mailen ()
{
	document.forms['KartLijst'].elements['Kart[Actie]'].value = 'personen_mailen';
	document.forms['KartLijst'].submit();
}

function Kart_Overzicht_Personen_Uitschrijven ()
{
	document.forms['KartLijst'].elements['Kart[Actie]'].value = 'personen_uitschrijven';
	document.forms['KartLijst'].submit();
}

function Kart_Overzicht_Inverteer_Cies ()
{
	if ($("#InverteerCies")[0].checked)
	{
		Kart_Overzicht_Selecteer_Cies();
	}
	else
	{
		Kart_Overzicht_Deselecteer_Cies();
	}
}

function Kart_Overzicht_Selecteer_Cies ()
{
	var boxes = $("#kart_cieslijst :checkbox").not("#InverteerCies");
	for (var i = 0; i < boxes.length; i++)
		boxes[i].checked = true;
}

function Kart_Overzicht_Deselecteer_Cies ()
{
	var boxes = $("#kart_cieslijst :checkbox").not("#InverteerCies");
	for (var i = 0; i < boxes.length; i++)
		boxes[i].checked = false;
}

function Kart_Overzicht_Cies_Mailen ()
{
	document.forms['KartLijst'].elements['Kart[Actie]'].value = 'cies_mailen';
	document.forms['KartLijst'].submit();
}

function Kart_Overzicht_Cies_Uitschrijven ()
{
	document.forms['KartLijst'].elements['Kart[Actie]'].value = 'cies_uitschrijven';
	document.forms['KartLijst'].submit();
}
