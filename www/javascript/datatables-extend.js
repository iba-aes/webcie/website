$.extend( true, $.fn.dataTable.defaults, {
	language: {
		lengthMenu: "Laat _MENU_ items per pagina zien",
		zeroRecords: "Niks gevonden",
		info: "Pagina _PAGE_ van de _PAGES_",
		infoEmpty: "Niks beschikbaar",
		infoFiltered: "(gefilterd van _MAX_ totaal)",
		emptyTable: "Geen data beschikbaar",
		infoPostFix: "",
		thousands: ".",
		loadingRecords: "Laden...",
		processing: "Verwerken...",
		search: "Zoeken:",
		zeroRecords: "Geen matches gevonden",
		paginate: {
			first: "Eerste",
			last: "Laatste",
			next: "Volgende",
			previous: "Vorige"
		},
		aria: {
			sortAscending: ": activeer om de kolom aflopend te sorteren",
			sortDescending: ": activeer om de kolom oplopend te sorteren"
		}
	}
});
