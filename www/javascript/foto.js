var photoArray = [];
var photoArrayIds = [];
var $grid;
var $gallery;
var items;

/*
 * Opent de PhotoSwipe-gallery die bij fotoid id hoort
 */
function openGallery(id)
{
	var pswpElement = document.querySelectorAll('.pswp')[0],
		options = {};

	options.index = $.inArray(id, photoArrayIds);

	options.preload = [2, 2];

	$gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

	/*$gallery.options.shareButtons = [
		{id:'download', label:'Download', url:'/FotoWeb/Media/' + id + '/Origineel', download:true}
	];*/

	$gallery.init();

	$gallery.options.closeOnScroll = false;
	$gallery.options.isClickableElement = function(el) {
		return true;
	}
}

/*
 * Parset alle fotos zodat ze in goed json-formaat voor de gallery staan
 */
function parsePhotos()
{
	var is = [];
	for(var i = 0; i < photoArray.length; i++) {
		var id = photoArray[i].mediaID;
		// create slide object
		if(photoArray[i].soort == "FILM") {
			item = {
				html: '<video controls="" class="foto-groot ">'
					+'<source src="/FotoWeb/Media/'+id+'/Groot.mp4" type="video/mp4">'
					+'<source src="/FotoWeb/Media/'+id+'/Groot.ogv" type="application/ogg">'
					+'Je browser is te oud en ondersteunt helaas geen HTML5-video. Om de video wel te '
					+'kunnen bekijken kan je bijvoorbeeld de nieuwste versie van '
					+'<a href="http://www.mozilla.org/nl/firefox/new/">firefox</a> downloaden.</video>',
				aesId: id
			};
		} else {
			item = {
				src: '/FotoWeb/Media/' + id + '/Groot',
				w: photoArray[i].breedte,
				h: photoArray[i].hoogte,
				aesId: id
			};
		}

		is.push(item);
	}

	photoArrayIds = [];

	for(var i = 0; i < photoArray.length; i++) {
		photoArrayIds.push(parseInt(photoArray[i].mediaID));
	}

	return is;
}

/*
 * Probeert een id van de url te halen zodat er meteen een gallery geopend kan worden
 */
$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results==null){
		return null;
	} else{
		return results[1] || 0;
	}
}

function checkOffset() {
	var height = $('#footer').height();
	var scrollBottom = $(window).scrollTop() + $(window).height() - height;
	$('#footer').offset({top: Math.min(scrollBottom, $('.navbar-footer').offset().top - height), left: 0});
	$('#footer').height(height);
}

function getWidthPercentage(selector) {
	return Math.round( 10000 * parseFloat($(selector).first().css('width'))
			/ parseFloat($(selector).first().parent().css('width'))) / 100;
}

$(function() {
	var gridWidth = getWidthPercentage('.grid-item');

	$('.grid-item').each(function() {
		var ratio = $(this).attr('data-height') / $(this).attr('data-width');
		$(this).css({ 'padding-bottom': (ratio * gridWidth)+'%' });
	});

	photoArray = eval($('#fotoarray').attr('value'));
	items = parsePhotos();

	$grid = $('#fotoContainer').isotope({
		itemSelector: ".grid-item",
		columnWidth: ".grid-sizer",
		percentPosition: true,
		getSortData: {
			gemaakt: "[id]"
		},
		sortBy: "gemaakt",
		transformsEnabled: false,
		sortAscending: false,
		transitionDuration: 0,
		layoutInstant: true
	});

	$grid.isotope({ "sortBy": "gemaakt"});
	$grid.isotope('layout');
	$("img.lazy").lazyload();

	$('a.overlay').click(function(e) {
		e.preventDefault();
		var $id = $(this).attr('data-id');
		openGallery(parseInt($id, 10));
		return false;
	});

	checkOffset();
	$(window).scroll(function() {
		checkOffset();
	});
	$(window).resize(function() {
		var newWidth = getWidthPercentage('.grid-item');

		if(newWidth != gridWidth)
		{
			gridWidth = newWidth;

			$('.grid-item').each(function() {
				var ratio = $(this).attr('data-height') / $(this).attr('data-width');
				$(this).css({ 'padding-bottom': ((ratio * gridWidth).toString())+'%' });
			});

			$grid.isotope('layout');
		}

		checkOffset();
	});

	// zoekt een fotoid uit de url om meteen een gallery te openen
	var photoId = $.urlParam('photoId');

	if(photoId !== null) {
		var newHash = window.location.href.split('#')[0] + '#';
		window.location.replace(newHash);
		openGallery(parseInt(photoId, 10));
	}
});
