$(function() {
	$(".resload").each(function() {
		$(this).on("load", function() {
			var $id = $(this).attr("id").replace("large-", "");
			$("#small-" + $id).css({
				"background-image" : "url("+$(this).attr("src")+")"
			});
			$(this).remove();
		});
	});
});
