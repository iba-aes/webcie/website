$(function() {
	var persoonid = $('.persoon-id').html();
	var login = $('.login-name').html();

	if (!confirm('Weet je zeker dat je het wachtwoord wilt resetten?')) {
		return;
	}

	$('.rotating-plane').show();
	$.ajax({
		url: '/Ajax/Persoon/MensjeaddPassword?id=' + persoonid + '&login=' + login,
		success: function(data) {
			var text = '';
			if(data.status == 'OK') {
				text += 'Random wachtwoord gegenereerd!<br/>';
				text += 'Wachtwoord: <b>' + data.wachtwoord + '</b><br/>';
				text += 'Uitvoer van commando: <br/><pre>' + data.output.join("<br/>") + '</pre><br/>';
				text += '<a class="btn btn-primary" href="' + $('.persoonurl').html() + '">Terug naar het lid</a>';
			} else {
				text += 'Random wachtwoord aanmaken niet gelukt!<br/>';
				text += 'Exit code was: ' + data.exitcode + '<br/>';
				text += 'Foutmelding: <br/><pre>' + data.fout.join("<br/>") + '</pre>';

				$('.paragraph').css('color', 'red');
			}
			$('.paragraph').append(text);
			$('.rotating-plane').remove();
		}
	});
});
