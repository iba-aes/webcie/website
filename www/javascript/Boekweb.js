// Omdat we een wel of niet een levering kunnen uitbreiden,
// moeten we rekening houden met twee veldnamen.
var margeVelden = ['marge', 'DictaatOrder[Marge]'];
var waardeVelden = ['waarde', 'DictaatOrder[WaardePerStuk]'];

// Wat functies om met geld te werken omdat Javascript alleen floats heeft.
function rondGeldAfNaarBeneden(geld) {
	// Want we kunnen niet afronden op decimalen, alleen op ints...
	return Math.floor(geld * 100) / 100;
}
function rondGeldAf(geld) {
	// Want we kunnen niet afronden op decimalen, alleen op ints...
	return Math.ceil(geld * 100) / 100;
}


function verdeelTotaal() {
	// Interpreteer het als centen
	var waarde = $(":input[name='totaalWaarde']").val() * 100;
	var aantal = +$(":input[name='aantal']").val();
	if (!aantal) {
		var aantal = +$(":input[name='Levering[Aantal]']").val();
	}

	var overschot = waarde % aantal;
	var gemiddeldePrijs = rondGeldAf((waarde - overschot) / aantal / 100);
	waardeVelden.forEach(function (veld) {
		if (!$(":input[name='" + veld + "']").prop('disabled')) {
			$(":input[name='" + veld + "']").val(gemiddeldePrijs);
		}
	});
	$(":input[name='factuurVerschil']").val(rondGeldAf(overschot / 100));

	bepaalMargeUitWaarde();
}

function bepaalMargeUitWaarde() {
	// zet een + ervoor zodat het daadwerkelijk ints worden
	var waarde = +$(":input[name='waarde']").val();
	if (!waarde) {
		var waarde = +$(":input[name='DictaatOrder[WaardePerStuk]']").val();
	}
	if ($(":checkbox[name='icaKaft']").prop('checked')) {
		waarde += +$(":input[name='icaKaftPrijs']").val();
	}

	var absoluteMarge = +$(":input[name='absoluteMargeFactor']").val();
	var afschrijvingMarge = +$(":input[name='afschrijvingMargeFactor']").val();
	var marge = afschrijvingMarge * waarde + absoluteMarge;
	margeVelden.forEach(function (veld) {
		if (!$(":input[name='" + veld + "']").prop('disabled')) {
			$(":input[name='" + veld + "']").val(rondGeldAf(marge));
		}
	});
}

function activeerLeverancier() {
	// Bepaal of deze leverancier doet aan marges, en zo nee, zet de margeberekening uit.
	var leverancierID = $(":input[name='leverancier']").val();
	// leverancierArray wordt door PHP gegenereerd
	if (leverancierID && leverancierArray[leverancierID]) {
		// Zet marge op 0 want dat boeit toch niet.
		margeVelden.forEach(function (veld) {
			$(":input[name='" + veld + "']").prop('disabled', true);
			$(":input[name='" + veld + "']").val(0);
		});
		$(":input[name='icaKaft']").prop('checked', false);
	} else {
		// Leverancier rekent marges, dus laat die invulbaar.
		margeVelden.forEach(function (veld) {
			$(":input[name='" + veld + "']").prop('disabled', false);
		});
		// en vergeet niet de marge weer goed neer te zetten!
		bepaalMargeUitWaarde();
	}
}

$(document).ready( function () {
	$(":input[name='leverancier']").change(activeerLeverancier);
	$(":input[name='totaalWaarde']").change(verdeelTotaal);
	$(":input[name='totaalWaarde']").keyup(verdeelTotaal);
	$(":input[name='aantal']").change(verdeelTotaal);
	$(":input[name='aantal']").keyup(verdeelTotaal);
	waardeVelden.forEach(function (veld) {
		$(":input[name='" + veld + "']").change(bepaalMargeUitWaarde);
		$(":input[name='" + veld + "']").keyup(bepaalMargeUitWaarde);
	});
	$(":input[name='icaKaft']").change(bepaalMargeUitWaarde);
});
