$(function() {
  var machtigingInputs =
    $('input[name="LidmaatschapsBetaling[Rekeningnummer]"], ' +
      'input[name="LidmaatschapsBetaling[Rekeninghouder]"]');

  var machtigingLabels = 
    $('label[for="rekeningnummer"], ' +
      'label[for="rekeninghouder"]');

  var toggleInputs = function(value) {
    switch (value) {
      case 'BOEKVERKOOP':
      case 'IDEAL':
        machtigingInputs.attr('disabled', '');
        machtigingLabels.find('span').remove();
        break;
      case 'MACHTIGING':
        machtigingInputs.attr('disabled', null);
        machtigingLabels.append('<span class="waarschuwing">*</span>');
        break;
    }
  }

  var selectBetaalsoort = $('select[name="LidmaatschapsBetaling[Soort]"]');

  toggleInputs(selectBetaalsoort.val());

  selectBetaalsoort.on("load change click", function() {
    toggleInputs(this.value);
    return false;
  });
});
