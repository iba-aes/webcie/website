
// select-picker werkt niet helemaal mee met knockout.
// https://stackoverflow.com/questions/21998531/knockout-js-with-bootstrap-selectpicker:

// KO 3.0 feature to ensure binding execution order
ko.bindingHandlers.selectPicker = {
	after: [ 'options' ],
	init: function(element, valueAccessor, allBindingsAccessor) {
		var $element = $(element);

		console.log("Init for " + $element);
		// $element = $element.parent();
		$element.addClass('selectpicker');
		$element.selectpicker();

		// Dit is aangepast, omdat de selectpicker al aangemaakt is, en de parent bevat de plugin-functies.
		/* if ($element.selectpicker || $element.parent().selectpicker) {
			$element = $element.parent();
		} else {
			$element.addClass('selectpicker').selectpicker();
		} */

		var doRefresh = function() {
			$element.selectpicker('refresh');
		}, subscriptions = [];

		// KO 3 requires subscriptions instead of relying on this binding's update
		// function firing when any other binding on the element is updated.

		// Add them to a subscription array so we can remove them when KO
		// tears down the element. Otherwise you will have a resource leak.
		var addSubscription = function(bindingKey) {
			var targetObs = allBindingsAccessor.get(bindingKey);
			if (targetObs && ko.isObservable(targetObs))
				subscriptions.push(targetObs.subscribe(doRefresh));
		};

		addSubscription('options');
		addSubscription('value'); // Single
		addSubscription('selectedOptions'); // Multiple

		ko.utils.domNodeDisposal.addDisposeCallback(element, function() { 
			while (subscriptions.length) {
				subscriptions.pop().dispose();
			}
		});
	},
	update: function(element, valueAccessor, allBindingsAccessor) {
	}
};

$(function() {
	var Telnr = function(id, telnr, soort) {
		var self = this;
		self.id = id;
		self.telnr = ko.observable(telnr);
		self.soort = ko.observable(soort);
	};

	var mapping = {
		create: function(options) {
			return new Telnr(
				options.data.id,
				options.data.telnr,
				options.data.soort
			);
		}
	};

	var ViewModel = function() {
		var self = this;
		self.nieuwCounter = ko.observable(0);

		self.defaultSoort = 0;
		self.telnrsSoorten = [];
		self.voorkeur = ko.observable(window.telnrVoorkeurJSON);
		for (var id in window.telnrsSoortenJSON) {
			if (!window.telnrsSoortenJSON.hasOwnProperty(id)) continue;
			self.telnrsSoorten.push({
				'waarde': id,
				'label': window.telnrsSoortenJSON[id]
			});
			if (id == window.telnrSoortDefaultJSON) {
				self.defaultSoort = id;
			}
		}

		// lees de huidige telefoonnummers in
		self.telnrs = ko.mapping.fromJS(window.telnrsJSON, mapping);

		self.nieuw = function() {
			var newid = self.nieuwCounter() + 1;
			self.nieuwCounter(newid);
			self.telnrs.push(new Telnr('nieuw' + newid, '', self.defaultSoort, false));
		};

		self.verwijder = function(telnr) {
			// destroy kan ook, maar '_destroy'ed objecten worden niet naar de
			// server gestuurd
			self.telnrs.remove(telnr);
		};
	}
	ko.applyBindings(new ViewModel());
});
