// Variabelen voor de teststatus.
var nr_gevonden = 0;
var nr_uitgevoerd = 0;
var nr_geslaagd = 0;

// Stop de gegeven info in het info-veld van de node.
function setInfo(node, info)
{
	// Gebruik JQuery om een descendant te krijgen.
	$(node).find('.test-item-info').text(info);
	// Toon de info-knop alleen als er info is.
	if (info) {
		$(node).find('.test-item-infoToggle').show();
	} else {
		$(node).find('.test-item-infoToggle').hide();
	}
}

// Voer de gegeven test uit als op de juiste knop wordt gedrukt.
// (Dit moet in een aparte functie ivm closures.)
function maakRunbaar(node, naam) {
	node.querySelector('.test-item-run').addEventListener('click', (event) => runTest(node, naam));
}

// Voer een test uit die in de gegeven tr staat, met gegeven naam.
function runTest(node, naam) {
	updateStatus(naam, 'wachtend');
	// Vraag over AJAX om de test te runnen.
	// Succes betekent dat de AJAX-call succes had,
	// niet dat de test succes had.
	$.ajax('/Service/Intern/Test/RunTest', {
		data: {'test': naam},
		error: (jqXHR, textStatus, errorThrown) => {
			updateStatus(naam, 'gefaald');
			setInfo(node, "HTTP error " + errorThrown);
		},
		success: (data, textStatus, jqXHR) => {
			updateStatus(naam, data.status);
			var info = "";
			if (data.info) {
				info = data.info;
			}
			setInfo(node, info);
		},
	});
}

// Update de status van alle tests.
function updateStatus(naam, waarde) {
	// Update variabelen indien relevant
	switch (waarde) {
		case 'geslaagd':
			nr_geslaagd++;
			// Fall-through!
		case 'gefaald':
			nr_uitgevoerd++;
			break;
		case 'gevonden':
			nr_gevonden++;
			break;
	}
	document.getElementById('test-nr-gevonden').innerHTML = nr_gevonden;
	document.getElementById('test-nr-uitgevoerd').innerHTML = nr_uitgevoerd;
	document.getElementById('test-nr-geslaagd').innerHTML = nr_geslaagd;

	tests[naam].status = waarde;
	tests[naam].node.className = 'test-item test-item-' + waarde;
	tests[naam].node.querySelector('.test-item-status').innerHTML = waarde;
}

// Maak een overzicht van beschikbare tests,
// en maak de run-knop klikbaar.
var tests = {};
var testNamen = [];
var testItemNodes = document.getElementsByClassName("test-item");
for (var i = 0; i < testItemNodes.length; i++) {
	var node = testItemNodes.item(i);
	var naam = node.id;
	tests[naam] = {
		'naam': naam,
		'node': node,
	};
	testNamen.push(naam);

	updateStatus(naam, 'gevonden');
	maakRunbaar(node, naam);
}

// Activeer de toggleknop voor de faalinfo van een test.
// Dit doen we met jQuery want die heeft een methode slideToggle.
$('.test-item-infoToggle').click(
	// LET OP: `() => { ... }` werkt niet omdat `this` in JavaScript
	// vage semantiek heeft, die niet goedgaat met `() => { ... }`-functies
	// mmar wel met `function () { ... }`-functies. WAT.
	function () {
		$(this).parent().children('.test-item-infoContainer').slideToggle();
	}
);
// Activeer de run-alles-knop
document.getElementById('test-runAlles').addEventListener('click', (event) => {
	nr_uitgevoerd = 0;
	nr_geslaagd = 0;
	for (var naam of testNamen) {
		runTest(tests[naam].node, naam);
	};
});
