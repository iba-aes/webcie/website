var cameraIds = [];
var sourceId;
var cameraId;
var localMediaStream;

function gotSources(sourceInfos) {
	for (var i = 0; i !== sourceInfos.length; ++i) {
		var sourceInfo = sourceInfos[i];
		if (sourceInfo.kind === 'video') {
			cameraIds.push(sourceInfo.id);
		}
	}
}

function loadCamera(camera) {
	if(typeof camera != 'undefined' && typeof cameraIds[camera] != 'undefined') {
		sourceId = camera;
	} else {
		sourceId = 0;
	}

	if(typeof cameraIds[sourceId] != 'undefined') {
		cameraId = cameraIds[sourceId];
	}

	var canvas = document.getElementById('qr-canvas');
	var context = canvas.getContext('2d');
	var video = document.getElementById('video');

	window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

	var successCallback = function(stream) {
		video.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
		localMediaStream = stream;

		video.play();
		$("#camera-select").val(sourceId);
		$('#video').css({'right': parseInt(window.innerWidth/2),
			'bottom': parseInt(window.innerHeight/2)
		});
		console.log(parseInt($('.reader').width()/2));
		console.log(parseInt($('.reader').height()/2));
	};

	if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		var config = {video: true};
		if( typeof cameraId != 'undefined') {
			config = {
				video: {
					optional: [{
						sourceId: cameraId
					}]
				}
			};
		}
		navigator.getUserMedia(config, successCallback, function(error) {
			alert(error);
			//if (typeof videoError == 'function') videoError(error, localMediaStream);
			//else console.log('Error callback is undefined or not a function.');
		});
	} else {
		alert('Native web camera streaming (getUserMedia) not supported in this browser.');
	}
}

function removeCamera() {
	if(localMediaStream) {
		localMediaStream.getVideoTracks().forEach(function(videoTrack) {
			videoTrack.stop();
		});
	}
}

function filterImage(filter, pixels, var_args) {
	var args = [pixels];
	for(var i = 2; i < arguments.length; ++i) {
		args.push(arguments[i]);
	}

	return filter.apply(null, args);
}

function grayscale(pixels, args) {
	var d = pixels.data;
	for (var i=0; i<d.length; i+=4) {
		var r = d[i];
		var g = d[i+1];
		var b = d[i+2];
		// CIE luminance for the RGB
		// The human eye is bad at seeing red and blue, so we de-emphasize them.
		var v = 0.2126*r + 0.7152*g + 0.0722*b;
		d[i] = d[i+1] = d[i+2] = v
	}
	return pixels;
};

function brightness(pixels, adjustment) {
	var d = pixels.data;
	for (var i=0; i<d.length; i+=4) {
		d[i] += adjustment;
		d[i+1] += adjustment;
		d[i+2] += adjustment;
	}
	return pixels;
};

function colorCanvas(json, _callback) {
	var canvas = document.getElementById('qr-canvas');
	var context = canvas.getContext('2d');
	context.fillStyle = json.color;
	context.fillRect(0, 0, canvas.width, canvas.height);
	$('#qr-canvas').show();

	setTimeout(_callback, 100);
}

function scan() {
	$('#qr-canvas').width($('#video').width());
	$('#qr-canvas').height($('#video').height());
	if(localMediaStream) {
		var canvas = document.getElementById('qr-canvas');
		var context = canvas.getContext('2d');
		var width = $('#video').width();
		var height = $('#video').height();
		context.drawImage(video, 0, 0, 640, 480);
		var pixels = context.getImageData(0, 0, 640, 480);
		pixels = filterImage(grayscale, pixels);
		pixels = filterImage(brightness, pixels, -50);
		context.putImageData(pixels, 0, 0);

		try {
			qrcode.decode();
		} catch (e) {
		}
	}

	qrcode.callback = function(result) {
		// TODO: verklaar hoe url hier een waarde kan hebben,
		// of fix de code!
		$.post(url, { code: result })
		.done(function( data ) {
			var json = JSON.parse(data);

			colorCanvas(json, function() {
				if(json.hasOwnProperty('magScannen')) {
					$('#header').html("Je mag nu scannen!");
				}
				alert(json.error);
				$('#qr-canvas').hide();
			});
		});
	}
}

$(document).ready(function(){

	var i = 0;

	navigator.mediaDevices.enumerateDevices()
	.then(function(devices) {
		devices.forEach(function(device) {
			if(device.kind === "videoinput") {
				$("#camera-select").append($('<option>', {
						value: i,
						text: device.label
				}));
				++i;
				cameraIds.push(device.deviceId);
			}
		});
	})
	.catch(function(err) {
		alert(err.name + ": " + err.message);
	});

	loadCamera(0);
	removeCamera();

	setInterval(scan, 500);
	$(".reader").click(function() {
		scan();
	});
});

