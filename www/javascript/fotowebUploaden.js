$(function() {
	$(".upload-form").append("<div class='dropzone' id='myDropzone'></div>");
	$('.fallback').remove();
	var $dropZone = $("#myDropzone").dropzone({
		maxFilesize: 10,
		parallelUploads: 5,
		dictFileTooBig: "Te groot bestand van {{filesize}} MB, mag max {{maxFilesize}} zijn",
		dictInvalidFileType: "Verkeerd bestandstype",
		dictDefaultMessage: "Sleep bestanden hier of klik hier om te uploaden",
		addRemoveLinks: true,
		/** Bug #7580
		 * Zie https://stackoverflow.com/questions/27654005/how-to-upload-photo-from-gallery-in-android-to-a-website-with-dropzonejs#29311409
		 * Alleen image/* forceert gebruik van de gallerij op Android, maar dan
		 * kunnen er geen videos meer worden geupload.
		 */
		acceptedFiles: "image/*,video/*",
		url: "/FotoWeb/Uploaden",
		autoProcessQueue: false,
		init: function() {
			this.on("sending", function(file, xhr, formData){
				formData.append("fotograaf", $("select[name='fotograaf']").val());
				formData.append("collectie", $("select[name='collectie']").val());
				formData.append("act", $("input[name='act']").val());
				formData.append("cie", $("input[name='cie']").val());
			});
		}
	});
	$dropZone[0].dropzone.on('queuecomplete', function() {
		$dropZone[0].dropzone.options.autoProcessQueue = false;
		$("input[name='uploaden']").removeAttr('disabled');
	});

	$("input[name='uploaden']").click(function() {
		$dropZone[0].dropzone.options.autoProcessQueue = true;
		$dropZone[0].dropzone.processQueue();
		$("input[name='uploaden']").attr('disabled', '');
	});
});
