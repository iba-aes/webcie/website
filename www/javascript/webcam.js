$(document).ready(function() {
	setInterval(function() {
		$('.webcam').each(function(key, val) {
			var src = $(val).attr('src').split('?')[0];
			$(val).attr('src', src + '?' + new Date().getTime());
		});
	}, 1200);

	setInterval(function() {
		$.ajax('/Ajax/Achievements/Webcam');
	}, 1800000);
});
