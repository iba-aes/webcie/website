$(function() {
	$("#myCarousel").carousel({
		interval: 5000
	});

	var counter = 0;
	var progressBar;

	function progressBarRun() {
		progressBar = setInterval(function() {
			counter += 25;
			if (counter > 100) {
				clearInterval(progressBar);
				counter = 0;
			}
			$(".progressbar").css("width", counter + "%");
		}, 1000);
	}
	progressBarRun();

	$("#myCarousel").on("slid.bs.carousel", function () {
		counter = 0;
		clearInterval(progressBar);
		progressBarRun();
	});
});
