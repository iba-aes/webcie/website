//tover het form voor de latexgenerator naar voren als dit nodig is
function customSelect() { 
	soortjavascriptvar = $('#soortselectbox').val();
	
	$.get('AJAXlatex', {form:'true', soort:soortjavascriptvar}, function(data,status){
		soortjavascriptvar = data;
		$('#latexstring').html(soortjavascriptvar);
		});
};

//Zoek het bedrag en anderen op, serialize het form, submit deze info en vul de latex in.
function generateLatex(){
	soortjavascriptvar = $('#soortselectbox').val();
	var bedrag = $('#bedragid').val();
	switch (soortjavascriptvar) {
		case 'LEZING':
		var lezingdatum = $('#lezingdatumid').val();
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar +'&generate=true' +'&uitvoerdatum=' + lezingdatum, function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;

		case 'OVERIG':
		var sectietitel = $('#sectietitelid').val();
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar +'&generate=true' +'&sectietitel=' + sectietitel, function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;

		case 'MAILING':
		var mailingdatum = $('#mailingdatumid').val();
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar +'&generate=true' +'&uitvoerdatum=' + mailingdatum, function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;

		case 'VACATURE':
		var vacatureperiode = $('#periodeid').val();
		var vacatureaantal = $('#aantalid').val();
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar +'&generate=true' +'&periode=' + vacatureperiode +'&generate=true' +'&aantal=' + vacatureaantal, function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;

		case 'VAKID':
		var VakIdcoljaar = $('#coljaarid').val();
		var VakIdeditiemaand = $('#editiemaandid').val();
		var VakIdeditie = $('#editieid').val();
		var VakIdAdvertentieplaatsing = $('#plaatsingid').val();
		var VakIdAdvertentietype = $('#typeid').val();
		var VakIdAdvertentieformaat = $('#formaatid').val();
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar +'&generate=true'  +'&coljaar=' + VakIdcoljaar +'&generate=true'  +'&editiemaand=' + VakIdeditiemaand +'&generate=true'  +'&editie=' + VakIdeditie +'&generate=true'  +'&plaatsing=' + VakIdAdvertentieplaatsing +'&generate=true'  +'&type=' + VakIdAdvertentietype +'&generate=true'  +'&formaat=' + VakIdAdvertentieformaat, function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;

		default:
		$.post('AJAXlatex', $('#latexgenerateform').serialize() + '&bedrag=' + bedrag +'&generate=true' + '&soort=' + soortjavascriptvar,
		function(data){
		$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\">'+data+'</textarea>'); 
		});
		break;
		}
};

//Laat mensen zelf de latex invullen
function zelfinvoerenfunc(){
	$('#latexstring').html('<textarea name=\"Contractonderdeel[LatexString]\" cols=\"45\" rows=\"7\"></textarea>'); 
};
