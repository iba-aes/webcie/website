#!/usr/bin/php
<?php

// Prop hier alles in wat we gaan outputten
$buffer = "";
  
/**
 * Dit zijn alle fonts die we gebruiken:
 * Roboto, SourceCodePro, SourceSansPro
 *
 * Er zijn de volgende gewichten:
 */

$WEIGHT_UNITS = array(
	'Thin' => 100,
	'ExtraLight' => 200,
	'Light' => 300,
	'Regular' => 400,
	'Medium' => 500,
	'SemiBold' => 600,
	'Bold' => 700,
	'ExtraBold' => 800,
	'Black' => 900
);

$fonts = array(
	'Roboto' => array(
		'has_italics' => true,
		'regular_in_normal' => false,
		'weights' => array('Thin', 'Light', 'Regular', 'Medium', 'Bold', 'Black')
	)
	, 'Source Code Pro' => array(
		'has_italics' => false,
		'regular_in_normal' => false,
		'weights' => array('ExtraLight', 'Light', 'Regular', 'Medium', 'SemiBold', 'Bold', 'Black')
	)
	, 'Source Sans Pro' => array(
		'has_italics' => true,
		'regular_in_normal' => true,
		'weights' => array('ExtraLight', 'Light', 'Regular', 'SemiBold', 'Bold', 'Black')
	)
);

foreach ($fonts as $fontName => $options) {

	$localName = $fontName;
	if ($options['regular_in_normal'])
		$localName .= " Regular";

	// false: not italics
	// true: italics
	$styles = $options['has_italics'] ? array(false, true) : array(false);

	$sourcePrefix = "url(../fonts/" . str_replace(' ', '_', $fontName) . "/" . str_replace(' ', '', $fontName) . "-";
	$sourceSuffix = ".ttf) format('truetype')";
	
	foreach ($options['weights'] as $weight) {
		foreach ($styles as $style) {
			$dots = $WEIGHT_UNITS[$weight];

			$buffer .= "@font-face {\n";
			$buffer .= "\tfont-family: '$fontName';\n";
			$buffer .= "\tfont-style: " . ($style ? 'italic' : 'normal') . ";\n";
			$buffer .= "\tfont-weight: $dots;\n";

			$prefix = $sourcePrefix;
			$type = $weight . ($style ? 'Italic' : '');
			if ($weight == 'Regular') {
				if ($style) {
					// Ga niet naar RegularItalic zoals normaal
					$type = 'Italic';
				} else {
					// Gebruik de lokale versie bij een gebruiker, als deze deze heeft geinstalleerd.
					$prefix = "local('" . str_replace(' ', '', $fontName) . "-Regular'), " . $prefix;
					$prefix = "local('$localName'), " . $prefix;
				}
			}
			$source = $prefix . $type . $sourceSuffix;

			$buffer .= "\tsrc: $source;\n";
			$buffer .= "}\n";
			$buffer .= "\n";
		}
	}
}

$buffer .= <<<OTHER
@font-face {
	font-family: 'Palanquin';
	src: url('../bootstrap-fonts/palanquin-regular.ttf');
}

OTHER;

file_put_contents("aes-fonts.gen.css", $buffer);

