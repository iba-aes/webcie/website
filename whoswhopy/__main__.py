"""whoswhopy.__main__ - Zet de uiteindelijke webserver op."""

# We gaan ervan uit dat dit gerund wordt met het volgende commando:
# sudo -u apache sh -c '. venv/bin/activate; python3 -m whoswhopy'

from whoswhopy.app import maak_app

app = maak_app()
app.run(host='0.0.0.0')
