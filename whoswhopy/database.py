"""whoswhopy.database - Maak een databaseverbinding."""

import sqlalchemy
import sqlalchemy.orm

import whoswhopy.config as config


def geef_wsw4db(auth):
    """Maak verbinding met de WhosWho4-database.

    :param auth: De authorisatie van de huidige gebruiker.
    """
    db_user, db_pass = auth.geef_db_auth()
    wsw4db_engine = sqlalchemy.create_engine(
        config.geef('whoswho4_db_uri').format(
            username=db_user, password=db_pass, whoswho4_db=config.geef('whoswho4_db')
        )
    )
    # Maak een sessie en begin die meteen.
    return sqlalchemy.orm.sessionmaker(bind=wsw4db_engine)()
