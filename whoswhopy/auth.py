"""whoswhopy.auth - Autorisatie (en autenticatie)."""

from enum import Enum
import re
import os

import whoswhopy.config as config

# Een dict {gebruikersnaam: wachtwoord} voor de database(s).
_db_wachtwoorden = {}


class AuthRecht(Enum):
    """De rechten die een gebruiker mogelijk kan hebben.

    De rechten die hierin beschreven staan,
    worden gegeven onafhankelijk van de overige rechten.
    """

    auth_customizen = 'auth-customizen'
    beheerpaginas = 'beheerpaginas'
    media_bekijken = 'media-bekijken'


class Auth:
    """Abstracte klasse voor een authorisatiesysteem.

    Een implementatie hiervan hoeft niet alle auth-checks te implementeren,
    alleen de volgende methoden:
     * check_recht
     * _geef_db_level
    Uiteraard mag je de overige methodes wel overriden.

    Je kan verscheidene versies van deze klasse implementeren,
    bijvoorbeeld zodat een script kan beslissen welke rechten aanstaan,
    en zodat goden alles mogen.
    """

    def check_recht(self, recht):  # pragma: no cover
        """Heeft de huidige gebruiker toegang tot een bepaald rechtenlevel?

        Verscheidene implementaties hiervan zijn mogelijk,
        zodat bijvoorbeeld scripts en goden zelf mogen kiezen,
        en dat je niets speciaals mag als je niet bent ingelogd.
        """
        raise NotImplementedError()

    def _geef_db_level(self):  # pragma: no cover
        """Geef het databaselevel van de huidige gebruiker.

        Dit moet een string geven die samen met config.geef('DB_USER_PREFIX')
        een gebruikersnaam voor de database oplevert.
        """
        raise NotImplementedError()

    def geef_db_auth(self):
        """Geef de inloggegevens voor de database voor de huidige authlevel.

        Returnt een tuple (gebruikersnaam, wachtwoord).
        """
        level = self._geef_db_level()
        user = config.geef('DB_USER_PREFIX', '') + level
        return user, _db_wachtwoorden[user]


class GastAuth(Auth):
    """Authorisatieklasse die niets toelaat."""

    def check_recht(self, recht):
        return False

    def _geef_db_level(self):
        return 'gast'


class IngelogdAuth(Auth):
    """Authorisatieklasse voor iemand die ingelogd is op de site.

    Afhankelijk van hun specifieke bezigheden krijgen ze extra rechten.
    Om dat bij te houden, gebruiken we het contactid van de gebruiker.
    """

    def __init__(self, contactid):
        """Constructor op basis van het contactid.

        We nemen een contactid, niet een Lid-object,
        omdat de authlevels ook bepalen hoe we verbinden met de db.

        :param contactid: int met het lidnummer/contactid van de huidige gebruiker
        """
        self.contactid = contactid

    def check_recht(self, recht):
        # Dingen die elk ingelogd lid mag:
        if recht == AuthRecht.media_bekijken:
            return True

        # Hier kunnen we rechten checken afhankelijk van de lidtoestand,
        # bijvoorbeeld: welke commissies doet deze persoon?
        # Daarvoor hebben we wel verbinding met de db nodig.
        # Zodra dat nodig is, kunnen we verzinnen hoe we hieraan willen komen.

        # Default: nope!
        return False

    def _geef_db_level(self):
        return 'ingelogd'


class BestuurAuth(IngelogdAuth):
    """Authorisatieklasse voor bestuursleden."""

    def check_recht(self, recht):
        # Uiteraard mag het bestuur alles wat een gewone gebruiker ook mag:
        if super().check_recht(recht):
            return True

        # Maar ze mogen ook de volgende bestuursspecifieke dingen:
        if recht == AuthRecht.beheerpaginas:
            return True

        # En sommige dingen zijn alleen voor goden:
        return False

    def _geef_db_level(self):
        # Bestuur heeft geen aparte db-user, dus we laten ze mee op godrechten.
        return 'god'


class GodAuth(Auth):
    """Authorisatieklasse o.a. nuttig voor webmeesters: je mag echt alles."""

    def check_recht(self, recht):
        return True

    def _geef_db_level(self):
        # We nemen aan dat goden alles wat whoswhopy kan, ook mogen in de database.
        #
        # Technisch gezien kunnen we nog niveaus hierboven maken:
        # iemand die 'root' is, mag ook databasestructuur aanpassen.
        # Het is qua security twijfelachtig of je dit zou willen toelaten,
        # beter om dat soort dingen nooit in dit programma toe te laten.
        return 'god'


class InstelbareAuth(Auth):
    """Authorisatieklasse o.a. nuttig voor scripts: je mag zelf kiezen."""

    def __init__(self, level, rechten):
        """Constructor.

        :param level: String met het db-level.
        Moet een correcte returnwaarde zijn van _geef_db_level.
        :param rechten: set met speciaal aangezette rechten.
        Let op: als je een recht aanzet, zorg ervoor dat je dit in de database wel kan,
        dus dat de waarde van level goed genoeg is.
        """
        self.level = level
        self.rechten = rechten

    def check_recht(self, recht):
        return recht in self.rechten

    def _geef_db_level(self):
        return self.level


def auth_ingelogd_als(contactid):
    """Geef het authorisatieobject met de rechten voor de gegeven gebruiker.

    De gebruiker identificeren we aan de hand van hun contactid,
    die bijvoorbeeld in de sessie ingesteld zou kunnen zijn.

    :param lidnr: Het contactid van de ingelogde gebruiker, of None voor uitgelogd.
    :rtype: Auth
    """
    if not contactid:
        return GastAuth()
    else:
        if config.webcielid(contactid):
            return GodAuth()
        elif config.bestuurslid(contactid):
            return BestuurAuth(contactid)
        else:
            return IngelogdAuth(contactid)


# Lees de db-wachtwoorden uit het PHP-bestand waar ze staan ingesteld.
# TODO: configureer dit beter.
db_wachtwoord_regex = re.compile(r"'([^']+)'\s*=>\s*getenv\('(.*)'\)")
with open('www/secret.php', 'r') as secret_php:
    for line in secret_php:
        match = db_wachtwoord_regex.search(line)
        if match is not None:
            _db_wachtwoorden[match.group(1)] = os.environ.get(match.group(2))
