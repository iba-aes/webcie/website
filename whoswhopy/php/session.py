"""whoswhopy.php.session - Lees en schrijf PHP-sessies vanuit Python."""

import secrets

import flask
import phpserialize

from whoswhopy import config


class PHPSessionBackend(flask.sessions.SessionInterface):
    def open_session(self, app, request):
        """Lees een sessie in of begin een nieuwe."""

        # Houd bij of de sessiecookie nodig is.
        cookie_nodig = False

        sessienaam = request.cookies.get(config.geef_verplicht('sessie_cookie'))
        if not sessienaam:
            # We moeten een nieuwe sessie aanmaken,
            # dus een cookie instellen.
            cookie_nodig = True
            sessienaam = secrets.token_hex()
            flask.current_app.logger.debug('nieuwe sessie %s', sessienaam)
            return PHPSession(sessienaam, cookie_nodig)

        with self._open_sessie_bestand(sessienaam, 'rb') as sessiebestand:
            # Skip eerst over wat symfonywaarden heen
            verwacht = b'_sf2_attributes|'
            gevonden = sessiebestand.read(len(verwacht))
            if gevonden == b'':
                # De sessie heeft nog geen data,
                # dat is prima,
                return PHPSession(sessienaam, cookie_nodig)

            if verwacht != gevonden:
                # Hee ho, hier klopt niet van!
                # Maar we geven geen exceptie, want dit schijnt gewoon wel eens te gebeuren.
                # TODO: wat als PHP schrijft naar een bestand dat wij net lezen?
                flask.current_app.logger.warning(
                    'Corrupt sessiebestand (voor Symfony) %s; gevonden = %s',
                    sessienaam,
                    gevonden,
                )
                return PHPSession(sessienaam)

            try:
                sessiedata = phpserialize.load(
                    sessiebestand,
                    decode_strings=True,
                    object_hook=phpserialize.phpobject,
                )
            except ValueError:
                flask.current_app.logger.info(
                    'Corrupt sessiebestand (voor phpserialize) %s', sessienaam
                )
                return PHPSession(sessienaam, cookie_nodig)

        return PHPSession(sessienaam, cookie_nodig, **sessiedata)

    def save_session(self, app, session, response):
        """Bewaar de nieuwe versie van de sessie."""

        sessienaam = session.naam
        with self._open_sessie_bestand(sessienaam, 'wb') as sessiebestand:
            sessiedata = dict(session)
            sessiebestand.write(b'_sf2_attributes|')
            phpserialize.dump(sessiedata, sessiebestand)

        if session.cookie_nodig:
            response.set_cookie(
                config.geef_verplicht('sessie_cookie'),
                sessienaam,
                domain=config.geef_verplicht('hostname'),
                secure=True,
                httponly=True,
            )

    def _open_sessie_bestand(self, sessienaam, *args, **kwargs):
        """Wrapper voor open die een sessiebestand opent."""
        bestandsnaam = config.geef_verplicht('sessie_dir') + config.geef_verplicht(
            'sessie_bestandsnaam_formaat'
        ).format(sessienaam)
        return open(bestandsnaam, *args, **kwargs)


class PHPSession(dict, flask.sessions.SessionMixin):
    def __init__(self, naam, cookie_nodig, **kwargs):
        super().__init__(**kwargs)
        self.naam = naam
        self.cookie_nodig = cookie_nodig
