"""whoswhopy.php.entry - Ondersteun het aanroepen van PHP-entries vanuit Python."""

import flask
import subprocess


class PHPEntry:
    """Het equivalent van een vfsEntry voor whoswhopy.

    Deze klasse onthoudt een displaynaam en php-code om uit te voeren.
    In feite is het een nuttige wrapper om de functie response_uit_php.
    """

    def __init__(self, app, route, displaynaam, bestand, functie, **opties):
        self.displaynaam = displaynaam
        self.bestand = bestand
        self.functie = functie

        if 'endpoint' not in opties:
            opties['endpoint'] = self.displaynaam

        app.add_url_rule(route, view_func=self, **opties)

    def __call__(self):
        """Wordt door flask aangeroepen om de entry te tonen.

        Roept php aan en returnt de response, door middel van response_uit_php.
        """
        return response_uit_php(
            specified='true',
            file=self.bestand,
            function=self.functie,
            display_name=self.displaynaam,
        )


def response_uit_php(**parameters):
    """Roep php aan om een response te krijgen.

    flask.request wordt uitgelezen en doorgegeven aan php.
    """
    wwwrootdir = 'www'
    php_script = 'www/space/space-cgi.php'

    # Lees POST-data uit.
    # Dit gebeurt zo vroeg mogelijk,
    # want bijvoorbeeld flask.request.form gooit deze data weg na parsen.
    post_data = flask.request.get_data()

    # De environmentvariabelen voor PHP.
    # Deze komen in $_SERVER terecht.
    env = {
        # CGI-gerelateerd:
        'GATEWAY_INTERFACE': 'CGI/1.1',
        # HTTP-gerelateerd:
        'PATH_INFO': flask.request.path,  # Geeft requestpad zonder host of queryparameters.
        'REDIRECT_STATUS': '200 OK',  # Geef door aan PHP dat we via CGI aanroepen.
        'REMOTE_ADDR': flask.request.remote_addr,
        'REQUEST_METHOD': flask.request.method,
        'QUERY_STRING': flask.request.query_string,
        'SERVER_NAME': flask.request.host,
        'SERVER_PORT': '80',  # TODO: bedenk deze beter
        'SERVER_PROTOCOL': 'HTTP/1.0',  # TODO: bedenk deze beter
        # PHP-instellingen:
        'PATH_TRANSLATED': php_script,
        'SCRIPT_NAME': php_script,
    }
    # Stel CONTENT_LENGTH in.
    # Volgens de spec mag dit alleen als er daadwerkelijk postdata is meegestuurd.
    if post_data:
        env['CONTENT_LENGTH'] = str(len(post_data))
        env['CONTENT_TYPE'] = flask.request.content_type
    # Stel overige headers in.
    for name, value in flask.request.headers.items():
        env_name = name.upper().replace('-', '_')
        # Stop alleen geldige identifiers in de environment.
        if not env_name.isidentifier():
            continue
        env['HTTP_' + env_name] = value
    # Stel overige parameters in.
    for name, value in parameters.items():
        env_name = name.upper().replace('-', '_')
        # Stop alleen geldige identifiers in de environment.
        if not env_name.isidentifier():
            continue
        env['SPACE_' + env_name] = value

    # Roep php aan.
    commando = ['php-cgi']
    try:
        # TODO: als we Python 3.5 hebben, kunnen we subprocess.run doen
        whoswho4 = subprocess.Popen(
            commando,
            stdin=subprocess.PIPE,  # Voor POST-data
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            env=env,  # Geef de instellingen mee.
        )
    except subprocess.CalledProcessError as e:
        # Kijk of PHP nog wat te melden heeft.
        _, stderr = whoswho4.communicate()
        raise Exception(b'PHP faalde met melding: ' + stderr)

    # Lees response uit van de stdout.
    # Let op dat dit een bytes-object is (want we kunnen ook binaire data krijgen).
    stdout, _ = whoswho4.communicate(post_data)
    # Splits op headers en body
    try:
        header_str, body = stdout.split(b'\r\n\r\n', 1)
    except ValueError:
        return stdout, 500
    headers = {}
    for header_line in header_str.split(b'\r\n'):
        name, value = header_line.split(b': ', 1)
        # We gaan ervan uit dat headers alleen maar geldige ASCII bevatten.
        headers[name.decode('ascii')] = value.decode('ascii')

    # PHP geeft status als header indien niet 200,
    # parse die dus uit de response om aan Flask te geven.
    status = headers.get('Status', '200 OK')

    return body, status, headers
