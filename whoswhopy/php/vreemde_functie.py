"""whoswhopy.php.vreemde_functie - Definieert het systeem van vreemde functies en hoe ze aan te roepen zijn."""

import subprocess
import os


class VreemdeError(subprocess.CalledProcessError):
    """Wordt gegooid als het PHP-proces een foutcode geeft."""

    pass


class VreemdeFunctie:
    """Een functie die in PHP leeft, maar die je vanuit Python kan aanroepen.

    We ondersteunen (op dit moment) functies en statische methoden.
    """

    def __init__(self, bestand, klasse, functie=None):
        """Specificeer een functie gegeven een bestand, klasse en functie of een bestand en functie.

        Pas bij het aanroepen wordt PHP-code uitgevoerd.
        """
        if functie is None:
            functie = klasse
            klasse = None

        self.bestand = bestand
        self.klasse = klasse
        self.functie = functie

    def __call__(self, stdin='', *, level='god', **kwargs):
        """Voer de functie uit.

        We nemen stdin van het proces en geven stdout, allebei als strings.
        Je kan ook environmentparameters instellen door middel van kwargs.

        De stdout van het php-proces moet geldige utf-8 zijn,
        want we decoderen het naar een Unicode-string.
        Zo niet, gooit deze functie een UnicodeDecodeError.

        :param stdin: De waarden die naar stdin worden geschreven.
        :type stdin: str
        :param level: Het auth-level dat het proces heeft.
        Moet overeenkomen met een waarde in www/space/auth/autorisatie.php:getLevel.

        :return: stdout van het php-proces
        :rtype: str
        """

        # Vergelijk deze code met whoswhopy.php.entry.response_uit_php
        php_script = 'space/space-cli.php'
        working_dir = 'www'

        commando = ['php', php_script, '--bestand', self.bestand]
        # Alleen als we een klasse hebben, geven we dat argument mee.
        if self.klasse:
            commando.extend(['--klasse', self.klasse])
        commando.extend(['--functie', self.functie])
        commando.extend(['--level', level])

        # TODO: als we Python 3.5 hebben, kunnen we subprocess.run doen
        space_cli = subprocess.Popen(
            commando,
            cwd=working_dir,  # Space gaat ervan uit dat we in een bepaalde working dir zitten.
            stdin=subprocess.PIPE,  # Zo geven we *args mee.
            stdout=subprocess.PIPE,  # Zo lezen we returnwaarde uit.
            stderr=subprocess.PIPE,  # Zo lezen we foutmeldingen uit.
            env=kwargs,  # Zo geven we **kwargs mee.
        )

        # Lees returnwaarde uit van de stdout.
        # Let op dat dit een bytes-object is (want we kunnen ook binaire data krijgen).
        stdout, _ = space_cli.communicate()
        if space_cli.returncode:
            # Kijk of PHP nog wat te melden heeft.
            raise VreemdeError(
                returncode=space_cli.returncode, cmd=commando, output=stdout
            )

            # Het is een bytes, maar we gaan ervan uit dat het utf-8 is.
        return stdout.decode('utf-8')
