"""whoswhopy.fotoweb - Host verscheidene foto's."""

from enum import Enum
import flask

from whoswhopy import config
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.php.vreemde_functie import VreemdeFunctie


class Afmeting(Enum):
    groot = 'groot'
    medium = 'medium'
    origineel = 'origineel'
    thumbnail = 'thumbnail'


# De naam van een afmeting is niet gelijk aan de map
# waar de foto's met deze afmeting te vinden zijn.
_map_voor_afmeting = {
    Afmeting.groot: 'groot',
    Afmeting.medium: 'medium',
    Afmeting.thumbnail: 'thumb',
}

# De volgende extensies ondersteunen we voor foto's specifiek, films specifiek en media:
_extensies_foto = {'jpg', 'jpeg', 'gif', 'png'}
_extensies_film = {'flv', 'avi', 'mpg', 'mpeg', 'mov', 'wmv', 'mp4', 'ogg'}
_extensies_media = _extensies_foto.union(_extensies_film)

_media_geef_pad_ffi = VreemdeFunctie(
    'WhosWho4/Fotoweb/DBObject/Media.cls.php', 'Media', 'geefAbsLoc_ffi'
)


class MediaBestand:
    """Representeert een bekijkbare vorm van een Media.

    Hiervoor heb je uiteraard een Media nodig,
    maar ook een afmeting en misschien een extensie.
    """

    def __init__(self, wsw4db, media_id, afmeting=Afmeting.origineel, extensie=None):
        self.media = wsw4db.query(Media).get(media_id)
        if not self.media:
            raise ValueError("Geen media gevonden met id {}".format(media_id))
        self.afmeting = afmeting
        self.extensie = extensie

    def geef_pad(self, content_root):
        """Geef het pad naar het bestand, of gooi een FileNotFoundError."""
        pad = _media_geef_pad_ffi(
            mediaid=str(self.media.mediaID),
            format=self.afmeting,
            extensie=self.extensie if self.extensie else '',
            content_root=str(content_root),
        )
        return pad


def stuur_media(content_root, wsw4db, media_id, afmeting, extensie=None):
    """Verstuur de media naar de browser.

        :param content_root: Het pad waar geÃploade bestanden te vinden zijn.
        :param wsw4db: De database voor WhosWho4.
    :param media_id: Het id van de Media. Ongeldige id's geven een 403-error.
    :param afmeting: Hoofdletterongevoelig. Hoe groot de Media weergegeven moet worden.
        Moet een zijn van:
        * "thumbnail" -- toont alleen thumbnail (content-type: image/jpeg)
        * "medium" -- toont medium formaat
        * "groot" - groot formaat, maar niet geresized naar standaardafmetingen (zoals 'medium') en dus zonder witte randen
        * "medium.flv" -- idem
        * "origineel" -- toont origineel

    Correspondeert met Media::passthruMediaFile.
    """
    afmeting = afmeting.lower()

    bestand = MediaBestand(wsw4db, media_id, afmeting, extensie)
    # TODO: zorg ervoor dat de cache gevuld is
    return flask.send_from_directory(content_root, bestand.geef_pad(content_root))
