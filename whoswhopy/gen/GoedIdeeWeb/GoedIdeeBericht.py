from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.GoedIdeeWeb.GoedIdee import GoedIdee
from whoswhopy.gen.Lid import Lid


class GoedIdeeBericht(Entiteit):
    __tablename__ = "GoedIdeeBericht"
    goedideeBerichtID = Column(Integer, primary_key=True, nullable=False)
    goedidee_goedideeID = Column(Integer, nullable=False)
    goedidee = relationship("GoedIdee", foreign_keys=[goedidee_goedideeID])
    melder_contactID = Column(Integer, nullable=False)
    melder = relationship("Lid", foreign_keys=[melder_contactID])
    titel = Column(String(255), nullable=True)
    status = Column(
        Enum("GEMELD", "AFGEWEZEN", "GEKEURD", "BEZIG", "UITGEVOERD", "VERJAARD"),
        nullable=False,
    )
    bericht = Column(Text, nullable=True)
    moment = Column(DateTime, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "GoedIdeeBericht"}
    __table_args__ = (
        ForeignKeyConstraint(
            ["goedidee_goedideeID"],
            ["GoedIdee.goedideeID"],
            name="GoedIdeeBericht_Goedidee",
        ),
        ForeignKeyConstraint(
            ["melder_contactID"], ["Lid.contactID"], name="GoedIdeeBericht_Melder"
        ),
    )
