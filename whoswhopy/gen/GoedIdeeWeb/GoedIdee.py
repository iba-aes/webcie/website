from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid


class GoedIdee(Entiteit):
    __tablename__ = "GoedIdee"
    goedideeID = Column(Integer, primary_key=True, nullable=False)
    melder_contactID = Column(Integer, nullable=False)
    melder = relationship("Lid", foreign_keys=[melder_contactID])
    titel = Column(String(255), nullable=False)
    omschrijving = Column(String(255), nullable=False)
    status = Column(
        Enum("GEMELD", "AFGEWEZEN", "GEKEURD", "BEZIG", "UITGEVOERD", "VERJAARD"),
        nullable=False,
    )
    publiek = Column(Boolean, server_default="false", nullable=False)
    moment = Column(DateTime, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "GoedIdee"}
    __table_args__ = (
        ForeignKeyConstraint(
            ["melder_contactID"], ["Lid.contactID"], name="GoedIdee_Melder"
        ),
    )
