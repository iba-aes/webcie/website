"""whoswhopy.app - Definieer de pagina's die op de site komen."""

# We gaan ervan uit dat dit gerund wordt met het volgende commando:
# sudo -u apache sh -c '. venv/bin/activate; python3 -m whoswhopy'

import functools
import pathlib

import flask

from htmlelement import HTMLElement, HTMLPage
from whoswhopy.auth import AuthRecht, auth_ingelogd_als
import whoswhopy.config as config
import whoswhopy.database as db
from whoswhopy.gen.alles import alle_klassen
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.fotoweb import stuur_media
from whoswhopy.php.entry import PHPEntry, response_uit_php
from whoswhopy.php.session import PHPSessionBackend


def webauth():
    """Autorisatieconstructor die kijkt naar de huidige request voor de auth."""

    lidnr = flask.session.get('mylidnr')
    return auth_ingelogd_als(lidnr)


class WhosWhoPy(flask.Flask):
    def __init__(self, stdauth, geef_wsw4db, session_interface, content_root):
        """Maak een Flask-applicatie met geen pagina's erin.

        Voor praktische doeleinden wil je waarschijnlijk gebruik maken van maak_app.

        :param stdauth: De constructor voor een Auth-object die gebruikt wordt in de met_stdauth-decorator.

        :param geef_wsw4db: Functie die gegeven een Auth-object connectie maakt met de database.

        :param session_interface: Default: lees sessies uit van PHP.
        Je kan dit overriden, wat vooral nuttig is voor testen, zonder dat je je testuser in de groep hoeft te stoppen die PHP-sessies mag inkijken.

        :param content_root: Een pad naar de directory waar (in subdirectories) alle geuploade bestanden terechtkomen.
        :type content_root: pathlib.Path
        """

        super().__init__('WhosWhoPy')

        self.stdauth = stdauth
        self.geef_wsw4db = geef_wsw4db
        if not session_interface:
            self.session_interface = PHPSessionBackend()
        else:
            self.session_interface = session_interface
        self.content_root = content_root

    def met_content_root(self, func):
        """Decorator die een pagina vertelt waar geuploade bestanden te vinden zijn.

        De content_root wordt meegegeven als een pathlib.Path.

        De content_root wordt meegegeven als `func(*args, **kwargs, content_root=...)`.
        """

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            auth = self.stdauth()
            return func(*args, content_root=self.content_root, **kwargs)

        return wrapped

    def met_stdauth(self, func):
        """Decorator die aan een pagina informatie over authorisatie van de gebruiker geeft.

        De auth wordt meegegeven als `func(*args, **kwargs, auth=...)`.
        """

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            auth = self.stdauth()
            return func(*args, auth=auth, **kwargs)

        return wrapped

    def met_wsw4db(self, func):
        """Decorator die aan een pagina een verbinding met de wsw4db meegeeft.

        Deze verbinding wordt meegegeven als `func(*args, **kwargs, wsw4db=...)`.

        We gaan ervan uit dat de functie een keyword-(only-)argument krijgt
        genaamd auth, die de authorisatie van de huidige gebruiker is.
        Gebruik bijvoorbeeld de decorator met_stdauth om dit voor elkaar te krijgen.
        """

        @functools.wraps(func)
        def wrapped(*args, auth, **kwargs):
            wsw4db = self.geef_wsw4db(auth)
            return func(*args, auth=auth, wsw4db=wsw4db, **kwargs)

        return wrapped

    def vereis_rechten(self, recht):
        """Decorator die bij het laden van een pagina error 403 geeft
        indien de gebruiker niet de juiste rechten heeft.

        We gaan ervan uit dat de functie een keyword-(only-)argument krijgt
        genaamd auth, die de authorisatie van de huidige gebruiker is.
        Gebruik bijvoorbeeld de decorator met_stdauth om dit voor elkaar te krijgen.
        """

        def decorator(func):
            @functools.wraps(func)
            def wrapped(*args, auth, **kwargs):
                if not auth.check_recht(recht):
                    return flask.abort(403)

                return func(*args, auth=auth, **kwargs)

            return wrapped

        return decorator


def maak_app(
    stdauth=webauth,
    geef_wsw4db=db.geef_wsw4db,
    session_interface=None,
    content_root=None,
):
    """Maak een Flask-applicatie met alle pagina's erin.

    :param stdauth: De constructor voor een Auth-object die we gaan gebruiken.

    :param geef_wsw4db: Functie die gegeven een Auth-object connectie maakt met de database.

    :param session_interface: Default: lees sessies uit van PHP.
    Je kan dit overriden, wat vooral nuttig is voor testen, zonder dat je je testuser in de groep hoeft te stoppen die PHP-sessies mag inkijken.

    :param content_root: Een pad naar de directory waar (in subdirectories) alle geuploade bestanden terechtkomen.
    :type content_root: pathlib.Path

    :rtype: WhosWhoPy
    """
    if not content_root:
        content_root = pathlib.Path(config.geef_verplicht('content_root'))

    app = WhosWhoPy(
        stdauth=stdauth,
        geef_wsw4db=geef_wsw4db,
        session_interface=session_interface,
        content_root=content_root,
    )

    @app.route('/')
    def root():
        """De homepagina staat op Home."""
        return flask.redirect(flask.url_for('Home'))

    PHPEntry(
        app,
        '/Home',
        'Home',
        'WhosWho4/Controllers/Vereniging.php',
        'Vereniging_Controller::homepagina',
    )

    @app.route('/Beheer')
    @app.met_stdauth
    @app.met_wsw4db
    @app.vereis_rechten(AuthRecht.beheerpaginas)
    def beheer(*, auth, wsw4db):
        pagina = HTMLPage()
        for klasse in sorted(alle_klassen, key=lambda klasse: klasse.__name__):
            # Alleen echte databasewaarden (oftewel strikte subklassen van Entiteit) tonen!
            if klasse is Entiteit or not issubclass(klasse, Entiteit):
                continue

            kwerrie = wsw4db.query(klasse).limit(10)
            objecten = kwerrie.all()

            # Zorg ervoor dat de output in een zinnige volgorde komt,
            # door middel van introspectie op de klasse.
            resultaatvolgorde = []
            for naam, kolom in klasse.__table__.columns.items():
                resultaatvolgorde.append(naam)
            tabel = HTMLElement('table')
            tabel.add(
                HTMLElement(
                    'thead',
                    [
                        HTMLElement(
                            'tr',
                            [HTMLElement('th', [kolom]) for kolom in resultaatvolgorde],
                        )
                    ],
                )
            )
            for obj in objecten:
                rij = HTMLElement('tr')
                for kolom in resultaatvolgorde:
                    rij.add(HTMLElement('td', [str(getattr(obj, kolom))]))
                tabel.add(rij)

            pagina.add(klasse.__name__ + str(tabel))
        return str(pagina)

    @app.route('/FotoWeb/Media/<int:media_id>')
    @app.met_content_root
    @app.met_stdauth
    @app.met_wsw4db
    @app.vereis_rechten(AuthRecht.media_bekijken)
    def geef_media_origineel(media_id, *, content_root, auth, wsw4db):
        """Fotoweb: geef het bestand met gegeven Media-id, op originele grootte."""
        return geef_media(
            media_id, 'origineel', content_root=content_root, auth=auth, wsw4db=wsw4db
        )

    @app.route('/FotoWeb/Media/<int:media_id>/<afmeting>')
    @app.met_content_root
    @app.met_stdauth
    @app.met_wsw4db
    @app.vereis_rechten(AuthRecht.media_bekijken)
    def geef_media(media_id, afmeting, *, content_root, auth, wsw4db):
        return stuur_media(content_root, wsw4db, media_id, afmeting)

    @app.route('/Layout/<path:bestandspad>')
    def geef_layout(bestandspad):
        """Geef een statisch layoutbestand."""
        return flask.send_from_directory('www/Layout', bestandspad)

    @app.route('/Minified/<soort>/<bestand>')
    def geef_minified(soort, bestand):
        """Geef een geminificeerd bestand vanaf het fs."""
        # De bestandsnaam ziet er uit als file.tijd.(min.)?.ext, bijvoorbeeld:
        # a-eskwadraat.1531244232.js
        # aes-debug.1523720907.min.css
        # Er staan ook dependencies in de Minified-map, die worden door geef_overig_minified gedaan.
        delen_bestandsnaam = bestand.split('.')
        if len(delen_bestandsnaam) == 3:
            basis, tijd, ext = delen_bestandsnaam
            minified = False
            bestandsnaam_fs = '.'.join([basis, ext])
        elif len(delen_bestandsnaam) == 4:
            basis, tijd, min, ext = delen_bestandsnaam
            minified = True
            bestandsnaam_fs = '.'.join([basis, min, ext])
        else:
            # Dit is geen 'echt' minified bestand maar een dependency, stuur dus door.
            return geef_overig_minified(soort + '/' + bestand)

        # TODO: willen we controleren dat de extensies met elkaar kloppen?
        # Merk op dat we safe_join en send_from_directory doen om grapjes als /Minified/../secret.php te voorkomen.
        dirnaam_fs = flask.safe_join('www/Minified', soort)
        return flask.send_from_directory(dirnaam_fs, bestandsnaam_fs)

    @app.route('/Minified/<path:bestandspad>')
    def geef_overig_minified(bestandspad):
        """Geef een statische file die dependency is van een minified bestand."""
        return flask.send_from_directory('www/Minified', bestandspad)

    @app.route('/', methods=['GET', 'POST'])
    @app.route('/<path:subpath>', methods=['GET', 'POST'])
    def fallback(subpath=''):
        """Gooi de response direct door PHP."""
        return response_uit_php(specified='false')

    return app
