"""ActiviteitVraag verplicht maken

Revision ID: cef95388116b
Revises: 8719c99bd154
Create Date: 2019-05-16 18:12:02.569073

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'cef95388116b'
down_revision = '8719c99bd154'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('ActiviteitVraag', sa.Column('verplicht', sa.Boolean(), server_default='0', nullable=False))


def downgrade():
    op.drop_column('ActiviteitVraag', 'verplicht')
