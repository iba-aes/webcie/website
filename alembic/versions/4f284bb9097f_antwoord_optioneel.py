"""antwoord_optioneel

Revision ID: 4f284bb9097f
Revises: cef95388116b
Create Date: 2024-09-05 15:59:34.567280

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '4f284bb9097f'
down_revision = 'cef95388116b'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('DeelnemerAntwoord', 'antwoord', existing_type=mysql.VARCHAR(255), nullable=True)
    op.alter_column('iDealAntwoord', 'antwoord', existing_type=mysql.VARCHAR(255), nullable=True)


def downgrade():
    op.alter_column('DeelnemerAntwoord', 'antwoord', existing_type=mysql.VARCHAR(255), nullable=False)
    op.alter_column('iDealAntwoord', 'antwoord', existing_type=mysql.VARCHAR(255), nullable=False)
