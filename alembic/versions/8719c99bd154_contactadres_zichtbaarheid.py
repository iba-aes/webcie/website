"""Voeg ContactAdres.zichtbaarheid toe

Revision ID: 8719c99bd154
Revises: 04d5e3210f2d
Create Date: 2024-08-21 20:28:29.082923

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '8719c99bd154'
down_revision = '04d5e3210f2d'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('ContactAdres', sa.Column('zichtbaarheid', mysql.ENUM('IEDEREEN', 'LEDEN', 'COMMISSIE', 'BESTUUR'), server_default=sa.text("'IEDEREEN'"), nullable=False))


def downgrade():
    op.drop_column('ContactAdres', 'zichtbaarheid')
