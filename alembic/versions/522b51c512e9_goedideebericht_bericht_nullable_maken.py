from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

revision = '522b51c512e9'
down_revision = 'b2f24190ffff'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('GoedIdeeBericht', 'bericht',
               existing_type=mysql.TEXT(),
               nullable=True)

def downgrade():
    op.alter_column('GoedIdeeBericht', 'bericht',
               existing_type=mysql.TEXT(),
               nullable=False)

