"""Onthoud of een JaargangArtikel verplicht is

Revision ID: 580ec2543816
Revises: 6b0f3429087a
Create Date: 2018-12-03 21:55:15.765740

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '580ec2543816'
down_revision = '6b0f3429087a'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('JaargangArtikel', sa.Column('verplicht', sa.Boolean(), server_default='1', nullable=False))

def downgrade():
    op.drop_column('JaargangArtikel', 'verplicht')
