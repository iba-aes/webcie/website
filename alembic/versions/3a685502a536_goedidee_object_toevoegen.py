"""GoedIdee object toevoegen

Revision ID: 3a685502a536
Revises: 580ec2543816
Create Date: 2019-04-17 13:55:52.609727

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '3a685502a536'
down_revision = '580ec2543816'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('GoedIdee',
    sa.Column('goedideeID', sa.Integer(), nullable=False),
    sa.Column('melder_contactID', sa.Integer(), nullable=True),
    sa.Column('titel', sa.String(length=255), nullable=False),
    sa.Column('omschrijving', sa.String(length=255), nullable=False),
    sa.Column('status', sa.Enum('GEMELD', 'AFGEWEZEN', 'GEKEURD', 'BEZIG', 'UITGEVOERD', 'VERJAARD'), nullable=False),
    sa.Column('publiek', sa.Boolean(), server_default='0', nullable=False),
    sa.Column('moment', sa.DateTime(), nullable=False),
    sa.Column('gewijzigdWie', sa.Integer(), nullable=True),
    sa.Column('gewijzigdWanneer', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['melder_contactID'], ['Lid.contactID'], name='GoedIdee_Melder'),
    sa.PrimaryKeyConstraint('goedideeID')
    )
    op.create_table('GoedIdeeBericht',
    sa.Column('goedideeBerichtID', sa.Integer(), nullable=False),
    sa.Column('goedidee_goedideeID', sa.Integer(), nullable=False),
    sa.Column('melder_contactID', sa.Integer(), nullable=True),
    sa.Column('titel', sa.String(length=255), nullable=True),
    sa.Column('status', sa.Enum('GEMELD', 'AFGEWEZEN', 'GEKEURD', 'BEZIG', 'UITGEVOERD', 'VERJAARD'), nullable=False),
    sa.Column('bericht', sa.Text(), nullable=False),
    sa.Column('moment', sa.DateTime(), nullable=False),
    sa.Column('gewijzigdWie', sa.Integer(), nullable=True),
    sa.Column('gewijzigdWanneer', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['goedidee_goedideeID'], ['GoedIdee.goedideeID'], name='GoedIdeeBericht_Goedidee'),
    sa.ForeignKeyConstraint(['melder_contactID'], ['Lid.contactID'], name='GoedIdeeBericht_Melder'),
    sa.PrimaryKeyConstraint('goedideeBerichtID')
    )
    op.add_column('PersoonVoorkeur', sa.Column('goedIdeeSortering', sa.Enum('ASC', 'DESC'), server_default='ASC', nullable=False))


def downgrade():
    op.drop_column('PersoonVoorkeur', 'goedIdeeSortering')
    op.drop_table('GoedIdeeBericht')
    op.drop_table('GoedIdee')
