"""melders van goede ideeen niet null mogelijk

Revision ID: b2f24190ffff
Revises: 3a685502a536
Create Date: 2019-04-17 18:56:42.122129

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


revision = 'b2f24190ffff'
down_revision = '3a685502a536'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('GoedIdee', 'melder_contactID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('GoedIdeeBericht', 'melder_contactID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)

def downgrade():
    op.alter_column('GoedIdeeBericht', 'melder_contactID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('GoedIdee', 'melder_contactID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
