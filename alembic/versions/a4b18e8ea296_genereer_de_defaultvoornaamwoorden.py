"""Genereer de defaultvoornaamwoorden

Revision ID: a4b18e8ea296
Revises: b46a09a05984
Create Date: 2018-05-08 17:17:34.177796

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String

# revision identifiers, used by Alembic.
revision = 'a4b18e8ea296'
down_revision = 'b46a09a05984'
branch_labels = None
depends_on = None

# Let op dat we zelf een tabel moeten maken:
# stel dat we deze migratie runnen als
# de Voornaamwoord-tabel is aangepast in de code,
# dan slaat de migratie opeens nergens op.
# (Zie ook https://stackoverflow.com/questions/24612395/_#comment43144168_24623979)
from sqlalchemy.ext.declarative import declarative_base
Entiteit = declarative_base(name="Entiteit")
class Voornaamwoord(Entiteit):
    __tablename__ = "Voornaamwoord"
    woordID = Column(Integer, primary_key=True, nullable=False)
    soort_NL = Column(String(255), nullable=False)
    soort_EN = Column(String(255), nullable=False)
    onderwerp_NL = Column(String(255), nullable=False)
    onderwerp_EN = Column(String(255), nullable=False)
    voorwerp_NL = Column(String(255), nullable=False)
    voorwerp_EN = Column(String(255), nullable=False)
    bezittelijk_NL = Column(String(255), nullable=False)
    bezittelijk_EN = Column(String(255), nullable=False)
    wederkerend_NL = Column(String(255), nullable=False)
    wederkerend_EN = Column(String(255), nullable=False)
    ouder_NL = Column(String(255), nullable=False)
    ouder_EN = Column(String(255), nullable=False)
    kind_NL = Column(String(255), nullable=False)
    kind_EN = Column(String(255), nullable=False)
    brusje_NL = Column(String(255), nullable=False)
    brusje_EN = Column(String(255), nullable=False)
    kozijn_NL = Column(String(255), nullable=False)
    kozijn_EN = Column(String(255), nullable=False)
    kindGrootouder_NL = Column(String(255), nullable=False)
    kindGrootouder_EN = Column(String(255), nullable=False)
    kleinkindOuder_NL = Column(String(255), nullable=False)
    kleinkindOuder_EN = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)

voornaamwoord = Voornaamwoord.__table__

def upgrade():
    op.bulk_insert(voornaamwoord, [
        # We inserten op id=3 en doen daarna update naar id=0 want MySQL...
        {'woordID': 3, 'soort_NL': "Onzijdig", 'soort_EN': "Neuter", 'onderwerp_NL': "die", 'onderwerp_EN': "they", 'voorwerp_NL': "die", 'voorwerp_EN': "them", 'bezittelijk_NL': "diens", 'bezittelijk_EN': "their", 'wederkerend_NL': "hun", 'wederkerend_EN': "them", 'ouder_NL': "ouder", 'ouder_EN': "parent", 'kind_NL': "kindje", 'kind_EN': "child", 'brusje_NL': "brusje", 'brusje_EN': "sibling", 'kozijn_NL': "kozijn", 'kozijn_EN': "cousin", 'kindGrootouder_NL': "kind van mentorgrootouder", 'kindGrootouder_EN': "parent's sibling", 'kleinkindOuder_NL': "%szegger", 'kleinkindOuder_EN': "sibling's child"},
        {'woordID': 1, 'soort_NL': "Man", 'soort_EN': "Male", 'onderwerp_NL': "hij", 'onderwerp_EN': "he", 'voorwerp_NL': "hem", 'voorwerp_EN': "him", 'bezittelijk_NL': "zijn", 'bezittelijk_EN': "his", 'wederkerend_NL': "zich", 'wederkerend_EN': "him", 'ouder_NL': "papa", 'ouder_EN': "dad", 'kind_NL': "zoontje", 'kind_EN': "son", 'brusje_NL': "broertje", 'brusje_EN': "brother", 'kozijn_NL': "neefje", 'kozijn_EN': "cousin", 'kindGrootouder_NL': "oom", 'kindGrootouder_EN': "uncle", 'kleinkindOuder_NL': "%szegger", 'kleinkindOuder_EN': "nephew"},
        {'woordID': 2, 'soort_NL': "Vrouw", 'soort_EN': "Female", 'onderwerp_NL': "zij", 'onderwerp_EN': "she", 'voorwerp_NL': "haar", 'voorwerp_EN': "her", 'bezittelijk_NL': "haar", 'bezittelijk_EN': "her", 'wederkerend_NL': "haar", 'wederkerend_EN': "her", 'ouder_NL': "mama", 'ouder_EN': "mom", 'kind_NL': "dochter", 'kind_EN': "daughter", 'brusje_NL': "zusje", 'brusje_EN': "sister", 'kozijn_NL': "nichtje", 'kozijn_EN': "cousin", 'kindGrootouder_NL': "tante", 'kindGrootouder_EN': "aunt", 'kleinkindOuder_NL': "%szegger", 'kleinkindOuder_EN': "niece"},
    ])
    op.execute(voornaamwoord.update()
        .where(voornaamwoord.c.woordID == 3)
        .values(woordID=0)
    )

def downgrade():
    op.execute(voornaamwoord.delete(voornaamwoord.c.woordID in {0, 1, 2}))

