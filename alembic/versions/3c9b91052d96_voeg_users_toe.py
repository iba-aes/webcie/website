"""Voeg de database users toe

Revision ID: 3c9b91052d96
Create Date: 2019-04-25 21:00:16.279138

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3c9b91052d96'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute("CREATE USER 'gast'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'god'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'ingelogd'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'gitwebhook'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'dibstablet'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'mollie'@'172.16.0.0/255.240.0.0' IDENTIFIED BY '';")
    op.execute("CREATE USER 'gast'@'localhost' IDENTIFIED BY '';")
    op.execute("CREATE USER 'god'@'localhost' IDENTIFIED BY '';")
    op.execute("CREATE USER 'ingelogd'@'localhost' IDENTIFIED BY '';")
    op.execute("CREATE USER 'gitwebhook'@'localhost' IDENTIFIED BY '';")
    op.execute("CREATE USER 'dibstablet'@'localhost' IDENTIFIED BY '';")
    op.execute("CREATE USER 'mollie'@'localhost' IDENTIFIED BY '';")


def downgrade():
	op.execute("DROP USER 'gast'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'gitwebhook'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'god'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'ingelogd'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'mollie'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'dibstablet'@'172.16.0.0/255.240.0.0'");
	op.execute("DROP USER 'gast'@'localhost'");
	op.execute("DROP USER 'gitwebhook'@'localhost'");
	op.execute("DROP USER 'god'@'localhost'");
	op.execute("DROP USER 'ingelogd'@'localhost'");
	op.execute("DROP USER 'mollie'@'localhost'");
	op.execute("DROP USER 'dibstablet'@'localhost'");