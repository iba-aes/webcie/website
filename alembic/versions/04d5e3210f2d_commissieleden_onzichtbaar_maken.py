from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

revision = '04d5e3210f2d'
down_revision = '522b51c512e9'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('CommissieLid', sa.Column('zichtbaar', sa.Boolean(), server_default='0', nullable=False))

def downgrade():
    op.drop_column('CommissieLid', 'zichtbaar')
