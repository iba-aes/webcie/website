"""Activiteit reserveringen

Revision ID: c9fa0c761033
Revises: 4f284bb9097f
Create Date: 2024-10-09 18:34:04.778224

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c9fa0c761033'
down_revision = '4f284bb9097f'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('ActiviteitInformatie', sa.Column('reservering', sa.Enum('GEEN','WERKKAMER','GEZELLIGHEIDSKAMER'), server_default='GEEN', nullable=False))
    op.execute("UPDATE ActiviteitInformatie SET reservering = 'WERKKAMER' WHERE aantalComputers > 0;")
    op.drop_column('ActiviteitInformatie', 'aantalComputers')


def downgrade():
    op.add_column('ActiviteitInformatie', sa.Column('aantalComputers', sa.INT, nullable=False))
    op.execute("UPDATE ActiviteitInformatie SET aantalComputers = 0;")
    op.execute("UPDATE ActiviteitInformatie SET aantalComputers = 10 WHERE reservering = 'WERKKAMER';")
    op.drop_column('ActiviteitInformatie', 'reservering')
