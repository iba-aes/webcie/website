<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis\Fixture;

use Periode;
use WhosWho4\TestHuis\Fixture;

/**
 * Fixture om een Studie-object aan te maken.
 */
class PeriodeFixture implements Fixture
{
	/** @var Periode $object */
	private $object;

	/**
	 * @brief Wordt voor elke test met deze fixture aangeroepen voor de test zelf.
	 *
	 * Kun je gebruiken om bijvoorbeeld een databaseconnectie op te zetten,
	 * of een generiek testobject aan te maken.
	 *
	 * @returns object
	 * Een waarde die als parameter aan de test wordt gegeven.
	 */
	public function erin()
	{
		$this->object = new Periode();
		$this->object->setCollegejaar(2017);
		$this->object->setPeriodeNummer(4);
		$this->object->setDatumBegin(\DateTimeLocale::createFromFormat('Y-m-d', '2018-04-24'));

		return $this->object;
	}

	/**
	 * @brief Wordt voor elke test met deze fixture aangeroepen na de test.
	 *
	 * Kun je gebruiken om bijvoorbeeld een databaseconnectie te stoppen,
	 * of een generiek testobject weer weg te gooien.
	 */
	public function eruit()
	{
		if ($this->object->getInDB())
		{
			$this->object->verwijderen();
		}
	}
}
