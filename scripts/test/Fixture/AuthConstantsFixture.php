<?php

namespace WhosWho4\TestHuis\Fixture;

use BestuursLid;
use Space\Auth\Constants;
use WhosWho4\TestHuis\Fixture;

/**
 * Class AuthConstantsFixture
 * @package WhosWho4\TestHuis\Fixture
 * @brief Fixture die de Constants voor een Auth klasse maakt.
 */
class AuthConstantsFixture implements Fixture
{
	/**
	 * @var Constants
	 */
	private $constants;

	/**
	 * @inheritdoc
	 */
	public function erin()
	{
		$this->constants = new Constants([
			'bestuursLid' => new BestuursLid(111, "prefix"),
			'boekcomLid' => new BestuursLid(112, "prefix", true),
		], 4, 5, 6);
		return $this->constants;
	}

	/**
	 * @inheritdoc
	 */
	public function eruit()
	{
		$this->constants = null;
	}
}