<?php

namespace WhosWho4\TestHuis\Fixture;

use Lid;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een Lid aanmaakt en meegeeft aan de test.
 *
 * Het object is nog niet opgeslagen bij in de test gaan.
 */
class LidFixture implements TestHuis\Fixture
{
	/**
	 * @var Lid
	 * Het object, wordt aangemaakt in erin en weggegooid in eruit.
	 */
	private $object;

	/**
	 * @brief De voornaam van het Lid om te maken.
	 */
	public $voornaam = 'Widmer';

	/**
	 * @brief De achternaam van het Lid om te maken.
	 */
	public $achternaam = 'Witbaard';

	function erin()
	{
		$this->object = new Lid();
		$this->object->setVoornaam($this->voornaam);
		$this->object->setAchternaam($this->achternaam);

		return $this->object;
	}

	function eruit()
	{
		if ($this->object->getInDB())
		{
			$this->object->verwijderen();
		}
	}
}
