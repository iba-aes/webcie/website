<?php

namespace WhosWho4\TestHuis\Fixture;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use WhosWho4\TestHuis;

/**
 * Class SessionFixture
 * @package WhosWho4\TestHuis\Fixture
 * @brief Fixture die een Session aanmaakt voor gebruik in een Auth klasse.
 */
class SessionFixture implements TestHuis\Fixture
{
	/**
	 * @var int
	 */
	private $lidnr;
	/**
	 * @var int
	 */
	private $realUserId;
	/**
	 * @var Session
	 */
	private $session;

	/**
	 * SessionFixture constructor.
	 * @param int $lidnr
	 */
	public function __construct($lidnr = null, $realUserId = null)
	{
		$this->lidnr = $lidnr;
		$this->realUserId = $realUserId;
	}

	/**
	 * @inheritdoc
	 */
	public function erin()
	{
		$this->session = new Session(new MockArraySessionStorage());

		if (!is_null($this->lidnr)) {
			$this->session->set('mylidnr', $this->lidnr);
		}
		if (!is_null($this->realUserId)) {
			$this->session->set('realuserid', $this->realUserId);
		}

		return $this->session;
	}

	/**
	 * @inheritdoc
	 */
	public function eruit()
	{
		$this->session = null;
	}
}