<?php

namespace WhosWho4\TestHuis\Fixture;

use Persoon;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een persoon aanmaakt en meegeeft aan de test.
 *
 * Het object is nog niet opgeslagen bij in de test gaan.
 */
class PersoonFixture implements TestHuis\Fixture
{
	/**
	 * @var Persoon
	 * Het object, wordt aangemaakt in erin en weggegooid in eruit.
	 */
	private $object;

	/**
	 * @brief De voornaam van de persoon om te maken.
	 */	 
	public $voornaam = 'Widmer';
	
	/**
	 * @brief De achternaam van de Persoon om te maken.
	 */
	public $achternaam = 'Witbaard';

	function erin()
	{
		$this->object = new Persoon();
		$this->object->setVoornaam($this->voornaam);
		$this->object->setAchternaam($this->achternaam);

		return $this->object;
	}

	function eruit()
	{
		if ($this->object->getInDB())
		{
			$this->object->verwijderen();
		}
	}
}
