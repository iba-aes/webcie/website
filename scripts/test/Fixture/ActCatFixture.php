<?php

namespace WhosWho4\TestHuis\Fixture;
use ActiviteitCategorie;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een activiteitcategorie in de db stopt en meegeeft aan de test.
 */
class ActCatFixture implements TestHuis\Fixture
{
	/**
	 * @var ActiviteitCategorie
	 * De categorie, wordt aangemaakt in erin en weggegooid in eruit.
	 */
	private $cat;

	function erin()
	{
		$this->cat = new ActiviteitCategorie("testcategorie", "test category");
		$this->cat->opslaan();
		return $this->cat;
	}

	function eruit()
	{
		$this->cat->verwijderen();
	}
}