<?php

namespace WhosWho4\TestHuis\Fixture;

use WhosWho4\TestHuis;

/**
 * @brief Fixture die automatisch de zeurmodus reset naar defaultwaarde.
 */
class ZeurmodusFixture implements TestHuis\Fixture
{
	/**
	 * @var mixed
	 * Slaat de waarde van de zeurmodus op bij ingaan van de test.
	 */
	private $oudeZeurmodus;

	function erin()
	{
		$this->oudeZeurmodus = $this->resetZeurmodus();
	}

	function eruit()
	{
		$this->resetZeurmodus($this->oudeZeurmodus);
	}

	/**
	 * @brief Reset de zeurmodus naar de begintoestand.
	 *
	 * Het idee hiervan is dat je in een test kan doen:
	 * `$oudeZeurmodus = resetZeurmodus();`
	 * `try {...} finally { resetZeurmodus($oudeZeurmodus); }`,
	 * wat dus ook in `ZeurmodusFunctieTest::run` gebeurt.
	 *
	 * Check AUB dat dit overeenkomt met de definitie van `$zeurmodus` in
	 * `www/space/debug/zeurmodus.php`.
	 *
	 * @param mixed waarde De waarde waarheen gereset moet worden.
	 *
	 * @return mixed
	 * De originele waarde van de zeurmodus.
	 */
	function resetZeurmodus($waarde = null)
	{
		global $zeurmodus;

		$origineel = $zeurmodus;
		if (is_null($waarde))
		{
			$zeurmodus = [];
		}
		else
		{
			$zeurmodus = $waarde;
		}
		return $origineel;
	}
}
