<?php

namespace WhosWho4\TestHuis;

use AssertionError;
use LogicException;

use Space\Bedrag;
use Space\GeenChocolaException;

/**
 * Hulpfunctie: neem een Bedrag en stel vast dat die aan de verwachtingen voldoet.
 *
 * @param Bedrag $bedrag
 * @param int $verwachtGeheel
 * @param int $verwachtRest
 *
 * @throws AssertionError
 * @return void
 */
function geheelEnRestTesten(Bedrag $bedrag, $verwachtGeheel, $verwachtRest)
{
	assert(
		$bedrag->getGeheel() == $verwachtGeheel && $bedrag->getRestcenten() == $verwachtRest,
		$bedrag . ' zou eruit moeten zien als' . $verwachtGeheel . ',' . $verwachtRest
	);
}
/**
 * Hulpfunctie: neem een Bedrag en stel vast dat die aan de verwachtingen voldoet.
 *
 * @param Bedrag $bedrag
 * @param float $verwachteFloat
 *
 * @throws AssertionError
 * @return void
 */
function floatTesten(Bedrag $bedrag, $verwachteFloat)
{
	assert(
		$bedrag->alsFloat() == $verwachteFloat,
		$bedrag . ' zou als float ' . $verwachteFloat . ' moeten geven'
	);
}

/**
 * Hulpfunctie: maak een Bedrag uit de hoeveelheid centen en stel vast dat die aan de verwachtingen voldoet.
 *
 * @param int $centen
 * @param int $verwachtGeheel
 * @param int $verwachtRest
 *
 * @throws AssertionError
 * @return void
 */
function centenGeheelEnRestTesten($centen, $verwachtGeheel, $verwachtRest)
{
	geheelEnRestTesten(new Bedrag($centen), $verwachtGeheel, $verwachtRest);
}

/**
 * Hulpfunctie: maak een Bedrag uit de hoeveelheid centen en stel vast dat die aan de verwachtingen voldoet.
 *
 * @param int $centen
 * @param float $verwachteFloat
 *
 * @throws AssertionError
 * @return void
 */
function centenFloatTesten($centen, $verwachteFloat)
{
	floatTesten(new Bedrag($centen), $verwachteFloat);
}

/**
 * Hulpfunctie: parse een Bedrag en stel vast dat die aan de verwachtingen voldoet.
 *
 * Je kan aangeven of het parsen succesvol zou moeten zijn, en zo ja, wat eruit zou moeten komen.
 *
 * @param string $input
 * @param bool $succes Of er chocola van de input gemaakt zou moeten kunnen worden.
 * @param int|null $verwachtGeheel
 * @param int|null $verwachtRest
 *
 * @throws AssertionError
 * @throws GeenChocolaException
 * @throws LogicException
 *
 * @return void
 */
function parsenGeheelEnRestTesten($input, $succes, $verwachtGeheel=null, $verwachtRest=null)
{
	try
	{
		$bedrag = Bedrag::parse($input);
	}
	catch (GeenChocolaException $e)
	{
		// Is het falen correct?
		if ($succes)
		{
			throw $e;
		}
		return;
	}

	// Is het niet-falen correct?
	if (!$succes)
	{
		throw new LogicException($input . " zou niet geparst moeten zijn maar gaf wel " . $bedrag);
	}
	geheelEnRestTesten($bedrag, $verwachtGeheel, $verwachtRest);
}

/**
 * Hulpfunctie: parse een Bedrag uit een maybestring en stel vast dat die aan de verwachtingen voldoet.
 *
 * Je kan aangeven of het parsen succesvol zou moeten zijn, en zo ja, wat eruit zou moeten komen.
 *
 * @param string $input
 * @param int|Bedrag $default
 * @param bool $succes Of er chocola van de input gemaakt zou moeten kunnen worden.
 * @param int|null $verwachtCenten
 *
 * @throws AssertionError
 * @throws GeenChocolaException
 * @throws LogicException
 *
 * @return void
 */
function parsenFromMaybeTesten($input, $default, $succes, $verwachtCenten=null)
{
	try
	{
		$bedrag = Bedrag::parseFromMaybe($input, $default);
	}
	catch (GeenChocolaException $e)
	{
		// Is het falen correct?
		if ($succes)
		{
			throw $e;
		}
		return;
	}

	// Is het niet-falen correct?
	if (!$succes)
	{
		throw new LogicException($input . " zou niet geparst moeten zijn maar gaf wel " . $bedrag);
	}
	assert($bedrag->getCenten() == $verwachtCenten, $input . ' is geparst naar foute waarde ' . $bedrag);
}

/**
 * Gegeven een hoeveelheid centen, assert dat het parsen van dat bedrag als string weer dat bedrag geeft.
 *
 * @param int $centen
 *
 * @throws AssertionError
 * @throws GeenChocolaException
 */
function roundtripTesten($centen)
{
	$bedrag = new Bedrag($centen);
	$alsString = $bedrag->__toString();
	$weerBedrag = Bedrag::parse($alsString);
	$weerCenten = $weerBedrag->getCenten();
	assert($centen == $weerCenten, "parse-roundtrip van " . $centen . " zou hetzelfde moeten zijn, maar gaf " . $weerCenten);
}

$this->def('bedragen omzetten in gehelen en restcenten', function() {
	centenGeheelEnRestTesten(0, 0, 0);
	centenGeheelEnRestTesten(1, 0, 1);
	centenGeheelEnRestTesten(2, 0, 2);
	centenGeheelEnRestTesten(99, 0, 99);
	centenGeheelEnRestTesten(100, 1, 0);
	centenGeheelEnRestTesten(101, 1, 1);
	centenGeheelEnRestTesten(234, 2, 34);
	centenGeheelEnRestTesten(-1, 0, -1);
	centenGeheelEnRestTesten(-2, 0, -2);
	centenGeheelEnRestTesten(-99, 0, -99);
	centenGeheelEnRestTesten(-100, -1, 0);
	centenGeheelEnRestTesten(-101, -1, -1);
	centenGeheelEnRestTesten(-234, -2, -34);
});
$this->def('bedragen omzetten in floats', function() {
	centenFloatTesten(0, 0.00);
	centenFloatTesten(1, 0.01);
	centenFloatTesten(2, 0.02);
	centenFloatTesten(99, 0.99);
	centenFloatTesten(100, 1.00);
	centenFloatTesten(101, 1.01);
	centenFloatTesten(234, 2.34);
	centenFloatTesten(-1, -0.01);
	centenFloatTesten(-2, -0.02);
	centenFloatTesten(-99, -0.99);
	centenFloatTesten(-100, -1.00);
	centenFloatTesten(-101, -1.01);
	centenFloatTesten(-234, -2.34);
});

$this->def('bedragen parsen uit strings', function() {
	// Lege string geeft foutmelding.
	// (Dit is anders dan de Money-klasse.)
	parsenGeheelEnRestTesten('', false);

	// Gehele getallen, met afscheiders voor duizendtallen.
	parsenGeheelEnRestTesten('0', true, 0, 0);
	parsenGeheelEnRestTesten('1', true, 1, 0);
	parsenGeheelEnRestTesten('12', true, 12, 0);
	parsenGeheelEnRestTesten('123', true, 123, 0);
	parsenGeheelEnRestTesten('1234', true, 1234, 0);
	parsenGeheelEnRestTesten('12345', true, 12345, 0);
	parsenGeheelEnRestTesten('1 234', true, 1234, 0);
	parsenGeheelEnRestTesten('12 345', true, 12345, 0);
	parsenGeheelEnRestTesten('1.234', true, 1234, 0);
	parsenGeheelEnRestTesten('12.345', true, 12345, 0);
	parsenGeheelEnRestTesten('1,234', true, 1234, 0);
	parsenGeheelEnRestTesten('12,345', true, 12345, 0);

	// Een punt voor de centen.
	parsenGeheelEnRestTesten('0.00', true, 0, 0);
	parsenGeheelEnRestTesten('0.67', true, 0, 67);
	parsenGeheelEnRestTesten('1.67', true, 1, 67);
	parsenGeheelEnRestTesten('12.67', true, 12, 67);
	parsenGeheelEnRestTesten('123.67', true, 123, 67);
	parsenGeheelEnRestTesten('1234.67', true, 1234, 67);
	parsenGeheelEnRestTesten('12345.67', true, 12345, 67);
	parsenGeheelEnRestTesten('1 234.67', true, 1234, 67);
	parsenGeheelEnRestTesten('12 345.67', true, 12345, 67);
	parsenGeheelEnRestTesten('1,234.67', true, 1234, 67);
	parsenGeheelEnRestTesten('12,345.67', true, 12345, 67);
	// Duizendtallen met een punt en decimalen met een punt wordt geaccepteerd
	// (op het moment van het schrijven van deze tests), maar gaan we niet vastleggen.

	// Een komma voor de centen.
	parsenGeheelEnRestTesten('0,00', true, 0,0);
	parsenGeheelEnRestTesten('0,67', true, 0, 67);
	parsenGeheelEnRestTesten('1,67', true, 1, 67);
	parsenGeheelEnRestTesten('12,67', true, 12, 67);
	parsenGeheelEnRestTesten('123,67', true, 123, 67);
	parsenGeheelEnRestTesten('1234,67', true, 1234, 67);
	parsenGeheelEnRestTesten('12345,67', true, 12345, 67);
	parsenGeheelEnRestTesten('1 234,67', true, 1234, 67);
	parsenGeheelEnRestTesten('12 345,67', true, 12345, 67);
	parsenGeheelEnRestTesten('1.234,67', true, 1234, 67);
	parsenGeheelEnRestTesten('12.345,67', true, 12345, 67);

	// Positieve en negatieve hoeveelheden met een + en - respectievelijk.
	parsenGeheelEnRestTesten('+0,00', true, 0,0);
	parsenGeheelEnRestTesten('+0,67', true, 0,67);
	parsenGeheelEnRestTesten('+1', true, 1,0);
	parsenGeheelEnRestTesten('+12,67', true, 12, 67);
	parsenGeheelEnRestTesten('+1234', true, 1234, 0);
	parsenGeheelEnRestTesten('+12345,67', true, 12345, 67);
	parsenGeheelEnRestTesten('+1 234', true, 1234, 0);
	parsenGeheelEnRestTesten('+12 345,67', true, 12345, 67);
	parsenGeheelEnRestTesten('+1.234', true, 1234, 0);
	parsenGeheelEnRestTesten('+12.345,67', true, 12345, 67);
	parsenGeheelEnRestTesten('-0,00', true, 0,0);
	parsenGeheelEnRestTesten('-0,67', true, 0,-67);
	parsenGeheelEnRestTesten('-1', true, -1,0);
	parsenGeheelEnRestTesten('-12,67', true, -12, -67);
	parsenGeheelEnRestTesten('-1234', true, -1234, 0);
	parsenGeheelEnRestTesten('-12345,67', true, -12345, -67);
	parsenGeheelEnRestTesten('-1 234', true, -1234, 0);
	parsenGeheelEnRestTesten('-12 345,67', true, -12345, -67);
	parsenGeheelEnRestTesten('-1.234', true, -1234, 0);
	parsenGeheelEnRestTesten('-12.345,67', true, -12345, -67);

	// We willen dat 010 niet octaal wordt opgevat.
	parsenGeheelEnRestTesten('010', true, 10, 0);
	// Je mag niet opeens negatieve centen neerzetten.
	parsenGeheelEnRestTesten('0.-10', false);
	parsenGeheelEnRestTesten('2.-10', false);
	// Overige onzin wordt ook geweigerd.
	parsenGeheelEnRestTesten('a10', false);
	parsenGeheelEnRestTesten('1e8', false);
	parsenGeheelEnRestTesten('0x10', false);
	parsenGeheelEnRestTesten('aap', false);
	parsenGeheelEnRestTesten(' ', false);
	parsenGeheelEnRestTesten('100_000', false);
	parsenGeheelEnRestTesten('1+1', false);
});

$this->def('bedragen parsen uit maybestrings', function() {
	// Bij null geven we de default.
	parsenFromMaybeTesten(null, 0, true, 0);
	parsenFromMaybeTesten(null, 37, true, 37);

	// Lege string geeft nog steeds(!) een foutmelding.
	parsenFromMaybeTesten('', 0, false);
	parsenFromMaybeTesten(' ', 0, false);

	// Falsy strings geven alsnog niet de default.
	parsenFromMaybeTesten('0', 37, true, 0);
	parsenFromMaybeTesten('0.00', 37, true, 0);

	// Gehele getallen, met afscheiders voor duizendtallen.
	parsenFromMaybeTesten('1234', 0, true, 123400);
	parsenFromMaybeTesten('12 345', 0, true, 1234500);
	parsenFromMaybeTesten('1.234', 0, true, 123400);
	parsenFromMaybeTesten('12,345', 0, true, 1234500);

	// Bedragen met centen.
	parsenFromMaybeTesten('0.00', 0, true, 0);
	parsenFromMaybeTesten('0.67', 0, true, 67);
	parsenFromMaybeTesten('1,67', 0, true, 167);
	parsenFromMaybeTesten('1 234.67', 0, true, 123467);
	parsenFromMaybeTesten('12 345,67', 0, true, 1234567);
	parsenFromMaybeTesten('1.234,67', 0, true, 123467);
	parsenFromMaybeTesten('12,345.67', 0, true, 1234567);
});

$this->def('roundtrip van toString -> parse', function() {
	// Gehele bedragen...
	roundtripTesten(0);
	roundtripTesten(100);
	roundtripTesten(1200);
	roundtripTesten(12300);
	roundtripTesten(123400);
	roundtripTesten(1234500);

	// ... en bedragen met centen.
	roundtripTesten(7);
	roundtripTesten(67);
	roundtripTesten(167);
	roundtripTesten(1267);
	roundtripTesten(12367);
	roundtripTesten(123467);
	roundtripTesten(1234567);
});

$this->def('optellen van Bedragen', function() {
	// Een plus twee is drie.
	$een = new Bedrag(100);
	$twee = new Bedrag(200);
	assert($een->plus($twee)->getCenten() == 300);
	// De optelling past de objecten niet aan.
	assert($een->getCenten() == 100);

	// We hebben geen vage floatafronding.
	// (Dit gaat dus mis als je (naïeve) floats van euro's gebruikt in je bedragen.)
	$een = new Bedrag(10);
	$twee = new Bedrag(20);
	assert($een->plus($twee)->alsFloat() == 0.3); // En niet 0.30000001 ofzo!
});

$this->def('negatief nemen van Bedragen', function() {
	// Min nul is plus nul.
	$nul = new Bedrag(0);
	assert($nul->negatief()->getCenten() == 0);

	// Min een euro bestaat uit min honderd centen.
	$een = new Bedrag(100);
	assert($een->negatief()->getCenten() == -100);
	// De operatie past bedragen niet aan.
	assert($een->getCenten() == 100);

	// Min min is plus. (Dit is supervermetel!!! :P)
	$mineen = new Bedrag(-123);
	assert($mineen->negatief()->getCenten() == 123);
});
