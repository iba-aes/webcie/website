<?php

namespace WhosWho4\TestHuis\Support;

use LogicException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Mock-object voor Symfony-sessieobjecten, waar je je eigen sessievariabelen in kan stoppen.
 *
 * We kunnen niet zomaar een lege Session teruggeven, want die probeert headers te zetten enzo.
 *
 * {@inheritDoc}
 */
class CustomSession implements SessionInterface
{
	protected $waarden = [];

	protected $flashBag = null;

	/**
	 * Maak een sessie aan (met eventueel gegeven waarden).
	 *
	 * @param array $waarden
	 * De associatieve array die Session::get opzoekt.
	 */
	public function __construct(array $waarden = [])
	{
		$this->waarden = $waarden;
		$this->flashBag = new FlashBag();
	}

	/**
	 * Geef de waarde die hoort bij de key, of $default als die niet bestaat.
	 *
	 * {@inheritdoc}
	 */
	public function get($name, $default = null)
	{
		if (array_key_exists($name, $this->waarden))
		{
			return $this->waarden[$name];
		}
		else
		{
			return $default;
		}
	}

	/**
	 * Stel de waarde in die hoort bij de key.
	 *
	 * {@inheritdoc}
	 */
	public function set($key, $value)
	{
		$this->waarden[$key] = $value;
	}

	/**
	 * Geef de FlashBag die in deze sessie zit.
	 *
	 * Hierin komen de meldingen.
	 *
	 * @return FlashBag
	 */
	public function getFlashBag()
	{
		return $this->flashBag;
	}

	/**
	 * {@inheritdoc}
	 */
	public function start()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId()
	{
		// Wordt gebruikt om unieke id's voor clients te maken,
		// dus als we gewoon een vaste waarde geven, is het prima.
		return '37';
	}

	/**
	 * {@inheritdoc}
	 */
	public function setId($id)
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function setName($name)
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function invalidate($lifetime = null)
	{
		$this->waarden = [];
		$this->flashBag = new FlashBag();
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function migrate($destroy = false, $lifetime = null)
	{
		// We hoeven niets te doen :D
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function save()
	{
		// We hoeven niets te doen :D
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function has($name)
	{
		return array_key_exists($name, $this->waarden);
	}

	/**
	 * {@inheritdoc}
	 */
	public function all()
	{
		return $this->waarden;
	}

	/**
	 * {@inheritdoc}
	 */
	public function replace(array $attributes)
	{
		// De +-operator neemt de linkerkant over en vult aan met de rechterkant.
		$this->waarden = $attributes + $this->waarden;
	}

	/**
	 * {@inheritdoc}
	 */
	public function remove($name)
	{
		$waarde = $this->waarden[$name];
		unset($this->waarden[$name]);
		return $waarde;
	}

	/**
	 * {@inheritdoc}
	 */
	public function clear()
	{
		$this->waarden = [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function isStarted()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function registerBag(SessionBagInterface $bag)
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBag($name)
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMetadataBag()
	{
		throw new LogicException('Deze operatie is een stub, doei!');
	}
}
