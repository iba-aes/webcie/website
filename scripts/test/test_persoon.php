<?php

use Space\Auth\Auth;
use Space\Auth\GodAuth;
use WhosWho4\TestHuis\Fixture\GodAuthFixture;
use WhosWho4\TestHuis\Fixture\LidAuthFixture;
use WhosWho4\TestHuis\Fixture\PersoonFixture;

$this->nieuw('Het wijzigen van een geboortedatum kan alleen als bestuur, behalve als er nog geen geboortedatum is')
	->fixture(new PersoonFixture())
	->fixture(new LidAuthFixture())
	->fixture(new GodAuthFixture())
	->doet(function(Persoon $figuur, Auth $newAuth, Auth $godAuth) {
		global $auth;
		$auth = $newAuth;
		assert($figuur->valid());
		$datum = new DateTimeLocale();
		$datum->modify('-14 years');
		assert($datum != $figuur->getDatumGeboorte());
		//Eerste keer de geboorte datum instellen. Dit zou prima moeten zijn
		$figuur->setDatumGeboorte($datum);
		assert($figuur->valid());
		//Stel nu een andere geboortedatum in. Dit zou alleen het bestuur mogen doen
		$datum2 = new DateTimeLocale();
		$datum2->modify('-15 years');
		$figuur->setDatumGeboorte($datum2);
		assert(!($figuur->valid()), "Deze persoon mag niet de geboortedatum wijzigen.");
		$auth = $godAuth;
	})->registreer();
