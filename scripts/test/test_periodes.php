<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use DateTimeLocale;
use Periode;
use WhosWho4\TestHuis\Fixture\PeriodeFixture;

$this->nieuw('de huidige periodes hebben het huidige collegejaar')
	->doet(function(){
		$periodeColjaar = Periode::vanColjaar()->getCollegejaar();
		$periodeDatum = Periode::vanDatum()->getCollegejaar();
		$coljaar = coljaar();

		assert($periodeColjaar == $coljaar, "de periode in dit collegejaar heeft het juiste collegejaar");
		assert($periodeDatum == $coljaar, "de huidige periode heeft het juiste collegejaar");
	})->registreer();

$this->nieuw('periodes in augustus hebben het vorige collegejaar')
	->doet(function(){
		$periode = Periode::vanDatum(new DateTimeLocale('2018-08-15'));
		$collegejaar = $periode->getCollegejaar();
		assert($collegejaar == 2017, "het collegejaar van de periode in augustus is het vorige jaar");
	})->registreer();
