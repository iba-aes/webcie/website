<?php

use WhosWho4\TestHuis\Fixture\PersoonFixture;

$this->nieuw('iemands geslacht kan null worden')
	->fixture(new PersoonFixture())
	->doet(function(Persoon $figuur) {
		assert($figuur->valid());
		$figuur->setGeslacht('');
		assert($figuur->valid());
		assert($figuur->getGeslacht() === null);
	})->registreer();

$this->nieuw('een persoon met geslacht null kan naar de db en terug')
	->fixture(new PersoonFixture())
	->doet(function(Persoon $figuur) {
		// Stop in de databaas.
		$figuur->setGeslacht('');
		$figuur->opslaan();
		// Zorg ervoor dat het object echt uit de db gelezen wordt.
		Persoon::invalidateCache();
		// Bekijk het object.
		$figuurOpnieuw = Persoon::geef($figuur->geefID());
		assert($figuurOpnieuw->getGeslacht() === null);
	})->registreer();
