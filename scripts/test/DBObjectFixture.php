<?php

namespace WhosWho4\TestHuis;


interface DBObjectFixture extends Fixture
{

	/**
	 * {@inheritdoc}
	 * @return \DBObject Het DBObject wat gemaakt is
	 */
	public function erin(): \DBObject;

}