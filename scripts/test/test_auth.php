<?php

use Space\Auth\Auth;
use Space\Auth\Constants;
use Symfony\Component\HttpFoundation\Session\Session;
use WhosWho4\TestHuis\Fixture\AuthConstantsFixture;
use WhosWho4\TestHuis\Fixture\SessionFixture;

$this->nieuw('getLidnr geeft correct lidnr')
	->fixture(new SessionFixture(1234))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->getLidnr() === 1234, "lidnr moet hetzelfde zijn als in de sessie");
	})->registreer();

$this->nieuw('getRealLidnr geeft correct lidnr')
	->fixture(new SessionFixture(1234, 4321))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->getRealLidnr() === 4321, "realuserid moet hetzelfde zijn als in de sessie");
	})->registreer();

$this->nieuw('getRealLidnr geeft mylidnr zonder realuserid')
	->fixture(new SessionFixture(1234))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->getRealLidnr() === 1234, "getRealLidnr moet hetzelfde zijn als mylidnr in de sessie");
	})->registreer();

$this->nieuw('terugNaarJeEigenHuid reset de juiste session variabelen')
	->fixture(new SessionFixture(1234, 4321))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		$auth->terugNaarJeEigenHuid();
		assert($session->get('mylidnr') === 4321, "mylidnr moet dezelfde waarde hebben als de oude realuserid");
		assert(is_null($session->get('realuserid')), "session moet geen realuserid meer hebben");
	})->registreer();

$this->nieuw('isDibsTablet geeft true met het juiste contactId')
	->fixture(new AuthConstantsFixture())
	->doet(function (Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), null, $constants, 'god', []);
		assert($auth->isDiBSTablet(4) === True);
	})->registreer();

$this->nieuw('isDibsTablet geeft true als mylidnr van session dibsTabletId is')
	->fixture(new SessionFixture(4))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->isDiBSTablet() === True);
	})->registreer();

$this->nieuw('isBestuur geeft true met het juiste contactId')
	->fixture(new AuthConstantsFixture())
	->doet(function (Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), null, $constants, 'god', []);
		assert($auth->isBestuur(111) === True);
	})->registreer();

$this->nieuw('isBestuur geeft true als mylidnr van session een bestuurslid is')
	->fixture(new SessionFixture(111))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->isBestuur() ===  True);
	})->registreer();

$this->nieuw('isBoekenCommissaris geeft true met het juiste contactId')
	->fixture(new AuthConstantsFixture())
	->doet(function (Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), null, $constants, 'god', []);
		assert($auth->isBoekenCommissaris(112) === True);
	})->registreer();

$this->nieuw('isBoekenCommissaris geeft true als mylidnr van session een boekcom is')
	->fixture(new SessionFixture(112))
	->fixture(new AuthConstantsFixture())
	->doet(function (Session $session, Constants $constants) {
		$auth = new Auth(new \Symfony\Component\HttpFoundation\Request(), $session, $constants, 'god', []);
		assert($auth->isBoekenCommissaris() === True);
	})->registreer();
