<?php

use WhosWho4\TestHuis\Fixture\ZeurmodusFixture;

$this->nieuw('zonder aanzetten staat de zeurmodus uit')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		assert(!getZeurmodus());
	})->registreer();

$this->nieuw('zeurmodus kun je aanzetten')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		setZeurmodus();
		assert(getZeurmodus());
	})->registreer();

$this->nieuw('zonder argumenten krijg je default zeurmodus')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		setZeurmodus();
		assert(getZeurmodus('default'));
	})->registreer();

$this->nieuw('zonder argumenten aanzetten zet geen andere zeurmodus aan')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		setZeurmodus();
		assert(!getZeurmodus('overig'));
	})->registreer();

$this->nieuw('zeurmodus met argumenten kun je aanzetten')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		setZeurmodus('overig');
		assert(getZeurmodus('overig'));
	})->registreer();

$this->nieuw('zeurmodus kun je ook weer uitzetten')
	->fixture(new ZeurmodusFixture())
	->doet(function() {
		setZeurmodus('overig');
		assert(getZeurmodus('overig'));
		setZeurmodus('overig', false);
		assert(!getZeurmodus('overig'));
	})->registreer();
