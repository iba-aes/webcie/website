<?php

use WhosWho4\TestHuis\Fixture\LidFixture;

$this->nieuw('een lid kan login aanvragen met lidnummer')
	->fixture(new LidFixture())
	->doet(function (Lid $lid) {
		require_once 'WhosWho4/Controllers/Wachtwoord.php';
		$lid->opslaan(); // geef het lid een lidnummer

		$id = $lid->geefID();
		$opties = Wachtwoord_Controller::bepaalLid($id);
		assert($opties->aantal() == 1);
		assert($opties->first()->geefID() == $id);
	})->registreer();
