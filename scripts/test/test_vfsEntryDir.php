<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

$this->nieuw("vfsEntryVariable met speciaal veld geeft geen error met contenthook")
	->doet(function () {
		// De motivatie voor deze test:
		// als een URL door Benamite gehaald wordt en het laatste stukje matcht niet,
		// en de laatste entry is een vfsEntryDir,
		// dan wordt gekeken of het een contenthook heeft.
		$entry = hrefToEntry('/Leden/111');
		assert((bool)$entry->getSpecial(), "vfsEntryVariable heeft een speciaal veld");
		assert(!$entry->hasHook('content'), "vfsEntryVar heeft geen contenthook");
	})
	->registreer();
