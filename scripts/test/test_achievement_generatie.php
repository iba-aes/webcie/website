<?php

use WhosWho4\TestHuis;
use WhosWho4\TestHuis\Fixture\IntroGroepFixture;
use WhosWho4\TestHuis\Fixture\LidFixture;

// Check dat de achievement geen error geeft als we bij iedereen kijken.
$this->nieuw('generatie-achievement voor iedereen')
	->uitgesteld()
	->doet(function () {
		// Er zijn erg veel Personen, dus we werken met limits.
		$begin = 0;
		$limiet = 500;
		do {
			$personen = PersoonQuery::table()
				->limit($limiet)->offset($begin)
				->verzamel();
			$begin += $limiet;
			foreach ($personen as $persoon) {
				// Dit mag max een seconde duren,
				// anders duren pagina's erg lang om te laden.
				$aantal = PersoonBadge::badgeBSGPCount("generatie", $persoon);
				// Ik verwacht dat de meeste bugs van bovenstaande regel komen,
				// maar kan geen kwaad om dit ook eventjes te checken.
				assert("$aantal >= 0 /* $persoon heeft geen negatieve generaties voortgebracht */");
			}
			// Er worden nogal wat dingen in de cache gestopt,
			// en we willen geen overflows ofzo.
			DBObject::invalidateCache();
			// Geef een beetje voortgang aan.
			TestHuis\progressBarVoortgang();
		} while ($personen->aantal() > 0);
	}); //->registreer();

// Je kreeg onterecht geen generaties-achievement als je (onder-)kleinkinderen hebt in je tweede leg.
$this->nieuw('generatie-achievement controleert alle mentorschappen')
	->fixture(new LidFixture())
	->fixture(new LidFixture())
	->fixture(new LidFixture())
	->fixture(new LidFixture())
	->doet(function(Lid $slachtoffer, Lid $kind1, Lid $kind2, Lid $kleinkind) {
		IntroGroepFixture::maakMentorVan($slachtoffer, $kind1);
		IntroGroepFixture::maakMentorVan($slachtoffer, $kind2);
		IntroGroepFixture::maakMentorVan($kind2, $kleinkind);

		// Worden de juiste groepen meegenomen?
		$groepen = $kind1->mentorVan();
		assert($groepen->aantal() == 0, "$kind1 moet 0 groepen hebben");
		$groepen = $kind2->mentorVan();
		assert($groepen->aantal() == 1, "$kind2 moet 1 groep hebben");
		$groepen = $slachtoffer->mentorVan();
		assert($groepen->aantal() == 2, "$slachtoffer moet 2 groepen hebben");

		// Kijk of de kleinkindjesgroepen gevonden worden.
		// We weten niet in welke volgorde deze groepen bezocht worden, dus we doen een of-statement op de index.
		$kindjeGroepen = [];
		foreach ($groepen as $groep)
		{
			/** @var IntroGroep $groep */
			$kindjeGroepen[] = $groep->getMentorGroepenVanMentoren();
		}
		assert($kindjeGroepen[0]->aantal() == 1 || $kindjeGroepen[1]->aantal() == 1, "groep van kind2 staat in de mentorgroepen van mentoren");

		// Nou, dan moet het antwoord wel kloppen, toch?
		$aantal = PersoonBadge::badgeBSGPCount("generatie", $slachtoffer);
		assert($aantal == 2, "slachtoffer moet 2 generaties hebben");
	})->registreer();

// Wat gebeurt er als een lid zijn eigen kindje is?
$this->nieuw('Geen oneindige loop bij mentorkindjescykel')
	->fixture(new LidFixture())
	->fixture(new LidFixture())
	->fixture(new IntroGroepFixture())
	->doet(function (Lid $slachtoffer, Lid $kindje, IntroGroep $groepje) {
		// Maak ze hun eigen kindje.
		IntroGroepFixture::maakMentorVan($slachtoffer, $slachtoffer);
		IntroGroepFixture::maakMentorVan($slachtoffer, $kindje);

		// Dit mag dus geen oneindige loop worden!
		$aantal = PersoonBadge::badgeBSGPCount("generatie", $slachtoffer);
		assert($aantal == 1, "slachtoffer moet precies 1 generatie hebben");
	})->registreer();
