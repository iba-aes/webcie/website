<?php

namespace WhosWho4\TestHuis;

define('BG_BLACK', '0m');
define('BG_DGRAY', '0;1m');
define('BG_DRED', '1m');
define('BG_RED', '1;1m');
define('BG_DGREEN', '2m');
define('BG_GREEN', '2;1m');
define('BG_DYELLOW', '3m');
define('BG_YELLOW', '3;1m');
define('BG_DBLUE', '4m');
define('BG_BLUE', '4;1m');
define('BG_DMAGENTA', '5m');
define('BG_MAGENTA', '5;1m');
define('BG_DCYAN', '6m');
define('BG_CYAN', '6;1m');
define('BG_GRAY', '7m');
define('BG_WHITE', '7;1m');

/**
 * \brief Print een symbooltje en een kleur om voortgang aan te geven.
 * \param symbooltje Een symbooltje van 1 karakter breed om te tonen.
 * \param kleur De code van de kleur die de achtergrond wordt van het symbooltje.
 */
function progressBar($symbooltje, $kleur) {
	// Toon alleen progressie als we interactief zijn.
	// (Anders kan het wel eens de output slopen...)
	if (!TestRunner::isInteractieveCLI()) return;

	echo "\x1B[4{$kleur}"; // Zet achtergrondkleur op de gegeven kleur.
	echo $symbooltje; // Print symbool.
	echo "\x1B[0m"; // Reset kleur.
}

/**
 * \brief Laat in de progress bar zien dat de huidige test helemaal is geslaagd.
 */
function progressBarGeslaagd() {
	return progressBar("ok!", BG_GREEN);
}
/**
 * \brief Laat in de progress bar zien dat de huidige test foute returnwaarden heeft geproduceerd.
 */
function progressBarGefaald() {
	return progressBar("no.", BG_RED);
}
/**
 * \brief Laat in de progress bar zien dat de huidige test een error heeft veroorzaakt.
 */
function progressBarError() {
	return progressBar("err", BG_RED);
}
/**
 * \brief Laat in de progress bar zien dat de huidige test later wel wordt uitgevoerd.
 */
function progressBarUitgesteld() {
	return progressBar("zzz", BG_BLUE);
}
/**
 * \brief Laat in de progress bar zien dat de huidige test nog voortgang maakt.
 */
function progressBarVoortgang() {
	return progressBar("...", BG_YELLOW);
}
/**
 * \brief Laat in de progress bar zien dat de huidige test niet uitgevoerd wordt.
 */
function progressBarGeskipt() {
	return progressBar(" x ", BG_DRED);
}
