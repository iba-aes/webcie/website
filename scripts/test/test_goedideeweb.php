<?php

use Space\Auth\Auth;
use WhosWho4\TestHuis\Fixture\GodAuthFixture;
use WhosWho4\TestHuis\Fixture\LidFixture;
use WhosWho4\TestHuis\Support\vfsEntrySupport;

$this->nieuw('de pagina om een goed idee te melden')
	->fixture(new GodAuthFixture())
	->doet(function(Auth $auth) {
		$entry = new vfsEntryInclude([
			'type' => ENTRYINCLUDE,
			'id' => ROOT_ID,
			'parent' => ROOT_ID,
			'name' => 'testpagina',
			'displayName' => 'Test de pagina!',
			'auth' => null,
			'visible' => true,
			'special' => '/WhosWho4/Controllers/GoedIdee.php:GoedIdee_Controller:nieuwGoedIdee',
			'rank' => 0,
			'wanneer' => null,
			'wie' => null,
		]);
		vfsEntrySupport::testEntryDisplay($auth, $entry);
	})->registreer();

$this->nieuw('GoedIdeeBericht zonder melder is invalid')
	->fixture(new LidFixture())
	->doet(function(Lid $melder) {
		$melder->opslaan();
		$goedIdee = new GoedIdee(new DateTimeLocale(), $melder, 'aap', 'noot', true);
		assert($goedIdee->valid(), "Het beginidee moet ook valid zijn!");
		$goedIdee->opslaan();
		$bericht = new GoedIdeeBericht($goedIdee, new DateTimeLocale());
		assert($bericht->getMelder() === null, "Een nieuw GoedIdeeBericht heeft nog geen melder.");
		assert(!$bericht->valid(), "Zonder setMelder is een nieuw GoedIdeeBericht invalid.");

		$bericht = GoedIdeeBericht::eersteBericht($goedIdee, new DateTimeLocale());
		assert($bericht->valid(), "Het eerste bericht moet nu gewoon weer valid zijn.");
		$bericht->opslaan();
	})->registreer();

$this->nieuw('GoedIdee zonder titel of omschrijving is invalid')
	->fixture(new LidFixture())
	->doet(function(Lid $melder) {
		$melder->opslaan();
		$goedIdee = new GoedIdee(new DateTimeLocale(), $melder, '', '', true);
		assert(!$goedIdee->valid(), "Zonder titel en zonder omschrijving moet idee invalid zijn");

		$goedIdee = new GoedIdee(new DateTimeLocale(), $melder, 'met titel', '', true);
		assert(!$goedIdee->valid(), "Zonder titel moet idee invalid zijn");

		$goedIdee = new GoedIdee(new DateTimeLocale(), $melder, '', 'met omschrijving', true);
		assert(!$goedIdee->valid(), "Zonder omschrijving moet idee invalid zijn");
	})->registreer();

$this->nieuw('als GoedIdee valid is, is ook GoedIdeeBericht::eersteBericht daarvan valid')
	->fixture(new LidFixture())
	->doet(function(Lid $melder) {
		$melder->opslaan();
		$goedIdee = new GoedIdee(new DateTimeLocale(), $melder, 'Jemoeder', 'Jebroer', true);
		assert($goedIdee->valid(), "Het beginidee moet ook valid zijn!");
		$goedIdee->opslaan();

		$bericht = GoedIdeeBericht::eersteBericht($goedIdee, new DateTimeLocale());
		assert($bericht->valid(), "Dan moet het eerste bericht ook valid zijn");
		$bericht->opslaan();

		$berichtverzameling = $goedIdee->getBerichten();
		assert(!($berichtverzameling->last() === null), "In getBerichten moet het eerste bericht zitten.");
	})->registreer();

?>
