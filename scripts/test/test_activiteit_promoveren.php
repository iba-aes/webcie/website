<?php

use WhosWho4\TestHuis\Fixture\ActHerhalingFixture;

$this->nieuw('activiteitherhaling promoveren')
	->fixture(new ActHerhalingFixture())
	->doet(function($acts) {
		list($act, $actHerhaling) = $acts;

		$actPromotie = new ActiviteitInformatie();
		$cieActs = new CommissieActiviteitVerzameling();
		$vragen = new ActiviteitVraagVerzameling();

		$actHerhaling->promoveer($actPromotie, $cieActs, $vragen);
		if (!$actPromotie->valid())
		{
			throw new Exception("De promotie is niet geldig: " . print_r($actPromotie->getErrors(), true));
		}
	})->registreer();

$this->nieuw('activiteitherhaling met reserveringen promoveren')
	->fixture(new ActHerhalingFixture())
	->doet(function($acts) {
		list($act, $actHerhaling) = $acts;

		// Voeg een register waarde toe als deze niet bestaat
		if (Register::getValue("maxComputers") !== null)
		{
			Register::updateRegister("maxComputers", 1000);
		}
		else
		{
			$registerEntry = new Register();
			$registerEntry->setLabel('maxComputers');
			$registerEntry->setWaarde(1000);
			$registerEntry->setBeschrijving('Het max aantal computers');
			$registerEntry->opslaan();
		}
		$max = $act->getMaxAantalComputers($act->getMomentBegin(), $act->getMomentEind());
		if ($max < 750) {
			throw new Exception("Kan niet genoeg computers reserveren voor test.");
		}
		$act->setAantalComputers(750);
		$act->opslaan();
		$actHerhaling->opslaan();

		// Check of de max nog werkt
		$max = $actHerhaling->getMaxAantalComputers($actHerhaling->getMomentBegin(), $actHerhaling->getMomentEind());
		if ($max < 750) {
			throw new Exception("Herhaling kan niet genoeg computers reserveren voor test.");
		}

		$actPromotie = new ActiviteitInformatie();
		$cieActs = new CommissieActiviteitVerzameling();
		$vragen = new ActiviteitVraagVerzameling();

		$actHerhaling->promoveer($actPromotie, $cieActs, $vragen);
		if (!$actPromotie->valid()) {
			throw new Exception("De promotie is niet geldig: " . print_r($actPromotie->getErrors(), true));
		}
	})->registreer();
