<?php

use Space\Auth\Auth;
use WhosWho4\TestHuis\Fixture\GodAuthFixture;
use WhosWho4\TestHuis\Support\vfsEntrySupport;
use WhosWho4\TestHuis\TestRunner;
/** @var TestRunner $this */

$this->nieuw('voorpagina met ingelogd-auth werkt')
	->fixture(new GodAuthFixture())
	->doet(function (Auth $auth)
	{
		$entry = vfsEntrySupport::entryVoorFunctie('/WhosWho4/Controllers/Vereniging.php:Vereniging_Controller:homepagina');
		vfsEntrySupport::testEntryDisplay($auth, $entry);
	})->registreer();
