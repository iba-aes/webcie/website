<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use Symfony\Component\HttpFoundation\Session\Session;

use Artikelen_Controller;
use WhosWho4\TestHuis\Fixture\SessionFixture;
use WhosWho4\TestHuis\Fixture\StudieFixture;
use WhosWho4\TestHuis\Support\vfsEntrySupport;

require_once(FS_ROOT . '/WhosWho4/Boekweb2/Controllers/Artikelen.php');

$this->nieuw("BoekenCSV geeft melding (maar geen error) bij ontbrekend vak")
	->fixture(new SessionFixture())
	->doet(function(Session $mockSession) {
		// De gebruiker krijgt meldingen via Page:addMelding, daarvoor hebben we een sessie nodig.
		global $session;
		$session = $mockSession;

		$regels = [
			"Vakcode,ISBN,Titel,Auteur,Editie,Uitgever,Verplicht?",
			"WISB3MOCHTJEWILLEN,9780141396118,Het Broodboek,De Broodkerstman,1,Spookdruk BV,ja",
		];
		list($errors, $opTeSlaan) = Artikelen_Controller::verwerkBoekenCSV(implode("\n", $regels));
		assert(count($errors), "Geen errors bij onbekend vak!");
	})->registreer();
