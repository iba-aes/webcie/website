<?php

namespace WhosWho4\TestHuis\phpunit;

use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_TestResult;
use WhosWho4\TestHuis\TestRunner;

require_once "PHPUnitWrapperCase.php";

class PHPUnitWrapperTest extends TestCase
{
	/**
	 * @var TestRunner
	 * Bij het constructen wordt deze geconstrueerd.
	 */
	protected $runner;

	public function __construct()
	{
		// Start de TestRunner op.
		$vorigeDir = getcwd();
		chdir('scripts');
		$this->runner = new TestRunner();
		$this->runner->fataleErrorsAfhandelen = false;
		chdir($vorigeDir);
	}

	/**
	 * Count elements of an object
	 * @link http://php.net/manual/en/countable.count.php
	 * @return int The custom count as an integer.
	 *
	 * The return value is cast to an integer.
	 * @since 5.1.0
	 */
	public function count(): int
	{
		return count($this->runner->getGevondenTests());
	}

	/**
	 * Runs a test and collects its result in a PHPUnit_Framework_TestResult instance.
	 * @param PHPUnit_Framework_TestResult|null $result
	 * Een PHPUnit_Framework_TestResult-object om bij te werken. Default: maak een nieuwe.
	 * @return PHPUnit_Framework_TestResult
	 */
	public function run(\PHPUnit\Framework\TestResult $result = null): \PHPUnit\Framework\TestResult
	{
		// We doen setup en teardown ongeveer als TestSuite,
		// en het runnen ongeveer als TestCase.

		// Defaultwaarde voor $result instellen.
		if ($result === null) {
			$result = new \PHPUnit\Framework\TestResult();
		}
		// Zonder tests hebben we niets te doen.
		if (\count($this) == 0) {
			return $result;
		}

		// Run alle gevonden tests.
		// TODO: willen we iets doen als $result->startTestSuite($this); ?
		foreach ($this->runner->getGevondenTests() as $testNaam => $testObject) {
			if ($result->shouldStop()) {
				break;
			}

			// Start de test volgens PHPUnit.
			// Coverage moeten we handmatig starten.
			$test = new PHPUnitWrapperCase($testNaam);
			$result->startTest($test);
			$cov = $result->getCodeCoverage();
			if ($cov) {
				$cov->start($testNaam);
			}

			// Run de test met behulp van de TestRunner, en stop het in het resultaat.
			$uitkomst = $this->runner->runLosseTest($testNaam);
			if ($uitkomst['status'] != 'geslaagd') {
				$result->addError($test, $uitkomst['throwable'], 0);
			}

			// Coverage moet ook weer handmatig gestopt worden.
			if ($cov) {
				$cov->stop();
			}
			// En dan is de test(case) over.
			$result->endTest($test, 0);
		}

		// TODO: willen we iets doen als $result->endTestSuite($this); ?

		return $result;
	}
}