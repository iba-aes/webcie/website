<?php
namespace WhosWho4\TestHuis;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestSuite;
use PHPUnit_Framework_TestResult;
require_once 'PHPUnitWrapperTest.php';
use function getcwd;

if (!defined('BOEKWEB2')) define('BOEKWEB2', true);
if (!defined('ERROR_HANDLER_SET')) define('ERROR_HANDLER_SET', true);
require_once(__DIR__ . '/../../script-init.php');

/**
 * Klasse die WhosWho4\TestHuis-tests toegankelijk maakt voor PHPUnit.
 *
 * Om deze te gebruiken moet je deze file in je phpunit.xml als testsuite zetten.
 *
 * @package WhosWho4\TestHuis
 */
class PHPUnitWrapper extends TestSuite
{
	/**
	 * Laad tests in vanuit het TestHuis naar PHPUnit.
	 *
	 * Wordt door PHPUnit aangeroepen als we het goed doen.
	 *
	 * @param $name string De naam van deze klasse. Wordt niet gebruikt.
	 *
	 * @return null|Test
	 * Een PHPUnit-test die alle tests van het TestHuis bevat.
	 */
	public static function suite($name)
	{
		// PHPUnit raakt soms in de war als je niet canonieke paden gebruikt,
		// dus we gaan even een sanity check doen.
		// (Je kan dan dingen krijgen als dezelfde file wordt twee keer gerequired,
		// dus krijg je allemaal errors van 'function foo redefined'.)
		$cwd = getcwd();
		if ($cwd != realpath($cwd))
		{
			echo "LET OP: de huidige locatie is niet een canoniek pad!" . PHP_EOL;
			echo "Als PHPUnit problemen geeft, kan dit de verklaring zijn." . PHP_EOL;
		}

		// We besteden het inladen van tests uit aan een andere klasse.
		return new \WhosWho4\TestHuis\phpunit\PHPUnitWrapperTest();
	}
}
