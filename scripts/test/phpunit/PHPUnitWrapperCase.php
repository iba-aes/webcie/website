<?php

namespace WhosWho4\TestHuis\phpunit;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\TestResult;

/**
 * Representeert een test in het TestHuis voor PHPUnit.
 */
class PHPUnitWrapperCase extends TestCase
{
	protected $naam;

	/**
	 * Constructor op basis van de naam van de test.
	 *
	 * @param string $naam
	 * De naam van de test, zoals in TestRunner::getGevondenTests().
	 */
	public function __construct($naam)
	{
		$this->naam = $naam;
	}

	public function count(): int
	{
		return 1;
	}

	/**
	 * Wordt door PHPUnit aangeroepen om de test te runnen.
	 *
	 * Omdat het TestHuis een runner nodig heeft, doen we dat hier niet.
	 *
	 * @param \PHPUnit\Framework\TestResult|null $result
	 * @return \PHPUnit\Framework\TestResult
	 * @throws \Exception
	 */
	public function run(\PHPUnit\Framework\TestResult $result = null): \PHPUnit\Framework\TestResult
	{
		throw new \Exception("Gebruik PHPUnitWrapperTest om tests echt te runnen!");
	}

	/**
	 * Geef de naam van de test.
	 *
	 * Nodig als PHPUnit_Framework_TestResult::addError aangeroepen wordt.
	 *
	 * @param bool $withDataSet
	 * @return string
	 * De naam van de test.
	 */
	public function getName(bool $withDataSet = true): string
	{
		return $this->naam;
	}
}