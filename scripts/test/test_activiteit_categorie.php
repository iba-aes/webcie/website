<?php

use WhosWho4\TestHuis\Fixture\ActCatFixture;
use WhosWho4\TestHuis\Fixture\ActFixture;
use WhosWho4\TestHuis\Support\vfsEntrySupport;

$this->def('net aangemaakte activiteitcategorie verwijderen', function () {
	$cat = new ActiviteitCategorie("testcategorie", "testcategory");
	assert($cat->magVerwijderen(), "een activiteitcategorie die net is aangemaakt, kun je verwijderen");
});

$this->nieuw('activiteitcategorie met activiteiten mag je niet verwijderen')
	->fixture(new ActCatFixture())
	->fixture(new ActFixture(new DateTimeLocale()))
	->doet(function($cat, $act) {
		// voeg de activiteit toe in deze categorie
		$act->setCategorie($cat);
		$act->opslaan();

		// dan zouden we hem niet mogen verwijderen
		$magNietVerwijderen = !$cat->magVerwijderen();
		assert(!$cat->magVerwijderen(), "een activiteitcategorie die activiteiten heeft, mag je niet verwijderen");
	})->registreer();

$this->nieuw('activiteitcategorie met activiteiten in het verleden mag je niet verwijderen')
	->fixture(new ActCatFixture())
	->fixture(new ActFixture(new DateTimeLocale('1-1-1')))
	->doet(function($cat, $act) {
		// voeg de activiteit toe in deze categorie
		$act->setCategorie($cat);
		$act->opslaan();

		// dan zouden we hem niet mogen verwijderen
		$magNietVerwijderen =
		assert(!$cat->magVerwijderen(), "een activiteitcategorie die activiteiten heeft, mag je niet verwijderen");
	})->registreer();
