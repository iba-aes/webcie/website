<?php

namespace WhosWho4\TestHuis;

/**
 * @brief Klasse die het opbouwen en registreren van tests afhandelt.
 *
 * Deze wordt door de TestRunner aangemaakt en roept zelf ook weer TestRunner aan,
 * maar dat is wel ok want we splitsen de verantwoordelijkheden van bouwen
 * en runnen zo iets netter.
 */
class TestFactory
{
	/**
	 * @var string
	 * De (unieke) naam van de test die we aan het maken zijn.
	 */
	private $naam;

	/**
	 * @var TestRunner
	 * De TestRunner waarin de test uiteindelijk moet worden toegevoegd.
	 */
	private $runner;

	/**
	 * @var Test
	 * De test die we willen opbouwen en registreren.
	 */
	private $test;

	/**
	 * @var bool
	 * Indien true, zal de gebouwde test alleen runnen als niet-uitgestelde succesvol waren.
	 * Indien false, wordt de test tegelijk met de andere niet-uitgestelde gerund.
	 */
	private $uitgesteld = false;

	/**
	 * @brief Bouw een test met de gegeven naam.
	 *
	 * @param string naam De (unieke) naam van de test die gebouwd wordt.
	 * @param TestRunner runner De TestRunner-instance waar uiteindelijk de test wordt geregistreerd.
	 */
	public function __construct($naam, $runner)
	{
		$this->naam = $naam;
		$this->runner = $runner;
		$this->test = new Test();
	}

	/**
	 * @brief Stel in wat de test doet.
	 *
	 * @param \Closure functie De functie die de test zelf is.
	 *
	 * @see Test::functie
	 *
	 * @return $this
	 */
	public function doet($functie)
	{
		$this->test->setFunctie($functie);
		return $this;
	}

	/**
	 * @brief Voeg een extra Fixture toe aan de test.
	 *
	 * @param Fixture $fixture
	 * Een Fixture-object waarop `->erin()` en `->eruit()` aangeroepen kan worden.
	 *
	 * @see Test::fixtures
	 *
	 * @return $this
	 */
	public function fixture(Fixture $fixture)
	{
		$this->test->addFixture($fixture);
		return $this;
	}

	/**
	 * @brief Klaar met de test bouwen, zet het in de TestRunner.
	 *
	 * @see TestRunner::registreerTest
	 *
	 * @return Test
	 * De gebouwde test.
	 */
	public function registreer()
	{
		$this->runner->registreerTest($this->naam, $this->test, $this->uitgesteld);
		return $this->test;
	}

	/**
	 * @brief Maak de gebouwde test uitgesteld.
	 * In dat geval zal de test alleen runnen als de niet-uitgestelde tests succesvol waren.
	 *
	 * @see uitgesteld.
	 *
	 * @return $this
	 */
	public function uitgesteld()
	{
		$this->uitgesteld = true;
		return $this;
	}
}