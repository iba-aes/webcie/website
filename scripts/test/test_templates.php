<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use DateTimeLocale;
use Template;
use Twig_LoaderDBObject;
use WhosWho4\TestHuis\Fixture\TemplateFixture;

$this->nieuw('ontbrekende templates bestaan inderdaad niet')
	->doet(function(){
		$loader = new Twig_LoaderDBObject();
		$onzin = "onzin.asdfghjkl";
		assert(!$loader->exists($onzin), "niet-bestaande templates worden ook niet als bestaand gemarkeerd");
	})->registreer();

$this->nieuw('bestaande publisher-pagina\'s worden gevonden')
	->fixture(new TemplateFixture())
	->doet(function(Template $template){
		$template->opslaan();
		$loader = new Twig_LoaderDBObject();
		$template = $template->getLabel();
		assert($loader->exists($template), "wel-bestaande templates worden ook wel als bestaand gemarkeerd");
	})->registreer();

$this->nieuw('veranderde templates zijn niet bedorven')
	->fixture(new TemplateFixture())
	->doet(function(Template $template){
		$template->opslaan();
		$loader = new Twig_LoaderDBObject();
		// TODO: maak dynamisch een template ipv deze hardcoden
		$template = $template->getLabel();
		$gemaakt = new DateTimeLocale(Template::geefVanLabel($template)->getGewijzigdWanneer());
		$gemaakt = $gemaakt->getTimestamp();
		assert($loader->isFresh($template, $gemaakt + 1), "nieuw gecachte templates worden als vers gemarkeerd");
		assert(!$loader->isFresh($template, $gemaakt - 1), "oud gecachte templates worden als bedorven gemarkeerd");
		assert($loader->isFresh($template, $gemaakt), "templates die precies nu zijn bewerkt, worden als vers gemarkeerd");
	})->registreer();

$this->nieuw('templates zonder substitutie geven origineel terug')
	->doet(function(){
		$label = 'test_template_text';
		$verwachteTekst = "Dit is een voorbeeldtekst.";

		if ($tpl = Template::geefVanLabel($label)) $tpl->verwijderen();
		$template = new Template();
		$template->setLabel($label);
		$template->setBeschrijving('template om tekst laden te testen');
		$template->setWaarde($verwachteTekst);
		$template->opslaan();

		$loader = new Twig_LoaderDBObject();
		$tekst = $loader->getSource($label);

		$template->verwijderen();

		assert($tekst == $verwachteTekst, "tekst uit templates trekken werkt");
	})->registreer();

$this->nieuw('templates met een variabele-substitutie')
	->doet(function(){
		$label = 'test_template_substitution';
		$origineleTekst = "Als hier nog curly braces staan, faalt de template: {{ hello_world }}";
		$verwachteTekst = "Als hier nog curly braces staan, faalt de template: Het werkt!";

		if ($tpl = Template::geefVanLabel($label)) $tpl->verwijderen();
		$template = new Template();
		$template->setLabel($label);
		$template->setBeschrijving('template om tekst laden te testen');
		$template->setWaarde($origineleTekst);
		$template->opslaan();

		$loader = new Twig_LoaderDBObject();
		$tekst = Template::render($label, array("hello_world" => "Het werkt!"));
		$template->verwijderen();

		assert($tekst == $verwachteTekst, "tekst vertolken werkt");
	})->registreer();

