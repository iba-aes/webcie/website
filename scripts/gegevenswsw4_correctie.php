#!/usr/bin/php -q
<?php
require('script-init.php');

/*
 * Doen alvorens dit script te draaien:
 * 1. Osirischeck
 * 2. Foto's eerstejaars fixen
 */

$leden = LidVerzameling::alleLeden();

foreach($leden as $lid)
{
	//Vraag een array van een persoon op en voeg hier zelf nog wat aan toe
	$persoonArray = PersoonView::alsArray($lid);
	$persoonArray['contactID'] = $lid->getContactID();
	$persoonArray['email'] = $lid->getEmail();
	$geslacht = $persoonArray['geslacht'] == 'M' ? 'Man' : 'Vrouw';

	$adresArray = ContactAdresView::adresArray($lid);
	if(!$adresArray)
	{
		$adresArray = array(
			'straat1' => 'Onbekend',
			'straat2' => 'Onbekend',
			'huisnummer' => 'Onbekend',
			'postcode' => 'Onbekend',
			'woonplaats' => 'Onbekend',
			'land'=> 'Onbekend'
			);
	} 
	$pf = ProfielFoto::zoekHuidig($lid);
	if($pf)
	{
		$foto = "Ja: https://www.a-eskwadraat.nl".$pf->getMedia()->url();
	}
	else
	{
		$foto = "Geen foto opgegeven";
	}

	//Gebruik een padding van 19 zodat het er mooi uit ziet
	$telnrs = ContactTelnrVerzamelingView::regels($lid->getTelefoonNummers(), 19);

	$studies = LidStudieVerzamelingView::simpel(LidStudieVerzameling::vanLid($lid, true));

	$mail = <<<HEREDOC
[ English summary below ]<br>
<br>
Beste $persoonArray[voornaam],<br>
<br>
A-Eskwadraat houdt gegevens bij van haar leden. Omdat wij graag zien<br>
dat deze gegevens correct zijn, vraag ik aan je om te kijken of de<br>
gegevens die wij van jou hebben nog kloppen. De gegevens die we op<br>
dit moment van je hebben, zijn:<br>
<br>
<table>
<tr><td>Voornaam:</td><td>$persoonArray[voornaam]</td></tr>
<tr><td>Voorletters:</td><td>$persoonArray[voorletters]</td></tr>
<tr><td>Tussenvoegsels:</td><td>$persoonArray[tussenvoegsels]</td></tr>
<tr><td>Achternaam:</td><td>$persoonArray[achternaam]</td></tr>
<tr><td>Geslacht:</td><td>$geslacht</td></tr>
<tr><td>Geboortedatum:</td><td>$persoonArray[datumGeboorte]</td></tr>
<tr></tr>
<tr><td>Foto:</td><td>$foto</td></tr>
<tr></tr>
<tr><td>Straat:</td><td>$adresArray[straat1]</td></tr>
<tr><td>Huisnummer:</td><td>$adresArray[huisnummer]</td></tr>
<tr><td>Postcode:</td><td>$adresArray[postcode]</td></tr>
<tr><td>Woonplaats:</td><td>$adresArray[woonplaats]</td></tr>
<tr><td>Telefoon:</td><td>$telnrs</td></tr>
<tr><td>E-mail:</td><td>$persoonArray[email]</td></tr>
<tr></tr>
<tr><td>Huidige studie(s):</td><td>$studies</td></tr>
</table>
<br>
Om dit en meer (je foto!) in te stellen of te wijzigen moet je langsgaan<br>
op je persoonlijke pagina: http://www.a-eskwadraat.nl/Leden/$persoonArray[contactID]/.<br>
Hiervoor moet je ingelogd zijn op de site. Als je nog geen inloggegevens<br>
hebt, kun je deze op de zojuist genoemde pagina eenvoudig aanvragen, of<br>
een antwoord sturen op dit bericht.<br>
<br>
Studeer je inmiddels niet meer (bij ons)? Dan hoor ik dat graag via een<br>
e-mailtje.<br>
<br>
Bedankt!<br>
<bR>
Met vriendelijke groet,<br>
<br>
HEREDOC
. SECRNAME . <<<HEREDOC
<br>
Secretaris van A-Eskwadraat<br>
<br>
[ English summary:]<br>
Dear $persoonArray[voornaam],<br>
<br>
A-Eskwadraat keeps records of the information of her members. We like<br>
to keep this updated and we ask you to check and edit, if necessary,<br>
the information we currently have of you:<br>
<br>
<table>
<tr><td>First name:</td><td>$persoonArray[voornaam]</td></tr>
<tr><td>Initials:</td><td>$persoonArray[voorletters]</td></tr>
<tr><td>Insertions:</td><td>$persoonArray[tussenvoegsels]</td></tr>
<tr><td>Surname:</td><td>$persoonArray[achternaam]</td></tr>
<tr><td>Gender:</td><td>$geslacht</td></tr>
<tr><td>Date of Birth:</td><td>$persoonArray[datumGeboorte]</td></tr>
<tr></tr>
<tr><td>Picture:</td><td>$foto</td></tr>
<tr></tr>
<tr><td>Street:</td><td>$adresArray[straat1]</td></tr>
<tr><td>Huis number:</td><td>$adresArray[huisnummer]</td></tr>
<tr><td>Zip code:</td><td>$adresArray[postcode]</td></tr>
<tr><td>City:</td><td>$adresArray[woonplaats]</td></tr>
<tr><td>Telephone:</td><td>$telnrs</td></tr>
<tr><td>E-mail:</td><td>$persoonArray[email]</td></tr>
<tr></tr>
<tr><td>Current study:</td><td>$studies</td></tr>
</table>
<br>
If you want to edit your data, or your picture, you can visit<br>
https://www.a-eskwadraat.nl/Leden/$persoonArray[contactID]/. You have<br>
to have a login on the website. If you don't have a login yet, visit<br>
the above page or simply reply to this message.<br>
<br>
If you no longer follow any program (at Utrecht University), please inform us.<br>
<br>
We like to thank you in advance!<br>
<br>
Kind regards,<br>
<br>
HEREDOC
. SECRNAME . <<<HEREDOC
<br>
Secretary A-Eskwadraat<br>
<br>
HEREDOC;


	echo "$persoonArray[contactID]: $persoonArray[email]\n";
	if ( empty( $persoonArray['email'] ) ) {
		echo "Dit lid heeft geen e-mailadres, overgeslagen.\n";
		continue;
	}
	echo $lid->geefID();
	$email = new Email();
	$secr = Commissie::cieByLogin('secr');
	$email->setFrom($secr);
	$email->setTo($lid);
	$email->setSubject('Gegevens A-Eskwadraat');
	$email->setBody($mail);

	if (@$argv[1] == 'for-real') {
		$email->send();
	} else {
		echo $mail;
	}
}
