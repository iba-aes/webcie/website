#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id$

/**
 * Stuur iedereen die openstaande bugs aan zich heeft toegewezen
 * een herinnering.
 */

global $WSW4DB;

$FROM = "Bugsysteem A-Eskwadraat <www@A-Eskwadraat.nl>";
$SUBJ = "Overzicht toegewezen bugs";
$PRE  = "Beste %s,\n\nHieronder volgt een overzicht van bugs die aan jou zijn toegewezen\nin ".
	"het A-Eskwadraat Bugsysteem, maar nog niet zijn opgelost.\n\n" .
	"Bugnr  Status            Omschrijving\n";
$POST = "\n\nDit overzicht wordt maandelijks verstuurd. Wil je het niet meer ontvangen?\n".
	"Los dan alle bugs op of wijs ze toe aan iemand anders.\n\n" .
	"Met vriendelijke groet,\n\nBugsysteem A-Eskwadraat\nhttp://bugs.A-Eskwadraat.nl\n";

$personen = $WSW4DB->q("COLUMN SELECT DISTINCT `BugToewijzing`.`persoon_contactID`"
	. " FROM `BugToewijzing`"
	. " LEFT JOIN `Bug` ON `Bug`.`bugID` = `BugToewijzing`.`bug_bugID`"
	. " WHERE `Bug`.`status` IN (%As)"
	. " AND `BugToewijzing`.`eind_bugBerichtID` IS NULL"
	, Bug::geefStatusSet('OPENS'));

foreach($personen as $persoon) {
	$ids = $WSW4DB->q("COLUMN SELECT `Bug`.`bugID`"
		. " FROM `BugToewijzing`"
		. " LEFT JOIN `Bug` ON `Bug`.`bugID` = `BugToewijzing`.`bug_bugID`"
		. " WHERE `Bug`.`status` IN (%As)"
		. " AND `BugToewijzing`.`persoon_contactID` = %i"
		. " AND `BugToewijzing`.`eind_bugBerichtID` IS NULL"
		, Bug::geefStatusSet('OPENS')
		, $persoon);
	$bugs = BugVerzameling::verzamel($ids);

	$bugslist = '';

	foreach($bugs as $bug) {
		$bugslist .= str_pad(BugView::waardeBugID($bug), 5);
		$bugslist .= "  ";
		$bugslist .= str_pad(BugView::waardeStatus($bug), 17);
		$bugslist .= " ";
		$bugslist .= BugView::waardeTitel($bug);
		$bugslist .= "\n";
	}
	$persoon = Persoon::geef($persoon);
	sendmail($FROM, $persoon->getEmail(), $SUBJ, sprintf($PRE, PersoonView::naam($persoon)) . $bugslist . $POST);
}
