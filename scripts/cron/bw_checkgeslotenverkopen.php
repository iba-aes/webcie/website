#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id$
global $BWDB;

$openverkopen = $BWDB->q('TABLE SELECT *
	FROM verkoopgeld
	WHERE sluitstamp IS NULL AND verkoopdag < CURDATE()
	ORDER BY verkoopdag');

if (sizeof($openverkopen) == 0) return;

$result = "De volgende verkopen zijn nog niet (netjes) afgesloten:\n";
foreach($openverkopen as $verkoop) {
	$result .= '* '.strftime('%e %B %Y',
		strtotime($verkoop['verkoopdag']))."\n";
	;
}

sendmail('www@a-eskwadraat.nl', 'boekcom@A-Eskwadraat.nl',
	'Verkopen in het verleden niet correct afgesloten',
	$result."\nGroeten,\nC.R.O.N.");
