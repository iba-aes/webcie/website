#!/usr/bin/php -q
<?php
require('script-init.php');
// $Id$
// Script om de WebCie te mailen over zaken die moeten gebeuren met
// BookWeb.

$maildata = array();

// Zijn de vakantieperiodes goed ingevoerd?
// Implementatie: zoeken naar vakantiedata die meer dan 6 maanden in de
// toekomst liggen.
$laststamp = null;
foreach ($VAKANTIES as $begindatum => $einddatum){
	$stamp = strtotime($begindatum);
	if ($stamp > $laststamp || $laststamp === null) $laststamp = $stamp;
}

if ($laststamp === null || $laststamp < strtotime("+6 months")){
	// Geen stamp gevonden die verder dan zes maanden in de toekomst ligt,
	// reminder naar WebCie sturen
	$maildata[] = 'Het is tijd om nieuwe vakantiedata in specialconstants.php ' .
		'te configureren. De laatst geconfigureerde vakantieperiode begint op ' .
		date('d-m-Y', $laststamp) . ', dat is binnen zes maanden van nu.';
}

// Zijn er afdoende bestel- en levermomenten bekend?
// Implementatie: zoeken naar bestel- en levermomenten die meer dan 6 maanden
// in de toekomst liggen.
/*$laststamp = null;
foreach ($BESTELDATA as $besteldag => $leverdag) {
	$stamp = strtotime($besteldag);
	if ($stamp > $laststamp || $laststamp === null) $laststamp = $stamp;
}

if ($laststamp === null || $laststamp < strtotime("+2 months")){
	// Geen stamp gevonden die verder dan zes maanden in de toekomst ligt,
	// reminder naar WebCie sturen
	$maildata[] = 'Het is tijd om nieuwe bestelmomenten in specialconstants.php ' .
		'te configureren. Het laatst geconfigureerde bestelmoment is ' .
		date('d-m-Y', $laststamp) . ', dat is binnen twee maanden van nu.';
}
*/

if (sizeOf($maildata) > 0) {
	bwmail(null, BWMAIL_WEBCIE_REMIND, $maildata);
}

