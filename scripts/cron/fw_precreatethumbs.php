#!/usr/bin/php -q
<?php
require("script-init.php");
global $WSW4DB, $filesystem;
global $logger;

$fouteMedia = [];
$mediaids = MediaVerzameling::geefNietVideos();

$chunks = array_chunk($mediaids, 200);
foreach($chunks as $mediaids) {
	foreach($mediaids as $mediaid) {
		$sizes = [
			'medium' => Media::geefAbsLoc("medium", $mediaid),
			'groot' => Media::geefAbsLoc("groot", $mediaid),
			'thumbnail' => Media::geefAbsLoc("thumbnail", $mediaid),
		];
		foreach ($sizes as $size => $path)
		{
			if ($filesystem->has($path)) continue;

			$mediafile = Media::geef($mediaid);

			try
			{
				$mediafile->createThumbnail($size);
			}
			catch (\Exception $e)
			{
				$fouteMedia[] = $mediaid;
				$logger->info("Genereren van gecachte foto's gaat mis: mediaid " . $mediaid . " afmeting " . $size);
				$logger->error($e);
			}
		}
		flush();
	}
}

$mediaids = MediaVerzameling::geefVideos();

$chunks = array_chunk($mediaids, 200);
foreach($chunks as $mediaids) {
	foreach($mediaids as $mediaid) {
		$sizes = [
			'medium' => Media::geefAbsLoc("medium", $mediaid),
			'groot' => Media::geefAbsLoc("groot", $mediaid, false, 'mp4'),
			'thumbnail' => Media::geefAbsLoc("thumbnail", $mediaid),
		];
		foreach ($sizes as $size => $path)
		{
			if ($filesystem->has($path)) continue;
			$mediafile = Media::geef($mediaid);

			// We vangen de error af en loggen die zodat de volgende media wel verwerkt worden.
			try
			{
				$mediafile->createThumbnail($size);
			}
			catch (\Exception $e)
			{
				$fouteMedia[] = $mediaid;
				$logger->info("Genereren van gecachte foto's gaat mis: mediaid " . $mediaid . " afmeting " . $size);
				$logger->error($e);
			}
		}
		flush();
	}
}

if (count($fouteMedia))
{
	echo "De volgende mediaid's gaven een error bij het voorgenereren:\n";
	var_dump($fouteMedia);
	exit(1);
}
?>
