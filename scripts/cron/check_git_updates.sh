#!/bin/bash

# Kijk of de live commit meer dan 2 weken oud is
# Zo ja, kijk of de live commit niet eens een nieuwere moet worden

cd /srv/http/www/git

git fetch &> /dev/null
if [ "`date -d "-14 days" +%s`" -gt "`git show -s --format=%ct`" ]; then
	# kijk of er ueberhaupt iets is om live te zetten
	diff_live_master=`git diff origin/$LIVEBRANCH origin/$DEVBRANCH --name-status --patience -M -C`
	diff_origin_local=`git diff HEAD origin/$LIVEBRANCH --name-status --patience -M -C`
	if [ -n "$diff_live_master" ] || [ -n "$diff_origin_local" ]; then
		echo "De livesite is al 14 dagen niet ge-upt."
		if [ -n "$diff_live_master" ]; then
			echo "De volgende veranderingen staan in $DEVBRANCH maar niet in $LIVEBRANCH:"
			echo "$diff_live_master"
			echo ""
		fi
		if [ -n "$diff_origin_local" ]; then
			echo "De volgende veranderingen staan in origin/$LIVEBRANCH maar niet op de vm-www:"
			echo "$diff_origin_local"
			echo ""
		fi
	fi
fi
