#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id$

/* Roep processTo4 aan voor alle boeken.
 * Wanneer de tijd 00:XX:XX is (dus tussen 12 en 1 's nachts), wordt de
 * cron-boolean FALSE meegegeven (op dit moment worden alle vrijgekomen boeken
 * doorgeschoven naar wachtende studenten). Alle andere tijden wordt TRUE
 * meegegeven.
*/
processTo4(NULL, strftime('%H') != 0);

// PapierMolen
// knal alles weg dat meer dan 12 weken verlopen is
$BWDB->q('DELETE LOW_PRIORITY FROM papiermolen WHERE TO_DAYS(NOW()) -TO_DAYS(verloopdatum) > 84');

// PapierMolen
// Stuur een herinnering over openstaande advertenties van > 3 mnd.
$pmlist = $BWDB->q('TABLE SELECT *
	FROM papiermolen
	WHERE
	verloopdatum > CURDATE() AND
	TO_DAYS(NOW()) - TO_DAYS(wanneer) > 95 AND
	(herinnerdatum IS NULL OR TO_DAYS(NOW()) - TO_DAYS(herinnerdatum) > 95)
	ORDER BY lidnr, wanneer');

$aanbiedingids = $ledenitems = array();
foreach($pmlist as $row) {
	$ledenitems[$row['lidnr']][] = $row;
	$aanbiedingids[$row['lidnr']][] = $row['aanbiedingid'];
}
foreach ($ledenitems as $lidnr => $items) {
	bwmail($lidnr, BWMAIL_PM_REMIND, $items);
	$BWDB->q('UPDATE LOW_PRIORITY papiermolen SET herinnerdatum = CURDATE()
		WHERE aanbiedingid IN (%Ai)', $aanbiedingids[$lidnr]);
}

// Studentbestellingen
// Gooi alle weg die al 180 dagen verlopen zijn.

$BWDB->q('DELETE LOW_PRIORITY FROM studentbestelling WHERE 
	(gekochtdatum IS NOT NULL AND (TO_DAYS(NOW()) - TO_DAYS(gekochtdatum)) > 180) OR
	(vervaldatum  IS NOT NULL AND (TO_DAYS(NOW()) - TO_DAYS(vervaldatum) ) > 180)');
