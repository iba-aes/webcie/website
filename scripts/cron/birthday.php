#!/usr/bin/php -q
<?php
require('script-init.php');
#
# progje om verjaardagen vanuit whoswho te mailen naar mensen
# (c) 2002-4 Bas Zoetekouw <bas@debian.org> & Webcie. All rights reserved.
# $Id$
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.  
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior written
#      permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
#  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
#  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE

# headers van verstuurde mails
$SECR = "Bestuur A-Eskwadraat <bestuur@A-Eskwadraat.nl>";
$WWW  = "World Wide Web Cie <www@A-Eskwadraat.nl>";
$JARIGL = "Verjaardag announcements <jarig@lists.A-Eskwadraat.nl>";
$MAILSUBJ = "Gefeliciteerd";

# %naam%, %voornaam%, %age%, %hem%, %zijn%, %jarigen%, %secr% en %lidnr% worden in
# onderstaande teksten vervangen door nuttige shit

# tekst voor mail naar jarige (niet-actief)
$LIDMAIL = "Hoi %voornaam%,\n\n".
"Volgens onze gegevens ben je vandaag jarig! Gefeliciteerd met je\n".
"%age%e verjaardag. We willen je een leuke dag toewensen. Zien we je\n".
"(nog) eens een keer op een A-Eskwadraat-activiteit?\n".
"\n".
"Groetjes,\n".
"%secr%,\n".
"namens het A-Eskwadraatbestuur\n".
"\n".
"-- \n".
"Voor actuele info over A-Eskwadraat en onze \n".
"activiteiten: zie http://www.A-Eskwadraat.nl";
# tekst voor actief lid
$ACTMAIL = "Hoi %voornaam%,\n\n".
"Volgens onze gegevens ben je vandaag jarig! Gefeliciteerd met je\n".
"%age%e verjaardag. We willen je een leuke dag toewensen.\n".
"We hopen dat je nog lang actief zult blijven bij onze vereniging.\n".
"\n".
"Groetjes,\n".
"%secr%,\n".
"namens het A-Eskwadraatbestuur\n".
"\n".
"-- \n".
"Voor actuele info over A-Eskwadraat en onze \n".
"activiteiten: zie http://www.A-Eskwadraat.nl";
# en de mail naar de secr
$SECRMAIL = "Hallo!\n\n".
"De volgende leden zijn vandaag jarig.\n".
"Vergeet niet om hen te feliciteren!\n\n".
"%jarigen%\n".
"Met vriendelijke groet,\nDe Webcie!";
# en tenslotte de mail naar jarig-l
$LISTMAIL = "Hoi!\n\n".
"Volgens onze gegevens is %naam% vandaag jarig.\n".
"Denk je eraan %hem% te feliciteren met %zijn% %age%e verjaardag?\n\n".
"Groetjes,\n%secr%\n\n\n".
"ps: http://www.A-Eskwadraat.nl/Leden/%lidnr%\n";


# hier begint het allemaal
# Wie is er vandaag jarig en is nog lid?

$jarigen = PersoonVerzameling::jarig()->leden();
$jarigenlijst = "";

foreach($jarigen as $jarige)
{
	$naam = PersoonView::naam($jarige);
	$voornaam = PersoonView::naam($jarige, WSW_NAME_VOORNAAM);
	$leeftijd = $jarige->leeftijd();
	$email = (@$jarige->getEmail()) ? $naam . ' <'.$jarige->getEmail().'>' : NULL;
	$lidnr = $jarige->geefID();
	
	$zijn = $jarige->getVoornaamwoord()->vervoeg('jouw');
	$hem = $jarige->getVoornaamwoord()->vervoeg('jou');
	$actief = $jarige->isActief(true);

	# actieve leden krijgen een ander mailtje
	$tekst = $actief ? $ACTMAIL : $LIDMAIL;

	# vervang shit uit algemene tekst door iets nuttigs
	$tekst = replace_vars($tekst);
	# en stuur het mailtje op als er een email bekend is
	$email && sendmail($SECR, $jarige->getContactID(), $MAILSUBJ, $tekst);

	# tekst voor de secr
	$warning = $email?'':' (maar heeft geen emailadres!)';
	$jarigenlijst .= " - $naam wordt $leeftijd jaar$warning\n";

	# mail naar jarig-l
	if($actief){
		$tekst = replace_vars($LISTMAIL);
		sendmail($SECR, $JARIGL, "Er is er een jarig!", $tekst);
	}
}

# en mail naar het bestuur
if ($jarigen->aantal() > 0) {
  $tekst = replace_vars($SECRMAIL);
  sendmail($WWW,$SECR,"Verjaardagen",$tekst);
}

function replace_vars($tekst)
{
  global $naam,$voornaam,$leeftijd,$hem,$zijn,$jarigenlijst,$lidnr;

  $tekst = str_replace('%naam%',    $naam,      $tekst);
  $tekst = str_replace('%voornaam%',$voornaam,  $tekst);
  $tekst = str_replace('%age%',     $leeftijd,  $tekst);
  $tekst = str_replace('%hem%',     $hem,       $tekst);
  $tekst = str_replace('%zijn%',    $zijn,      $tekst);
  $tekst = str_replace('%jarigen%', $jarigenlijst,   $tekst);
  $tekst = str_replace('%secr%',    SECRNAME, $tekst);
  $tekst = str_replace('%lidnr%',   $lidnr,     $tekst);

  return $tekst;
}
