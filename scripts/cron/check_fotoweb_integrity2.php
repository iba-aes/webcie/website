#!/usr/bin/php -q
<?php

ini_set('memory_limit','512M');

require("script-init.php");
global $WSW4DB, $filesystem;

//We doen hier een for-loop zodat de queries niet een te grote output terug geven
for($j=0;$j<1000;$j++) {
	$ids = $WSW4DB->q("TABLE SELECT `mediaID`,`activiteit_activiteitID` FROM `Media` WHERE `activiteit_activiteitID` IS NOT NULL AND `mediaID` > %i AND `mediaID` <= %i",$j*1000,($j+1)*1000);
	foreach($ids as $a) {
		if(!$WSW4DB->q("MAYBEVALUE SELECT `activiteitID` FROM `Activiteit` WHERE `activiteitID` = %i",$a[1]))
			echo "Media $a[0] heeft geen geldig activiteitID $a[1]\n";
	}
}

$ids = $WSW4DB->q("TABLE SELECT `tagID`,`commissie_commissieID` FROM `Tag` WHERE `commissie_commissieID` IS NOT NULL");
foreach($ids as $a) {
	if(!$WSW4DB->q("MAYBEVALUE SELECT `commissieID` FROM `Commissie` WHERE `commissieID` = %i",$a[1]))
		echo "Tag $a[0] heeft geen geldig commissieID $a[1]\n";
}

$ids = $WSW4DB->q("TABLE SELECT `tagID`,`introgroep_groepID` FROM `Tag` WHERE `introgroep_groepID` IS NOT NULL");
foreach($ids as $a) {
	if(!$WSW4DB->q("MAYBEVALUE SELECT `groepID` FROM `IntroGroep` WHERE `groepID` = %i",$a[1]))
		echo "Tag $a[0] heeft geen geldig groepID $a[1]\n";
}

//We doen hier een for-loop zodat de queries niet een te grote output terug geven
for($j=0;$j<1000;$j++) {
	$ids = $WSW4DB->q("TABLE SELECT `tagID`,`persoon_contactID` FROM `Tag` WHERE `persoon_contactID` IS NOT NULL AND `tagID` > %i AND `tagID` <= %i",$j*1000,($j+1)*1000);
	foreach($ids as $a) {
		if(!$WSW4DB->q("MAYBEVALUE SELECT `contactID` FROM `Persoon` WHERE `contactID` = %i",$a[1]))
			echo "Tag $a[0] heeft geen geldig contactID $a[1]\n";
	}
}

//We doen hier een for-loop zodat de queries niet een te grote output terug geven
for($j=0;$j<1000;$j++) {
	$ids = $WSW4DB->q("TABLE SELECT `tagID`,`media_mediaID`,`collectie_collectieID` FROM `Tag` WHERE `tagID` > %i AND `tagID` <= %i",$j*1000,($j+1)*1000);
	foreach($ids as $a) {
		if(!$WSW4DB->q("MAYBEVALUE SELECT `mediaID` FROM `Media` WHERE `mediaID` = %i",$a[1]))
			echo "Collectie-Tag $a[0] heeft geen geldig mediaID $a[1]\n";
		if(!$WSW4DB->q("MAYBEVALUE SELECT `collectieID` FROM `Collectie` WHERE `collectieID` = %i",$a[2]))
			echo "Collectie-Tag $a[0] heeft geen geldig collectieID $a[2]\n";
	}
}
 
//We doen hier een for-loop zodat de queries niet een te grote output terug geven
for($j=0;$j<1000;$j++) {
	$ids = $WSW4DB->q("COLUMN SELECT `mediaID` FROM `Media` WHERE `mediaID` > %i AND `mediaID` <= %i",$j*1000,($j+1)*1000);
	foreach($ids as $a) {
		$absloc = Media::geefAbsLoc('Origineel',$a,true);
		if(!$filesystem->has($absloc)) {
			echo "Media $a bestaat niet op het filesystem!\n";
		}
	}
}

$fw_dir = dir(FOTOWEB_DIR);
$fsMediaids = array();
global $FW_EXTENSIONS;
while (false !== ($entry = $fw_dir->read())){
	if (is_dir(FOTOWEB_DIR . $entry)){
		$subdir = dir(FOTOWEB_DIR . $entry);
		if (!is_numeric($entry)) continue; // geen fotodir

		while (false !== ($mediafile = $subdir->read())){
			$fullpath = FOTOWEB_DIR . "$entry/$mediafile";
			// LET OP: array_pop(explode('.', $fullpath)) geeft een memory leak
			$currextension = explode('.', $fullpath);
			$currextension = array_pop($currextension);
			$firstdotpos = strpos($mediafile, ".");

			if (in_array($currextension, $FW_EXTENSIONS)){
				$clean_filename = substr($mediafile, 0, $firstdotpos); // bestandsnaam tot eerste punt (".")
				$mediaid = $clean_filename;

				if(strpos($mediafile,'.debug')){
					// Debugzaken negeren, worden na een tijdje automatisch verwijderd
					continue;
				}

				if (!$WSW4DB->q("MAYBEVALUE SELECT `mediaID` FROM `Media` WHERE `mediaID` = %i",$mediaid)){
					echo "Media $mediaid bestaat wel op het filesystem ($fullpath), maar niet in de database!\n";
				}
			}
		}
	}
}
?>
