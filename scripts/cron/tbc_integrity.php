#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id 

$outputBuffer = "";

GLOBAL $filesystem;

$tents = TentamenQuery::table()->verzamel();

foreach($tents as $tent) {
	$loc = $tent->getTentamen();

	// Deze zijn true-ish als er iets VERKEERD is.
	$tentNietGevonden = $loc && !$filesystem->has($tent->getBestand());
	$geenBestanden = !$loc && $tent->geefAantalUitwerkingen() == 0;
	$uitwNietGevonden = array(); // kleinere array van alleen de data die we gaan printen.

	$uitwerkingen = TentamenUitwerkingVerzameling::geefVanTentamen($tent);
	foreach($uitwerkingen as $uitwerking)
	{
		if(!$filesystem->has($uitwerking->getBestand())) {
			$uitwNietGevonden[] = array(
				"bestand" => $uitwerking->getBestand(),
				"uploader" => $uitwerking->getUploader()->getNaam()
			);
		}
	}

	if ($geenBestanden || $tentNietGevonden || $uitwNietGevonden) {
		if ($geenBestanden) {
			$outputBuffer .= "- Er is geen pdf-bestand en er zijn geen uitwerkingen.\n";
		}
		if ($tentNietGevonden || $uitwNietGevonden) {
			$aantalmis = ($tentNietGevonden ? 1 : 0) + count($uitwNietGevonden);
			$outputBuffer .= "Voor tentamen " . $tent->geefID()
					. " bij vak " . $tent->getVak()->getCode()
					. " (id: " . $tent->getVak()->getVakID()
					. ") mist volgens de db " . ($aantalmis == 1 ? "het volgende bestand" : "de volgende bestanden") .": \n";
			if ($tentNietGevonden) {
				$outputBuffer .= "\t-> Tentamen " . $tent->getBestand() . "\n";
			}
			if ($uitwNietGevonden) {
				foreach($uitwNietGevonden as $uitw) {
					$outputBuffer .= "\t-> Uitwerking " . $uitw["bestand"] . " van uploader " . $uitw["uploader"] . "\n";
				}
			}
		}
	}
}

if ($outputBuffer) echo $outputBuffer;
