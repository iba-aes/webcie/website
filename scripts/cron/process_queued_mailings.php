#!/usr/bin/php -q
<?php
require('script-init.php');
require_once('WhosWho4/init.php');

/* $Id: $
  Hier worden gequeuede mailings (uit MailingWeb) verwerkt en verstuurd
*/

// Te verzenden mailings
$teverzenden = MailingVerzameling::teVerzendenMailings();
foreach ($teverzenden as $mailing){
	$error = null;
	echo 'MailingWeb mailing ' . $mailing->getMailingID() . ' versturen...';

	// Checken of er nergens 'todo' in de body staat
	$body = $mailing->getMailBody();
	try {
		$mailing->verstuurMailing();
	} catch (Exception $e){
		// Er ging iets mis...
		$error = $e->getMessage();
	}

	if ($error){
		echo "error: $error";

		// Afzender van de mailing even notifyen van probleem
		$mailing_sender = $mailing->getMailFrom();
		$msg = "Hoi!\n\n";
		$msg .= "Er is een fout opgetreden bij het versturen van een mailing uit MailingWeb: $error";
		$msg .= "\n\n";
		$msg .= "Je kunt de mailing bekijken of bewerken door de volgende link te volgen:\n";
		$msg .= "http://www.A-Eskwadraat.nl" . $mailing->url();
		$msg .= "\n\n";
		$msg .= "Succes met fixen!";
		sendmail("MailingWeb <www@a-eskwadraat.nl>", $mailing_sender . ", www@a-eskwadraat.nl", "Fout bij versturen mailing uit MailingWeb", wordwrap($msg));
	}
}

