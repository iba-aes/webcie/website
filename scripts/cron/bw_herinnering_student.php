#!/usr/bin/php -q
<?php
require('script-init.php');
// $Id$

geleverdBoek();
besteldBoek();

function besteldBoek()
{
	global $BWDB, $BESTELDATA;

	$nodig = FALSE;
	$waarschuwmoment = cal_AddDays(time(), SB_HERINNERDAGEN, '%Y-%m-%d');
	foreach ($BESTELDATA as $begin => $eind) {
		if ($begin == $waarschuwmoment) {
			$nodig = TRUE;
			break;
		}
	}

	if (!$nodig) {
		return;
	}

	$fase1 = sqlFaseInfo('1');

	foreach ($fase1 as $EAN => $boekinfo) {
		if ($boekinfo['aantal'] < 0) {
			$boekinfo['aantal'] = 0;
		}
		/* Alle studenten die momenteel dit EAN in bestelling hebben */
		$studenten = $BWDB->q('TABLE SELECT lidnr, titel, auteur, bestelnr
				FROM studentbestelling LEFT JOIN boeken USING (EAN)
				WHERE gemailddatum is null 
					AND gekochtdatum is null 
					AND vervaldatum is null
					AND boeken.EAN = %s
				ORDER BY bestelddatum DESC
				LIMIT ' . $boekinfo['aantal'], $EAN);

		foreach($studenten as $row) {
			$data = array(
				'titel'       => $row['titel'],
				'auteur'      => $row['auteur'], 
				'besteldatum' => printdate($waarschuwmoment, FALSE),
				'bestelnr'    => $row['bestelnr']
				);

			bwmail($row['lidnr'], BWMAIL_SB_HERINNERING_BESTELLING, $data);
		}
	}
}

function geleverdBoek()
{
	global $BWDB;

	/* 1 week, 3 dagen en 1 dag van tevoren en op de verloopdag */
	$MAILDAGEN = array(7, 3, 1, 0); 

	/* Alle studenten die hun boek momenteel kunnen afhalen */
	$studenten = $BWDB->q(
			'TABLE SELECT lidnr, (TO_DAYS(vervaldatum) - TO_DAYS(now())) AS dagen, titel,
				auteur, DATE_SUB(vervaldatum, INTERVAL 1 DAY) as laatstedag,
				boeken.EAN
			FROM boeken LEFT JOIN studentbestelling USING (EAN)
			WHERE gemailddatum is not null 
				AND gekochtdatum is null 
				AND TO_DAYS(vervaldatum) >= TO_DAYS(now())');

	foreach($studenten as $row) {
		if (!in_array($row['dagen'], $MAILDAGEN)) {
			continue;
		}

		$dagstr = ($row['dagen'] == 1)?'dag':'dagen';
		$data = array(
			'titel'      => $row['titel'],
			'auteur'     => $row['auteur'], 
			'laatstedag' => strftime('%e %B %Y', strtotime($row['laatstedag']))
			);

		if ($row['dagen'] == 0) {
			$fase3 = sqlFaseInfo('3', array($row['EAN']));
			$data['boekinvrijeverkoop'] = ( $fase3[$row['EAN']]['aantal'] > 0 );

			$mailwhat = BWMAIL_BOEKVERLOPEN;
		} else {
			$mailwhat = BWMAIL_BOEKALPOOSJEBINNEN;
		}

		bwmail($row['lidnr'], $mailwhat, $data);
	}
}
