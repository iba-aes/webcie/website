#!/bin/bash
#
# Dit scriptje kijkt of er wijzigingen in de config op vm-www(-debug) zijn die niet
# gecommit zijn.

# Onder vm-www-debug, voer dit uit als:
# ./check_gitst_changes.sh -s vm-www-debug
# In alle andere gevallen (dus ook vm-www) wordt de status van de live server
# gecontroleerd

TMPFILE=`mktemp -t check-config-git.$$.XXXXXXXX`

# git_st_check <path> [ignore regexp]
function git_st_check()
{
	local exitcode=0

	(cd $1 && git status -s) &> $TMPFILE
	exitcode=$?

	if [ $exitcode -ne 0 ]; then
		echo "'git status' op '$1' retourneerde exitcode $exitcode."
		# we willen ook even de foutmelding met de mail sturen zodat mensen
		# het makkelijk kunnen oplossen:
		cat $TMPFILE
		return
	fi
	if [ -n "$2" ]; then
		grep -v -e "$2" $TMPFILE | sponge $TMPFILE
	fi
	if [ -s $TMPFILE ]; then
		echo "Wijzigingen in $1:"
		cat $TMPFILE
		echo
	fi
}

HEEFT_SERVER=0

while getopts ":s:" opt; do
	case $opt in
		s)
			HEEFT_SERVER=1
			SERVER=$OPTARG
			;;
		\?)
			echo "Parameter -$OPTARG snap ik niet!" >&2
			exit 1
			;;
		:)
			echo "De optie -$OPTARG wil een argument vreten. Geef deze!" >&2
			exit 1
			;;
	esac
done


if [ $HEEFT_SERVER -eq 0 ]; then
	echo "Je moet een server meegeven met '-s', kluns!"
	exit 1
fi

case $SERVER in
	vm-www)
		git_st_check /srv/http/www/git
		;;
	vm-www-debug)
		git_st_check /srv/config/vm-www-debug
		git_st_check /srv/http/demo/lib
		git_st_check /srv/http/demo/scripts
		git_st_check /srv/http/demo/www       space/sendmail.php
		;;
	*)
		echo "Die server vreet ik niet!"
		;;
esac

# ruim tot slot tempfile op
if [ -f $TMPFILE ]; then
	rm -f $TMPFILE
fi

exit 0
