#!/usr/bin/php -q
<?php
/* \file stuur_plugin_email.php
 * Dit script probeert de WebCie in de toekomst eraan te herinneren dat
 * libraries en plugins moeten worden geüpdatet zo af en toe. Als je een
 * nieuwere versie van een library of plugin installeert of een geheel
 * nieuwe library of plugin, vergeet dan niet om deze in de mail erbij
 * te zetten!
 */
require('script-init.php');

$date = new DateTimeLocale();

$MAIL = sprintf("Lieve leden van de WebCie,

Het is weer %s en dat betekent dat de website jullie aanraadt
om alle plugins en libraries te controleren en te kijken of er
misschien niet nieuwere versies zijn, zo houd je de website
up-to-date en zorg je ervoor dat je geen deprecated software hebt.

De volgende plugins en libraries worden op dit moment gebruikt:

JS:

datatables.js
dragscroll.js
dropzone.js
imageMapResizer.js
isotope.js
jquery-3.1.1.js (en jquery-migrate-3.0.0.js)
jquery-ui.js
jquery-ui-timepicker.js
jquery.color.js
jquery.cycle.all.js
jquery.visible.js
knockout.js
knockout.mapping.js
linedtextarea.js
llqrcode.js
photoswipe.js (en photoswipe-ui-default.js)
sorttable.js

CSS:
bootstrap 3.3.7 (bootstrap.css, bootstrap.js, etc.)
jquery-ui.css (en jquery-ui.structure.css, jquery-ui.theme.css)
photoswipe.css ( en photswip-ui-default.css)
datatables.css
dropzone.css
linedtextarea.css
normalize.css
photoswipe*.css


REST:
closer-compiler & minifiy
Mollie-API
twig 2.2.0

Contoleer dus of deze allemaal nog werken en up-to-date zijn!

Als je plugins en libraries update, of je installeert nieuwe plugins
of libraries, vergeet dan niet om ze in deze mail erbij te zetten.

De groente,
De WebCie uit het verleden!", $date->format('j F'));

sendmail('webcie@a-eskwadraat.nl', 'webcie@a-eskwadraat.nl',
	'Controleer je libraries en plugins', $MAIL);
