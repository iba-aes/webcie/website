#!/bin/sh
# $Id$ 

# Maak incrementele backups van alle databases onder controle van de www

set -e

if ! [ -d /archief/www ]; then
	echo "De backupschijf moet gemount zijn op /archief en een schrijfbare subdirectory op /archief/www hebben!"
	echo "(Je wilt uiteraard niet backuppen naar dezelfde (virtuele) machine)"
	echo "FIX DIT MOEILIJK SNEL!"
	echo "Tot gauw, het backupscript"
	exit 1
fi

# Key ID van de A-eskwadraat public key
ENCRYPTION_KEY_ID='F32C698FE211266F935B3943B29B325E26B56716'
# Check of de key bestaat. Void alle output en ga verder zelfs al faalt het command
# met een error code
gpg --list-keys ${ENCRYPTION_KEY_ID} > /dev/null 2>&1 || true
# Variabele of de sleutel bestaat
KEY_EXISTS=$?

# Als het is gefaald echo'en we nette output
if [[ $KEY_EXISTS != 0 ]]; then
    echo "De public key van de A-Eskwadraat Backup Service staat niet in de keyring van de apache user!"
    echo "Hierdoor werken de backups niet!"
    echo "FIX DIT MOEILIJK SNEL!"
    echo "Tot gauw, het backupscript"
    exit 1
fi


# Niet 2x tegelijkertijd draaien, da's eng
lockfile -r0 /archief/www/lock/backup_databases.lock

cd /archief/www

newdir=`date +%Y-%m/%d`
newfile=`date +%Y-%m-%d_%H_%M`

mkdir -p $newdir
ln -snf $newdir today

for db in benamite boeken whoswho4; do
	new=$newdir/$db.$newfile

	if test "$1" = "--full"; then
		mysqldump --complete-insert --skip-extended-insert --skip-comments --add-drop-table $db > $new.full
		# Encrypt de logs
		gpg --output "${new}.full.gpg" --encrypt --recipient "webcie@a-eskwadraat.nl" "${new}.full"
		# En verwijder de oude logs.
		# TODO: Zet dit aan, momenteel doen we dit nog niet om het systeem te testen
		#rm ${new}.full
	else
		# Non-volledige backups zijn deprecated.
		echo "Waarschuwing: de incrementele backup via een diff van de mysqldump is deprecated."
		echo "We maken nu een kopie van de binaire logs van mysql."
		mysqldump --complete-insert --skip-extended-insert --skip-comments --add-drop-table $db > $new

		diff -U 0 -H $db.old $new --speed-large-files > $new.diff && rm $new.diff
		rm $db.old
		mv $new $db.old
	fi
done

rm -f /archief/www/lock/backup_databases.lock
