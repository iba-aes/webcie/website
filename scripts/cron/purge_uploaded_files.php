#!/usr/bin/php
<?php
require_once('script-init.php');
/**
 * Bok geuploade files weg die niet meer gereferenced worden
 * in de database, voor benamite uploaded files en foto's.
 */

$BENFILESBASE = BENAMITE_FILES;
$FOTOSBASE = '';

$res = $BMDB->q('TABLE SELECT * FROM benamite WHERE type = %i', ENTRYUPLOADED);
$benfiles = array();
foreach($res as $row) {
	$benfiles[] = substr($row['special'], 0, 32);
}

$dir = opendir($BENFILESBASE);
while ( $file = readdir($dir) ) {
	if ( $file == '.' || $file == '..' ) continue;
	
	if ( !in_array($file,$benfiles) ) {
		unlink ($BENFILESBASE . $file);
	}
}

