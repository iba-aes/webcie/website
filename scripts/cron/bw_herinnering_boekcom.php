#!/usr/bin/php -q
<?php
require('script-init.php');
// $Id$

$data = array();

/* Is er vandaag een bestelmoment? */
foreach ($BESTELDATA as $besteldag => $leverdag) {
	if ($besteldag != date('Y-m-d')) {
		continue;
	}
	$tebestellen = sqlFaseInfo('1');
	$aantal = 0;
	foreach ($tebestellen as $EAN => $info) {
		if ($info['aantal'] <= 0 || !$info['bestelbaar']) continue;
		$aantal += $info['aantal'];
	}
}
if (isset($aantal) && $aantal > 0) {
	$data[] = 'Vandaag is een bestelmoment bij de leverancier. '
		."Er zijn $data[aantal] boeken die besteld dienen te worden.";
}


$vakkkendata = sqlGetHuidigeVakken(colJaar(), NULL, NULL, strftime('%Y-%m-%d %H:%M:%S'), NULL);
$ontberekendedata = array();
foreach ($vakkkendata as $vaknr => $vakinfo) {
	if (!isset($vakinfo['email']) || !$vakinfo['email']) {
		$data[] = "Bij het vak $vakinfo[naam] ($vaknr) is geen e-mailadres bekend.";
	}
	if (!isset($vakinfo['docent']) || !$vakinfo['docent']) {
		$data[] = "Bij het vak $vakinfo[naam] ($vaknr) is geen docent bekend.";
	}
}

if (time() - strtotime(EJBV) > 60 * 60 * 24 * 7){
	// Het is langer dan een week na de EJBV
	$voorraden = VoorraadSet::createByVoorraadLocatie("ejbv_voorraad");
	if (sizeof($voorraden) > 0){
		foreach ($voorraden as $voorraad){
			$EAN = $voorraad->getEAN();
			$info = sqlBoekGegevens($EAN);
			$aantal = $voorraad->getVoorraad("ejbv_voorraad");
			if ($aantal == 1){
				$data[] = "Van het artikel '$info[titel]' van '$info[auteur]' (EAN: $EAN) ligt nog 1 exemplaar in de EJBV-voorraad, terwijl de EJBV meer dan een week geleden is!";
			} else {
				$data[] = "Van het artikel '$info[titel]' van '$info[auteur]' (EAN: $EAN) liggen nog $aantal exemplaren in de EJBV-voorraad, terwijl de EJBV meer dan een week geleden is!";
			}
		}
	} // else: geen ejbv_voorraden meer
}

if (sizeOf($data) > 0) {
	bwmail(null, BWMAIL_BOEKCOM_BOOKWEBINFO, $data);
}
