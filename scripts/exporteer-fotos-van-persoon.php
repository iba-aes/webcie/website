<?php

/**
 * Script om alle foto's van een persoon te exporteren in een ZIP bestand
 */

require('script-init.php');

function main() {
	$persoonId = processArguments();
	$persoon = Persoon::geef($persoonId);
	if (!$persoon) {
		printErrorAndExit("Het ID is niet van een valide persoon!");
	}
	$fotoIds = MediaVerzameling::geefFotos('persoon', $persoon);
	$zip = new ZipArchive();
	$zipName = "{$persoonId}.zip";
	$zip->open($zipName, ZipArchive::CREATE);
	foreach ($fotoIds as $fotoId) {
		$foto = Media::geef($fotoId[0]);
		$fileLoc = $foto->geefLoc('Origineel', true);
		$zip->addFile($fileLoc, $foto->getMediaID() . ".png");
	}
	$zip->close();
	echo "Zip file {$zipName} gemaakt!";
}

function processArguments(): ?int {
	$argv = $_SERVER['argv'];
	if (count($argv) > 1) {
		$persoonId = $argv[1];
		if (is_numeric($persoonId)) {
			return $persoonId;
		}
		printErrorAndExit("Argument is nummer!");
	} else {
		printErrorAndExit("Geef het persoon ID mee als argument!");
	}
}

function printErrorAndExit(string $message) {
	fwrite(STDERR, $message);
	exit(1);
}

main();
