<?PHP
/*
 * Dit script is ervoor om de privileges van databases naar test_databases 
 * te kopi�ren. (of andersom, indien $real2test false is)
 * in te vullen voor gebruik: 
 * 	$real2test
 *	$rpw
 */


// copieren van real 2 test, of andersom
//$real2test=;

// passwd
//$rpw=;

if(!isset($real2test)|| !isset($rpw)){
	echo "Variabelen rpw en real2test dienen ingevuld te zijn";
	exit(1);
}

require('script-init.php');

$GLOBALS['LEVEL']='root';

//make connection
$MYSQLDB=new db('mysql', 'localhost', 'root', $rpw);

//voor welke usernames moeten we iets doen
$names=array_keys(secret::dbpasswords());

// welke tabelen in mysql hebben privilege data
$tables=array('columns_priv', 'db', 'tables_priv');
foreach ($tables as $priv_table){
	
	$test_dbs=$MYSQLDB->q("COLUMN SELECT DISTINCT(Db) FROM db WHERE Db LIKE 'test%%' ");
	foreach ($test_dbs as $test_db){
		if (	substr($test_db, 0,5) == "test_" 
		   &&	strpos($test_db, "%") === false
		   )
			
			if($real2test)	{
				$dbs[]=substr($test_db,5);
			} else {
				$dbs[]=$test_db;
			}
	}
		
	$privs= $MYSQLDB->q("TABLE SELECT * FROM ".$priv_table." WHERE User IN (%As) AND Db IN (%As)", $names, $dbs);
	
	$tmp = array();
	foreach($privs as $index=>$row){
		foreach($row as $key=>$priv)
		if(!is_numeric($key)){
			if(strtolower($key) == 'db' 
				&& $real2test){
				$tmp[$key]='test_'.$priv;
			} else {
				$tmp[$key]=$priv;
			}
		}
		$privs[$index]=$tmp;
		$MYSQLDB->q('REPLACE '.$priv_table.' SET  %S', $tmp);
		unset($tmp);
	}
}
exit(0);
?>
