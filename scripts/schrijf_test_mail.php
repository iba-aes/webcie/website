#!/usr/bin/php -q
<?php
require_once('script-init.php');
/* Genereer een voorbeeldmailtje, zodat je die aan scripts kunt geven die mails verwerken.
 * Voorbeeld: ./schrijf_test_mail.php | ./mail_process_bug_vm-www
 * (Technische details: dit script vraagt info aan de gebruiker via stderr,
 *  en schrijft resultaten naar stdout.)
 */

$stdin = fopen('php://stdin', 'r');
$stderr = fopen('php://stderr', 'w');
$velden = array("From", "To", "Subject");
foreach ($velden as $veld) {
	fwrite($stderr, $veld . "? ");
	$antwoord = trim(fgets($stdin));
	echo $veld . ": " . $antwoord . "\n";
}
echo "\n"; // extra newline om de body te beginnen
fwrite($stderr, "Body? (Ctrl-D om te stoppen)\n");
while ($line = fgets($stdin)) {
	echo $line; // geen \n omdat we niet trimmen
}
?>
