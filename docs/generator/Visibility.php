<?php

namespace Generator;

use MyCLabs\Enum\Enum;

/**
 * Class Visibility
 * @package Generator
 * @method static Visibility PUBLIC()
 * @method static Visibility PRIVATE()
 * @method static Visibility PROTECTED()
 * @method static Visibility IMPLEMENTATION()
 */
final class Visibility extends Enum
{
	const PUBLIC = 'public';
	const PRIVATE = 'private';
	const PROTECTED = 'protected';
	const IMPLEMENTATION = 'implementation';
}