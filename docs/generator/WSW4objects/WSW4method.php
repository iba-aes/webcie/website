<?php

namespace Generator\WSW4objects;

use Generator\DocCommentTemplate;
use Generator\Bestand;
use Generator\Visibility;

/**
 * Class WSW4method
 * @package Generator\WSW4objects
 */
class WSW4method
{
	/**
	 * @var string
	 */
	protected $name;
	/**
	 * @var
	 */
	protected $comment;
	/**
	 * @var Visibility
	 */
	public $visibility;
	/**
	 * @var bool
	 */
	protected $abstract = False;
	/**
	 * @var bool
	 */
	protected $static = False;
	/**
	 * @var bool
	 */
	protected $virtual = False;
	/**
	 * @var bool
	 */
	protected $generated = False;
	/**
	 * @var WSW4param[]
	 */
	protected $params = [];

	/**
	 * WSW4method constructor.
	 * @param string $name
	 * @param string $comment
	 */
	public function __construct(string $name, string $comment)
	{
		$this->name = $name;
		$this->visibility = Visibility::PUBLIC();
		$this->comment = $comment;
	}

	/**
	 * @return string
	 */
	public function name(): string
	{
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isAbstract(): bool
	{
		return $this->abstract;
	}

	/**
	 * @return bool
	 */
	public function isStatic(): bool
	{
		return $this->static;
	}

	/**
	 * @return bool
	 */
	public function isVirtual(): bool
	{
		return $this->virtual;
	}

	/**
	 * @return bool
	 */
	public function isGenerated(): bool
	{
		return $this->generated;
	}

	/**
	 * @return WSW4param[]
	 */
	public function params(): array
	{
		return $this->params;
	}

	/**
	 * @param bool $static
	 */
	public function setStatic(bool $static)
	{
		$this->static = (bool)$static;
	}

	/**
	 * @param Visibility $visibility
	 */
	public function setVisibility(Visibility $visibility)
	{
		$this->visibility = $visibility;
	}

	/**
	 * @param bool $virtual
	 */
	public function setVirtual(bool $virtual)
	{
		$this->virtual = $virtual;
		if ($virtual && !$this->static) {
			$this->abstract = True;
		}
	}

	/**
	 * @param bool $abstract
	 */
	public function setAbstract(bool $abstract): void
	{
		$this->abstract = $abstract;
	}

	/**
	 * @param bool $generated
	 */
	public function setGenerated(bool $generated): void
	{
		$this->generated = $generated;
	}

	/**
	 * @param WSW4param[] $params
	 */
	public function setParams(array $params): void
	{
		$this->params = $params;
	}

	/**
	 * @param WSW4param $param
	 */
	public function addParam(WSW4param $param)
	{
		$this->params[] = $param;
	}

	/**
	 * @param WSW4method $method
	 * @return WSW4method
	 */
	public static function fromWSW4method(WSW4method $method): WSW4method
	{
		$m = new static($method->name, $method->comment);
		$m->visibility = $method->visibility;
		$m->abstract = $method->abstract;
		$m->static = $method->static;
		$m->virtual = $method->virtual;
		$m->generated = $method->generated;
		$m->params = $method->params;
		return $m;
	}

	/**
	 * @param WSW4class $class
	 * @return bool
	 */
	protected function shouldNotGenerateBody(WSW4class $class): bool
	{
		return ($this->virtual && !$this->generated)
			|| $this->abstract
			|| !$this->virtual
			|| $class->getPackage()->name() == "Basis";
	}

	protected function generateAbstractCode(Bestand $bestand)
	{
		if ($this->virtual && !$this->generated && $this->static) {
			return;
		}

		if ($this->abstract) {
			$bestand->addContent($this->codeMethodHeader(false) . ";\n");
			return;
		}

		if (!$this->virtual) {
			$bestand->addContent($this->codeMethodHeader()
				. "\t{\n"
				. "\t\tuser_error('501 Not Implemented', E_USER_ERROR);\n"
				. "\t}\n");
		}

		return;
	}

	/**
	 * Deze methode maakt de bijbehorende implementatie code en schrijft die
	 * gelijk naar $fp
	 * @param WSW4class $class
	 */
	public function generateCode(Bestand $bestand, WSW4class $class)
	{
		if ($this->virtual && !$this->generated) {
			$this->abstract = True;
		}

		if ($this->shouldNotGenerateBody($class)) {
			$this->generateAbstractCode($bestand);
			return;
		}

		$this->generateCodeBody($bestand, $class);
	}

	protected function generateCodeBody(Bestand $bestand, WSW4class $class) {
		// Voor virtual methods ergens bovenin de klassenhierarchie worden
		// gedeclareerd maar pas lager in de klassehierarchie worden
		// gegenereerd. Uitzonderingsgevallen dus!
		error($this, "Virtual method '" . $this->name . "' gedefinieerd in class diagram, maar is onbekend in codegenerator!\n"
			. "Maak een nieuw `WSW4method`-kindklasse voor deze methode en implementeer de method `generateCodeBody`.");
		return;
	}

	/**
	 * Geneert methode-header en commentaar voor deze method, bijv:
	 * 'protected function bladiebla($myparam, $andereparam)'
	 *
	 * (de accolade die een method body opent wordt _niet_ gegenereerd!)
	 * @param bool $newline
	 * @return string
	 */
	protected function codeMethodHeader($newline = true): string
	{
		$generatedHeader = "";
		$this->comment = trim($this->comment);
		if (!empty($this->comment)) {
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief($this->comment);
			$generatedHeader .= $docCommentTemplate->generate(1);
		}

		$generatedHeader .= "\t";
		if ($this->abstract) {
			$generatedHeader .= 'abstract ';
		}
		if ($this->static) {
			$generatedHeader .= 'static ';
		}
		$generatedHeader .= $this->visibility . ' function ' . $this->name . '(';

		$generatedHeader .= implode(
			", ",
			array_map(function (WSW4param $param) {
				return $param->generateCode();
			}, $this->params)
		);

		$generatedHeader .= ")";

		if ($newline) {
			$generatedHeader .= "\n";
		}

		return $generatedHeader;
	}
}
