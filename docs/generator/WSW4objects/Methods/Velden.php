<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\Visibility;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Velden extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$localvelden = $this->name . $class->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Aanvulling op parent::velden(welke).")
			->addSee($localvelden);

		$generatedCode = $docCommentTemplate->generate(1);
		$generatedCode .= <<<EOT
	public static function {$this->name}(\$welke = NULL)
	{
		static \$veldenCache = array();

		if(!isset(\$veldenCache[\$welke])) {
			\$veldenCache[\$welke] =
					array_merge( parent::velden(\$welke)
					           , static::$localvelden(\$welke)
					           );
		}

		return \$veldenCache[\$welke];
	}

EOT;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef velden die bij deze klasse horen.")
			->addParam("welke", 'string|null', "String die een subset van velden beschrijft, of NULL voor alle velden.")
			->addSee("velden")
			->setReturn('array', "Een array met velden.");

		$generatedCode .= $docCommentTemplate->generate(1)
			. "\tprotected static function $localvelden(\$welke = NULL)\n"
			. "\t{\n"
			. "\t\t\$velden = array();\n"
			. "\t\tswitch(\$welke) {\n"
			. "\t\tcase 'set':\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| $var->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->visibility->equals(Visibility::PRIVATE())
				|| $var->visibility->equals(Visibility::PROTECTED())
				|| $var->isForeignKey()
				|| $var->annotation['PRIMARY']
				|| $var->annotation['PSEUDO']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'lang':\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| $var->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->visibility->equals(Visibility::PRIVATE())
				|| $var->visibility->equals(Visibility::PROTECTED())
				|| $var->isForeignKey()
				|| !$var->annotation['LANG']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'lazy':\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| !$var->annotation['LAZY']
				|| $this->visibility->equals(Visibility::IMPLEMENTATION())) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'verplicht':\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| $var->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->visibility->equals(Visibility::PRIVATE())
				|| $var->visibility->equals(Visibility::PROTECTED())
				|| $var->type == 'bool'
				|| $var->type == 'enum'
				|| $var->type == 'int'
				|| $var->type == 'bigint'
				|| $var->annotation['NULL']
				|| $var->annotation['PSEUDO']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'viewInfo':\n"
			. "\t\tcase 'viewWijzig':\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| $this->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->isForeignKey()
				|| $var->annotation['PRIMARY']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'get':\n";

		foreach ($class->vars as $var) {
			if ($var->isForeign() && !$var->isForeignObject()) {
				continue;
			}

			if ($var->static
				|| $this->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->isForeignKey()
				|| $var->annotation['PRIMARY']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\tcase 'primary':\n";
		foreach ($class->vars as $var) {
			if ($var->isForeign() && !$var->isForeignObject()) {
				$generatedCode .= "\t\t\t\$velden[] = '" . $var->name . "';\n";
				continue;
			}
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tcase 'verzamelingen':\n";

		foreach ($class->foreignrelations as $c => $isnull) {
			$generatedCode .= "\t\t\t\$velden[] = '" . ucfirst($c) . "Verzameling';\n";
		}

		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\tdefault:\n";

		foreach ($class->vars as $var) {
			if ($var->static
				|| $this->visibility->equals(Visibility::IMPLEMENTATION())
				|| $var->isForeignKey()
				|| $var->annotation['PRIMARY']) {
				continue;
			}

			$generatedCode .= "\t\t\t\$velden[] = '" . $var->upper() . "';\n";
		}
		$generatedCode .= "\t\t\tbreak;\n"
			. "\t\t}\n"
			. "\t\treturn \$velden;\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
