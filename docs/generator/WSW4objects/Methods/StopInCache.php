<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class StopInCache extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$generatedCode = "\tstatic public function stopInCache(\$id, \$obj, \$class = NULL, \$key = NULL)\n"
			. "\t{\n";

		if ($class->IDsource) {
			$generatedCode .= "\t\tparent::stopInCache(\$id, \$obj);\n"
				. "\t}\n\n";
			$bestand->addContent($generatedCode);
			return;
		}

		$generatedCode .= "\t\tparent::stopInCache(\$id, \$obj, '{$class->real->name}');\n";

		foreach ($class->IDchildren as $c) {
			$generatedCode .= "\t\tparent::stopInCache(\$id, \$obj, '"
				. $c->name . "');\n";
		}
		$generatedCode .= "\t}\n\n";

		$bestand->addContent($generatedCode);
	}
}
