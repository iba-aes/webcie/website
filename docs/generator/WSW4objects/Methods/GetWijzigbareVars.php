<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class GetWijzigbareVars extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$return = <<<EOT
	public function getWijzigbareVars(\$vars = NULL)
	{
		if(!\$this->magWijzigen())
			return array();

		if(is_null(\$vars) || !is_array(\$vars))
			\$vars = array();

		\$vars += static::velden('set');
		return parent::getWijzigbareVars(\$vars);
	}

EOT;
		$bestand->addContent($return);
	}
}
