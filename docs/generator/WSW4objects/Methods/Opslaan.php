<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Opslaan extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		// TODO what als ==eigenschap en $key == increment ?

		$classname = $class->real->name;
		$single = False;
		if (count($class->IDs) == 1) {
			$single = reset($class->IDs)->name;
		}

		$generatedCode = "";
		if (!empty($this->comment)) {
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief($this->comment);

			$generatedCode .= $docCommentTemplate->generate(1);
		}

		$keystr = '';
		$str = <<<EOT
	public function $this->name($keystr\$classname = '$classname')
	{
		global \$WSW4DB;

		if(!\$this->valid()) {
			\$errorInfo = "";
			\$first = true;
			foreach (\$this->getErrors() as \$id => \$error) {
				if (is_string(\$error)) {
					if (\$first) \$first = false;
					else \$errorInfo .= ",";
					\$errorInfo .= "\\n\$id: '\$error'";
				}
				else if (is_array(\$error))
					foreach (\$error as \$lang => \$msg)
						if (is_string(\$msg)) {
							if (\$first) \$first = false;
							else \$errorInfo .= ",";
							\$errorInfo .= "\\n\$id (\$lang): '\$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . \$errorInfo);
		}
EOT;
		$generatedCode .= $str;

		foreach ($class->IDs as $id => $var) {
			if (!$var->isForeignObject()) {
				continue;
			}

			$objname = $var->upper();
			$generatedCode .= "\n"
				. "\t\t\$rel = \$this->get$objname();\n"
				. "\t\tif(!\$rel->getInDB())\n"
				. "\t\t\tthrow new LogicException('foreign $objname is not in DB');\n";

			foreach ($var->foreign->IDs as $n => $c) {
				if ($c->isForeignObject()) {
					continue;
				}
				$getter = preg_replace_callback("/_(.)/",
					function ($matches) {
						return strtoupper($matches[1]);
					},
					$c->upper()
				);

				$name = $c->name;
				if ($c->isForeign()) {
					$name = substr($name, strrpos($name, '_') + 1);
				}
				$generatedCode .= "\t\t\$this->" . $id . "_" . $name
					. " = \$rel->get$getter();\n";
			}
		}
		if ($class->IDsource) {
			$generatedCode .= "\n"
				. "\t\t\$isDirty = \$this->dirty;"
				. "\n"
				. "\t\tparent::opslaan(\$classname);\n"
				. "\n"
				. "\t\t\$stillDirty = \$this->dirty;\n"
				. "\n"
				. "\t\t// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan\n"
				. "\t\tif (\$isDirty == \$stillDirty && !\$this->dirty)\n"
				. "\t\t\treturn;\n";
			$single = False;
		} else {
			$generatedCode .= "\n"
				. "\t\tif (!\$this->dirty)\n"
				. "\t\t\treturn;\n";
		}
		$first = true;
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if (in_array($var, $class->IDs)) {
				continue;
			}
			if (!$var->isForeignKey()) {
				continue;
			}

			$getter = preg_replace_callback("/_(.)/",
				function ($matches) {
					return strtoupper($matches[1]);
				},
				'get_' . $var->name
			);
			$generatedCode .= "\t\t\$this->$getter();\n";
			continue;
		}
		$generatedCode .= "\n"
			. "\t\t\$this->gewijzigd();\n";
		if (!$class->IDsource) {
			$generatedCode .= "\n";
		}
		$generatedCode .= "\t\tif(!\$this->inDB) {\n";
		$generatedCode .= ($single ? "\t\t\t\$this->$single =\n" : "")
			. "\t\t\t\$WSW4DB->q('" . ($single ? "RETURNID " : "")
			. "INSERT INTO `$classname`'\n";

		/** @var string[] $insertVelden
		 * Mapt veldnaam naar waarde (bijvoorbeeld 'overerving' => '%s'.
		 */
		$insertVelden = [];

		if ($class->IDchildren) {
			$insertVelden['overerving'] = '%s';
		}
		if ($class->IDsource) {
			foreach ($class->IDs as $var) {
				$insertVelden[$var->nameForDB] = '%' . $var->getLibDBType();
			}
		}
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if ($single && isset($class->IDs[$var->name])) {
				$single = $var->name;
				continue;
			}
			if ($var->type == 'object') {
				continue;
			}
			if ($var->isForeignObject()) {
				continue;
			}
			if ($var->annotation['LANG']) {
				$insertVelden[$var->nameForDB . '_NL'] = '%' . $var->getLibDBType();
				$insertVelden[$var->nameForDB . '_EN'] = '%' . $var->getLibDBType();
			} else {
				$insertVelden[$var->nameForDB] = '%' . $var->getLibDBType();
			}
		}
		if (!$class->IDsource) {
			$insertVelden['gewijzigdWanneer'] = '%s';
			$insertVelden['gewijzigdWie'] = '%i';
		}

		// Geef de veldnamen om in te inserten.
		$generatedCode .= "\t\t\t          . ' (";
		$first = true;
		foreach ($insertVelden as $veldnaam => $expressie) {
			if (!$first) {
				$generatedCode .= ', ';
			}
			$generatedCode .= '`' . $veldnaam . '`';
			$first = false;
		}
		$generatedCode .= ")'\n";
		// Geef de waarden om te inserten.
		$generatedCode .= "\t\t\t          . ' VALUES (";
		$first = true;
		foreach ($insertVelden as $expressie) {
			if (!$first) {
				$generatedCode .= ', ';
			}
			$generatedCode .= $expressie;
			$first = false;
		}
		$generatedCode .= ")'\n";

		if ($class->IDchildren) {
			$generatedCode .= "\t\t\t          , \$classname\n";
		}
		if ($class->IDsource) {
			foreach ($class->IDs as $var) {
				$generatedCode .= "\t\t\t          , \$this->" . $var->name . "\n";
			}
		}
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if ($single == $var->name) {
				continue;
			}
			if ($var->type == 'object') {
				continue;
			}
			if ($var->isForeignObject()) {
				continue;
			}
			if ($var->annotation['LAZY']) //Code duplicatie, doe iets slims?
			{
				if ($var->annotation['LANG']) {
					$generatedCode .= "\t\t\t          , isset(\$this->" . $var->name . "['nl'])?\$this->" . $var->name . "['nl']:" . $var->default . "\n"
						. "\t\t\t          , isset(\$this->" . $var->name . "['en'])?\$this->" . $var->name . "['en']:" . $var->default . "\n";
				} elseif ($var->type == 'date' || $var->type == 'datetime') {
					$generatedCode .= "\t\t\t          , isset(\$this->" . $var->name . ")?\$this->" . $var->name . "->strftime('%F %T'):" . $var->default . "\n";
				} else {
					$generatedCode .= "\t\t\t          , isset(\$this->" . $var->name . ")?\$this->" . $var->name . ":" . $var->default . "\n";
				}
			} else {
				if ($var->annotation['LANG']) {
					$generatedCode .= "\t\t\t          , \$this->" . $var->name . "['nl']\n"
						. "\t\t\t          , \$this->" . $var->name . "['en']\n";
				} elseif ($var->type == 'date' || $var->type == 'datetime') {
					if ($var->annotation['NULL']) {
						$generatedCode .= "\t\t\t          , (!is_null(\$this->" . $var->name . "))?\$this->" . $var->name . "->strftime('%F %T'):null\n";
					} else {
						$generatedCode .= "\t\t\t          , \$this->" . $var->name . "->strftime('%F %T')\n";
					}
				} else {
					$generatedCode .= "\t\t\t          , \$this->" . $var->name . "\n";
				}
			}
		}
		$generatedCode .= (!$class->IDsource
				? "\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
				. "\t\t\t          , \$this->gewijzigdWie\n"
				: "")
			. "\t\t\t          );\n"
			. "\n"
			. "\t\t\tif(\$classname == '$classname')\n"
			. "\t\t\t\t\$this->inDB = True;\n"
			. "\n"
			. "\t\t\tself::stopInCache(\$this->geefID(), \$this);\n"
			. "\t\t} else {\n"
			. "\t\t\t\$WSW4DB->q('UPDATE `$classname`'\n";
		// Komende regels zijn enorme code-duplicatie, moet iets beters voor te verzinnen zijn
		$first = True;
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if (isset($class->IDs[$var->name])) {
				continue;
			}
			if ($var->annotation['LAZY']) {
				continue;
			}
			if ($var->isForeignObject()) {
				continue;
			}
			if ($var->type == 'object') {
				continue;
			}
			if ($var->annotation['LANG']) {
				$generatedCode .= "\t\t\t          "
					. ($first ? ".' SET" : ".   ',") . " `"
					. $var->name . "_NL` = %"
					. $var->getLibDBType() . "'\n"
					. "\t\t\t          .   ', `"
					. $var->name . "_EN` = %"
					. $var->getLibDBType() . "'\n";
			} else {
				$generatedCode .= "\t\t\t          "
					. ($first ? ".' SET" : ".   ',")
					. " `" . $var->name . "` = %"
					. $var->getLibDBType() . "'\n";
			}
			$first = False;
		}
		if (!$class->IDsource) {
			$generatedCode .= "\t\t\t" . ($first ? ".' SET" : ".   ',") . " `gewijzigdWanneer` = %s'\n"
				. "\t\t\t          .   ', `gewijzigdWie` = %i'\n";
		}

		$generatedCode .= $class->genSQLWhere(3);

		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if (isset($class->IDs[$var->name])) {
				continue;
			}
			if ($var->isForeignObject()) {
				continue;
			}
			if ($var->type == 'object') {
				continue;
			}
			if ($var->annotation['LAZY']) {
				continue;
			}
			if ($var->annotation['LANG']) {
				$generatedCode .= "\t\t\t          , \$this->" . $var->name . "['nl']\n"
					. "\t\t\t          , \$this->" . $var->name . "['en']\n";
			} elseif ($var->type == 'date' || $var->type == 'datetime') {
				if ($var->annotation['NULL']) {
					$generatedCode .= "\t\t\t          , (!is_null(\$this->" . $var->name . "))?\$this->" . $var->name . "->strftime('%F %T'):null\n";
				} else {
					$generatedCode .= "\t\t\t          , \$this->" . $var->name . "->strftime('%F %T')\n";
				}
			} else {
				$generatedCode .= "\t\t\t          , \$this->" . $var->name . "\n";
			}
		}
		$generatedCode .= (!$class->IDsource
			? "\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
			. "\t\t\t          , \$this->gewijzigdWie\n"
			: "");

		$generatedCode .= $class->genSQLWhereArgs(3)
			. "\t\t\t          );\n"
			. "\t\t}\n\t\t\$this->dirty = false;\n"
			. "\t}\n";
		$bestand->addContent($generatedCode);
	}
}
