<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class MagBekijken extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$extended = true;
		if (!$class->IDsource) {
			$extended = false;
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker dit mag zien.");

		$bestand->addContent($docCommentTemplate->generate(1)
			. "\tpublic function magBekijken()\n"
			. "\t{\n"
			. "\t\treturn {$class->real->name}::magKlasseBekijken()"
			. ($extended ? " || parent::magBekijken();\n" : ";\n")
			. "\t}\n");
	}
}
