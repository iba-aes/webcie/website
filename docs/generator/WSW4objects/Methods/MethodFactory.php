<?php

namespace Generator\WSW4objects\Methods;

use Generator\WSW4objects\WSW4method;

final class MethodFactory
{
	public static function newMethodInstance(WSW4method $method)
	{
		switch ($method->name()) {
			case 'geefID':
				return GeefID::fromWSW4method($method);
			case 'stopInCache':
				return StopInCache::fromWSW4method($method);
			case 'geef':
				return Geef::fromWSW4method($method);
			case 'magKlasseBekijken':
				return MagKlasseBekijken::fromWSW4method($method);
			case 'magKlasseWijzigen':
				return MagKlasseWijzigen::fromWSW4method($method);
			case 'magBekijken':
				return MagBekijken::fromWSW4method($method);
			case 'magWijzigen':
				return MagWijzigen::fromWSW4method($method);
			case 'geefExtensies':
				return GeefExtensies::fromWSW4method($method);
			case 'cache':
				return Cache::fromWSW4method($method);
			case 'opslaan':
				return Opslaan::fromWSW4method($method);
			case 'verwijderen':
				return Verwijderen::fromWSW4method($method);
			case 'uitDB':
				return UitDB::fromWSW4method($method);
			case 'naarDB':
				return NaarDB::fromWSW4method($method);
			case 'gewijzigd':
				return Gewijzigd::fromWSW4method($method);
			case 'velden':
				return Velden::fromWSW4method($method);
			case 'getVeldType':
				return VeldType::fromWSW4method($method);
			case 'getBekijkbareVars':
				return GetBekijkbareVars::fromWSW4method($method);
			case 'getWijzigbareVars':
				return GetWijzigbareVars::fromWSW4method($method);
			case 'getDependencies':
				return GetDependencies::fromWSW4method($method);
			default:
				return $method;
		}
	}
}