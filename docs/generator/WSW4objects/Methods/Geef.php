<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Geef extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$classname = $class->real->name;
		$hasFK = false;

		$letter = 'a';
		foreach($class->IDs as $n => $id)
		{
			if($id->isForeignObject()) {
				$hasFK = true;
				continue;
			}

			$args[] = $letter++;
			$ids[$n] = $id;
		}
		$idstr = '$' . implode(', $', array_keys($ids));
		$fstarg = $args[0];

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief($this->comment . "\n"
			.	"\n"
			.	"geef() kan op verschillende manieren aangeroepen worden:\n"
			.	"- alle parameters netjes invullen (dit heeft de voorkeur)\n"
			.	"- alleen de eerste parameter, met als vorm: \"\$aID_\$bID\"\n"
			.	"- alleen de eerste parameter, met als vorm: array(\$aID, \$bID)"
		)->setReturn("{$classname}|false", "Een {$classname}-object als deze gevonden wordt; False anders.");

		foreach ($args as $arg) {
			$docCommentTemplate->addparam($arg, 'mixed', "Object of ID waarop gezocht moet worden.");
		}

		$generatedCode = $docCommentTemplate->generate(1)
			.	"\tstatic public function geef(";
		$first = true;
		foreach($args as $arg) {
			$generatedCode .= ($first?'':', ').'$'.$arg.($first?'':' = NULL');
			$first = false;
		}
		$generatedCode .= ")\n\t{\n"
			.	"\t\tif(is_null(\$"
			.		implode(")\n"
				.	"\t\t&& is_null(\$", $args) . "))\n"
			.	"\t\t\treturn false;\n"
			.	"\n"
			.	"\t\tif(is_string(\$"
			.		implode(")\n"
				.	"\t\t&& is_null(\$", $args) . "))\n"
			.	"\t\t\t\$$fstarg = static::vanID(\$$fstarg); # string -> array\n"
			.	"\n"
			.	"\t\tif(is_array(\$"
			. implode(")\n"
				.	"\t\t&& is_null(\$", $args) . ")\n"
			.	"\t\t&& count(\$$fstarg) == ".count($args).")\n"
			.	"\t\t{\n";

		$nr = 0;
		foreach($ids as $n => $id) {
			$generatedCode .= "\t\t\t$$n = ".maybeint($n)."$".$args[0]."[".$nr++."];\n";
		}
		$generatedCode .= "\t\t}\n";

		if($hasFK) {
			$nr = 0;
			$first = true;
			foreach($class->IDs as $id)
			{
				if($id->isForeignObject()) {
					$generatedCode .= ($first
							? "\t\telse if("
							: "\n\t\t     && ")
						.	'$'.$args[$nr++].' instanceof '.$id->foreign->name;

					$first = false;
					continue;
				}
				if($id->isForeign())
					continue;

				$generatedCode .= ($first
						? "\t\telse if("
						: "\n\t\t     && ")
					.	'isset($'.$args[$nr++].')';

				$first = false;
			}
			while($nr < count($args)) {
				$generatedCode .= "\n\t\t     && !isset($".$args[$nr++].')';
			}
			$generatedCode .= ")\n"
				.	"\t\t{\n";

			$nr = 0;
			foreach($class->IDs as $n => $id)
			{
				if(!$id->isForeign()) {
					$generatedCode .= "\t\t\t$$n = ".maybeint($n)."$".$args[$nr++].";\n";

					continue;
				}
				if(!$id->isForeignObject())
					continue;

				foreach($id->foreign->IDs as $a => $b)
				{
					if($b->isForeignObject()) continue;
					$keyget = preg_replace_callback("/_(.)/",
						function($matches) { return strtoupper($matches[1]); },
						"get_".$b->upper()
					);
					$tail = array_reverse(explode('_', $a));
					$generatedCode .= "\t\t\t$${n}_$tail[0] = $".$args[$nr]."->$keyget();\n";
				}
				$nr++;
#							, "get_".$found->upper());
			}
			$generatedCode .= "\t\t}\n";
		}
		$generatedCode .= "\t\telse if(isset(\$"
			. implode(")\n"
				.	"\t\t     && isset(\$", $args) . "))\n"
			.	"\t\t{\n";

		$nr = 0;
		foreach($ids as $n => $id) {
			$generatedCode .= "\t\t\t$$n = ".maybeint($n)."$".$args[$nr++].";\n";
		}
		$generatedCode .= "\t\t}\n"
			.	"\n"
			.	"\t\tif(is_null(\$"
			. implode(")\n"
				.	"\t\t|| is_null($", array_keys($ids))."))\n"
			.	"\t\t\tthrow new BadMethodCallException();\n"
			.	"\n"
			.	"\t\tstatic::cache(array( array($idstr) ));\n"
			.	"\t\treturn Entiteit::geefCache(array($idstr), '$classname');\n"
			.	"\t}\n";
		$bestand->addContent($generatedCode);
	}
}
