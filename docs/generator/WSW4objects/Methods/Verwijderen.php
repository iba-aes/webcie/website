<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\Visibility;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Verwijderen extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		global $objects;

		$classname = $class->real->name;

		$generatedCode = $this->codeMethodHeader();

		$generatedCode .= "\t{\n"
			. "\t\tglobal \$WSW4DB;\n"
			. "\n"
			. "\t\tif(!\$this->inDB)\n"
			. "\t\t\tthrow new LogicException('object is not inDB');\n"
			. "\n"
			. "\t\t\$magVerwijderen = \$this->magVerwijderen();\n"
			. "\t\tif(!\$magVerwijderen)\n"
			. "\t\t{\n"
			. "\t\t\treturn(\"Je mag het object niet verwijderen.\");\n"
			. "\t\t}\n";

		foreach ($class->foreignrelations as $c => $isnull) {
			$childclass = $objects['class'][$c . "_Generated"];
			foreach ($childclass->vars as $id) {
				if ($id->isForeignObject() && $id->foreign->name == $class->real->name) {
					// We kunnen alle objecten met een foreign key naar dit object verwijderen,
					// maar als het kan, zetten we dat veld gewoon op null.
					// We mogen een veld op null zetten als die null lust,
					// en als die niet private is (want private staat voor immutable...)
					if ($isnull && !$id->visibility->equals(Visibility::PRIVATE())) {
						// Welke operaties doen we op de foreign klasse?
						// Komt in de comment bij dit stukje code.
						$foreignOperatie = "om de foreign key op null te zetten";

						// Welke methode roepen we aan om te checken
						// dat deze operatie mag?
						$rechtenCheck = "magWijzigen";

						// Wat melden we aan de gebruiker als het misgaat?
						$foutmelding = "mag niet gewijzigd worden";
					} else {
						$foreignOperatie = "om te verwijderen";
						$rechtenCheck = "magVerwijderen";
						$foutmelding = "mag niet verwijderd worden";
					}

					// De naam van de foreign key.
					$foreignKeyNaam = ucfirst($id->name);
					// De naam van de verzameling gerelateerde objecten.
					// (Dit willen we uniek hebben, dus vandaar klasse en foreign key).
					$verzamelingNaam = '$' . $c . "From" . $foreignKeyNaam . "Verz";

					$generatedCode .= "\n"
						. "\t\t// Verzamel alle " . $c . "-objecten die op dit object dependen\n"
						. "\t\t// " . $foreignOperatie . ",\n"
						. "\t\t// en return een error als ze niet aangepakt mogen worden.\n"
						. "\t\t" . $verzamelingNaam . " = " . $c . "Verzameling::from" . $foreignKeyNaam . "(\$this);\n"
						. "\t\t\$returnValue = " . $verzamelingNaam . "->" . $rechtenCheck . "();\n"
						. "\t\tif(\$returnValue !== true)\n"
						. "\t\t{\n"
						. "\t\t\tforeach(\$returnValue as \$i => \$val)\n"
						. "\t\t\t{\n"
						. "\t\t\t\t\$returnValue[\$i] = \"Een object " . $c . " met id \".\$val.\" verwijst naar het te verwijderen object, maar " . $foutmelding . ".\";\n"
						. "\t\t\t}\n"
						. "\t\t\treturn \$returnValue;\n"
						. "\t\t}\n";
				}
			}
		}

		$generatedCode .= "\n"
			. "\t\t//Hier begint de mysql-transactie\n"
			. "\t\tif(!\$foreigncall)\n"
			. "\t\t\t\$WSW4DB->q('START TRANSACTION');\n"
			. "\t\t\$queryarray = array();\n";

		$foreigndeletearray = array();

		if (isset($class->foreignrelations)) {
			foreach ($class->foreignrelations as $c => $isnull) {
				$childclass = $objects['class'][$c . "_Generated"];
				foreach ($childclass->vars as $id) {
					if ($id->isForeignObject() && $id->foreign->name == $class->real->name) {
						if (!$isnull || $id->visibility->equals(Visibility::PRIVATE())) {
							$foreigndeletearray[$c] = $id->name;
						} else {
							$generatedCode .= "\t\tforeach(\$" . $c . "From" . ucfirst($id->name) . "Verz as \$v)\n"
								. "\t\t{\n"
								. "\t\t\t\$v->set" . ucfirst($id->name) . "(NULL);\n"
								. "\t\t}\n"
								. "\t\t\$" . $c . "From" . ucfirst($id->name) . "Verz->opslaan();\n"
								. "\n";
						}
					}
				}
			}
		}

		$generatedCode .= "\t\t// Als de return-value van het verwijderen van dependencies\n"
			. "\t\t// niet een array is, is het een string met een foutcode\n";
		foreach ($foreigndeletearray as $c => $name) {
			$generatedCode .= "\t\t\$returnValue = \$" . $c . "From" . ucfirst($name) . "Verz->verwijderen(true);\n"
				. "\t\tif(!is_array(\$returnValue))\n"
				. "\t\t{\n"
				. "\t\t\treturn \$returnValue;\n"
				. "\t\t} else {\n"
				. "\t\t\t\$queryarray[] = \$returnValue;\n"
				. "\t\t}\n\n";
		}

		$generatedCode .= "\n"
			. "\t\t// uit de DB\n"
			. "\t\t\$query = \$WSW4DB->q('DELETE FROM `$classname`'\n"
			. $class->genSQLWhere(2)
			. "\t\t          .' LIMIT 1'\n"
			. $class->genSQLWhereArgs(2)
			. "\t\t          );\n";

		if ($class->IDsource) {
			$generatedCode .= "\n"
				. "\t\t\$queryarray[] = parent::verwijderen(true);\n";
		}


		$generatedCode .= "\n"
			. "\t\t// Verzamel alle queries in 1 array\n"
			. "\t\t\$finalqueries = array();\n"
			. "\t\t\$it = new RecursiveIteratorIterator(new RecursiveArrayIterator(\$queryarray));\n"
			. "\t\tforeach(\$it as \$singlequery)\n"
			. "\t\t{\n"
			. "\t\t\t\$finalqueries[] = \$singlequery;\n"
			. "\t\t}\n"
			. "\t\t\$finalqueries[] = \$query;\n"
			. "\n"
			. "\t\t// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql\n"
			. "\t\tif(!\$foreigncall)\n"
			. "\t\t{\n"
			. "\t\t\t\$queriesgelukt = true;\n"
			. "\t\t\t\$output = array();\n"
			. "\t\t\tforeach(\$finalqueries as \$singlequery)\n"
			. "\t\t\t{\n"
			. "\t\t\t\tif(\$singlequery !== 1) {\n"
			. "\t\t\t\t\t\$queriesgelukt = false;\n"
			. "\t\t\t\t\t\$output[] = \$singlequery;\n"
			. "\t\t\t\t\tbreak;\n"
			. "\t\t\t\t}\n"
			. "\t\t\t}\n"
			. "\t\t\tif(\$queriesgelukt)\n"
			. "\t\t\t{\n"
			. "\t\t\t\t\$WSW4DB->q('COMMIT');\n"
			. "\t\t\t}\n"
			. "\t\t\telse\n"
			. "\t\t\t{\n"
			. "\t\t\t\t\$WSW4DB->q('ROLLBACK');\n"
			. "\t\t\t\treturn \$output;\n"
			. "\t\t\t}\n"
			. "\t\t}\n";

		if (!$class->IDsource) {
			$generatedCode .= "\n"
				. "\t\t\$this->inDB = False;\n"
				. "\n"
				. "\t\t// unset IDs\n";
			foreach ($class->IDs as $id) {
				$generatedCode .= "\t\t\$this->" . $id->name . " = NULL;\n";
			}
		}

		$generatedCode .= "\n"
			. "\t\tif(\$foreigncall)\n"
			. "\t\t\treturn(\$finalqueries);\n"
			. "\t\telse\n"
			. "\t\t\treturn NULL;\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
