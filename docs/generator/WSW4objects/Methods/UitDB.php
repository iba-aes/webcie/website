<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class UitDB extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$classname = $class->real->name;

		$generatedCode = $this->codeMethodHeader();

		$generatedCode .= "\t{\n"
			. "\t\tglobal \$WSW4DB;\n\n";

		$ts = array();
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			$ts[$var->annotation['LAZY']][] = $var->name;
		}

		if (count($ts) > 1 || $class->IDsource) {
			ksort($ts);
			$generatedCode .= "\t\tswitch(\$veld) {\n";
			foreach ($ts as $l => $vs) {
				if (count($vs) == 0) {
					continue;
				}

				foreach ($vs as $v) {
					$generatedCode .= "\t\tcase '$v':\n";
				}
				if ($l)    // == LAZY, dus uitDB mag
				{
					$generatedCode .= "\t\t\tbreak;\n";
				} else {
					$generatedCode .= "\t\t\tthrow new BadMethodCallException("
						. "\"veld '\$veld' is niet LAZY\");\n";
				}
			}
			$generatedCode .= "\t\tdefault:\n"
				. ($class->IDsource
					? "\t\t\treturn parent::uitDB("
					. "\$veld, \$lang);\n"
					: "\t\t\tthrow new BadMethodCallException("
					. "\"onbekend veld '\$veld'\");\n")
				. "\t\t}\n"
				. "\n";
		}

		$generatedCode .= "\t\tif(isset(\$lang))\n"
			. "\t\t\t\$veld .= '_' . strtoupper(\$lang);\n"
			. "\n"
			. "\t\treturn \$WSW4DB->q('VALUE SELECT `%l`'\n"
			. "\t\t                 .' FROM `$classname`'\n"
			. $class->genSQLWhere(2)
			. "\t\t                 , \$veld\n"
			. $class->genSQLWhereArgs(2)
			. "\t\t                 );\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
