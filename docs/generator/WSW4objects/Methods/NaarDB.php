<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class NaarDB extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		// TODO IDs weigeren te updaten
		$classname = $class->real->name;

		$generatedCode = $this->codeMethodHeader();

		$generatedCode .= "\t{\n"
			. "\t\tglobal \$WSW4DB;\n"
			. "\n";

		$ts = array();
		foreach ($class->vars as $var) {
			if ($var->annotation['PSEUDO']) {
				continue;
			}
			if ($var->isForeignObject()) {
				continue;
			}
			if ($var->type == 'object') {
				continue;
			}
			$type = $var->getLibDBType();    // wordt gebruikt bij count($ts)==1
			$ts[$type][] = $var->name;
		}

		if (count($ts) > 1 || $class->IDsource) {
			ksort($ts);
			$generatedCode .= "\t\tswitch(\$veld) {\n";
			foreach ($ts as $t => $vs) {
				foreach ($vs as $v) {
					$generatedCode .= "\t\tcase '$v':\n";
				}
				$generatedCode .= "\t\t\t\$type = '%$t';\n"
					. "\t\t\tbreak;\n";
			}
			$generatedCode .= "\t\tdefault:\n"
				. ($class->IDsource
					? "\t\t\treturn parent::naarDB("
					. "\$veld, \$waarde, \$lang);\n"
					: "\t\t\tthrow new BadMethodCallException("
					. "\"onbekend veld '\$veld'\");\n")
				. "\t\t}\n"
				. "\n";
		}

		$where = $class->genSQLWhere(2);
		$val = $class->genSQLWhereArgs(2);

		$generatedCode .= "\t\tif(isset(\$lang))\n"
			. "\t\t\t\$veld .= '_' . strtoupper(\$lang);\n"
			. "\n"
			. "\t\t\$WSW4DB->q('UPDATE `$classname`'\n"
			. "\t\t          .\" SET `%l` = "
			. (count($ts) > 1 ? '$type' : "%$type") . "\"\n"
			. (!$class->IDsource
				? "\t\t          .   ', `gewijzigdWanneer` = %s'\n"
				. "\t\t          .   ', `gewijzigdWie` = %i'\n"
				: "")
			. $where
			. "\t\t          , \$veld\n"
			. "\t\t          , \$waarde\n"
			. (!$class->IDsource
				? "\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
				. "\t\t          , \$this->gewijzigdWie\n"
				: "")
			. $val
			. "\t\t          );\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
