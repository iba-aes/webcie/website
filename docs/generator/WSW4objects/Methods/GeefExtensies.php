<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class GeefExtensies extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		if ($this->shouldNotGenerateBody($class)) {
			$this->generateAbstractCode($bestand);
			return;
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft alle klassen terug die een extensie zijn van {$class->real->name}.")
			->setReturn('array|false', "Een array met de klassen as string, of False als er geen klassen zijn.");

		$generatedCode = $docCommentTemplate->generate(1)
			. "\tstatic public function geefExtensies()\n"
			. "\t{\n";

		if ($class->IDsource) {
			$generatedCode .= "\t\treturn false;\n"
				. "\t}\n";

			$bestand->addContent($generatedCode);
			return;
		}

		$arrayElements = implode(
			",\n\t\t\t",
			array_map(function (WSW4class $class) {
				return "'{$class->name}'";
			}, $class->IDchildren)
		);

		$generatedCode .= "\t\treturn array({$arrayElements});\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
