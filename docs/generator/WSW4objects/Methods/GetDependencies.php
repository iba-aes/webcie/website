<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class GetDependencies extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		global $objects;

		// Poep eerst de methodeheader uit...
		$generatedCode = $this->codeMethodHeader();

		// ... en dan de body:
		// We gaan ervan uit dat het eind van de recursie
		// (i.e. een lege lijst) geïmplementeerd is in DBObject.
		$generatedCode .= "\t{\n"
			. "\t\t// Maak een array klassenaam => dependencyverzameling.\n"
			. "\t\t\$dependencies = parent::getDependencies();\n";

		// Zoek alle dependencies uit de dia op,
		// en stop die in een mooie array.
		// TODO: dit lijkt best wel op codeVerwijderen,
		// kan die niet op basis van deze methode in de niet-generated code?
		foreach ($class->foreignrelations as $depNaam => $magNull) {
			// De (gegenereerde editie van de) klasse die op ons dependt.
			$dependencyGenerated = $objects['class'][$depNaam . "_Generated"];
			foreach ($dependencyGenerated->vars as $foreignKey) {
				// We willen wel dat de foreign key naar deze klasse precies wijst,
				// niet naar een parent / childklasse.
				if ($foreignKey->isForeignObject() && $foreignKey->foreign->name == $class->real->name) {
					// De naam van de foreign key.
					$foreignKeyNaam = ucfirst($foreignKey->name);
					// De naam van de verzameling gerelateerde objecten.
					// (Dit willen we uniek hebben, dus vandaar klasse en foreign key).
					$verzamelingNaam = '$' . $depNaam . "From" . $foreignKeyNaam . "Verz";

					$generatedCode .= "\n"
						. "\t\t// Verzamel alle " . $depNaam . "-objecten die op dit object dependen,\n"
						. "\t\t// en stop die in onze returnwaarde.\n"
						. "\t\t" . $verzamelingNaam . " = " . $depNaam . "Verzameling::from" . $foreignKeyNaam . "(\$this);\n"
						. "\t\t\$dependencies['" . $depNaam . "'] = " . $verzamelingNaam . ";\n";
				}
			}
		}

		// Array gevuld dus returnen die hap!
		$generatedCode .= "\n"
			. "\t\treturn \$dependencies;\n"
			. "\t}\n";
		$bestand->addContent($generatedCode);
	}
}
