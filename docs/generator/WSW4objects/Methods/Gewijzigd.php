<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Gewijzigd extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$generatedCode = $this->codeMethodHeader();

		if ($class->getPackage()->name() != 'Basis'
			&& $class->inherits->getPackage()->name() == 'Basis') {
			$generatedCode .= "\t{\n"
				. "\t\tparent::gewijzigd(False);\n"
				. "\n"
				. "\t\tif(\$updatedb)\n"
				. "\t\t{\n"
				. "\t\t\tglobal \$WSW4DB;\n";
			$where = $class->genSQLWhere(3);
			$val = $class->genSQLWhereArgs(3);
			$generatedCode .= "\n"
				. "\t\t\t\$WSW4DB->q('UPDATE LOW_PRIORITY "
				. "`" . $class->real->name . "`'\n"
				. "\t\t\t          .' SET `gewijzigdWanneer` = %s'\n"
				. "\t\t\t          .   ', `gewijzigdWie` = %i'\n"
				. $where
				. "\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
				. "\t\t\t          , \$this->gewijzigdWie\n"
				. $val
				. "\t\t\t          );\n"
				. "\t\t}\n"
				. "\t}\n";

			$bestand->addContent($generatedCode);
			return;
		}
		$generatedCode .= "\t{\n"
			. "\t\tparent::gewijzigd(\$updatedb);\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
