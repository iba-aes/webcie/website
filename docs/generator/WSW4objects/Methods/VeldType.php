<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class VeldType extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft van een veld van {$class->real->name} terug wat het type is.")
			->addParam("field", 'string', "Het veld waarvan het type teruggegeven moet worden.")
			->setReturn('string|null', "Het type als string.");

		$generatedCode = $docCommentTemplate->generate(1)
			. "\tstatic public function getVeldType(\$field)\n"
			. "\t{\n"
			. "\t\tswitch(strtolower(\$field))\n"
			. "\t\t{\n";

		foreach ($class->vars as $var) {
			$generatedCode .= "\t\tcase '" . strtolower($var->name) . "':\n"
				. "\t\t\treturn '" . $var->type . "';\n";
		}

		$generatedCode .= "\t\tdefault:\n"
			. "\t\t\treturn NULL;\n"
			. "\t\t}\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
