<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class MagKlasseBekijken extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker deze klasse mag zien.");

		$bestand->addContent($docCommentTemplate->generate(1)
			. "\tstatic public function magKlasseBekijken()\n"
			. "\t{\n"
			. "\t\treturn hasAuth('{$class->annotation['AUTH_GET']}');\n"
			. "\t}\n");
	}
}
