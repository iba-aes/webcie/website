<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\DocCommentTemplate;
use Generator\Visibility;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class Cache extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$classname = $class->real->name;

		// Array van de private/id-velden van het object.
		// Deze moeten meegegeven worden aan de constructor.
		// Format: [phpveldnaam => $veld]
		$constructorParams = array();
		// Deze velden worden door de constructor geset.
		// Merk op dat het niet hetzelfde is als $constructorParams:
		// daarin staan de parameters, hierin het gevolg.
		// In het bijzonder zijn foreign keys naar WSW4-objecten
		// wel in deze array maar niet in de vorige.
		$constructedFields = array();

		// Zoek in de inheritancehierarchie alle variabelen op.
		$vars = array();
		for($nxt = $class; $nxt; $nxt = $nxt->inherits)
		{
			if($nxt->name === 'Entiteit')
				break;
			$vars = array_merge($nxt->vars, $vars);
		}

		foreach($vars as $id)
		{
			if(!$id->visibility->equals(Visibility::PRIVATE()) && !$id->annotation['PRIMARY'])
				continue;

			// Het auto-increment-veld gaat vanzelf.
			if($id->annotation['PRIMARY'] && $id->type == 'int' && !$id->isForeign())
				continue;

			// We werken bij foreign variabele vanuit het object, niet de subkeys.
			if ($id->isForeignKey())
			{
				continue;
			}
			if ($id->isForeignObject())
			{
				// Door het object te setten, worden ook zijn keys geset.
				foreach ($id->subKeys as $subkey)
				{
					$constructedFields[$subkey->name] = $subkey;
				}
			}

			$constructorParams[$id->name] = $id;
			$constructedFields[$id->name] = $id;
		}

		// Zoek de keys op waarmee we gaan SELECT-en.
		$primaryKeys = []; // Lijst van WSW4Vars
		$primaryKeysForDB = []; // Lijst van strings
		$primaryKeysForPHP = []; // Lijst van strings
		foreach($class->IDs as $id)
		{
			if($id->isForeignObject()) continue;
			$primaryKeys[] = $id;
			$primaryKeysForDB[] = $id->nameForDB;
			$primaryKeysForPHP[] = $id->name;
		}

		// Geef de queryparameter die bij de id's hoort.
		if(count($primaryKeys) > 1) {
			$idsParameter = '%A{';
			foreach($primaryKeys as $id) {
				$idsParameter .= $id->getLibDBType();
			}
			$idsParameter .= '}';
		} else {
			$idsParameter = '%A{i}';
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief(str_replace("\n", "\n\t * ", $this->comment));

		if(count($primaryKeys) == 1) {
			$docCommentTemplate->addParam("ids", 'array', "is een ARRAY met per object een int. "
				.	"\$ids = array( aID, bID, ...)");
		} else {
			$docCommentTemplate->addParam("ids", 'array', "is een ARRAY met per object een array met de ID's. "
				.	"\$ids = array( array(a1ID,a2ID), array(b1ID,b2ID), ...)");
		}
		$docCommentTemplate->addParam("recur", 'bool|array', "recur is een parameter om aan te "
			.	"geven of we een recursieve invulstap moeten doen.");

		$generatedCode = $docCommentTemplate->generate(1)
			.	"\tstatic public function cache(\$ids, \$recur = False)\n"
			.	"\t{\n"
			.	"\t\tglobal \$WSW4DB;\n"
			.	"\t\t\$cachetm = NULL;\n"
			.	"\n";
		$ind = "\t\t";
		if($class->IDsource || $class->IDchildren)
		{
			$generatedCode .= "\t\tif(\$recur === False)\n"
				.	"\t\t{\n";
			$ind = "\t\t\t";
		}
		if($class->IDsource)
		{
			$generatedCode .= "\t\t\t// de basis klasse gaat het allemaal fixen\n"
				.	"\t\t\treturn ".$class->IDsource->name."::cache(\$ids);\n"
				.	"\t\t}\n"
				.	"\n";
		} else
		{
			$generatedCode .= "$ind\$ids = Entiteit::nietInCache(\$ids, '$classname');\n"
				.	"\n"
				.	"$ind// is er nog iets te doen?\n"
				.	"${ind}if(empty(\$ids)) return;\n"
				.	"\n";
			if($class->IDchildren)
			{
				$generatedCode .= "\t\t\t// zoek uit wat de klasse van een id is\n"
					.	"\t\t\t\$res = \$WSW4DB->q('TABLE SELECT `"
					.	implode("`'\n\t\t\t                 .     ', `"
						, $primaryKeysForDB
					) . "`'\n"
					.	"\t\t\t                 .     ', `overerving`'\n"
					.	"\t\t\t                 .' FROM `$classname`'\n"
					.	"\t\t\t                 .' WHERE (`"
					.	implode("`, `" , $primaryKeysForDB) . "`)'\n"
					.	"\t\t\t                 .      ' IN (".$idsParameter.")'\n"
					.	"\t\t\t                 , \$ids\n"
					.	"\t\t\t                 );\n"
					.	"\n"
					.	"\t\t\t\$recur = array();\n"
					.	"\t\t\tforeach(\$res as \$row)\n"
					.	"\t\t\t{\n"
					.	"\t\t\t\t\$recur[\$row['overerving']][]\n"
					.	"\t\t\t\t    = array(\$row['"
					.	implode("']\n"
						.	"\t\t\t\t           ,\$row['"
						, $primaryKeysForDB
					) . "']);\n"
					.	"\t\t\t}\n"
					.	"\t\t\tforeach(\$recur as \$class => \$obj)\n"
					.	"\t\t\t{\n"
					.	"\t\t\t\t\$class::cache(\$obj, True);\n"
					.	"\t\t\t}\n"
					.	"\n"
					.	"\t\t\treturn;\n"
					.	"\t\t}\n"
					.	"\n";
			}
		}

		if($class->IDsource || $class->IDchildren)
		{
			$generatedCode .= "\t\tif(!is_array(\$recur))\n"
				.	"\t\t{\n";
			$ind = "\t\t\t";
		}
		else
		{
			$ind = "\t\t";
		}

		$generatedCode .= "$ind\$cachetm = "
			.	"new ProfilerTimerMark('$classname"
			.	"::cache( #'.count(\$ids).' )');\n"
			.	"${ind}Profiler::getSingleton()->addTimerMark(\$cachetm);\n"
			.	"\n"
			.	"$ind// query alles in 1x\n";
		$select_vars = array();
		$select_from = array();
		for($nxt = $class; $nxt; $nxt = $nxt->inherits)
		{
			if(!$nxt->generated)
				continue;

			$classprefix = '`'.$nxt->real->name.'`.`';
			foreach($nxt->vars as $var)
			{
				if ($var->annotation['PSEUDO'])
					continue;
				if($var->type == 'object') continue;
				if($var->isForeignObject()) continue;
				if($var->annotation['LAZY']) continue;
				if($var->annotation['LANG']) {
					$select_vars[] = $classprefix.$var->nameForDB.'_NL`';
					$select_vars[] = $classprefix.$var->nameForDB.'_EN`';
					continue;
				}
				$select_vars[] = $classprefix.$var->nameForDB.'`';
			}
			if($nxt != $class)
				$select_from[] = $nxt->real->name;

			if(!$nxt->IDsource)
				break;
		}
		if($class->IDsource)
		{
			$select_vars[] = "`".$class->IDsource->name."`.`gewijzigdWanneer`";
			$select_vars[] = "`".$class->IDsource->name."`.`gewijzigdWie`";
		}
		else
		{
			$select_vars[] = "`$classname`.`gewijzigdWanneer`";
			$select_vars[] = "`$classname`.`gewijzigdWie`";
		}

		$first = True;
		foreach($select_vars as $var) {
			$generatedCode .= ($first
					? "$ind\$res = \$WSW4DB->q('TABLE SELECT"
					: "$ind                 .     ',")
				.	" $var'\n";
			$first = False;
		}
		$generatedCode .= "$ind                 .' FROM `$classname`'\n";
		foreach($select_from as $from)
		{
			$generatedCode .= "$ind                 .' LEFT JOIN `$from` USING (`"
				.	implode("`'\n"
					.	"$ind                 .                         ',`"
					, $primaryKeysForDB
				) . "`)'\n";
		}
		$generatedCode .= "$ind                 .' WHERE (`"
			.	implode("`, `", $primaryKeysForDB). "`)'\n"
			.	"$ind                 .      ' IN (".$idsParameter.")'\n"
			.	"$ind                 , \$ids\n"
			.	"$ind                 );\n";

		if($class->IDsource || $class->IDchildren)
		{
			$generatedCode .= "\t\t}\n"
				.	"\t\telse\n"
				.	"\t\t{\n"
				.	"\t\t\t\$res = array(\$recur);\n"
				.	"\t\t}\n";
		}
		$generatedCode .= "\n"
			.	"\t\t\$found = array();\n"
			.	"\t\tforeach(\$res as \$row)\n"
			.	"\t\t{\n"
			.	"\t\t\t\$id = static::naarID(\$row['"
			.	implode("']\n"
				.	"\t\t\t                  ,\$row['"
				, $primaryKeysForDB
			) . "']);\n"
			.	"\n";

		// Vertaal de rij uit de db naar args voor de constructor
		// en velden die geassigned moeten worden.
		// Eerst de constructor:
		$constructorArgsStr = '';
		if(count($constructorParams))
		{
			$first = true;
			foreach($constructorParams as $veld)
			{
				if($first)
				{
					$first = false;
				}
				else
				{
					$constructorArgsStr .= ', ';
				}

				// Als het veld een object is,
				// geven we de subkeys als array.
				if ($veld->isForeignObject())
				{
					$constructorArgsStr .= 'array(';
					$subKeyCount = 0;
					foreach ($veld->subKeys as $subKey)
					{
						if ($subKeyCount > 0)
						{
							$constructorArgsStr .= ', ';
						}
						$constructorArgsStr .= "\$row['" . $subKey->nameForDB . "']";
						$subKeyCount++;
					}
					$constructorArgsStr .= ')';
				}
				else
				{
					// Dit veld is niet een object of foreign key,
					// en komt direct uit de query rollen.
					assert(!$veld->isForeign());
					$constructorArgsStr .= "\$row['" . $veld->nameForDB . "']";
				}
			}
		}

		if($class->IDsource || $class->IDchildren)
		{
			$generatedCode .= "\t\t\tif(is_array(\$recur)) {"
				.	" // dit is een recursieve invulstap\n"
				.	"\t\t\t\t\$obj = static::\$objcache['$classname'][\$id];\n"
				.	"\t\t\t} else {\n"
				.	"\t\t\t\t\$obj = new $classname($constructorArgsStr);\n"
				.	"\t\t\t}\n";
		}
		else
		{
			$generatedCode .= "\t\t\t\$obj = new $classname($constructorArgsStr);\n";
		}
		$generatedCode .= "\n"
			.	"\t\t\t\$obj->inDB = True;\n"
			.	"\n";
		foreach($class->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
			{
				continue;
			}
			// Verwijzingen naar objecten hoeven niet geset.
			if ($var->isForeignObject())
			{
				continue;
			}
			if($var->annotation['LAZY'])
			{
				continue;
			}

			// Dit veld wordt al in de constructor geset:
			// of direct als parameter van de constructor
			if(isset($constructedFields[$var->name]))
			{
				continue;
			}

			switch($var->type) {
				case 'bool':
					$type = '(bool)';
					break;
				default:
					$type = '';
			}

			if($var->annotation['LANG']) {
				$generatedCode .= "\t\t\t\$obj->" . $var->name . "['nl']"
					. " = $type\$row['" . $var->nameForDB . "_NL'];\n"
					.	"\t\t\t\$obj->" . $var->name . "['en']"
					. " = $type\$row['" . $var->nameForDB . "_EN'];\n";
				continue;
			}
			if($var->annotation['NULL'])
				$null = '(is_null($row[\'' . $var->nameForDB . '\'])) ? null : ';
			else
				$null = '';

			switch ($var->type)
			{
				case 'int':
				case 'bool':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = ' . $null . '(' . $var->type . ') $row[\'' . $var->nameForDB . '\'];' . "\n";
					break;
				case 'bigint':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = ' . $null . '(int) $row[\'' . $var->nameForDB . '\'];' . "\n";
					break;
				case 'money':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = ' . $null . '(float) $row[\'' . $var->nameForDB . '\'];' . "\n";
					break;
				case 'string':
				case 'text':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = ' . $null . 'trim($row[\'' . $var->nameForDB . '\']);' . "\n";
					break;
				case 'date':
				case 'datetime':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = new DateTimeLocale($row[\'' . $var->nameForDB . '\']);' . "\n";
					break;
				case 'enum':
					$generatedCode .= "\t\t\t" . '$obj->' . $var->name .
						'  = ' . $null . 'strtoupper(trim($row[\'' . $var->nameForDB . '\']));' . "\n";
					break;
				default:
					$generatedCode .= "\t\t\t\$obj->" . $var->name . " = $type\$row['" . $var->nameForDB . "'];\n";
					break;
			}
		}
		if(!$class->IDsource)
		{
			$generatedCode .= "\t\t\t\$obj->gewijzigdWanneer"
				. " = new DateTimeLocale(\$row['gewijzigdWanneer']);\n"
				.	"\t\t\t\$obj->gewijzigdWie"
				. " = \$row['gewijzigdWie'];\n"
				.	"\t\t\t\$obj->dirty = false;\n";
		}
		if($class->IDsource || $class->IDchildren)
		{
			$generatedCode .= "\t\t\tif(\$recur === True)\n"
				.	"\t\t\t\tself::stopInCache(\$id, \$obj);\n"
				.	($class->IDsource
					?	"\n"
					.	"\t\t\t// naar de super klasse de andere velden\n"
					.	"\t\t\t".$class->inherits->name."::cache(array(), \$row);\n"
					:	""
				)
				.	"\n"
				.	"\t\t\tif(is_array(\$recur))\n"
				.	"\t\t\t\treturn; // dit was een recursieve invul stap\n";
		}
		else
		{
			$generatedCode .= "\t\t\tself::stopInCache(\$id, \$obj);\n";
		}
		$generatedCode .= "\n"
			.	"\t\t\t\$obj->errors = array();\n"
			.	"\t\t\tforeach(static::velden('get') as \$veld)\n"
			.	"\t\t\t\t\$obj->errors[\$veld] = False;\n"
			.	"\n"
			.	"\t\t\t\$found[\$id] = True;\n"
			.	"\t\t}\n"
			.	"\n"
			.	"\t\t// markeer ook de ids die we niet konden vinden\n"
			.	"\t\tforeach(\$ids as \$id)\n"
			.	"\t\t{\n"
			.	"\t\t\tif(!isset(\$found[static::naarID(\$id)]))\n"
			.	"\t\t\t\tself::stopInCache(\$id, False);\n"
			.	"\t\t}\n"
			.	"\n"
			.	"\t\tif(\$cachetm) \$cachetm->markEnd();\n"
			.	"\t}\n";
		$bestand->addContent($generatedCode);
	}
}
