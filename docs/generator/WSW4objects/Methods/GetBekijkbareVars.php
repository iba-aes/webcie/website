<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

class GetBekijkbareVars extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$return = <<<EOT
	public function getBekijkbareVars(\$vars = NULL)
	{
		if(!\$this->magBekijken())
			return array();

		if(is_null(\$vars) || !is_array(\$vars))
			\$vars = array();

		if(!isset(\$vars['vars']))
			\$vars['vars'] = array();
		\$vars['vars'] += static::velden('get');
		if(!isset(\$vars['verzamelingen']))
			\$vars['verzamelingen'] = array();
		\$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars(\$vars);
	}

EOT;
		$bestand->addContent($return);
	}
}
