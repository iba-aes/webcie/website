<?php

namespace Generator\WSW4objects\Methods;

use Generator\Bestand;
use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;

final class GeefID extends WSW4method
{
	public function generateCodeBody(Bestand $bestand, WSW4class $class)
	{
		$generatedCode = $this->codeMethodHeader();

		$generatedCode .= "\t{\n"
			. "\t\treturn static::naarID("
			. (count($class->IDs) > 1 ? " " : "");

		$first = True;
		foreach ($class->IDs as $idn => $idv) {
			if ($idv->isForeignObject()) {
				continue;
			}
			$getter = preg_replace_callback("/_(.)/",
				function ($matches) {
					return strtoupper($matches[1]);
				},
				'get_' . $idn
			);
			$generatedCode .= ($first ? "" : "\n\t\t                      , ")
				. "\$this->$getter()";
			$first = False;
		}
		$generatedCode .= (count($class->IDs) > 1 ? "\n\t\t                      " : "")
			. ");\n"
			. "\t}\n";

		$bestand->addContent($generatedCode);
	}
}
