<?php

namespace Generator\WSW4objects;

/**
 * Class WSW4package
 * @package Generator\WSW4objects
 */
class WSW4package
{
	/**
	 * @var string
	 */
	private $id;
	/**
	 * @var string
	 */
	private $name;
	/**
	 * @var WSW4package|null
	 */
	private $parent;

	/**
	 * WSW4package constructor.
	 * @param string $id
	 * @param string $name
	 * @param WSW4package|null $parent
	 */
	public function __construct(string $id, string $name, ?WSW4package $parent)
	{
		$this->id = $id;
		$this->name = $name;

		if (!is_null($parent)) {
			$this->parent = $parent;
		}
	}

	/**
	 * @return string
	 */
	public function name(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function parent(): string
	{
		return $this->parent;
	}

	/**
	 * @return string
	 */
	public function id(): string
	{
		return $this->id;
	}

	/**
	 * @param mixed[] $objects
	 */
	public function check($objects)
	{
		if (isset($this->parent) && !array_key_exists($this->parent->id(), $objects['id'])) {
			error($this, "1 parent not a valid object");
		}
	}

	/**
	 * @return string
	 */
	public function path(): string
	{
		$path = '';
		if ($this->parent) {
			$path = $this->parent->path();
		}

		return $path . $this->name . '/';
	}
}
