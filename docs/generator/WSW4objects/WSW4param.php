<?php

namespace Generator\WSW4objects;

/**
 * Class WSW4param
 * @package Generator\WSW4objects
 */
use Generator\Bestand;

class WSW4param
{
	/**
	 * @var string
	 */
	private $name;
	/**
	 * @var string
	 */
	private $default;

	/**
	 * WSW4param constructor.
	 * @param string $name
	 * @param string $default
	 */
	public function __construct(string $name, string $default)
	{
		$this->name = $name;
		$this->default = $default;
	}

	/**
	 * @return string
	 */
	public function generateCode(): string
	{
		return '$' . $this->name
			. ($this->default ? ' = '.$this->default : '');
	}
}
