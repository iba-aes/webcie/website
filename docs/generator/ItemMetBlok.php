<?php

abstract class ItemMetBlok
{
	protected $codeBlok;

	public function __construct()
	{
		$this->codeBlok = new CodeBlok();
	}

	public function addContent($content)
	{
		$this->codeBlok->addContent($content);
		return $this;
	}
}
