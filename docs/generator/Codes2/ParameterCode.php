<?php

namespace Generator\Codes;

final class ParameterCode implements Stringable
{
	private $naam;
	private $defaultWaarde;

	public function __construct($naam, $defaultWaarde = null)
	{
		$this->naam = $naam;
		$this->defaultWaarde = $defaultWaarde;
	}

	public function __toString()
	{
		$str = "\${$this->naam}";

		if($this->defaultWaarde) {
			$str .= " = {$this->defaultWaarde}";
		}

		return $str;
	}
}
