<?php

namespace Generator\Codes;

final class CommentCode implements Stringable
{
	private $comments;
	private $multiLine;

	public function __construct($multiLine = true)
	{
		$this->comments = [];
		$this->multiLine = $multiLine;
	}

	public function addComment($comment)
	{
		$this->comments[] = $comment;
		return $this;
	}

	public function __toString()
	{
		$str = "/**";

		if($this->multiLine) {
			$contents = implode("\n", $this->comments);
			$str .= "\n";
			$str .= preg_replace('/^/m', ' * ', $contents);
			$str .= "\n */";
		} else {
			$str .= " " . implode(" ", $this->comments) . " **/";
		}

		$str .= "\n";

		return $str;
	}
}
