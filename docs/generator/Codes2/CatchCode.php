<?php

namespace Generator\Codes;

final class CatchCode implements Stringable, BlokGebruiker
{
	private $catch;
	private $blok;

	public function __construct($catch)
	{
		$this->catch = $catch;
		$this->blok = new Blok(true);
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok->addContent($content);
		return $this;
	}

	public function __toString()
	{
		return "catch({$this->catch}) {$this->blok->__toString()}";
	}
}
