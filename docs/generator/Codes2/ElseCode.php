<?php

namespace Generator\Codes;

final class ElseCode implements Stringable, BlokGebruiker
{
	private $blok;
	private $conditie;

	public function __construct($conditie = null)
	{
		$this->conditie = $conditie;
		$this->blok = new Blok(false);
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok->addContent($content);
		return $this;
	}

	public function __toString()
	{
		$str = "else ";

		if(!is_null($this->conditie)) {
			$str = "elseif({$this->conditie}) ";
		}

		$str .= $this->blok->__toString();

		return $str;
	}
}
