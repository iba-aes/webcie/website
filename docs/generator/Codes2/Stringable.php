<?php

namespace Generator\Codes;

interface Stringable
{
	public function __toString();
}
