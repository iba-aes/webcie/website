<?php

namespace Generator;

/**
 * Class Bestand
 * @package Generator\Helpers
 */
class Bestand
{
	/**
	 * De constante waarmee alle files worden geprefixed.
	 */
	const FILE_PREFIX = "../www/";
	/**
	 * @var string De naam van het bestand.
	 */
	private $name;
	/**
	 * @var null De pointer naar het bestand (geopend dmv fopen).
	 */
	private $filePointer;
	/**
	 * @var array De content die in het bestand moet komen te staan.
	 */
	private $content;

	/**
	 * Bestand constructor.
	 * @param string $name De naam van het bestand.
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
		$this->filePointer = null;
		$this->content = [];
	}

	/**
	 * Geeft het bestand met prefix terug.
	 * @return string
	 */
	private function getFile(): string
	{
		return self::FILE_PREFIX . $this->name;
	}

	/**
	 * Opent het bestand.
	 * @param bool $force Of we het bestand geforceerd moeten openen.
	 * @return $this
	 */
	private function open(bool $force = true): self
	{
		$file = $this->getFile();
		$path = dirname($file);

		if (!empty($path) && !file_exists($path)) {
			mkdir($path, 0755, true);
		}

		if (!file_exists($file) || $force) {
			$this->filePointer = fopen($file, 'w+');
		}

		return $this;
	}

	/**
	 * Sluit de pointer van het bestand.
	 */
	private function close()
	{
		if (!is_null($this->filePointer)) {
			fclose($this->filePointer);
		}
	}

	/**
	 * Checkt of het bestand al bestat.
	 * @return bool Of het bestand al bestaat.
	 */
	public function fileExists(): bool
	{
		$file = $this->getFile();
		return file_exists($file);
	}

	/**
	 * Voegt content toe om later naar het bestand weg te schrijven.
	 * @param string $content De content om te schrijven.
	 */
	public function addContent(string $content = "")
	{
		$this->content[] = $content;
	}

	/**
	 * Voegt meerdere lijnen als content toe.
	 * @param string[] $content De content om toe te voegen.
	 */
	public function addMultiContent(array $content = [])
	{
		$this->content = array_merge($this->content, $content);
	}

	/**
	 * Schrijft alle content naar het bestand toe.
	 * @param bool $force Of het bestand geforceerd geopend moet worden.
	 */
	public function write(bool $force = true)
	{
		$this->open($force);
		foreach ($this->content as $content) {
			fwrite($this->filePointer, $content);
		}
		$this->close();
	}
}