<?php

final class FunctieCode extends ItemMetBlok implements Stringable
{
	private $parameters;
	private $naam;
	private $isPublic;
	private $isProtected;
	private $isPrivate;
	private $isStatic;
	private $isAbstract;
	private $isFinal;

	public function __construct($naam)
	{
		parent::__construct();

		$this->naam = $naam;
		$this->parameters = [];
		$this->isPublic = false;
		$this->isProtected = false;
		$this->isPrivate = false;
		$this->isStatic = false;
		$this->isAbstract = false;
		$this->isFinal = false;
	}

	public function setPublic()
	{
		if($this->isPrivate || $this->isProtected) {
			throw new \Exception("Private/protected methode mag niet public gemaakt worden");
		}

		$this->isPublic = true;
		return $this;
	}

	public function setPrivate()
	{
		if($this->isPublic || $this->isProtected) {
			throw new \Exception("Public/protected methode mag niet private gemaakt worden");
		}

		$this->isPrivate = true;
		return $this;
	}

	public function setProtected()
	{
		if($this->iisPublic || $this->isPrivate) {
			throw new \Exception("Public/private methode mag niet protected gemaakt worden");
		}

		$this->isProtected = true;
		return $this;
	}

	public function setStatic()
	{
		$this->isStatic = true;
		return $this;
	}

	public function setFinal()
	{
		if($this->isAbstract) {
			throw new \Exception("Abstract methode mag niet final gemaakt worden");
		}

		$this->isFinal = true;
		return $this;
	}

	public function setAbstract()
	{
		if($this->isFinal) {
			throw new \Exception("Final methode mag niet abstract gemaakt worden");
		}

		$this->isAbstract = true;
		return $this;
	}

	public function addParameter(ParameterCode $parameter)
	{
		$this->parameters[] = $parameter;
		return $this;
	}

	public function __toString()
	{
		$functieStr = "";

		if($this->isAbstract) {
			$functieStr .= "abfunctieStract ";
		}

		if($this->isFinal) {
			$functieStr .= "final ";
		}

		if($this->isPublic) {
			$functieStr .= "public ";
		}

		if($this->isProtected) {
			$functieStr .= "protected ";
		}

		if($this->isPrivate) {
			$functieStr .= "private ";
		}

		if(!$this->isPrivate && !$this->isProtected && !$this->isPublic) {
			$functieStr .= "public ";
		}

		if($this->isStatic) {
			$functieStr .= "static ";
		}

		$functieStr .= "function {$this->naam}(";

		$parameters = array_map(function($parameter) {
			return $parameter->__toString();
		}, $this->parameters);
		$functieStr .= implode(",\n\t\t", $parameters);

		$functieStr .= ")\n";

		$functieStr .= $this->codeBlok->__toString();

		return $functieStr;
	}
}
