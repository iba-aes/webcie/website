FROM registry.gitlab.com/iba-aes/webcie/website-docker-image-dependencies:latest

SHELL ["/bin/bash", "-c"]
ENV CI true
WORKDIR /var/www/html/

# We switchen hier naar de dockeruser om te zorgen dat alles onder de dockeruser wordt aangemaakt.
USER dockeruser

RUN mkdir website

# Kopieeren respecteert het USER command niet dus we moeten hier alles converten naar de dockeruser
COPY . website/
RUN sudo chown -R dockeruser:dockeruser website

WORKDIR /var/www/html/website

RUN eval $PY_INIT; pyenv virtualenv 3.12 website; pyenv local website; cd www; pyenv local website;

# Install pip3 dependencies
RUN eval $PY_INIT; pip install -r requirements.txt; pip install -e .

WORKDIR /var/www/html/website

# Dit kan alleen root
USER root
RUN echo "server" > /etc/function

USER dockeruser
RUN eval $PY_INIT; cd www; ./manage build --enable-debug --hostname=localhost --apache_user=www-data

