"""test_auth - Tests voor authorisatielogica."""

from whoswhopy.auth import AuthRecht, GodAuth, InstelbareAuth, auth_ingelogd_als
import whoswhopy.config as config


def test_godrechten():
    """Met godrechten kun je echt alles."""
    auth = GodAuth()
    level, wachtwoord = auth.geef_db_auth()
    assert level == config.geef('DB_USER_PREFIX', '') + 'god'
    for recht in AuthRecht:
        assert auth.check_recht(recht)


def test_rechten_instellen():
    """Bij instelbare rechten kun je kiezen of je media mag bekijken."""
    geen_media_bekijken = InstelbareAuth('ingelogd', set())
    wel_media_bekijken = InstelbareAuth('ingelogd', {AuthRecht.media_bekijken})

    ingelogde_db_user = config.geef('DB_USER_PREFIX', '') + 'ingelogd'
    assert geen_media_bekijken.geef_db_auth()[0] == ingelogde_db_user
    assert wel_media_bekijken.geef_db_auth()[0] == ingelogde_db_user

    assert not geen_media_bekijken.check_recht(AuthRecht.media_bekijken)
    assert wel_media_bekijken.check_recht(AuthRecht.media_bekijken)


def test_auth_ingelogd_als():
    """Kijk wat voor rechten je hebt aan de hand van een paar contactid's."""
    # Lees uit de configuratie contactid's voor bestuur en webcie uit.
    contactid_bestuur = config.geef('bestuur')[0].contactid
    contactid_webcie = config.geef('webcie')[0].contactid
    assert not config.webcielid(contactid_bestuur)
    # Kies een arbitrair lidnummer dat niet bestuur of webcie is.
    contactid_overig = 1
    assert not config.bestuurslid(contactid_overig)
    assert not config.webcielid(contactid_overig)

    auth_gast = auth_ingelogd_als(None)
    assert auth_gast.geef_db_auth()[0] == config.geef('DB_USER_PREFIX', '') + 'gast'
    assert not auth_gast.check_recht(AuthRecht.media_bekijken)
    assert not auth_gast.check_recht(AuthRecht.beheerpaginas)
    assert not auth_gast.check_recht(AuthRecht.auth_customizen)

    auth_ingelogd = auth_ingelogd_als(contactid_overig)
    assert (
        auth_ingelogd.geef_db_auth()[0]
        == config.geef('DB_USER_PREFIX', '') + 'ingelogd'
    )
    assert auth_ingelogd.check_recht(AuthRecht.media_bekijken)
    assert not auth_ingelogd.check_recht(AuthRecht.beheerpaginas)
    assert not auth_ingelogd.check_recht(AuthRecht.auth_customizen)

    auth_bestuur = auth_ingelogd_als(contactid_bestuur)
    assert auth_bestuur.geef_db_auth()[0] == config.geef('DB_USER_PREFIX', '') + 'god'
    assert auth_bestuur.check_recht(AuthRecht.media_bekijken)
    assert auth_bestuur.check_recht(AuthRecht.beheerpaginas)
    assert not auth_bestuur.check_recht(AuthRecht.auth_customizen)

    auth_webcie = auth_ingelogd_als(contactid_webcie)
    assert auth_webcie.geef_db_auth()[0] == config.geef('DB_USER_PREFIX', '') + 'god'
    assert auth_webcie.check_recht(AuthRecht.media_bekijken)
    assert auth_webcie.check_recht(AuthRecht.beheerpaginas)
    assert auth_webcie.check_recht(AuthRecht.auth_customizen)
