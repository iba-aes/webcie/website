import pytest

from whoswhopy.php.vreemde_functie import VreemdeError, VreemdeFunctie


def test_rooktest():
    """Kijk of we ueberhaupt output kunnen oversturen."""
    rooktest = VreemdeFunctie('space/test_space-cli.php', 'Space\\rooktest')
    assert rooktest() == 'test'


def test_statische_methode():
    """We kunnen statische methodes aanroepen via hetzelfde mechanisme."""
    test_statisch = VreemdeFunctie(
        'space/test_space-cli.php', 'Space\\CLITest', 'testStatisch'
    )
    assert test_statisch() == 'test statisch'


def test_foutmeldingen():
    """Als je een onbestaande functie probeert aan te roepen, krijg je een fout."""

    with pytest.raises(VreemdeError):
        bestaat_niet = VreemdeFunctie('space/test_space-cli.php', 'bestaatNiet')
        bestaat_niet()
