"""conftest.py - Configuratie voor de tests van whoswhopy."""

import pathlib
import secrets

import flask
import pytest

SESSIE_COOKIE = 'whoswhopy-session-test'


class TempDB:
    """Maak een SQLite-database aan in een tijdelijk bestand en return een verbinding ermee.

    De database bevat automatisch alle tabellen zoals in de dia beschreven.

    We gebruiken een tijdelijk bestand zodat meerdere verbindingen dezelfde data zien.
    """

    def __init__(self, tempfile):
        self.tempfile = tempfile
        import sqlalchemy
        import sqlalchemy.orm
        from whoswhopy.gen.Basis.Entiteit import Entiteit

        wsw4db_engine = sqlalchemy.create_engine('sqlite:///' + str(self.tempfile))

        # Importeer alle tabellen en maak die aan.
        import whoswhopy.gen.alles

        Entiteit.metadata.create_all(wsw4db_engine)

        # Geef de verbinding als sessie.
        self.sessionmaker = sqlalchemy.orm.sessionmaker(bind=wsw4db_engine)

    def __call__(self, auth):
        # We hoeven alleen een nieuwe sessie aan te maken :D
        return self.sessionmaker()


@pytest.fixture(scope='function')
def content_root(tmpdir):
    """Geef een directory waar we veilig uploaded tsjak kunnen neerzetten.

    :rtype: pathlib.Path
    """
    return pathlib.Path(tmpdir)


@pytest.fixture(scope='function')
def app(tmpdir, content_root):
    from whoswhopy.app import maak_app

    app = maak_app(
        geef_wsw4db=TempDB(tmpdir / 'whoswho4.sql'),
        # PHP-sessies werken niet fijn voor testen (bijvoorbeeld rechtenissues).
        session_interface=flask.sessions.SecureCookieSessionInterface(),
        content_root=content_root,
    )
    # Voor SecureCookieSessionInterface moeten we wel een 'geheime' sleutel instellen.
    app.secret_key = 'supergeheim'
    return app
