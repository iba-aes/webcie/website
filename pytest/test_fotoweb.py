"""test_fotoweb.py - Tests voor de basale operatie van Fotoweb."""

import flask
import pytest

from datetime import datetime
from whoswhopy.auth import GodAuth
from whoswhopy.fotoweb import Afmeting, _media_geef_pad_ffi
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.gen.Lid import Lid
from whoswhopy.gen.Voornaamwoord import Voornaamwoord

# Een geldig png-bestand dat erg klein is.
_kleine_png = b'''\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x01\x00\x00\x00\x01\x00\x01\x03\x00\x00\x00f\xbc:%\x00\x00\x00\x03PLTE\xb5\xd0\xd0c\x04\x16\xea\x00\x00\x00\x1fIDATh\x81\xed\xc1\x01\r\x00\x00\x00\xc2\xa0\xf7Om\x0e7\xa0\x00\x00\x00\x00\x00\x00\x00\x00\xbe\r!\x00\x00\x01\x9a`\xe1\xd5\x00\x00\x00\x00IEND\xaeB`\x82'''


@pytest.fixture()
def nieuwe_media():
    """Geef een Media-object."""
    media = Media(
        soort='FOTO',
        licence='CC3.0-BY-NC-SA',
        premium=False,
        views=0,
        lock=False,
        rating=50,
        breedte=1024,
        hoogte=768,
    )
    return media


def mock_upload(content_root, media):
    """Mock de cache. (-ba, mock de cacheba).

    Stop een geldig nepbestand in de fotocache, zodat we die in latere tests kunnen uitlezen.
    """
    assert media.mediaID, "Kan bestand alleen opslaan als ID bekend is."
    extensie = 'png'
    for afmeting in Afmeting:
        bestandspad = _media_geef_pad_ffi(
            mediaid=str(media.mediaID), format=str(afmeting), extensie=extensie
        )
        volledig_pad = content_root / bestandspad
        # Zorg ervoor dat de directories allemaal bestaan.
        volledig_pad.parent.mkdir(parents=True, exist_ok=True)
        with volledig_pad.open('wb') as bestand:
            bestand.write(_kleine_png)
    return media


@pytest.fixture()
def nieuw_voornaamwoord():
    voornaamwoord = Voornaamwoord(
        woordID=1,
        soort_NL="Onzijdig",
        soort_EN="Neuter",
        onderwerp_NL="die",
        onderwerp_EN="",
        voorwerp_NL="",
        voorwerp_EN="",
        bezittelijk_NL="",
        bezittelijk_EN="",
        wederkerend_NL="",
        wederkerend_EN="",
        ouder_NL="",
        ouder_EN="",
        kind_NL="",
        kind_EN="",
        brusje_NL="",
        brusje_EN="",
        kozijn_NL="",
        kozijn_EN="",
        kindGrootouder_NL="",
        kindGrootouder_EN="",
        kleinkindOuder_NL="",
        kleinkindOuder_EN="",
    )
    return voornaamwoord


@pytest.fixture()
def nieuw_lid():
    lid = Lid(
        contactID=1,
        vakidOpsturen=False,
        aes2rootsOpsturen=True,
        achternaam="Pieter",
        voornaamwoord_woordID=1,
        overleden=False,
        voornaamwoordZichtbaar=True,
        lidToestand="LID",
        lidMaster=False,
        lidExchange=False,
        inAlmanak=False,
    )
    return lid


def test_uitgelogde_media(app, client, nieuwe_media):
    """Je mag media niet bekijken als je bent uitgelogd."""
    # Stop de media in de databaas.
    wsw4db = app.geef_wsw4db(GodAuth())
    wsw4db.add(nieuwe_media)
    wsw4db.commit()

    with client.session_transaction() as session:
        session['mylidnr'] = None
    assert (
        client.get(
            flask.url_for(
                'geef_media', media_id=nieuwe_media.mediaID, afmeting='origineel'
            )
        ).status_code
        == 403
    )


def test_ingelogde_media(
    app, content_root, client, nieuwe_media, nieuw_voornaamwoord, nieuw_lid
):
    """Je mag media wel bekijken als je bent ingelogd."""
    # Stop de media in de databaas.
    wsw4db = app.geef_wsw4db(GodAuth())
    wsw4db.add(nieuw_voornaamwoord)
    wsw4db.add(nieuw_lid)
    wsw4db.add(nieuwe_media)
    wsw4db.commit()

    mock_upload(content_root, nieuwe_media)

    with client.session_transaction() as session:
        session['mylidnr'] = nieuw_lid.contactID
    assert (
        client.get(
            flask.url_for(
                'geef_media',
                media_id=nieuwe_media.mediaID,
                afmeting='origineel',
                extensie='png',
            )
        ).status_code
        == 200
    )
