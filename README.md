Dit is de broncode van de enige echte [A-Eskwadraat][aes] website!

Algemene documentatie staat op [de wiki][wiki], en meer gedetailleerde
documentatie staat in [Doxygen][dg].

[aes]: https://a-eskwadraat.nl/
[wiki]: https://mediawiki.a-eskwadraat.nl/wiki/index.php/WebCie
[dg]: https://doxygen-www.a-eskwadraat.nl/
