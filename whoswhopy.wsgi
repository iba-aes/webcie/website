# WSGI-applicatie om WhosWhoPy te runnen.
# Het object `application` moet een geldige WSGI-application zijn.
# In principe zorgt Flask ervoor dat dat zo is.

# Stel als eerste stap de virtualenv in.
import os
working_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(working_dir)
python_home = working_dir + '/venv'
activate_this = python_home + '/bin/activate_this.py'
exec(open(activate_this).read(), dict(__file__=activate_this))

# Nu kunnen we alles inladen.
from whoswhopy.app import maak_app
application = maak_app()
