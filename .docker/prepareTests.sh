#!/bin/bash

# Script om de database op te zetten. Dit maakt de database door middel van alembic,
# en maakt ook de test_boeken en test_benamite tabellen.
set -o allexport
source .env
set +o allexport
alembic upgrade head
mysql --host=$DATABASE_HOST --user=$DATABASE_USER -e "CREATE USER 'vm_aeskwadraat_whoswho4_god'@'%';"
mysql --host=$DATABASE_HOST --user=$DATABASE_USER -e "GRANT ALL PRIVILEGES ON test_whoswho4.* TO 'vm_aeskwadraat_whoswho4_god'@'%';"
mysql --host=$DATABASE_HOST --user=$DATABASE_USER -e "CREATE DATABASE IF NOT EXISTS test_benamite;"
mysql --host=$DATABASE_HOST --user=$DATABASE_USER -e "CREATE USER 'vm_aeskwadraat_benamite_god'@'%';"
mysql --host=$DATABASE_HOST --user=$DATABASE_USER -e "GRANT ALL PRIVILEGES ON test_benamite.* TO 'vm_aeskwadraat_benamite_god'@'%';"
mysql --host=$DATABASE_HOST --user=$DATABASE_USER test_benamite < scripts/database.benamite.sql
mysql --host=$DATABASE_HOST --user=$DATABASE_USER test_benamite < scripts/databasecontent.benamite.sql

